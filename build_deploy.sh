#!/bin/bash

read -p "Selecione o servidor para deploy (1 - poc, 2 - producao)" readserver
case $readserver in
	[1]* ) serverhost=pocserver.gimb.com.br;;
	[2]* ) serverhost=masterserver.gimb.com.br;;
	* ) echo "opcao invalida"; exit;;
esac


echo '############################ Build do projeto ############################'

mvn clean package -Dmaven.test.skip=true

echo '############################ Fim do Build ############################'

echo ''
echo ''
echo ''

echo '############################ Transferindo nova versão para o servidor ############################'

ssh ubuntu@${serverhost} rm /home/ubuntu/apiGIMB.jar
scp target/apiGIMB.jar ubuntu@${serverhost}:~

echo '############################ Fim da transferência ############################'

echo ''
echo ''
echo ''

echo '############################ Undeploy da versão atual ############################'

ssh ubuntu@${serverhost} << 'ENDSSH'

echo 'stop api-gimb-old => sudo service api-gimb-old stop'
sudo service api-gimb-old stop

echo 'remove last backup file => rm ~/backup/apiGIMB.jar'
rm ~/backup/apiGIMB.jar

echo 'backup apiGIMB.jar file => mv /gimb/old_api/apiGIMB.jar ~/backup/apiGIMB.jar'
mv /gimb/old_api/apiGIMB.jar ~/backup/apiGIMB.jar

echo 'move new apiGIMB.jar to correct directory => mv ~/apiGIMB.jar /gimb/old_api/'
mv ~/apiGIMB.jar /gimb/old_api/

echo 'aguardar 15 segundos'
sleep 15

echo '############################ Fim do undeploy ############################'

echo ''
echo ''
echo ''

echo '############################ Deploy da nova versão ############################'

echo 'Voltar servico api-gimb-old => sudo service api-gimb-old start'
sudo service api-gimb-old start

ENDSSH

echo '############################ Fim do undeploy ############################'

mvn clean 
