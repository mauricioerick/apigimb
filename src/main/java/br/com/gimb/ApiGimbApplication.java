package br.com.gimb;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.GregorianCalendar;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

import com.google.firebase.FirebaseApp;

import br.com.gimb.ws.filter.TokenFilter;
import br.com.gimb.ws.util.Util;
import io.sentry.Sentry;

@SpringBootApplication
public class ApiGimbApplication {

	public static final long TEMPO = 3600000;
	//public static final long TEMPO = 120000;
	
	@Bean
	public FilterRegistrationBean getFilterJwt() {
		FilterRegistrationBean filtro = new FilterRegistrationBean();
		filtro.setFilter(new TokenFilter());
		filtro.addUrlPatterns("/admin/*");

		return filtro;
	}

	public static void main(String[] args) {
		SpringApplication.run(ApiGimbApplication.class, args);
		Sentry.init(Util.getSentryDSN(false));
		if(System.getenv("GOOGLE_APPLICATION_CREDENTIALS") != null )
			FirebaseApp.initializeApp();
		
		//verificando a cada 24 se tem algum cartao que precisa ser renovado o credito
		Timer timer = null;
		if(timer == null) {
			timer = new Timer();
			TimerTask tarefa = new TimerTask() {
				
				@Override
				public void run() {
					GregorianCalendar d = new GregorianCalendar();
					int hr = d.get(GregorianCalendar.HOUR_OF_DAY);
					
					if(hr == 1) {
						try {
							String[] repositories = {"multar", "multartec", "solis", "pampili", "pecompe", "raizen", "zenega", "expert-radiocom", "tecaut", "grupoferrante", "solinftec",
									"itaetemaquinas", "climaenergia", "graciano", "negrao", "controle", "raizen-paraiso"};
							//String[] repositories = {"localhost"};
							checkRenewal(repositories);
							
						}catch (Exception e) {
							e.printStackTrace();	
						}
					}
				}
			};
			timer.scheduleAtFixedRate(tarefa, TEMPO, TEMPO);
		}
	}
	
	private static void checkRenewal(String[] repositories) throws Exception {
		for(int l=0; l<repositories.length; l++) {
			String baseName = repositories[l];
			String url = "http://"+baseName+".gimb.com.br/apiGIMB/allCardsToRenewal";
			//String url = "http://"+baseName+".gimb.com.br:8100/apiGIMB/allCardsToRenewal";
			setCards(url);
		}
	}
	
	private static void setCards(String urlStr) throws Exception {
		URL url = new URL(urlStr);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("POST");
		con.setReadTimeout(20000);
		con.setConnectTimeout(20000);
		con.setDoOutput(true);
		con.setDoInput(true);
		
		int responseCode = con.getResponseCode();
		System.out.println("RESPONSE CODE: " + responseCode);
		
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
		
		while((inputLine = in.readLine()) != null)
			response.append(inputLine);

		in.close();
	}
}


