package br.com.gimb.ws.util.aws;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.HttpMethod;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.util.IOUtils;

import br.com.gimb.ws.model.CustomPicture;

/**
 * AwsS3Utils
 */
public class AwsS3Utils extends AwsUtils {
    public static final String s3 = "gimb-s3-";
    private static final String BUCKET_ADMIN = "gimb-admin";
    
    private static String buildBucketName(String baseName) {
    	if (baseName.startsWith(s3)) return baseName;
    	else return s3+baseName;
    }
	
	private static void createBucket(final AmazonS3 s3Client, String baseName) {
		if (!s3Client.doesBucketExistV2(buildBucketName(baseName)))
			s3Client.createBucket(buildBucketName(baseName));
	}

    public static AmazonS3 initializeS3Client(String baseName) {
		AmazonS3 s3Client;
		try {
			AWSCredentials credentials = new BasicAWSCredentials(keyAccess, passAccess);
			s3Client = AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.US_EAST_1)
					.build();
			createBucket(s3Client, buildBucketName(baseName));
		} catch (Exception e) {
			throw new AmazonClientException( "Cannot load the credentials from the credential profiles file. "
                    + "Please make sure that your credentials file is at the correct "
                    + "location (~/.aws/credentials), and is in valid format.",
                    e);
		}
		
		
		return s3Client;
	}
	
	public static void deleteImg(String keyName, String baseName) {
		try {
            AmazonS3 s3Client = initializeS3Client(buildBucketName(baseName));
            s3Client.deleteObject(new DeleteObjectRequest(buildBucketName(baseName), keyName));
        }
        catch(AmazonServiceException e) {
            e.printStackTrace();
        }
        catch(SdkClientException e) {
            e.printStackTrace();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
	}
	
	public static void deleteXLS(String nameXls) {
		try {
			AmazonS3 s3Client = initializeS3Client(BUCKET_ADMIN);
			s3Client.deleteObject(new DeleteObjectRequest(BUCKET_ADMIN, nameXls));
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void generatePreSignedUrl(List<CustomPicture> listImage, String baseName) {
        AmazonS3 s3Client = initializeS3Client(buildBucketName(baseName));

        for (CustomPicture image : listImage) {
			AwsS3Utils.generatePreSignedUrl(image, buildBucketName(baseName), s3Client);
		}
	}

	public static void generatePreSignedUrl(CustomPicture img, String baseName, AmazonS3 s3Client) {
		try {            
            if (s3Client == null) s3Client = initializeS3Client(buildBucketName(baseName));
    
            // Set the presigned URL to expire after one hour.
            java.util.Date expiration = new java.util.Date();
            long expTimeMillis = expiration.getTime();
            expTimeMillis += 1000 * 60 * 60;
            expiration.setTime(expTimeMillis);

			if (img.getCustompictureId() > 0) {
				// Generate the presigned URL.
	            GeneratePresignedUrlRequest generatePresignedUrlRequest = 
	                    new GeneratePresignedUrlRequest(buildBucketName(baseName), img.getPicName())
	                    .withMethod(HttpMethod.GET)
	                    .withExpiration(expiration);
	            URL url = s3Client.generatePresignedUrl(generatePresignedUrlRequest);
	            
	            img.setPicPath(url.toString());
			}
        }
        catch(AmazonServiceException e) {
            e.printStackTrace();
        }
        catch(SdkClientException e) {
            e.printStackTrace();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
	}

	public static String generatePreSignedUrl(String img, String baseName, AmazonS3 s3Client) {
		try {            
			if (s3Client == null) s3Client = initializeS3Client(buildBucketName(baseName));
			
            java.util.Date expiration = new java.util.Date();
            long expTimeMillis = expiration.getTime();
            expTimeMillis += 24000 * 60 * 60;
            expiration.setTime(expTimeMillis);

			GeneratePresignedUrlRequest generatePresignedUrlRequest = 
				new GeneratePresignedUrlRequest(buildBucketName(baseName), img)
	                .withMethod(HttpMethod.GET)
	                .withExpiration(expiration);
			URL url = s3Client.generatePresignedUrl(generatePresignedUrlRequest);

			return url.toString();
        }
        catch(AmazonServiceException e) {
            e.printStackTrace();
        }
        catch(SdkClientException e) {
            e.printStackTrace();
        }
        catch(Exception e) {
            e.printStackTrace();
		}
		
		return "";
	}
	
	public static void uploadImage(AmazonS3 s3Client ,byte[] imgBytes, String keyName, String baseName, String mime) {
		try {
			InputStream stream = new ByteArrayInputStream(imgBytes);
			
			ObjectMetadata meta = new ObjectMetadata();
			meta.setContentLength(imgBytes.length);
			
			if(mime.equalsIgnoreCase("mp4"))
				meta.setContentType("video/"+mime);	
			else
				meta.setContentType("image/"+mime);

			s3Client.putObject(new PutObjectRequest(buildBucketName(baseName), keyName, stream, meta));
			
			stream.close();
		} catch(AmazonServiceException e) {
            e.printStackTrace();
        }
        catch(SdkClientException e) {
            e.printStackTrace();
        } 
		catch (IOException e) {
			e.printStackTrace();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void uploadXLS(AmazonS3 s3Client ,byte[] data, String keyName, String baseName, String mime) {
		try {
			InputStream stream = new ByteArrayInputStream(data);
			
			ObjectMetadata meta = new ObjectMetadata() {{
				setContentLength(data.length);
				setContentType("excel/"+mime);
			}};
			
			s3Client.putObject(new PutObjectRequest(BUCKET_ADMIN, keyName, stream, meta));
			
			stream.close();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static byte[] downloadXLS(String nome) {
		if(nome != null && !nome.equals("")) {
			final AmazonS3 s3 = AwsS3Utils.initializeS3Client(BUCKET_ADMIN);
			try {
				S3Object o = s3.getObject(new GetObjectRequest(BUCKET_ADMIN, nome)); 
				S3ObjectInputStream s3is = o.getObjectContent();
				byte[] read_buf = IOUtils.toByteArray(s3is);
				
				return read_buf;
			}catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		return null;
	}
	
	public static byte[] downloadImage(String nome, String baseName) {
		if(nome!=null && !(nome.equals(""))) {
			final AmazonS3 s3 = AwsS3Utils.initializeS3Client(buildBucketName(baseName));
			try {
				S3Object o = s3.getObject(new GetObjectRequest(buildBucketName(baseName), nome));
				S3ObjectInputStream s3is = o.getObjectContent();
				byte[] read_buf = IOUtils.toByteArray(s3is);
				
				return read_buf;
				
			}catch(IOException e) {
				e.printStackTrace();
			}
			catch (AmazonServiceException e) {
				e.printStackTrace();
			}
			catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		return null;
	}
}
