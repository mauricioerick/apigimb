package br.com.gimb.ws.util;

import java.math.BigInteger;
import java.security.MessageDigest;

public class HashUtils {
	
	public static String md5FromString(String str) {
		MessageDigest m;
		try {
			m = MessageDigest.getInstance("MD5");
		    m.update(String.valueOf(str).getBytes(),0,String.valueOf(str).length());
		    return new BigInteger(1,m.digest()).toString(16);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return String.valueOf(str);
	}

}
