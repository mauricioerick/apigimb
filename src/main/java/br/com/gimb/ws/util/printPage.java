package br.com.gimb.ws.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.DocAttributeSet;
import javax.print.attribute.HashDocAttributeSet;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;

import com.itextpdf.text.DocumentException;

public class printPage {
	
	public static final String HTML = "output.html";
	public static final String PDF = "C:/temp/impressao_servico.pdf";
	
	public void print(File file) throws PrintException, FileNotFoundException {
	    // get the printer service by printer name
	    PrintService pss = PrintServiceLookup.lookupDefaultPrintService();
	    System.out.println("Printer - " + pss.getName());
	    DocPrintJob job = pss.createPrintJob();
	    DocAttributeSet das = new HashDocAttributeSet();
	    Doc document = new SimpleDoc(new FileInputStream(new File(PDF)), DocFlavor.INPUT_STREAM.AUTOSENSE, das);
	    // new htmldo
	    PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
	    job.print(document, pras);
	}
	
	public void printPDF() throws DocumentException, IOException, PrintException {
		/*String url = new File(HTML).toURI().toURL().toString();
	    FileOutputStream os = new FileOutputStream(PDF);
	    
	    Document document = new Document();
	    document.add((Element) os);
	    
	    os.close();*/
	    
	    print(null);
	}
}
