package br.com.gimb.ws.util;

import java.util.HashMap;
import java.util.Map;

import br.com.gimb.ws.enumerated.PermissionWebEnum;
import br.com.gimb.ws.model.Profile;

public class PermissionsUtil {
	
	public static String getPermissionValue(Profile profile, PermissionWebEnum permissionEnum) {
		String configWeb = profile.getConfigWeb();

		if (configWeb != null) {
			configWeb = configWeb.substring(1, configWeb.length()-1);
			String[] keyValuePairs = configWeb.split(",");
			Map<String, Object> map = new HashMap<>();
	
			for (String pair : keyValuePairs) {
			    Object[] entry = pair.split(":");
			    
			    String key = entry[0].toString().replace("\"", "");
			    String value = entry[1].toString();
			    
			    map.put(key, value);
			}

			if (map.get(permissionEnum.getDescription()) != null)
				return map.get(permissionEnum.getDescription()).toString(); 
 		}
		
		return "";
	}

}
