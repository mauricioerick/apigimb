package br.com.gimb.ws.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.common.collect.Lists;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.MulticastMessage;

import br.com.gimb.ws.model.Action;
import br.com.gimb.ws.model.Card;
import br.com.gimb.ws.model.Client;
import br.com.gimb.ws.model.CostCenter;
import br.com.gimb.ws.model.Device;
import br.com.gimb.ws.model.Event;
import br.com.gimb.ws.model.NatureExpense;
import br.com.gimb.ws.model.ObjectType;
import br.com.gimb.ws.model.Storage;
import br.com.gimb.ws.model.TypePayment;
import br.com.gimb.ws.model.User;
import br.com.gimb.ws.model.Vehicle;

public class FirebaseUtil {

    public static final List<Class> classes = Arrays.asList(Action.class, Event.class, Vehicle.class, TypePayment.class, Client.class, User.class, ObjectType.class, Storage.class, CostCenter.class, NatureExpense.class, Card.class);

    public static void sendNotificationUpdateAll(List<Device> devices, String type, String identifier){
        if(System.getenv("GOOGLE_APPLICATION_CREDENTIALS") == null ) return;

        Lists.partition(devices, 100).forEach(batch -> {
            List<String> registrationTokens = new ArrayList();
            batch.forEach(device -> {
                registrationTokens.add(device.getPushToken());
            });

            MulticastMessage message = MulticastMessage.builder()
                    .putData("action","update")
                    .putData("type",type)
                    .putData("identifier",identifier)
                    .addAllTokens(registrationTokens)
                    .build();
            try {
                FirebaseMessaging.getInstance().sendMulticast(message);
            } catch (FirebaseMessagingException e) {
                e.printStackTrace();
            }

        });

    }

}
