package br.com.gimb.ws.util;
import com.lowagie.text.DocumentException;

import org.apache.tomcat.util.codec.binary.Base64;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import br.com.gimb.ws.controller.BaseController;
import br.com.gimb.ws.model.ChecklistServices;
import br.com.gimb.ws.model.Company;
import br.com.gimb.ws.model.ServiceImage;
import br.com.gimb.ws.model.Services;
import br.com.gimb.ws.util.aws.AwsS3Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class PdfService extends BaseController {

    public File generatePdfFromHtml(String html) throws IOException, DocumentException {
        // String outputFolder = System.getProperty("user.home") + File.separator + "thymeleaf.pdf";
        File outputFolder = File.createTempFile("teste", ".pdf");
        OutputStream outputStream = new FileOutputStream(outputFolder);

        ITextRenderer renderer = new ITextRenderer();
        renderer.setDocumentFromString(html);
        renderer.layout();        
        renderer.createPDF(outputStream);        
        outputStream.close();

        return outputFolder;
    }

    public String parsePdfTemplate(Services services, Company company) {
        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode(TemplateMode.HTML);

        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);

        Context context = new Context();

        // Base64 encoder = new Base64();
		// byte[] img = AwsS3Utils.downloadImage(company.getLogoName(), baseName());        
            
        // String base64 = "data:image/jpeg;base64,"+ new String(encoder.encode(img), "UTF-8");

        //Info Company
        context.setVariable("infoCompanyImg", "teste");
        context.setVariable("infoCompanyName", company.getCompanyName());
        context.setVariable("infoDocumentCompany", company.getDocument());
        context.setVariable("infoRegistrationCompany", company.getRegistration());
        context.setVariable("infoAddressCompany", company.getAddress());
        context.setVariable("infoRZipCodeCompany", company.getZipCode());
        context.setVariable("infoCityCompany", company.getCity());
        context.setVariable("infoTelefoneCompany", company.getPhone());
        context.setVariable("infoSiteCompany", company.getSite());

        //Info OS
        context.setVariable("reportNumber", services.getServiceId());
        context.setVariable("reportDate", DateHelper.getDateFromDateTimeString(services.getEndTime()));

        //Info Client
        context.setVariable("tradingName", services.getClient().getTradingName());
        context.setVariable("companyName", services.getClient().getCompanyName());
        context.setVariable("document", services.getClient().getDocument());
        context.setVariable("address", services.getClient().getAddress());
        context.setVariable("phone", services.getClient().getPhone());

        //Info Action
        context.setVariable("actionDescription", services.getAction().getDescription());

        //Info User
        context.setVariable("userName", services.getUser().getName());

        //Info Equipment
        context.setVariable("equipmentDescription", services.getVehicle().getEquipment().getDescription());
        context.setVariable("equipmentBrand", services.getVehicle().getBrand());
        context.setVariable("equipmentModel", services.getVehicle().getModel());        
        context.setVariable("equipmentPlate", services.getVehicle().getPlate());
        context.setVariable("equipmentModelYear", services.getVehicle().getModelYear());

        //Info CustomFields
        context.setVariable("customFieldList", services.getCustomFieldList());

        //Info Images
        context.setVariable("listaImagensPrint", services.getPicsList());

        //Info Criticidade
        if (services.getAction() != null && (services.getAction().getCritically() != null && services.getAction().getCritically())) {
            
            // context.setVariable("listChecklistServices", checklistServices);
        }

        //Info ChecklistServices
        if (services.getAction() != null && (services.getAction().getUsaSistemaSubSistema() != null && services.getAction().getUsaSistemaSubSistema())) {
            List<ChecklistServices> checklistServices = new ArrayList<ChecklistServices>();

            for (ServiceImage serviceImage : services.getPicsList()) {
                if (serviceImage.getChecklistItems() != null) {     
                    ChecklistServices check = new ChecklistServices(); 
                    check = serviceImage.getChecklistItems().getChecklistServices(); 
                    
                    if (checklistServices.size() > 0) {
                        if (!checklistServices.contains(check)) {
                            checklistServices.add(check);                            
                        }
                    } else {
                        checklistServices.add(check);
                    }                  
                }
            }

            context.setVariable("listChecklistServices", checklistServices);
        }        

        //Infos actions
        context.setVariable("action", services.getAction());

        if (services.getAction() != null && (services.getAction().getUsaSistemaSubSistema() != null && services.getAction().getUsaSistemaSubSistema()))
            return templateEngine.process("templates/templateServicePrint", context);

        return templateEngine.process("templates/templateServicePrintOs", context);
        
    }
}