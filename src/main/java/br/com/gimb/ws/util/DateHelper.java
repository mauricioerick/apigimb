package br.com.gimb.ws.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateHelper {

    private static SimpleDateFormat dateformat;

    public static final String DATE_FORMAT = "dd/MM/yyyy";
    public static final String DATE_TIME_FORMAT = "dd/MM/yyyy HH:mm:ss";
    public static final String DATE_JSON_FORMAT = "yyyy-MM-dd";

    public static String dateFormat(Date date) {
        DateHelper.dateformat = new SimpleDateFormat(DateHelper.DATE_FORMAT, Locale.getDefault());
        return String.valueOf(DateHelper.dateformat.format(date));
    }

    public static String nowDateFormat() {
        DateHelper.dateformat = new SimpleDateFormat(DateHelper.DATE_FORMAT, Locale.getDefault());
        return String.valueOf(DateHelper.dateformat.format(Calendar.getInstance().getTime()));
    }

    public static String nowDateJsonFormat() {
        DateHelper.dateformat = new SimpleDateFormat(DateHelper.DATE_JSON_FORMAT, Locale.getDefault());
        return String.valueOf(DateHelper.dateformat.format(Calendar.getInstance().getTime()));
    }

    public static String nowDateTimeFormat() {
        DateHelper.dateformat = new SimpleDateFormat(DateHelper.DATE_TIME_FORMAT, Locale.getDefault());
        return String.valueOf(DateHelper.dateformat.format(Calendar.getInstance().getTime()));
    }

    public static Date stringToDate(String string) {
        try {
            DateHelper.dateformat = new SimpleDateFormat(DateHelper.DATE_TIME_FORMAT, Locale.getDefault());
            return DateHelper.dateformat.parse(string);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String dateJsonFormatToDateFormat(String string) {
        try {
            DateHelper.dateformat = new SimpleDateFormat(DateHelper.DATE_JSON_FORMAT, Locale.getDefault());
            Date date = DateHelper.dateformat.parse(string); 
            DateHelper.dateformat = new SimpleDateFormat(DateHelper.DATE_FORMAT, Locale.getDefault());
            return DateHelper.dateformat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Date stringToDate(String string, String format) {
        try {
            DateHelper.dateformat = new SimpleDateFormat(format, Locale.getDefault());
            return DateHelper.dateformat.parse(string);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
    
    public static String getDateFromDateTimeString(String date) {
    	try {
			return date.substring(0, 10);
		} catch (Exception e) {
			
		}
    	return "";
    }
    
    public static String getTimeFromDateTimeString(String date) {
    	try {
			return date.substring(11, 16);
		} catch (Exception e) {
			
		}
    	return "";
    }
    
    public static String getDayFrom(String date) {
    	return date.length() >= 10 ? date.substring(0, 2) : "";
    }
    
    public static String getMonthFrom(String date) {
    	return date.length() >= 10 ? date.substring(3, 5) : "";
    }
    
    public static String getYearFrom(String date) {
    	return date.length() >= 10 ? date.substring(6, 10) : "";
    }

}
