package br.com.gimb.ws.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URL;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;

public class FileUtils {
	
	public static File getFileFromUrl(String fileUrl, String fileName) throws Exception {
		URL url = new URL(fileUrl);
	    InputStream is = url.openStream();
	    
	    File file = new File(fileName != null ? fileName : FilenameUtils.getName(url.getPath()));
	    OutputStream os = new FileOutputStream(file);

	    byte[] b = new byte[2048];
	    int length;

	    while ((length = is.read(b)) != -1) {
	        os.write(b, 0, length);
	    }

	    is.close();
	    os.close();
		
		return file;
	}
	
	public static File createZipWithFiles(List<File> files, String zipFileName) throws Exception {
		Path zipfile = Paths.get(zipFileName);
        zipfile.toFile().delete();

        Map<String, String> env = new HashMap<>(); 
        env.put("create", "true");
        URI uri = URI.create("jar:" + zipfile.toUri());
        try (FileSystem zipfs = FileSystems.newFileSystem(uri, env)) {

        	for (File file : files) {
				
	            Path externalFile = file.toPath();
	            Path pathInZipfile = zipfs.getPath(file.getName());          

	            Files.copy(externalFile, pathInZipfile, StandardCopyOption.REPLACE_EXISTING);      
	            
	            externalFile.toFile().delete();
			}
        }

        return zipfile.toFile();
	}
	
	public static byte[] bytesFromFile(File file) throws Exception {
		return org.apache.commons.io.FileUtils.readFileToByteArray(file);
	}
	
	public static File getDirectory(String directoryName) {
		File directory = new File(directoryName);
		if (!directory.exists())
			directory.mkdirs();
		
		return directory;
	}
	
	public static String getSeparator() {
		return FileSystems.getDefault().getSeparator();
	}

}
