package br.com.gimb.ws.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import br.com.gimb.ws.enumerated.EnvironmentEnum;

public class Util {

	public static final String VERSAO_APP = "2.00.001";
	public static final String KEY_TOKEN = "pr3ul@";
	public static final String GIMB_ENV = "GIMB_ENV";

	private static final String apiSentryDSN = "https://72f790173a1e451b92589c3fabdd231a@sentry.io/1724638";
	private static final String frontSentryDSN = "https://83444dbe8fc44a31aad094dd620a1d93@sentry.io/1724636";
	private static final String frontSentryDSNDEV = "https://ab79f7baeb2f4beaae5a1d52d4a697f8@sentry.io/1724637";

	private static String role;

	public Util() {
	}

	public static String getRole() {
		return role;
	}

	public static EnvironmentEnum getEnvironment() {
		EnvironmentEnum environment = EnvironmentEnum.LCL;
		try {
			if (System.getenv(Util.GIMB_ENV) != null)
				environment = EnvironmentEnum.valueOf(System.getenv(Util.GIMB_ENV));
		} catch (Exception e) {
			environment = EnvironmentEnum.LCL;
		}
		// environment = EnvironmentEnum.PRD;
		return environment;
	}

	public static String getSentryDSN(Boolean isRequestMethod) {
		EnvironmentEnum environment = getEnvironment();

		switch (environment) {
		case LCL:
			return isRequestMethod ? frontSentryDSNDEV : "";
		default:
			return isRequestMethod ? frontSentryDSN : apiSentryDSN;
		}
	}

	public static void setRole(String _role) {
		role = _role;
	}

	public static void setDefaultLocale(String language) {
		List<String> languages = new ArrayList<>(Arrays.asList("pt", "en", "es"));
		language = languages.contains(language) ? language : "pt";

		Locale.setDefault(new Locale(language));
	}

	public static String msgBundle(String language, String message, String parameters) {
		setDefaultLocale(language);
		ResourceBundle bundle = ResourceBundle.getBundle("locale/message");
		String msg = String.format(message, bundle.getString(parameters));

		return msg;
	}

	public static String lblBundle(String language, String label, String parameters) {
		setDefaultLocale(language);
		ResourceBundle bundle = ResourceBundle.getBundle("locale/label");
		String msg = label == null ? bundle.getString(parameters) : String.format(label, bundle.getString(parameters));

		return msg;
	}
}
