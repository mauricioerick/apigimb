package br.com.gimb.ws.model;

import br.com.gimb.annotation.IgnoreUpperCase;
import br.com.gimb.ws.enumerated.ExpenseStatusEnum;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "expense")
public class Expense extends BaseModel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "expense_id")
	private long expenseId;

	@ManyToOne
	@JoinColumn(name = "client_id", foreignKey = @ForeignKey(name = "FK_EXPENSE_CLIENT"))
	private Client client;

	@Column(name = "start_date", length = 20)
	private String startDate;

	@Column(name = "end_date", length = 20)
	private String endDate;

	@Column(name = "expense_note", length = 5000)
	private String expenseNote;

	@ManyToOne
	@JoinColumn(name = "payment_client_id", foreignKey = @ForeignKey(name = "FK_EXPENSE_PAYMENT_CLIENT"))
	private Client paymentClient;

	@Column(name = "payment_expiration_date", length = 20)
	private String paymentExpirationDate;

	@Column(name = "payment_note", length = 5000)
	private String paymentNote;

	@OneToMany(mappedBy = "expense", cascade = CascadeType.MERGE)
	private List<ExpenseItem> items;

	@Column(name = "create_date", length = 20)
	private String createDate;

	@ManyToOne
	@JoinColumn(name = "create_user", foreignKey = @ForeignKey(name = "FK_EXPENSE_USER"))
	private User createUser;

	@Column(name = "hash", length = 32)
	private String hash;

	@Enumerated(EnumType.STRING)
	@Column(name = "status", length = 15)
	private ExpenseStatusEnum status;

	@IgnoreUpperCase
	@Column(name = "demonstrative_text")
	private String demonstrativeText;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "debit_note_id")
	private DebitNote debitNote;

	public String getDemonstrativeText() {
		return this.demonstrativeText;
	}

	public void setDemonstrativeText(String text) {
		this.demonstrativeText = text;
	}

	public Expense() {
		this.items = new ArrayList<ExpenseItem>();
	}

	public Expense(long expenseId, Client client, String startDate, String endDate, String expenseNote,
			Client paymentClient, String paymentExpirationDate, String paymentNote, List<ExpenseItem> items,
			String createDate, User createUser, String hash, ExpenseStatusEnum status) {
		super();
		this.expenseId = expenseId;
		this.client = client;
		this.startDate = startDate;
		this.endDate = endDate;
		this.expenseNote = expenseNote;
		this.paymentClient = paymentClient;
		this.paymentExpirationDate = paymentExpirationDate;
		this.paymentNote = paymentNote;
		this.items = items;
		this.createDate = createDate;
		this.createUser = createUser;
		this.hash = hash;
		this.status = status;
	}

	public long getExpenseId() {
		return expenseId;
	}

	public void setExpenseId(long expenseId) {
		this.expenseId = expenseId;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getExpenseNote() {
		return expenseNote;
	}

	public void setExpenseNote(String expenseNote) {
		this.expenseNote = expenseNote;
	}

	public Client getPaymentClient() {
		return paymentClient;
	}

	public void setPaymentClient(Client paymentClient) {
		this.paymentClient = paymentClient;
	}

	public String getPaymentExpirationDate() {
		return paymentExpirationDate;
	}

	public void setPaymentExpirationDate(String paymentExpirationDate) {
		this.paymentExpirationDate = paymentExpirationDate;
	}

	public String getPaymentNote() {
		return paymentNote;
	}

	public void setPaymentNote(String paymentNote) {
		this.paymentNote = paymentNote;
	}

	public List<ExpenseItem> getItems() {
		return items;
	}

	public void setItems(List<ExpenseItem> items) {
		this.items = items;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public User getCreateUser() {
		return createUser;
	}

	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public ExpenseStatusEnum getStatus() {
		return status;
	}

	public void setStatus(ExpenseStatusEnum status) {
		this.status = status;
	}

	public String getTotalValueString() {
		Double totalValue = 0.0;

		if (items != null) {
			for (ExpenseItem expenseItem : items) {
				totalValue += expenseItem.getTotalValue();
			}
		}

		return String.format("%.2f", totalValue);
	}

	public String getStatusDescription() {
		if (this.status == null)
			return "";
		return this.status.getDescription();
	}

	public Boolean isCanceled() {
		if (this.status == null)
			return false;
		return this.status.equals(ExpenseStatusEnum.CANCEL);
	}

	public Long getSequence() {
		return debitNote != null ? debitNote.getSequence(): null;
	}

	public DebitNote getDebitNote() {
		return debitNote;
	}

	public void setDebitNote(DebitNote debitNote) {
		this.debitNote = debitNote;
	}
}
