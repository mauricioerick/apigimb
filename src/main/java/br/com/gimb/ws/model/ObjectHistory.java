package br.com.gimb.ws.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import java.util.List;

@Entity
public class ObjectHistory extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "object_history_id")
    private long objectHistoryId;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "objectId")
    private ObjectInventory object;

    @ManyToOne
    @JoinColumn(name = "storage_id", foreignKey = @ForeignKey(name = "object_history_storage_FK"))
    private Storage storage;

    @Column(name = "date_time", length = 20, columnDefinition = "varchar(20)")
    private String dateTime;

    @Column(name = "location", length = 100, columnDefinition = "varchar(100)")
    private String location;

    @ManyToOne
    @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "object_history_user_FK"))
    private User user;

    @Column(name = "note")
    private String note;

    @ManyToOne
    @JoinColumn(name="service_id")
    private Services services;

    @Transient
    private List<CustomField> customFieldList;

    @Transient
    private List<CustomPicture> customPictureList;

    public long getObjectHistoryId() {
        return objectHistoryId;
    }

    public void setObjectHistoryId(long objectHistoryId) {
        this.objectHistoryId = objectHistoryId;
    }

    public ObjectInventory getObject() {
        return object;
    }

    public void setObject(ObjectInventory object) {
        this.object = object;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Storage getStorage() {
        return storage;
    }

    public void setStorage(Storage storage) {
        this.storage = storage;
    }

    public Services getServices() {
        return services;
    }

    public void setServices(Services services) {
        this.services = services;
    }

    public List<CustomField> getCustomFieldList() {
        return customFieldList;
    }

    public void setCustomFieldList(List<CustomField> customFieldList) {
        this.customFieldList = customFieldList;
    }

    public List<CustomPicture> getCustomPictureList() {
        return customPictureList;
    }

    public void setCustomPictureList(List<CustomPicture> customPictureList) {
        this.customPictureList = customPictureList;
    }
}
