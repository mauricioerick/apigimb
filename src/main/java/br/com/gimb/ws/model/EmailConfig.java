package br.com.gimb.ws.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class EmailConfig extends BaseModel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "email_config_id")
	private long emailConfigId;

	@Column(name = "sender", columnDefinition = "TEXT")
	private String sender;

	@Column(name = "request_credit", columnDefinition = "TEXT")
	private String requestCredit;

	public long getEmailConfigId() {
		return emailConfigId;
	}

	public void setEmailConfigId(long emailConfigId) {
		this.emailConfigId = emailConfigId;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getRequestCredit() {
		return requestCredit;
	}

	public void setRequestCredit(String requestCredit) {
		this.requestCredit = requestCredit;
	}

	

}
