package br.com.gimb.ws.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "settlement_cost_center")
public class SettlementCostCenter extends BaseModel {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "settlement_cost_center_id")
  private Long settlementCostCenterId;

  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "settlement_id")
  private Settlement settlement;

  @ManyToOne
  @JoinColumn(name = "cost_center_id")
  private CostCenter costCenter;

  @Column(name = "perc")
  private Double perc;

  public Long getSettlementCostCenterId() {
    return this.settlementCostCenterId;
  }

  public void setSettlementCostCenterId(Long settlementCostCenterId) {
    this.settlementCostCenterId = settlementCostCenterId;
  }

  public Settlement getSettlement() {
    return this.settlement;
  }

  public void setSettlement(Settlement settlement) {
    this.settlement = settlement;
  }

  public CostCenter getCostCenter() {
    return this.costCenter;
  }

  public void setCostCenter(CostCenter costCenter) {
    this.costCenter = costCenter;
  }

  public Double getPerc() {
    return this.perc;
  }

  public void setPerc(Double perc) {
    this.perc = perc;
  }

  @Override
  public String toString() {
    return "{" + " settlementCostCenterId='" + getSettlementCostCenterId() + "'" + ", settlement='" + getSettlement()
        + "'" + ", costCenter='" + getCostCenter() + "'" + ", perc='" + getPerc() + "'" + "}";
  }

}