package br.com.gimb.ws.model;

import br.com.gimb.annotation.IgnoreUpperCase;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "table_parameter")
public class TableParameter extends BaseModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "table_parameter_id")
    private long tableParameterId;

    @IgnoreUpperCase
    @Column(name = "table_reference")
    private String tableReference;
    
    @Column(name = "json_version")
    private Integer jsonVersion;
    
    @IgnoreUpperCase
    @Column(name = "json_structure")
    private String jsonStructure;
    
    @Column(name = "create_date", length = 20, columnDefinition = "varchar(20)")
    private String createDate;

    @ManyToOne
    @JoinColumn(name = "create_user", foreignKey = @ForeignKey(name = "FK_TABLE_PARAMETER_USER_CREATE"))
    private User createUser;
    
    @Column(name = "active")
    private Boolean active;

	public long getTableParameterId() {
		return tableParameterId;
	}

	public TableParameter setTableParameterId(long tableParameterId) {
		this.tableParameterId = tableParameterId;
		return this;
	}

	public String getTableReference() {
		return tableReference;
	}

	public TableParameter setTableReference(String tableReference) {
		this.tableReference = tableReference;
		return this;
	}

	public Integer getJsonVersion() {
		return jsonVersion;
	}

	public TableParameter setJsonVersion(Integer jsonVersion) {
		this.jsonVersion = jsonVersion;
		return this;
	}

	public String getJsonStructure() {
		return jsonStructure;
	}

	public TableParameter setJsonStructure(String jsonStructure) {
		this.jsonStructure = jsonStructure;
		return this;
	}

	public String getCreateDate() {
		return createDate;
	}

	public TableParameter setCreateDate(String createDate) {
		this.createDate = createDate;
		return this;
	}

	public User getCreateUser() {
		return createUser;
	}

	public TableParameter setCreateUser(User createUser) {
		this.createUser = createUser;
		return this;
	}

	public Boolean getActive() {
		return active;
	}

	public TableParameter setActive(Boolean active) {
		this.active = active;
		return this;
	}

}
