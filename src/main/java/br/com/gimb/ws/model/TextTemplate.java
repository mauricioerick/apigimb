package br.com.gimb.ws.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class TextTemplate extends BaseModel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "text_template_id")
	private long textTemplateId;

	@Column(name = "origin")
	private String origin;

	@Column(name = "template")
	private String template;

	public long getTextTemplateId() {
		return this.textTemplateId;
	}

	public void setTextTemplateId(long id) {
		this.textTemplateId = id;
	}

	public String getOrigin() {
		return this.origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getTemplate() {
		return this.template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}
}
