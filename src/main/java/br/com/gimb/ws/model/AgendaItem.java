package br.com.gimb.ws.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.persistence.*;

@Entity
public class AgendaItem extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "agenda_item_id")
    private Long agendaItemId;

    @Column(name = "agenda_id")
    private Long agendaId;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("agenda_id")
    @JoinColumn(name = "agenda_id", referencedColumnName = "agenda_id", insertable = false, updatable = false)
    private Agenda agenda;

    @Column(name = "client_id")
    private Long clientId;

    @Column(name = "action_id")
    private Long actionId;

    @Column(name = "vehicle_id")
    private Long vehicleId;

    @Column(name = "project_id")
    private Long projectId;

    @Column(name = "type")
    private String type;

    @Column(name = "status")
    private Integer status;

    @Column(name = "note")
    private String note;

    public AgendaItem() {
    }

    public AgendaItem(Long agendaItemId, Long agendaId, Long clientId, Long actionId, Long vehicleId,
                      Long projectId, String type, Integer status, String note) {
        this.agendaItemId = agendaItemId;
        this.agendaId = agendaId;
        this.clientId = clientId;
        this.actionId = actionId;
        this.vehicleId = vehicleId;
        this.projectId = projectId;
        this.type = type;
        this.status = status;
        this.note = note;
    }

    public Long getAgendaItemId() {
        return agendaItemId;
    }

    public void setAgendaItemId(Long agendaItemId) {
        this.agendaItemId = agendaItemId;
    }

    public Long getAgendaId() {
        return agendaId;
    }

    public void setAgendaId(Long agendaId) {
        this.agendaId = agendaId;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Long getActionId() {
        return actionId;
    }

    public void setActionId(Long actionId) {
        this.actionId = actionId;
    }

    public Long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @JsonIgnore
    public Agenda getAgenda() {
        return agenda;
    }

    @JsonSetter
    public void setAgenda(Agenda agenda) {
        this.agenda = agenda;
    }
}
