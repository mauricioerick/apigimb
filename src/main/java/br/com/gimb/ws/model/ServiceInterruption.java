package br.com.gimb.ws.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="service_interruption")
public class ServiceInterruption extends BaseModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="service_interruption_id")
	private Long serviceInterruptionID;

	@JsonIgnore
    @ManyToOne
	@JoinColumn(name="service_id")
	private Services service;
    
	@Column(name="start_time")
    private String startTime;

    @Column(name="latitude_start_time")
    private Double latStartTime;

    @Column(name="longitude_start_time")
    private Double lgtStartTime;
    
    @Column(name="end_time")
    private String endTime;

    @Column(name="latitude_end_time")
    private Double latEndTime;

    @Column(name="longitude_end_time")
    private Double lgtEndTime;

	public ServiceInterruption() {

	}

	public ServiceInterruption(Long serviceInterruptionID, Services service, String startTime, Double latStartTime,
			Double lgtStartTime, String endTime, Double latEndTime, Double lgtEndTime) {
		this.serviceInterruptionID = serviceInterruptionID;
		this.service = service;
		this.startTime = startTime;
		this.latStartTime = latStartTime;
		this.lgtStartTime = lgtStartTime;
		this.endTime = endTime;
		this.latEndTime = latEndTime;
		this.lgtEndTime = lgtEndTime;
	}

	public Long getServiceInterruptionID() {
		return serviceInterruptionID;
	}

	public void setServiceInterruptionID(Long serviceInterruptionID) {
		this.serviceInterruptionID = serviceInterruptionID;
	}

	public Services getService() {
		return service;
	}

	public void setService(Services service) {
		this.service = service;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public Double getLatStartTime() {
		return latStartTime;
	}

	public void setLatStartTime(Double latStartTime) {
		this.latStartTime = latStartTime;
	}

	public Double getLgtStartTime() {
		return lgtStartTime;
	}

	public void setLgtStartTime(Double lgtStartTime) {
		this.lgtStartTime = lgtStartTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Double getLatEndTime() {
		return latEndTime;
	}

	public void setLatEndTime(Double latEndTime) {
		this.latEndTime = latEndTime;
	}

	public Double getLgtEndTime() {
		return lgtEndTime;
	}

	public void setLgtEndTime(Double lgtEndTime) {
		this.lgtEndTime = lgtEndTime;
	}

}
