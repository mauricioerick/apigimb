package br.com.gimb.ws.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;


@Entity
public class Card extends TableParameterBaseModel {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "card_id")
	private long cardId;
	
	@Column(name = "card_number")
	private String cardNumber;
	
	@Column(name = "card_banner")
	private String cardBanner;
	
	@Column(name = "emitting_bank")
	private String emittingBank;
	
	@Column(name = "invoice_close_date")
	private int invoiceCloseDate;
	
	@Column(name = "invoice_due_date")
	private int invoiceDueDate;
	
	@Column(name = "blocks_launch")
	private Boolean blocksLaunch;
	
	@Column(name = "automatic_renovation")
	private Boolean automaticRenovation;
	
	@Column(name = "renewal_day")
	private int renewalDay;
	
	@Column(name = "color_id")
	private String colorId;
	
	@Column(name = "active")
	private Boolean active;
	
	@Column(name="credict")
	private Double credict;
	
	@Column(name="credit_limit")
	private Double limit;

	@Column(name="renovated")
	private Boolean renovated;
	
	@JsonIgnore
	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "user_id")
	private User user;

	@Transient
	private String userName = this.user != null? this.user.getName() : "";

	public long getCardId() {
		return cardId;
	}

	public void setCardId(long cardId) {
		this.cardId = cardId;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getCardBanner() {
		return cardBanner;
	}

	public void setCardBanner(String cardBanner) {
		this.cardBanner = cardBanner;
	}

	public String getEmittingBank() {
		return emittingBank;
	}

	public void setEmittingBank(String emittingBank) {
		this.emittingBank = emittingBank;
	}

	public int getInvoiceCloseDate() {
		return invoiceCloseDate;
	}

	public void setInvoiceCloseDate(int invoiceCloseDate) {
		this.invoiceCloseDate = invoiceCloseDate;
	}

	public int getInvoiceDueDate() {
		return invoiceDueDate;
	}

	public void setInvoiceDueDate(int invoiceDueDate) {
		this.invoiceDueDate = invoiceDueDate;
	}

	public Boolean getBlocksLaunch() {
		return blocksLaunch;
	}

	public void setBlocksLaunch(Boolean blocksLaunch) {
		this.blocksLaunch = blocksLaunch;
	}

	public Boolean getAutomaticRenovation() {
		return automaticRenovation;
	}

	public void setAutomaticRenovation(Boolean automaticRenovation) {
		this.automaticRenovation = automaticRenovation;
	}

	public int getRenewalDay() {
		return renewalDay;
	}

	public void setRenewalDay(int renewalDay) {
		this.renewalDay = renewalDay;
	}

	public String getColorId() {
		return colorId;
	}

	public void setColorId(String colorId) {
		this.colorId = colorId;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Double getCredict() {
		return credict;
	}

	public void setCredict(Double credict) {
		this.credict = credict;
	}

	public Double getLimit() {
		return limit;
	}

	public void setLimit(Double limit) {
		this.limit = limit;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
		this.userName = user != null? user.getName() : "";
	}

	public String getUserName() {
		userName = this.user != null ? this.user.getName() : "";
		return userName;
	}

	public Boolean getRenovated() {
		return renovated;
	}

	public void setRenovated(Boolean renovated) {
		this.renovated = renovated;
	}
	
}
