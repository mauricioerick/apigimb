package br.com.gimb.ws.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import br.com.gimb.ws.enumerated.ClassesEnum;

@Entity
public class Imports extends BaseModel {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "imports_id")
	private long importsId;
	
	@Column(name = "name_class")
	private ClassesEnum nameClass;
	
	@Column(name = "file_path")
	private String filePath;
	
	@Column(name = "mime")
	private String mime;

	public long getImportsId() {
		return importsId;
	}

	public void setImportsId(long importsId) {
		this.importsId = importsId;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getMime() {
		return mime;
	}

	public void setMime(String mime) {
		this.mime = mime;
	}

	public ClassesEnum getNameClass() {
		return nameClass;
	}

	public void setNameClass(ClassesEnum nameClass) {
		this.nameClass = nameClass;
	}
	
	
}
