package br.com.gimb.ws.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.gimb.ws.enumerated.ChecklistStatusEnum;

@Entity
@Table(name = "checklist_items")
public class ChecklistItems extends BaseModel {
    public ChecklistItems() {
		
	}
    
    @Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "item_id")
	private long itemId;
	
	@Column(name = "item_name")
	private String itemName;

	@Column(name = "active", columnDefinition = "bit default b'1'")
    private Boolean active;
    
    @Column(name = "note")
    private String note;
    
    @Enumerated
	@Column(name = "status")
	private ChecklistStatusEnum status;

	@JoinColumn(name = "checklist_services_id", foreignKey = @ForeignKey(name  = "checklist_services_id"))
	private long checklistServicesId;
    
    @Column(name = "CHKEXP", columnDefinition = "bit default b'1'")
    private Boolean CHKEXP;

    @Column(name = "CHKOBGT", columnDefinition = "bit default b'1'")
    private Boolean CHKOBGT;

    @Column(name = "CHKGAL", columnDefinition = "bit default b'1'")
	private Boolean CHKGAL;

	@Column(name="required_photos")
	private String requiredPhotos;

	@Transient
	private ChecklistServices checklistServices;

    public long getItemId() {
		return itemId;
	}

	public void setItemId(long itemId) {
		this.itemId = itemId;
    }
    
    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
    }
    
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public ChecklistStatusEnum getStatus() {
		return status;
	}

	public void setStatus(ChecklistStatusEnum status) {
		this.status = status;
    }

    public long getChecklistServicesId() {
		return checklistServicesId;
	}

	public void setChecklistServicesId(long checklistServicesId) {
		this.checklistServicesId = checklistServicesId;
	}
    
    public Boolean getChkObgt() {
		return CHKOBGT;
	}

	public void setChkObgt(Boolean CHKOBGT) {
		this.CHKOBGT = CHKOBGT;
    }

    public Boolean getChkGal() {
		return CHKGAL;
	}

	public void setChkGal(Boolean CHKGAL) {
		this.CHKGAL = CHKGAL;
    }

    public Boolean getChkExp() {
		return CHKEXP;
	}

	public void setChkExp(Boolean CHKEXP) {
		this.CHKEXP = CHKEXP;
	}

	public String getRequiredPhotos() {
		return requiredPhotos;
	}

	public void setRequiredPhotos(String requiredPhotos) {
		this.requiredPhotos = requiredPhotos;
	}

	public ChecklistServices getChecklistServices() {
		return checklistServices;
	}

	public void setChecklistServices(ChecklistServices checklistServices) {
		this.checklistServices = checklistServices;
	}
}
