package br.com.gimb.ws.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Extract extends BaseModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "extract_id")
    private long extractId;

    @Column(name = "release_date", length = 20, columnDefinition = "varchar(20)")
    private String releaseDate;

    @Column(name = "type", length = 1, columnDefinition = "varchar(1)")
    private String type;

    @Column(name = "amount", columnDefinition = "double")
    private Double amount;

    @Column(name = "amount_approved", columnDefinition = "double")
    private Double amountApproved;

    @ManyToOne
    @JoinColumn(name = "settlement_id", foreignKey = @ForeignKey(name = "FK_EXTRACT_SETTLEMENT"))
    private Settlement settlement;

    @ManyToOne
    @JoinColumn(name = "payment_type_id", foreignKey = @ForeignKey(name = "FK_EXTRACT_PAYMENT_TYPE"))
    private TypePayment typePayment;

    @Column(name = "status", length = 1, columnDefinition = "varchar(1)")
    private String status;

    @Column(name = "create_date", length = 20, columnDefinition = "varchar(20)")
    private String createDate;

    @ManyToOne
    @JoinColumn(name = "create_user", foreignKey = @ForeignKey(name = "FK_EXTRACT_CREATE_USER"))
    private User createUser;

    @Column(name = "approve_date", length = 20, columnDefinition = "varchar(20)")
    private String approveDate;

    @ManyToOne
    @JoinColumn(name = "approve_user", foreignKey = @ForeignKey(name = "FK_EXTRACT_APPROVE_USER"))
    private User approveUser;

    @ManyToOne
    @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "FK_EXTRACT_USER"))
    private User user;

    @Column(name = "info", columnDefinition = "text")
    private String info;

    @Column(name = "note", columnDefinition = "text")
    private String note;

    @ManyToOne
    @JoinColumn(name = "card_id")
    private Card card;

    public Card getCard() {
        return this.card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public Double getAmountApproved() {
        return this.amountApproved;
    }

    public Extract setAmountApproved(Double amountApproved) {
        this.amountApproved = amountApproved;
        return this;
    }

    public static Extract builder() {
        return new Extract();
    };

    public long getExtractId() {
        return extractId;
    }

    public Extract setExtractId(long extractId) {
        this.extractId = extractId;
        return this;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public Extract setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
        return this;
    }

    public String getType() {
        return type;
    }

    public Extract setType(String type) {
        this.type = type;
        return this;
    }

    public Double getAmount() {
        return amount;
    }

    public Extract setAmount(Double amount) {
        this.amount = amount;
        return this;
    }

    public Settlement getSettlement() {
        return settlement;
    }

    public Extract setSettlement(Settlement settlement) {
        this.settlement = settlement;
        return this;
    }

    public TypePayment getTypePayment() {
        return typePayment;
    }

    public Extract setTypePayment(TypePayment typePayment) {
        this.typePayment = typePayment;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public Extract setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getCreateDate() {
        return createDate;
    }

    public Extract setCreateDate(String createDate) {
        this.createDate = createDate;
        return this;
    }

    public User getCreateUser() {
        return createUser;
    }

    public Extract setCreateUser(User createUser) {
        this.createUser = createUser;
        return this;
    }

    public String getApproveDate() {
        return approveDate;
    }

    public Extract setApproveDate(String approveDate) {
        this.approveDate = approveDate;
        return this;
    }

    public User getApproveUser() {
        return approveUser;
    }

    public Extract setApproveUser(User approveUser) {
        this.approveUser = approveUser;
        return this;
    }

    public User getUser() {
        return user;
    }

    public Extract setUser(User user) {
        this.user = user;
        return this;
    }

    public String getInfo() {
        return info;
    }

    public Extract setInfo(String info) {
        this.info = info;
        return this;
    }

    public String getNote() {
        return note;
    }

    public Extract setNote(String note) {
        this.note = note;
        return this;
    }

}
