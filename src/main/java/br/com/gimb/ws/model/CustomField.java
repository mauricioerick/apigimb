package br.com.gimb.ws.model;

import br.com.gimb.ws.enumerated.ReferenceTypeEnum;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="custom_field")
public class CustomField extends BaseModel  {

	public CustomField() {
	}

    @Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "custom_field_id")
    private Integer customFieldId;

    @Enumerated(EnumType.STRING)
    @Column(name = "reference_type")
    private ReferenceTypeEnum referenceType;

    @Column(name = "reference_id")
    private String referenceId;

    @Column(name = "custom_field")
    private String customField;

    @Column(name = "custom_field_value")
    private String customFieldValue;

	public Integer getCustomFieldId() {
		return customFieldId;
	}

	public void setCustomFieldId(Integer customFieldId) {
		this.customFieldId = customFieldId;
	}

	public ReferenceTypeEnum getReferenceType() {
		return referenceType;
	}

	public void setReferenceType(ReferenceTypeEnum referenceType) {
		this.referenceType = referenceType;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getCustomField() {
		return customField;
	}

	public void setCustomField(String customField) {
		this.customField = customField;
	}

	public String getCustomFieldValue() {
		return customFieldValue;
	}

	public void setCustomFieldValue(String customFieldValue) {
		this.customFieldValue = customFieldValue;
	}
	
}
