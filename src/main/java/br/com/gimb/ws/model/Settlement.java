package br.com.gimb.ws.model;

import br.com.gimb.ws.enumerated.PaymentTypeEnum;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import java.util.ArrayList;
import java.util.List;

@Entity
public class Settlement extends BaseModel {

	public Settlement() {

	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "settlement_id")
	private long settlementId;

	@ManyToOne
	@JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "FK_SETTLEMENT_USER"))
	private User user;

	@ManyToOne
	@JoinColumn(name = "client_id", foreignKey = @ForeignKey(name = "FK_SETTLEMENT_CLIENT"))
	private Client client;

	@ManyToOne
	@JoinColumn(name = "event_id", foreignKey = @ForeignKey(name = "FK_SETTLEMENT_EVENT"))
	private Event event;

	@ManyToOne
	@JoinColumn(name = "type_payment_id", foreignKey = @ForeignKey(name = "FK_SETTLEMENT_TYPE_PAYMENT"))
	private TypePayment typePayment;

	@Enumerated(EnumType.STRING)
	@Column(name = "payment_type")
	private PaymentTypeEnum paymentType;

	@Column(name = "payment_id")
	private String paymentId;

	@Column(name = "date")
	private String date;

	@Column(name = "start_time")
	private String startTime;

	@Column(name = "amount")
	private Double amount;

	@Column(name = "note")
	private String note;

	@Column(name = "latitude")
	private Double latitude;

	@Column(name = "longitude")
	private Double longitude;

	@Column(name = "status")
	private String status;

	@OneToMany(mappedBy = "settlement")
	private List<SettlementCostCenter> costCenterList;

	@ManyToOne
	@JoinColumn(name = "nature_expense_id")
	private NatureExpense natureExpense;

	@ManyToOne
	@JoinColumn(name = "card_id")
	private Card card;

	@Transient
	private List<CustomField> customFieldList;

	@Transient
	private List<CustomPicture> customPictureList;

	@Transient
	private Long settlementIdOnLine;

	@Transient
	private Extract extract;

	public Card getCard() {
		return this.card;
	}

	public void setCard(Card card) {
		this.card = card;
	}

	public List<SettlementCostCenter> getCostCenterList() {
		return this.costCenterList;
	}

	public void setCostCenterList(List<SettlementCostCenter> costCenterList) {
		this.costCenterList = costCenterList;
	}

	public long getSettlementId() {
		return settlementId;
	}

	public void setSettlementId(long settlementId) {
		this.settlementId = settlementId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public TypePayment getTypePayment() {
		return typePayment;
	}

	public void setTypePayment(TypePayment typePayment) {
		this.typePayment = typePayment;
	}

	public PaymentTypeEnum getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(PaymentTypeEnum paymentType) {
		this.paymentType = paymentType;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public NatureExpense getNatureExpense() {
		return natureExpense;
	}

	public void setNatureExpense(NatureExpense natureExpense) {
		this.natureExpense = natureExpense;
	}

	public List<CustomField> getCustomFieldList() {
		if (customFieldList == null) {
			customFieldList = new ArrayList<CustomField>();
		}
		return customFieldList;
	}

	public void setCustomFieldList(List<CustomField> customFieldList) {
		this.customFieldList = customFieldList;
	}

	public List<CustomPicture> getCustomPictureList() {
		return customPictureList;
	}

	public void setCustomPictureList(List<CustomPicture> customPictureList) {
		this.customPictureList = customPictureList;
	}

	public Long getSettlementIdOnLine() {
		return settlementIdOnLine;
	}

	public void setSettlementIdOnLine(Long settlementIdOnLine) {
		this.settlementIdOnLine = settlementIdOnLine;
	}

	public Extract getExtract() {
		return extract;
	}

	public void setExtract(Extract extract) {
		this.extract = extract;
	}

	/* Custom gets to use in view */

	public String getAmountFormatted() {
		return String.format("%.2f", amount);
	}

	public String getDateYYYYMMDD() {
		String result = date;
		if (date.length() >= 10)
			result = date.substring(6, 10) + date.substring(3, 5) + date.substring(0, 2);

		return result;
	}

	public String getInfo() {
		StringBuilder str = new StringBuilder();

		if (note != null)
			str.append(String.format("%s\n", note));

		return str.toString();
	}

	// public static class CostCenterAux {
	// private Long costCenterId;
	// private Double perc;

	// public CostCenterAux() {
	// }

	// public CostCenterAux(Long costCenterId, Double perc) {
	// this.costCenterId = costCenterId;
	// this.perc = perc;
	// }

	// public void setCostCenterId(Long costCenterId) {
	// this.costCenterId = costCenterId;
	// }

	// public Long getCostCenterId() {
	// return this.costCenterId;
	// }

	// public void setPerc(Double perc) {
	// this.perc = perc;
	// }

	// public Double getPerc() {
	// return this.perc;
	// }
	// }
}
