package br.com.gimb.ws.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;


@Entity
public class Vehicle extends BaseModel {

	public Vehicle() {
		// TODO Auto-generated constructor stub
	}
	
	public Vehicle(long vehicleId) {
		this.vehicleId = vehicleId;
	}
	
	public Vehicle(Client client, Equipment equipment, String brand, String model, int fabricationYear, int modelYear, String plate) {
		this.client = client;
		this.equipment = equipment;
		this.brand = brand;
		this.model = model;
		this.fabricationYear = fabricationYear;
		this.modelYear = modelYear;
		this.plate = plate;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "vehicleId")
	private long vehicleId;
	
	@JsonIgnore
	@OneToOne
	private Client client;
	
	@OneToOne
	private Equipment equipment;
	
	@Column(name = "brand")
	private String brand;
	
	@Column(name = "model")
	private String model;
	
	@Column(name = "fabrication_year")
	private int fabricationYear;
	
	@Column(name = "model_year")
	private int modelYear;
	
	@Column(name = "plate")
	private String plate;
	
	public long getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(long vehicleId) {
		this.vehicleId = vehicleId;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Equipment getEquipment() {
		return equipment;
	}

	public void setEquipment(Equipment equipment) {
		this.equipment = equipment;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getFabricationYear() {
		return fabricationYear;
	}

	public void setFabricationYear(int fabricationYear) {
		this.fabricationYear = fabricationYear;
	}

	public int getModelYear() {
		return modelYear;
	}

	public void setModelYear(int modelYear) {
		this.modelYear = modelYear;
	}

	public String getPlate() {
		return plate;
	}

	public void setPlate(String plate) {
		this.plate = plate;
	}

}
