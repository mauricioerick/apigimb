package br.com.gimb.ws.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Trace extends BaseModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "trace_id")
	private Integer traceId;

	@ManyToOne
	@JoinColumn (name  = "user_id", foreignKey = @ForeignKey(name  = "FK_TRACE_USER")) 
	private User user;

	@Column(name = "startTime", length=20, columnDefinition="varchar(20)")
	private String startTime;

	@Column(name = "latitude")
    private Double latitude;

	@Column(name = "longitude")
    private Double longitude;

	@Column(name = "status", length=1, columnDefinition="char(1)")
	private String status;
	
	public Trace() {
	
	}

	public Trace(Integer traceId, User user, String startTime, Double latitude, Double longitude, String status) {
		super();
		this.traceId = traceId;
		this.user = user;
		this.startTime = startTime;
		this.latitude = latitude;
		this.longitude = longitude;
		this.status = status;
	}

	public Integer getTraceId() {
		return traceId;
	}

	public void setTraceId(Integer traceId) {
		this.traceId = traceId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
