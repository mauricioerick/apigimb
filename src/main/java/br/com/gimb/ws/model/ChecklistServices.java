package br.com.gimb.ws.model;

import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "checklist_services")
public class ChecklistServices extends BaseModel {
	public ChecklistServices() {
		
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "checklist_services_id")
	private long checklistId;
	
	@Column(name = "checklist_name")
	private String checklistName;

	@Column(name = "active", columnDefinition = "bit default b'1'")
	private Boolean active;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "checklist_main_id", referencedColumnName = "checklist_services_id")
	private ChecklistServices checklistMainId;

	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "service_id", foreignKey = @ForeignKey(name  = "service_id"))
	private Services services;

	@JoinColumn(name = "equipment_equipment_id", foreignKey = @ForeignKey(name  = "equipment_id"))
	private long equipmentEquipmentId;

	@OneToOne(fetch = FetchType.EAGER, mappedBy = "checklist_items", optional = true)	
	private transient List<ChecklistItems> checklistItems;

	@OneToOne(fetch = FetchType.EAGER, mappedBy = "equipment", optional = true)	
	private transient Equipment equipment;

	public long getChecklistId() {
		return checklistId;
	}

	public void setChecklistId(long checklistId) {
		this.checklistId = checklistId;
	}

	public String getChecklistName() {
		return checklistName;
	}

	public void setChecklistName(String checklistName) {
		this.checklistName = checklistName;
	}

	public ChecklistServices getChecklistMainId() {
		return checklistMainId;
	}

	public void setChecklistMainId(ChecklistServices checklistMainId) {
		this.checklistMainId = checklistMainId;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Services getServices() {
		return services;
	}

	public void setServices(Services services) {
		this.services = services;
	}

	public long getEquipmentEquipmentId() {
		return equipmentEquipmentId;
	}

	public void setEquipmentEquipmentId(long equipmentEquipmentId) {
		this.equipmentEquipmentId = equipmentEquipmentId;
	}

	public List<ChecklistItems> getChecklistItems() {
		return checklistItems;
	}

	public void setChecklistItems(List<ChecklistItems> checklistItems) {
		this.checklistItems = checklistItems;
	}

	public Equipment getEquipment() {
		return equipment;
	}

	public void setEquipment(Equipment equipment) {
		this.equipment = equipment;
	}
}
