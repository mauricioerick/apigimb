package br.com.gimb.ws.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class DebitNote extends BaseModel {
	public DebitNote() {
		
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "debit_note_id")
	private long debitNoteId;

	@Column(name = "series")
	private String series;

	@Column(name = "sequence")
	private long sequence;


	public long getDebitNoteId() {
		return debitNoteId;
	}

	public void setDebitNoteId(long debitNoteId) {
		this.debitNoteId = debitNoteId;
	}

	public String getSeries() {
		return series;
	}

	public void setSeries(String description) {
		this.series = description;
	}

	public long getSequence() {
		return sequence;
	}

	public void setSequence(long sequence) {
		this.sequence = sequence;
	}
	
	public String getSequenceInString() {
		return Long.toString(sequence);
	}
}
