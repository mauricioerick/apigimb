package br.com.gimb.ws.model;

import br.com.gimb.ws.util.DateHelper;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity
public class Balance extends BaseModel {
	
    @Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "balance_id")
    private long balanceId;

    @Column(name = "day", length = 20, columnDefinition = "varchar(20)")
    private String day;
    
    @Column(name = "month", length = 20, columnDefinition = "varchar(20)")
    private String month;

    @Column(name = "year", length = 20, columnDefinition = "varchar(20)")
    private String year;

    @Column(name = "balance", columnDefinition = "double")
    private Double balance;
    
    @ManyToOne
	@JoinColumn (name  = "user_id", foreignKey = @ForeignKey(name  = "FK_EXTRACT_USER"))
    private User user;
    
    @ManyToOne
	@JoinColumn (name  = "payment_type_id", foreignKey = @ForeignKey(name  = "FK_EXTRACT_PAYMENT_TYPE"))
	private TypePayment typePayment;

    @ManyToOne
	@JoinColumn (name  = "card_id", foreignKey = @ForeignKey(name  = "FK_EXTRACT_CARD"))
    private Card card;

	public static Balance builder() {
    	return new Balance();
    }

	public long getBalanceId() {
		return balanceId;
	}

	public Balance setBalanceId(long balanceId) {
		this.balanceId = balanceId;
		return this;
	}

	public String getDay() {
		return day;
	}

	public Balance setDay(String day) {
		this.day = day;
		return this;
	}

	public String getMonth() {
		return month;
	}

	public Balance setMonth(String month) {
		this.month = month;
		return this;
	}

	public String getYear() {
		return year;
	}

	public Balance setYear(String year) {
		this.year = year;
		return this;
	}
	
	public String getDate() {
		return String.format("%s/%s/%s", day, month, year);
	}

	public Double getBalance() {
		return balance;
	}

	public Balance setBalance(Double balance) {
		this.balance = balance;
		return this;
	}

	public User getUser() {
		return user;
	}
	
	public Long getUserId() {
		return user == null ? 0 : user.getUserId();
	}

	public Balance setUser(User user) {
		this.user = user;
		return this;
	}

	public TypePayment getTypePayment() {
		return typePayment;
	}
	
	public Long getTypePaymentId() {
		return typePayment == null ? 0 : typePayment.getTypePaymentId();
	}

	public Balance setTypePayment(TypePayment typePayment) {
		this.typePayment = typePayment;
		return this;
	}

	public Card getCard() {
		return card;
	}

	public Balance setCard(Card card) {
		this.card = card;
		return this;
	}

	public int compareToByUserAndTypePaymentAndDate(Balance other) {
		int result = getUserId().compareTo(other.getUserId());
		if (result == 0) 
			result = getTypePaymentId().compareTo(other.getTypePaymentId());
			
		if (result == 0) {
			Date d1 = DateHelper.stringToDate(getDate());
			Date d2 = DateHelper.stringToDate(other.getDate());
			result = d1.compareTo(d2);
		}
		
		return result;
	}
    
    
}
