package br.com.gimb.ws.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.com.gimb.ws.enumerated.TypeDurationEnum;

@Entity
public class Project extends BaseModel {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "project_id")
	private Long projectId;
	
	@Column(name = "project_name")
	private String projectName;
	
	@Column(name = "project_time")
	private int projectTime;
	
	@Column(name = "active", columnDefinition = "bit default b'1'")
	private Boolean active;
	
	@Column(name = "equipment")
	private String equipment;
	
	@Column(name="initial_date")
	private String initialDate;

	@Column(name="project_available_days")
	private Integer projectAvailableDays;
	
	@Enumerated(EnumType.STRING)
	@Column(name="duration_type")
	private TypeDurationEnum durationType;
	
	@ManyToOne
	@JoinColumn(name = "client", foreignKey = @ForeignKey(name = "client"))
	private Client client;
	
	@Column(name="final_date")
	private String endDate;
	
	@ManyToOne
	@JoinColumn(name="area_id", foreignKey = @ForeignKey(name = "area_id"))
	private Area area;
	
	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public int getProjectTime() {
		return projectTime;
	}

	public void setProjectTime(int projectTime) {
		this.projectTime = projectTime;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getEquipment() {
		return equipment;
	}

	public void setEquipment(String equipment) {
		this.equipment = equipment;
	}

	public String getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(String initialDate) {
		this.initialDate = initialDate;
	}

	public TypeDurationEnum getDurationType() {
		return durationType;
	}

	public void setDurationType(TypeDurationEnum durationType) {
		this.durationType = durationType;
	}

	public Integer getProjectAvailableDays() {
		return projectAvailableDays;
	}

	public void setProjectAvailableDays(Integer projectAvailableDays) {
		this.projectAvailableDays = projectAvailableDays;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}
	
}
