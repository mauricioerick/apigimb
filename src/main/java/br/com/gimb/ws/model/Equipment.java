package br.com.gimb.ws.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import java.util.List;

@Entity
public class Equipment extends BaseModel {

	public Equipment() {

	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "equipmentId")
	private long equipmentId;

	@Column(name = "description", length = 80, columnDefinition = "varchar(80)")
	private String description;

	@Column(name = "messages", length = 5000)
	private String images;

	@Column(name = "estimate_time")
	private Integer estimateTime;

	@Column(name = "checklist", length = 5000)
	private String checklist;

	@ManyToMany
	@JoinTable(name = "equipment_object_type", joinColumns = @JoinColumn(name = "equipment_id"), inverseJoinColumns = @JoinColumn(name = "object_type_id"))
	private List<ObjectType> objectTypes;

	@Column(name = "color_id")
	private String colorId;

	@Column(name = "active", columnDefinition = "bit default b'1'")
	private Boolean active;

	@OneToOne(fetch = FetchType.EAGER, mappedBy = "equipment", optional = true)	
	private transient List<ChecklistServices> checklistsServices;

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public long getEquipmentId() {
		return equipmentId;
	}

	public void setEquipmentId(long equipmentId) {
		this.equipmentId = equipmentId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}

	public Integer getEstimateTime() {
		return estimateTime;
	}

	public void setEstimateTime(Integer estimateTime) {
		this.estimateTime = estimateTime;
	}

	public String getChecklist() {
		return checklist;
	}

	public void setChecklist(String checklist) {
		this.checklist = checklist;
	}

	public List<ObjectType> getObjectTypes() {
		return objectTypes;
	}

	public void setObjectTypes(List<ObjectType> objectTypes) {
		this.objectTypes = objectTypes;
	}

	public String getColorId() {
		return colorId;
	}

	public void setColorId(String colorId) {
		this.colorId = colorId;
	}

	public List<ChecklistServices> getChecklistsServices() {
		return checklistsServices;
	}

	public void setChecklistsServices(List<ChecklistServices> checklistsServices) {
		this.checklistsServices = checklistsServices;
	}

}
