package br.com.gimb.ws.model;

import br.com.gimb.ws.enumerated.PaymentIdentificationEnum;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class TypePayment extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "type_payment_id")
    private long typePaymentId;

    @Column(name = "description", length = 255, columnDefinition = "varchar(255)")
    private String description;

    @Column(name = "consume_balance", columnDefinition = "bit default b'0'")
    private Boolean consumeBalance;

    @Column(name = "payment_identifier", columnDefinition = "bit default b'0'")
    private Boolean paymentIdentifier;

    @Column(name = "mask", length = 255, columnDefinition = "varchar(255)")
    private String mask;

    @Column(name = "active", columnDefinition = "bit default b'1'")
    private Boolean active;

    @Column(name = "color_id", length = 100, columnDefinition = "varchar(100)")
    private String colorId;

    @Column(name = "images", length = 5000)
    private String images;

    @Column(name = "fields", columnDefinition = "TEXT")
    private String fields;

    @Column(name = "cost_center_origin")
    private String costCenterOrigin;

    @Enumerated(EnumType.STRING)
    @Column(name = "identification_type")
    private PaymentIdentificationEnum identificationType;

    public String getCostCenterOrigin() {
        return this.costCenterOrigin;
    }

    public void setCostCenterOrigin(String costCenterOrigin) {
        this.costCenterOrigin = costCenterOrigin;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getFields() {
        return fields;
    }

    public void setFields(String fields) {
        this.fields = fields;
    }

    public long getTypePaymentId() {
        return typePaymentId;
    }

    public void setTypePaymentId(long typePaymentId) {
        this.typePaymentId = typePaymentId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getConsumeBalance() {
        return consumeBalance == null ? false : consumeBalance;
    }

    public void setConsumeBalance(Boolean consumeBalance) {
        this.consumeBalance = consumeBalance == null ? false : consumeBalance;
    }

    public Boolean getPaymentIdentifier() {
        return paymentIdentifier;
    }

    public void setPaymentIdentifier(Boolean paymentIdentifier) {
        this.paymentIdentifier = paymentIdentifier;
    }

    public String getMask() {
        return mask;
    }

    public void setMask(String mask) {
        this.mask = mask;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getColorId() {
        return colorId;
    }

    public void setColorId(String colorId) {
        this.colorId = colorId;
    }

    public PaymentIdentificationEnum getIdentificationType() {
        return identificationType;
    }

    public void setIdentificationType(PaymentIdentificationEnum identificationType) {
        this.identificationType = identificationType;
    }
}
