package br.com.gimb.ws.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import org.springframework.context.annotation.Primary;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.*;

@Entity
public class Agenda extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "agenda_id")
    private Long agendaId;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "date", columnDefinition = "DATE")
    @JsonFormat(pattern = "dd/MM/yyyy")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate date;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "agendaId")
    private List<AgendaItem> agendaItems = new ArrayList<>();

    public Agenda() {
    }

    public Agenda(Long agendaId, Long userId, LocalDate date) {
        this.agendaId = agendaId;
        this.userId = userId;
        this.date = date;
    }

    public Long getAgendaId() {
        return agendaId;
    }

    public void setAgendaId(Long agendaId) {
        this.agendaId = agendaId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @JsonIgnore
    public List<AgendaItem> getAgendaItems() {
        return agendaItems;
    }

    @JsonSetter
    public void setAgendaItems(List<AgendaItem> agendaItems) {
        this.agendaItems = agendaItems;
    }
}
