package br.com.gimb.ws.model;

import br.com.gimb.annotation.IgnoreUpperCase;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class Company extends BaseModel  {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "company_id")
	private long companyId;
	
	@Column(name = "document", length=20)
	private String document;
	
	@Column(name = "registration", length=20)
	private String registration;
	
	@Column(name = "company_name", length=80)
	private String companyName;
	
	@Column(name = "trading_name", length=60)
	private String tradingName;
	
	@Column(name = "address", length=200)
	private String address;
	
	@Column(name = "zip_code", length=200)
	private String zipCode;
	
	@Column(name = "city", length=200)
	private String city;
	
	@Column(name = "phone", length=45)
	private String phone;
	
	@IgnoreUpperCase
	@Column(name = "site", length=45)
	private String site;

	@Column(name = "logo_name", length=255)
	private String logoName;
	
	@Transient
	private String logoBase64;

	public Company() {

	}

	public Company(long companyId, String document, String registration, String companyName, String tradingName,
			String address, String zipCode, String city, String phone, String site) {
		super();
		this.companyId = companyId;
		this.document = document;
		this.registration = registration;
		this.companyName = companyName;
		this.tradingName = tradingName;
		this.address = address;
		this.zipCode = zipCode;
		this.city = city;
		this.phone = phone;
		this.site = site;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public String getRegistration() {
		return registration;
	}

	public void setRegistration(String registration) {
		this.registration = registration;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getTradingName() {
		return tradingName;
	}

	public void setTradingName(String tradingName) {
		this.tradingName = tradingName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getLogoName() {
		return logoName;
	}

	public void setLogoName(String logoName) {
		this.logoName = logoName;
	}

	public String getLogoBase64() {
		return logoBase64;
	}

	public void setLogoBase64(String logoBase64) {
		this.logoBase64 = logoBase64;
	}
}
