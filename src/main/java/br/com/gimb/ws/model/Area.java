package br.com.gimb.ws.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
public class Area extends BaseModel {
	public Area() {
		
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "area_id")
	private long areaId;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "active", columnDefinition = "bit default b'1'")
	private Boolean active;
	
	@Column(name = "color_id")
	private String colorId;

	@OneToMany(mappedBy="areaId", cascade=CascadeType.REFRESH)
	public List<AreaUser> users;

	public List<AreaUser> getUsers() {
		return users;
	}

	public void setUsers(List<AreaUser> users) {
		this.users = users;
	}

	public long getAreaId() {
		return areaId;
	}

	public void setAreaId(long areaId) {
		this.areaId = areaId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getColorId() {
		return colorId;
	}

	public void setColorId(String colorId) {
		this.colorId = colorId;
	}
}
