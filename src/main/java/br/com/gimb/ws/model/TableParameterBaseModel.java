package br.com.gimb.ws.model;

import br.com.gimb.annotation.IgnoreUpperCase;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import java.util.HashMap;

@MappedSuperclass
public class TableParameterBaseModel {

    @JsonIgnore
    @IgnoreUpperCase
    @Column(name="custom_values")
    private String customValuesString;

    @Transient
    private HashMap<String,Object> customValues;

    @Column(name = "guid")
    private String guid;

    public String getCustomValuesString() {
        return customValuesString;
    }

    public void setCustomValuesString(String customValuesString) {
        try {
            this.customValues = new ObjectMapper().readValue(customValuesString, HashMap.class);
        } catch (Exception e) {
            customValues = new HashMap<>();
        }
        this.customValuesString = customValuesString;
    }

    public HashMap<String, Object> getCustomValues() {
        if(customValues == null){
            try {
                this.customValues = new ObjectMapper().readValue(customValuesString, HashMap.class);
            } catch (Exception e) {
                customValues = new HashMap<>();
            }
        }
        return customValues;
    }

    public void setCustomValues(HashMap<String, Object> customValues) {
        try {
            this.customValuesString = new ObjectMapper().writeValueAsString(customValues);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        this.customValues = customValues;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

}
