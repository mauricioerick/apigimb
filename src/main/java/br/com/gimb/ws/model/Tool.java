package br.com.gimb.ws.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tools")
public class Tool extends BaseModel {
    public Tool() {

	}

    @Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "toolId")
	private long toolId;

    @Column(name = "description", length = 120, columnDefinition = "varchar(120)")
	private String description;

    @Column(name = "active", columnDefinition = "bit default b'1'")
	private Boolean active;

    public long getToolId() {
        return toolId;
    }

    public void setToolId(long toolId) {
        this.toolId = toolId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
