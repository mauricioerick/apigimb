package br.com.gimb.ws.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "object")
public class ObjectInventory extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "object_id")
    private long objectId;

    @Column(name = "identifier", length = 255, columnDefinition = "varchar(255)")
    private String identifier;

    @Column(name = "serial_number", length = 100, columnDefinition = "varchar(100)")
    private String serialNumber;
    
    @Column(name = "patrimony", length = 100, columnDefinition = "varchar(100)")
    private String patrimony;

    @Column(name = "location", length = 255, columnDefinition = "varchar(255)")
    private String location;

    @ManyToOne
	@JoinColumn(name="user_id_create", foreignKey = @ForeignKey(name="object_user_create_FK"))
    private User userCreate;
    
    @Column(name="date_time_create", length=20, columnDefinition = "varchar(20)")
    private String dateTimeCreate;
    
    @ManyToOne
	@JoinColumn(name="user_id_update", foreignKey = @ForeignKey(name="object_user_update_FK"))
    private User userUpdate;

    @Column(name="date_time_update", length=20, columnDefinition = "varchar(20)")
    private String dateTimeUpdate;

    @Column(name = "active", columnDefinition = "bit default b'1'")
    private Boolean active;

    @ManyToOne
	@JoinColumn(name="object_type_id", foreignKey = @ForeignKey(name="object_object_type_FK"))
    private ObjectType objectType;

    @ManyToOne
	@JoinColumn(name="storage_id", foreignKey = @ForeignKey(name="object_storage_FK"))
    private Storage storage;
    
    @OneToMany(mappedBy="object", cascade=CascadeType.MERGE)
	private List<ObjectHistory> objectHistoryList;
    
    @Transient
	private List<CustomField> customFieldList;
    
    @Transient
   	private List<CustomPicture> customPictureList;

    public long getObjectId() {
        return objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getPatrimony() {
        return patrimony;
    }

    public void setPatrimony(String patrimony) {
        this.patrimony = patrimony;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String startLocation) {
        this.location = startLocation;
    }

    public User getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(User userCreate) {
        this.userCreate = userCreate;
    }

    public String getDateTimeCreate() {
        return dateTimeCreate;
    }

    public void setDateTimeCreate(String dateTimeCreate) {
        this.dateTimeCreate = dateTimeCreate;
    }

    public User getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(User userUpdate) {
        this.userUpdate = userUpdate;
    }

    public String getDateTimeUpdate() {
        return dateTimeUpdate;
    }

    public void setDateTimeUpdate(String dateTimeUpdate) {
        this.dateTimeUpdate = dateTimeUpdate;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public ObjectType getObjectType() {
        return objectType;
    }

    public void setObjectType(ObjectType objectType) {
        this.objectType = objectType;
    }

    public Storage getStorage() {
        return storage;
    }

    public void setStorage(Storage storage) {
        this.storage = storage;
    }

    public List<ObjectHistory> getObjectHistoryList() {
        return objectHistoryList;
    }

    public void setObjectHistoryList(List<ObjectHistory> objectHistoryList) {
        this.objectHistoryList = objectHistoryList;
    }

	public List<CustomField> getCustomFieldList() {
		return customFieldList;
	}

	public void setCustomFieldList(List<CustomField> customFieldList) {
		this.customFieldList = customFieldList;
	}

	public List<CustomPicture> getCustomPictureList() {
		return customPictureList;
	}

	public void setCustomPictureList(List<CustomPicture> customPictureList) {
		this.customPictureList = customPictureList;
	}


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ObjectInventory that = (ObjectInventory) o;
        return objectId == that.objectId &&
                Objects.equals(identifier, that.identifier) &&
                Objects.equals(serialNumber, that.serialNumber) &&
                Objects.equals(patrimony, that.patrimony);
    }

    @Override
    public int hashCode() {
        return Objects.hash(objectId, identifier, serialNumber, patrimony);
    }
}
