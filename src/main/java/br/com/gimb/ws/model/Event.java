package br.com.gimb.ws.model;

import br.com.gimb.ws.enumerated.AppearsOnEnum;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Arrays;

@Entity
public class Event extends BaseModel {

	public Event() {

	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "event_id")
	private long eventId;

	@Column(name = "description", length = 80, columnDefinition = "varchar(80)")
	private String description;

	@Column(name = "improdutive_time")
	private Boolean improdutiveTime;

	@Column(name = "appears_on")
	private String appearsOn;

	@Column(name = "display_formula")
	private Boolean displayFormula;

	@Column(name = "images", length = 5000)
	private String images;

	@Column(name = "fields", columnDefinition = "TEXT")
	private String fields;

	@Column(name = "color_id")
	private String colorId;

	@Column(name = "request_km")
	private Boolean requestKm;

	@Transient
	private String[] appearsOnList;

	@Column(name = "active", columnDefinition = "bit default b'1'")
	private Boolean active;

	@ManyToOne
	@JoinColumn(name = "nature_expense_id")
	private NatureExpense natureExpense;

	public NatureExpense getNatureExpense() {
		return this.natureExpense;
	}

	public void setNatureExpense(NatureExpense natureExpense) {
		this.natureExpense = natureExpense;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public long getEventId() {
		return eventId;
	}

	public void setEventId(long eventId) {
		this.eventId = eventId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getImprodutiveTime() {
		return improdutiveTime;
	}

	public void setImprodutiveTime(Boolean improdutiveTime) {
		this.improdutiveTime = improdutiveTime;
	}

	public String getAppearsOn() {
		return appearsOn;
	}

	public void setAppearsOn(String appearsOn) {
		this.appearsOn = appearsOn;
	}

	public String[] getAppearsOnList() {

		if (appearsOnList == null && appearsOn != null) {
			ArrayList<String> lst = new ArrayList<String>();
			for (String t : appearsOn.split(",")) {
				lst.add(AppearsOnEnum.valueOf(t).getDescription());
			}

			appearsOnList = Arrays.copyOf(lst.toArray(), lst.size(), String[].class);
		}

		return appearsOnList;
	}

	public void setAppearsOnList(String[] appearsOnList) {
		this.appearsOnList = appearsOnList;
	}

	public Boolean getDisplayFormula() {
		return displayFormula;
	}

	public void setDisplayFormula(Boolean displayFormula) {
		this.displayFormula = displayFormula;
	}

	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}

	public String getFields() {
		return fields;
	}

	public void setFields(String fields) {
		this.fields = fields;
	}

	public String getColorId() {
		return colorId;
	}

	public void setColorId(String colorId) {
		this.colorId = colorId;
	}

	public Boolean getRequestKm() {
		return requestKm;
	}

	public void setRequestKm(Boolean requestKm) {
		this.requestKm = requestKm;
	}
}
