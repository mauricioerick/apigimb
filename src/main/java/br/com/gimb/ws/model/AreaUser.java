package br.com.gimb.ws.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "area_user")
public class AreaUser extends BaseModel {
	public AreaUser() {
		
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "area_user_id")
	private long areaUserId;
	
	@Column(name = "user_name")
	private String name;
	
	@Column(name = "user_user")
	private String user;
	
	@JoinColumn(name = "area_id", foreignKey = @ForeignKey(name  = "area_id"))
	private long areaId;
	
	@JoinColumn(name = "user_id", foreignKey = @ForeignKey(name  = "user_id"))
	private long userId;
	
	@Column(name = "responsible", columnDefinition="bit default b'1'")
	private Boolean responsible;

	public long getAreaId() {
		return areaId;
	}

	public void setAreaId(long areaId) {
		this.areaId = areaId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public Boolean getResponsible() {
		return responsible;
	}

	public void setResponsible(Boolean responsible) {
		this.responsible = responsible;
	}

	public long getAreaUserId() {
		return areaUserId;
	}

	public void setAreaUserId(long areaUserId) {
		this.areaUserId = areaUserId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}
	
}
