package br.com.gimb.ws.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Entity
public class Services extends BaseModel {

	public Services() {
		// TODO Auto-generated constructor stub
	}
	
	public Services(long serviceId) {
		this.serviceId = serviceId;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "serviceId")
	private long serviceId;
	
	@ManyToOne
	@JoinColumn (name  = "user_id", foreignKey = @ForeignKey(name  = "FK_SERVICE_USER")) 
	private User user;

	@ManyToOne
	@JoinColumn (name  = "created_by_user", foreignKey = @ForeignKey(name  = "FK_SERVICE_CREATED_BY_USER")) 
	private User createdByUser;
	
	@ManyToOne
	@JoinColumn (name  = "client_id", foreignKey = @ForeignKey(name  = "FK_SERVICE_CLIENT"))
	private Client client;
	
	@ManyToOne
	@JoinColumn (name  = "vehicle_id", foreignKey = @ForeignKey(name  = "FK_SERVICE_VEHICLE"))
	private Vehicle vehicle;
	
	@ManyToOne
	@JoinColumn (name  = "action_id", foreignKey = @ForeignKey(name  = "FK_SERVICE_ACTION"))
	private Action action;
	
	@Column(name = "startTime", length=20, columnDefinition="varchar(20)")
	private String startTime;

	@Column(name = "latStartTime")
	private Double latStartTime;
	
	@Column(name="lgtStartTime")
	private Double lgtStartTime;
	
	@Column(name = "endTime", length=20, columnDefinition="varchar(20)")
	private String endTime;
	
	@Column(name="latEndTime")
	private Double latEndTime;
	
	@Column(name = "lgtEndTime")
	private Double lgtEndTime;
	
	@Column(name = "note", length=5000)
	private String note;
	
	@Column(name = "status", length=1, columnDefinition="char(1)")
	private String status;
	
	@Column(name = "tags", length=100, columnDefinition="varchar(100)")
	private String tags;

	@Column(name = "feedback", columnDefinition="text")
	private String feedback;

	@Column(name = "technical_analysis", columnDefinition="text")
	private String technicalAnalysis;

	@Column(name = "conclusion", columnDefinition="text")
	private String conclusion;
	
	@OneToMany(mappedBy="servico", cascade=CascadeType.MERGE)
	private List<ServiceEvents> eventsList;
	
	@OneToMany(mappedBy="servico", cascade=CascadeType.MERGE)
	private List<ServiceImage> picsList;
	
	@OneToMany(mappedBy="service", cascade=CascadeType.MERGE)
	private List<ServiceInterruption> interruptionList;
	
	@Column(name = "justify_not_signature", columnDefinition="text")
	private String justify_not_signature;
	
	@Column(name = "path_signature", columnDefinition="text")
	private String path_signature;

	@Column(name = "justify_not_inventory", columnDefinition="text")
	private String justify_not_inventory;

	@Transient
	private List<HashMap<String,Object>> serviceObjectIdentifiers;

	@OneToMany(mappedBy="services", cascade=CascadeType.MERGE)
	private List<Checklist> checklists;

	@JsonIgnore
	@OneToMany(mappedBy="services", cascade=CascadeType.MERGE)
	private List<ObjectHistory> listObjectHistory;
    
	@ManyToOne
	@JoinColumn (name  = "project_id", foreignKey = @ForeignKey(name  = "project_id")) 
	private Project project;
	
	@Column(name = "status_checklist")
	private Boolean statusCheckList;
	
	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public String getJustify_not_signature() {
		return justify_not_signature;
	}

	public void setJustify_not_signature(String justify_not_signature) {
		this.justify_not_signature = justify_not_signature;
	}

	public String getPath_signature() {
		return path_signature;
	}

	public void setPath_signature(String path_signature) {
		this.path_signature = path_signature;
	}

	@Transient
	private List<CustomField> customFieldList;
	
	@Transient
	private Long serviceIdOnLine;
	
	public long getServiceId() {
		return serviceId;
	}

	public void setServiceId(long serviceId) {
		this.serviceId = serviceId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public Double getLatStartTime() {
		return latStartTime;
	}

	public void setLatStartTime(Double latStartTime) {
		this.latStartTime = latStartTime;
	}

	public Double getLgtStartTime() {
		return lgtStartTime;
	}

	public void setLgtStartTime(Double lgtStartTime) {
		this.lgtStartTime = lgtStartTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Double getLatEndTime() {
		return latEndTime;
	}

	public void setLatEndTime(Double latEndTime) {
		this.latEndTime = latEndTime;
	}

	public Double getLgtEndTime() {
		return lgtEndTime;
	}

	public void setLgtEndTime(Double lgtEndTime) {
		this.lgtEndTime = lgtEndTime;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTags() {
		if (tags == null) tags = "";
		
		if ("A".equals(status)) { // Partial
			if (tags.length() > 0) 
				tags = tags + "|";
			
			tags = tags + "PA";
		}
		
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getFeedback() {
		return feedback;
	}

	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}

	public String getTechnicalAnalysis() {
		return technicalAnalysis;
	}

	public void setTechnicalAnalysis(String technicalAnalysis) {
		this.technicalAnalysis = technicalAnalysis;
	}

	public String getConclusion() {
		return conclusion;
	}

	public void setConclusion(String conclusion) {
		this.conclusion = conclusion;
	}

	public List<ServiceEvents> getEventsList() {
		return eventsList;
	}

	public void setEventsList(List<ServiceEvents> eventsList) {
		this.eventsList = eventsList;
	}

	public List<ServiceImage> getPicsList() {
		return picsList;
	}

	public void setPicsList(List<ServiceImage> picsList) {
		this.picsList = picsList;
	}

	public List<ServiceInterruption> getInterruptionList() {
		return interruptionList;
	}

	public void setInterruptionList(List<ServiceInterruption> interruptionList) {
		this.interruptionList = interruptionList;
	}

	public List<CustomField> getCustomFieldList() {
		if (customFieldList == null) {
			customFieldList = new ArrayList<CustomField>();
		}
		return customFieldList;
	}

	public void setCustomFieldList(List<CustomField> customFieldList) {
		this.customFieldList = customFieldList;
	}

	public Long getServiceIdOnLine() {
		return serviceIdOnLine;
	}

	public void setServiceIdOnLine(Long serviceIdOnLine) {
		this.serviceIdOnLine = serviceIdOnLine;
	}

	public User getCreatedByUser() {
		return createdByUser;
	}

	public void setCreatedByUser(User createdByUser) {
		this.createdByUser = createdByUser;
	}

	public String getJustify_not_inventory() {
		return justify_not_inventory;
	}

	public void setJustify_not_inventory(String justify_not_inventory) {
		this.justify_not_inventory = justify_not_inventory;
	}

	public List<HashMap<String, Object>> getServiceObjectIdentifiers() {
		return serviceObjectIdentifiers;
	}

	public void setServiceObjectIdentifiers(List<HashMap<String, Object>> serviceObjectIdentifiers) {
		this.serviceObjectIdentifiers = serviceObjectIdentifiers;
	}

	public List<Checklist> getChecklists() {
		return checklists;
	}

	public void setChecklists(List<Checklist> checklists) {
		this.checklists = checklists;
	}

	public List<ObjectHistory> getListObjectHistory() {
		return listObjectHistory;
	}

	public void setListObjectHistory(List<ObjectHistory> listObjectHistory) {
		this.listObjectHistory = listObjectHistory;
	}

	public long getTimeElapsedMilli() {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			String end = endTime != null ? endTime : null;
			
			Long startMillis = sdf.parse(startTime).getTime();
			Long endMillis = end != null ? sdf.parse(end).getTime() : 0L;
			
			long millis = 0L;
			if (interruptionList != null && interruptionList.size() > 0) {
                for (ServiceInterruption interruption : interruptionList) {
                	if (interruption.getStartTime() == null) continue;
                	else if (interruption.getEndTime() == null && !interruption.equals(interruptionList.get(interruptionList.size() - 1))) continue;
                	
                    Long endInterruptionMillis = sdf.parse(interruption.getStartTime()).getTime();
                    millis += endInterruptionMillis - startMillis;

                    startMillis = 0L;
                    if (interruption.getEndTime() != null)
                    	startMillis = sdf.parse(interruption.getEndTime()).getTime();
                }

                millis += endMillis - startMillis;
			}
			
			if (millis == 0L) millis = (endMillis - startMillis);
			
			return millis <= 0 ? 0 : millis;
		} catch (Exception e) {
			// do nothing
		}
		
		return 0;
	}

	public String getTimeElapsed() {
		long millis = getTimeElapsedMilli();
			
		int seconds = (int) (millis / 1000) % 60 ;
		int minutes = (int) ((millis / (1000*60)) % 60);
		int hours   = (int) ((millis / (1000*60*60)) % 24);
        int days    = (int) ((millis / (1000*60*60*24)) % 365);
		
        String strTime = String.format("Tempo: %02d:%02d:%02d", hours, minutes, seconds);;
        if (days > 0)
            strTime = String.format("Dia(s): %02d Tempo: %02d:%02d:%02d", days, hours, minutes, seconds);
        
        return strTime;
	}

	public Boolean getStatusCheckList() {
		return statusCheckList;
	}

	public void setStatusCheckList(Boolean statusCheckList) {
		this.statusCheckList = statusCheckList;
	}
	
}
