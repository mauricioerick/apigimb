package br.com.gimb.ws.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.text.SimpleDateFormat;
import java.util.List;

@Entity
public class Transfer extends BaseModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "transferId")
	private long transferId;
	
	@ManyToOne
	@JoinColumn (name  = "user_id", foreignKey = @ForeignKey(name  = "FK_TRANSFER_USER")) 
	private User user;
	
	@ManyToOne
	@JoinColumn (name  = "client_id", foreignKey = @ForeignKey(name  = "FK_TRANSFER_CLIENT"))
	private Client client;
	
	@OneToMany(mappedBy="transfer", cascade=CascadeType.ALL)
	private List<TransferEvents> eventsList;
	
	@Column(name = "start_time", length=20, columnDefinition="varchar(20)")
	private String startTime;
	
	@Column(name = "start_time_original", length=20, columnDefinition="varchar(20)")
	private String startTimeOriginal;
	
	@Column(name = "endTime", length=20, columnDefinition="varchar(20)")
	private String endTime;
	
	@Column(name = "end_time_original", length=20, columnDefinition="varchar(20)")
	private String endTimeOriginal;

	@Column(name = "latStartTime")
	private Double latStartTime;
	
	@Column(name="latEndTime")
	private Double latEndTime;
	
	@Column(name="lgtStartTime")
	private Double lgtStartTime;
	
	@Column(name = "lgtEndTime")
	private Double lgtEndTime;
	
	@Column(name = "status", length=1, columnDefinition="char(1)")
	private String status;

	@Column(name="note", length=255, columnDefinition="varchar(255)")
	private String note;

	public long getTransferId() {
		return transferId;
	}

	public void setTransferId(long transferId) {
		this.transferId = transferId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public List<TransferEvents> getEventsList() {
		return eventsList;
	}

	public void setEventsList(List<TransferEvents> eventsList) {
		this.eventsList = eventsList;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getStartTimeOriginal() {
		return startTimeOriginal;
	}

	public void setStartTimeOriginal(String startTimeOriginal) {
		this.startTimeOriginal = startTimeOriginal;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getEndTimeOriginal() {
		return endTimeOriginal;
	}

	public void setEndTimeOriginal(String endTimeOriginal) {
		this.endTimeOriginal = endTimeOriginal;
	}

	public Double getLatStartTime() {
		return latStartTime;
	}

	public void setLatStartTime(Double latStartTime) {
		this.latStartTime = latStartTime;
	}

	public Double getLatEndTime() {
		return latEndTime;
	}

	public void setLatEndTime(Double latEndTime) {
		this.latEndTime = latEndTime;
	}

	public Double getLgtStartTime() {
		return lgtStartTime;
	}

	public void setLgtStartTime(Double lgtStartTime) {
		this.lgtStartTime = lgtStartTime;
	}

	public Double getLgtEndTime() {
		return lgtEndTime;
	}

	public void setLgtEndTime(Double lgtEndTime) {
		this.lgtEndTime = lgtEndTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public long getTimeElapsedMilli() {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Long startMillis = sdf.parse(startTime).getTime();
			Long endMillis = sdf.parse(endTime).getTime();
			
			long millis = (endMillis - startMillis);
			
			return millis <= 0 ? 0 : millis;
		} catch (Exception e) {
			// do nothing
		}
		
		return 0;
	}

	public String getTimeElapsed() {
		long millis = getTimeElapsedMilli();
			
		int seconds = (int) (millis / 1000) % 60 ;
		int minutes = (int) ((millis / (1000*60)) % 60);
		int hours   = (int) ((millis / (1000*60*60)) % 24);
        int days    = (int) ((millis / (1000*60*60*24)) % 365);
		
        String strTime = String.format("Tempo: %02d:%02d:%02d", hours, minutes, seconds);;
        if (days > 0)
            strTime = String.format("Dia(s): %02d Tempo: %02d:%02d:%02d", days, hours, minutes, seconds);
        
        return strTime;
	}
	
}
