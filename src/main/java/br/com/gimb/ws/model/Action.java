package br.com.gimb.ws.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Action extends BaseModel {

	public Action() {

	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "actionId")
	private long actionId;

	@Column(name = "description", length = 80, columnDefinition = "varchar(80)")
	private String description;

	@Column(name = "fields", columnDefinition = "TEXT")
	private String fields;

	@Column(name = "tools", columnDefinition = "TEXT")
	private String tools;

	@Column(name = "checklist", columnDefinition = "bit default b'0'")
	private Boolean checklist;

	@Column(name = "color_id")
	private String colorId;

	@Column(name = "active", columnDefinition = "bit default b'1'")
	private Boolean active;

	@Column(name = "signature", columnDefinition = "bit default b'0'")
	private Boolean signature;

	@Column(name = "inventory", columnDefinition = "bit default b'0'")
	private Boolean inventory;

	@Column(name = "observation", columnDefinition = "bit default b'0'")
	private Boolean observation;

	@Column(name = "critically", columnDefinition = "bit default b'0'")
	private Boolean critically;

	@Column(name = "critically_required", columnDefinition = "bit default b'0'")
	private Boolean criticallyRequired;

	@Column(name = "usa_sistema_subsistema", columnDefinition = "bit default b'0'")
	private Boolean usaSistemaSubSistema;
	
	@Column(name="required_photos")
	private String requiredPhotos;
	
	public Boolean getSignature() {
		return this.signature;
	}

	public void setSignature(Boolean signature) {
		this.signature = signature;
	}
	
	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public long getActionId() {
		return actionId;
	}

	public void setActionId(long actionId) {
		this.actionId = actionId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFields() {
		return fields;
	}

	public void setFields(String fields) {
		this.fields = fields;
	}

	public Boolean getChecklist() {
		return checklist != null ? checklist : false;
	}

	public void setChecklist(Boolean checklist) {
		this.checklist = checklist;
	}

	public Boolean getObservation() {
		return observation;
	}

	public void setObservation(Boolean observation) {
		this.observation = observation;
	}

	public Boolean getCritically() {
		return critically;
	}

	public void setCritically(Boolean critically) {
		this.critically = critically;
	}

	public Boolean getUsaSistemaSubSistema() {
		return usaSistemaSubSistema;
	}

	public void setUsaSistemaSubSistema(Boolean usaSistemaSubSistema) {
		this.usaSistemaSubSistema = usaSistemaSubSistema;
	}

	public Boolean getInventory() {
		return inventory;
	}

	public void setInventory(Boolean inventory) {
		this.inventory = inventory;
	}

	public String getColorId() {
		return colorId;
	}

	public void setColorId(String colorId) {
		this.colorId = colorId;
	}

	public String getRequiredPhotos() {
		return requiredPhotos;
	}

	public void setRequiredPhotos(String requiredPhotos) {
		this.requiredPhotos = requiredPhotos;
	}

	public String getTools() {
		return tools;
	}

	public void setTools(String tools) {
		this.tools = tools;
	}

	public Boolean getCriticallyRequired() {
		return criticallyRequired;
	}

	public void setCriticallyRequired(Boolean criticallyRequired) {
		this.criticallyRequired = criticallyRequired;
	}
}
