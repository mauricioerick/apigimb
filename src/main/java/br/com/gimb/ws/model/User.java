package br.com.gimb.ws.model;

import br.com.gimb.annotation.IgnoreUpperCase;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class User extends BaseModel {

	public User() {

	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "userId")
	private long userId;

	@Column(name = "user", length = 45, columnDefinition = "varchar(45)", nullable = false, unique = true)
	private String user;

	@Column(name = "name", length = 45, columnDefinition = "varchar(45)", nullable = false)
	private String name;

	@IgnoreUpperCase
	@Column(name = "pass", length = 60, columnDefinition = "varchar(60)", nullable = false)
	private String pass;

	@Column(name = "phone", length = 20, columnDefinition = "varchar(20)")
	private String phone;

	@Column(name = "phone_2", length = 20, columnDefinition = "varchar(20)")
	private String phone2;

	@IgnoreUpperCase
	@Column(name = "mail", length = 20, columnDefinition = "varchar(80)")
	private String mail;

	@Column(name = "birthDate", columnDefinition = "date")
	private Date birthDate;

	@Column(name = "userWeb", columnDefinition = "bit")
	private Boolean userWeb;

	@Column(name = "active", columnDefinition = "bit default b'1'")
	private Boolean active;

	@ManyToOne
	@JoinColumn(name = "profile_id", foreignKey = @ForeignKey(name = "FK_SERVICE_PROFILE"))
	private Profile profile;

	@Column(name = "color_id")
	private String colorId;
	
	@Column(name="cpf")
	private String cpf;

	@ManyToMany
	@JoinTable(name = "user_cost_center", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "cost_center_id"))
	private List<CostCenter> costcenters;

	@OneToMany(mappedBy = "user")
	private List<Device> devices;

	private transient String token;

	private transient String role;

	@JsonIgnore
	private transient String confirmPass;

	@ManyToOne
	@JoinColumn(name = "cost_center_id")
	private CostCenter costCenter;
	
	@OneToOne
	@JoinColumn(name = "card_id_main", foreignKey = @ForeignKey(name = "fk_card_id_main"))
	private Card cardMain;

	@OneToMany(mappedBy = "user")
	private List<Card> cards;

	@JsonIgnore
	@ManyToMany
	@JoinTable(name = "user_card", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "card_id"))
	private List<Card> userCards;

	@Column(name = "super_user")
	private boolean superUser;

	public List<CostCenter> getCostCenters() {
		return this.costcenters;
	}

	public void setCostCenters(List<CostCenter> costCenters) {
		this.costcenters = costCenters;
	}

	public CostCenter getCostCenter() {
		return this.costCenter;
	}

	public void seCostCenter(CostCenter costCenter) {
		this.costCenter = costCenter;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Boolean getUserWeb() {
		return userWeb;
	}

	public void setUserWeb(Boolean userWeb) {
		this.userWeb = userWeb;
	}

	public String getConfirmPass() {
		return confirmPass;
	}

	public void setConfirmPass(String confirmPass) {
		this.confirmPass = confirmPass;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public String getColorId() {
		return colorId;
	}

	public void setColorId(String colorId) {
		this.colorId = colorId;
	}

	public String getBirthDateString() {
		if (birthDate == null)
			return "";
		else {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			return sdf.format(birthDate);
		}
	}

	public Card getCardMain() {
		return cardMain;
	}

	public void setCardMain(Card cardMain) {
		this.cardMain = cardMain;
	}

	public List<Card> getUserCards() {
		return userCards;
	}

	public void setUserCards(List<Card> userCards) {
		this.userCards = userCards;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public List<Device> getDevices() {
		return devices;
	}

	public void setDevices(List<Device> devices) {
		this.devices = devices;
	}

	public List<Card> getCards() {
		return cards;
	}

	public void setCards(List<Card> cards) {
		this.cards = cards;
	}

	public boolean isSuperUser() {
		return superUser;
	}

	public void setSuperUser(boolean superUser) {
		this.superUser = superUser;
	}
}
