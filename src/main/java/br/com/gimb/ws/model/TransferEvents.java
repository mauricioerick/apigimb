package br.com.gimb.ws.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.text.SimpleDateFormat;

@Entity
@Table(name="transfer_events")
public class TransferEvents extends BaseModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "transfer_events_id")
	private long transferEventsId;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="transfersId", foreignKey = @ForeignKey(name  = "FK_TRANSFER"))
	private Transfer transfer;
	
	@ManyToOne
	@JoinColumn(name="eventId", foreignKey = @ForeignKey(name  = "FK_EVENT"))
	private Event event;
	
	@Column(name="start_time", length=20)
	private String startTime;
	
	@Column(name="start_time_original", length=20)
	private String startTimeOriginal;
	
	@Column(name = "endTime", length=20)
	private String endTime;
	
	@Column(name = "end_time_original", length=20)
	private String endTimeOriginal;
	
	@Column(name="latitude")
	private Double latitude;
	
	@Column(name="longitude")
	private Double longitude;

	@Column(name="note", length=255, columnDefinition="varchar(255)")
	private String note;

	@Column(name = "km_start")
	private Double kmStart;

	@Column(name = "km_end")
	private Double kmEnd;

	@Column(name = "km_traveled")
	private Double kmTraveled;

	public long getTransferEventsId() {
		return transferEventsId;
	}

	public void setTransferEventsId(long transferEventsId) {
		this.transferEventsId = transferEventsId;
	}

	public Transfer getTransfer() {
		return transfer;
	}

	public void setTransfer(Transfer transfer) {
		this.transfer = transfer;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getStartTimeOriginal() {
		return startTimeOriginal;
	}

	public void setStartTimeOriginal(String startTimeOriginal) {
		this.startTimeOriginal = startTimeOriginal;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getEndTimeOriginal() {
		return endTimeOriginal;
	}

	public void setEndTimeOriginal(String endTimeOriginal) {
		this.endTimeOriginal = endTimeOriginal;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Double getKmStart() {
		return kmStart;
	}

	public void setKmStart(Double kmStart) {
		this.kmStart = kmStart;
	}

	public Double getKmEnd() {
		return kmEnd;
	}

	public void setKmEnd(Double kmEnd) {
		this.kmEnd = kmEnd;
	}

	public Double getKmTraveled() {
		return kmTraveled;
	}

	public void setKmTraveled(Double kmTraveled) {
		this.kmTraveled = kmTraveled;
	}

	public long getTimeElapsedMilli() {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Long startMillis = sdf.parse(startTime).getTime();
			Long endMillis = sdf.parse(endTime).getTime();
			
			long millis = (endMillis - startMillis);
			
			return millis <= 0 ? 0 : millis;
		} catch (Exception e) {
			// do nothing
		}
		
		return 0;
	}

	public String getTimeElapsed() {
		long millis = getTimeElapsedMilli();
			
		int seconds = (int) (millis / 1000) % 60 ;
		int minutes = (int) ((millis / (1000*60)) % 60);
		int hours   = (int) ((millis / (1000*60*60)) % 24);
        int days    = (int) ((millis / (1000*60*60*24)) % 365);
		
        String strTime = String.format("Tempo: %02d:%02d:%02d", hours, minutes, seconds);;
        if (days > 0)
            strTime = String.format("Dia(s): %02d Tempo: %02d:%02d:%02d", days, hours, minutes, seconds);
        
        return strTime;
	}

	public String getSimpleTimeElapsed() {
		long millis = getTimeElapsedMilli();
			
		int seconds = (int) (millis / 1000) % 60 ;
		int minutes = (int) ((millis / (1000*60)) % 60);
		int hours   = (int) ((millis / (1000*60*60)) % 24);
        int days    = (int) ((millis / (1000*60*60*24)) % 365);
		
        String strTime = String.format("%02d:%02d:%02d", hours, minutes, seconds);;
        if (days > 0)
            strTime = String.format("%02d - %02d:%02d:%02d", days, hours, minutes, seconds);
        
        return strTime;
	}
}
