package br.com.gimb.ws.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="service_events")
public class ServiceEvents extends BaseModel {

	public ServiceEvents() {
		// TODO Auto-generated constructor stub
	}

	public ServiceEvents(long serviceEventsId, Services servico, Event event, Double latitude,
			Double longitude, String dateTime) {
		super();
		this.serviceEventsId = serviceEventsId;
		this.servico = servico;
		this.event = event;
		this.latitude = latitude;
		this.longitude = longitude;
		this.dateTime = dateTime;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "service_events_id")
	private long serviceEventsId;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="servicesId", foreignKey = @ForeignKey(name  = "FK_SERVICE"))
	private Services servico;
	
	@ManyToOne
	@JoinColumn(name="eventId", foreignKey = @ForeignKey(name  = "FK_EVET"))
	private Event event;
	
	@Column(name="date_time", length=20)
	private String dateTime;
	
	@Column(name="latitude")
	private Double latitude;
	
	@Column(name="longitude")
	private Double longitude;

	public long getServiceEventsId() {
		return serviceEventsId;
	}

	public void setServiceEventsId(long serviceEventsId) {
		this.serviceEventsId = serviceEventsId;
	}

	public Services getServico() {
		return servico;
	}

	public void setServico(Services servico) {
		this.servico = servico;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
	public String getDate() {
		return this.dateTime.substring(0, 10);
	}
	
	public String getTime() {
		return this.dateTime.substring(11, 16);
	}
	
	public String getEventAndTimeDescription() {
		return String.format("%1$20s - %s", this.event != null ? this.event.getDescription() : "", this,getTime());
	}
	
}
