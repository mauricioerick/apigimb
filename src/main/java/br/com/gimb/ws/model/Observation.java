package br.com.gimb.ws.model;

import br.com.gimb.ws.enumerated.AppearsOnEnum;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Arrays;

@Entity
public class Observation extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "observation_id")
    private long observationId;

    @Column(name = "description", length = 80, columnDefinition = "varchar(80)")
	private String description;

    @Column(name = "appears_on")
    private String appearsOn;
    
    @Column(name = "active", columnDefinition = "bit default b'1'")
    private Boolean active;
    
    @Transient
    private String[] appearsOnList;

    public long getObservationId() {
        return observationId;
    }

    public void setObservationId(long observationId) {
        this.observationId = observationId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAppearsOn() {
        return appearsOn;
    }

    public void setAppearsOn(String appearsOn) {
        this.appearsOn = appearsOn;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String[] getAppearsOnList() {

		if (appearsOnList == null && appearsOn != null) {
			ArrayList<String> lst = new ArrayList<String>();
			for (String t : appearsOn.split(",")) {
				lst.add(AppearsOnEnum.valueOf(t).getDescription());
			}

			appearsOnList = Arrays.copyOf(lst.toArray(), lst.size(), String[].class);
		}

		return appearsOnList;
	}

	public void setAppearsOnList(String[] appearsOnList) {
		this.appearsOnList = appearsOnList;
    }    
}
