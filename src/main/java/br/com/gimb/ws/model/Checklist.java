package br.com.gimb.ws.model;

import br.com.gimb.ws.enumerated.ChecklistStatusEnum;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "checklist")
public class Checklist extends BaseModel {
	public Checklist() {
		
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "checklist_id")
	private long checklistId;
	
	@Column(name = "cheklist_item")
	private String cheklistItem	;

	@Enumerated
	@Column(name = "status")
	private ChecklistStatusEnum status;

	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "service_id", foreignKey = @ForeignKey(name  = "service_id"))
	private Services services;

	public long getChecklistId() {
		return checklistId;
	}

	public void setChecklistId(long checklistId) {
		this.checklistId = checklistId;
	}

	public String getCheklistItem() {
		return cheklistItem;
	}

	public void setCheklistItem(String cheklistItem) {
		this.cheklistItem = cheklistItem;
	}

	public ChecklistStatusEnum getStatus() {
		return status;
	}

	public void setStatus(ChecklistStatusEnum status) {
		this.status = status;
	}

	public Services getServices() {
		return services;
	}

	public void setServices(Services services) {
		this.services = services;
	}
}
