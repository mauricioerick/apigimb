package br.com.gimb.ws.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "tool_services_pics")
public class ToolServicesPics extends BaseModel {
    public ToolServicesPics() {

	}

    @Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "toolServicesPicsId")
	private long toolServicesPicsId;
    
    // @ManyToOne(cascade = CascadeType.ALL)   
    @Column(name = "tool_id")
	private long toolId;

    // @ManyToOne(cascade = CascadeType.ALL)   
    @Column(name = "service_pics_id")
    private long servicePicsId;

    @Column(name = "tools", length = 120, columnDefinition = "varchar(120)")
	private String tools;

    public long getToolServicesPicsId() {
        return toolServicesPicsId;
    }

    public void setToolServicesPicsId(long toolServicesPicsId) {
        this.toolServicesPicsId = toolServicesPicsId;
    }

    public long getToolId() {
        return toolId;
    }

    public void setToolId(long toolId) {
        this.toolId = toolId;
    }

    public long getServicesPicsId() {
        return servicePicsId;
    }

    public void setServicesPicsId(long servicePicsId) {
        this.servicePicsId = servicePicsId;
    }

    public String getTools() {
        return tools;
    }

    public void setTools(String tools) {
        this.tools = tools;
    }            
}
