package br.com.gimb.ws.model;

import br.com.gimb.annotation.IgnoreUpperCase;
import br.com.gimb.ws.enumerated.ReferenceTypeEnum;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "custom_picture")
public class CustomPicture extends BaseModel  {

	public CustomPicture() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "custom_picture_id")
	private long custompictureId;

	@Enumerated(EnumType.STRING)
	@Column(name = "reference_type")
	private ReferenceTypeEnum referenceType;

	@Column(name = "reference_id")
	private String referenceId;

	@Column(name = "latitude")
	private Double latitude;

	@Column(name = "longitude")
	private Double longitude;

	@IgnoreUpperCase
	@Column(name = "pic_path", length = 255)
	private String picPath;

	@IgnoreUpperCase
	@Column(name = "pic_name", length = 80)
	private String picName;

	@IgnoreUpperCase
	@Column(name = "pic_mime", length = 3)
	private String picMime;

	@Column(name = "message")
	private String message;

	/**
	 * String Base64 from picture
	 */
	private transient String pic;

	public CustomPicture(Integer custompictureId, ReferenceTypeEnum referenceType, String referenceId, Double latitude,
			Double longitude, String picPath, String picName, String picMime, String message, String pic) {
		super();
		this.custompictureId = custompictureId;
		this.referenceType = referenceType;
		this.referenceId = referenceId;
		this.latitude = latitude;
		this.longitude = longitude;
		this.picPath = picPath;
		this.picName = picName;
		this.picMime = picMime;
		this.message = message;
		this.pic = pic;
	}

	public long getCustompictureId() {
		return custompictureId;
	}

	public void setCustompictureId(long custompictureId) {
		this.custompictureId = custompictureId;
	}

	public ReferenceTypeEnum getReferenceType() {
		return referenceType;
	}

	public void setReferenceType(ReferenceTypeEnum referenceType) {
		this.referenceType = referenceType;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getPicPath() {
		return picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}

	public String getPicName() {
		return picName;
	}

	public void setPicName(String picName) {
		this.picName = picName;
	}

	public String getPicMime() {
		return picMime;
	}

	public void setPicMime(String picMime) {
		this.picMime = picMime;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

}
