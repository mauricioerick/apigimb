package br.com.gimb.ws.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class NatureExpense extends BaseModel {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "nature_expense_id")
	private long natureExpenseId;

	@Column(name = "account_code")
	private String accountCode;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "active", columnDefinition = "bit default b'1'")
	private Boolean active;

	@Column(name = "color_id")
	private String colorId;

	public long getNatureExpenseId() {
		return natureExpenseId;
	}

	public void setNatureExpenseId(long natureExpenseId) {
		this.natureExpenseId = natureExpenseId;
	}

	public String getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getColorId() {
		return colorId;
	}

	public void setColorId(String colorId) {
		this.colorId = colorId;
	}
	
	
}
