package br.com.gimb.ws.model;

import br.com.gimb.annotation.IgnoreUpperCase;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "service_pics")
public class ServiceImage extends BaseModel {

	public ServiceImage() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "pic_id")
	private long picId;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "servicesId")
	private Services servico;

	@IgnoreUpperCase
	@Column(name = "pic_name", length = 80)
	private String picName;

	@IgnoreUpperCase
	@Column(name = "pic_path", length = 255)
	private String picPath;

	@IgnoreUpperCase
	@Column(name = "pic_mime", length = 3)
	private String picMime;

	@Column(name = "pic_latitude")
	private Double latPic;

	@Column(name = "pic_longitude")
	private Double lgtPic;

	@Column(name = "message")
	private String message;

	@IgnoreUpperCase
	@Column(name = "note")
	private String note;

	@Column(name = "manually", columnDefinition = "bit default b'0'")
	private Boolean manually;

	@Column(name = "status_checklist")
	private Boolean statusChecklist;

	@Column(name = "status_fix")
	private Boolean statusFix;

	@IgnoreUpperCase
	@Column(name = "fix_pic_name", length = 80)
	private String fixPicName;

	@IgnoreUpperCase
	@Column(name = "fix_pic_path", length = 255)
	private String fixPicPath;

	@IgnoreUpperCase
	@Column(name = "fix_pic_mime", length = 3)
	private String fixPicMime;

	private transient String pic;

	private transient String fixPic;

	private transient String picThumb;

	@IgnoreUpperCase
	@Column(name = "pic_thumb_name")
	private String picThumbName;

	@IgnoreUpperCase
	@Column(name = "pic_thumb_path")
	private String picThumbPath;

	@IgnoreUpperCase
	@Column(name = "pic_thumb_mime")
	private String picThumbMime;

	@Column(name = "chk_print")
	private Boolean chkPrint;

	@Column(name = "critically")
	private String critically;

	@Column(name = "work")
	private String work;

	@Column(name = "time")
	private String time;

	@ManyToOne
	@JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "service_pics_user_id_fk"))
	private User user;

	@ManyToOne
	@JoinColumn(name = "checklist_items_item_id", foreignKey = @ForeignKey(name = "service_pics_checklist_items_item_id_fk"))
	private ChecklistItems checklistItems;

	@ManyToMany(cascade = CascadeType.ALL)  	
	@JoinTable(name = "tool_services_pics", joinColumns = @JoinColumn(name = "service_pics_id"), inverseJoinColumns = @JoinColumn(name = "tool_id"))
	private List<ToolServicesPics> toolServicesPics;

	public Boolean getChkPrint() {
		return chkPrint;
	}

	public void setChkPrint(Boolean chk) {
		this.chkPrint = chk;
	}

	public String getPicThumb() {
		return picThumb;
	}

	public void setPicThumb(String picThumb) {
		this.picThumb = picThumb;
	}

	public long getPicId() {
		return picId;
	}

	public Services getServico() {
		return servico;
	}

	public String getPicName() {
		return picName;
	}

	public String getPicPath() {
		return picPath;
	}

	public String getPicMime() {
		return picMime;
	}

	public Double getLatPic() {
		return latPic;
	}

	public Double getLgtPic() {
		return lgtPic;
	}

	public String getPic() {
		return pic;
	}

	public void setPicId(long picId) {
		this.picId = picId;
	}

	public void setServico(Services servico) {
		this.servico = servico;
	}

	public void setPicName(String picName) {
		this.picName = picName;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}

	public void setPicMime(String picMime) {
		this.picMime = picMime;
	}

	public void setLatPic(Double latPic) {
		this.latPic = latPic;
	}

	public void setLgtPic(Double lgtPic) {
		this.lgtPic = lgtPic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Boolean getManually() {
		return manually;
	}

	public void setManually(Boolean manually) {
		this.manually = manually;
	}

	public String getFixPic() {
		return fixPic;
	}

	public void setFixPic(String fixPic) {
		this.fixPic = fixPic;
	}

	public Boolean getStatusChecklist() {
		return statusChecklist;
	}

	public void setStatusChecklist(Boolean statusChecklist) {
		this.statusChecklist = statusChecklist;
	}

	public Boolean getStatusFix() {
		return statusFix;
	}

	public void setStatusFix(Boolean statusFix) {
		this.statusFix = statusFix;
	}

	public String getFixPicName() {
		return fixPicName;
	}

	public void setFixPicName(String fixPicName) {
		this.fixPicName = fixPicName;
	}

	public String getFixPicPath() {
		return fixPicPath;
	}

	public void setFixPicPath(String fixPicPath) {
		this.fixPicPath = fixPicPath;
	}

	public String getFixPicMime() {
		return fixPicMime;
	}

	public void setFixPicMime(String fixPicMime) {
		this.fixPicMime = fixPicMime;
	}

	public String getPicThumbName() {
		return picThumbName;
	}

	public void setPicThumbName(String picThumbName) {
		this.picThumbName = picThumbName;
	}

	public String getPicThumbPath() {
		return picThumbPath;
	}

	public void setPicThumbPath(String picThumbPath) {
		this.picThumbPath = picThumbPath;
	}

	public String getPicThumbMime() {
		return picThumbMime;
	}

	public void setPicThumbMime(String picThumbMime) {
		this.picThumbMime = picThumbMime;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public ChecklistItems getChecklistItems() {
		return checklistItems;
	}

	public void setChecklistItems(ChecklistItems checklistItems) {
		this.checklistItems = checklistItems;
	}

	public String getCritically() {
		return critically;
	}

	public void setCritically(String critically) {
		this.critically = critically;
	}

	public String getWork() {
		return work;
	}

	public void setWork(String work) {
		this.work = work;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public List<ToolServicesPics> getToolServicesPics() {
		return toolServicesPics;
	}

	public void setToolServicesPics(List<ToolServicesPics> toolServicesPics) {
		this.toolServicesPics = toolServicesPics;
	}
}
