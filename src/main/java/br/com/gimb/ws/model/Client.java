package br.com.gimb.ws.model;

import br.com.gimb.annotation.IgnoreUpperCase;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import java.util.List;

@Entity
public class Client extends BaseModel {

	public Client() {

	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "clientId")
	private long clientId;

	@Column(name = "document", length = 20, columnDefinition = "varchar(20)", unique = true)
	private String document;

	@Column(name = "companyName", length = 60, columnDefinition = "varchar(60)", unique = true)
	private String companyName;

	@Column(name = "tradingName", length = 60, columnDefinition = "varchar(60)")
	private String tradingName;

	@Column(name = "address", length = 60, columnDefinition = "varchar(60)")
	private String address;

	@Column(name = "phone", length = 45, columnDefinition = "varchar(45)")
	private String phone;

	@IgnoreUpperCase
	@Column(name = "pass", length = 45, columnDefinition = "varchar(45)")
	private String pass;

	@Deprecated
	@IgnoreUpperCase
	@Column(name = "situacao", length = 7, columnDefinition = "varchar(7) default 'ativo'")
	private String situacao;

	@Column(name = "color_id")
	private String colorId;

	@Column(name = "active", columnDefinition = "bit default b'1'")
	private Boolean active;

	@Column(name = "zip_code", length = 60, columnDefinition = "varchar(60)")
	private String zipCode;

	@Column(name = "city", length = 255, columnDefinition = "varchar(255)")
	private String city;

	@Column(name = "registration", length = 20, columnDefinition = "varchar(20)")
	private String registration;

	@OneToOne(fetch = FetchType.EAGER, mappedBy = "client", optional = true)
	private transient List<Vehicle> vehicles;

	@OneToOne(fetch = FetchType.EAGER, mappedBy = "client", optional = true)
	private transient List<Contact> contacts;

	@ManyToMany
	@JoinTable(name = "client_cost_center", joinColumns = @JoinColumn(name = "client_id"), inverseJoinColumns = @JoinColumn(name = "cost_center_id"))
	private List<CostCenter> costcenters;

	public List<CostCenter> getCostCenters() {
		return this.costcenters;
	}

	public void setCostCenters(List<CostCenter> costCenters) {
		this.costcenters = costCenters;
	}

	@ManyToOne
	@JoinColumn(name = "cost_center_id")
	private CostCenter costCenter;

	public CostCenter getCostCenter() {
		return this.costCenter;
	}

	public void seCostCenter(CostCenter costCenter) {
		this.costCenter = costCenter;
	}

	public List<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}

	public long getClientId() {
		return clientId;
	}

	public void setClientId(long clientId) {
		this.clientId = clientId;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getTradingName() {
		return tradingName;
	}

	public void setTradingName(String tradingName) {
		this.tradingName = tradingName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public List<Vehicle> getVehicles() {
		return vehicles;
	}

	public void setVehicles(List<Vehicle> vehicles) {
		this.vehicles = vehicles;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public String getColorId() {
		return colorId;
	}

	public void setColorId(String colorId) {
		this.colorId = colorId;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getRegistration() {
		return registration;
	}

	public void setRegistration(String registration) {
		this.registration = registration;
	}
}
