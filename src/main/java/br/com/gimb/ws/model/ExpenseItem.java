package br.com.gimb.ws.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="expense_item")
public class ExpenseItem extends BaseModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "expense_item_id")
	private long expenseItemId;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "expense_id", foreignKey = @ForeignKey(name = "FK_EXPENSE_ITEM_EXPENSE"))
	private Expense expense;
	
	@Column(name = "date", length=20)
	private String date;
	
	@ManyToOne
	@JoinColumn(name = "event_id", foreignKey = @ForeignKey(name = "FK_EXPENSE_ITEM_EVENT"))
	private Event event;
	
	@Column(name = "quantity", precision=2)
	private Double quantity;
	
	@Column(name = "unit_value", precision=2)
	private Double unitValue;
	
	@Column(name = "total_value", precision=2)
	private Double totalValue;
	
	@OneToOne
	@JoinColumn(name = "settlement_id", foreignKey = @ForeignKey(name = "FK_EXPENSE_ITEM_SETTLEMENT"))
	private Settlement settlement;
	
	@Column(name = "note", length=1000)
	private String note;
	
	@Column(name = "create_date", length=20)
	private String createDate;
	
	@ManyToOne
	@JoinColumn(name = "create_user", foreignKey = @ForeignKey(name = "FK_EXPENSE_ITEM_USER"))
	private User createUser;

	public ExpenseItem() {
	}

	public ExpenseItem(long expenseItemId, Expense expense, String date, Event event, Double quantity, Double unitValue,
			Double totalValue, Settlement settlement, String note, String createDate, User createUser) {
		super();
		this.expenseItemId = expenseItemId;
		this.expense = expense;
		this.date = date;
		this.event = event;
		this.quantity = quantity;
		this.unitValue = unitValue;
		this.totalValue = totalValue;
		this.settlement = settlement;
		this.note = note;
		this.createDate = createDate;
		this.createUser = createUser;
	}

	public long getExpenseItemId() {
		return expenseItemId;
	}

	public void setExpenseItemId(long expenseItemId) {
		this.expenseItemId = expenseItemId;
	}

	public Expense getExpense() {
		return expense;
	}

	public void setExpense(Expense expense) {
		this.expense = expense;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public Double getQuantity() {
		return quantity != null ? quantity : 0.0;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getUnitValue() {
		return unitValue != null ? unitValue : 0.0;
	}

	public void setUnitValue(Double unitValue) {
		this.unitValue = unitValue;
	}

	public Double getTotalValue() {
		return totalValue != null ? totalValue : 0.0;
	}

	public void setTotalValue(Double totalValue) {
		this.totalValue = totalValue;
	}

	public Settlement getSettlement() {
		return settlement;
	}

	public void setSettlement(Settlement settlement) {
		this.settlement = settlement;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public User getCreateUser() {
		return createUser;
	}

	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}
	
	public String getQuantityFormatted() {
		return (quantity.doubleValue() == quantity.intValue()) ? String.format("%d", quantity.intValue()) : String.format("%.2f", quantity);
	}
	
	public String getUnitValueFormatted() {
		return String.format("%.2f", unitValue);
	}
	
	public String getTotalValueFormatted() {
		return String.format("%.2f", totalValue);
	}
	
}
