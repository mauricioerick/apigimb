package br.com.gimb.ws.model;

import br.com.gimb.annotation.IgnoreUpperCase;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Profile extends BaseModel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "profile_id")
	private long profileId;

	@Column(name = "description", length = 80, columnDefinition = "varchar(80)")
	private String description;

	@Column(name = "app_access", columnDefinition = "text")
	private String appAccess;

	@IgnoreUpperCase
	@Column(name = "web_access", columnDefinition = "text")
	private String webAccess;

	@Column(name = "color_id")
	private String colorId;

	@IgnoreUpperCase
	@Column(name = "config", columnDefinition = "text")
	private String config;

	@Column(name = "active", columnDefinition = "bit default b'1'")
	private Boolean active;

	@Column(name = "config_web", columnDefinition = "text")
	private String configWeb;
	
	public String getConfigWeb() {
		return configWeb;
	}

	public void setConfigWeb(String configWeb) {
		this.configWeb = configWeb;
	}
	
	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public long getProfileId() {
		return profileId;
	}

	public void setProfileId(long profileId) {
		this.profileId = profileId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAppAccess() {
		return appAccess;
	}

	public void setAppAccess(String appAccess) {
		this.appAccess = appAccess;
	}

	public String getWebAccess() {
		return webAccess;
	}

	public void setWebAccess(String webAccess) {
		this.webAccess = webAccess;
	}

	public String getConfig() {
		return config;
	}

	public void setConfig(String config) {
		this.config = config;
	}

	public String getColorId() {
		return colorId;
	}

	public void setColorId(String colorId) {
		this.colorId = colorId;
	}

	
}
