package br.com.gimb.ws.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import java.util.List;

@Entity
public class Document extends BaseModel {
	public Document() {
		
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "document_id")
	private long documentId;
	
	@ManyToOne
	@JoinColumn(name = "user_id", foreignKey = @ForeignKey(name  = "user_id"))
	private User user;
	
	@Column(name = "request")
	private String request;
	
	@ManyToOne
	@JoinColumn(name = "area_id", foreignKey = @ForeignKey(name  = "area_id"))
	private Area area;
	
	@Column(name = "note")
	private String note;
	
	@Column(name = "start_time")
	private String startTime;
	
	@Column(name = "location")
	private String location;
	
	@Transient
	private List<CustomPicture> picsList;

	public long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(long documentId) {
		this.documentId = documentId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public List<CustomPicture> getPicsList() {
		return picsList;
	}

	public void setPicsList(List<CustomPicture> picsList) {
		this.picsList = picsList;
	}

	
}
