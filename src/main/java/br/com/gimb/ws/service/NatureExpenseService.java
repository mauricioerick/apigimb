package br.com.gimb.ws.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.NatureExpense;
import br.com.gimb.ws.repository.NatureExpenseRepository;

@Service
public class NatureExpenseService extends BaseService<NatureExpense, Long> {
	@RepositoryClass(clazz = NatureExpenseRepository.class)
	@Autowired
	private NatureExpenseRepository natureExpenseRepository;
	
	public List<NatureExpense> findAll(){
		return natureExpenseRepository.findAll();
	}
	
	public List<NatureExpense> findByNatureExpenseId(long id){
		return natureExpenseRepository.findByNatureExpenseId(id);
	}
	
	public List<NatureExpense> findByDescription(String description){
		return natureExpenseRepository.findByDescription(description);
	}
	
	public List<NatureExpense> findByActive(String active){
		if(active.equalsIgnoreCase("all"))
				return natureExpenseRepository.findAll();
		else
			return natureExpenseRepository.findByActive(active.equalsIgnoreCase("true"));
	}
}
