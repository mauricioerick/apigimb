package br.com.gimb.ws.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.Equipment;
import br.com.gimb.ws.repository.EquipmentRepository;

@Service
public class EquipmentService extends BaseService<Equipment, Long> {

	@RepositoryClass(clazz = EquipmentRepository.class)
	@Autowired
	private EquipmentRepository equipmentRepository;

	public List<Equipment> findByDescription(String description) {
		return equipmentRepository.findByDescription(description);
	}

	public List<Equipment> findByActive(String active) {
		if (active.equalsIgnoreCase("all"))
			return equipmentRepository.findAll();
		else
			return equipmentRepository.findByActive(active.equalsIgnoreCase("true"));
	}
}
