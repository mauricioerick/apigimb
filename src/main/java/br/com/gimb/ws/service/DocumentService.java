package br.com.gimb.ws.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.Document;
import br.com.gimb.ws.repository.DocumentRepository;

@Service
public class DocumentService extends BaseService<Document, Long> {
	@RepositoryClass(clazz = DocumentRepository.class)
	@Autowired
	private DocumentRepository documentRepository;
	
	public List<Document> findAll(){
		return documentRepository.findAll();
	}
	
	public Document findByDocumentId(long id) {
		return documentRepository.findByDocumentId(id);
	}
}
