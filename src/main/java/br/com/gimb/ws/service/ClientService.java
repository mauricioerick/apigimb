package br.com.gimb.ws.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.Client;
import br.com.gimb.ws.repository.ClientRepository;

@Service
public class ClientService extends BaseService<Client, Long> {

	@RepositoryClass(clazz = ClientRepository.class)
	@Autowired
	private ClientRepository clientRepository;
	
	public List<Client> findAllBySituacao(String active) {
		return clientRepository.findByActive("true".equalsIgnoreCase(active));
	}

	public Client findByDocument(String document) {
		List<Client> clients = clientRepository.findByDocument(document);
		if (clients != null && clients.size() > 0)
			return clients.get(0);
		
		return null;
	}

	public List<Client> findByActive(String active) {
		if (active.equalsIgnoreCase("all"))
			return clientRepository.findAll();
		else
			return clientRepository.findByActive(active.equalsIgnoreCase("true"));
	}

	public Client findByTradingName(String tradingName) {
		List<Client> clients = clientRepository.findByTradingName(tradingName);
		if (clients != null && clients.size() > 0)
			return clients.get(0);
		
		return null;
	}
}
