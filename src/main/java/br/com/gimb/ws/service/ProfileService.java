package br.com.gimb.ws.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.Profile;
import br.com.gimb.ws.repository.ProfileRepository;

@Service
public class ProfileService extends BaseService<Profile, Long> {

	@RepositoryClass(clazz = ProfileRepository.class)
	@Autowired
	private ProfileRepository profileRepository;

	public List<Profile> findByActive(String active) {
		if (active.equalsIgnoreCase("all"))
			return profileRepository.findAll();
		else
			return profileRepository.findByActive(active.equalsIgnoreCase("true"));
	}

}
