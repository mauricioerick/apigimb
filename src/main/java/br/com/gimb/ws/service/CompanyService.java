package br.com.gimb.ws.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.Company;
import br.com.gimb.ws.repository.CompanyRepository;

@Service
public class CompanyService extends BaseService<Company, Long>{

	@RepositoryClass(clazz = CompanyRepository.class)
	@Autowired
	private CompanyRepository companyRepository;
}
