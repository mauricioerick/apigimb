package br.com.gimb.ws.service;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.ws.controller.CustomPictureController;
import br.com.gimb.ws.model.CustomPicture;
import br.com.gimb.ws.model.Document;

@Service
public class PhotosDownloadService {
	
	@Autowired
	CustomPictureController customPictureController;
	
	public byte[] getZipFiles(List<Document> documents) throws Exception {
		List<File> images = new ArrayList<>();
		
		for(int i=0; i<documents.size(); i++)
			images.addAll(base64ToFile(documents.get(i)));
		
		byte[] zip = zipFiles(images);
		return zip;
	}
	
	private byte[] zipFiles(List<File> images) throws IOException {
		ByteArrayOutputStream boas = new ByteArrayOutputStream();
		ZipOutputStream zos = new ZipOutputStream(boas);
		byte[] bytes = new byte[2048];
		
		for(int i=0; i<images.size(); i++) {
			FileInputStream fis = new FileInputStream(images.get(i).getPath());
			BufferedInputStream bis = new BufferedInputStream(fis);
			
			String nome = images.get(i).getName() + ".jpeg";
			zos.putNextEntry(new ZipEntry(nome));
			Files.copy(images.get(i).toPath(), zos);
			
			int bytesRead;
			while((bytesRead = bis.read()) != -1)
				zos.write(bytes, 0, bytesRead);
			
			zos.closeEntry();
			bis.close();
			fis.close();
		}
		
		zos.flush();
		boas.flush();
		zos.close();
		boas.close();
		
		return boas.toByteArray();
	}
	
	private List<File> base64ToFile(Document document) throws Exception {
		List<File> images = new ArrayList<>();
		
		List<CustomPicture> customPicture = document.getPicsList();
		
		for(int i=0; i<customPicture.size(); i++) {
			if(customPicture.get(i) != null) {
				String nome = customPicture.get(i).getPicName();
				String base64 = customPictureController.downloadImage(nome);
				
				String nomeFile = createName(customPicture.get(i).getCustompictureId(), customPicture.get(i).getMessage(), document.getStartTime(), document.getUser().getName());
				File file = new File(nomeFile);
				
				BufferedOutputStream writer = new BufferedOutputStream(new FileOutputStream(file));
				writer.write(loadFileAsByteArray(base64));
				writer.flush();
				writer.close();
				images.add(file);
			}
			else 
				break;
		}
		
		return images;
	}
	
	private byte[] loadFileAsByteArray(String base64) throws Exception {
		String newBase64 = base64.substring(base64.indexOf(",") + 1);
		byte[] decodedString = java.util.Base64.getDecoder().decode(newBase64.getBytes());
		return decodedString;
	}
	
	private String createName(long id, String messageImage, String data, String user) throws ParseException {
		String name = user + "_" + messageImage + "_" + formatData(data) + "_" + id;
		name = name.replace(" ", "_");
		return name;
	}
	
	private String formatData(String date) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Date formattedDate = format.parse(date);
		
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(formattedDate);
		
		String returnedDate = calendar.get(Calendar.DAY_OF_MONTH) + "-" + calendar.get(Calendar.MONTH) + "-" + calendar.get(Calendar.YEAR); 
		
		return returnedDate;
	}
}
