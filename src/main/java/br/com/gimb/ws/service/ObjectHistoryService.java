package br.com.gimb.ws.service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.ObjectHistory;
import br.com.gimb.ws.model.ObjectInventory;
import br.com.gimb.ws.model.User;
import br.com.gimb.ws.repository.ObjectHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ObjectHistoryService extends BaseService<ObjectHistory, Long> {

	@RepositoryClass(clazz = ObjectHistoryRepository.class)
	@Autowired
	private ObjectHistoryRepository objectHistoryRepository;
    
	public List<ObjectHistory> findAll(){
		return objectHistoryRepository.findAll();
	}

	public List<ObjectHistory> findByDate(String begin, String end){
		return objectHistoryRepository.findAllByDateTimeBetweenOrderByObjectAsc(begin,end);
	}

	public ObjectHistory findByObjectAndUserAndDatetime(ObjectInventory object, User user, String inicio, String fim){
		return objectHistoryRepository.findFirstByObjectAndUserAndDateTimeBetweenOrderByDateTimeDesc(object,user,inicio, fim);
	}
}