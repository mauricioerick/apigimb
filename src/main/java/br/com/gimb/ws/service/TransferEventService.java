package br.com.gimb.ws.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.TransferEvents;
import br.com.gimb.ws.repository.TransferEventRepository;

@Service
public class TransferEventService extends BaseService<TransferEvents, Long> {

	@RepositoryClass(clazz = TransferEventRepository.class)
	@Autowired
	private TransferEventRepository transferEventRepository;
}
