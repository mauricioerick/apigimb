package br.com.gimb.ws.service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.DTO.ChecklistServicesDTO;
import br.com.gimb.ws.model.ChecklistServices;
import br.com.gimb.ws.repository.ChecklistServiceRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ChecklistServicesService extends BaseService<ChecklistServices, Long> {

	@RepositoryClass(clazz = ChecklistServiceRepository.class)
	@Autowired
	private ChecklistServiceRepository checklistServiceRepository;

	public List<ChecklistServices> findAll() {
		return (List<ChecklistServices>) checklistServiceRepository.findAll();
	}

	public ChecklistServices findByChecklistId(Long id){
		return checklistServiceRepository.findById(id).orElse(null);
	}

	public List<ChecklistServices> findChecklistServicesByEquipmentId(long id) {
		return checklistServiceRepository.findByEquipmentEquipmentId(id);
	}

	public List<ChecklistServices> findByActive(String active) {
		if (active.equalsIgnoreCase("all"))
			return checklistServiceRepository.findAll();
		else
			return checklistServiceRepository.findByActive(active.equalsIgnoreCase("true"));
	}

	public List<ChecklistServicesDTO> getChecklistServicesAndCriticallyByServiceId(Long serviceId) {    	
		List<ChecklistServicesDTO> result = new ArrayList<>();

		List<Object[]> records = checklistServiceRepository.getChecklistServicesAndCriticallyByService(serviceId);    	
		for (Object[] objects : records) {
			ChecklistServicesDTO checkDTO = new ChecklistServicesDTO();
			checkDTO.setChecklistServicesId(objects[0].toString());
			checkDTO.setChecklistName(objects[1].toString());
			checkDTO.setCriticallyHigh(objects[2].toString());
			checkDTO.setCriticallyMedium(objects[3].toString());
			checkDTO.setCriticallyLow(objects[4].toString());
					
			result.add(checkDTO);
		}

    	return result;
    }

	public List<ChecklistServicesDTO> getChecklistServicesAndCriticallyByServiceIdTotal(Long serviceId) {    	
		List<ChecklistServicesDTO> result = new ArrayList<>();

		List<Object[]> records = checklistServiceRepository.getChecklistServicesAndCriticallyByServiceTotal(serviceId);    	
		for (Object[] objects : records) {
			ChecklistServicesDTO checkDTO = new ChecklistServicesDTO();
			checkDTO.setChecklistServicesId(objects[0].toString());			
			checkDTO.setCriticallyHigh(objects[1].toString());
			checkDTO.setCriticallyMedium(objects[2].toString());
			checkDTO.setCriticallyLow(objects[3].toString());
					
			result.add(checkDTO);
		}

    	return result;
    }
}
