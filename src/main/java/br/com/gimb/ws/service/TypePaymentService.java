package br.com.gimb.ws.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.TypePayment;
import br.com.gimb.ws.repository.TypePaymentRepository;

@Service
public class TypePaymentService extends BaseService<TypePayment, Long> {

	@RepositoryClass(clazz = TypePaymentRepository.class)
	@Autowired
	private TypePaymentRepository typePaymentRepository;
	
	public List<TypePayment> findAll() {
		return typePaymentRepository.findAll();
  }
    
  public List<TypePayment> findByActive(String active) {
		if (active.equalsIgnoreCase("all"))
		  return typePaymentRepository.findAll();
		else
		  return typePaymentRepository.findByActive(active.equalsIgnoreCase("true"));
	}
}