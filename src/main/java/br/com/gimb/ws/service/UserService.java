package br.com.gimb.ws.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.User;
import br.com.gimb.ws.repository.UserRepository;

@Service
public class UserService extends BaseService<User, Long> {

	@RepositoryClass(clazz = UserRepository.class)
	@Autowired
	private UserRepository userRepository;

	public User findByUserId(long userId) {
		return userRepository.findByUserId(userId);
	}

	public User findByUser(String name) {
		return userRepository.findByUser(name);
	}

	public List<User> findByActive(String active) {
		if (active.equalsIgnoreCase("all"))
			return userRepository.findAll();
		else
			return userRepository.findByActive(active.equalsIgnoreCase("true"));
	}
}
