package br.com.gimb.ws.service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.Device;
import br.com.gimb.ws.repository.DeviceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeviceService extends BaseService<Device, Long> {

	@RepositoryClass(clazz = DeviceRepository.class)
	@Autowired
	private DeviceRepository deviceRepository;

	public Device findByDeviceId(long deviceId) {
		return deviceRepository.findByDeviceId(deviceId);
	}

	public Device findByGuid(String guid){
		return deviceRepository.findByGuid(guid).orElse(null);
	}

}
