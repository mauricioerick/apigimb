package br.com.gimb.ws.service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.enumerated.ReferenceTypeEnum;
import br.com.gimb.ws.enumerated.StatusExtractEnum;
import br.com.gimb.ws.enumerated.TypeExtractEnum;
import br.com.gimb.ws.model.Card;
import br.com.gimb.ws.model.Event;
import br.com.gimb.ws.model.Extract;
import br.com.gimb.ws.model.Settlement;
import br.com.gimb.ws.model.TypePayment;
import br.com.gimb.ws.model.User;
import br.com.gimb.ws.repository.ExtractRepository;
import br.com.gimb.ws.util.DateHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ExtractService extends BaseService<Extract, Long> {

    @RepositoryClass(clazz = ExtractRepository.class)
    @Autowired
    private ExtractRepository extractRepository;
    
    @Autowired
    private BalanceService balanceService;
    
    @Autowired
    private CardService cardService;
    
    @Autowired
    private CustomFieldService customFieldService;

    @Autowired
    private SettlementService settlementService;
    
    public List<Extract> findAllBetweenReleaseDate(String startDate, String endDate, User user, TypePayment typePayment) {
    	List<Extract> extracts = extractRepository.findAllBetweenReleaseDate(startDate, endDate, user, typePayment);
    	for (Extract extract : extracts) {
			if (extract.getSettlement() != null) {
				extract.getSettlement().setCustomFieldList(customFieldService.findByReferenceTypeAndReferenceId(ReferenceTypeEnum.LQ, String.valueOf(extract.getSettlement().getSettlementId())));
			}
		}
        return extracts;
    }
    
    public List<Extract> findAllBetweenReleaseDateCard(String starDate, String endDate, Long cardId, Long typePaymentId, Long userId){
    	return extractRepository.findAllBetweenReleaseDateCard(starDate,endDate,cardId,typePaymentId,userId);
    }
	
	public List<Extract> findAllPending(User user, Event event, Card card) {
        return extractRepository.findAllPending(user, event, card);
	}
    
    public Map<String, Double> sumAmountByStatus(Long userId, Long typePaymentId, String status) {
    	Map<String, Double> result = new HashMap<>();
    	List<Object[]> list = extractRepository.findSumAmountByStatus(userId, typePaymentId, status);
    	for (Object[] objects : list) {
    		String user = objects[0].toString();
    		String typePayment = objects[1].toString();
    		Double amount = Double.valueOf(objects[2].toString());
    		
    		result.put(String.format("%s.%s", user, typePayment), amount);
		}
    	
        return result;
    }
    
    public Map<String, String> lastCreditDate(Long userId, Long typePaymentId) {
    	Map<String, String> result = new HashMap<>();
    	List<Object[]> list = extractRepository.findLastCreditDate(userId, typePaymentId);
    	for (Object[] objects : list) {
    		String user = objects[0].toString();
    		String typePayment = objects[1].toString();
    		String date = objects[2].toString();
    		
    		result.put(String.format("%s.%s", user, typePayment), date);
		}
    	
        return result;
    }
    
    public List<Object[]> sumAmountGroupByDateFromDate(Long userId, Long typePaymentId, Long cardId, String date, String status) {
    	return extractRepository.findSumAmountGroupByDateFromDate(userId, typePaymentId, cardId, date, status);
    }
    
    public void createExtractBySettlement(Settlement settlement) throws Exception {
    	Extract extract = Extract.builder();
    	
		try {
			extract
				.setReleaseDate	(settlement.getDate())
    			.setType		(TypeExtractEnum.D.getCode())
    			.setAmount		(settlement.getAmount())
    			.setSettlement	(settlement)
    			.setTypePayment	(settlement.getTypePayment())
    			.setStatus		(StatusExtractEnum.P.getCode())
    			.setCreateDate	(DateHelper.nowDateTimeFormat())
    			.setCreateUser	(settlement.getUser())
				.setUser		(settlement.getCard() == null ? settlement.getUser(): null)
				.setCard		(settlement.getCard());

			save(extract);
			extract.setSettlement(null);
			settlement.setExtract(extract);
			
		} catch (Exception e) {
			delete(extract);
			throw new Exception();
		}
    	
	}
	
	@Override
	public <S> Extract save(Extract extract) {
		try {
			super.save(extract);
		
			balanceService.reprocessBalanceFrom(extract.getUser(), extract.getTypePayment(), extract.getCard(), extract.getReleaseDate());
		} catch (Exception e) {
			delete(extract);
			e.printStackTrace();
		}
		return extract;
	}

	public Extract findBySettlement(Settlement settlement) {
		return extractRepository.findBySettlement(settlement);
	}
    
}