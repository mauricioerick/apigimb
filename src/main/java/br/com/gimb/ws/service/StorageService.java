package br.com.gimb.ws.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.Storage;
import br.com.gimb.ws.repository.StorageRepository;

@Service
public class StorageService extends BaseService<Storage, Long> {

	@RepositoryClass(clazz = StorageRepository.class)
	@Autowired
	private StorageRepository storageRepository;
    
	public List<Storage> findByActive(String active) {
		if (active.equalsIgnoreCase("all"))
			return storageRepository.findAll();
		else
			return storageRepository.findByActive(active.equalsIgnoreCase("true"));
	}
    
  public List<Storage> findAll(){
		return storageRepository.findAll();
	}
}