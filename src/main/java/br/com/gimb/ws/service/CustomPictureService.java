package br.com.gimb.ws.service;

import java.io.IOException;
import java.util.List;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.services.s3.AmazonS3;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.enumerated.ReferenceTypeEnum;
import br.com.gimb.ws.model.CustomPicture;
import br.com.gimb.ws.repository.CustomPictureRepository;
import br.com.gimb.ws.util.aws.AwsS3Utils;

@Service
public class CustomPictureService extends BaseService<CustomPicture, Long>{
	
	@RepositoryClass(clazz = CustomPictureRepository.class)
	@Autowired
	private CustomPictureRepository customPictureRepository;
	
	public List<CustomPicture> findByReferenceTypeAndReferenceId(ReferenceTypeEnum referencetype, String referenceId) {
		return customPictureRepository.findByReferenceTypeAndReferenceId(referencetype, referenceId);
	}
	
	public CustomPicture convertBase64ToImage(CustomPicture customPicture, int imgIdx, AmazonS3 s3Client, String baseName) throws IOException {
		String nameImg = "IMG_CP_" + customPicture.getReferenceType().getCode() + "_" + customPicture.getReferenceId() + "_" + imgIdx + ".jpg";
		byte[] data = Base64.decodeBase64(customPicture.getPic());
		
		customPicture.setPicMime("jpg");
		customPicture.setPicName(nameImg.replace(customPicture.getPicMime(), "").replace(".", ""));
		customPicture.setPicPath(nameImg);
		
		AwsS3Utils.uploadImage(s3Client, data, customPicture.getPicName(), baseName, "jpg");
		
		return customPicture;
	}
	
	@Override
	public void delete(CustomPicture customPicture) {
		customPictureRepository.delete(customPicture);
		return;
	}
	
}
