package br.com.gimb.ws.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.ToolServicesPics;
import br.com.gimb.ws.repository.ToolServicePicsRepository;

@Service
public class ToolServicePicsService extends BaseService<ToolServicesPics, Long> {
    @RepositoryClass(clazz = ToolServicePicsRepository.class)
	@Autowired
	private ToolServicePicsRepository toolServicePicsRepository;
    
}
