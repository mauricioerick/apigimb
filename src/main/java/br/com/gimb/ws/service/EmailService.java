package br.com.gimb.ws.service;

import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.ws.enumerated.EmailDomainEnum;
import br.com.gimb.ws.model.User;

@Service
public class EmailService {

	@Autowired
	private UserService userService;
	
	@Autowired
	private EmailConfigService emailConfigService;
	
	private Session getMailSession(Map<String, String> sender) {
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", sender.get("smtp_host"));
		props.put("mail.smtp.port", sender.get("smtp_port"));

		return Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(sender.get("user"), sender.get("pass"));
			}
		});
	}
	
	private String buildMailContent(EmailDomainEnum domain, String content, Map<String, String> object) {
		if (domain.equals(EmailDomainEnum.REQUEST_CREDIT)) {
			content = content
					.replace("{user}", object.get("user") != null ? object.get("user") : "")
					.replace("{typePayment}", object.get("typePayment") != null ? object.get("typePayment") : "")
					.replace("{date}", object.get("date") != null ? object.get("date") : "")
					.replace("{amount}", object.get("amount") != null ? object.get("amount") : "")
					.replace("{reason}", object.get("reason") != null ? object.get("reason") : "");
		}
		return content;
	}
		
	public void sendmail(EmailDomainEnum domain, Map<String, String> object) throws AddressException, MessagingException, IOException {
		Map<String, String> requestCredit = emailConfigService.getRequestCredit();
		Map<String, String> sender = emailConfigService.getSenderMap();
		
		String mailCopy = null;
		if (object.containsKey("userId")) {
			try {
				String userId = object.get("userId");
				User user = userService.findByUserId(Long.valueOf(userId));
				if (user != null && user.getMail() != null && user.getMail().trim().length() > 0) mailCopy = user.getMail();
			} catch (Exception e) {
				mailCopy = null;
			}
		}
		
		Session session = getMailSession(sender);
		Message msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress(sender.get("user"), false));
		msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(requestCredit.get("mail_to")));
		if (mailCopy != null) msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(mailCopy));
		msg.setSubject(requestCredit.get("subject"));
		msg.setContent(buildMailContent(domain, requestCredit.get("body"), object), "text/html");
		msg.setSentDate(new Date());
		Transport.send(msg);
	}

}
