package br.com.gimb.ws.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.enumerated.AppearsOnEnum;
import br.com.gimb.ws.model.Event;
import br.com.gimb.ws.repository.EventRepository;
import br.com.gimb.ws.util.Util;

@Service
public class EventService extends BaseService<Event, Long> {

	@RepositoryClass(clazz = EventRepository.class)
	@Autowired
	private EventRepository eventRepository;

	public List<Event> findByDescription(String description) {
		return eventRepository.findByDescription(description);
	}
	
	public void prepareAppearsOn(Event event, String language) {
		if (event.getAppearsOnList() != null && event.getAppearsOnList().length > 0) {
			String s = String.join(",", event.getAppearsOnList());
			s = s.replace(Util.lblBundle(language, null, AppearsOnEnum.OS.getDescription()), AppearsOnEnum.OS.getCode()).replace(AppearsOnEnum.OS.getDescription(), AppearsOnEnum.OS.getCode());
			s = s.replace(Util.lblBundle(language, null, AppearsOnEnum.LQ.getDescription()), AppearsOnEnum.LQ.getCode()).replace(AppearsOnEnum.LQ.getDescription(), AppearsOnEnum.LQ.getCode());
			
			event.setAppearsOn(s);
		} else {
			event.setAppearsOn(null);
		}
	}

	public List<Event> findByActive(String active) {
		if (active.equalsIgnoreCase("all"))
			return eventRepository.findAll();
		else
			return eventRepository.findByActive(active.equalsIgnoreCase("true"));
	}
	
	public List<Event> findAll(){
		return (List<Event>) eventRepository.findAll();
	}
}
