package br.com.gimb.ws.service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.*;
import br.com.gimb.ws.repository.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServiceService extends BaseService<Services, Long> {

	@RepositoryClass(clazz = ServiceRepository.class)
	@Autowired
	private ServiceRepository serviceRepository;

	@Autowired
	private PicService picService;

	@Autowired
	private ServiceEventService serviceEventService;

	@Autowired
	private ServiceInterruptionService serviceInterruptionService;

	@Override
	public List<Services> findAll() {
		return serviceRepository.findAll();
	}

	public List<Services> findByDateBetween(String startDate, String endDate, User user) {
		return serviceRepository.findAllByStartTimeLessThanEqualAndStartTimeGreaterThanEqual(startDate, endDate, user);
	}

	public List<Services> findByUserClientAndDateBetween(String user, String startDate, String endDate) {
		return serviceRepository.findAllByUserClientAndStartTimeLessThanEqualAndStartTimeGreaterThanEqual(user, startDate,
				endDate);
	}

	public List<Services> findByUser(User user) {
		return serviceRepository.findByUser(user);
	}

	public List<Services> findByUserAndStatus(User user, String status) {
		return serviceRepository.findByUserAndStatus(user, status);
	}

	public List<Services> findByProject(Project project){
		return serviceRepository.findByProject(project);
	}

	public Services findByUserAndClientAndVehicleAndStartTimeAndEndDateAndAction(String startDate, String endDate, Long user, Long client, 
			Long vehicle, Long action) {

		return serviceRepository.findByUserAndClientAndVehicleAndStartTimeAndEndDateAndAction(startDate, endDate, user, client, vehicle, action);
	}

	@Override
	public void delete(Services service) {
		picService.deleteAllByService(service);
		serviceEventService.deleteAllByService(service);
		serviceInterruptionService.deleteAllByService(service);
		super.delete(service);
		return;
	}

	public List<Services> findByUserAndClientAndVehicleAndStartTime(User user, Client client, Vehicle vehicle,
			String startTime) {
		return serviceRepository.findByUserAndClientAndVehicleAndStartTime(user, client, vehicle, startTime);
	}
}
