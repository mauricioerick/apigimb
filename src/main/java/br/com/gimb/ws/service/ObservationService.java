package br.com.gimb.ws.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.enumerated.AppearsOnEnum;
import br.com.gimb.ws.model.Observation;
import br.com.gimb.ws.repository.ObservationRepository;
import br.com.gimb.ws.util.Util;

@Service
public class ObservationService extends BaseService<Observation, Long> {

    @RepositoryClass(clazz = ObservationRepository.class)
	@Autowired
    private ObservationRepository observationRepository;
    
    public List<Observation> findByActive(String active) {
		if (active.equalsIgnoreCase("all"))
			return observationRepository.findAll();
		else
			return observationRepository.findByActive(active.equalsIgnoreCase("true"));
    }

    public void prepareAppearsOn(Observation obs, String language) {
		if (obs.getAppearsOnList() != null) {
			String s = String.join(",", obs.getAppearsOnList());
			s = s.replace(Util.lblBundle(language, null, AppearsOnEnum.IV.getDescription()), AppearsOnEnum.IV.getCode()).replace(AppearsOnEnum.IV.getDescription(), AppearsOnEnum.IV.getCode());
			
			obs.setAppearsOn(s);
		}
	}
    
    public List<Observation> findAll(){
		return observationRepository.findAll();
	}
}