package br.com.gimb.ws.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.TableParameter;
import br.com.gimb.ws.repository.TableParameterRepository;

@Service
public class TableParameterService extends BaseService<TableParameter, Long> {

	@RepositoryClass(clazz = TableParameterRepository.class)
	@Autowired
	private TableParameterRepository tableParameterRepository;
	
	public List<TableParameter> findAll() {
		return tableParameterRepository.findAll();
	}
	
	public TableParameter findByTableParameterId(long tableParameterId) {
		return tableParameterRepository.findByTableParameterId(tableParameterId);
	}
	
	public List<TableParameter> findByTableReference(String tableReference) {
		return tableParameterRepository.findByTableReference(tableReference);
	}
	
	public TableParameter findByTableReferenceActive(String tableReference) {
		List<TableParameter> list = tableParameterRepository.findByTableReferenceAndActive(tableReference, true);
		if (list != null && list.size() > 0)
			return list.get(0);
		
		return null;
	}
	
	public List<TableParameter> findByActive(Boolean active) {
		return tableParameterRepository.findByActive(active);
	}

}
