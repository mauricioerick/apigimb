package br.com.gimb.ws.service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.Agenda;
import br.com.gimb.ws.model.AgendaItem;
import br.com.gimb.ws.repository.AgendaItemRepository;
import br.com.gimb.ws.repository.AgendaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AgendaItemService extends BaseService<AgendaItem, Long> {

    @RepositoryClass(clazz = AgendaItemRepository.class)
    @Autowired
    private AgendaItemRepository agendaItemRepository;

    public List<AgendaItem> saveAll(List<AgendaItem> agendaItems) {
        return agendaItems.stream().map(this::save).collect(Collectors.toList());
    }

}
