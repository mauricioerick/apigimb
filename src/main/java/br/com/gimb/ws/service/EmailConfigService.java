package br.com.gimb.ws.service;

import java.io.IOException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.EmailConfig;
import br.com.gimb.ws.repository.EmailConfigRepository;

@Service
public class EmailConfigService extends BaseService<EmailConfig, Long> {

	@RepositoryClass(clazz = EmailConfigRepository.class)
	@Autowired
	private EmailConfigRepository emailConfigRepository;

	public EmailConfig getEmailConfig() {
		return emailConfigRepository.findByEmailConfigId(1l);
	}
	
	public Map<String, String> getSenderMap() {
			try {
				if (getEmailConfig().getSender() != null) 
					return new ObjectMapper().readValue(getEmailConfig().getSender(), new TypeReference<Map<String, String>>(){});
			} catch (IOException e) {
				e.printStackTrace();
			}
		
		return null;
	}

	public Map<String, String> getRequestCredit() {
		try {
			if (getEmailConfig().getRequestCredit() != null) 
				return new ObjectMapper().readValue(getEmailConfig().getRequestCredit(), new TypeReference<Map<String, String>>(){});
		} catch (IOException e) {
			e.printStackTrace();
		}
	
		return null;
	}

}
