package br.com.gimb.ws.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.Expense;
import br.com.gimb.ws.repository.ExpenseRepository;
import br.com.gimb.ws.util.DateHelper;
import br.com.gimb.ws.util.HashUtils;

@Service
public class ExpenseService extends BaseService<Expense, Long>{

	@RepositoryClass(clazz = ExpenseRepository.class)
	@Autowired
	private ExpenseRepository expenseRepository;
	
	@Autowired
	private ExpenseItemService expenseItemService;
	
	public Expense findByHash(String hash) {
		List<Expense> list = expenseRepository.findByHash(hash);
		return (list != null && list.size() > 0) ? list.get(0) : null;
	}
	
	public List<Expense> findByStartDateBetween(String startDate, String endDate) {
		return expenseRepository.findByStartDateBetween(startDate, endDate);
	}
	
	@Override
	public Expense save(Expense expense) {
		if (expense.getExpenseId() == 0) {
			expense.setHash(HashUtils.md5FromString(DateHelper.nowDateTimeFormat()));
		}
		else {
			this.expenseItemService.save(expense);
		}
		expense = super.save(expense);
		return expense;
	}
	
	public void delete(Expense expense) {
		if (expense.getItems() != null)
			this.expenseItemService.delete(expense.getItems());
		
		expenseRepository.delete(expense);
	}
}
