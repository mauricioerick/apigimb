package br.com.gimb.ws.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.AreaUser;
import br.com.gimb.ws.repository.AreaUserRepository;

@Service
public class AreaUserService extends BaseService<AreaUser, Long> {
	@RepositoryClass(clazz = AreaUserRepository.class)
	@Autowired
	private AreaUserRepository areaUserRepository;
	
	public List<AreaUser> findAll(){
		return areaUserRepository.findAll();
	}
	
	public List<AreaUser> findByUserId(long id){
		return areaUserRepository.findByUserId(id);
	}
	
	public List<AreaUser> findByAreaId(long id){
		return areaUserRepository.findByAreaId(id);
	}
}
