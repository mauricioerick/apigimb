package br.com.gimb.ws.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.Client;
import br.com.gimb.ws.model.Vehicle;
import br.com.gimb.ws.repository.VehicleRepository;

@Service
public class VehicleService extends BaseService<Vehicle, Long>{
	
	@RepositoryClass(clazz = VehicleRepository.class)
	@Autowired
	private VehicleRepository vRepository;
	
	public List<Vehicle> findVehicleByClientId(Client client) {
		return vRepository.findByClient(client);
	}

	public List<Vehicle> findVehiclesEquipmentNull(Client client) {
		return vRepository.findByEquipmentNull(client);
	}
}
