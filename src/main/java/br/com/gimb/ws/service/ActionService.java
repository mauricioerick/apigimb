package br.com.gimb.ws.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.Action;
import br.com.gimb.ws.repository.ActionRepository;

@Service
public class ActionService extends BaseService<Action, Long> {

	@RepositoryClass(clazz = ActionRepository.class)
	@Autowired
	private ActionRepository actionRepository;

	public List<Action> findByDescription(String description) {
		return actionRepository.findByDescription(description);
	}

	public List<Action> findAll() {
		return (List<Action>) actionRepository.findAll();
	}

	public List<Action> findByActive(String active) {
		if (active.equalsIgnoreCase("all"))
			return actionRepository.findAll();
		else
			return actionRepository.findByActive(active.equalsIgnoreCase("true"));
	}
}
