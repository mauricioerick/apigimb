package br.com.gimb.ws.service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.DTO.TimeLineFeedDTO;
import br.com.gimb.ws.enumerated.PermissionWebEnum;
import br.com.gimb.ws.enumerated.TimeLineTypeEnum;
import br.com.gimb.ws.model.*;
import br.com.gimb.ws.repository.DashboardsRepository;
import br.com.gimb.ws.repository.TransferRepository;
import br.com.gimb.ws.util.DateHelper;
import br.com.gimb.ws.util.PermissionsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class DashboardsService extends BaseService<User, Long>{

	@RepositoryClass(clazz = DashboardsRepository.class)
	@Autowired
	private DashboardsRepository dashboardRepository;
	
	@Autowired
	private TransferRepository transferRepository;
	
	@Autowired
	private UserService userService;

	@Autowired
	private AreaService areaService;
	
	public List<TimeLineFeedDTO> findTimeLineFeedsByDateBetween(Boolean groupByDate, String startDate, String endDate, List<String> client, List<String> user, Long userLogadoId) {
		
		List<TimeLineFeedDTO> feeds = new ArrayList<>();
		
		User userFilter = userService.findById(userLogadoId);
		User userFiltertransfer = null;
		
		if(PermissionsUtil.getPermissionValue(userFilter.getProfile(), PermissionWebEnum.filtrarServicoUsuario).equals("1")) {
			List<String> l = new ArrayList<>();
			l.add(userFilter.getUser());
			user.addAll(l); // caso filtre por usuario, muda o parametro selecionado na tela pelo usuario logado
		}
		else if (PermissionsUtil.getPermissionValue(userFilter.getProfile(), PermissionWebEnum.filtrarServicoArea).equals("1")){
			user.addAll(areaService.findUsersByArea(userFilter).stream().map(areaUser -> areaUser.getUser()).collect(Collectors.toList()));
		}
		
		if(PermissionsUtil.getPermissionValue(userFilter.getProfile(), PermissionWebEnum.filtrarLiquidacaoUsuario).equals("1")) {
			List<String> l = new ArrayList<>();
			l.add(userFilter.getUser());
			user.addAll(l); // caso filtre por usuario, muda o parametro selecionado na tela pelo usuario logado
		} else if (PermissionsUtil.getPermissionValue(userFilter.getProfile(), PermissionWebEnum.filtrarLiquidacaoUsuario).equals("1")){
			user.addAll(areaService.findUsersByArea(userFilter).stream().map(areaUser -> areaUser.getUser()).collect(Collectors.toList()));
		}
		
		if(PermissionsUtil.getPermissionValue(userFilter.getProfile(), PermissionWebEnum.filtrarApontamentoUsuario).equals("1"))
			userFiltertransfer = userFilter;
		
		feeds.addAll(dashboardRepository.findTimeLineFeedsServiceByDateBetween(startDate, endDate));
		feeds.addAll(dashboardRepository.findTimeLineFeedsSettlementByDateBetween(startDate, endDate));
		
		List<Transfer> transfers = transferRepository.findByDateBetween(startDate, endDate, userFiltertransfer);
		if (transfers != null) {
			if (user != null && user.size() > 0) {
				for(String u : user) {
					String usr = u;
					transfers = transfers.stream().filter(tra -> tra.getUser().getUser().equalsIgnoreCase(usr)).collect(Collectors.toList());
				}
			}
			
			for (Transfer transfer : transfers) {
				try {
					String endTime = DateHelper.getTimeFromDateTimeString(transfer.getEndTime());
					String timeElapsed = transfer.getTimeElapsed().toUpperCase().substring(0,  transfer.getTimeElapsed().length() - 3);
					String note = transfer.getNote() != null ? transfer.getNote() : "";

					String msg = String.format("ENCERRAMENTO DE PONTO: %s\n\t%s\n\t%s", endTime, timeElapsed, note);
					feeds.add(new TimeLineFeedDTO(transfer.getUser().getName(), msg, DateHelper.getDateFromDateTimeString(transfer.getStartTime()), DateHelper.getTimeFromDateTimeString(transfer.getStartTime()), transfer.getTransferId(), TimeLineTypeEnum.TRA.getCode(), transfer.getNote(), transfer.getClient().getTradingName()));
					for (TransferEvents event : transfer.getEventsList()) {
						try {
							String eventName = event.getEvent().getDescription();
							endTime = DateHelper.getTimeFromDateTimeString(event.getEndTime());
							timeElapsed = event.getTimeElapsed().toUpperCase().substring(0,  event.getTimeElapsed().length() - 3);
							note = event.getNote() != null ? event.getNote() : "";
							Double kmStart = event.getKmStart();
							Double kmEnd = event.getKmEnd();
							Double kmTraveled = event.getKmTraveled();

							msg = String.format("%s\n\tENCERRAMENTO DE EVENTO: %s\n\t%s\n\tKM INICIO: %.2f\n\tKM FINAL: %.2f\n\tKM PERCORRIDO: %.2f\n\t%s", eventName, endTime, timeElapsed, kmStart, kmEnd, kmTraveled, note);
							feeds.add(new TimeLineFeedDTO(transfer.getUser().getName(), msg, DateHelper.getDateFromDateTimeString(event.getStartTime()), DateHelper.getTimeFromDateTimeString(event.getStartTime()), transfer.getTransferId(), TimeLineTypeEnum.TEV.getCode(), event.getNote(), ""));
						} catch (Exception e) {
							// Do nothing
						}
					}					
				} catch (Exception e) {
					// Do nothing
				}
			}
		}
		
		if(user.size() > 0 || client.size() > 0)
			feeds = filterFeeds(feeds, user, client);
		
		if (groupByDate)
			feeds = groupFeedsByDate(feeds);
		
		feeds.sort(new Comparator<TimeLineFeedDTO>() {
			@Override
			public int compare(TimeLineFeedDTO o1, TimeLineFeedDTO o2) {
				int ret = o1.getUser().compareToIgnoreCase(o2.getUser());
				if (ret != 0) return ret;
				
				ret = o1.getDate().compareToIgnoreCase(o2.getDate());
				if (ret != 0) return ret;
				
				ret = o1.getTime().compareToIgnoreCase(o2.getTime());
				
				return ret;
			}
		});
		
		return feeds;
	}
	
	private List<TimeLineFeedDTO> filterFeeds(List<TimeLineFeedDTO> tmpFeeds, List<String> user, List<String>  client) {
		List<TimeLineFeedDTO> feeds = new ArrayList<TimeLineFeedDTO>();
		
		for( TimeLineFeedDTO timeLine : tmpFeeds) {
			feeds.add(timeLine);
			boolean removeuElemento = false;
			
			if (client.size() > 0) {
				int idx = 1;
				for( String c : client) {
					if(timeLine.getClient().equalsIgnoreCase(c)) break;
					if(!timeLine.getClient().equalsIgnoreCase(c) && idx == client.size()) {
						int indexTimeLine = feeds.indexOf(timeLine);
						feeds.remove(indexTimeLine);
						removeuElemento = true;
					}
					idx ++;
				}
			}
			
			if (user.size() > 0 && !removeuElemento) {
				int idx = 1;
				for( String u : user) {
					if(timeLine.getUser().equalsIgnoreCase(u)) break;
					if(!timeLine.getUser().equalsIgnoreCase(u) && idx == user.size()) {
						int indexTimeLine = feeds.indexOf(timeLine);
						feeds.remove(indexTimeLine);
						removeuElemento = true;
					}
					idx ++;
				}
			}
		}
		
		return feeds;
	}
	
	private List<TimeLineFeedDTO> groupFeedsByDate(List<TimeLineFeedDTO> tmpFeeds) {
		List<TimeLineFeedDTO> feeds = new ArrayList<>();

		Map<String, Map<String, Integer>> map = new HashMap<>();
		for (TimeLineFeedDTO feed : tmpFeeds) {
			String key = feed.getDate();
			Map<String, Integer> data = map.get(key);
			if (data == null) {
				data = new HashMap<>();
				
				for (TimeLineTypeEnum type : TimeLineTypeEnum.values())
					data.put(type.getCode(), 0);
			}
			
			data.put(feed.getTimeLineType().getCode(), data.get(feed.getTimeLineType().getCode()) + 1);
			map.put(key, data);
		}
		
		for (String keyDate : map.keySet()) {
			String message = "";
			for (String keyType : map.get(keyDate).keySet()) {
				int q = map.get(keyDate).get(keyType).intValue();
				
				if (q == 0) continue;
				
				if (keyType.equalsIgnoreCase(TimeLineTypeEnum.TRA.getCode()))
					message += String.format("Registros de ponto: %d\n", q);
				else if (keyType.equalsIgnoreCase(TimeLineTypeEnum.SER.getCode()))
					message += String.format("Ordens de serviço: %d\n", q);
				else if (keyType.equalsIgnoreCase(TimeLineTypeEnum.SET.getCode()))
					message += String.format("Liquidações: %d\n", q);
			}
			
			feeds.add(new TimeLineFeedDTO(message, keyDate, TimeLineTypeEnum.GEN));
		}
		
		return feeds;
	}
}
