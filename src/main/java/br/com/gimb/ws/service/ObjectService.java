package br.com.gimb.ws.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.ObjectInventory;
import br.com.gimb.ws.repository.ObjectRepository;

@Service
public class ObjectService extends BaseService<ObjectInventory, Long> {

    @RepositoryClass(clazz = ObjectRepository.class)
	@Autowired
    private ObjectRepository objectRepository;
    
    public List<ObjectInventory> findByActive(String active) {
		if (active.equalsIgnoreCase("all"))
			return objectRepository.findAll();
		else
			return objectRepository.findByActive(active.equalsIgnoreCase("true"));
    }
    
    public List<ObjectInventory> findAll(){
		return objectRepository.findAll();
	}
    
    public ObjectInventory findByIdentifier(String identifier) {
    	return objectRepository.findByIdentifier(identifier);
	}

	public ObjectInventory findByIdentifierOrSerialNumberOrPatrimony(String identifier, String serialNumber, String patrimony) {
		return objectRepository.findByIdentifierOrSerialNumberOrPatrimony(identifier, serialNumber, patrimony);
	}
}