package br.com.gimb.ws.service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.DTO.AgendaDTO;
import br.com.gimb.ws.model.Agenda;
import br.com.gimb.ws.repository.ActionRepository;
import br.com.gimb.ws.repository.AgendaRepository;
import br.com.gimb.ws.requests.RequestAgenda;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class AgendaService extends BaseService<Agenda, Long> {

    @RepositoryClass(clazz = AgendaRepository.class)
    @Autowired
    private AgendaRepository agendaRepository;

    public List<AgendaDTO> listByAgendaWithJoin(RequestAgenda requestAgenda) throws ParseException {
        return agendaRepository.listByAgendaWithJoin(
                requestAgenda.getDtInicialBySearch(), requestAgenda.getDtFinalBySearch(),
                requestAgenda.getStatusBySearch(), requestAgenda.getUserId()
        );
    }

    public Optional<Agenda> findByUserIdAndDate(Agenda agenda) {
        return agendaRepository.findByUserIdAndDate(agenda.getUserId(), agenda.getDate());
    }

    public void deleteById(Long id) {
        agendaRepository.deleteById(id);
    }

}
