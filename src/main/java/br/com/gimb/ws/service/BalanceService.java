package br.com.gimb.ws.service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.DTO.BalanceDTO;
import br.com.gimb.ws.enumerated.StatusExtractEnum;
import br.com.gimb.ws.model.Balance;
import br.com.gimb.ws.model.Card;
import br.com.gimb.ws.model.TypePayment;
import br.com.gimb.ws.model.User;
import br.com.gimb.ws.repository.BalanceRepository;
import br.com.gimb.ws.util.DateHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class BalanceService extends BaseService<Balance, Long> {

	@RepositoryClass(clazz = BalanceRepository.class)
	@Autowired
	private BalanceRepository balanceRepository;

	@Autowired
	private ExtractService extractService;

	@Override
	public <S> Balance save(Balance balance) {
		Balance persisted = this.balanceByUserAndTypePaymentAndDateEqual(balance.getUserId() <= 0 ? null : balance.getUserId(), balance.getTypePaymentId(),
				balance.getDate(), balance.getCard() != null ? balance.getCard().getCardId() : null);
			
		if (persisted != null)
			balance.setBalanceId(persisted.getBalanceId());

		super.save(balance);
		
		return balance;
	}

	public List<Balance> balanceByUserAndTypePayment(Long userId, Long typePaymentId, Long card) {
		List<Balance> result = new ArrayList<>();

		List<Object[]> records = balanceRepository.findLastBalanceByUserAndTypePayment(userId, typePaymentId, card);
		for (Object[] objects : records) {
			Long user = null;
			if(objects[0] != null)
				user = Long.valueOf(objects[0].toString());
			
			Long typePayment = Long.valueOf(objects[1].toString());
			String date = DateHelper.dateJsonFormatToDateFormat(String.valueOf(objects[2]));
			Long cardId =  null;
			
			if(objects[3] != null)
				cardId =  Long.valueOf(objects[3].toString());
					
			result.add(balanceByUserAndTypePaymentAndDateEqual(user, typePayment, date, cardId));
		}
		return result;
	}

	public Balance balanceByUserAndTypePaymentAndDateEqual(Long userId, Long typePaymentId, String date, Long cardId) {
		return balanceRepository.findAllByUserAndTypePaymentAndDate(userId, typePaymentId, date, cardId);
	}
	
	public List<Balance> balanceByUserAndTypePaymentAndDateEqualThanGreater(Long userId, Long typePaymentId,
			String date) {
		return balanceRepository.findAllByUserAndTypePaymentAndDateEqualThanGreater(userId, typePaymentId, date);
	}

	public void reprocessBalanceFrom(User user, TypePayment typePayment, Card card, String date) throws Exception {
		try {
			List<Object[]> list = extractService.sumAmountGroupByDateFromDate((user != null ? user.getUserId() : null), typePayment.getTypePaymentId(),
					(card != null ? card.getCardId() : null), date, StatusExtractEnum.A.getCode());

			Double priorBalance = balanceRepository.findPriorBalance(user != null? user.getUserId() : null, typePayment.getTypePaymentId(), date, card != null ? card.getCardId() : null);
			if (priorBalance == null)
				priorBalance = 0d;


			for (Object[] objects : list) {
				String releaseDate = objects[0].toString();
				Double balanceAmount = Double.valueOf(objects[1].toString());

				Balance balance = Balance.builder().setDay(DateHelper.getDayFrom(releaseDate))
						.setMonth(DateHelper.getMonthFrom(releaseDate)).setYear(DateHelper.getYearFrom(releaseDate)).setUser(user)
						.setTypePayment(typePayment).setCard(card).setBalance(BigDecimal.valueOf(Double.sum(balanceAmount, priorBalance))
								.setScale(2, RoundingMode.HALF_UP).doubleValue());
				
				save(balance);

				priorBalance = new Double(balance.getBalance());
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<BalanceDTO> convertBalanceToDTO(List<Balance> balances) {
		List<BalanceDTO> result = new ArrayList<>();

		Map<String, Double> pendingValues = extractService.sumAmountByStatus(null, null, StatusExtractEnum.P.getCode());
		Map<String, Double> reprovedValues = extractService.sumAmountByStatus(null, null, StatusExtractEnum.R.getCode());
		Map<String, String> lastCreditDates = extractService.lastCreditDate(null, null);

		if (balances != null) {
			for (Balance balance : balances) {
				try {
					String key = String.format("%s.%s", balance.getUserId().toString(), balance.getTypePaymentId().toString());

					Double pendingBalance = pendingValues.get(key);
					if (pendingBalance == null)
						pendingBalance = 0d;
					Double reprovedBalance = reprovedValues.get(key);
					if (reprovedBalance == null)
						reprovedBalance = 0d;
					String lastCreditDate = lastCreditDates.get(key);
					if (lastCreditDate != null)
						lastCreditDate = DateHelper
								.dateFormat(DateHelper.stringToDate(lastCreditDate, DateHelper.DATE_JSON_FORMAT));

					BalanceDTO b = BalanceDTO.builder().setObjBalance(balance).setBalance(balance.getBalance())
					.setPendingBalance(pendingBalance).setLastCreditDate(lastCreditDate).setReprovedBalance(reprovedBalance);
					b.setCard(balance.getCard());
					
					result.add(b);
				} catch (Exception e) {
					// do nothing
				}
			}
		}
		return result;
	}

}
