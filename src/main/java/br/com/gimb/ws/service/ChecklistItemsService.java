package br.com.gimb.ws.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.ChecklistItems;
import br.com.gimb.ws.repository.ChecklistItemsRepository;

@Service
public class ChecklistItemsService extends BaseService<ChecklistItems, Long> {
    
    @RepositoryClass(clazz = ChecklistItemsRepository.class)
	@Autowired
	private ChecklistItemsRepository checklistItemsRepository;

	public List<ChecklistItems> findAll() {
		return (List<ChecklistItems>) checklistItemsRepository.findAll();
	}

	public ChecklistItems findByChecklistId(Long id){
		return checklistItemsRepository.findById(id).orElse(null);
	}

	public List<ChecklistItems> findChecklistItemsById(Long checklistServicesId) {
		return checklistItemsRepository.findChecklistItemsById(checklistServicesId);
	}
}
