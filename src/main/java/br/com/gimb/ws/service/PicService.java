package br.com.gimb.ws.service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.ChecklistItems;
import br.com.gimb.ws.model.ServiceImage;
import br.com.gimb.ws.model.Services;
import br.com.gimb.ws.repository.PicRepository;
import br.com.gimb.ws.util.aws.AwsS3Utils;
import com.amazonaws.services.s3.AmazonS3;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service	
public class PicService extends BaseService<ServiceImage, Long> {
	
	@RepositoryClass(clazz = PicRepository.class)
	@Autowired
	private PicRepository picRepository;
	
	@Override
	public void delete(ServiceImage picImage) {
		picRepository.delete(picImage);
		return;
	}
	
	public void deleteAllByService(Services service) {
		if (service.getPicsList() != null && service.getPicsList().size() > 0) {
			for (ServiceImage pic : service.getPicsList()) {
				pic.setServico(null);
				picRepository.delete(pic.getPicId());	
			}
		}
	}
	
	public void deleteNull() {
		picRepository.deleteNull();
	}
	
	public ServiceImage convertBase64ToImage(Services service, int imgIdx, ServiceImage servicePic, AmazonS3 s3Client, String baseName) throws IOException {
		ServiceImage retorno = new ServiceImage();
		
		if((servicePic.getPic() != null && servicePic.getPic().length() > 0) || (servicePic.getFixPic() != null && servicePic.getFixPic().length() > 0)) {
			if (servicePic.getPic() != null && servicePic.getPic().length() > 0) {
				String nameImg = "img_service_" + service.getServiceId() + "_" + service.getVehicle().getVehicleId() + "_" + imgIdx + "."+servicePic.getPicMime();
				
				retorno.setPicMime(servicePic.getPicMime());
				retorno.setPicName(nameImg.replace(retorno.getPicMime(), "").replace(".", ""));
				retorno.setPicPath("/imagens/" + nameImg);
				
				byte[] data = Base64.decodeBase64(servicePic.getPic());
				AwsS3Utils.uploadImage(s3Client, data, retorno.getPicName(), baseName, servicePic.getPicMime());
			}
			
			if (servicePic.getPicThumb() != null && servicePic.getPicThumb().length() > 0) {
				String nameImg = "img_service_thumb_" + service.getServiceId() + "_" + service.getVehicle().getVehicleId() + "_" + imgIdx + "."+servicePic.getPicThumbMime();
				
				retorno.setPicThumbMime(servicePic.getPicThumbMime());
				retorno.setPicThumbName(nameImg.replace(retorno.getPicThumbMime(), "").replace(".", ""));
				retorno.setPicThumbPath("/imagens/" + nameImg);
				
				byte[] data = Base64.decodeBase64(servicePic.getPicThumb());
				AwsS3Utils.uploadImage(s3Client, data, retorno.getPicThumbName(), baseName, servicePic.getPicThumbMime());				
			}
			
			if(servicePic.getFixPic() != null && servicePic.getFixPic().length() > 0) {
				String nameImg = "img_service_fix" + service.getServiceId() + "_" + service.getVehicle().getVehicleId() + "_" + imgIdx + "."+servicePic.getFixPicMime();
				
				retorno.setFixPicMime(servicePic.getFixPicMime());
				retorno.setFixPicName(nameImg.replace(retorno.getPicMime(), "").replace(".", ""));
				retorno.setFixPicPath("/imagens/" + nameImg);
				
				byte[] data = Base64.decodeBase64(servicePic.getFixPic());
				AwsS3Utils.uploadImage(s3Client, data, retorno.getFixPicName(), baseName, servicePic.getFixPicMime());
			}
		}
		
		retorno.setServico(service);
		retorno.setLatPic(servicePic.getLatPic());
		retorno.setLgtPic(servicePic.getLgtPic());
		retorno.setMessage(servicePic.getMessage());
		retorno.setNote(servicePic.getNote());
		retorno.setManually(servicePic.getManually());
		retorno.setStatusChecklist(servicePic.getStatusChecklist());
		retorno.setStatusFix(servicePic.getStatusFix());
		retorno.setCritically(servicePic.getCritically());
		retorno.setWork(servicePic.getWork());
		retorno.setTime(servicePic.getTime());
		retorno.setUser(servicePic.getUser());
		retorno.setChecklistItems(servicePic.getChecklistItems());
		
		return retorno;
	}
}
