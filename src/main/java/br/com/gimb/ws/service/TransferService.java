package br.com.gimb.ws.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.Transfer;
import br.com.gimb.ws.model.User;
import br.com.gimb.ws.repository.TransferRepository;

@Service
public class TransferService extends BaseService<Transfer, Long> {

	@RepositoryClass(clazz = TransferRepository.class)
	@Autowired
	private TransferRepository transferRepository;
	
	public List<Transfer> findByDateBetween(String startDate, String endDate, User user) {
		return transferRepository.findByDateBetween(startDate, endDate, user);
	}

	public List<Transfer> findByEndTimeIsNull(User user) {
		return transferRepository.findByUserAndEndTimeIsNull(user);
	}
}
