package br.com.gimb.ws.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.Expense;
import br.com.gimb.ws.model.ExpenseItem;
import br.com.gimb.ws.repository.ExpenseItemRepository;

@Service
public class ExpenseItemService extends BaseService<ExpenseItem, Long> {

	@RepositoryClass(clazz = ExpenseItemRepository.class)
	@Autowired
	private ExpenseItemRepository expenseItemRepository;

	public void save(Expense expense) {
		expenseItemRepository.deleteAll(expenseItemRepository.findByExpense(expense));
		if (expense.getItems() != null) {
			super.setRepository(expenseItemRepository);

			for (ExpenseItem item : expense.getItems()) {
				item.setExpenseItemId(0);
				item.setExpense(expense);
				super.save(item);
			}
		}
	}

	public void delete(List<ExpenseItem> listExpenseItem) {
		expenseItemRepository.deleteAll(listExpenseItem);
	}
}
