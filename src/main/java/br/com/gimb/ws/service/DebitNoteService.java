package br.com.gimb.ws.service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.DebitNote;
import br.com.gimb.ws.repository.DebitNoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class DebitNoteService extends BaseService<DebitNote, Long> {

	@RepositoryClass(clazz = DebitNoteRepository.class)
	@Autowired
	private DebitNoteRepository debitNoteRepository;

	public List<DebitNote> findAll() {
		return (List<DebitNote>) debitNoteRepository.findAll();
	}

	public HashMap<String, Long> getAllSeriesWithSequence(){
		HashMap<String, Long> toReturn = new HashMap<>();
		debitNoteRepository.getAllSeriesWithSequence().forEach( obj -> {
			toReturn.put((String)obj[0],(Long) obj[1]);
		});
		return toReturn;
	}
}
