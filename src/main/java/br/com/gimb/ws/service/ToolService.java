package br.com.gimb.ws.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.Tool;
import br.com.gimb.ws.repository.ToolRepository;

@Service
public class ToolService extends BaseService<Tool, Long> {

	@RepositoryClass(clazz = ToolRepository.class)
	@Autowired
	private ToolRepository toolRepository;

	public List<Tool> findByDescription(String description) {
		return toolRepository.findByDescription(description);
	}

	public List<Tool> findAll() {
		return (List<Tool>) toolRepository.findAll();
	}

	public List<Tool> findByActive(String active) {
		if (active.equalsIgnoreCase("all"))
			return toolRepository.findAll();
		else
			return toolRepository.findByActive(active.equalsIgnoreCase("true"));
	}
    
}
