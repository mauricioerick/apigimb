package br.com.gimb.ws.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.ServiceEvents;
import br.com.gimb.ws.model.Services;
import br.com.gimb.ws.repository.ServiceEventRepository;

@Service
public class ServiceEventService extends BaseService<ServiceEvents, Long> {

	@RepositoryClass(clazz = ServiceEventRepository.class)
	@Autowired
	private ServiceEventRepository serviceEventRepository;
	
	public List<ServiceEvents> findByService(Services service) {
		return serviceEventRepository.findByServico(service);
	}
	
	public void deleteAllByService(Services service) {
		if (service.getEventsList() != null && service.getEventsList().size() > 0) {
			for (ServiceEvents event : service.getEventsList()) {
				serviceEventRepository.deleteById(event.getServiceEventsId());
			}
		}
	}
}
