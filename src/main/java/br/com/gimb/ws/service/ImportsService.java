package br.com.gimb.ws.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.poi.ss.util.NumberToTextConverter;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.controller.AreaController;
import br.com.gimb.ws.controller.ChecklistItemsController;
import br.com.gimb.ws.controller.ChecklistServicesController;
import br.com.gimb.ws.controller.ClientController;
import br.com.gimb.ws.controller.EquipmentController;
import br.com.gimb.ws.controller.VehicleController;
import br.com.gimb.ws.model.Area;
import br.com.gimb.ws.model.ChecklistItems;
import br.com.gimb.ws.model.ChecklistServices;
import br.com.gimb.ws.model.Client;
import br.com.gimb.ws.model.Equipment;
import br.com.gimb.ws.model.Imports;
import br.com.gimb.ws.model.Vehicle;
import br.com.gimb.ws.repository.ImportsRepository;
import br.com.gimb.ws.util.printPage;
import br.com.gimb.ws.util.aws.AwsS3Utils;

@Service
public class ImportsService extends BaseService<Imports, Long> {
	
	@Autowired
	EquipmentController equipmentController;
	
	@Autowired
	ClientController clientController;

	// @Autowired
	// private ClientService clientService;
	
	@Autowired
	AreaController areaController;

	@Autowired
	VehicleController vehicleController;

	@Autowired
	ChecklistServicesController checklistServicesController;

	@Autowired
	ChecklistItemsController checklistItemsController;

	@Autowired
	EquipmentService equipmentService;
	
	@RepositoryClass(clazz = ImportsRepository.class)
	@Autowired
	private ImportsRepository importsRepository;
	
	public Imports findByImportsId(long id) {
		return importsRepository.findByImportsId(id);
	}

	public Imports findByFilePath(String filePath) {
		return importsRepository.findByFilePath(filePath);
	}
	
	public byte[] downloadModel(String filePath) {
		try {
			
			return getModel(filePath);
					
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private byte[] getModel(String nome) {
		byte[] model = AwsS3Utils.downloadXLS(nome);
		return model;
	}
	
	public void transformXLSTable(Imports imports) throws IOException {
		byte[] data = Base64ToFile(imports.getFilePath(), imports.getMime());
		
		switch (imports.getNameClass()) {
			case CLIENT:
				clientCreate(data);
				break;
			case AREA:
				areaCreate(data);
			case EQUIPMENT:
				equipamentCreate(data);			
			default:
				break;
		}
	}

	public void importXLSTableVehicle(Imports imports, Long clientId) throws IOException {
		byte[] data = Base64ToFile(imports.getFilePath(), imports.getMime());

		switch (imports.getNameClass()) {			
			case EQUIPMENT:
				vehicleEquipamentCreate(data, clientId);
			default:
				break;
		}
	}	

	//Import items vehicleEquipment
	private void vehicleEquipamentCreate(byte[] data, Long clientId) {
		try {
			readXlsVehicleEquipment(data, clientId);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void readXlsVehicleEquipment(byte[] vehicleEquipamentByteArray, Long clientId) {
		try {
			ByteArrayInputStream arquivo = new ByteArrayInputStream(vehicleEquipamentByteArray);
			Workbook workbook =  WorkbookFactory.create(arquivo);
			Sheet sheet = workbook.getSheetAt(0);
			
			List<Vehicle> vehicles = getVehiclesByExcelEquipment(sheet, clientId);
			
			for(Vehicle vehicle : vehicles) {
				vehicleController.create(vehicle);
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	private List<Vehicle> getVehiclesByExcelEquipment(Sheet sheet, Long clientId) {
		List<Vehicle> vehicles = new ArrayList<>();

		int lastRowNum = sheet.getLastRowNum();
		
		for(int i = 16; i < lastRowNum + 1; i++) {
			Row row = sheet.getRow(i);
			
			if(row != null) {
				Vehicle vehicle = new Vehicle();
				
				Cell cellTypeEquipment = row.getCell(0);
				String equipmentDescription = "";
				if(cellTypeEquipment != null && cellTypeEquipment.getStringCellValue() != null) {
					equipmentDescription = cellTypeEquipment.getStringCellValue();
				}
				Equipment typeEquipmentId = getTypeEquipmentIdByDescription(equipmentDescription);
				vehicle.setEquipment(typeEquipmentId);
				
				Cell CellIdentifier = row.getCell(1);
				if (CellIdentifier.getCellType() == CellType.NUMERIC) {
					vehicle.setPlate(Double.toString(CellIdentifier.getNumericCellValue()));
				} else {
					vehicle.setPlate(CellIdentifier.getStringCellValue());
				}						
				
				Cell cellBrand = row.getCell(2);
				vehicle.setBrand(cellBrand.getStringCellValue());
				
				Cell cellModel = row.getCell(3);
				if (cellModel.getCellType() == CellType.NUMERIC) {
					vehicle.setModel(Double.toString(cellModel.getNumericCellValue()));
				} else {
					vehicle.setModel(cellModel.getStringCellValue());
				}
				

				Client client = clientController.findClientById(clientId);
				vehicle.setClient(client);
				
				vehicles.add(vehicle);
			}
			else 
				break;
		}
		
		return vehicles;
	}

	//Import dos itens de checklist services e checklist items
	public void importXLSTableChecklistServices(Imports imports, Long equipmentId) throws IOException {
		byte[] data = Base64ToFile(imports.getFilePath(), imports.getMime());

		switch (imports.getNameClass()) {			
			case CHECKLISTSERVICES:
				checklistServicesCreate(data, equipmentId);
			default:
				break;
		}
	}

	private void checklistServicesCreate(byte[] data, Long equipmentId) {
		try {
			readXlsChecklistServices(data, equipmentId);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void readXlsChecklistServices(byte[] checklistServicesByteArray, Long equipmentId) {
		try {
			ByteArrayInputStream arquivo = new ByteArrayInputStream(checklistServicesByteArray);
			Workbook workbook =  WorkbookFactory.create(arquivo);
			Sheet sheet = workbook.getSheetAt(0);
									
			checklistServicesController.deleteAllByEquipment(equipmentId);

			List<ChecklistServices> checklistServices = getChecklistServicesByExcel(sheet);
			
			for(ChecklistServices checklist : checklistServices) {
				checklist.setEquipmentEquipmentId(equipmentId);
				checklistServicesController.create(checklist);

				getChecklistItemsByExcel(sheet, checklist);
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	private List<ChecklistServices> getChecklistServicesByExcel(Sheet sheet){
		int lastRowNum = sheet.getLastRowNum();
		String checklistServiceName = "";

		List<ChecklistServices> checklistServices = new ArrayList<>();
		
		for(int i = 1; i < lastRowNum + 1; i++) {
			Row row = sheet.getRow(i);
			
			if(row != null) {
				ChecklistServices checklistService = new ChecklistServices();				
				
				Cell cellDescription = row.getCell(1);

				if (checklistServiceName == "" || !checklistServiceName.equals(cellDescription.getStringCellValue())) {
					checklistServiceName = cellDescription.getStringCellValue();
					checklistService.setChecklistName(checklistServiceName);
									
					checklistService.setActive(true);
					
					checklistServices.add(checklistService);					
				}
			}
		}
		
		return checklistServices;
	}

	private void getChecklistItemsByExcel(Sheet sheet, ChecklistServices checklistServices) {
		int lastRowNum = sheet.getLastRowNum();			

		for(int i = 1; i < lastRowNum + 1; i++) {
			Row row = sheet.getRow(i);
			String requiredPhotos = "";

			if (row != null) {
				ChecklistItems checklistItems = new ChecklistItems();				
				
				Cell cellChecklist = row.getCell(1);
				Cell cellDescription = row.getCell(2);

				Cell cellPhotoOK = row.getCell(3);
				Cell cellPhotoNOK = row.getCell(4);
				Cell cellPhotoFIX = row.getCell(5);				

				if (checklistServices.getChecklistName().equalsIgnoreCase(cellChecklist.getStringCellValue())) {
					checklistItems.setItemName(cellDescription.getStringCellValue());
					checklistItems.setActive(true);
					checklistItems.setChecklistServicesId(checklistServices.getChecklistId());

					String ok = "";
					String nok = "";
					String fix = "";

					if (cellPhotoOK.getStringCellValue().equalsIgnoreCase("s")) {
						ok = "\"OK\":TRUE,";
					}

					if (cellPhotoNOK.getStringCellValue().equalsIgnoreCase("s")) {
						nok = "\"NOK\":TRUE,";
					}

					if (cellPhotoFIX.getStringCellValue().equalsIgnoreCase("s")) {
						fix = "\"FIX\":TRUE";
					}

					if (!ok.isEmpty() && nok.isEmpty() && fix.isEmpty()) {
						ok.replace(",", "");
					}

					if (fix.isEmpty() && !nok.isEmpty()) {
						nok.replace(",", "");
					}

					requiredPhotos = "{"+ok+nok+fix+"}";

					checklistItems.setRequiredPhotos(requiredPhotos);
					checklistItemsController.create(checklistItems);
				}				
			}
		}
	}

	//Import dos itens de equipment
	private void equipamentCreate(byte[] data) {
		try {
			readXlsEquipament(data);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void readXlsEquipament(byte[] equipamentByteArray) {
		try {
			ByteArrayInputStream arquivo = new ByteArrayInputStream(equipamentByteArray);
			Workbook workbook =  WorkbookFactory.create(arquivo);
			Sheet sheet = workbook.getSheetAt(0);
			
			List<Equipment> equipments = getEquipamentByExcel(sheet);
			
			for(Equipment equipment : equipments) {
				equipmentController.create(equipment);
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	private List<Equipment> getEquipamentByExcel(Sheet sheet){
		int lastRowNum = sheet.getLastRowNum();

		List<Equipment> equipments = new ArrayList<>();
		
		for(int i = 16; i < lastRowNum + 1; i++) {
			Row row = sheet.getRow(i);
			
			if(row != null) {
				Equipment equipment = new Equipment();				
				
				Cell cellDescription = row.getCell(1);
				String description = cellDescription.getStringCellValue();
				equipment.setDescription(description);
								
				equipment.setActive(true);
				
				equipments.add(equipment);
			}
		}
		
		return equipments;
	}
	
	//Import items area
	private void areaCreate(byte[] data) {
		try {
			readXlsArea(data);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void readXlsArea(byte[] areaByteArray) {
		try {
			ByteArrayInputStream arquivo = new ByteArrayInputStream(areaByteArray);
			Workbook workbook =  WorkbookFactory.create(arquivo);
			Sheet sheet = workbook.getSheetAt(0);
			
			List<Area> areas = getAreasByExcel(sheet);
			
			for(Area area : areas) {
				areaController.saveArea(area);
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private List<Area> getAreasByExcel(Sheet sheet){
		int lastRowNum = sheet.getLastRowNum();
		List<Area> areas = new ArrayList<>();
		
		for(int i = 1; i< lastRowNum + 1; i++) {
			Row row = sheet.getRow(i);
			
			if(row != null) {
				Area area = new Area();
				
				Cell cellDescription = row.getCell(0);
				String description = cellDescription.getStringCellValue();
				area.setDescription(description);
				
				Cell cellActive = row.getCell(1);
				String active = cellActive.getStringCellValue();
				area.setActive( active.equalsIgnoreCase("sim"));
				
				areas.add(area);
			}
		}
		
		return areas;
	}
	
	//Import items client
	private void clientCreate(byte[] data) {
		
		try {
			readXlsClient(data);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void readXlsClient(byte[] clientByteArray) {
		try {
			ByteArrayInputStream arquivo = new ByteArrayInputStream(clientByteArray);
			Workbook workbook =  WorkbookFactory.create(arquivo);
			Sheet sheetTable = workbook.getSheetAt(0);
			Iterator<Row> rowIterator = sheetTable.iterator();
			
			Client client = getClientByExcel(rowIterator);
			List<Vehicle> vehicles = getVehiclesByExcel(workbook);
			client.setVehicles(vehicles);
			
			clientController.create(client);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}	
	
	private List<Vehicle> getVehiclesByExcel(Workbook workbook){
		List<Vehicle> vehicles = new ArrayList<>();
		
		Sheet sheet = workbook.getSheetAt(0);
		int lastRowNum = sheet.getLastRowNum();
		
		for(int i = 13; i < lastRowNum + 1; i++) {
			Row row = sheet.getRow(i);
			
			if(row != null) {
				Vehicle vehicle = new Vehicle();
				
				Cell cellTypeEquipment = row.getCell(0);
				String equipmentDescription = "";
				if(cellTypeEquipment != null && cellTypeEquipment.getStringCellValue() != null) {
					equipmentDescription = cellTypeEquipment.getStringCellValue();
				}
				Equipment typeEquipmentId = getTypeEquipmentIdByDescription(equipmentDescription);
				vehicle.setEquipment(typeEquipmentId);
				
				Cell CellIdentifier = row.getCell(1);
				vehicle.setPlate(Double.toString(CellIdentifier.getNumericCellValue()));
				
				Cell cellBrand = row.getCell(2);
				vehicle.setBrand(cellBrand.getStringCellValue());
				
				Cell cellModel = row.getCell(3);
				vehicle.setModel(cellModel.getStringCellValue());
				
				vehicles.add(vehicle);
			}
			else 
				break;
		}
		
		return vehicles;
	}

	private Equipment getTypeEquipmentIdByDescription(String description) {
		Equipment typeEquipment = null;
		
		List<Equipment> equipments = equipmentController.index();
		
		for(Equipment equipment : equipments) {
			if(equipment.getDescription().equalsIgnoreCase(description)) {
				typeEquipment = equipment;
			}
		}
		
		return typeEquipment;
	}
	
	private Client getClientByExcel(Iterator<Row> rowIterator) {
		Client client = new Client();
		
		Row rowCompanyName = rowIterator.next();
		Cell cellCompanyName = rowCompanyName.getCell(1);
		client.setCompanyName(cellCompanyName.getStringCellValue());
		
		Row rowTradingName = rowIterator.next();
		Cell cellTradingName = rowTradingName.getCell(1);
		client.setTradingName(cellTradingName.getStringCellValue());
		
		Row rowRegistration = rowIterator.next();
		Cell cellRegistration = rowRegistration.getCell(1);
		client.setRegistration(Double.toString(cellRegistration.getNumericCellValue()));
		
		Row rowDocument = rowIterator.next();
		Cell cellDocument = rowDocument.getCell(1);
		client.setDocument(Double.toString(cellDocument.getNumericCellValue()));
		
		Row rowAddress = rowIterator.next();
		Cell cellAddress = rowAddress.getCell(1);
		client.setAddress(cellAddress.getStringCellValue());
		
		Row rowPhone = rowIterator.next();
		Cell cellPhone = rowPhone.getCell(1);
		client.setPhone(Double.toString(cellPhone.getNumericCellValue()));
		
		Row rowZipCode = rowIterator.next();
		Cell cellZipCode = rowZipCode.getCell(1);
		client.setZipCode(Double.toString(cellZipCode.getNumericCellValue()));
		
		Row rowCity = rowIterator.next();
		Cell cellCity = rowCity.getCell(1);
		client.setCity(cellCity.getStringCellValue());
		
		client.setActive(true);
		
		return client;
	}
	
	private byte[] Base64ToFile(String base64, String mime) throws IOException {
		byte[] byteArray = Base64.getDecoder().decode(base64);	
		return byteArray;
	}

}