package br.com.gimb.ws.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.AreaUser;
import br.com.gimb.ws.model.Project;
import br.com.gimb.ws.repository.ProjectRepository;

@Service
public class ProjectService extends BaseService<Project, Long> {
	@RepositoryClass(clazz = ProjectRepository.class)
	@Autowired
	private ProjectRepository projectRepository;

	public List<Project> findAll() {
		return projectRepository.findAll();
	}

	public Project findByProjectId(long projectId) {
		return projectRepository.findByProjectId(projectId);
	}

	public List<Project> findByActive(Boolean active) {
		return projectRepository.findByActive(active);
	}

	public Project update(Long projectId, Project projectParam) {
		Project project = projectRepository.findByProjectId(projectId);

		project.setProjectName(projectParam.getProjectName());
		project.setProjectTime(projectParam.getProjectTime());
		project.setActive(projectParam.getActive());

		return project;
	}
	
	public List<Project> findByAreas(List<AreaUser> areaUsers){
		List<Long> areaIds = new ArrayList<>();
		
		for(AreaUser areaUser : areaUsers) {
			areaIds.add(areaUser.getAreaId());
		}
		
		return projectRepository.findByArea(areaIds);
	}
	
	public List<Project> findByAreaNull() {
		return projectRepository.findByAreaNull();
	}
}
