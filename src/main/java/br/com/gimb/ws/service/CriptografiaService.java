package br.com.gimb.ws.service;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class CriptografiaService {
	public static String criptMD5(String input) throws NoSuchAlgorithmException {
		
		MessageDigest md = MessageDigest.getInstance("MD5");
		byte[] msgd = md.digest(input.getBytes());
		BigInteger number = new BigInteger(1, msgd);
		String hash = number.toString(16);
		
		while(hash.length() < 32) {
			hash = "0" + hash;
		}
		
		return hash;
	}
}
