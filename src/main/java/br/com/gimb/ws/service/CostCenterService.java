package br.com.gimb.ws.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.CostCenter;
import br.com.gimb.ws.repository.CostCenterRepository;

@Service
public class CostCenterService extends BaseService<CostCenter, Long> {
	@RepositoryClass(clazz = CostCenterRepository.class)
	@Autowired
	private CostCenterRepository costCenterRepository;
	
	public List<CostCenter> findAll(){
		return costCenterRepository.findAll();
	}
	
	public CostCenter findByCostCenterId(long id){
		return costCenterRepository.findByCostCenterId(id);
	}
	
	public List<CostCenter> findByDescription(String description){
		return costCenterRepository.findByDescription(description);
	}
	
	public List<CostCenter> findByAccountCode(String accountCode){
		return costCenterRepository.findByAccountCode(accountCode);
	}
	
	public List<CostCenter> findByActive(String active){
		if(active.equalsIgnoreCase("all"))
			return costCenterRepository.findAll();
		else
			return costCenterRepository.findByActive(active.equalsIgnoreCase("true"));
	}
}
