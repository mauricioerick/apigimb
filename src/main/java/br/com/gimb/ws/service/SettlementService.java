package br.com.gimb.ws.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.CustomField;
import br.com.gimb.ws.model.CustomPicture;
import br.com.gimb.ws.model.Extract;
import br.com.gimb.ws.model.Settlement;
import br.com.gimb.ws.model.SettlementCostCenter;
import br.com.gimb.ws.model.User;
import br.com.gimb.ws.repository.SettlementRepository;
import br.com.gimb.ws.util.aws.AwsS3Utils;

@Service
public class SettlementService extends BaseService<Settlement, Long> {

	@RepositoryClass(clazz = SettlementRepository.class)
	@Autowired
	private SettlementRepository settlementRepository;

	@Autowired
	private ExtractService extractService;

	public List<Settlement> findByDateBetween(User user, String startDate, String endDate) {
		List<Settlement> settlementList = settlementRepository.findAllByDateLessThanEqualAndDateGreaterThanEqual(user,
				startDate, endDate);
		this.bindSettlementToExtract(settlementList, startDate, endDate, null);
		return settlementList;
	}

	public List<Settlement> findByDateBetweenAndClientAndUserAndEvent(String startDate, String endDate, Long clientId,
			Long userId, Long eventId, String natureExpenseId) {
		List<Settlement> settlementList = settlementRepository.findByDateBetweenAndClientAndUserAndEvent(startDate, endDate,
				clientId, userId, eventId, natureExpenseId != null ? Long.valueOf(natureExpenseId): null);
		this.bindSettlementToExtract(settlementList, startDate, endDate, natureExpenseId);
		return settlementList;
	}

	public void save(List<Settlement> list) {
		settlementRepository.saveAll(list);
	}

	private void bindSettlementToExtract(List<Settlement> list, String startDate, String endDate,
			String natureExpenseId) {
		if (list != null && !list.isEmpty()) {
			List<Extract> lExtracts = extractService.findAllBetweenReleaseDate(startDate, endDate, null, null);
			if (lExtracts != null && !lExtracts.isEmpty()) {
				list.forEach(set -> {
					Optional<Extract> extract = lExtracts.stream()
							.filter(ex -> ex.getSettlement() != null && ex.getSettlement().getSettlementId() == set.getSettlementId())
							.findFirst();
					if (extract.isPresent()) {
						set.setExtract(extract.get());
						set.getExtract().setSettlement(null);
					}
				});
			}
		}

	}
	
	public Map<String, Object> convertSettlementToMap(Settlement settlement, String baseName) {
		Map<String, Object> result = new HashMap<>();
		
		result.put("settlementId", settlement.getSettlementId());
        if (settlement.getUser() != null) {
            result.put("userId", settlement.getUser().getUserId());
            result.put("user", settlement.getUser().getUser());
            result.put("userName", settlement.getUser().getName());
        }
        if (settlement.getClient() != null) {
            result.put("clientId", settlement.getClient().getClientId());
            result.put("clientDocument", settlement.getClient().getDocument());
            result.put("clientCompanyName", settlement.getClient().getCompanyName());
            result.put("clientTradingName", settlement.getClient().getTradingName());
            result.put("clientAddress", settlement.getClient().getAddress());
        }
        if (settlement.getEvent() != null) {
            result.put("eventId", settlement.getEvent().getEventId());
            result.put("eventDescription", settlement.getEvent().getDescription());
            result.put("eventImprodutiveTime", settlement.getEvent().getImprodutiveTime());
        }
        if (settlement.getTypePayment() != null) {
            result.put("settlementPaymentTypeId", settlement.getTypePayment().getTypePaymentId());
            result.put("settlementPaymentType", settlement.getTypePayment().getDescription());
        }
        result.put("settlementCreditCardNumber", settlement.getPaymentId());
        if (settlement.getCard() != null && settlement.getCard().getCustomValues() != null) {
        	if (settlement.getCard().getCustomValues().get("jsonValues") != null) {
        		try {
            		Map<String, Object> values = (Map<String, Object>) settlement.getCard().getCustomValues().get("jsonValues"); 
            		values.forEach((key, value) -> {
                		result.put("settlementCard"+key, value);
                	});
        		} catch (Exception ex) {
        			// Do Nothing
        		}
        	}
        }
        
        result.put("settlementDate", settlement.getDate());
        result.put("settlementStartTime", settlement.getStartTime());
        result.put("settlementAmount", settlement.getAmount());
        result.put("settlementNote", settlement.getNote());
        result.put("settlementLatitude", settlement.getLatitude());
        result.put("settlementLongitude", settlement.getLongitude());
        if (settlement.getEvent() != null && settlement.getEvent().getNatureExpense() != null) {
            result.put("natureExpenseId", settlement.getEvent().getNatureExpense().getNatureExpenseId());
            result.put("natureExpenseAccountCode", settlement.getEvent().getNatureExpense().getAccountCode()); 
            result.put("natureExpenseDescription", settlement.getEvent().getNatureExpense().getDescription());
        }
        if (settlement.getExtract() != null) {
            result.put("approveDate", settlement.getExtract().getApproveDate());
            result.put("approveUser", settlement.getExtract().getApproveUser() != null
                    ? settlement.getExtract().getApproveUser().getUser() : null);
            result.put("approveUserName", settlement.getExtract().getApproveUser() != null
                    ? settlement.getExtract().getApproveUser().getName() : null);
            result.put("approveStatus", settlement.getExtract().getStatus());
            result.put("approveNote", settlement.getExtract().getNote());
        }

        Map<String, Object> custom;
        if (settlement.getCustomFieldList() != null && settlement.getCustomFieldList().size() > 0) {
            result.put("customFieldList", new ArrayList<Object>());
            for (CustomField field : settlement.getCustomFieldList()) {
                custom = new HashMap<String, Object>();

                custom.put("customField", field.getCustomField());
                custom.put("customFieldValue", field.getCustomFieldValue());
                ((ArrayList) result.get("customFieldList")).add(custom);
            }
        }

        if (settlement.getCustomPictureList() != null && settlement.getCustomPictureList().size() > 0) {
            result.put("customImageList", new ArrayList<Object>());
            for (CustomPicture pic : settlement.getCustomPictureList()) {
                custom = new HashMap<String, Object>();

                custom.put("latitude", pic.getLatitude());
                custom.put("longitude", pic.getLongitude());
                custom.put("picName", pic.getPicName());
                custom.put("message", pic.getMessage());
                custom.put("urlImage", AwsS3Utils.generatePreSignedUrl(pic.getPicName(), baseName, AwsS3Utils.initializeS3Client(baseName)));
                ((ArrayList) result.get("customImageList")).add(custom);
            }
        }

        if (settlement.getCostCenterList() != null && settlement.getCostCenterList().size() > 0) {
            result.put("centerCostList", new ArrayList<>());
            for (SettlementCostCenter settlementCostCenter : settlement.getCostCenterList()) {
                custom = new HashMap<String, Object>();

                custom.put("costCenterId", settlementCostCenter.getCostCenter().getCostCenterId());
                custom.put("costCenterAccountCode", settlementCostCenter.getCostCenter().getAccountCode());
                custom.put("costCenterDescription", settlementCostCenter.getCostCenter().getDescription());
                custom.put("costCenterPerc", settlementCostCenter.getPerc());
                ((ArrayList) result.get("centerCostList")).add(custom);
            }
        }
		
		return result;
	}
}
