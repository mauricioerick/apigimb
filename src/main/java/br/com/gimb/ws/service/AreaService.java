package br.com.gimb.ws.service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.Area;
import br.com.gimb.ws.model.AreaUser;
import br.com.gimb.ws.model.User;
import br.com.gimb.ws.repository.AreaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AreaService extends BaseService<Area, Long>{
	
	@RepositoryClass(clazz = AreaRepository.class)
	@Autowired
	private AreaRepository areaRepository;
	
	public List<Area> findAll(){
		return areaRepository.findAll();
	}
	
	public Area findByAreaId(long areaId) {
		return areaRepository.findByAreaId(areaId);
	}
	
	public List<Area> findByDescription(String description){
		return areaRepository.findByDescription(description);
	}
	
	public List<Area> findByActive(String active){
		if(active.equalsIgnoreCase("all"))
			return findAll();
		else
			return areaRepository.findByActive(active.equalsIgnoreCase("true"));
	}

	public List<Area> findByUserResponsible(User user){
		return areaRepository.findByUserResponsible(user.getUserId());
	}

	public List<AreaUser> findUsersByArea(User user){
		List<AreaUser> l = new ArrayList<>();
		for ( Area area : findByUserResponsible(user)){
			for(AreaUser areaUser :area.getUsers()){
				l.add(areaUser);
			}
		}
		return l;
	}
}
