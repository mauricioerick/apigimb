package br.com.gimb.ws.service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.Checklist;
import br.com.gimb.ws.repository.ActionRepository;
import br.com.gimb.ws.repository.ChecklistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChecklistService extends BaseService<Checklist, Long> {

	@RepositoryClass(clazz = ActionRepository.class)
	@Autowired
	private ChecklistRepository checklistRepository;

	public List<Checklist> findAll() {
		return (List<Checklist>) checklistRepository.findAll();
	}

	public Checklist findByChecklistId(Long id){
		return checklistRepository.findById(id).orElse(null);
	}

}
