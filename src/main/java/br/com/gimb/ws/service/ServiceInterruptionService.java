package br.com.gimb.ws.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.ServiceInterruption;
import br.com.gimb.ws.model.Services;
import br.com.gimb.ws.repository.ServiceInterruptionRepository;

@Service
public class ServiceInterruptionService extends BaseService<ServiceInterruption, Long> {

	@RepositoryClass(clazz = ServiceInterruptionRepository.class)
	@Autowired
	private ServiceInterruptionRepository serviceInterruptionRepository;
	
	public List<ServiceInterruption> findByService(Services service) {
		return serviceInterruptionRepository.findByService(service);
	}
	
	public void deleteAllByService(Services service) {
		if (service.getInterruptionList() != null && service.getInterruptionList().size() > 0) {
			for (ServiceInterruption interruption : service.getInterruptionList()) {
				serviceInterruptionRepository.deleteById(interruption.getServiceInterruptionID());
			}
		}
	}
}
