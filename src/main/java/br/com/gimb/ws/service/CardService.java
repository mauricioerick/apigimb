package br.com.gimb.ws.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.Card;
import br.com.gimb.ws.repository.CardRepository;

@Service
public class CardService extends BaseService<Card, Long>{
	
	@RepositoryClass(clazz = CardRepository.class)
	@Autowired
	private CardRepository cardRepository;
	
	public List<Card> findAll(){
		return cardRepository.findAll();
	}
	
	public Card findByCardId(long cardId) {
		Card card = cardRepository.findByCardId(cardId);
		return card;
	}

    public List<Card> avaibleCards() {
		return cardRepository.findAllByUserIsNull();
    }
    
    public List<Card> findCardBetweenDate(Long dtInicio, Long dtFinal){
    	return cardRepository.findCardBetweenDate(dtInicio, dtFinal);
    }
}
