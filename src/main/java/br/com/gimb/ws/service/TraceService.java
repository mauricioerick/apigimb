package br.com.gimb.ws.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.DTO.LocationDTO;
import br.com.gimb.ws.model.Trace;
import br.com.gimb.ws.repository.TraceRepository;

@Service
public class TraceService extends BaseService<Trace, Long>{

	@RepositoryClass(clazz = TraceRepository.class)
	@Autowired
	private TraceRepository traceRepository;
	
	public void save(List<Trace> traces) {
		traceRepository.saveAll(traces);
	}
	
	public List<Trace> findByDateBetween(String startDate, String endDate) {
		return traceRepository.findByDateBetween(startDate, endDate);
	}
	
	public List<Trace> findByDate(long id, String date) {
		return traceRepository.findByDate(id, date);
	}
	
	public List<LocationDTO> findAllByGroupUser() {
		List<LocationDTO> result = new ArrayList<>();
		
		Object[][] rows = traceRepository.findAllByGroupUser(); 
		if (rows != null) {
			for (Object[] rec : rows) {
				result.add(new LocationDTO(
								(Double) rec[0], // Double latitude 
								(Double) rec[1], // Double longitude 
								(String) rec[2], // String traceStartTime 
								Long.parseLong(rec[3].toString()), //long userId 
								(String) rec[4], // String name
								(String) rec[5], // String actionStartTime
								(String) rec[6], // String actionDescription
								(String) rec[7]  // String client
							)
						);
			}
		}
		
		return result;
	}
}
