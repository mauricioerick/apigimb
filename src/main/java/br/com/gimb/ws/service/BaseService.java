package br.com.gimb.ws.service;

import br.com.gimb.annotation.IgnoreUpperCase;
import br.com.gimb.ws.model.Device;
import br.com.gimb.ws.model.User;
import br.com.gimb.ws.repository.BalanceRepository;
import br.com.gimb.ws.util.FirebaseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class BaseService<T, ID extends Serializable> {

	protected CrudRepository<T, ID> repository;

	@Autowired
	DeviceService deviceService;
	
	@Autowired
	BalanceRepository balanceRepository;

	public void setRepository(CrudRepository<T, ID> repository) {
		this.repository = repository;
	}

	public List<T> findAll() {
		List<T> target = new ArrayList<T>();
		repository.findAll().forEach(target::add);
		return target;
	}

	public T findById(ID id) {
		return id == null ? null : repository.findById(id).isPresent() ? repository.findById(id).get() : null;
	}

	public void delete(T entity) {
		try {
			repository.delete(entity);
			sendNotification(entity);
		} catch (Exception e){
			e.printStackTrace();
		}
	}

	public <S extends Object> T save(T entity) {
		try { 
			setUpperCaseStrings(entity);
			setGUID(entity);
			sendNotification(entity);
			return repository.save(entity);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private void setGUID(T entity) throws NoSuchFieldException, IllegalAccessException {
		Field f = entity.getClass().getSuperclass().getDeclaredField("guid");
		if (f != null) {
			f.setAccessible(true);
			if (f.get(entity) == null) {
				f.set(entity, UUID.randomUUID().toString());
			}
		}
	}

	private void setUpperCaseStrings(T entity) throws IllegalAccessException {
		for (Field f : entity.getClass().getDeclaredFields()) {
			f.setAccessible(true);
			if (f.get(entity) == null)
				continue;
			else if (f.isAnnotationPresent(IgnoreUpperCase.class))
				continue;

			if (f.getType().equals(String.class)) {
				String value = f.get(entity).toString();
				if (isUrl(value))
					continue;
				if (isGUID(f))
					continue;

				f.set(entity, value.toUpperCase());
			}
		}
	}

	private boolean isUrl(String str) {
		return (str.toLowerCase().startsWith("http:") || str.toLowerCase().startsWith("https:"));
	}

	private boolean isGUID(Field field) {
		return (field.getName().toUpperCase().contains("GUID"));
	}

	public  void sendNotification( T entity) throws NoSuchFieldException, IllegalAccessException {
		try{
			if(!FirebaseUtil.classes.contains(entity.getClass())) return;
			List<Device> devices = new ArrayList();
			if(entity.getClass().equals(User.class)){
				Field f = entity.getClass().getDeclaredField("devices");
				if (f != null) {
					f.setAccessible(true);
					if (f.get(entity) != null) {
						devices = (List<Device>) f.get(entity);
					}
				}
			}else {
				devices = deviceService.findAll();
			}
			FirebaseUtil.sendNotificationUpdateAll(devices, entity.getClass().getName(), entity.toString());
		} catch (NoSuchFieldException e) {
			throw e;
		} catch (IllegalAccessException e) {
			throw e;
		} catch (Exception e) {}
	}

}
