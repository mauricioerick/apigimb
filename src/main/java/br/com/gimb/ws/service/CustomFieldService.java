package br.com.gimb.ws.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.enumerated.ReferenceTypeEnum;
import br.com.gimb.ws.model.CustomField;
import br.com.gimb.ws.repository.CustomFieldRepository;

@Service
public class CustomFieldService extends BaseService<CustomField, Long> {

	@RepositoryClass(clazz = CustomFieldRepository.class)
	@Autowired
	private CustomFieldRepository customFieldRepository;
	
	public List<CustomField> findByReferenceTypeAndReferenceId(ReferenceTypeEnum referencetype, String referenceId) {
		return customFieldRepository.findByReferenceTypeAndReferenceId(referencetype, referenceId);
	}
	
	public List<CustomField> findByReferenceTypeAndReferenceIdIn(ReferenceTypeEnum referencetype, List<String> referenceIds) {
		return customFieldRepository.findByReferenceTypeAndReferenceIdIn(referencetype, referenceIds);
	}
	
	public void deleteByReferenceTypeAndReferenceId(ReferenceTypeEnum referenceType, String referenceId) {
		List<CustomField> customFields = this.findByReferenceTypeAndReferenceId(referenceType, referenceId);
		if (customFields != null)
			customFieldRepository.deleteAll(customFields);
	}
}
