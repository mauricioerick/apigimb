package br.com.gimb.ws.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.ObjectType;
import br.com.gimb.ws.repository.ObjectTypeRepository;

@Service
public class ObjectTypeService extends BaseService<ObjectType, Long> {

    @RepositoryClass(clazz = ObjectTypeRepository.class)
	@Autowired
    private ObjectTypeRepository objectTypeRepository;
    
    public List<ObjectType> findByActive(String active) {
		if (active.equalsIgnoreCase("all"))
			return objectTypeRepository.findAll();
		else
			return objectTypeRepository.findByActive(active.equalsIgnoreCase("true"));
    }
    
    public List<ObjectType> findAll(){
		return objectTypeRepository.findAll();
	}
}