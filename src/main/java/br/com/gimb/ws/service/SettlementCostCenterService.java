package br.com.gimb.ws.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.SettlementCostCenter;
import br.com.gimb.ws.repository.SettlementCostCenterRepository;

@Service
public class SettlementCostCenterService extends BaseService<SettlementCostCenter, Long> {

	@RepositoryClass(clazz = SettlementCostCenterRepository.class)
	@Autowired
	private SettlementCostCenterRepository settlementCostCenterRepository;

}
