package br.com.gimb.ws.service;

import br.com.gimb.ws.enumerated.ChecklistStatusEnum;
import br.com.gimb.ws.enumerated.ExportExcelDomainEnum;
import br.com.gimb.ws.enumerated.PermissionWebEnum;
import br.com.gimb.ws.enumerated.ReferenceTypeEnum;
import br.com.gimb.ws.model.*;
import br.com.gimb.ws.util.PermissionsUtil;
import br.com.gimb.ws.util.Util;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ExportExcelService {

	@Autowired
	UserService userService;

	@Autowired
	ExtractService extractService;
	
	@Autowired
	CardService cardService;

	@Autowired
	ObjectHistoryService objectHistoryService;

	@Autowired
	CustomFieldService customFieldService;

	@Autowired
	ServiceService serviceService;

	@Autowired
	ProjectService projectService;

	@Autowired
	TransferService transferService;

	@Autowired
	AreaService areaService;

	public File createExcel(ExportExcelDomainEnum domain, Map<String, String[]> parameters) {
		File excel;

		switch (domain){
			case EXTRACT:
				excel = extractToExcel(parameters);
				break;
			case EXTRACTCARD:
				excel = extractCardToExcel(parameters);
				break;
			case INVENTORY:
				excel = inventoryToExcel(parameters);
				break;
			case SERVICE:
				excel = serviceToExcel(parameters);
				break;
			case POINT:
				excel = treansferToExcel(parameters);
				break;
			default:
				excel = null;
		}
		
		return excel;
	}

	private File extractToExcel(Map<String, String[]> parameters) {

		File excel;

		Long userId = Long.parseLong(parameters.get("userId")[0]);
		User user = userService.findByUserId(userId);

		String saldo = parameters.get("saldo")[0];
		String pendenteAprovacao = parameters.get("pendenteAprovacao")[0];
		String saldoDisponivel = parameters.get("saldoDisponivel")[0];

		String initDate = parameters.get("startDate")[0].replace('-', '/');
		String finDate = parameters.get("endDate")[0].replace('-', '/');

		String type[] = parameters.get("type");
		String status[] = parameters.get("status");

		String language = parameters.get("language")[0] == null ? "pt" : parameters.get("language")[0];

		List<Extract> listaExtract = extractService.findAllBetweenReleaseDate(initDate, finDate, user, null);

		// Filtra de acordo com o tipo -> C = Credito e D = Debito
		listaExtract = listaExtract.stream().filter(e -> Arrays.asList(type).contains(e.getType()))
				.collect(Collectors.toList());

		// Filtra de acordo com o status -> A = Aprovado, R = Reprovado e P = pendente
		listaExtract = listaExtract.stream().filter(e -> Arrays.asList(status).contains(e.getStatus()))
				.collect(Collectors.toList());

		String userName = "";
		String filename = "";

		if (user != null) {
			userName = user.getName();
			filename = userName.replace(' ', '_') + ".xls";
		} else {
			userName = "TODOS";
			filename = "TODOS.xls";
		}

		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheetExtract = workbook.createSheet(userName);

		try {
			int rownum = 0;

			Row row_0 = sheetExtract.createRow(rownum++);

			Cell cellNome = row_0.createCell(0);
			cellNome.setCellValue(parameters.get("name")[0]);

			rownum++;

			Row row_1 = sheetExtract.createRow(rownum++);

			Cell cellSaldoDesc = row_1.createCell(0);
			cellSaldoDesc.setCellValue(Util.lblBundle(language, null, "Saldo"));

			Cell cellPengindAprovDesc = row_1.createCell(2);
			cellPengindAprovDesc.setCellValue(Util.lblBundle(language, null, "pendenteDeAprovacao"));

			Cell cellSaldoDispoDesc = row_1.createCell(5);
			cellSaldoDispoDesc.setCellValue(Util.lblBundle(language, null, "SaldoDisponivel"));

			Row row_2 = sheetExtract.createRow(rownum++);

			Cell cellSaldo = row_2.createCell(0);
			cellSaldo.setCellValue(saldo);

			Cell cellPengindAprov = row_2.createCell(2);
			cellPengindAprov.setCellValue(pendenteAprovacao);

			Cell cellSaldoDispo = row_2.createCell(5);
			cellSaldoDispo.setCellValue(saldoDisponivel);

			rownum++;

			Row row_3 = sheetExtract.createRow(rownum++);

			Cell cellDataInicioDesc = row_3.createCell(0);
			cellDataInicioDesc.setCellValue(Util.lblBundle(language, null, "dataInicio"));

			Cell cellDataFinalDesc = row_3.createCell(2);
			cellDataFinalDesc.setCellValue(Util.lblBundle(language, null, "dataFinal"));

			Row row_4 = sheetExtract.createRow(rownum++);

			Cell cellDataInicio = row_4.createCell(0);
			cellDataInicio.setCellValue(initDate);

			Cell cellDataFinal = row_4.createCell(2);
			cellDataFinal.setCellValue(finDate);

			rownum++;

			Row row_5 = sheetExtract.createRow(rownum++);

			Cell cellUsuario = row_5.createCell(0);
			cellUsuario.setCellValue(Util.lblBundle(language, null, "User"));

			Cell cellDia = row_5.createCell(1);
			cellDia.setCellValue(Util.lblBundle(language, null, "Day"));

			Cell cellEvento = row_5.createCell(2);
			cellEvento.setCellValue(Util.lblBundle(language, null, "Event"));

			Cell cellVal = row_5.createCell(3);
			cellVal.setCellValue(Util.lblBundle(language, null, "Valor"));

			Cell cellSituacao = row_5.createCell(4);
			cellSituacao.setCellValue(Util.lblBundle(language, null, "Situation"));

			Cell cellObservacao = row_5.createCell(5);
			cellObservacao.setCellValue(Util.lblBundle(language, null, "Obs"));

			Cell cellInformacao = row_5.createCell(6);
			cellInformacao.setCellValue(Util.lblBundle(language, null, "Info"));

			Cell cellAdicional = row_5.createCell(7);
			cellAdicional.setCellValue(Util.lblBundle(language, null, "adicionais"));

			for (Extract extract : listaExtract) {
				Row row = sheetExtract.createRow(rownum++);
				int cellnum = 0;

				Cell cellUser = row.createCell(cellnum++);
				if (extract.getUser() != null) {
					cellUser.setCellValue(extract.getUser().getName());
				} else
					cellUser.setCellValue("");

				Cell cellDay = row.createCell(cellnum++);
				if (extract.getReleaseDate() != null) {
					cellDay.setCellValue(extract.getReleaseDate());
				} else
					cellDay.setCellValue("");

				Cell cellEvent = row.createCell(cellnum++);
				if (extract.getSettlement() != null && extract.getSettlement().getEvent() != null
						&& extract.getSettlement().getEvent().getDescription() != null)
					cellEvent.setCellValue(extract.getSettlement().getEvent().getDescription());
				else
					cellEvent.setCellValue(Util.lblBundle(language, null, "CreditoAdd"));

				Cell cellValor = row.createCell(cellnum++);
				if (extract.getAmount() != null) {
					double valor = extract.getType().equalsIgnoreCase("D") ? 0 - extract.getAmount() : extract.getAmount();
					cellValor.setCellValue(String.format("%.2f", valor));
				} else
					cellValor.setCellValue("");

				Cell cellStatus = row.createCell(cellnum++);

				if (extract.getStatus() != null) {
					String s = extract.getStatus().toUpperCase();
					if (s.equals("A"))
						cellStatus.setCellValue(Util.lblBundle(language, null, "Aprovado"));
					else if (s.equals("P"))
						cellStatus.setCellValue(Util.lblBundle(language, null, "Pendente"));
					else if (s.equals("R"))
						cellStatus.setCellValue(Util.lblBundle(language, null, "Desaprovado"));
				} else
					cellStatus.setCellValue("");

				Cell cellObs = row.createCell(cellnum++);
				if (extract.getNote() != null)
					cellObs.setCellValue(extract.getNote());
				else
					cellObs.setCellValue("");

				Cell cellInfo = row.createCell(cellnum++);

				cellInfo.setCellValue(montarInfo(extract, language));

				Cell cellAdd = row.createCell(cellnum++);
				StringBuilder content = new StringBuilder();
				if (extract.getSettlement() != null && extract.getSettlement().getCustomFieldList() != null
						&& extract.getSettlement().getCustomFieldList().size() > 0) {
					for (CustomField cf : extract.getSettlement().getCustomFieldList()) {
						if (content.length() > 0)
							content.append("\n");

						content.append(cf.getCustomField() + ": " + cf.getCustomFieldValue());
					}
				}

				cellAdd.setCellValue(content.toString());

				CellStyle cs = workbook.createCellStyle();
				cs.setWrapText(true);
				cellAdd.setCellStyle(cs);
			}

			excel = new File(filename);

			FileOutputStream out = new FileOutputStream(excel);
			workbook.write(out);
			workbook.close();

		} catch (Exception e) {
			return null;
		}

		return excel;
	}

	private String montarInfo(Extract extract, String lang) {
		if (extract.getSettlement() != null) {
			String informacao = Util.lblBundle(lang, null, "mensagemInfoForExtract")
					.replace("{event}", extract.getSettlement().getEvent().getDescription())
					.replace("{data}", extract.getSettlement().getStartTime())
					.replace("{client}", extract.getSettlement().getClient().getTradingName())
					.replace("{typePayment}", extract.getTypePayment().getDescription())
					.replace("{user}", extract.getUser().getName())
					.replace("{id}", extract.getSettlement().getSettlementId() + "")
					.replace("{createdData}", extract.getReleaseDate());

			if (extract.getSettlement().getNote().length() > 0) {
				informacao = informacao + "\n ---------------------------------------------------- \n";
				informacao = informacao
						+ Util.lblBundle(lang, null, "observacaoUsuario").replace("{note}", extract.getSettlement().getNote());
			}

			return informacao;
		}

		return extract.getInfo() != null ? extract.getInfo() : "";
	}

	private File extractCardToExcel(Map<String, String[]> parameters) {
		File excel = null;

		Long userId = null;

		if(parameters.get("userId") != null)
			userId = Long.parseLong(parameters.get("userId")[0]);

		String initDate = parameters.get("startDate")[0].replace('-', '/');
		String finDate = parameters.get("endDate")[0].replace('-', '/');

		String type[] = parameters.get("type");

		String language = parameters.get("language")[0] == null ? "pt" : parameters.get("language")[0];

		String c = parameters.get("cardId")[0];
		Long cardId = (long) Long.parseLong(c);

		Card card = cardService.findByCardId(cardId);

		String tp = parameters.get("typePayment")[0];
		Long typePayment = (long) Long.parseLong(tp);

		List<Extract> listaExtract = extractService.findAllBetweenReleaseDateCard(initDate, finDate, cardId, typePayment, userId);


		// Filtra de acordo com o tipo -> C = Credito e D = Debito
		listaExtract = listaExtract.stream().filter(e -> Arrays.asList(type).contains(e.getType()))
				.collect(Collectors.toList());

		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheetExtract = workbook.createSheet(card.getCardNumber());

		try {
			int rownum = 0;

			Row row_cardNumberDesc = sheetExtract.createRow(rownum++);
			Cell cellNumberCardDesd = row_cardNumberDesc.createCell(0);
			cellNumberCardDesd.setCellValue(Util.lblBundle(language, null, "cardNumber"));

			Row row_0 = sheetExtract.createRow(rownum++);
			Cell cellNumber = row_0.createCell(0);
			String cellCardNumberValue = ( card.getCardNumber() != null) ? card.getCardNumber().toString() : "";
			cellNumber.setCellValue(cellCardNumberValue);

			rownum++;

			Row row_cardUserDesc = sheetExtract.createRow(rownum++);
			Cell cellCardUserDesc = row_cardUserDesc.createCell(0);
			cellCardUserDesc.setCellValue(Util.lblBundle(language, null, "User"));

			Row row_cardUser = sheetExtract.createRow(rownum++);
			Cell cellCardUser = row_cardUser.createCell(0);
			cellCardUser.setCellValue(userId != null ? userService.findById(userId).getName() : Util.lblBundle(language, null, "ALL"));


			Row row_cardDateDesc = sheetExtract.createRow(++rownum);
			Cell cellInitDateDesc = row_cardDateDesc.createCell(0);
			cellInitDateDesc.setCellValue(Util.lblBundle(language, null, "dataInicio"));

			Cell cellFinalDateDesc = row_cardDateDesc.createCell(2);
			cellFinalDateDesc.setCellValue(Util.lblBundle(language, null, "dataFinal"));

			Row row_cardDate = sheetExtract.createRow(++rownum);
			Cell cellInitDate = row_cardDate.createCell(0);
			cellInitDate.setCellValue(initDate);

			Cell cellFinalDate = row_cardDate.createCell(2);
			cellFinalDate.setCellValue(finDate);

			rownum++;
			rownum++;

			Row row_1 = sheetExtract.createRow(rownum++);

			Cell cellSaldoDesc = row_1.createCell(0);
			cellSaldoDesc.setCellValue(Util.lblBundle(language, null, "Saldo"));

			Cell cellPengindAprovDesc = row_1.createCell(2);
			cellPengindAprovDesc.setCellValue(Util.lblBundle(language, null, "Limit"));

			Row row_2 = sheetExtract.createRow(rownum++);

			Cell cellSaldo = row_2.createCell(0);
			String cellSaldoValue = ( card.getCredict() != null) ? card.getCredict().toString() : "";
			cellSaldo.setCellValue(cellSaldoValue);

			Cell cellPengindAprov = row_2.createCell(2);
			String cellLimitValue = ( card.getLimit() != null) ? card.getLimit().toString() : "";
			cellPengindAprov.setCellValue( cellLimitValue );

			rownum++;
			Row row_3 = sheetExtract.createRow(rownum++);

			Cell cellUserDesc = row_3.createCell(0);
			cellUserDesc.setCellValue(Util.lblBundle(language, null, "User"));

			Cell cellDiaDesc = row_3.createCell(1);
			cellDiaDesc.setCellValue(Util.lblBundle(language, null, "Day"));

			Cell cellEventoDesc = row_3.createCell(2);
			cellEventoDesc.setCellValue(Util.lblBundle(language, null, "Event"));

			Cell cellValorDesc = row_3.createCell(3);
			cellValorDesc.setCellValue(Util.lblBundle(language, null, "Valor"));

			int cellNum = 0;

			for(Extract extract : listaExtract) {
				Row row = sheetExtract.createRow(rownum++);

				Cell cellUser = row.createCell(cellNum++);
				String cellUserValue = (extract.getUser() != null) ? extract.getUser().getName() : "";
				cellUser.setCellValue(cellUserValue);

				Cell cellDia = row.createCell(cellNum++);
				String cellDayValue = (extract.getReleaseDate() != null) ? extract.getReleaseDate() : "";
				cellDia.setCellValue(cellDayValue);

				String description = extract.getSettlement() != null ? extract.getSettlement().getEvent().getDescription()
						: Util.lblBundle(language, null, "CreditoAdd");

				Cell cellEvento = row.createCell(cellNum++);
				cellEvento.setCellValue(description);

				Cell cellValor = row.createCell(cellNum++);
				String cellAmountAprovvedValue = (extract.getAmountApproved() != null) ? extract.getAmountApproved().toString() : ""; 
				cellValor.setCellValue(cellAmountAprovvedValue);

				cellNum = 0;
			}

			excel = new File(listaExtract.get(0).getCard().getCardNumber() + ".xls");

			FileOutputStream out = new FileOutputStream(excel);
			workbook.write(out);
			workbook.close();

		}catch (Exception e) {
			e.printStackTrace();
		}

		return excel;
	}


	private File inventoryToExcel(Map<String, String[]> parameters) {
		File excel = null;

		try {
			String initDate = parameters.get("startDate")[0].replace('-', '/');
			String finDate = parameters.get("endDate")[0].replace('-', '/');

			Boolean groupBy = "groupBy".equalsIgnoreCase(parameters.get("groupBy")[0]);

			String language = parameters.get("language")[0] == null ? "pt" : parameters.get("language")[0];

			List<ObjectHistory> listObjectHistory = objectHistoryService.findByDate(initDate,finDate);

			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheetExtract = workbook.createSheet();

			int rownum = 0;

			final int colObjectType=0;
			final int colIdentifier=1;
			final int colPatrimony=2;
			final int colSerialNumber=3;
			final int colStorage=4;
			final int colDate=5;
			final int colUser=6;

			Row rowHeader = sheetExtract.createRow(rownum++);

			rowHeader.createCell(colObjectType).setCellValue(Util.lblBundle(language, null, "ObjectType"));
			rowHeader.createCell(colIdentifier).setCellValue(Util.lblBundle(language, null, "Identifier"));
			rowHeader.createCell(colPatrimony).setCellValue(Util.lblBundle(language, null, "Patrimony"));
			rowHeader.createCell(colSerialNumber).setCellValue(Util.lblBundle(language, null, "SerialNumber"));
			rowHeader.createCell(colStorage).setCellValue(Util.lblBundle(language, null, "Storage"));
			rowHeader.createCell(colDate).setCellValue(Util.lblBundle(language, null, "Date"));
			rowHeader.createCell(colUser).setCellValue(Util.lblBundle(language, null, "User"));

			ObjectInventory objectInventory = null;

			for(ObjectHistory oh : listObjectHistory) {
				if(groupBy){
					if(oh.getObject().equals(objectInventory)){
						continue;
					} else {
						objectInventory = oh.getObject();
					}
				}

				Row row = sheetExtract.createRow(rownum++);

				row.createCell(colObjectType).setCellValue(oh.getObject().getObjectType().getDescription());
				row.createCell(colIdentifier).setCellValue(oh.getObject().getIdentifier());
				row.createCell(colPatrimony).setCellValue(oh.getObject().getPatrimony());
				row.createCell(colSerialNumber).setCellValue(oh.getObject().getSerialNumber());
				row.createCell(colStorage).setCellValue(oh.getStorage().getDescription());
				row.createCell(colDate).setCellValue(oh.getDateTime());
				row.createCell(colUser).setCellValue(oh.getUser().getUser());
				int currentRow = colUser+1;
			
				for(CustomField customField : customFieldService.findByReferenceTypeAndReferenceId(ReferenceTypeEnum.IH, String.valueOf(oh.getObjectHistoryId()))){
					rowHeader.createCell(currentRow).setCellValue(customField.getCustomField());
					row.createCell(currentRow++).setCellValue(customField.getCustomFieldValue());
				}

				for(CustomField customField : customFieldService.findByReferenceTypeAndReferenceId(ReferenceTypeEnum.IN, String.valueOf(oh.getObject().getObjectId()))){
					row.createCell(currentRow).setCellValue(customField.getCustomField());
					row.createCell(currentRow++).setCellValue(customField.getCustomFieldValue());
				}

			}

			excel = new File(Util.lblBundle(language, null, "Inventory") + ".xls");

			FileOutputStream out = new FileOutputStream(excel);
			workbook.write(out);
			workbook.close();

		}catch (Exception e) {
			e.printStackTrace();
		}

		return excel;
	}

	private File serviceToExcel(Map<String, String[]> parameters) {
		File excel = null;

		try {

			String language = parameters.get("language")[0] == null ? "pt" : parameters.get("language")[0];

			String projectId = parameters.get("projectId")[0];

			Project project = projectService.findByProjectId(Long.valueOf(projectId));
			List<Services> listService = serviceService.findByProject(project);

			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheetExtract = workbook.createSheet();

			int rownum = 0;

			final int colDate=0;
			final int colUser=1;
			final int colEquipment=2;
			final int colBrand=3;
			final int colModel=4;
			final int colNote=5;


			Row rowHeader = sheetExtract.createRow(rownum++);

			rowHeader.createCell(colDate).setCellValue(Util.lblBundle(language, null, "Date"));
			rowHeader.createCell(colUser).setCellValue(Util.lblBundle(language, null, "User"));
			rowHeader.createCell(colEquipment).setCellValue(Util.lblBundle(language, null, "Equipment"));
			rowHeader.createCell(colBrand).setCellValue(Util.lblBundle(language, null, "Brand"));
			rowHeader.createCell(colModel).setCellValue(Util.lblBundle(language, null, "Model"));
			rowHeader.createCell(colNote).setCellValue(Util.lblBundle(language, null, "Obs"));

			for(Services service : listService) {


				Row row = sheetExtract.createRow(rownum++);

				row.createCell(colDate).setCellValue(service.getEndTime());
				row.createCell(colUser).setCellValue(service.getUser().getUser());
				if(service.getVehicle().getEquipment() != null)
				row.createCell(colEquipment).setCellValue(service.getVehicle().getEquipment().getDescription());
				row.createCell(colBrand).setCellValue(service.getVehicle().getBrand());
				row.createCell(colModel).setCellValue(service.getVehicle().getModel());
				row.createCell(colNote).setCellValue(service.getNote());

				int currentCol = colNote+1;

				List<CustomField> customFields = customFieldService.findByReferenceTypeAndReferenceId(ReferenceTypeEnum.SE, String.valueOf(service.getServiceId()));
				if(customFields != null && customFields.size() > 0){
					row.createCell(currentCol++).setCellValue(Util.lblBundle(language, null, "CustomFields"));

					for(CustomField customField : customFields){
						row.createCell(currentCol++).setCellValue(customField.getCustomField());
						row.createCell(currentCol++).setCellValue(customField.getCustomFieldValue());
					}
				}

				if(service.getChecklists() != null && service.getChecklists().size() > 0){
					row.createCell(currentCol++).setCellValue(Util.lblBundle(language, null, "Checklists"));

					for(Checklist checklist : service.getChecklists()){
						row.createCell(currentCol++).setCellValue(checklist.getCheklistItem());
						boolean ok = !ChecklistStatusEnum.NOT_FIXED.equals(checklist.getStatus());
						row.createCell(currentCol++).setCellValue(Util.lblBundle(language, null, ok?"YES":"NO"));
					}
				}

				if(service.getListObjectHistory() != null && service.getListObjectHistory().size() >0 ){
					row.createCell(currentCol++).setCellValue(Util.lblBundle(language, null, "Inventory"));

					for(ObjectHistory oh :  service.getListObjectHistory()){
						row.createCell(currentCol++).setCellValue(Util.lblBundle(language, null, "ObjectType"));
						row.createCell(currentCol++).setCellValue(oh.getObject().getObjectType().getDescription());
						row.createCell(currentCol++).setCellValue(Util.lblBundle(language, null, "SerialNumber"));
						row.createCell(currentCol++).setCellValue(oh.getObject().getSerialNumber());
						row.createCell(currentCol++).setCellValue(Util.lblBundle(language, null, "Patrimony"));
						row.createCell(currentCol++).setCellValue(oh.getObject().getPatrimony());
					}
				}

			}

			excel = new File(Util.lblBundle(language, null, "Services") + ".xls");

			FileOutputStream out = new FileOutputStream(excel);
			workbook.write(out);
			workbook.close();

		}catch (Exception e) {
			e.printStackTrace();
		}

		return excel;
	}


	private File treansferToExcel(Map<String, String[]> parameters) {
		File excel = null;

		try {

			String language = parameters.get("language")[0] == null ? "pt" : parameters.get("language")[0];

			String initDate = parameters.get("startDate")[0].replace('-', '/');
			String finDate = parameters.get("endDate")[0].replace('-', '/');
			String users = parameters.get("users")[0];
			String userId = parameters.get("user")[0];

			String areaUsers = null;
			if(userId != null) {
				User userObj = userService.findById(Long.parseLong(userId));
				if(PermissionsUtil.getPermissionValue(userObj.getProfile(), PermissionWebEnum.filtrarApontamentoArea).equals("1")){
					areaUsers = areaService.findUsersByArea(userObj).stream().map(areaUser -> areaUser.getUser()).collect(Collectors.joining(","));
				}
				if(PermissionsUtil.getPermissionValue(userObj.getProfile(), PermissionWebEnum.filtrarApontamentoUsuario).equals("0"))
					users = null;
			}

			List<Transfer> listTransfer = transferService.findByDateBetween(initDate,finDate,null);

			HashMap<User, List<Transfer>> mapa = new HashMap<>();

			for(Transfer transfer : listTransfer){
				if(users != null && !users.isEmpty() && !users.contains(transfer.getUser().getName())) continue;
				if(areaUsers != null && !areaUsers.isEmpty() && !areaUsers.contains(transfer.getUser().getUser())) continue;
				List<Transfer> list = mapa.get(transfer.getUser());
				if(list == null){
					list = new ArrayList<>();
				}
				list.add(transfer);
				mapa.put(transfer.getUser(),list);
			}

			HSSFWorkbook workbook = new HSSFWorkbook();

			final int colAction = 0;
			final int colInitDate = 1;
			final int colEndDate = 2;
			final int colInitLatLng = 3;
			final int colEndLatLng = 4;
			final int colNote = 5;

			mapa.forEach((user, transfers) -> {
				HSSFSheet sheetExtract = workbook.createSheet(user.getName());
				int rownum = 0;

				Row rowHeader = sheetExtract.createRow(rownum++);
				rowHeader.createCell(colAction).setCellValue(Util.lblBundle(language, null, "Action"));
				rowHeader.createCell(colInitDate).setCellValue(Util.lblBundle(language, null, "dataInicio"));
				rowHeader.createCell(colEndDate).setCellValue(Util.lblBundle(language, null, "dataFinal"));
				rowHeader.createCell(colInitLatLng).setCellValue(Util.lblBundle(language, null, "locInicio"));
				rowHeader.createCell(colEndLatLng).setCellValue(Util.lblBundle(language, null, "locFim"));
				rowHeader.createCell(colNote).setCellValue(Util.lblBundle(language, null, "Obs"));

				for(Transfer transfer : transfers){
					Row rowTransfer = sheetExtract.createRow(rownum++);
					rowTransfer.createCell(colAction).setCellValue(Util.lblBundle(language, null, "PointRecord"));
					rowTransfer.createCell(colInitDate).setCellValue(transfer.getStartTime());
					rowTransfer.createCell(colEndDate).setCellValue(transfer.getEndTime());
					rowTransfer.createCell(colInitLatLng).setCellValue(transfer.getLatStartTime()+","+transfer.getLgtStartTime());
					rowTransfer.createCell(colEndLatLng).setCellValue(transfer.getLatEndTime()+","+transfer.getLgtEndTime());
					rowTransfer.createCell(colNote).setCellValue(transfer.getNote());

					long elapsedTime = 0;

					for(TransferEvents event : transfer.getEventsList()){
						elapsedTime += event.getTimeElapsedMilli();
						Row rowEvent = sheetExtract.createRow(rownum++);
						rowEvent.createCell(colAction).setCellValue(event.getEvent().getDescription());
						rowEvent.createCell(colInitDate).setCellValue(event.getStartTime());
						rowEvent.createCell(colEndDate).setCellValue(event.getEndTime());
						rowEvent.createCell(colInitLatLng).setCellValue(event.getTransfer().getLatStartTime()+","+event.getTransfer().getLgtStartTime());
						rowEvent.createCell(colEndLatLng).setCellValue(event.getTransfer().getLatEndTime()+","+event.getTransfer().getLgtEndTime());
						rowEvent.createCell(colNote).setCellValue(event.getNote());
					}

					for (Services service : serviceService.findByDateBetween(transfer.getStartTime(), transfer.getEndTime(), transfer.getUser())){
						elapsedTime += service.getTimeElapsedMilli();
					}

					Row rowNoAppointment = sheetExtract.createRow(rownum++);

					long diff = transfer.getTimeElapsedMilli() - elapsedTime;
					if(diff < 0) diff = 0;

					int seconds = (int) (diff / 1000) % 60;
					int minutes = (int) ((diff / (1000*60)) % 60);
					int hours   = (int) ((diff / (1000*60*60)));

					rowNoAppointment.createCell(colAction).setCellValue(Util.lblBundle(language, null, "NoAppointment"));
					rowNoAppointment.createCell(colInitDate).setCellValue(String.format("%02d:%02d:%02d",hours,minutes,seconds));
					sheetExtract.addMergedRegion(new CellRangeAddress(rowNoAppointment.getRowNum(),rowNoAppointment.getRowNum(),colInitDate,colNote));

					rownum++;
				}

			});

			excel = new File(Util.lblBundle(language, null, "PointRecord" +
					"") + ".xls");

			FileOutputStream out = new FileOutputStream(excel);
			workbook.write(out);
			workbook.close();

		}catch (Exception e) {
			e.printStackTrace();
		}

		return excel;
	}

}
