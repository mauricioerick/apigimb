package br.com.gimb.ws.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.ws.model.Client;
import br.com.gimb.ws.model.Contact;
import br.com.gimb.ws.repository.ContactRepository;

@Service
public class ContactService extends BaseService<Contact, Long>{
	
	@RepositoryClass(clazz = ContactRepository.class)
	@Autowired
	private ContactRepository contactsRepository;
	
	public List<Contact> findByClient(Client client){
		return contactsRepository.findByClient(client);
	}
}
