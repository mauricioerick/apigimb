package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

	List<User> findAll();

	List<User> findByActive(Boolean active);

	User findByUserId(long userId);

	User findByUser(String name);

	User findByPass(String pass);
}
