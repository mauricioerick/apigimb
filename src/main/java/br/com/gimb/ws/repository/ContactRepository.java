package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.Client;
import br.com.gimb.ws.model.Contact;

@Repository
public interface ContactRepository extends CrudRepository<Contact, Long>{
	
	List<Contact> findByClient(Client client);
}
