package br.com.gimb.ws.repository;

import br.com.gimb.ws.model.AgendaItem;
import org.springframework.data.repository.CrudRepository;

public interface AgendaItemRepository extends CrudRepository<AgendaItem, Long> {
}
