package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.Action;

@Repository
public interface ActionRepository extends CrudRepository<Action, Long> {
	List<Action> findAll();

	List<Action> findByActionId(long eventId);

	List<Action> findByDescription(String description);

	List<Action> findByActive(Boolean active);
}
