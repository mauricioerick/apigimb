package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.ObjectInventory;

@Repository
public interface ObjectRepository extends CrudRepository<ObjectInventory, Long> {
	
    List<ObjectInventory> findByActive(Boolean active);

    List<ObjectInventory> findAll();
    
    ObjectInventory findByIdentifier(String identifier);

    ObjectInventory findByIdentifierOrSerialNumberOrPatrimony(String identifier, String serialNumber, String patrimony);
    
}