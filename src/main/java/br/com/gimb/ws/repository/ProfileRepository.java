package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.Profile;

@Repository
public interface ProfileRepository extends CrudRepository<Profile, Long> {

	List<Profile> findAll();

	List<Profile> findByActive(Boolean active);
}
