package br.com.gimb.ws.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.Device;

@Repository
public interface DeviceRepository extends CrudRepository<Device, Long> {

	Device findByDeviceId(long deviceId);

	Optional<Device> findByGuid(String guid);
}
