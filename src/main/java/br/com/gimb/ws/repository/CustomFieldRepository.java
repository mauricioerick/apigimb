package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.enumerated.ReferenceTypeEnum;
import br.com.gimb.ws.model.CustomField;

@Repository
public interface CustomFieldRepository extends CrudRepository<CustomField,Long> {
	
	List<CustomField> findByReferenceTypeAndReferenceId(ReferenceTypeEnum referencetype, String referenceId);
	
	List<CustomField> findByReferenceTypeAndReferenceIdIn(ReferenceTypeEnum referencetype, List<String> referenceIds);
	
}
