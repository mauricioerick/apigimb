package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.TableParameter;

@Repository
public interface TableParameterRepository extends CrudRepository<TableParameter, Long> {
	
	List<TableParameter> findAll();

	TableParameter findByTableParameterId(long tableParameterId);

	List<TableParameter> findByTableReference(String tableReference);

	List<TableParameter> findByTableReferenceAndActive(String tableReference, Boolean active);

	List<TableParameter> findByActive(Boolean active);

}
