package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.Client;
import br.com.gimb.ws.model.Vehicle;

@Repository
public interface VehicleRepository extends CrudRepository<Vehicle, Long> {
	public List<Vehicle> findByClient(Client client);

	@Query(value = " SELECT v FROM Vehicle v WHERE equipment is NULL AND client = :client ")	
	public List<Vehicle> findByEquipmentNull(@Param("client") Client client);
}
