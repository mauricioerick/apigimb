package br.com.gimb.ws.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.CostCenter;
import br.com.gimb.ws.model.Settlement;
import br.com.gimb.ws.model.SettlementCostCenter;

@Repository
public interface SettlementCostCenterRepository extends CrudRepository<SettlementCostCenter, Long> {
  public List<SettlementCostCenter> findAll();

  public Optional<SettlementCostCenter> findById(Long id);

  public Optional<SettlementCostCenter> findBySettlement(Settlement settlement);

  public Optional<SettlementCostCenter> findByCostCenter(CostCenter costCenter);
}