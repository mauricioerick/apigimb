package br.com.gimb.ws.repository;

import br.com.gimb.ws.DTO.AgendaDTO;
import br.com.gimb.ws.model.Agenda;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface AgendaRepository extends CrudRepository<Agenda, Long> {

    @Query(value = "SELECT new br.com.gimb.ws.DTO.AgendaDTO(" +
            "a.date,  " +
            "ai.agendaItemId, " +
            "c.companyName,  " +
            "a2.description,  " +
            "CONCAT(v.brand, ' - ', v.plate), " +
            "p.projectName, " +
            "ai.type as type, " +
            "ai.note as note, " +
            "ai.status as status)" +
            "FROM Agenda a " +
            "LEFT JOIN AgendaItem ai ON (ai.agendaId = a.agendaId) " +
            "LEFT JOIN Client c ON (c.clientId = ai.clientId) " +
            "LEFT JOIN Action a2 ON (a2.actionId = ai.actionId) " +
            "LEFT JOIN Vehicle v ON (v.vehicleId = ai.vehicleId) " +
            "LEFT JOIN Project p ON (p.projectId = ai.projectId) " +
            "WHERE a.date BETWEEN :dtInicial AND :dtFinal AND (:active IS NULL OR ai.status = :active) and a.userId = :userId ")
    List<AgendaDTO> listByAgendaWithJoin(
            @Param("dtInicial") LocalDate dtInicial, @Param("dtFinal") LocalDate dtFinal,
            @Param("active") Integer active, @Param("userId") Long userId);


    Optional<Agenda> findByUserIdAndDate(Long userId, LocalDate date);
}
