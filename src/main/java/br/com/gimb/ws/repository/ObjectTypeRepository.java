package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.ObjectType;

@Repository
public interface ObjectTypeRepository extends CrudRepository<ObjectType, Long> {
    List<ObjectType> findByActive(Boolean active);

    List<ObjectType> findAll();
}