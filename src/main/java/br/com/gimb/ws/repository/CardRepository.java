package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import br.com.gimb.ws.model.Card;

public interface CardRepository extends CrudRepository<Card, Long> {
	List<Card> findAll();
	
	Card findByCardId(long cardId);

	List<Card> findAllByUserIsNull();
	
	@Query(value = "SELECT "
			+ "*FROM card "
			+ "WHERE "
			+ "(:dtFinal is null || invoice_close_date <= :dtFinal) AND "
			+ "(:dtInicio is null || invoice_close_date >= :dtInicio)", nativeQuery = true)
	List<Card> findCardBetweenDate(@Param("dtInicio")Long dtInicio, @Param("dtFinal")Long dtFinal);
	
}
