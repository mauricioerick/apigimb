package br.com.gimb.ws.repository;

import br.com.gimb.ws.model.DebitNote;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DebitNoteRepository extends CrudRepository<DebitNote, Long> {
	List<DebitNote> findAll();

	@Query(value = "select dn.series, count(dn.sequence) from DebitNote dn group by dn.series")
	List<Object[]> getAllSeriesWithSequence();
}
