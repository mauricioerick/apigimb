package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.Client;

@Repository
public interface ClientRepository extends CrudRepository<Client,Long> {

	@Deprecated
	/**
	 * Attribute 'situacao' is deprecated, now must be use attribute 'active'
	 * @param situacao
	 * @return List<Client>
	 */
	List<Client> findBySituacao(String situacao);

	List<Client> findByActive(Boolean active);
	
	List<Client> findByDocument(String document);

	List<Client> findByTradingName(String tradingName);

	List<Client> findAll();
}
