package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.Settlement;
import br.com.gimb.ws.model.User;

@Repository
public interface SettlementRepository extends CrudRepository<Settlement, Long> {

	@Query(value = " SELECT s FROM Settlement s ORDER BY STR_TO_DATE(s.date, '%d/%m/%Y'), s.user, s.client ")
	List<Settlement> findAll();

	Settlement findBySettlementId(long settlementId);

	List<Settlement> findByDate(String date);

	@Query(value = " SELECT s FROM Settlement s WHERE STR_TO_DATE(s.date, '%d/%m/%Y') BETWEEN STR_TO_DATE(:startDate, '%d/%m/%Y') AND STR_TO_DATE(:endDate, '%d/%m/%Y') AND (:user IS NULL OR :user = s.user) ORDER BY STR_TO_DATE(s.date, '%d/%m/%Y'), s.user ")
	List<Settlement> findAllByDateLessThanEqualAndDateGreaterThanEqual(@Param("user") User user,
			@Param("startDate") String startDate, @Param("endDate") String endDate);

	@Query(value = " " + " SELECT s " + " FROM Settlement s " + " WHERE "
			+ " (:clientId IS NULL OR s.client.clientId = :clientId) " + " AND (:userId IS NULL OR s.user.userId = :userId) "
			+ " AND (:eventId IS NULL OR s.event.eventId = :eventId)"
			+ " AND STR_TO_DATE(s.date, '%d/%m/%Y') BETWEEN STR_TO_DATE(:startDate, '%d/%m/%Y') AND STR_TO_DATE(:endDate, '%d/%m/%Y') "
			+ " AND COALESCE(s.status, 'T') <> 'V' "
			+ " AND (:natureExpenseId IS NULL OR s.natureExpense.natureExpenseId = :natureExpenseId)"
			+ " ORDER BY STR_TO_DATE(s.date, '%d/%m/%Y'), s.user ")
	List<Settlement> findByDateBetweenAndClientAndUserAndEvent(@Param("startDate") String startDate,
			@Param("endDate") String endDate, @Param("clientId") Long clientId, @Param("userId") Long userId,
			@Param("eventId") Long eventId, @Param("natureExpenseId") Long natureExpenseId);
}
