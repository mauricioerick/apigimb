package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.Transfer;
import br.com.gimb.ws.model.User;

@Repository
public interface TransferRepository extends CrudRepository<Transfer, Long> {
	@Query(value=""
			+ " SELECT t "
			+ " FROM Transfer t "
			+ " WHERE STR_TO_DATE(t.startTime, '%d/%m/%Y') "
			+ " BETWEEN STR_TO_DATE(:startDate, '%d/%m/%Y') "
			+ " AND STR_TO_DATE(:endDate, '%d/%m/%Y') "
			+ " AND (:user IS NULL OR :user = t.user)"
			+ " ORDER BY STR_TO_DATE(t.startTime, '%d/%m/%Y'), t.user ")
	List<Transfer> findByDateBetween(@Param("startDate") String startDate, @Param("endDate") String endDate, @Param("user") User user);

	List<Transfer> findByUserAndEndTimeIsNull(User user);
}
