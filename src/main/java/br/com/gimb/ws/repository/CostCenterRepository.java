package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.CostCenter;

@Repository
public interface CostCenterRepository extends CrudRepository<CostCenter, Long> {
	List<CostCenter> findAll();
	
	CostCenter findByCostCenterId(long id);
	
	List<CostCenter> findByDescription(String description);
	
	List<CostCenter> findByAccountCode(String accountCode);
	
	List<CostCenter> findByActive(Boolean active);
}
