package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.TypePayment;

@Repository
public interface TypePaymentRepository extends CrudRepository<TypePayment, Long> {
    List<TypePayment> findByActive(Boolean active);
    List<TypePayment> findAll();
}