package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.ServiceInterruption;
import br.com.gimb.ws.model.Services;

@Repository
public interface ServiceInterruptionRepository extends CrudRepository<ServiceInterruption, Long> {
	
	List<ServiceInterruption> findByService(Services service);
	
}
