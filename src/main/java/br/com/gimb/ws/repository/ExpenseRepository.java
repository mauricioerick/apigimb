package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.Expense;

@Repository
public interface ExpenseRepository extends CrudRepository<Expense,Long> {
	List<Expense> findByHash(String hash);
	
	@Query(value=" SELECT t FROM Expense t WHERE STR_TO_DATE(t.startDate, '%d/%m/%Y') BETWEEN STR_TO_DATE(:startDate, '%d/%m/%Y') AND STR_TO_DATE(:endDate, '%d/%m/%Y') ORDER BY STR_TO_DATE(t.startDate, '%d/%m/%Y'), t.client ")
	List<Expense> findByStartDateBetween(@Param("startDate") String startDate, @Param("endDate") String endDate);
	
}
