package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.Observation;

@Repository
public interface ObservationRepository extends CrudRepository<Observation, Long> {
    List<Observation> findByActive(Boolean active);
    
    List<Observation> findAll();
}