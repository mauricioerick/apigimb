package br.com.gimb.ws.repository;

import br.com.gimb.ws.DTO.ChecklistServicesDTO;
import br.com.gimb.ws.model.ChecklistServices;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface ChecklistServiceRepository extends CrudRepository<ChecklistServices, Long> {
    List<ChecklistServices> findAll();

    List<ChecklistServices> findByActive(Boolean active);

    public List<ChecklistServices> findByEquipmentEquipmentId(long equipmentEquipmentId);

    @Query(value = " SELECT cs.checklist_services_id, cs.checklist_name, " 
        + " count((select COUNT(sp2.critically) from service_pics sp2 where sp2.critically = sp.critically AND sp2.critically = 'A' and sp2.services_id = :serviceId group by sp2.critically)) as criticallyHigh, "
        + " count((select COUNT(sp2.critically) from service_pics sp2 where sp2.critically = sp.critically AND sp2.critically = 'M' and sp2.services_id = :serviceId group by sp2.critically)) as criticallyMedium, "
        + " count((select COUNT(sp2.critically) from service_pics sp2 where sp2.critically = sp.critically AND sp2.critically = 'B' and sp2.services_id = :serviceId group by sp2.critically)) as criticallyLow " 
        + " from checklist_services cs " 
        + " join checklist_items ci on ci.checklist_services_id = cs.checklist_services_id "
        + " join service_pics sp on sp.checklist_items_item_id = ci.item_id " 
        + " where sp.services_id = :serviceId "
        + " group by cs.checklist_services_id, cs.checklist_name "
    , nativeQuery = true)
	List<Object[]> getChecklistServicesAndCriticallyByService(@Param("serviceId") Long serviceId);

    @Query(value = " SELECT sp.services_id, " 
        + " count((select COUNT(sp2.critically) from service_pics sp2 where sp2.critically = sp.critically AND sp2.critically = 'A' and sp2.services_id = :serviceId group by sp2.critically)) as criticallyHigh, "
        + " count((select COUNT(sp2.critically) from service_pics sp2 where sp2.critically = sp.critically AND sp2.critically = 'M' and sp2.services_id = :serviceId group by sp2.critically)) as criticallyMedium, "
        + " count((select COUNT(sp2.critically) from service_pics sp2 where sp2.critically = sp.critically AND sp2.critically = 'B' and sp2.services_id = :serviceId group by sp2.critically)) as criticallyLow " 
        + " from checklist_services cs " 
        + " join checklist_items ci on ci.checklist_services_id = cs.checklist_services_id "
        + " join service_pics sp on sp.checklist_items_item_id = ci.item_id " 
        + " where sp.services_id = :serviceId "
        + " group by sp.services_id "
    , nativeQuery = true)
	List<Object[]> getChecklistServicesAndCriticallyByServiceTotal(@Param("serviceId") Long serviceId);

}
