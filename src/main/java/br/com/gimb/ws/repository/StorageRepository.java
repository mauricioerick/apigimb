package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.Storage;

@Repository
public interface StorageRepository extends CrudRepository<Storage, Long> {
    List<Storage> findByActive(Boolean active);

    List<Storage> findAll();
}