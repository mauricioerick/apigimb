package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.DTO.TimeLineFeedDTO;
import br.com.gimb.ws.model.User;

@Repository
public interface DashboardsRepository extends CrudRepository<User, Long> {
	
	@Query(" "
			+ " SELECT "
			+ "   new br.com.gimb.ws.DTO.TimeLineFeedDTO( "
			+ "     ser.user.name, "
			+ "     ser.action.description||'\n'||ser.client.tradingName, "
			+ "     SUBSTRING(ser.startTime, 1, 10), "
			+ "     SUBSTRING(ser.startTime, 12, 5), "
			+ "     ser.serviceId, "
			+ "     'SER', "
			+ "     ser.note, "
			+ "		ser.client.tradingName"
			+ "   ) "
			+ " FROM "
			+ "   Services ser "
			+ " WHERE "
			+ "   (STR_TO_DATE(ser.startTime, '%d/%m/%Y') BETWEEN STR_TO_DATE(:startDate, '%d/%m/%Y') AND STR_TO_DATE(:endDate, '%d/%m/%Y'))"
			+ " ORDER BY "
			+ "   STR_TO_DATE(ser.startTime, '%d/%m/%Y %H:%i:%s'), ser.user.user "
			+ "")
	public List<TimeLineFeedDTO> findTimeLineFeedsServiceByDateBetween(@Param("startDate") String startDate, @Param("endDate") String endDate);
	
	@Query(" "
			+ " SELECT "
			+ "   new br.com.gimb.ws.DTO.TimeLineFeedDTO( "
			+ "     s.user.name, "
			+ "     s.event.description||'\n'||s.client.tradingName, "
			+ "     SUBSTRING(s.startTime, 1, 10), "
			+ "     SUBSTRING(s.startTime, 12, 5), "
			+ "     s.settlementId, "
			+ "     'SET', "
			+ "     s.note, "
			+ "		s.client.tradingName"
			+ "   ) "
			+ " FROM "
			+ "   Settlement s"
			+ " WHERE "
			+ "   STR_TO_DATE(s.startTime, '%d/%m/%Y') BETWEEN STR_TO_DATE(:startDate, '%d/%m/%Y') AND STR_TO_DATE(:endDate, '%d/%m/%Y') "
			+ " ORDER BY "
			+ "   STR_TO_DATE(s.startTime, '%d/%m/%Y %H:%i:%s'), s.user.user "
			+ "")
	public List<TimeLineFeedDTO> findTimeLineFeedsSettlementByDateBetween(@Param("startDate") String startDate, @Param("endDate") String endDate);
	
}
