package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.Expense;
import br.com.gimb.ws.model.ExpenseItem;

@Repository
public interface ExpenseItemRepository extends CrudRepository<ExpenseItem,Long> {
	List<ExpenseItem> findByExpense(Expense expense);
}
