package br.com.gimb.ws.repository;

import br.com.gimb.ws.model.Area;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AreaRepository extends CrudRepository<Area, Long> {
	List<Area> findAll();
	
	Area findByAreaId(long areaId);
	
	List<Area> findByDescription(String description);
	
	List<Area> findByActive(Boolean active);

	@Query(value = "select area.* from area join area_user au on area.area_id = au.area_id\n" +
			"where user_id = ? and responsible = 1", nativeQuery = true)
	List<Area> findByUserResponsible(Long userId);


}