package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.ChecklistItems;

@Repository
public interface ChecklistItemsRepository extends CrudRepository<ChecklistItems, Long> {

    @Query(value = "SELECT * FROM checklist_items WHERE checklist_services_id = :checklistServicesId", nativeQuery = true)
    List<ChecklistItems> findChecklistItemsById(@Param("checklistServicesId") Long checklistServicesId);
}
