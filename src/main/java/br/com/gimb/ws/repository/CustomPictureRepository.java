package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.enumerated.ReferenceTypeEnum;
import br.com.gimb.ws.model.CustomPicture;

@Repository
public interface CustomPictureRepository extends CrudRepository<CustomPicture,Long> {
	List<CustomPicture> findByReferenceTypeAndReferenceId(ReferenceTypeEnum referencetype, String referenceId);
}
