package br.com.gimb.ws.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.Company;

@Repository
public interface CompanyRepository extends CrudRepository<Company, Long> {
	Company findByCompanyId(long companyId);
}
