package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.Event;

@Repository
public interface EventRepository extends CrudRepository<Event, Long> {
	Event findByEventId(long eventId);

	List<Event> findByDescription(String description);

	List<Event> findByActive(Boolean active);

	List<Event> findAll();
}
