package br.com.gimb.ws.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.EmailConfig;

@Repository
public interface EmailConfigRepository extends CrudRepository<EmailConfig, Long> {
	
	EmailConfig findByEmailConfigId(long emailConfigId);
	
}
