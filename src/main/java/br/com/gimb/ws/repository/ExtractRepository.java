package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.Card;
import br.com.gimb.ws.model.Event;
import br.com.gimb.ws.model.Extract;
import br.com.gimb.ws.model.Settlement;
import br.com.gimb.ws.model.TypePayment;
import br.com.gimb.ws.model.User;

@Repository
public interface ExtractRepository extends CrudRepository<Extract, Long>{
    
    @Query(value=" "
            + " SELECT "
            + "     e "
            + " FROM "
            + "     Extract e "
            + " WHERE "
            + "     STR_TO_DATE(e.releaseDate, '%d/%m/%Y') BETWEEN STR_TO_DATE(:startDate, '%d/%m/%Y') AND "
            + "     STR_TO_DATE(:endDate, '%d/%m/%Y') AND "
            + "     (:user is null OR :user = e.user) AND "
            + "     (:typePayment IS NULL OR :typePayment = e.typePayment) AND " 
            + "     EXISTS (SELECT 1 FROM Balance b where b.typePayment = e.typePayment AND (b.user = e.user OR b.card = e.card)) "
            + " ORDER BY "
            + "     STR_TO_DATE(e.releaseDate, '%d/%m/%Y %H:%i:%s') desc ")
    List<Extract> findAllBetweenReleaseDate(@Param("startDate") String startDate, @Param("endDate") String endDate, @Param("user") User user, @Param("typePayment") TypePayment typePayment);
    
    @Query(value=" "
            + " SELECT "
            + "   e.user_id, "
            + "   e.payment_type_id, "
            + "   ROUND(sum(if(type='C', e.amount, (e.amount * -1))), 2) "
            + " FROM "
            + "   extract e "
            + " WHERE "
            + "   (:userId is null OR :userId = e.user_id) AND "
            + "   (:typePaymentId IS NULL OR :typePaymentId = e.payment_type_id) AND "
            + "   e.status = :status AND "
            + "   EXISTS (SELECT DISTINCT b.user_id FROM balance b where b.user_id = e.user_id AND b.payment_type_id = e.payment_type_id) "
            + " GROUP BY "
            + "   e.user_id, "
            + "   e.payment_type_id "
            + " ORDER BY "
            + "   e.user_id, "
            + "   e.payment_type_id ", nativeQuery = true )
    List<Object[]> findSumAmountByStatus(@Param("userId") Long userId, @Param("typePaymentId") Long typePaymentId, @Param("status") String status);
    
    @Query(value=" "
            + " SELECT "
            + "   e.user_id, "
            + "   e.payment_type_id, "
            + "   MAX(STR_TO_DATE(e.release_date, '%d/%m/%Y')) last_credit_date "
            + " FROM "
            + "   extract e "
            + " WHERE "
            + "   (:userId is null OR :userId = e.user_id) AND "
            + "   (:typePaymentId IS NULL OR :typePaymentId = e.payment_type_id) AND "
            + "   EXISTS (SELECT DISTINCT b.user_id FROM balance b where b.user_id = e.user_id AND b.payment_type_id = e.payment_type_id) "
            + " GROUP BY "
            + "   e.user_id, "
            + "   e.payment_type_id "
            + " ORDER BY "
            + "   e.user_id, "
            + "   e.payment_type_id ", nativeQuery = true )
    List<Object[]> findLastCreditDate(@Param("userId") Long userId, @Param("typePaymentId") Long typePaymentId);
    
    @Query(value=" "
            + " SELECT "
            + "   e.release_date, "
            + "   ROUND(sum(if(type='C', e.amount, (e.amount_approved * -1))), 2) "
            + " FROM "
            + "   extract e "
            + " WHERE "
            + "   (:userId is null OR :userId = e.user_id) AND "
            + "   (:typePaymentId IS NULL OR :typePaymentId = e.payment_type_id) AND "
            + "   (:cardId IS NULL OR :cardId = e.card_id) AND "
            + "   (:status IS NULL OR e.status = :status) AND "
            + "   STR_TO_DATE(e.release_date, '%d/%m/%Y') >= STR_TO_DATE(:date, '%d/%m/%Y') "
            + " GROUP BY "
            + "   e.release_date "
            + " ORDER BY "
            + "   STR_TO_DATE(e.release_date, '%d/%m/%Y') ", nativeQuery = true )
    List<Object[]> findSumAmountGroupByDateFromDate(@Param("userId") Long userId, @Param("typePaymentId") Long typePaymentId, @Param("cardId") Long cardId, @Param("date") String date, @Param("status") String status);
  
    @Query(value=" SELECT e FROM Extract e WHERE (:user IS NULL OR :user = e.user) AND (:card IS NULL OR :card = e.card) AND (:event IS NULL OR :event = e.settlement.event) AND e.status = 'P' ORDER BY STR_TO_DATE(e.releaseDate, '%d/%m/%Y %H:%i:%s')")
    List<Extract> findAllPending(@Param("user") User user, @Param("event") Event event, @Param("card") Card card);
    
    @Query(value="SELECT"
    		+ "		*FROM"
    		+ "		extract e"
    		+ "		WHERE"
    		+ "			(:cardId = e.card_id) AND"
    		+ "			(:typePaymentId is null OR :typePaymentId = e.payment_type_id) AND"
    		+ "			(:userId is null OR :userId = e.user_id) AND"
    		+ "			STR_TO_DATE(e.release_date, '%d/%m/%Y') >= STR_TO_DATE(:starDate, '%d/%m/%Y') AND"
    		+ "			STR_TO_DATE(e.release_date, '%d/%m/%Y') <= STR_TO_DATE(:endDate, '%d/%m/%Y')"
    		+ "		ORDER BY"
    		+ "			STR_TO_DATE(e.release_date, '%d/%m/%Y') desc", nativeQuery = true)
    List<Extract>findAllBetweenReleaseDateCard(@Param("starDate") String starDate, @Param("endDate") String endDate, @Param("cardId") Long cardId, @Param("typePaymentId") Long typePaymentId, @Param("userId") Long userId);
    
    Extract findBySettlement(Settlement settlement);
    
}