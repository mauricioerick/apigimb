package br.com.gimb.ws.repository;

import br.com.gimb.ws.model.ObjectHistory;
import br.com.gimb.ws.model.ObjectInventory;
import br.com.gimb.ws.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ObjectHistoryRepository extends CrudRepository<ObjectHistory, Long> {
    List<ObjectHistory> findAll();

    @Query(value = "select * from object_history oh " +
            "join object o on o.object_id = oh.object_id " +
            "join storage s on oh.storage_id = s.storage_id " +
            "join user u on oh.user_id = u.user_id " +
            "where STR_TO_DATE(oh.date_time, '%d/%m/%Y') between STR_TO_DATE(:starDate, '%d/%m/%Y') and STR_TO_DATE(:endDate, '%d/%m/%Y') " +
            "group by oh.object_history_id " +
            "order by o.object_id", nativeQuery = true)
    List<ObjectHistory> findAllByDateTimeBetweenOrderByObjectAsc(@Param("starDate") String starDate, @Param("endDate") String endDate);

    ObjectHistory findFirstByObjectAndUserAndDateTimeBetweenOrderByDateTimeDesc(ObjectInventory object, User user, String inicio, String fim);
}