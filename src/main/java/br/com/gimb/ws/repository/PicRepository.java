package br.com.gimb.ws.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.gimb.ws.model.ServiceImage;

@Repository
public interface PicRepository extends CrudRepository<ServiceImage, Long> {
	default void delete(Long id) {

	}
	
	@Modifying
	@Transactional
	@Query(value="delete from ServiceImage s where s.servico IS NULL")
	void deleteNull();
	
}
