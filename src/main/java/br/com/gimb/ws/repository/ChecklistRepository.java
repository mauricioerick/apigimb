package br.com.gimb.ws.repository;

import br.com.gimb.ws.model.Checklist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChecklistRepository extends CrudRepository<Checklist, Long> {

}
