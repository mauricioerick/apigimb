package br.com.gimb.ws.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.Imports;

@Repository
public interface ImportsRepository extends CrudRepository<Imports, Long> {	
	Imports findByImportsId(long id);

	Imports findByFilePath(String filePath);
}
