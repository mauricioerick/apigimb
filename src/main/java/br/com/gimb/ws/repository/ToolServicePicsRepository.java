package br.com.gimb.ws.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.ToolServicesPics;

@Repository
public interface ToolServicePicsRepository extends CrudRepository<ToolServicesPics, Long> {
    
}
