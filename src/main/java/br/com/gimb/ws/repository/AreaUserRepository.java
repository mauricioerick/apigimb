package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.AreaUser;

@Repository
public interface AreaUserRepository extends CrudRepository<AreaUser, Long>{
	List<AreaUser> findAll();
	
	List<AreaUser> findByUserId(long id);
	
	List<AreaUser> findByAreaId(long id);
}
