package br.com.gimb.ws.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.TransferEvents;

@Repository
public interface TransferEventRepository extends CrudRepository<TransferEvents,Long> {
}
