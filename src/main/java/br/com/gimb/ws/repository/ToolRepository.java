package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.Tool;

@Repository
public interface ToolRepository extends CrudRepository<Tool, Long> {
    List<Tool> findAll();

	List<Tool> findByToolId(long toolId);

	List<Tool> findByDescription(String description);

	List<Tool> findByActive(Boolean active);
}
