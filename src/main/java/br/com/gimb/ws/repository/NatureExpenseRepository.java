package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.NatureExpense;

@Repository
public interface NatureExpenseRepository extends CrudRepository<NatureExpense, Long>{
	List<NatureExpense> findAll();
	
	List<NatureExpense> findByNatureExpenseId(long id);
	
	List<NatureExpense> findByDescription(String description);
	
	List<NatureExpense> findByActive(Boolean active);
}
