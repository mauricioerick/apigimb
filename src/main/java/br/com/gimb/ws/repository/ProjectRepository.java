package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.Project;

@Repository
public interface ProjectRepository extends CrudRepository<Project, Long> {
	List<Project> findAll();
	
	Project findByProjectId(long projectId);
	
	List<Project> findByActive(Boolean active);
	
	@Query(value = "SELECT p FROM Project p "
			+ "WHERE p.area IS NULL")
	List<Project> findByAreaNull();
	
	@Query(value = "SELECT p FROM Project p "
			+ "		WHERE "
			+ "			p.area.areaId IN (:areaIds) OR"
			+ "			p.area IS NULL")
	List<Project> findByArea(@Param("areaIds") List<Long> areaIds);
}
