package br.com.gimb.ws.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.TextTemplate;

@Repository
public interface TextTemplateRepository extends CrudRepository<TextTemplate, Long> {
	List<TextTemplate> findAll();

	Optional<TextTemplate> findByTextTemplateId(long eventId);

	List<TextTemplate> findByOrigin(String origin);
}
