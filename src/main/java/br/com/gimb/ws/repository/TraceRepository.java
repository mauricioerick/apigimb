package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.Trace;

@Repository
public interface TraceRepository extends CrudRepository<Trace, Long> {

	@Query(value=" "
			+ " SELECT t FROM Trace t "
			+ " WHERE "
			+ "   STR_TO_DATE(t.startTime, '%d/%m/%Y') BETWEEN STR_TO_DATE(:startDate, '%d/%m/%Y') AND STR_TO_DATE(:endDate, '%d/%m/%Y') "
			+ " ORDER BY "
			+ "   t.user.user, STR_TO_DATE(t.startTime, '%d/%m/%Y %H:%i:%s') ")
	List<Trace> findByDateBetween(@Param("startDate") String startDate, @Param("endDate") String endDate);
	
	@Query(value=" "
			+ " SELECT t FROM Trace t "
			+ " WHERE "
			+ "   t.user.userId = :id "
			+ "   AND STR_TO_DATE(t.startTime, '%d/%m/%Y') = STR_TO_DATE(:date, '%d/%m/%Y') "
			+ " ORDER BY "
			+ "   t.user.user, STR_TO_DATE(t.startTime, '%d/%m/%Y %H:%i:%s') ")
	List<Trace> findByDate(@Param("id") long id, @Param("date") String date);

	@Query(value=" " +
			"  select " + 
			"    t.latitude, " + 
			"    t.longitude, " + 
			"    substring(t.start_time, 1, 16) startTime, " + 
			"    t.user_id userId, " + 
			"    u.name userName, " + 
			"    s.start_time actionStartTime, " + 
			"    s.description actionDescription, " + 
			"    s.company_name client " + 
			"  from " + 
			"    trace t " + 
			"    inner join user u on (t.user_id = u.user_id) " + 
			"    left join ( " + 
			"      select " + 
			"        s.user_id, " + 
			"        s.start_time, " + 
			"        a.description, " + 
			"        c.company_name " + 
			"      from " + 
			"        services s " + 
			"        inner join action a on s.action_id = a.action_id " + 
			"        inner join client c on s.client_id = c.client_id " + 
			"      where " + 
			"        s.service_id in (select max(service_id) from services group by user_id) " + 
			"    ) s on t.user_id = s.user_id " + 
			"  where " + 
			"    u.active = 1 " + 
			"    and (t.trace_id in (select max(trace_id) from trace group by user_id)) "
			, nativeQuery=true)
	Object[][] findAllByGroupUser();

}
