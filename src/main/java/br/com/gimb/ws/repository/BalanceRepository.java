package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.Balance;

@Repository
public interface BalanceRepository extends CrudRepository<Balance, Long> {
	
	@Query(value = "" +
			" SELECT  \n" + 
			"	user_id, payment_type_id, MAX(STR_TO_DATE(CONCAT(day, '/', month, '/', year), '%d/%m/%Y')) as max_date, card_id \n" + 
			" FROM \n" + 
			"	balance b \n" + 
			" WHERE \n" + 
			"	(:userId is null || :userId = b.user_id) AND \n" + 
			"	(:cardId IS NULL || :cardId = b.card_id) AND" + 
			"	(:typePaymentId is null || :typePaymentId = b.payment_type_id) \n" + 
			" GROUP BY \n" + 
			"	user_id, payment_type_id, card_id", nativeQuery = true)
	 List<Object[]> findLastBalanceByUserAndTypePayment(@Param("userId") Long userId, @Param("typePaymentId") Long typePaymentId, @Param("cardId") Long cardId);
		
	@Query(value = " " +
			  " SELECT  " +
			  " *FROM "+
			  "   balance  " + 
			  " WHERE " +
			  "   (:userId IS NULL || user_id = :userId) AND " +
			  "   payment_type_id = :typePaymentId AND "
			  + "(:cardId is null || card_id = :cardId) AND" +
			  "   STR_TO_DATE(CONCAT(day, '/', month, '/', year), '%d/%m/%Y') = STR_TO_DATE(:date, '%d/%m/%Y')", nativeQuery = true)
	Balance findAllByUserAndTypePaymentAndDate(@Param("userId") Long userId, @Param("typePaymentId") Long typePaymentId, @Param("date") String date, @Param("cardId") Long cardId);
	
	@Query(value = " " +
			  " SELECT " +
			  " *FROM "+
			  "   balance b " + 
			  " WHERE " +
			  "   b.user_id = :userId AND " +
			  "   b.payment_type_id = :typePaymentId AND " +
			  "	  (b.card_id = :cardId) AND" +
			  "   STR_TO_DATE(CONCAT(b.day, '/', b.month, '/', b.year), '%d/%m/%Y') = STR_TO_DATE(:date, '%d/%m/%Y') ", nativeQuery = true)
	Balance findAllByUserAndTypePaymentAndDateCard(@Param("userId") Long userId, @Param("typePaymentId") Long typePaymentId, @Param("date") String date, @Param("cardId") Long cardId);
	
	@Query(value = " " +
			  " SELECT b " +
			  " FROM "+
			  "   Balance b " + 
			  " WHERE " +
			  "   b.user.userId = :userId AND " +
			  "   b.typePayment.typePaymentId = :typePaymentId AND " +
			  "   STR_TO_DATE(CONCAT(b.day, '/', b.month, '/', b.year), '%d/%m/%Y') >= STR_TO_DATE(:date, '%d/%m/%Y') " +
			  " ORDER BY " +
			  "   STR_TO_DATE(CONCAT(b.day, '/', b.month, '/', b.year), '%d/%m/%Y') " )
	List<Balance> findAllByUserAndTypePaymentAndDateEqualThanGreater(@Param("userId") Long userId, @Param("typePaymentId") Long typePaymentId, @Param("date") String date);
	
	@Query(value = " " +
			  " SELECT b.balance " +
			  " FROM "+
			  "   balance b " + 
			  " WHERE " +
			  "   (:userId IS NULL || b.user_id = :userId) AND " +
			  "   (:cardId IS NULL || b.card_id = :cardId) AND " +
			  "   b.payment_type_id = :typePaymentId AND " +
			  "   STR_TO_DATE(CONCAT(b.day, '/', b.month, '/', b.year), '%d/%m/%Y') < STR_TO_DATE(:date, '%d/%m/%Y') " +
			  " ORDER BY " +
			  "   STR_TO_DATE(CONCAT(b.day, '/', b.month, '/', b.year), '%d/%m/%Y') DESC " +
			  " LIMIT 1 ", nativeQuery = true )
	Double findPriorBalance(@Param("userId") Long userId, @Param("typePaymentId") Long typePaymentId, @Param("date") String date, @Param("cardId")Long cardId);
	
}
