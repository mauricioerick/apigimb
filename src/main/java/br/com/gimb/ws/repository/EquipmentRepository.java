package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.Equipment;

@Repository
public interface EquipmentRepository extends CrudRepository<Equipment, Long> {
	List<Equipment> findByDescription(String description);

	List<Equipment> findByActive(Boolean active);

	List<Equipment> findAll();
}
