package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.Document;

@Repository
public interface DocumentRepository extends CrudRepository<Document, Long>{
	List<Document> findAll();
	
	Document findByDocumentId(long id);
}
