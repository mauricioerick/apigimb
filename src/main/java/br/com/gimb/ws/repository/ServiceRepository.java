package br.com.gimb.ws.repository;

import br.com.gimb.ws.model.*;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceRepository extends CrudRepository<Services, Long> {

	@Query(value = " SELECT s FROM Services s ORDER BY STR_TO_DATE(s.startTime, '%d/%m/%Y'), s.user, s.client ")
	List<Services> findAll();

	List<Services> findByUser(User user);

	List<Services> findByUserAndStatus(User user, String status);

	@Query(value = " SELECT s " + "FROM Services s " + "WHERE STR_TO_DATE(s.startTime, '%d/%m/%Y') "
			+ "BETWEEN STR_TO_DATE(:startDate, '%d/%m/%Y') " + "AND STR_TO_DATE(:endDate, '%d/%m/%Y') "
			+ "AND (:user IS NULL OR :user = s.user) " + "ORDER BY STR_TO_DATE(s.startTime, '%d/%m/%Y %H:%i:%s'), s.user ")
	List<Services> findAllByStartTimeLessThanEqualAndStartTimeGreaterThanEqual(@Param("startDate") String startDate,
			@Param("endDate") String endDate, @Param("user") User user);

	@Query(value = " SELECT s FROM Services s WHERE s.client.document = :user AND STR_TO_DATE(s.startTime, '%d/%m/%Y') BETWEEN STR_TO_DATE(:startDate, '%d/%m/%Y') AND STR_TO_DATE(:endDate, '%d/%m/%Y') ORDER BY STR_TO_DATE(s.startTime, '%d/%m/%Y %H:%i:%s'), s.user ")
	List<Services> findAllByUserClientAndStartTimeLessThanEqualAndStartTimeGreaterThanEqual(@Param("user") String user,
			@Param("startDate") String startDate, @Param("endDate") String endDate);

	List<Services> findByUserAndClientAndVehicleAndStartTime(User user, Client client, Vehicle vehicle, String startTime);

	List<Services> findByProject(Project project);

	@Query(value = "SELECT s FROM Services s WHERE s.client.clientId = :client AND s.vehicle.vehicleId = :vehicle AND s.action.actionId = :action "
			+ " AND s.startTime = :startDate AND s.endTime = :endDate and s.user.userId = :user " )
	Services findByUserAndClientAndVehicleAndStartTimeAndEndDateAndAction(@Param("startDate") String startDate, @Param("endDate") String endDate, @Param("user") Long user, 
			@Param("client") Long client, @Param("vehicle") Long vehicle, @Param("action") Long action);
}
