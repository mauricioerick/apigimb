package br.com.gimb.ws.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.gimb.ws.model.ServiceEvents;
import br.com.gimb.ws.model.Services;

@Repository
public interface ServiceEventRepository extends CrudRepository<ServiceEvents,Long> {
	List<ServiceEvents> findByServico(Services service);
}
