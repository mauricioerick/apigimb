package br.com.gimb.ws.enumerated;

public enum EmailDomainEnum {
	
	REQUEST_CREDIT("REQUEST_CREDIT");
	
	private String name;
	
	private EmailDomainEnum(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
}
