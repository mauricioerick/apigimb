package br.com.gimb.ws.enumerated;

public enum ExpenseStatusEnum {
	
	OPEN("OP", "InProgress"),
	WAIT_PAYMENT("WP", "AwaitingPayment"),
	PAID("PA", "PaidOut"),
	CANCEL("CA", "Canceled"),
	WAIT_APPR("WA", "WaitingForApproval");
	
	
	ExpenseStatusEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}
	
	private String code;
	private String description;
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
}
