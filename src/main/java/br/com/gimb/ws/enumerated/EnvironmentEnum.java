package br.com.gimb.ws.enumerated;

public enum EnvironmentEnum {
	
	LCL("Local"),
	POC("Proof of Concept"),
	PRD("Production");
	
	private String description;
	
	private EnvironmentEnum(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
