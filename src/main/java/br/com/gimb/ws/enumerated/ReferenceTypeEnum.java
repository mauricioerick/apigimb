package br.com.gimb.ws.enumerated;

public enum ReferenceTypeEnum {
	
    LQ("LQ", "Settlements"),
    SE("SE", "Services"),
    IN("IN", "Inventory"),
    IH("IH", "Inventory History"),
	DC("DC", "Document");

    private String code;

    private String description;

    ReferenceTypeEnum(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return getDescription();
    }
	
}
