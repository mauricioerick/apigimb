package br.com.gimb.ws.enumerated;

public enum TimeLineTypeEnum {

    NON("NON", "None"),
    TRA("TRA", "PointRecords"),
    TEV("TEV", "EventRecord"),
    SER("SER", "ServiceOrders"),
    SET("SET", "Settlements"),
    GEN("GEN", "General");

    private String code;
    private String description;

    TimeLineTypeEnum(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
