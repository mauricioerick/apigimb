package br.com.gimb.ws.enumerated;

public enum StatusExtractEnum {
	
    A("A", "Approved"),
    P("P", "Pending"),
    R("R", "Reproved");

    private String code;

    private String description;

    StatusExtractEnum(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return getDescription();
    }
	
}