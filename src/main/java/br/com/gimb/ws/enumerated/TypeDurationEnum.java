package br.com.gimb.ws.enumerated;

public enum TypeDurationEnum {
	
	D("D", "DAY"),
	M("M", "MOTH"),
	W("W", "WEEK");

    TypeDurationEnum(String code, String description) {
		this.code = code;
    	this.description = description;
    }

    private String description;
    private String code;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
