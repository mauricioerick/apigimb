package br.com.gimb.ws.enumerated;

public enum SendServiceOrderStatusEnum {
	
    P("P", "Pending"),
    R("R", "Received"),
    D("D", "Deleted");

    private String code;

    private String description;

    SendServiceOrderStatusEnum(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return getDescription();
    }
	
}