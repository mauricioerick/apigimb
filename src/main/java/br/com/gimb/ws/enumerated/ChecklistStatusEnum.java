package br.com.gimb.ws.enumerated;

public enum ChecklistStatusEnum {
    OK("OK","OK"),
    FIXED("FIX", "FIXED"),
    NOT_FIXED("NOK", "NOT FIXED");

    ChecklistStatusEnum(String code, String description){
        this.code = code;
        this.description = description;
    }

    private String code;
    private String description;

    public String getCode() {
        return code;
    }
    public String getDescription() {
        return description;
    }
}
