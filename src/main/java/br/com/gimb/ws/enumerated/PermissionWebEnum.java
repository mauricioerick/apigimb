package br.com.gimb.ws.enumerated;

public enum PermissionWebEnum {

    ServcePicInsert("INSERTPICSERVICE"),
	ServcePicChange("CHANGEPICSERVICE"),
	ServcePicDelete("DELETEPICSERVICE"),
	ServcePicEdit("EDITPICSERVICE"),
	SettlementPicInsert("INSERTPICSETTLEMENTE"),
	SettlementPicChange("CHANGEPICSETTLEMENTE"),
	SettlementPicDelete("DELETEPICSETTLEMENTE"),
	SettlementPicEdit("EDITPICSETTLEMENTE"),
	filtrarServicoUsuario("FILTRARSERVICOUSUARIO"),
	filtrarLiquidacaoUsuario("FILTRARLIQUIDACAOUSUARIO"),
	filtrarApontamentoUsuario("FILTRARAPONTAMENTOUSUARIO"),
	filtrarServicoArea("FILTRARSERVICOAREA"),
	filtrarLiquidacaoArea("FILTRARLIQUIDACAOAREA"),
	filtrarApontamentoArea("FILTRARAPONTAMENTOAREA");
	
	private String description;

	private PermissionWebEnum(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
