package br.com.gimb.ws.enumerated;

public enum ClassesEnum {
	CLIENT("CLIENT"),
	AREA("AREA"),
	EQUIPMENT("EQUIPMENT"),
	CHECKLISTSERVICES("CHECKLISTSERVICES");

	private ClassesEnum(String description) {
		this.description = description;
	}
	
	private String description;
	
	public String getDescription() {
		return description;
	}
}
