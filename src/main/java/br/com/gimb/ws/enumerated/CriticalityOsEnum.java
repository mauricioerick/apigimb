package br.com.gimb.ws.enumerated;

public enum CriticalityOsEnum {
    BAIXO("B","BAIXO"),
    MEDIO("M", "MEDIO"),
    ALTO("A","ALTO");

    CriticalityOsEnum(String code, String description){
        this.code = code;
        this.description = description;
    }

    private String code;
    private String description;

    public String getCode() {
        return code;
    }
    public String getDescription() {
        return description;
    }
}
