package br.com.gimb.ws.enumerated;

public enum ExportExcelDomainEnum {
	
	EXTRACT("EXTRACT"),
	EXTRACTCARD("EXTRACTCARD"),
	INVENTORY("INVENTORY"),
	SERVICE("SERVICE"),
	POINT("POINT");
	
	private String name;
	
	private ExportExcelDomainEnum(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
}
