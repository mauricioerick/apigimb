package br.com.gimb.ws.enumerated;

public enum AppearsOnEnum {
	
	OS("OS", "SO"),
	LQ("LQ", "Settlements"),
	IV("IV", "Inventory");
	
	AppearsOnEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}
	
	private String code;
	private String description;
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
}
