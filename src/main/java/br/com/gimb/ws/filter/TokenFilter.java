package br.com.gimb.ws.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.filter.GenericFilterBean;

import br.com.gimb.ws.util.Util;
import io.jsonwebtoken.Jwts;

public class TokenFilter extends GenericFilterBean {
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
	throws IOException, ServletException{
		
		HttpServletRequest req = (HttpServletRequest)request;
		String header = req.getHeader("Authorization");
		String role = req.getHeader("Role");
		
		if(role != null) {
			Util.setRole(role);
		}
		
		if(header == null || !header.startsWith("Bearer ")) {
			throw new ServletException("Token inválido");
		}
		
		String token = header.substring(7);
		
		try {
			Jwts.parser().setSigningKey(Util.KEY_TOKEN).parseClaimsJws(token).getBody();
			chain.doFilter(request, response);
		} catch (Exception e) {
			((HttpServletResponse)response).sendError(HttpServletResponse.SC_UNAUTHORIZED, "Token inválido");
			((HttpServletResponse)response).setHeader("Location", "/apiGIMB/login");
		}
	}
	
}
