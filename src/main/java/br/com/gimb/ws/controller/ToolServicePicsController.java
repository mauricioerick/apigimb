package br.com.gimb.ws.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.gimb.ws.model.ToolServicesPics;
import br.com.gimb.ws.service.ToolServicePicsService;

@RestController
@RequestMapping("/admin")
public class ToolServicePicsController extends BaseController {

	@Autowired
	private ToolServicePicsService toolServicePicsService;

	@RequestMapping(value = "/toolServicePics", method = RequestMethod.GET)
	@ResponseBody
	public List<ToolServicesPics> index() {
		setRepositories();

		return toolServicePicsService.findAll();
	}

	@RequestMapping(value = "/toolServicePics/{id}", method = RequestMethod.GET)
	public ToolServicesPics findById(@PathVariable long id) {
		setRepositories();
		return toolServicePicsService.findById(id);
	}

	@RequestMapping(value = "/toolServicePicsCreate", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ToolServicesPics> create(@RequestBody ToolServicesPics toolServicePics) {
		setRepositories();

		try {
			toolServicePicsService.save(toolServicePics);
		} catch (Exception e) {
			toolServicePics = null;
			e.printStackTrace();
			return new ResponseEntity<ToolServicesPics>(toolServicePics, HttpStatus.CONFLICT);
		}

		return new ResponseEntity<ToolServicesPics>(toolServicePics, HttpStatus.CREATED);
	}

	@RequestMapping("/deleteToolServicePics/{id}")
	@ResponseBody
	public ResponseEntity<ToolServicesPics> delete(@PathVariable long id) {
		setRepositories();

		ToolServicesPics tool = toolServicePicsService.findById(id);

		try {
			toolServicePicsService.delete(tool);
		} catch (Exception e) {
			return new ResponseEntity<ToolServicesPics>(tool, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<ToolServicesPics>(new ToolServicesPics(), HttpStatus.OK);
	}

	@RequestMapping(value = "/updateToolServicePics/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<ToolServicesPics> update(@RequestBody ToolServicesPics toolServicePics, @PathVariable long id) {
		setRepositories();

		try {
			// toolServicePics.setToolId(id);
			toolServicePicsService.save(toolServicePics);
		} catch (Exception e) {
			return new ResponseEntity<ToolServicesPics>(toolServicePics, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<ToolServicesPics>(toolServicePics, HttpStatus.OK);
	}
    
}
