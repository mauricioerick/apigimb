package br.com.gimb.ws.controller;

import br.com.gimb.ws.model.Card;
import br.com.gimb.ws.model.User;
import br.com.gimb.ws.service.CardService;
import br.com.gimb.ws.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/admin")
public class CardController extends BaseController {
	@Autowired
	CardService cardService;

	@Autowired
	UserService userService;
	
	@RequestMapping(value = "/allCards")
	@ResponseBody
	public List<Card> allCard(){
		setRepositories();
		return cardService.findAll();
	}

	@RequestMapping(value = "/avaibleCards")
	@ResponseBody
	public List<Card> avaibleCards(){
		setRepositories();
		return cardService.avaibleCards();
	}
	
	@RequestMapping(value = "/card/{cardId}")
	@ResponseBody
	public Card card(@PathVariable long cardId){
		setRepositories();
		return cardService.findByCardId(cardId);
	}
	
	@RequestMapping(value = "/saveCard")
	public ResponseEntity<Card> saveCard(@RequestBody Card card){
		setRepositories();
		try {
			Card currentCard = cardService.findByCardId(card.getCardId());
			if(currentCard != null){
				card.setUser(currentCard.getUser());
			}
			cardService.save(card);
			
			return new ResponseEntity<Card>(card, HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<Card>(card, HttpStatus.EXPECTATION_FAILED);
		}
	}

	@PostMapping(value = "/card/linkUser/{cardId}&{userId}")
	public ResponseEntity<Object> unlinkCard(@PathVariable long cardId,@PathVariable long userId){
		setRepositories();
		try{
			Card card = cardService.findByCardId(cardId);
			if(userId > 0){
				card.setUser(userService.findByUserId(userId));
			}else {
				card.setUser(null);
			}
			cardService.save(card);
			return new ResponseEntity<Object>(true, HttpStatus.OK);
		}catch (Exception e){
			return new ResponseEntity<Object>(false, HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	@RequestMapping(value = "/getUserByCard/{cardId}")
	@ResponseBody
	public User getUserByCard(@PathVariable long cardId) {
		User user = new User();
		try {
			Card card = cardService.findByCardId(cardId);
			user = card.getUser();
			
		}catch (Exception e) {
			return null;
		}
		
		return user;
	}
	
	@GetMapping(value = "/getCardBetweentData/{dtInicio}&{dtFinal}")
	@ResponseBody
	public List<Card> getCardsBetweenDate(@PathVariable Long dtInicio, @PathVariable Long dtFinal){
		setRepositories();
		try {
			List<Card> cards = cardService.findCardBetweenDate(dtInicio, dtFinal);
			return cards;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
