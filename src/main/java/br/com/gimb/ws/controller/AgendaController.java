package br.com.gimb.ws.controller;

import br.com.gimb.ws.model.Agenda;
import br.com.gimb.ws.model.AgendaItem;
import br.com.gimb.ws.requests.RequestAgenda;
import br.com.gimb.ws.service.AgendaItemService;
import br.com.gimb.ws.service.AgendaService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/admin")
public class AgendaController extends BaseController {

    private final AgendaService agendaService;
    private final AgendaItemService agendaItemService;

    @Autowired
    public AgendaController(AgendaService agendaService, AgendaItemService agendaItemService) {
        this.agendaService = agendaService;
        this.agendaItemService = agendaItemService;
    }

    @GetMapping("/agenda")
    public ResponseEntity<?> get(RequestAgenda requestAgenda) throws ParseException {
        setRepositories();
        return new ResponseEntity<>(agendaService.listByAgendaWithJoin(requestAgenda), HttpStatus.OK);
    }

    @GetMapping("/agenda/{id}")
    public ResponseEntity<?> find(@PathVariable("id") Long id) {
        setRepositories();
        return new ResponseEntity<>(agendaService.findById(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/agenda", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Agenda> create(@RequestBody Agenda agenda) {
		setRepositories();

		try {
            Agenda schedule = new Agenda();
            schedule.setDate(agenda.getDate());
            schedule.setUserId(agenda.getUserId());

            List<AgendaItem> agendaItem = agenda.getAgendaItems();

			agendaService.save(agenda);

            if (agendaItem.size() > 0) {
                for (AgendaItem item : agendaItem) {
                    item.setAgendaId(agenda.getAgendaId());
                    agendaItemService.save(item);
                }
            }
		} catch (Exception e) {
			agenda = null;
			e.printStackTrace();
			return new ResponseEntity<Agenda>(agenda, HttpStatus.CONFLICT);
		}

		return new ResponseEntity<Agenda>(agenda, HttpStatus.CREATED);
	}

    @RequestMapping(value = "/updateAgenda/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Agenda> update(@RequestBody Agenda agenda, @PathVariable long id) {
		setRepositories();

		try {
			Agenda schedule = agendaService.findById(id);
            schedule.setDate(agenda.getDate());
            schedule.setUserId(agenda.getUserId());

            List<AgendaItem> agendaItem = agenda.getAgendaItems();

			agendaService.save(agenda);

            if (agendaItem.size() > 0) {
                for (AgendaItem item : agendaItem) {
                    item.setAgendaId(agenda.getAgendaId());
                    agendaItemService.save(item);
                }
            }
		} catch (Exception e) {
			return new ResponseEntity<Agenda>(agenda, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<Agenda>(agenda, HttpStatus.OK);
	}

    // @PostMapping("/agenda")
    // public ResponseEntity<?> post(@RequestBody Agenda agendaRequest) {
    //     setRepositories();

    //     Optional<Agenda> optionalAgenda = agendaService.findByUserIdAndDate(agendaRequest);
    //     if(optionalAgenda.isPresent()) {
    //         if(agendaRequest.getAgendaItems().size() > 0) {
    //             List<AgendaItem> agendaItemList = agendaRequest.getAgendaItems().stream().map(item -> {
    //                 item.setAgenda(optionalAgenda.get());
    //                 item.setAgendaId(optionalAgenda.get().getAgendaId());
    //                 return item;
    //             }).collect(Collectors.toList());

    //             agendaItemService.saveAll(agendaItemList);
    //         }
    //         return new ResponseEntity<>(agendaService.findById(optionalAgenda.get().getAgendaId()), HttpStatus.OK);
    //     }

    //     Agenda newAgenda = agendaService.save(agendaRequest);
    //     newAgenda.setAgendaItems(agendaRequest.getAgendaItems().stream().map(agendaItem -> {
    //         agendaItem.setAgenda(newAgenda);
    //         agendaItem.setAgendaId(newAgenda.getAgendaId());
    //         return agendaItemService.save(agendaItem);
    //     }).collect(Collectors.toList()));

    //     return new ResponseEntity<>(newAgenda, HttpStatus.CREATED);
    // }

    @DeleteMapping("/agenda/{id}")
    public ResponseEntity<?> del(@PathVariable("id") Long id) throws NotFoundException {
        setRepositories();
        Optional<Agenda> optionalAgenda = Optional.ofNullable(agendaService.findById(id));
        if(optionalAgenda.isPresent()) {
            agendaService.deleteById(id);
        }

        throw new NotFoundException("Agenda não Existe");
    }
}
