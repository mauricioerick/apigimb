package br.com.gimb.ws.controller;

import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.gimb.ws.service.ImportsService;

@RestController
public class modelXlsDownload {
	@Autowired
	ImportsService importsService;
	
	@RequestMapping(value = "/downloadModel", method = RequestMethod.GET)
	public void downloadModel(HttpServletResponse response, HttpServletRequest request) {	
		try {
			Map<String, String[]> params = request.getParameterMap();
			String filePath = params.get("filePath")[0];
			String mime = params.get("mime")[0]; 
			
			byte[] model = importsService.downloadModel(filePath);
			
			ServletOutputStream sos = response.getOutputStream();
			
			if(mime.equalsIgnoreCase("xls"))
				response.setContentType("application/vnd.ms-excel");
			else if(mime.equalsIgnoreCase("xlsx"))
				response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			
			response.setHeader("Content-Disposition", "attachment; filename="+filePath+"."+mime.toLowerCase());
			
			sos.write(model);
			sos.flush();
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
}
