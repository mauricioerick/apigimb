package br.com.gimb.ws.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.gimb.ws.DTO.LocationDTO;
import br.com.gimb.ws.DTO.TimeLineFeedDTO;
import br.com.gimb.ws.model.Action;
import br.com.gimb.ws.model.Client;
import br.com.gimb.ws.model.Equipment;
import br.com.gimb.ws.model.Trace;
import br.com.gimb.ws.model.User;
import br.com.gimb.ws.service.ActionService;
import br.com.gimb.ws.service.ClientService;
import br.com.gimb.ws.service.DashboardsService;
import br.com.gimb.ws.service.EquipmentService;
import br.com.gimb.ws.service.TraceService;
import br.com.gimb.ws.service.UserService;

@RestController
@RequestMapping("/admin")
public class DashboardsController extends BaseController {
	
	@Autowired
	private DashboardsService dsService;
	
	@Autowired
	private TraceService traceService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ClientService clientService;
	
	@Autowired
	private ActionService actionService;
	
	@Autowired
	private EquipmentService equipmentService;
	
	@RequestMapping(value = "/ds/listUsers", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<User> listUsers() {
		setRepositories(); 
		List<User> users = userService.findAll();		
		users.sort((User u1, User u2)->u1.getUser().compareToIgnoreCase(u2.getUser()));
		
		return users;
	}
	
	@RequestMapping(value = "/ds/listClients", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Client> listClients() {
		setRepositories(); 
		List<Client> clients = clientService.findAllBySituacao("true");		
		clients.sort((Client c1, Client c2)->c1.getTradingName().compareToIgnoreCase(c2.getTradingName()));
		
		return clients;
	}
	
	@RequestMapping(value = "/ds/timeLineFeedsByDateBetween", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<TimeLineFeedDTO> findTimeLineFeedsByDateBetween(@RequestBody  Map<String, String> params) {
		setRepositories(); 
		try {
			Boolean groupByDate = "true".equalsIgnoreCase(params.get("groupByDate"));
			String startDate = params.get("startDate");
			String endDate = params.get("endDate");
			
			String[] client = null;
			if(params.get("client") != null)
				client = params.get("client").split(",");
			
			String[] user = null;
			if(params.get("user") != null)
				user = params.get("user").split(",");
			
			Long userLogadoId = Long.parseLong(params.get("userLogadoId").toString());
			
			List<String> cid = new ArrayList<String>();
			if(client != null)
				for(String c : client)
					cid.add(c);
			
			List<String> ur = new ArrayList<>();
			if(user != null)
				for(String u : user)
					ur.add(u);
			
			return dsService.findTimeLineFeedsByDateBetween(groupByDate, startDate, endDate, cid, ur, userLogadoId);
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@RequestMapping(value = "/ds/findTracesByDateBetween", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Trace> findTracesByDateBetween(@RequestBody  Map<String, String> params) {
		setRepositories();
		String[] users = null;
		
		if(params.get("user") != null)
			users = params.get("user").split(",");
		
		String startDate = params.get("startDate");
		String endDate = params.get("endDate");
		
		List<Trace> traces = traceService.findByDateBetween(startDate, endDate);
		
		if(users != null && users.length > 0)
			traces = filterTracesByUser(users, traces);
		
		return traces;
	}
	
	@RequestMapping(value = "/op/findAllByGroupUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody	
	public List<LocationDTO> findAllByGroupUser() {
		setRepositories(); 
		return traceService.findAllByGroupUser();
	}
	
	@RequestMapping(value = "/ds/listAction", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Action> listActions() {
		setRepositories();
		List<Action> actions = actionService.findAll();
		actions.sort((Action a1, Action a2)->a1.getDescription().compareToIgnoreCase(a2.getDescription()));
		
		Action action = new Action();
		action.setDescription("TODOS");
		
		actions.add(0, action);
		return actions;
		
	}
	
	@RequestMapping(value = "/ds/equipment", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Equipment> index() {
		setRepositories(); 
		List<Equipment>equipments = equipmentService.findAll();
		
		Equipment equipment = new Equipment();
		equipment.setDescription("TODOS");
		equipments.add(0, equipment);
		
		return equipments;
	}
	
	private List<Trace> filterTracesByUser(String[] users, List<Trace> tmpTrace) {
		List<Trace> traces = new ArrayList<>();
		
		for(Trace trace : tmpTrace) {
			traces.add(trace);
			
			for(String user : users) {
				int idx = 1;
				
				if(trace.getUser().getUser().equalsIgnoreCase(user)) 
					break;
				
				if(!trace.getUser().getUser().equalsIgnoreCase(user) && idx == users.length) {
					int indexTrace = traces.indexOf(trace);
					traces.remove(indexTrace);
				}
				idx ++;
			}
		}
				
		return traces;		
	}
	
}
