package br.com.gimb.ws.controller;

import br.com.gimb.ws.enumerated.ReferenceTypeEnum;
import br.com.gimb.ws.model.CustomField;
import br.com.gimb.ws.model.CustomPicture;
import br.com.gimb.ws.model.ObjectHistory;
import br.com.gimb.ws.model.ObjectInventory;
import br.com.gimb.ws.service.CustomFieldService;
import br.com.gimb.ws.service.CustomPictureService;
import br.com.gimb.ws.service.ObjectHistoryService;
import br.com.gimb.ws.service.ObjectService;
import br.com.gimb.ws.service.StorageService;
import br.com.gimb.ws.service.UserService;
import br.com.gimb.ws.util.aws.AwsS3Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@RestController
@RequestMapping("/admin")
public class ObjectController extends BaseController {

	@Autowired
    private ObjectService objectService;
    
    @Autowired
	private ObjectHistoryService objectHistoryService;

	@Autowired
	private StorageService storageService;

	@Autowired
	private UserService userService;
	
	@Autowired
	private CustomFieldService customFieldService;
	
	@Autowired
	private CustomPictureService customPictureService;

	@RequestMapping(value = "/object", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<ObjectInventory> index() {
		setRepositories();
		List<ObjectInventory> list = objectService.findAll();
		if (list != null && list.size() > 0) {
			list.stream().sorted((e1, e2) -> e1.getIdentifier().compareTo(e2.getIdentifier()));
			for (ObjectInventory objectInventory : list) {
				objectInventory.setCustomFieldList(customFieldService.findByReferenceTypeAndReferenceId(ReferenceTypeEnum.IN, String.valueOf(objectInventory.getObjectId())));	
			}
		}
		return list;
	}

	@RequestMapping(value = "/objectByActive/{active}", method = RequestMethod.GET)
	public List<ObjectInventory> eventByActive(@PathVariable String active) {
		setRepositories();
		return objectService.findByActive(active);
	}

	@RequestMapping(value = "/object/{id}", method = RequestMethod.GET)
	public ObjectInventory findById(@PathVariable long id) {
		setRepositories();
		ObjectInventory objectInventory = objectService.findById(id);
		if (objectInventory != null) {
			objectInventory.setCustomFieldList(customFieldService.findByReferenceTypeAndReferenceId(ReferenceTypeEnum.IN, String.valueOf(objectInventory.getObjectId())));
			objectInventory.setCustomPictureList(customPictureService.findByReferenceTypeAndReferenceId(ReferenceTypeEnum.IN, String.valueOf(objectInventory.getObjectId())));
			objectInventory.getObjectHistoryList().forEach(objectHistory -> {
				objectInventory.getCustomFieldList().addAll(customFieldService.findByReferenceTypeAndReferenceId(ReferenceTypeEnum.IH, String.valueOf(objectHistory.getObjectHistoryId())));
				objectInventory.getCustomPictureList().addAll(customPictureService.findByReferenceTypeAndReferenceId(ReferenceTypeEnum.IH, String.valueOf(objectHistory.getObjectHistoryId())));
			});
		}
		AwsS3Utils.generatePreSignedUrl(objectInventory.getCustomPictureList(), baseName());
		return objectInventory;
	}
	
	@RequestMapping(value = "/createObject", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)	
	public ResponseEntity<ObjectInventory> create(@RequestBody  ObjectInventory object) {
		setRepositories();

		try {
			List<ObjectHistory> listObjectHistory = object.getObjectHistoryList();
			List<CustomField> customFields = object.getCustomFieldList();
			List<CustomPicture> customPictures = object.getCustomPictureList();
			
            ObjectInventory persistedObject = objectService.findByIdentifier(object.getIdentifier());
            StringBuilder historyNote = new StringBuilder();
            if (persistedObject != null) {
				object.setObjectId(persistedObject.getObjectId());
				object.setUserCreate(persistedObject.getUserCreate());
				object.setDateTimeCreate(persistedObject.getDateTimeCreate());
				
//				List<CustomField> oldCustomFields = customFieldService.findByReferenceTypeAndReferenceId(ReferenceTypeEnum.IN, String.valueOf(persistedObject.getObjectId()));
//				if (oldCustomFields != null) {
//					for (CustomField customField : oldCustomFields) {
//						historyNote.append(String.format("%s: %s", customField.getCustomField(), customField.getCustomFieldValue()));
//						customFieldService.delete(customField);
//					}
//				}
            }
            
            object.setObjectHistoryList(null);
            object.setCustomFieldList(null);
            object.setCustomPictureList(null);
			object.setActive(true);

            objectService.save(object);

			ObjectHistory lastObjectHistory = null;
            
            if (listObjectHistory != null) {
                for (ObjectHistory objectHistory : listObjectHistory) {
					objectHistory.setObject(object);
					objectHistory.setStorage(storageService.findById(objectHistory.getStorage().getStorageId()));
					objectHistory.setUser(userService.findById(objectHistory.getUser().getUserId()));
					
					if (historyNote.length() > 1 && listObjectHistory.size() > 1 && listObjectHistory.get(listObjectHistory.size() -1).equals(objectHistory)) {
						if (objectHistory.getNote() == null) objectHistory.setNote("");
						
						objectHistory.setNote(String.format("%s\n%s", objectHistory.getNote(), historyNote.toString()));
					}

					lastObjectHistory = objectHistoryService.save(objectHistory);
                }
            }
            
            if (customFields != null) {
    			for (CustomField customField : customFields) {

					if(lastObjectHistory != null){
						customField.setReferenceId(String.valueOf(lastObjectHistory.getObjectHistoryId()));
						customField.setReferenceType(ReferenceTypeEnum.IH);
					}else {
						customField.setReferenceId(String.valueOf(object.getObjectId()));
					}
					customFieldService.save(customField);
    			}
			}
            
            if (customPictures != null) {
            	int idx = 1;
				for(CustomPicture customPic : customPictures) {
					if(customPic.getPic() != null) {
						if(lastObjectHistory != null){
							customPic.setReferenceId(String.valueOf(lastObjectHistory.getObjectHistoryId()));
							customPic.setReferenceType(ReferenceTypeEnum.IH);
							customPic.setMessage(customPic.getMessage() +" "+ new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
						}else {
							customPic.setReferenceId(String.valueOf(object.getObjectId()));
						}
						customPic = customPictureService.convertBase64ToImage(customPic, idx, AwsS3Utils.initializeS3Client(baseName()), baseName());
						customPictureService.save(customPic);
						idx++;
					}
				}
            }
		} catch (Exception e) {
			object = null;
			return new ResponseEntity<ObjectInventory>(object, HttpStatus.CONFLICT);
		}

		return new ResponseEntity<ObjectInventory>(object, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/updateObject/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<ObjectInventory> update(@RequestBody  ObjectInventory object, @PathVariable long id) {		
		setRepositories();

		try {
			object.setObjectId(id);
			object.setObjectHistoryList(null);
			object.setCustomFieldList(null);
			object.setCustomPictureList(null);
			objectService.save(object);
		} catch (Exception e) {
			return new ResponseEntity<ObjectInventory>(object, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<ObjectInventory>(object, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/object-inventory/{value}", method = RequestMethod.GET)
	public ObjectInventory findByValue(@PathVariable String value) {
		setRepositories();
		ObjectInventory objectInventory = objectService.findByIdentifierOrSerialNumberOrPatrimony(value, value, value);
		if (objectInventory != null) {

			objectInventory.getObjectHistoryList().forEach(objectHistory -> {
				objectHistory.setCustomFieldList(customFieldService.findByReferenceTypeAndReferenceId(ReferenceTypeEnum.IH, String.valueOf(objectHistory.getObjectHistoryId())));
				objectHistory.setCustomPictureList(customPictureService.findByReferenceTypeAndReferenceId(ReferenceTypeEnum.IH, String.valueOf(objectHistory.getObjectHistoryId())));
			});
			
			if (objectInventory.getUserCreate() != null)
				objectInventory.getUserCreate().setProfile(null);
			
			if (objectInventory.getUserUpdate() != null)
				objectInventory.getUserUpdate().setProfile(null);
			
		}
		return objectInventory;
	}
}
