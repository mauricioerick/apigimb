package br.com.gimb.ws.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.gimb.ws.model.Storage;
import br.com.gimb.ws.service.StorageService;


@RestController
@RequestMapping("/admin")
public class StorageController extends BaseController {

	@Autowired
	private StorageService storageService;

	@RequestMapping(value = "/storage", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object index() {
		setRepositories();
		List<Storage> list = storageService.findAll();
		if (list != null)
			list.stream().sorted((e1, e2) -> e1.getDescription().compareTo(e2.getDescription()));
		return list;
	}

	@RequestMapping(value = "/storageByActive/{active}", method = RequestMethod.GET)
	public List<Storage> eventByActive(@PathVariable String active) {
		setRepositories();
		return storageService.findByActive(active);
	}

	@RequestMapping(value = "/storage/{id}", method = RequestMethod.GET)
	public Object findById(@PathVariable long id) {
		setRepositories();
		Storage ret = storageService.findById(id);
		return ret;
	}
	
	@RequestMapping(value = "/createStorage", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)	
	public ResponseEntity<Storage> create(@RequestBody  Storage storage) {
		setRepositories();

		try {
			storageService.save(storage);
		} catch (Exception e) {
			storage = null;
			return new ResponseEntity<Storage>(storage, HttpStatus.CONFLICT);
		}

		return new ResponseEntity<Storage>(storage, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/updateStorage/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Storage> update(@RequestBody  Storage storage, @PathVariable long id) {		
		setRepositories();

		try {
			storage.setStorageId(id);
			storageService.save(storage);
		} catch (Exception e) {
			return new ResponseEntity<Storage>(storage, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<Storage>(storage, HttpStatus.OK);
	}

}
