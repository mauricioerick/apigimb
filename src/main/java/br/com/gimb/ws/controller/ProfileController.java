package br.com.gimb.ws.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.gimb.ws.model.Profile;
import br.com.gimb.ws.service.ProfileService;

@RestController
@RequestMapping("/admin")
public class ProfileController extends BaseController {

	@Autowired
	private ProfileService profileService;

	@RequestMapping(value = "/profile", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Profile> listAll() {
		setRepositories();
		return profileService.findAll();
	}

	@RequestMapping(value = "/profileByActive/{active}", method = RequestMethod.GET)
	public List<Profile> profileByActive(@PathVariable String active) {
		setRepositories();
		return profileService.findByActive(active);
	}

	@RequestMapping(value = "/profile/{id}", method = RequestMethod.GET)
	public Profile findById(@PathVariable long id) {
		setRepositories();
		return profileService.findById(id);
	}

	@RequestMapping(value = "/profileCreate", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Profile> create(@RequestBody Profile profile) {
		setRepositories();
		try {
			profileService.save(profile);
			
			return new ResponseEntity<Profile>(profile, HttpStatus.CREATED);
			
		} catch (Exception e) {
			profile = null;
			return new ResponseEntity<Profile>(profile, HttpStatus.CONFLICT);
		}
	}

	@RequestMapping(value = "/profileUpdate/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Profile> upddate(@RequestBody Profile profile, @PathVariable long id) {
		setRepositories();

		try {
			profile.setProfileId(id);
			profileService.save(profile);
		} catch (Exception e) {
			return new ResponseEntity<Profile>(profile, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<Profile>(profile, HttpStatus.OK);
	}

	@RequestMapping("/profileDelete/{id}")
	@ResponseBody
	public ResponseEntity<Profile> delete(@PathVariable long id) {
		setRepositories();

		Profile profile = profileService.findById(id);

		try {
			profileService.delete(profile);
		} catch (Exception e) {
			return new ResponseEntity<Profile>(profile, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<Profile>(new Profile(), HttpStatus.OK);
	}
}
