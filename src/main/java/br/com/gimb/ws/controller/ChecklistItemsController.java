package br.com.gimb.ws.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.gimb.ws.model.ChecklistItems;
import br.com.gimb.ws.service.ChecklistItemsService;

@RestController
@RequestMapping("/admin")
public class ChecklistItemsController extends BaseController {
    @Autowired
    private ChecklistItemsService checklistItemsService;
	
	@RequestMapping(value = "/checklistItemsByChecklistId/{id}", method = RequestMethod.GET)
	public Object findByChecklistServicesId(@PathVariable long id) {
		setRepositories();
		
        List<ChecklistItems> retorno = checklistItemsService.findChecklistItemsById(id);

        return retorno;
	}

    @RequestMapping(value = "/createChecklistItems", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody	
	public ResponseEntity<ChecklistItems> create(@RequestBody ChecklistItems chkItems) {
		setRepositories();
		try {
			checklistItemsService.save(chkItems);
			
		} catch (Exception e) {
			return new ResponseEntity<ChecklistItems>(chkItems, HttpStatus.EXPECTATION_FAILED);
		}
		return new ResponseEntity<ChecklistItems>(chkItems, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/updateChecklistItems/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<ChecklistItems> update(@RequestBody ChecklistItems checklistItems, @PathVariable long id) {
		setRepositories();

		try {
			checklistItems.setItemId(id);
			checklistItemsService.save(checklistItems);
			
		} catch (Exception e) {
			return new ResponseEntity<ChecklistItems>(checklistItems, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<ChecklistItems>(checklistItems, HttpStatus.OK);
	}

	@RequestMapping("/removeChecklistItem/{id}")
	@ResponseBody
	public ResponseEntity<ChecklistItems> delete(@PathVariable long id) {
		setRepositories();

		ChecklistItems checklistItems = checklistItemsService.findById(id);
		try {

			if (checklistItems == null)
				return new ResponseEntity<ChecklistItems>(checklistItems, HttpStatus.NOT_FOUND);

			checklistItemsService.delete(checklistItems);

		} catch (Exception e) {
			return new ResponseEntity<ChecklistItems>(checklistItems, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<ChecklistItems>(new ChecklistItems(), HttpStatus.OK);
	}
}
