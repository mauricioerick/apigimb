package br.com.gimb.ws.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.gimb.ws.model.ObjectType;
import br.com.gimb.ws.service.ObjectTypeService;

@RestController
@RequestMapping("/admin")
public class ObjectTypeController extends BaseController {

	@Autowired
	private ObjectTypeService objectTypeService;

	@RequestMapping(value = "/objectType", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object index() {
		setRepositories();
		List<ObjectType> list = objectTypeService.findAll();
		if (list != null)
			list.stream().sorted((e1, e2) -> e1.getDescription().compareTo(e2.getDescription()));
		return list;
	}

	@RequestMapping(value = "/objectTypeByActive/{active}", method = RequestMethod.GET)
	public List<ObjectType> eventByActive(@PathVariable String active) {
		setRepositories();
		return objectTypeService.findByActive(active);
	}

	@RequestMapping(value = "/objectType/{id}", method = RequestMethod.GET)
	public Object findById(@PathVariable long id) {
		setRepositories();
		ObjectType ret = objectTypeService.findById(id);
		return ret;
	}
	
	@RequestMapping(value = "/createObjectType", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)	
	public ResponseEntity<ObjectType> create(@RequestBody  ObjectType objectType) {
		setRepositories();

		try {
			objectTypeService.save(objectType);
		} catch (Exception e) {
			objectType = null;
			return new ResponseEntity<ObjectType>(objectType, HttpStatus.CONFLICT);
		}

		return new ResponseEntity<ObjectType>(objectType, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/updateObjectType/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<ObjectType> update(@RequestBody  ObjectType objectType, @PathVariable long id) {		
		setRepositories();

		try {
			objectType.setObjectTypeId(id);
			objectTypeService.save(objectType);
		} catch (Exception e) {
			return new ResponseEntity<ObjectType>(objectType, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<ObjectType>(objectType, HttpStatus.OK);
	}

}
