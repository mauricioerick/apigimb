package br.com.gimb.ws.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.gimb.ws.model.TextTemplate;
import br.com.gimb.ws.repository.TextTemplateRepository;

@RestController
@RequestMapping("/text-template")
public class TextTemplateController extends BaseController {

  @Autowired
  private TextTemplateRepository textTemplateRepository;

  @RequestMapping(method = RequestMethod.GET)
  public List<TextTemplate> findAll() {
    setRepositories();
    return textTemplateRepository.findAll();
  }
}