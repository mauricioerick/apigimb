package br.com.gimb.ws.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.gimb.ws.model.User;
import br.com.gimb.ws.service.CriptografiaService;
import br.com.gimb.ws.service.UserService;

@RestController
@RequestMapping("/admin")
public class UserController extends BaseController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/user", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object index() {
		setRepositories();
		return userByActive("all");
	}

	@RequestMapping(value = "/userByActive/{active}", method = RequestMethod.GET)
	public List<User> userByActive(@PathVariable String active) {
		setRepositories();
		return userService.findByActive(active);
	}

	@RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
	public ResponseEntity<User> findById(@PathVariable long id) {
		setRepositories();
		User ret = userService.findById(id);

		if (ret.getUserWeb() == null) {
			ret.setUserWeb(true);
		}

		return new ResponseEntity<User>(ret, HttpStatus.OK);
	}

	@RequestMapping(value = "/createUser", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> create(@RequestBody User user) {
		setRepositories();
		try {
			if (userService.findByUser(user.getUser()) != null) {
				user = null;
				return new ResponseEntity<User>(user, HttpStatus.CONFLICT);
			}

			user.setPass(CriptografiaService.criptMD5(user.getPass()));
			userService.save(user);
		} catch (Exception e) {
			user = null;
			return new ResponseEntity<User>(user, HttpStatus.CONFLICT);
		}

		return new ResponseEntity<User>(user, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/updateUser/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<User> update(@RequestBody User user, @PathVariable long id) {
		setRepositories();
		try {
			userService.save(user);
		} catch (Exception e) {
			return new ResponseEntity<User>(user, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	@RequestMapping(value = "/updateUserPassword", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<User> updateUserPassword(@RequestBody User user) {
		setRepositories();
		try {
			user.setPass(CriptografiaService.criptMD5(user.getPass()));
			userService.save(user);
		} catch (Exception e) {
			return new ResponseEntity<User>(user, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	@RequestMapping("/deleteUser/{id}")
	@ResponseBody
	public ResponseEntity<User> delete(@PathVariable long id) {
		setRepositories();

		try {
			User user = userService.findById(id);
			if (user == null)
				return new ResponseEntity<User>(user, HttpStatus.NOT_FOUND);

			userService.delete(user);
		} catch (Exception e) {
			return new ResponseEntity<User>(HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<User>(new User(), HttpStatus.OK);
	}

}
