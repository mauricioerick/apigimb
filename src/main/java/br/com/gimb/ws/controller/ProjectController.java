package br.com.gimb.ws.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.gimb.ws.model.AreaUser;
import br.com.gimb.ws.model.Project;
import br.com.gimb.ws.model.User;
import br.com.gimb.ws.service.AreaUserService;
import br.com.gimb.ws.service.ProjectService;
import br.com.gimb.ws.service.UserService;

@RestController
@RequestMapping("/admin/projects")
public class ProjectController extends BaseController {

	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private AreaUserService areaUserService;
	
	@Autowired
	private UserService userService;

	@PostMapping
	public ResponseEntity<Project> createProject(@RequestBody Project project) {
		setRepositories();
		try {
			Project savedProject = projectService.save(project);

			return ResponseEntity.ok(savedProject);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}

	@GetMapping
	public ResponseEntity<List<Project>> allProjectsActive(
			@RequestParam(name = "active", required = false) Boolean active) {
		setRepositories();

		try {
			List<Project> projects = (active != null) ? projectService.findByActive(active) : projectService.findAll();
			return ResponseEntity.ok(projects);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}

	@GetMapping("/{projectId}")
	public ResponseEntity<Project> getById(@PathVariable Long projectId) {
		setRepositories();

		try {
			Project project = projectService.findById(projectId);
			return project == null ? ResponseEntity.status(HttpStatus.NOT_FOUND).build() : ResponseEntity.ok(project);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}

	@PutMapping("/{projectId}")
	public ResponseEntity<Project> update(@PathVariable Long projectId, @RequestBody Project projectParam) {
		setRepositories();

		try {
			Project editedProject = projectService.update(projectId, projectParam);
			Project project = projectService.save(editedProject);
			return ResponseEntity.ok(project);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}

	}

	@DeleteMapping("/{projectId}")
	public void delete(@PathVariable Long projectId) {
		Project project = projectService.findByProjectId(projectId);
		projectService.delete(project);
	}
	
	@GetMapping("/byArea/{userId}")
	public List<Project> projectsByArea(@PathVariable Long userId){
		try {
			User user = userService.findByUserId(userId);
			List<AreaUser> areaUsers = areaUserService.findByUserId(user.getUserId());
			
			if(areaUsers != null && areaUsers.size() > 0) {
				return projectService.findByAreas(areaUsers);
			}
			else {
				return projectService.findByAreaNull();
			}
			
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
