package br.com.gimb.ws.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.gimb.ws.model.ServiceEvents;
import br.com.gimb.ws.service.ServiceEventService;

@RestController
@RequestMapping("/admin")
public class ServiceEventController extends BaseController {
	
	@Autowired
	private ServiceEventService serviceEventService;
	
	@RequestMapping(value = "/createServiceEvents", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)	
	public ResponseEntity<ServiceEvents> create(@RequestBody ServiceEvents serviceEvents) {
		setRepositories();
		
		try {
			serviceEventService.save(serviceEvents);
		} catch (Exception e) {
			serviceEvents = null;
			return new ResponseEntity<ServiceEvents>(serviceEvents, HttpStatus.CONFLICT);
		}
		
		return new ResponseEntity<ServiceEvents>(serviceEvents, HttpStatus.CREATED);
	}
	
}
