package br.com.gimb.ws.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.gimb.ws.model.Tool;
import br.com.gimb.ws.service.ToolService;

@RestController
@RequestMapping("/admin")
public class ToolController extends BaseController {

	@Autowired
	private ToolService toolService;

	@RequestMapping(value = "/tool", method = RequestMethod.GET)
	@ResponseBody
	public List<Tool> index() {
		setRepositories();

		return toolService.findAll();
	}

	@RequestMapping(value = "/toolByActive/{active}", method = RequestMethod.GET)
	public List<Tool> actionByActive(@PathVariable String active) {
		setRepositories();
		return toolService.findByActive(active);
	}

	@RequestMapping(value = "/tool/{id}", method = RequestMethod.GET)
	public Tool findById(@PathVariable long id) {
		setRepositories();
		return toolService.findById(id);
	}

	@RequestMapping(value = "/toolCreate", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Tool> create(@RequestBody Tool action) {
		setRepositories();

		try {
			toolService.save(action);
		} catch (Exception e) {
			action = null;
			e.printStackTrace();
			return new ResponseEntity<Tool>(action, HttpStatus.CONFLICT);
		}

		return new ResponseEntity<Tool>(action, HttpStatus.CREATED);
	}

	@RequestMapping("/deleteTool/{id}")
	@ResponseBody
	public ResponseEntity<Tool> delete(@PathVariable long id) {
		setRepositories();

		Tool tool = toolService.findById(id);

		try {
			toolService.delete(tool);
		} catch (Exception e) {
			return new ResponseEntity<Tool>(tool, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<Tool>(new Tool(), HttpStatus.OK);
	}

	@RequestMapping(value = "/updateTool/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Tool> update(@RequestBody Tool tool, @PathVariable long id) {
		setRepositories();

		try {
			tool.setToolId(id);
			toolService.save(tool);
		} catch (Exception e) {
			return new ResponseEntity<Tool>(tool, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<Tool>(tool, HttpStatus.OK);
	}
    
}
