package br.com.gimb.ws.controller;

import br.com.gimb.ws.enumerated.ExportExcelDomainEnum;
import br.com.gimb.ws.service.ExportExcelService;
import br.com.gimb.ws.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.Map;

@RestController
public class ExportExcelController extends BaseController{

	@Autowired
	private ExportExcelService exportService;

	private static String REPONSE = "{\"message\": \"%s\"}";

	@RequestMapping(value = "/export-excel/{domain}", method = RequestMethod.GET)
	public ResponseEntity<String> export(@RequestHeader Map<String, String> header, HttpServletRequest request,
			HttpServletResponse response, @PathVariable String domain) {
		setRepositories();
		String language = header.get("language") == null ? "pt" : header.get("language");

		try {
			Map<String, String[]> parameters = request.getParameterMap();
			File excel = exportService.createExcel(ExportExcelDomainEnum.valueOf(domain), parameters);

			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment;filename=" + excel.getName());
			BufferedInputStream inStrem = new BufferedInputStream(new FileInputStream(excel));
			BufferedOutputStream outStream = new BufferedOutputStream(response.getOutputStream());

			byte[] buffer = new byte[1024];
			int bytesRead = 0;
			while ((bytesRead = inStrem.read(buffer)) != -1) {
				outStream.write(buffer, 0, bytesRead);
			}

			outStream.flush();
			inStrem.close();
			excel.delete();
		} catch (Exception e) {
			return new ResponseEntity<String>(Util.msgBundle(language, REPONSE, "userOrPassInvalid"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<String>(Util.msgBundle(language, "tudo tranquilo no mamilo", "userOrPassInvalid"),
				HttpStatus.OK);
	}

}
