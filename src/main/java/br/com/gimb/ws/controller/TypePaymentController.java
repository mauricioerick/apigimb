package br.com.gimb.ws.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.gimb.ws.model.TypePayment;
import br.com.gimb.ws.service.TypePaymentService;


@RestController
@RequestMapping("/admin")
public class TypePaymentController extends BaseController {

	@Autowired
	private TypePaymentService typePaymentService;

	@RequestMapping(value = "/paymentTypeList", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object index() {
		setRepositories();
		List<TypePayment> list = typePaymentService.findAll();
		if (list != null)
			list.stream().sorted((e1, e2) -> e1.getDescription().compareTo(e2.getDescription()));
		return list;
	}

	@RequestMapping(value = "/typePaymentByActive/{active}", method = RequestMethod.GET)
	public List<TypePayment> eventByActive(@PathVariable String active) {
		setRepositories();
		return typePaymentService.findByActive(active);
	}

	@RequestMapping(value = "/typePayment/{id}", method = RequestMethod.GET)
	public Object findById(@PathVariable long id) {
		setRepositories();
		TypePayment tp = typePaymentService.findById(id);
		return tp;
	}
	
	@RequestMapping(value = "/createTypePayment", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)	
	public ResponseEntity<TypePayment> create(@RequestBody TypePayment typePayment) {
		setRepositories();

		try {
			typePaymentService.save(typePayment);
		} catch (Exception e) {
			typePayment = null;
			return new ResponseEntity<TypePayment>(typePayment, HttpStatus.CONFLICT);
		}

		return new ResponseEntity<TypePayment>(typePayment, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/updateTypePayment/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<TypePayment> update(@RequestBody  TypePayment typePayment, @PathVariable long id) {		
		setRepositories();

		try {
			typePayment.setTypePaymentId(id);
			typePaymentService.save(typePayment);
		} catch (Exception e) {
			return new ResponseEntity<TypePayment>(typePayment, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<TypePayment>(typePayment, HttpStatus.OK);
	}

}
