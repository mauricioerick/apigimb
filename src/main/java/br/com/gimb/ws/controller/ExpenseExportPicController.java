package br.com.gimb.ws.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.gimb.ws.enumerated.ReferenceTypeEnum;
import br.com.gimb.ws.model.CustomPicture;
import br.com.gimb.ws.model.Expense;
import br.com.gimb.ws.model.ExpenseItem;
import br.com.gimb.ws.service.CustomPictureService;
import br.com.gimb.ws.service.ExpenseService;
import br.com.gimb.ws.util.FileUtils;
import br.com.gimb.ws.util.aws.AwsS3Utils;

@RestController
public class ExpenseExportPicController extends BaseController {
	
	@Autowired
	private ExpenseService expenseService;
	
	@Autowired
	private CustomPictureService customPictureService;

	@GetMapping("/exportPicFromExpense/{hash}")
	public void exportPicFromExpense(HttpServletRequest request, HttpServletResponse response, @PathVariable String hash) {
		setRepositories();
		Expense expense = expenseService.findByHash(hash);
		if (expense != null && expense.getItems() != null) {
			List<File> files = new ArrayList<>();
			
			try {
				for (ExpenseItem item : expense.getItems()) {
					if (item.getSettlement() != null) {
						item.getSettlement().setCustomPictureList(customPictureService.findByReferenceTypeAndReferenceId(ReferenceTypeEnum.LQ, String.valueOf(item.getSettlement().getSettlementId())));
						
						try {
							List<HashMap<String, Object>> imgConfs = new ObjectMapper().readValue(item.getSettlement().getEvent().getImages(), ArrayList.class);
							
							List<String> imgs = new ArrayList<>();											
							for (HashMap<String, Object> conf : imgConfs) {
								if (conf.containsKey("CHKEXP") && Integer.parseInt(String.valueOf(conf.get("CHKEXP"))) == 1)
									imgs.add(String.valueOf(conf.get("VALUE")));
							}
							
							if (item.getSettlement().getCustomPictureList() != null) {
								AwsS3Utils.generatePreSignedUrl(item.getSettlement().getCustomPictureList(), baseName());
								for (CustomPicture customPic : item.getSettlement().getCustomPictureList()) {
									for (String img : imgs) {
										if (customPic.getMessage().equals(img)) {
											String id = String.valueOf(customPic.getCustompictureId());
											String date = item.getSettlement().getDateYYYYMMDD();
											String event = item.getSettlement().getEvent().getDescription();

											files.add(FileUtils.getFileFromUrl(customPic.getPicPath(), date+"."+id+"-"+event+".jpg"));
											break;
										}
										
									}
								}
							}
						} catch (Exception e) {
							// Do nothing
						}
					}
				}
				
				File zip = FileUtils.createZipWithFiles(files, hash+".zip");

				response.setContentType("application/octet-stream");
				response.setHeader("Content-Disposition", "attachment;filename=" + zip.getName());
			    BufferedInputStream inStrem = new BufferedInputStream(new FileInputStream(zip));
			    BufferedOutputStream outStream = new BufferedOutputStream(response.getOutputStream());

			    byte[] buffer = new byte[1024];
			    int bytesRead = 0;
			    while ((bytesRead = inStrem.read(buffer)) != -1) {
			    	outStream.write(buffer, 0, bytesRead);
			    }
			    outStream.flush();
			    inStrem.close();
			    
			    zip.delete();
			    
			} catch (Exception e) {

			}
		}
	}
	
}
