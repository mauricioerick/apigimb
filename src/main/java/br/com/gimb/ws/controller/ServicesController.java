package br.com.gimb.ws.controller;

import br.com.gimb.ws.DTO.UserServiceDTO;
import br.com.gimb.ws.enumerated.ChecklistStatusEnum;
import br.com.gimb.ws.enumerated.PermissionWebEnum;
import br.com.gimb.ws.enumerated.ReferenceTypeEnum;
import br.com.gimb.ws.model.*;
import br.com.gimb.ws.service.*;
import br.com.gimb.ws.util.PdfService;
import br.com.gimb.ws.util.PermissionsUtil;
import br.com.gimb.ws.util.aws.AwsS3Utils;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.HttpMethod;
import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.lowagie.text.DocumentException;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.http.HttpServletResponse;

@RestController
public class ServicesController extends BaseController {

	@Autowired
	private ServiceService serviceService;

	@Autowired
	private UserService userService;

	@Autowired
	private ClientService clientService;

	@Autowired
	private VehicleService vehicleService;

	@Autowired
	private ProjectService projectService;

	@Autowired
	private PicService picService;

	@Autowired
	private ServiceEventService serviceEventService;

	@Autowired
	private ServiceInterruptionService serviceInterruptionService;

	@Autowired
	private EventService eventService;

	@Autowired
	private ActionService actionService;

	@Autowired
	private CustomFieldService customFieldService;

	@Autowired
	private ObjectService objectService;

	@Autowired
	private ObjectHistoryService objectHistoryService;

	@Autowired
	private ChecklistService checklistService;

	@Autowired
	private AreaService areaService;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private ToolServicePicsService toolServicePicsService;

	@Autowired
	private ChecklistServicesService checklistServicesService;

	private PdfService pdfService;

	public static final String iHTML = "";

	@RequestMapping(value = "/servico", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object index() {
		setRepositories();
		List<Services> retorno = serviceService.findAll();

		for (Services servico : retorno) {
			servico.getClient().setVehicles(null);
			servico.getVehicle().setClient(null);
			servico.setPicsList(null);

			servico.setCustomFieldList(customFieldService.findByReferenceTypeAndReferenceId(ReferenceTypeEnum.SE,
					String.valueOf(servico.getServiceId())));
		}

		return retorno;
	}

	@RequestMapping(value = "/servicoWeb", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object servicos(@RequestBody Map<String, Object> params) {
		setRepositories();
		String startDate = (String) params.get("startDate");
		String endDate = (String) params.get("endDate");
		Map<String, Object> mapUser = (Map<String, Object>) params.get("user");

		List<Services> retorno = null;
		if (mapUser.get("role").equals("C"))
			retorno = serviceService.findByUserClientAndDateBetween(mapUser.get("user").toString(), startDate, endDate);
		else {
			User user = userService.findById(Long.parseLong(mapUser.get("userId").toString()));

			List<AreaUser> areaUsers = null;
			if(PermissionsUtil.getPermissionValue(user.getProfile(), PermissionWebEnum.filtrarServicoArea).equals("1")){
				areaUsers = areaService.findUsersByArea(user);
			}

			if(areaUsers != null && areaUsers.size() > 0){
				retorno = new ArrayList<>();
				for (AreaUser areaUser : areaUsers){
					retorno.addAll(serviceService.findByDateBetween(startDate, endDate, userService.findById(areaUser.getUserId())));
				}
			} else {
				if (PermissionsUtil.getPermissionValue(user.getProfile(), PermissionWebEnum.filtrarServicoUsuario).equals("0"))
					user = null;
				retorno = serviceService.findByDateBetween(startDate, endDate, user);
			}

		}

		List<String> ids = new ArrayList<>();
		for (Services servico : retorno) {
			if (servico.getClient() != null)
				servico.getClient().setVehicles(null);

			if (servico.getVehicle() != null)
				servico.getVehicle().setClient(null);

			ids.add(String.valueOf(servico.getServiceId()));
		}
		
		List<CustomField> fields = customFieldService.findByReferenceTypeAndReferenceIdIn(ReferenceTypeEnum.SE, ids);
		for (CustomField field : fields) {
			Optional<Services> optional = retorno.stream().filter(service -> 
				String.valueOf(service.getServiceId()).equals(field.getReferenceId())
			).findFirst();
			
			if (optional.get() != null) {
				optional.get().getCustomFieldList().add(field);
			}
			
		}

		return retorno;
	}

	@RequestMapping(value = "/dadosServicosPorUsuario/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object dadosServicosPorUsuario(@PathVariable long userId) {
		setRepositories();
		User user = userService.findById(userId);

		if (user != null) {
			List<Services> filtro = new ArrayList<Services>();
			List<Services> retorno = serviceService.findByUser(user);

			for (Services servico : retorno) {
				servico.getClient().setVehicles(null);
				servico.getVehicle().setClient(null);
				servico.setPicsList(null);

				servico.setCustomFieldList(customFieldService.findByReferenceTypeAndReferenceId(ReferenceTypeEnum.SE,
						String.valueOf(servico.getServiceId())));

				if ("C".equals(user.getRole())) {
					if (servico.getClient().getDocument().equals(user.getUser())) {
						filtro.add(servico);
					}
				}
			}

			if ("C".equals(user.getRole()))
				retorno = filtro;

			Map<String, UserServiceDTO> map = new HashMap<>();

			String key = "";
			UserServiceDTO dto = null;
			for (Services service : retorno) {
				key = service.getStartTime().substring(0, 10);

				dto = map.get(key);
				if (dto == null)
					dto = new UserServiceDTO();

				dto.setUsuario(service.getUser().getUser());
				dto.setData(service.getStartTime().substring(0, 10));
				dto.setQtdeServicos(dto.getQtdeServicos() + 1);
				dto.getListaServicos().add(service);

				service.setEventsList(serviceEventService.findByService(service));
				for (ServiceEvents event : service.getEventsList()) {
					event.setServico(null);
				}

				map.put(key, dto);
			}

			SimpleDateFormat format = new SimpleDateFormat("dd/mm/yyyy");

			List<String> keys = new ArrayList<String>(map.keySet());
			keys.sort(new Comparator<String>() {
				@Override
				public int compare(String o1, String o2) {
					try {
						Date d1 = format.parse(o1);
						Date d2 = format.parse(o2);

						return d1.compareTo(d2);
					} catch (ParseException e) {
						e.printStackTrace();
					}
					return 0;
				}
			});

			ArrayList<UserServiceDTO> lst = new ArrayList<UserServiceDTO>();
			for (String k : keys) {
				dto = map.get(k);
				dto.getListaServicos().sort(new Comparator<Services>() {
					@Override
					public int compare(Services o1, Services o2) {
						return o1.getStartTime().compareToIgnoreCase(o2.getStartTime());
					}
				});
				lst.add(dto);
			}
			return lst;
		}

		return new ArrayList<UserServiceDTO>();
	}

	@RequestMapping(value = "/servico/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Object findById(@PathVariable long id) {
		setRepositories();
		Services service = serviceService.findById(id);		
		service.setCustomFieldList(customFieldService.findByReferenceTypeAndReferenceId(ReferenceTypeEnum.SE,
				String.valueOf(service.getServiceId())));

		generatePreSignedUrl(service.getPicsList());

		return service;
	}	

	// @GetMapping(value = "/render-pdf/{serviceId}")
	@RequestMapping(value = "/render-pdf/{serviceId}", method = RequestMethod.GET)
	public void renderPDF(HttpServletResponse response, @PathVariable long serviceId) {
		setRepositories();

		Services service;
		service = serviceService.findById(serviceId);
		
		service.setCustomFieldList(customFieldService.findByReferenceTypeAndReferenceId(ReferenceTypeEnum.SE,
					String.valueOf(service.getServiceId())));

		generatePreSignedUrl(service.getPicsList());
		
		Long idCompany = (long) 1;
		Company company = companyService.findById(idCompany);

        try {
			PdfService pdfService = new PdfService();
			String html = pdfService.parsePdfTemplate(service, company);
			

            Path file = Paths.get(pdfService.generatePdfFromHtml(html).getAbsolutePath());
            if (Files.exists(file)) {
                response.setContentType("application/pdf");
                response.addHeader("Content-Disposition",
                        "inline; filename=" + file.getFileName());
                Files.copy(file, response.getOutputStream());
                response.getOutputStream().flush();
            }
        } catch (DocumentException | IOException ex) {
            ex.printStackTrace();
        }
    }

	@GetMapping(value = "/download-pdf/{serviceId}")
	public void downloadPDFResource(HttpServletResponse response, @PathVariable long serviceId) {
		setRepositories();

		Services service;
		service = serviceService.findById(serviceId);
		
		service.setCustomFieldList(customFieldService.findByReferenceTypeAndReferenceId(ReferenceTypeEnum.SE,
					String.valueOf(service.getServiceId())));

		generatePreSignedUrl(service.getPicsList());
		
		Long idCompany = (long) 1;
		Company company = companyService.findById(idCompany);

        try {
			PdfService pdfService = new PdfService();
			String html = pdfService.parsePdfTemplate(service, company);
			

            Path file = Paths.get(pdfService.generatePdfFromHtml(html).getAbsolutePath());
            if (Files.exists(file)) {
                response.setContentType("application/pdf");
                response.addHeader("Content-Disposition",
                        "attachment; filename=" + file.getFileName());
                Files.copy(file, response.getOutputStream());
                response.getOutputStream().flush();
            }
        } catch (DocumentException | IOException ex) {
            ex.printStackTrace();
        }
    }

	// @GetMapping(value = "/download-pdf/{startDate}/{endDate}/{userId}/{clientId}/{vehicleId}/{actionId}")
	// public void downloadPDFResource(HttpServletResponse response, @PathVariable @DateTimeFormat(iso=DateTimeFormat.ISO.DATE) String startDate, @PathVariable @DateTimeFormat(iso=DateTimeFormat.ISO.DATE) String endDate, @PathVariable long userId, 
	// 		@PathVariable long clientId, @PathVariable long vehicleId, @PathVariable long actionId) {
	// 	setRepositories();

	// 	String initialDate = startDate.replace("_", " ");
	// 	String finalDate = endDate.replace("_", " ");

	// 	Services service;		
	// 	service = serviceService.findByUserAndClientAndVehicleAndStartTimeAndEndDateAndAction(initialDate.replace("-", "/"), finalDate.replace("-", "/"), userId, 
	// 			clientId, vehicleId, actionId);								
		
	// 	service.setCustomFieldList(customFieldService.findByReferenceTypeAndReferenceId(ReferenceTypeEnum.SE,
	// 				String.valueOf(service.getServiceId())));

	// 	generatePreSignedUrl(service.getPicsList());
		
	// 	Long idCompany = (long) 1;
	// 	Company company = companyService.findById(idCompany);

    //     try {
	// 		PdfService pdfService = new PdfService();
	// 		String html = pdfService.parsePdfTemplate(service, company);
			

    //         Path file = Paths.get(pdfService.generatePdfFromHtml(html).getAbsolutePath());
    //         if (Files.exists(file)) {
    //             response.setContentType("application/pdf");
    //             response.addHeader("Content-Disposition",
    //                     "attachment; filename=" + file.getFileName());
    //             Files.copy(file, response.getOutputStream());
    //             response.getOutputStream().flush();
    //         }
    //     } catch (DocumentException | IOException ex) {
    //         ex.printStackTrace();
    //     }
    // }
	

	@RequestMapping(value = "/pendingServiceByUser/{userId}", method = RequestMethod.GET)
	@ResponseBody
	public Object pendingServiceByUser(@PathVariable long userId) {
		setRepositories();
		User user = userService.findById(userId);

		List<Services> servicesList = serviceService.findByUserAndStatus(user, "P");

		for (Services service : servicesList) {
			service = configReturn(service);
			service.setCustomFieldList(customFieldService.findByReferenceTypeAndReferenceId(ReferenceTypeEnum.SE,
					String.valueOf(service.getServiceId())));
		}

		return servicesList;
	}

	@RequestMapping(value = "/updateServiceStatus/{userId}", method = RequestMethod.PUT)
	@ResponseBody
	public void updateServiceStatus(@PathVariable long userId) {
		setRepositories();
		User user = userService.findById(userId);

		List<Services> servicesList = serviceService.findByUserAndStatus(user, "P");

		for (Services service : servicesList) {
			service.setStatus("R");

			serviceService.save(service);
		}
	}

	@RequestMapping(value = "/createService", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Services> create(@RequestBody Services service) {
		setRepositories();

		service = this.loadServiceRelations(service);

		try {
			List<Services> persistedServiceList = new ArrayList<>();
			Services persistedService = serviceService.findById(service.getServiceIdOnLine());			

			if (persistedService == null) {
				persistedServiceList = serviceService.findByUserAndClientAndVehicleAndStartTime(service.getUser(),
						service.getClient(), service.getVehicle(), service.getStartTime());

				if (persistedServiceList != null && persistedServiceList.size() >= 1) {
					persistedService = persistedServiceList.get(0);
				}
			}

			if (persistedService != null) {
				serviceEventService.deleteAllByService(persistedService);

				for (ServiceImage image : persistedService.getPicsList()) {
					if (image.getPicId() > 0 && image.getPicName() != null && image.getPicName().length() > 0) {
						AwsS3Utils.deleteImg(image.getPicName(), baseName());												
					}

					List<ToolServicesPics> listToolServicePics = image.getToolServicesPics();
					image.setToolServicesPics(null);
				}

				picService.deleteAllByService(persistedService);
				serviceInterruptionService.deleteAllByService(persistedService);
				customFieldService.deleteByReferenceTypeAndReferenceId(ReferenceTypeEnum.SE,
						String.valueOf(persistedService.getServiceId()));

				service.setServiceId(persistedService.getServiceId());
				service.setCreatedByUser(persistedService.getCreatedByUser());

				if (service.getPath_signature() != null && !service.getPath_signature().isEmpty()) {
					byte[] data = Base64.decodeBase64(service.getPath_signature());
					String mime = "jpg";
					String nome = "SIGNATURE_" + service.getServiceId();
					service.setPath_signature(nome);
					AwsS3Utils.uploadImage(AwsS3Utils.initializeS3Client(baseName()), data, nome, baseName(), mime);
				}
			}

			List<ServiceEvents> listEvents = service.getEventsList();
			service.setEventsList(null);

			List<ServiceInterruption> listInterruption = service.getInterruptionList();
			service.setInterruptionList(null);

			List<CustomField> customFields = service.getCustomFieldList();
			service.setCustomFieldList(null);

			List<Checklist> checklists = service.getChecklists();
			service.setChecklists(null);

			Action action = actionService.findById(service.getAction().getActionId());
			
			if(action.getChecklist() && checklists != null) {
				boolean conclude = true;
				for(Checklist checklist : checklists) {
					if(!(checklist.getStatus().equals(ChecklistStatusEnum.OK) || checklist.getStatus().equals(ChecklistStatusEnum.FIXED))) {
						conclude = false;
					}
				}
				
				service.setStatusCheckList(conclude);
			}
			
			serviceService.save(service);

			if (listEvents != null) {
				for (ServiceEvents serviceEvents : listEvents) {
					Event event = null;
					if (serviceEvents.getEvent() != null) {
						event = eventService.findById(serviceEvents.getEvent().getEventId());
						serviceEvents.setEvent(event);
						serviceEvents.setServico(service);
						serviceEventService.save(serviceEvents);
					}
				}
			}

			if (listInterruption != null) {
				for (ServiceInterruption interruption : listInterruption) {
					interruption.setService(service);
					serviceInterruptionService.save(interruption);
				}
			}

			if (customFields != null) {
				for (CustomField customField : customFields) {
					customField.setReferenceId(String.valueOf(service.getServiceId()));
					customField.setReferenceType(ReferenceTypeEnum.SE);
					customFieldService.save(customField);
				}
			}

			if (checklists != null) {
				for (Checklist check : checklists) {
					check.setServices(service);
					checklistService.save(check);
				}
			}

			if (service.getPicsList() != null && service.getPicsList().size() > 0) {
				int idx = 1;
				Boolean temImagem = false;
				for (ServiceImage image : service.getPicsList()) {
					if (action.getChecklist() || image.getPic() != null) {
						temImagem = true;
						image = picService.convertBase64ToImage(service, idx, image, AwsS3Utils.initializeS3Client(baseName()),
								baseName());
						image.setChkPrint(true);

						picService.save(image);
						idx++;

						// if (image.getToolServicesPics() != null && image.getToolServicesPics().size() > 0) {
						// 	toolServicePicsService = new ToolServicePicsService();
				
						// 	for (ToolServicesPics toolPics : image.getToolServicesPics()) {
						// 		ToolServicesPics tool = new ToolServicesPics();
				
						// 		tool.setToolId(toolPics.getToolId());
						// 		tool.setServicesPicsId(image.getPicId());
				
						// 		toolServicePicsService.save(tool);
						// 	}				
						// }
					}
				}

				picService.deleteNull();

				if (!temImagem) {
					service.setPicsList(null);
				}
			}

			if(service.getServiceObjectIdentifiers() != null && service.getServiceObjectIdentifiers().size() > 0 ){
				for(HashMap<String,Object> map : service.getServiceObjectIdentifiers()){
					String objectIdentifier = (String) map.get("identifier");
					ObjectInventory object = objectService.findByIdentifier(objectIdentifier);
					ObjectHistory objectHistory =  objectHistoryService.findByObjectAndUserAndDatetime(object,service.getUser(),service.getStartTime(), service.getEndTime());
					objectHistory.setServices(service);
					objectHistoryService.save(objectHistory);
				}
			}

		} catch (Exception e) {
			serviceService.delete(service);
			service = null;
			return new ResponseEntity<Services>(service, HttpStatus.EXPECTATION_FAILED);
		}

		service = configReturn(service);

		return new ResponseEntity<Services>(service, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/deleteService/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseEntity<Services> delete(@PathVariable long id) {
		setRepositories();
		Services service = serviceService.findById(id);
		try {

			if (service == null)
				return new ResponseEntity<Services>(service, HttpStatus.NOT_FOUND);

			// For now, it will maintain the pics at the AWS S3
			// for (ServiceImage image : service.getPicsList()) {
			// 	if (image.getPicId() > 0) {
			// 		AwsS3Utils.deleteImg(image.getPicName(), baseName());
			// 	}
			// }

			serviceService.delete(service);
		} catch (Exception e) {
			return new ResponseEntity<Services>(service, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<Services>(new Services(), HttpStatus.OK);
	}

	@RequestMapping(value = "/updateService/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Services> update(@RequestBody Services service, @PathVariable long id) {
		setRepositories();
		Services servico = new Services();
		try {

			servico = serviceService.findById(id);

			servico.setEndTime(service.getEndTime());
			servico.setFeedback(service.getFeedback());
			servico.setTechnicalAnalysis(service.getTechnicalAnalysis());
			servico.setConclusion(service.getConclusion());			

			if ((servico.getVehicle().getVehicleId() != service.getVehicle().getVehicleId())
					&& service.getVehicle().getVehicleId() > 0)
				servico.getVehicle().setVehicleId(service.getVehicle().getVehicleId());

			serviceService.save(servico);

			servico = configReturn(servico);
		} catch (Exception e) {
			return new ResponseEntity<Services>(servico, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<Services>(servico, HttpStatus.OK);
	}

	@RequestMapping("/deleteServicesImage/{id}")
	@ResponseBody
	public ResponseEntity<ServiceImage> deleteImage(@PathVariable long id, @RequestBody String nome) {
		setRepositories();

		ServiceImage image = picService.findById(id);

		try {
			if (!nome.contains("fix"))
				picService.delete(image);

			else {
				image.setFixPic(null);
				image.setFixPicMime(null);
				image.setFixPicName(null);
				image.setFixPicPath(null);
				image.setStatusFix(false);
				picService.save(image);
			}

			if (image.getPicId() > 0)
				AwsS3Utils.deleteImg(nome, baseName());

		} catch (Exception e) {
			return new ResponseEntity<ServiceImage>(image, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<ServiceImage>(new ServiceImage(), HttpStatus.OK);
	}

	@RequestMapping("/getServiceWithImageId/{id}")
	@ResponseBody
	public long getServiceWithImageId(@PathVariable long id) {
		setRepositories();
		ServiceImage image = picService.findById(id);
		Services service = image.getServico();

		return service.getServiceId();
	}

	@RequestMapping(value = "/addServiceImage", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object addImage(@RequestBody HashMap<String, HashMap<String, Object>> body) {
		setRepositories();
		try {
			Services service = serviceService.findById(Long.valueOf(body.get("service").get("serviceId").toString()));

			ServiceImage serviceImage = new ServiceImage();
			serviceImage.setLatPic(0.0);
			serviceImage.setLgtPic(0.0);
			serviceImage.setPic(body.get("serviceImage").get("pic").toString());
			serviceImage.setMessage(body.get("serviceImage").get("message").toString());
			serviceImage.setNote(body.get("serviceImage").get("note").toString());

			if (body.get("serviceImage").get("critically") != null) {
				serviceImage.setCritically(body.get("serviceImage").get("critically").toString());
			}			

			if (body.get("serviceImage").get("fixPicName") != null) {
				serviceImage.setFixPicMime(body.get("serviceImage").get("fixPicName").toString());
				serviceImage.setFixPicName(body.get("serviceImage").get("fixPicMime").toString());
			}

			serviceImage.setPicMime(body.get("serviceImage").get("picMime").toString());

			serviceImage.setManually(false);

			List<ServiceImage> list = picService.findAll();

			int nextIdx = (int) list.get(list.size() - 1).getPicId() + 1;

			serviceImage = picService.convertBase64ToImage(service, nextIdx, serviceImage,
					AwsS3Utils.initializeS3Client(baseName()), baseName());
			picService.save(serviceImage);

			List<ServiceImage> lista = new ArrayList<ServiceImage>();
			lista.add(serviceImage);

			generatePreSignedUrl(lista);

			return serviceImage;

		} catch (Exception e) {
			return new ResponseEntity<String>("ERRO", HttpStatus.EXPECTATION_FAILED);
		}
	}

	@RequestMapping(value = "/uploadServiceImage/{nome}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<String> updateImage(@PathVariable String nome, @RequestBody String imgBase64) {
		try {
			byte[] data = Base64.decodeBase64(imgBase64);
			AwsS3Utils.uploadImage(AwsS3Utils.initializeS3Client(baseName()), data, nome, baseName(), "jpg");
			return new ResponseEntity<String>("OK", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>("ERRO", HttpStatus.EXPECTATION_FAILED);
		}
	}

	private Services configReturn(Services service) {
		setRepositories();
		User user = null;
		Client client = null;
		Vehicle vehicle = null;

		if (service.getUser() != null) {
			user = userService.findById(service.getUser().getUserId());
			user.setProfile(null);
		}

		if (service.getClient() != null) {
			client = clientService.findById(service.getClient().getClientId());
			client.setVehicles(null);
		}

		if (service.getVehicle() != null) {
			vehicle = vehicleService.findById(service.getVehicle().getVehicleId());
			vehicle.setClient(null);
			vehicle.setEquipment(null);
		}

		service.setServiceIdOnLine(service.getServiceId());
		service.setClient(client);
		service.setUser(user);
		service.setVehicle(vehicle);
		service.setCreatedByUser(null);
		service.setEventsList(null);
		service.setPicsList(null);
		service.setInterruptionList(null);

		return service;
	}

	private void generatePreSignedUrl(List<ServiceImage> listImg) {
		try {
			String bucketName = AwsS3Utils.s3 + baseName();
			AmazonS3 s3Client = AwsS3Utils.initializeS3Client(bucketName);

			// Set the presigned URL to expire after one hour.
			java.util.Date expiration = new java.util.Date();
			long expTimeMillis = expiration.getTime();
			expTimeMillis += 1000 * 60 * 60;
			expiration.setTime(expTimeMillis);

			for (ServiceImage image : listImg) {
				if (image.getPicId() > 0 && ((image.getFixPicName() != null && image.getFixPicName().length() > 0)
						|| (image.getPicName() != null && image.getPicName().length() > 0))) {
					// Generate the presigned URL.
					GeneratePresignedUrlRequest generatePresignedUrlRequest;
					URL url;

					if (image.getPicName() != null && image.getPicName().length() > 0) {
						generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, image.getPicName())
								.withMethod(HttpMethod.GET).withExpiration(expiration);
						url = s3Client.generatePresignedUrl(generatePresignedUrlRequest);
						image.setPicPath(url.toString());
					}

					if (image.getPicThumbName() != null && image.getPicThumbName().length() > 0) {
						generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, image.getPicThumbName())
								.withMethod(HttpMethod.GET).withExpiration(expiration);
						url = s3Client.generatePresignedUrl(generatePresignedUrlRequest);
						image.setPicThumbPath(url.toString());
					}

					if (image.getFixPicName() != null && image.getFixPicName().length() > 0) {
						generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, image.getFixPicName())
								.withMethod(HttpMethod.GET).withExpiration(expiration);
						url = s3Client.generatePresignedUrl(generatePresignedUrlRequest);
						image.setFixPicPath(url.toString());
					}
				}

				if (image.getChecklistItems() != null) {					
					image.getChecklistItems().setChecklistServices(checklistServicesService.findByChecklistId(image.getChecklistItems().getChecklistServicesId()));					
				}
			}
		} catch (AmazonServiceException e) {
			e.printStackTrace();
		} catch (SdkClientException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(value = "/downloadImageService/{nomeImage}", method = RequestMethod.POST)
	@ResponseBody
	public String downloadImage(@PathVariable String nomeImage) throws UnsupportedEncodingException {
		setRepositories();
		Base64 encoder = new Base64();
		byte[] img = AwsS3Utils.downloadImage(nomeImage, baseName());

		String base64 = "data:image/jpeg;base64," + new String(encoder.encode(img), "UTF-8");
		return base64;
	}

	@RequestMapping(value = "/pictureService/{id}", method = RequestMethod.GET)
	public ServiceImage findById(@PathVariable Long id) {
		setRepositories();
		return picService.findById(id);
	}

	@RequestMapping(value = "/updatePictureService/{picId}", method = RequestMethod.PUT)
	public ServiceImage updatePictureService(@RequestBody ServiceImage servicePic, @PathVariable long picId) {
		setRepositories();

		ServiceImage serviceImage = picService.findById(picId);
 
		serviceImage.setNote(servicePic.getNote());
		serviceImage.setChkPrint(servicePic.getChkPrint());

		picService.save(serviceImage);

		return serviceImage;
	}

	@RequestMapping(value = "/findByProject", method = RequestMethod.PUT)
	@ResponseBody
	public List<Services> findByProject(@RequestBody Project project){
		setRepositories();
		
		try {
			return serviceService.findByProject(project);
			
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private Services loadServiceRelations(Services service){
		service.setClient(clientService.findById(service.getClient().getClientId()));
		service.setVehicle(vehicleService.findById(service.getVehicle().getVehicleId()));
		service.setAction(actionService.findById(service.getAction().getActionId()));
		service.setUser(userService.findByUserId(service.getUser().getUserId()));
		if(service.getProject() != null)
			service.setProject(projectService.findByProjectId(service.getProject().getProjectId()));
		return service;
	}

}
