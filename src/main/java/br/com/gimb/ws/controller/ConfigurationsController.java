package br.com.gimb.ws.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.gimb.ws.util.Util;

@RestController
@RequestMapping("/configurations")
public class ConfigurationsController extends BaseController {

  @RequestMapping(method = RequestMethod.GET, value = "/sentryDSN")
  public ResponseEntity<Object> sentryDSN() {
    setRepositories();

    Map<String, String> map = new HashMap<String, String>();
    map.put("DSN", Util.getSentryDSN(true));
    map.put("ENV", Util.getEnvironment().name());

    return new ResponseEntity<Object>(map, HttpStatus.OK);
  }

}