package br.com.gimb.ws.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.gimb.ws.model.ChecklistItems;
import br.com.gimb.ws.model.ChecklistServices;
import br.com.gimb.ws.model.Equipment;
import br.com.gimb.ws.service.ChecklistItemsService;
import br.com.gimb.ws.service.ChecklistServicesService;
import br.com.gimb.ws.service.EquipmentService;

@RestController
@RequestMapping("/admin")
public class EquipmentController extends BaseController {

	@Autowired
	private EquipmentService equipmentService;

	@Autowired
	private ChecklistServicesService checklistServicesService;

	@Autowired
	private ChecklistItemsService checklistItemsService;

	@RequestMapping(value = "/equipment", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Equipment> index() {
		setRepositories();
		return equipmentService.findAll();
	}

	@RequestMapping(value = "/equipmentByActive/{active}", method = RequestMethod.GET)
	public List<Equipment> equipmentByActive(@PathVariable String active) {
		setRepositories();
		return equipmentService.findByActive(active);
	}

	@RequestMapping(value = "/equipment/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object findById(@PathVariable long id) {
		setRepositories();

		Equipment retorno = equipmentService.findById(id);
		retorno.setChecklistsServices(checklistServicesService.findChecklistServicesByEquipmentId(id));		

		return retorno;
	}

	@RequestMapping(value = "/createEquipment", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Equipment> create(@RequestBody Equipment equipment) {
		setRepositories();

		try {
			equipmentService.save(equipment);
		} catch (Exception e) {
			equipment = null;
			return new ResponseEntity<Equipment>(equipment, HttpStatus.CONFLICT);
		}

		return new ResponseEntity<Equipment>(equipment, HttpStatus.CREATED);
	}

	@RequestMapping("/deleteEquipment/{id}")
	@ResponseBody
	public ResponseEntity<Equipment> delete(@PathVariable long id) {
		setRepositories();

		Equipment equipment = equipmentService.findById(id);
		try {
			equipmentService.delete(equipment);
		} catch (Exception e) {
			return new ResponseEntity<Equipment>(equipment, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<Equipment>(new Equipment(), HttpStatus.OK);
	}

	@RequestMapping(value = "/updateEquipment/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Equipment> update(@RequestBody Equipment equipment, @PathVariable long id) {
		setRepositories();

		try {
			equipment.setEquipmentId(id);
			equipmentService.save(equipment);
		} catch (Exception e) {
			return new ResponseEntity<Equipment>(equipment, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<Equipment>(equipment, HttpStatus.OK);
	}

}
