package br.com.gimb.ws.controller;

import br.com.gimb.ws.DTO.BalanceDTO;
import br.com.gimb.ws.model.Balance;
import br.com.gimb.ws.model.TypePayment;
import br.com.gimb.ws.model.User;
import br.com.gimb.ws.service.BalanceService;
import br.com.gimb.ws.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/admin")
public class BalanceController extends BaseController {
	
	@Autowired
	private BalanceService balanceService;

	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/balance", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Object> postBalance(@RequestBody Balance balance) {
		setRepositories();

		try {
			balanceService.save(balance);
		} catch (Exception e) {
			return new ResponseEntity<Object>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<Object>(balance, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/balance", method = RequestMethod.GET)
	@ResponseBody
	public List<BalanceDTO> balance(@RequestHeader Map<String, Object> header) {
		setRepositories();
		
		try {
			String userId = header.get("userid") != null ? header.get("userid").toString() : null;
			List<Long> users = new ArrayList<>();
			if(userId != null && userId.length()>0){
				if(userId.contains(";")){
					users = Arrays.asList(userId.split(";")).stream().map( (u) -> Long.valueOf(u)).collect(Collectors.toList());
				} else {
					users = Arrays.asList(Long.valueOf(userId));
				}
			}
			Long typePaymentId = header.get("typepaymentid") != null ? Long.valueOf(header.get("typepaymentid").toString()) : null;
			String date = header.get("date") != null ? header.get("date").toString() : null;
			String dateGreater = header.get("dategreater") != null ? header.get("dategreater").toString() : null;
			String cardsParam = (String) header.get("listcards");
			List<Long> cardIds = (cardsParam != null && !cardsParam.isEmpty()) ? Arrays.asList((cardsParam).split(";")).stream().map( (x) -> Long.valueOf(x)).collect(Collectors.toList()) : null;
			
			List<Balance> balances = new ArrayList<>();
			if (date != null) {
				users.forEach((u) -> {
					// retorna saldo específico, por usuario, tipo e data
					balances.add(balanceService.balanceByUserAndTypePaymentAndDateEqual(u, typePaymentId, date, null));
				});
			}
			else if (dateGreater != null) {
				users.forEach((u) -> {
					// retorna lista de saldos, por usuario, tipo e data maior ou igual a data informada
					balances.addAll(balanceService.balanceByUserAndTypePaymentAndDateEqualThanGreater(u, typePaymentId, dateGreater));
				});
			}
			else {
				// retorna lista do ultimo saldo, por usuario e tipo
				if(users.size() > 0){
					users.forEach((u) -> {
						balances.addAll(balanceService.balanceByUserAndTypePayment(u, typePaymentId, null));
					});
					if(cardIds != null && cardIds.size()>0){
						cardIds.forEach( (id) -> {
							balances.addAll(balanceService.balanceByUserAndTypePayment(null, typePaymentId, id));
						});
					} else if (userId != null) {
						users.forEach((u) -> {
							userService.findByUserId(u).getCards().forEach((card) -> {
								balances.addAll(balanceService.balanceByUserAndTypePayment(null,typePaymentId, card.getCardId()));
							});
						});
					}
				} else {
					balances.addAll(balanceService.balanceByUserAndTypePayment(null, typePaymentId, null));
					if(cardIds != null && cardIds.size()>0){
						cardIds.forEach( (id) -> {
							balances.addAll(balanceService.balanceByUserAndTypePayment(null, typePaymentId, id));
						});
					}
				}
			}
			
			return balanceService.convertBalanceToDTO(balances);
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	// TODO: Remover esse metodo
	@RequestMapping(value = "/initialBalance", method = RequestMethod.GET)
	@ResponseBody
	public Object reprocessar() {
		try {
			setRepositories();
			
			User user = new User();
			user.setUserId(1);
			TypePayment typePayment = new TypePayment();
			typePayment.setTypePaymentId(2);
			balanceService.reprocessBalanceFrom(user, typePayment, null, "01/01/1900");
		} catch (Exception e) {
			
		}
		return "OK";
	}

}
