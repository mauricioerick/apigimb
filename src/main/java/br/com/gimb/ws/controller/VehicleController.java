package br.com.gimb.ws.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.gimb.ws.model.Client;
import br.com.gimb.ws.model.Vehicle;
import br.com.gimb.ws.service.VehicleService;

@RestController
@RequestMapping("/admin")
public class VehicleController extends BaseController {
	
	@Autowired
	private VehicleService vService;
	
	@RequestMapping(value = "/vehicle", method = RequestMethod.GET)
	@ResponseBody
	public Object index() {
		setRepositories();
		return vService.findAll();
	}
	
	@RequestMapping(value = "/vehicle/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Object findById(@PathVariable long id) {
		setRepositories();
		return vService.findById(id);
	}
	
	@RequestMapping(value = "/createVehicle", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody	
	public ResponseEntity<Vehicle> create(@RequestBody Vehicle vhc) {
		setRepositories();
		try {
			vService.save(vhc);
			
		} catch (Exception e) {
			return new ResponseEntity<Vehicle>(vhc, HttpStatus.EXPECTATION_FAILED);
		}
		return new ResponseEntity<Vehicle>(vhc, HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/removeFromClientVehicle/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Vehicle> removeFromClient(@RequestBody Vehicle vhc, @PathVariable long id)
	{
		setRepositories();
		try 
		{
			vhc.setVehicleId(id);
			vhc.setClient(null);
			vService.save(vhc);
			
			return new ResponseEntity<Vehicle>(vhc, HttpStatus.ACCEPTED);
		} 
		catch (Exception e) {
			return new ResponseEntity<Vehicle>(vhc, HttpStatus.CONFLICT);
		}
		
	}
	
	@RequestMapping(value="/deleteVehicle/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseEntity<Vehicle> delete(@PathVariable long id) {
		setRepositories();
		Vehicle vhc = vService.findById(id);
		
		if(vhc == null)
			return new ResponseEntity<Vehicle>(vhc, HttpStatus.NOT_FOUND);
		
		try {
			
			vService.delete(vhc);
		} catch (Exception e) {
			return tratarRetorno(e, vhc);
			//return new ResponseEntity<Vehicle>(vhc, HttpStatus.EXPECTATION_FAILED);
		}
		
		return new ResponseEntity<Vehicle>(vhc, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/updateVehicle/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Vehicle> update(@RequestBody Vehicle vhc, @PathVariable long id) {
		setRepositories();
		try {
			vhc.setVehicleId(id);
			vService.save(vhc);
		} catch (Exception e) {
			return new ResponseEntity<Vehicle>(vhc, HttpStatus.EXPECTATION_FAILED);
		}
		
		return new ResponseEntity<Vehicle>(vhc, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/updateVehicleToClient/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Vehicle> updateToClient(@RequestBody Client cli, @PathVariable long id) {
		setRepositories();
		Vehicle vhc = null;
		try {
			vhc = vService.findById(id);
			if (vhc != null) {
				vhc.setClient(cli);
				vService.save(vhc);
			}
		} catch (Exception e) {
			return new ResponseEntity<Vehicle>(vhc, HttpStatus.EXPECTATION_FAILED);
		}
		
		return new ResponseEntity<Vehicle>(vhc, HttpStatus.OK);
	}
	
	public ResponseEntity<Vehicle> tratarRetorno(Exception e, Vehicle vehicle) {
		if(e.getCause().toString().lastIndexOf("Constraint") > -1) {
			return new ResponseEntity<Vehicle>(vehicle, HttpStatus.CONFLICT);
		}
		
		return new ResponseEntity<Vehicle>(vehicle, HttpStatus.CREATED);
	}
	
}
