package br.com.gimb.ws.controller;

import br.com.gimb.ws.DTO.LoginAppDTO;
import br.com.gimb.ws.DTO.LoginDTO;
import br.com.gimb.ws.model.Client;
import br.com.gimb.ws.model.Device;
import br.com.gimb.ws.model.User;
import br.com.gimb.ws.service.ClientService;
import br.com.gimb.ws.service.CriptografiaService;
import br.com.gimb.ws.service.DeviceService;
import br.com.gimb.ws.service.UserService;
import br.com.gimb.ws.util.Util;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.rmi.ServerException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
public class AccountController extends BaseController {

	@Autowired
	private UserService userService;

	@Autowired
	private ClientService cliService;

	@Autowired
	private DeviceService deviceService;

	private static String REPONSE_USER_PASS_ERROR = "{\"message\": \"%s\"}";
	private static String REPONSE_USER_NOT_FOUND = "{\"message\": \"%s\"}";
	private static String REPONSE_USER_INACTIVE = "{\"message\": \"%s\"}";

	@RequestMapping(value = "/autenticar", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> autenticar(@RequestBody LoginAppDTO params) throws ServerException {

		setRepositories();

		if (params.user == null || params.pass == null)
			return new ResponseEntity<Object>(REPONSE_USER_PASS_ERROR, HttpStatus.BAD_REQUEST);

		User usuarioAutenticar = userService.findByUser(params.user);

		if (usuarioAutenticar != null) {
			try {
				if (CriptografiaService.criptMD5(params.pass).equals(usuarioAutenticar.getPass())) {
					if (usuarioAutenticar.getUserWeb() == null)
						usuarioAutenticar.setUserWeb(true);

					if (!usuarioAutenticar.getActive())
						return new ResponseEntity<Object>(REPONSE_USER_INACTIVE, HttpStatus.BAD_REQUEST);

					usuarioAutenticar = getToken(usuarioAutenticar);
					usuarioAutenticar.setRole("U");

					if(params.device != null){
						Device deviceJSON;
						try {
							deviceJSON = new ObjectMapper().readValue(params.device, Device.class);
							deviceJSON.setUser(usuarioAutenticar);
							deviceJSON.setActive(true);
							Device oldDevice = deviceService.findByGuid(deviceJSON.getGuid());
							if(oldDevice != null){
								deviceJSON.setDeviceId(oldDevice.getDeviceId());
							}
							deviceService.save(deviceJSON);
						} catch (IOException e) {
							e.printStackTrace();
						}


					}

					return new ResponseEntity<Object>(usuarioAutenticar, HttpStatus.ACCEPTED);
				} else {
					return new ResponseEntity<Object>(Util.msgBundle(params.language, REPONSE_USER_PASS_ERROR, "userOrPassInvalid"),
							HttpStatus.BAD_REQUEST);
				}

			} catch (NoSuchAlgorithmException e) {
				return new ResponseEntity<Object>(Util.msgBundle(params.language, REPONSE_USER_NOT_FOUND, "userNotFound"),
						HttpStatus.BAD_REQUEST);
			}
		} else {
			Client client = cliService.findByDocument(params.user);

			if (client != null) {
				if (!client.getPass().equals(params.pass))
					return new ResponseEntity<Object>(Util.msgBundle(params.language, REPONSE_USER_PASS_ERROR, "userOrPassInvalid"),
							HttpStatus.BAD_REQUEST);
				else if ("inativo".equalsIgnoreCase(client.getSituacao()))
					return new ResponseEntity<Object>(Util.msgBundle(params.language, REPONSE_USER_INACTIVE, "inactiveUser"),
							HttpStatus.BAD_REQUEST);

				usuarioAutenticar = new User();
				usuarioAutenticar.setUser(client.getDocument().toString());
				usuarioAutenticar = getToken(usuarioAutenticar);
				usuarioAutenticar.setRole("C");

				return new ResponseEntity<Object>(usuarioAutenticar, HttpStatus.ACCEPTED);
			} else {
				return new ResponseEntity<Object>(Util.msgBundle(params.language, REPONSE_USER_NOT_FOUND, "userNotFound"),
						HttpStatus.BAD_REQUEST);
			}
		}
	}

	@RequestMapping(value = "/autenticarWeb", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> autenticarWeb(@RequestBody LoginAppDTO params) throws ServerException {

		setRepositories();

		if (params.user == null || params.pass == null)
			return new ResponseEntity<Object>(Util.msgBundle(params.language, REPONSE_USER_INACTIVE, "inactiveUser"),
					HttpStatus.BAD_REQUEST);

		User usuarioAutenticar = userService.findByUser(params.user);

		if (usuarioAutenticar != null) {
			try {
				if (CriptografiaService.criptMD5(params.pass).equals(usuarioAutenticar.getPass())) {
					if (usuarioAutenticar.getUserWeb() == null)
						usuarioAutenticar.setUserWeb(true);

					if (!usuarioAutenticar.getUserWeb())
						return new ResponseEntity<Object>(Util.msgBundle(params.language, REPONSE_USER_PASS_ERROR, "userOrPassInvalid"),
								HttpStatus.BAD_REQUEST);
					else if (!usuarioAutenticar.getActive())
						return new ResponseEntity<Object>(Util.msgBundle(params.language, REPONSE_USER_INACTIVE, "inactiveUser"),
								HttpStatus.BAD_REQUEST);

					usuarioAutenticar = getToken(usuarioAutenticar);
					usuarioAutenticar.setRole("U");

					return new ResponseEntity<Object>(usuarioAutenticar, HttpStatus.ACCEPTED);
				} else {
					return new ResponseEntity<Object>(Util.msgBundle(params.language, REPONSE_USER_PASS_ERROR, "userOrPassInvalid"),
							HttpStatus.BAD_REQUEST);
				}

			} catch (NoSuchAlgorithmException e) {
				return new ResponseEntity<Object>(Util.msgBundle(params.language, REPONSE_USER_PASS_ERROR, "userOrPassInvalid"),
						HttpStatus.BAD_REQUEST);
			}
		} else {
			Client client = cliService.findByDocument(params.user);

			if (client != null) {
				if (!client.getPass().equals(params.pass))
					return new ResponseEntity<Object>(Util.msgBundle(params.language, REPONSE_USER_PASS_ERROR, "userOrPassInvalid"),
							HttpStatus.BAD_REQUEST);

				usuarioAutenticar = new User();
				usuarioAutenticar.setUser(client.getDocument().toString());
				usuarioAutenticar = getToken(usuarioAutenticar);
				usuarioAutenticar.setRole("C");

				return new ResponseEntity<Object>(usuarioAutenticar, HttpStatus.ACCEPTED);
			} else {
				return new ResponseEntity<Object>(Util.msgBundle(params.language, REPONSE_USER_NOT_FOUND, "userNotFound"),
						HttpStatus.BAD_REQUEST);
			}
		}
	}

	@RequestMapping(value = "/token", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getToken(@RequestBody LoginDTO params) throws ServerException {
		String language = "pt";

		setRepositories();

		if (params.user == null || params.pass == null)
			return new ResponseEntity<Object>(REPONSE_USER_PASS_ERROR, HttpStatus.BAD_REQUEST);

		User usuarioAutenticar = userService.findByUser(params.user);

		if (usuarioAutenticar != null) {
			try {
				if (CriptografiaService.criptMD5(params.pass).equals(usuarioAutenticar.getPass())) {
					if (usuarioAutenticar.getUserWeb() == null)
						usuarioAutenticar.setUserWeb(true);

					if (!usuarioAutenticar.getActive())
						return new ResponseEntity<Object>(REPONSE_USER_INACTIVE, HttpStatus.BAD_REQUEST);

					usuarioAutenticar = getToken(usuarioAutenticar);
					Map<String, String> token = new HashMap<String, String>();
					token.put("access_token", usuarioAutenticar.getToken());

					return new ResponseEntity<Object>(token, HttpStatus.ACCEPTED);
				} else {
					return new ResponseEntity<Object>(Util.msgBundle(language, REPONSE_USER_PASS_ERROR, "userOrPassInvalid"),
							HttpStatus.BAD_REQUEST);
				}

			} catch (NoSuchAlgorithmException e) {
				return new ResponseEntity<Object>(Util.msgBundle(language, REPONSE_USER_NOT_FOUND, "userNotFound"),
						HttpStatus.BAD_REQUEST);
			}
		}

		return new ResponseEntity<Object>(Util.msgBundle(language, REPONSE_USER_NOT_FOUND, "userNotFound"),
				HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(value = "/infoApp", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<HashMap<String, Object>> getInfoApp() throws ServerException {
		HashMap<String, Object> retorno = new HashMap<>();
		retorno.put("versaoApp", Util.VERSAO_APP);

		return new ResponseEntity<HashMap<String, Object>>(retorno, HttpStatus.OK);
	}

	private User getToken(User user) {
		String token = Jwts.builder()
				.setSubject(user.getUser())
				.setExpiration(new Date(System.currentTimeMillis() + 24 * 60 * 60 * 1000))
				.signWith(SignatureAlgorithm.HS512, Util.KEY_TOKEN).compact();

		LoginResponse login = new LoginResponse(token);

		user.setToken(login.token);

		return user;
	}

	private class LoginResponse {
		public String token;

		public LoginResponse(String token) {
			this.token = token;
		}
	}
}
