package br.com.gimb.ws.controller;

import br.com.gimb.ws.model.Card;
import br.com.gimb.ws.model.Event;
import br.com.gimb.ws.model.Extract;
import br.com.gimb.ws.model.TypePayment;
import br.com.gimb.ws.model.User;
import br.com.gimb.ws.service.CardService;
import br.com.gimb.ws.service.EventService;
import br.com.gimb.ws.service.ExtractService;
import br.com.gimb.ws.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/admin/extract")
public class ExtractController extends BaseController {

    @Autowired
    private ExtractService extractService;

    @Autowired
    private UserService uService;

    @Autowired
    private EventService eService;
    
    @Autowired
    private CardService cService;

    @RequestMapping(value = "/findAllBetweenReleaseDateCard", method = RequestMethod.PUT)
    @ResponseBody
    public List<Extract> findAllBetweenReleaseDateCard(@RequestBody Map<String, Object> params){
    	setRepositories();
    	try {
    		String startDate = (String) params.get("startDate");
            String endDate = (String) params.get("endDate");
            Integer tp = (Integer) params.get("typePayment");
            Integer c = (Integer) params.get("card");
            String u = (String) params.get("user");
            
            @SuppressWarnings("unchecked")
			List<String> t = (List<String>) params.get("type");
            
            String type[] = new String[t.size()];
            
            Long typePaymentId = tp != null ? Long.valueOf(tp) : null;
            Long cardId = c != null ? Long.valueOf(c) : null;
            Long userId = null;
            if(u != null && !u.isEmpty())
            	userId= Long.valueOf(u);
            
            List<Extract> listaExtract = extractService.findAllBetweenReleaseDateCard(startDate, endDate, cardId, typePaymentId, userId);
            
            for(int i=0; i<t.size(); i++)
            	type[i] = t.get(i);
            
            listaExtract = listaExtract.stream().filter(e -> Arrays.asList(type).contains(e.getType())).collect(Collectors.toList());
            
            return listaExtract;
            
    	}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
    }
    
    @RequestMapping(value = "/findAllBetweenReleaseDate", method = RequestMethod.PUT)
    @ResponseBody
    public List<Extract> findAllBetweenReleaseDate(@RequestBody Map<String, Object> params) {
        setRepositories();
        String startDate = (String) params.get("startDate");
        String endDate = (String) params.get("endDate");
        ObjectMapper mapper = new ObjectMapper();
        User user = mapper.convertValue(params.get("user"), User.class);
        TypePayment typePayment = mapper.convertValue(params.get("typePayment"), TypePayment.class);
        boolean cardExtract = params.get("cardExtract") == null ? false : (boolean) params.get("cardExtract");

        List<String> t = (List<String>) params.get("type");
        List<String> s = (List<String>) params.get("status");

        String type[] = new String[t.size()];
        String status[] = new String[s.size()];

        for (int i = 0; i < t.size(); i++)
            type[i] = t.get(i);

        for (int i = 0; i < s.size(); i++)
            status[i] = s.get(i);

        List<Extract> listaExtract = extractService.findAllBetweenReleaseDate(startDate, endDate, user, typePayment);

        if(cardExtract){
            listaExtract = listaExtract.stream().filter(e -> e.getCard() != null).collect(Collectors.toList());
        }


        // Filtra de acordo com o tipo -> C = Credito e D = Debito
        listaExtract = listaExtract.stream().filter(e -> Arrays.asList(type).contains(e.getType()))
                .collect(Collectors.toList());

        // Filtra de acordo com o status -> A = Aprovado, R = Reprovado e P = pendente
        listaExtract = listaExtract.stream().filter(e -> Arrays.asList(status).contains(e.getStatus()))
                .collect(Collectors.toList());

        return listaExtract;
    }

    @RequestMapping(value = "/find-all-pending", method = RequestMethod.GET)
    public List<Extract> findAllPending(@RequestHeader Map<String, String> header) {
        setRepositories();
        try {
        	Long userId = header.get("userid") != null ? Long.valueOf(header.get("userid")) : null;
            Long eventId = header.get("eventid") != null ? Long.valueOf(header.get("eventid")) : null;
            Long cardId = header.get("cardid") != null ? Long.valueOf(header.get("cardid")) : null;
            		
            User user = null;
            Event event = null;
            Card card = null;
            
            if (userId != null) {
                user = uService.findById(userId);
            }

            if (eventId != null) {
                event = eService.findById(eventId);
            }
            
            if (cardId != null) {
            	card = cService.findByCardId(cardId);
            }

            return extractService.findAllPending(user, event, card);
        } catch (Exception e) {
			e.printStackTrace();
			return null;
		}
    }

    @RequestMapping(value = "/sum-amount", method = RequestMethod.GET)
    public Map<String, Double> sumAmount(@RequestHeader Map<String, String> header) {
        setRepositories();

        Long userId = header.get("userid") != null ? Long.valueOf(header.get("userid")) : null;
        Long typePaymentId = header.get("typePaymentId") != null ? Long.valueOf(header.get("typePaymentId")) : null;
        String status = header.get("status") != null ? header.get("status") : null;

        return extractService.sumAmountByStatus(userId, typePaymentId, status);
    }

    @RequestMapping(value = "/createExtract", method = RequestMethod.POST)
    public ResponseEntity<Extract> create(@RequestBody Extract extract) {
        setRepositories();
        try {
            extractService.save(extract);
            return new ResponseEntity<Extract>(extract, HttpStatus.OK);
        } catch (Exception e) {
            extractService.delete(extract);
            extract = null;

            return new ResponseEntity<Extract>(extract, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/updateExtract", method = RequestMethod.PUT)
    public ResponseEntity<Extract> update(@RequestBody Extract extract) {
        setRepositories();
        try {
            extractService.save(extract);
            return new ResponseEntity<Extract>(extract, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Extract>(extract, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @RequestMapping(value = "/update-extract-list", method = RequestMethod.PUT)
    public void updateExtractList(@RequestBody List<Extract> extractList) {
        setRepositories();
        try {
            for (Extract extract : extractList) {
                extractService.save(extract);
            }

        } catch (Exception e) {

        }
    }

    @RequestMapping("/{id}")
    public ResponseEntity<Extract> findById(@PathVariable Long id) {
        setRepositories();

        Extract extractLocated = extractService.findById(id);

        return (extractLocated != null) ? ResponseEntity.ok(extractLocated)
                : ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    private Date stringConvertToDate(String d) {
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        Date dataFormatada;
        try {
            dataFormatada = formato.parse(d);
            return dataFormatada;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String obtemMes(int mes) {
        String meses[] = { "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro",
                "Outubro", "Novembro", "Dezembro" };

        return meses[mes];
    }

    private String formataData(Extract extract) {
        Date data = stringConvertToDate(extract.getReleaseDate());

        Calendar calendario = Calendar.getInstance();
        calendario.setTime(data);

        String mes = obtemMes(calendario.get(Calendar.MONTH));
        int dia = calendario.get(Calendar.DAY_OF_MONTH);

        return dia + " de " + mes;
    }
}
