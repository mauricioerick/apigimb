package br.com.gimb.ws.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.gimb.ws.DTO.LocationDTO;
import br.com.gimb.ws.model.Trace;
import br.com.gimb.ws.service.TraceService;

@RestController
public class TraceController extends BaseController {
	
	@Autowired
	private TraceService traceService;

	@RequestMapping(value = "/createAllTraces", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody	
	public ResponseEntity<Object> createAllTraces(@RequestBody List<Trace> traces) {
		setRepositories();
		try {
			traceService.save(traces);
		} catch (Exception e) {
			return new ResponseEntity<Object>(null, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<Object>(null, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/findTracesByDateBetween", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody	
	public List<Trace> findTracesByDateBetween(@RequestBody  Map<String, String> params) {
		setRepositories();
		String user = params.get("user");
		String startDate = params.get("startDate");
		String endDate = params.get("endDate");
			
		return traceService.findByDateBetween(startDate, endDate);
	}
	
	@RequestMapping(value = "/findTracesByDate", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody	
	public List<Trace> findTracesByDate(@RequestBody  Map<String, String> params) {
		setRepositories();
		long id = Long.valueOf(params.get("id"));
		String date = params.get("date");
			
		return traceService.findByDate(id, date);
	}
	
	@RequestMapping(value = "/findAllByGroupUser", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody	
	public List<LocationDTO> findAllByGroupUser() {
		setRepositories();
		return traceService.findAllByGroupUser();
	}
	
}
