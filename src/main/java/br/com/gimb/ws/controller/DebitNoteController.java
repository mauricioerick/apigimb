package br.com.gimb.ws.controller;

import br.com.gimb.ws.model.DebitNote;
import br.com.gimb.ws.service.DebitNoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/admin")
public class DebitNoteController extends BaseController{
	@Autowired
	private DebitNoteService debitNoteService;

	@RequestMapping(value = "/debitNoteSeries", method = RequestMethod.GET)
	public ResponseEntity<HashMap<String,Long>> getSeries(){
		setRepositories();
		try{
			return new ResponseEntity<>(debitNoteService.getAllSeriesWithSequence(),HttpStatus.OK);
		} catch (Exception e){
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}
	

	@RequestMapping(value = "/createDebitNote", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<DebitNote> saveDebitNote(@RequestBody DebitNote debitNote){
		setRepositories();
		try {
			return new ResponseEntity<>(debitNoteService.save(debitNote), HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity< >(debitNote, HttpStatus.EXPECTATION_FAILED);
		}
	}
	
}
