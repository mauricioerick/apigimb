package br.com.gimb.ws.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.tomcat.util.codec.binary.Base64;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import br.com.gimb.ws.model.BaseModel;
import br.com.gimb.ws.model.Client;
import br.com.gimb.ws.model.Imports;
import br.com.gimb.ws.model.Vehicle;
import br.com.gimb.ws.service.ClientService;
import br.com.gimb.ws.service.ImportsService;
import br.com.gimb.ws.service.VehicleService;
import br.com.gimb.ws.util.aws.AwsS3Utils;

@RestController
@RequestMapping("/admin")
public class ImportsController extends BaseController {
	@Autowired
	private ImportsService importsService;

	@Autowired
	private VehicleService vServices;

	@Autowired
	private ClientService cServices;
	
	@RequestMapping(value = "/allImports", method = RequestMethod.GET)
	@ResponseBody
	public List<Imports> allImports(){
		setRepositories();
		
		try {
			return importsService.findAll();
			
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	@RequestMapping(value = "/imports/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Imports findByImportsId(@PathVariable long id) {
		setRepositories();
		
		try {
			return importsService.findById(id);
			
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@RequestMapping(value = "/importsByClass/{filePath}", method = RequestMethod.GET)
	@ResponseBody
	public Imports findByFilePath(@PathVariable String filePath) {
		setRepositories();
		
		try {
			return importsService.findByFilePath(filePath);
			
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@RequestMapping(value = "/createImports", method = RequestMethod.PUT)
	public ResponseEntity<Imports> createImports(@RequestBody Imports imports){
		setRepositories();
		
		try {
			if(!imports.getFilePath().isEmpty()) {
				String filePath = imports.getFilePath();
				byte data[] = Base64.decodeBase64(filePath);
				String mime = imports.getMime();
				String nome = imports.getNameClass().getDescription().toUpperCase();
				imports.setFilePath(nome);
				
				AwsS3Utils.uploadXLS(AwsS3Utils.initializeS3Client(baseName()), data, nome, baseName(), mime);
			}
			
			importsService.save(imports);
			return new ResponseEntity<Imports>(imports, HttpStatus.OK);
			
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Imports>(imports, HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	@RequestMapping(value = "/deleteImports", method = RequestMethod.POST)
	public ResponseEntity<Imports> deleteImports(@RequestBody Imports imports){
		setRepositories();
		
		try {
			importsService.delete(imports);
			AwsS3Utils.deleteXLS(imports.getFilePath());
			return new ResponseEntity<Imports>(imports, HttpStatus.OK);
			
		}catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Imports>(imports, HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	@RequestMapping(value = "/allTablesForImports", method = RequestMethod.GET)
	@ResponseBody
	public List<String> allTablesForImports(){
		setRepositories();
		
		try {
			Reflections reflections = new Reflections("br.com.gimb.ws.model");
			Set<Class<? extends BaseModel>> allClasses = 
					reflections.getSubTypesOf(BaseModel.class);
			
			return allClasses.stream().map((c) -> 
				c.getSimpleName().toUpperCase()
			)
			.collect(Collectors.toList());
			
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@RequestMapping(value = "/transformXLSTable", method = RequestMethod.POST)
	public ResponseEntity<Imports> transformXLSTable(@RequestBody Imports imports){
		try {
			importsService.transformXLSTable(imports);
			
			return new ResponseEntity<Imports>(HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<Imports>(HttpStatus.EXPECTATION_FAILED);
		}
	}

	@RequestMapping(value = "/importXLSTableVehicle/{clientId}", method = RequestMethod.POST)
	public ResponseEntity<Imports> importXLSTableVehicle(@RequestBody Imports imports, @PathVariable Long clientId){		
		try {
			importsService.importXLSTableVehicle(imports, clientId);

			Client client = cServices.findById(clientId);

			List<Vehicle> vhc = new ArrayList<>();
			vhc = vServices.findVehiclesEquipmentNull(client);
			
			if (vhc.size() > 0)
				return new ResponseEntity(vhc, HttpStatus.OK);
			
			return new ResponseEntity<Imports>(HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<Imports>(HttpStatus.EXPECTATION_FAILED);
		}
	}

	@RequestMapping(value = "/importXLSTableChecklistService/{equipmentId}", method = RequestMethod.POST)
	public ResponseEntity<Imports> importXLSTableChecklistServices(@RequestBody Imports imports, @PathVariable Long equipmentId){		
		try {
			importsService.importXLSTableChecklistServices(imports, equipmentId);

			Client client = cServices.findById(equipmentId);

			List<Vehicle> vhc = new ArrayList<>();
			vhc = vServices.findVehiclesEquipmentNull(client);
			
			if (vhc.size() > 0)
				return new ResponseEntity(vhc, HttpStatus.OK);
			
			return new ResponseEntity<Imports>(HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<Imports>(HttpStatus.EXPECTATION_FAILED);
		}
	}
}
