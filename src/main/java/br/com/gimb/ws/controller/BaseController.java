package br.com.gimb.ws.controller;

import java.lang.reflect.Field;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;

import br.com.gimb.annotation.RepositoryClass;
import br.com.gimb.config.TenantContext;
import br.com.gimb.ws.service.BaseService;

public class BaseController {
	@Autowired
	protected HttpServletRequest http;

	protected String baseName() {
		String baseName = "gimb";

		String[] arr = http.getServerName().split("\\.");
		if (http != null && arr.length > 0) {
			if ("192".equals(arr[0]))
				baseName = "localhost";
			else
				baseName = arr[0];
		}

//		return "zenega";
		 return baseName;
	}

	protected void setRepositories() {
		TenantContext.setCurrentTenant(baseName());
		this.setRepositories(this);
	}

	protected void setRepositories(Object obj) {
		try {
			Field[] mainFields = obj.getClass().getDeclaredFields();
			if (mainFields != null) {
				for (Field field : mainFields) {
					if (BaseService.class.isAssignableFrom(field.getType())) {
						field.setAccessible(true);
						BaseService service = (BaseService) field.get(this);

						Field[] serviceFields = service.getClass().getDeclaredFields();
						if (serviceFields != null) {
							for (Field f : serviceFields) {
								if (BaseService.class.isAssignableFrom(f.getType()))
									setRepositories(f);
								else if (f.isAnnotationPresent(RepositoryClass.class)) {
									f.setAccessible(true);
									RepositoryClass ann = f.getAnnotation(RepositoryClass.class);
									if (ann != null) {
										service.setRepository((CrudRepository) f.get(service));
									}
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
}
