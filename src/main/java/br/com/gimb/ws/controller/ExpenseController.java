package br.com.gimb.ws.controller;

import br.com.gimb.ws.model.Expense;
import br.com.gimb.ws.service.ExpenseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/admin")
public class ExpenseController extends BaseController {
	
	@Autowired
	private ExpenseService expenseService;
	
	@RequestMapping(value = "/expense", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Expense> findAll() {
		setRepositories();
		return expenseService.findAll();
	}
	
	@RequestMapping(value = "/expenseWeb", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Expense> expenseWeb(@RequestBody Map<String, String> params) {
		setRepositories();
		return expenseService.findByStartDateBetween(params.get("startDate"), params.get("endDate"));
	}
	
	@RequestMapping(value = "/expense/{id}", method = RequestMethod.GET)
	public Expense findById(@PathVariable long id) {
		setRepositories();
		return expenseService.findById(id);
	}
	
	@RequestMapping(value = "/createExpense", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)	
	public ResponseEntity<Expense> create(@RequestBody Expense expense) {
		setRepositories();
		
		try {
			expenseService.save(expense);
		} catch (Exception e) {
			e.printStackTrace();
			expense = null;
			return new ResponseEntity<Expense>(expense, HttpStatus.CONFLICT);
		}
		
		return new ResponseEntity<Expense>(expense, HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/updateExpense/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Expense> update(@RequestBody Expense expense, @PathVariable long id) {
		setRepositories();
		
		try {
			expense.setExpenseId(id);
			expenseService.save(expense);
		} catch (Exception e) {
			return new ResponseEntity<Expense>(expense, HttpStatus.EXPECTATION_FAILED);
		}
		
		return new ResponseEntity<Expense>(expense, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/deleteExpense/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Expense> delete(@PathVariable long id) {
		setRepositories();
		Expense expense = expenseService.findById(id);
		
		if(expense == null)
			return new ResponseEntity<Expense>(expense, HttpStatus.NOT_FOUND);
		
		try {
			expenseService.delete(expense);
		} catch (Exception e) {
			return new ResponseEntity<Expense>(expense, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<Expense>(new Expense(), HttpStatus.OK);
	}

}
