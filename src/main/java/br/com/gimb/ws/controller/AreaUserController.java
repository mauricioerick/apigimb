package br.com.gimb.ws.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.gimb.ws.model.AreaUser;
import br.com.gimb.ws.service.AreaUserService;

@RestController
@RequestMapping("/admin")
public class AreaUserController extends BaseController{

	@Autowired
	private AreaUserService areaUserService;
	
	@RequestMapping(value = "/areaUser", method = RequestMethod.GET)
	@ResponseBody
	public List<AreaUser> areaUser(){
		setRepositories();
		try {
			return areaUserService.findAll();
		}catch (Exception e) {
			return null;
		}
	}
	
	@RequestMapping(value = "/areaUserByUser/{id}", method = RequestMethod.GET)
	@ResponseBody
	public List<AreaUser> areaUserByUser(@PathVariable long id){
		setRepositories();
		try {
			return areaUserService.findByUserId(id);
		}catch (Exception e) {
			return null;
		}
	}
	
	@RequestMapping(value = "/areaUserByArea/{id}", method = RequestMethod.GET)
	@ResponseBody
	public List<AreaUser> areaUserByArea(@PathVariable long id){
		setRepositories();
		try {
			return areaUserService.findByAreaId(id);
		}catch (Exception e) {
			return null;
		}
	}
	
	@RequestMapping(value = "/removeAreaUser", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<AreaUser> removeAreaUser(@RequestBody AreaUser areaUser){
		setRepositories();
		try {
			areaUserService.delete(areaUser);
			return new ResponseEntity<AreaUser>(areaUser, HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<AreaUser>(areaUser, HttpStatus.EXPECTATION_FAILED);
		}
	}
}
