package br.com.gimb.ws.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.gimb.ws.DTO.ChecklistServicesDTO;
import br.com.gimb.ws.model.ChecklistItems;
import br.com.gimb.ws.model.ChecklistServices;
import br.com.gimb.ws.model.Equipment;
import br.com.gimb.ws.service.ChecklistItemsService;
import br.com.gimb.ws.service.ChecklistServicesService;
import br.com.gimb.ws.service.EquipmentService;

@RestController
@RequestMapping("/admin")
public class ChecklistServicesController extends BaseController {

	@Autowired
	private ChecklistServicesService checklistServicesService;

	@Autowired
	private ChecklistItemsService checklistItemsService;

	@Autowired
	private EquipmentService equipmentService;

	@Autowired
	private EquipmentController equipmentController;

	@RequestMapping(value = "/checklistService", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<ChecklistServices> index() {
		setRepositories();
		return checklistServicesService.findAll();
	}

	@RequestMapping(value = "/checklistService/{id}", method = RequestMethod.GET)
	public ChecklistServices findById(@PathVariable long id) {
		setRepositories();
		return checklistServicesService.findById(id);
	}

	@RequestMapping(value = "/checklistServiceByActive/{active}", method = RequestMethod.GET)
	public List<ChecklistServices> checklistServicesByActive(@PathVariable String active) {
		setRepositories();

		List<ChecklistServices> checklistServices = checklistServicesService.findByActive(active);
		for (ChecklistServices checklist : checklistServices) {
			checklist.setChecklistItems(checklistItemsService.findChecklistItemsById(checklist.getChecklistId()));
			checklist.setEquipment(equipmentService.findById(checklist.getEquipmentEquipmentId()));
		}

		return checklistServices;
	}

	@RequestMapping(value = "/createChecklistServices", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<ChecklistServices> create(@RequestBody ChecklistServices chkServices) {
		setRepositories();
		try {
			checklistServicesService.save(chkServices);

		} catch (Exception e) {
			return new ResponseEntity<ChecklistServices>(chkServices, HttpStatus.EXPECTATION_FAILED);
		}
		return new ResponseEntity<ChecklistServices>(chkServices, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/updateChecklistServices/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<ChecklistServices> update(@RequestBody ChecklistServices checklistServices,
			@PathVariable long id) {
		setRepositories();

		try {
			checklistServices.setChecklistId(id);
			checklistServicesService.save(checklistServices);
		} catch (Exception e) {
			return new ResponseEntity<ChecklistServices>(checklistServices, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<ChecklistServices>(checklistServices, HttpStatus.OK);
	}

	@RequestMapping(value = "/removeFromEquipmentChecklist/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<ChecklistServices> removeFromClient(@RequestBody ChecklistServices checklistServices,
			@PathVariable long id) {
		setRepositories();
		try {
			checklistServices.setChecklistId(id);

			List<ChecklistItems> checklistItems = checklistItemsService.findChecklistItemsById(id);
			for (ChecklistItems chkItem : checklistItems) {
				checklistItemsService.delete(chkItem);
			}

			checklistServicesService.delete(checklistServices);

			return new ResponseEntity<ChecklistServices>(checklistServices, HttpStatus.ACCEPTED);
		} catch (Exception e) {
			return new ResponseEntity<ChecklistServices>(checklistServices, HttpStatus.CONFLICT);
		}

	}

	public void deleteAllByEquipment(Long equipmentId) {
		Equipment equipment = (Equipment) equipmentController.findById(equipmentId);

		if (equipment.getChecklistsServices() != null && equipment.getChecklistsServices().size() > 0) {
			for (ChecklistServices checklistServices : equipment.getChecklistsServices()) {
				List<ChecklistItems> checklistItems = checklistItemsService
						.findChecklistItemsById(checklistServices.getChecklistId());

				for (ChecklistItems chkItems : checklistItems) {
					checklistItemsService.delete(chkItems);
				}

				checklistServicesService.delete(checklistServices);
			}
		}
	}

	@RequestMapping(value = "/checklistServiceCritically/{id}", method = RequestMethod.GET)
	public List<ChecklistServicesDTO> getChecklistServiceCritically(@PathVariable Long id) {
		setRepositories();
		return checklistServicesService.getChecklistServicesAndCriticallyByServiceId(id);		
	}

	@RequestMapping(value = "/checklistServiceCriticallyTotal/{id}", method = RequestMethod.GET)
	public List<ChecklistServicesDTO> getChecklistServiceCriticallyTotal(@PathVariable Long id) {
		setRepositories();
		return checklistServicesService.getChecklistServicesAndCriticallyByServiceIdTotal(id);		
	}

}
