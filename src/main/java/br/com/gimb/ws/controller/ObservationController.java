package br.com.gimb.ws.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.gimb.ws.model.Observation;
import br.com.gimb.ws.service.ObservationService;


@RestController
@RequestMapping("/admin")
public class ObservationController extends BaseController {

	@Autowired
	private ObservationService observationService;

	@RequestMapping(value = "/observation", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object index() {
		setRepositories();
		List<Observation> list = observationService.findAll();
		if (list != null)
			list.stream().sorted((e1, e2) -> e1.getDescription().compareTo(e2.getDescription()));
		return list;
	}

	@RequestMapping(value = "/observationByActive/{active}", method = RequestMethod.GET)
	public List<Observation> eventByActive(@PathVariable String active) {
		setRepositories();
		return observationService.findByActive(active);
	}

	@RequestMapping(value = "/observation/{id}", method = RequestMethod.GET)
	public Object findById(@PathVariable long id) {
		setRepositories();
		Observation ret = observationService.findById(id);
		ret.getAppearsOnList();
		return ret;
	}
	
	@RequestMapping(value = "/createObservation", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)	
	public ResponseEntity<Observation> create(@RequestBody  Map<String, Object> params) {
		String language = (String) params.get("language");
		ObjectMapper mapper = new ObjectMapper();
		Observation observation = mapper.convertValue(params.get("observation"), Observation.class);
		
		setRepositories();

		try {
			observationService.prepareAppearsOn(observation, language);
			observationService.save(observation);
		} catch (Exception e) {
			observation = null;
			return new ResponseEntity<Observation>(observation, HttpStatus.CONFLICT);
		}

		return new ResponseEntity<Observation>(observation, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/updateObservation/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Observation> update(@RequestBody  Map<String, Object> params, @PathVariable long id) {
		String language = (String) params.get("language");
		ObjectMapper mapper = new ObjectMapper();
		Observation observation = mapper.convertValue(params.get("observation"), Observation.class);
		
		setRepositories();

		try {
			observation.setObservationId(id);
			observationService.prepareAppearsOn(observation, language);
			observationService.save(observation);
		} catch (Exception e) {
			return new ResponseEntity<Observation>(observation, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<Observation>(observation, HttpStatus.OK);
	}

}
