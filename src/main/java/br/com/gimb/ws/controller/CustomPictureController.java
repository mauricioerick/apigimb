package br.com.gimb.ws.controller;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.gimb.ws.enumerated.ReferenceTypeEnum;
import br.com.gimb.ws.model.CustomPicture;
import br.com.gimb.ws.service.CustomPictureService;
import br.com.gimb.ws.util.aws.AwsS3Utils;

@RestController
@RequestMapping("/admin")
public class CustomPictureController extends BaseController {
	
	@Autowired
	private CustomPictureService customPictureService;	
	
	@RequestMapping(value = "/customPicture/{id}", method = RequestMethod.GET)
	public CustomPicture findById(@PathVariable Long id) {
		setRepositories();
		return customPictureService.findById(id);
	}
	
	@RequestMapping(value="/deleteImage/{id}", method=RequestMethod.DELETE)
	@ResponseBody
	public ResponseEntity<CustomPicture> deleteImage(@PathVariable long id){
		setRepositories();
		
		CustomPicture image = customPictureService.findById(id);
		
		try {
			if(image.getCustompictureId() > 0) {
				AwsS3Utils.deleteImg(image.getPicName(), baseName());
			}
			customPictureService.delete(image);
		}catch(Exception e) {
			return new ResponseEntity<CustomPicture>(image, HttpStatus.EXPECTATION_FAILED);
		}
		return new ResponseEntity<CustomPicture>(new CustomPicture(), HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/uploadCustomImage/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<String> updateImage(@PathVariable long id, @RequestBody String imgBase64){
		setRepositories();
		try {
			CustomPicture image = customPictureService.findById(id);
			byte[] data = Base64.decodeBase64(imgBase64);
			AwsS3Utils.uploadImage(AwsS3Utils.initializeS3Client(baseName()), data, image.getPicName(), baseName(), "jpg");
			return new ResponseEntity<String>("OK", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>("ERRO", HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	@RequestMapping(value = "/addCustomImage/{type}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object addImage(@PathVariable String type, @RequestBody HashMap<String, HashMap<String, Object>> body){
		setRepositories();
		try {
			int nextIdx = 0;
			CustomPicture customPicture = new CustomPicture();
			
			if(type.equalsIgnoreCase("LQ")) {
				java.util.List<CustomPicture> lista = customPictureService.findAll();
				
				nextIdx = (int) lista.get(lista.size() - 1).getCustompictureId() + 1;
				
				customPicture.setReferenceType(ReferenceTypeEnum.LQ);
				customPicture.setReferenceId(body.get("referenceType").get("id").toString());
			}
			
			customPicture.setLatitude(0.0);
			customPicture.setLongitude(0.0);
			customPicture.setPic(body.get("serviceImage").get("pic").toString());
			customPicture.setMessage(body.get("serviceImage").get("message").toString());
			
			customPicture = customPictureService.convertBase64ToImage(customPicture, nextIdx, AwsS3Utils.initializeS3Client(baseName()), baseName());
			customPictureService.save(customPicture);
			
			AwsS3Utils.generatePreSignedUrl(customPicture, baseName(), null);
			
			return customPicture;
			
		}catch (Exception e) {
			return new ResponseEntity<String>("ERRO", HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	@RequestMapping(value = "/downloadImage/{nomeImage}", method = RequestMethod.GET)
	@ResponseBody
	public String downloadImage(@PathVariable String nomeImage) throws UnsupportedEncodingException {
		setRepositories();
		Base64 encoder = new Base64();
		byte[] img = AwsS3Utils.downloadImage(nomeImage, baseName());
		
		String base64 = "data:image/jpeg;base64,"+ new String(encoder.encode(img), "UTF-8");   
		return base64;
	}
	
	@RequestMapping(value = "/imageLink/{imageName}", method = RequestMethod.GET)
	public String returnImageLink(@PathVariable String imageName) {
		return AwsS3Utils.generatePreSignedUrl(imageName, baseName(), AwsS3Utils.initializeS3Client(baseName()));
	}
	
	@RequestMapping(value = "/imageLinkList", method = RequestMethod.POST)
	public Map<String, String> returnImageLinkList(@RequestBody List<String> nameImagesList) {
		Map<String, String> listUrlImages = new HashMap<String, String>();
		for (String imageName : nameImagesList) {
			listUrlImages.put(imageName, AwsS3Utils.generatePreSignedUrl(imageName, baseName(), AwsS3Utils.initializeS3Client(baseName())));
		}
		return listUrlImages;
	}

}
