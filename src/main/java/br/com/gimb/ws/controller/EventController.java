package br.com.gimb.ws.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.gimb.ws.model.Event;
import br.com.gimb.ws.service.EventService;

@RestController
@RequestMapping("/admin")
public class EventController extends BaseController {

	@Autowired
	private EventService eventService;

	@RequestMapping(value = "/event", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object index() {
		setRepositories();
		List<Event> list = eventService.findAll();
		if (list != null)
			list.stream().sorted((e1, e2) -> e1.getDescription().compareTo(e2.getDescription()));
		return list;
	}

	@RequestMapping(value = "/eventByActive/{active}", method = RequestMethod.GET)
	public List<Event> eventByActive(@PathVariable String active) {
		setRepositories();
		return eventService.findByActive(active);
	}

	@RequestMapping(value = "/event/{id}", method = RequestMethod.GET)
	public Object findById(@PathVariable long id) {
		setRepositories();
		Event ret = eventService.findById(id);
		ret.getAppearsOnList();
		return ret;
	}
	
	@RequestMapping(value = "/createEvent", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)	
	public ResponseEntity<Event> create(@RequestBody  Map<String, Object> params) {
		String language = (String) params.get("language");
		ObjectMapper mapper = new ObjectMapper();
		Event event = mapper.convertValue(params.get("event"), Event.class);
		
		setRepositories();

		try {
			eventService.prepareAppearsOn(event, language);
			eventService.save(event);
		} catch (Exception e) {
			event = null;
			return new ResponseEntity<Event>(event, HttpStatus.CONFLICT);
		}

		return new ResponseEntity<Event>(event, HttpStatus.CREATED);
	}

	@RequestMapping("/deleteEvent/{id}")
	@ResponseBody
	public ResponseEntity<Event> delete(@PathVariable long id) {
		setRepositories();
		Event event = eventService.findById(id);

		if (event == null)
			return new ResponseEntity<Event>(event, HttpStatus.NOT_FOUND);

		try {
			eventService.delete(event);
		} catch (Exception e) {
			return new ResponseEntity<Event>(event, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<Event>(new Event(), HttpStatus.OK);
	}

	@RequestMapping(value = "/updateEvent/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Event> update(@RequestBody  Map<String, Object> params, @PathVariable long id) {
		String language = (String) params.get("language");
		ObjectMapper mapper = new ObjectMapper();
		Event event = mapper.convertValue(params.get("event"), Event.class);
		
		setRepositories();

		try {
			event.setEventId(id);
			eventService.prepareAppearsOn(event, language);
			eventService.save(event);
		} catch (Exception e) {
			return new ResponseEntity<Event>(event, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<Event>(event, HttpStatus.OK);
	}

}
