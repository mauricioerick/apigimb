package br.com.gimb.ws.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.gimb.ws.model.CostCenter;
import br.com.gimb.ws.service.CostCenterService;

@RestController
@RequestMapping("/admin")
public class CostCenterController extends BaseController{
	@Autowired
	private CostCenterService costCenterService;
	
	@RequestMapping(value = "/allCostCenter", method = RequestMethod.GET)
	@ResponseBody
	public List<CostCenter> allCostCenter(){
		setRepositories();
		return costCenterService.findAll();
	}
	
	@RequestMapping(value = "/costCenter/{costCenterId}", method = RequestMethod.GET)
	@ResponseBody
	public CostCenter costCenter(@PathVariable long costCenterId){
		setRepositories();
		return costCenterService.findByCostCenterId(costCenterId);
	}
	
	@RequestMapping(value = "/costCenterDescription/{description}", method = RequestMethod.GET)
	@ResponseBody
	public List<CostCenter> costCenterDescription(@PathVariable String description){
		setRepositories();
		return costCenterService.findByDescription(description);
	}
	
	@RequestMapping(value = "/costCenterAccountCode/{accountCode}", method = RequestMethod.GET)
	@ResponseBody
	public List<CostCenter> costCenterAccountCode(@PathVariable String accountCode){
		setRepositories();
		return costCenterService.findByDescription(accountCode);
	}
	
	@RequestMapping(value = "/costCenterActive/{active}", method = RequestMethod.GET)
	@ResponseBody
	public List<CostCenter> costCenterActive(@PathVariable String active){
		setRepositories();
		return costCenterService.findByActive(active);
	}
	
	@RequestMapping(value = "/updateCostCenter/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<CostCenter> updateCostCenter(@PathVariable long id, @RequestBody CostCenter costCenter){
		setRepositories();
		
		try {
			costCenter.setCostCenterId(id);
			costCenterService.save(costCenter);
		}catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<CostCenter>(costCenter, HttpStatus.EXPECTATION_FAILED);
		}
		
		return new ResponseEntity<CostCenter>(costCenter, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/saveCostCenter", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<CostCenter> saveCostCenter(@RequestBody CostCenter costCenter){
		setRepositories();
		
		try {
			/*List<CostCenter> listaAll = allCostCenter();
			long id = listaAll.get(listaAll.size() - 1).getCostCenterId() + 1;
			costCenter.setCostCenterId(id);*/
			
			costCenterService.save(costCenter);
			return new ResponseEntity<CostCenter>(costCenter, HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<CostCenter>(costCenter, HttpStatus.EXPECTATION_FAILED);
		}
	}
}
