package br.com.gimb.ws.controller;

import br.com.gimb.ws.model.TableParameter;
import br.com.gimb.ws.model.TableParameterBaseModel;
import br.com.gimb.ws.service.TableParameterService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/admin")
public class TableParameterController extends BaseController {

	@Autowired
	private TableParameterService tableParameterService;

	@RequestMapping(value = "/listTables", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<String> getTables(){
		Reflections reflections = new Reflections("br.com.gimb.ws.model");

		Set<Class<? extends TableParameterBaseModel>> allClasses =
				reflections.getSubTypesOf(TableParameterBaseModel.class);

		return allClasses.stream().map((c) -> c.getSimpleName().toLowerCase()).collect(Collectors.toList());
	}

	@RequestMapping(value = "/tableParameter", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object index() {
		setRepositories();
		return tableParameterService.findAll();
	}

	@RequestMapping(value = "/tableParameterByTableReference/{tableReference}", method = RequestMethod.GET)
	public TableParameter TableParameterByActive(@PathVariable String tableReference) {
		setRepositories();
		return tableParameterService.findByTableReferenceActive(tableReference);
	}

	@RequestMapping(value = "/tableParameterByActive/{active}", method = RequestMethod.GET)
	public List<TableParameter> TableParameterByActive(@PathVariable Boolean active) {
		setRepositories();
		return tableParameterService.findByActive(active);
	}

	@RequestMapping(value = "/tableParameter/{id}", method = RequestMethod.GET)
	public Object findById(@PathVariable long id) {
		setRepositories();
		return tableParameterService.findByTableParameterId(id);
	}
	
	@RequestMapping(value = "/createTableParameter", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)	
	public ResponseEntity<TableParameter> create(@RequestBody  Map<String, Object> params) {
		setRepositories();
		
		TableParameter tableParameter = null;
		try {
			List<Map<String, Object>> jsonStructure = (List<Map<String, Object>>) params.get("jsonStructure");
			params.put("jsonStructure", null);
			
			ObjectMapper mapper = new ObjectMapper();
			tableParameter = mapper.convertValue(params, TableParameter.class);
			tableParameter.setJsonVersion(1);
			tableParameter.setJsonStructure(mapper.writeValueAsString(jsonStructure));
			tableParameter.setActive(true);
			
			List<TableParameter> parameters = tableParameterService.findByTableReference(tableParameter.getTableReference());
			if (parameters != null && parameters.size() > 0) {
				TableParameter tableParameterPersisted = parameters.get(parameters.size() - 1);
				
				if (!tableParameterPersisted.getJsonStructure().equals(tableParameter.getJsonStructure())) {
					tableParameterPersisted.setActive(false);
					tableParameterService.save(tableParameterPersisted);
					
					tableParameter.setJsonVersion(parameters.size()+1);
				}
				else {
					tableParameter.setTableParameterId(tableParameterPersisted.getTableParameterId());
					tableParameter.setGuid(tableParameterPersisted.getGuid());
					tableParameter.setJsonVersion(tableParameterPersisted.getJsonVersion());
				}
			}
			
			tableParameterService.save(tableParameter);
		} catch (Exception e) {
			tableParameter = null;	
			return new ResponseEntity<TableParameter>(tableParameter, HttpStatus.CONFLICT);
		}

		return new ResponseEntity<TableParameter>(tableParameter, HttpStatus.CREATED);
	}

}
