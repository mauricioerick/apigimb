package br.com.gimb.ws.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.gimb.ws.model.Document;
import br.com.gimb.ws.service.DocumentService;
import br.com.gimb.ws.service.PhotosDownloadService;

@RestController
public class PhotosDownloadController extends BaseController{

	@Autowired
	DocumentService documentService;
	
	@Autowired
	DocumentController documentController;
	
	@Autowired
	PhotosDownloadService photosDownloadService;
	
	@RequestMapping(value = "/downloadPhotos/{idDocument}", method = RequestMethod.GET)
	public ResponseEntity<String> downloadPhotos(@PathVariable long idDocument, HttpServletResponse response, HttpServletRequest request){
		try {
			Document document = documentController.document(idDocument);
			
			List<Document> documents = new ArrayList<>();
			documents.add(document);
			
			byte[] zip = photosDownloadService.getZipFiles(documents);
			
			ServletOutputStream sos = response.getOutputStream();
			response.setContentType("application/zip");
			response.setHeader("Content-Disposition", "attachment; filename=photos.zip");
			
			sos.write(zip);
			sos.flush();
			
		}catch (Exception e) {
			return new ResponseEntity<String>(e.toString(), HttpStatus.OK);
		}
		
		return new ResponseEntity<String>("deu bom", HttpStatus.OK);
	}
	
	@RequestMapping(value = "/downloadSeveralPhotos", method = RequestMethod.GET)
	public ResponseEntity<String> downloadSeveralPhotos(HttpServletResponse response, HttpServletRequest request) throws Exception {
		try {
			Map<String, String[]> params = request.getParameterMap();
			String[] paramsIds = params.get("id");
			
			List<Document> documents = new ArrayList<>();
			
			long ids[] = new long[paramsIds.length];
			
			for(int i=0; i<paramsIds.length; i++)
				ids[i] = Long.parseLong(paramsIds[i]);
			
			for(int i = 0; i<paramsIds.length; i++) {
				Document document = documentController.document(ids[i]);
				documents.add(document);
			}
			
			byte[] zip = photosDownloadService.getZipFiles(documents);
			
			ServletOutputStream sos = response.getOutputStream();
			response.setContentType("application/zip");
			response.setHeader("Content-Disposition", "attachment; filename=photos.zip");
			
			sos.write(zip);
			sos.flush();
			
		}catch (Exception e) {
			return new ResponseEntity<String>(e.toString(), HttpStatus.OK);
		}
		
		return new ResponseEntity<String>("deu bom", HttpStatus.OK);
	}
}
