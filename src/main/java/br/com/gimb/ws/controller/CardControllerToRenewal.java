package br.com.gimb.ws.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.gimb.ws.enumerated.PaymentIdentificationEnum;
import br.com.gimb.ws.model.Card;
import br.com.gimb.ws.model.Extract;
import br.com.gimb.ws.model.TypePayment;
import br.com.gimb.ws.service.BalanceService;
import br.com.gimb.ws.service.CardService;
import br.com.gimb.ws.service.ExtractService;
import br.com.gimb.ws.service.TypePaymentService;

@RestController
public class CardControllerToRenewal extends BaseController {
	
	@Autowired
	CardService cardService;
	
	@Autowired
	ExtractService extractService;
	
	@Autowired
	TypePaymentService typePaymentService;
	
	@Autowired
	BalanceService balanceService;
	
	@RequestMapping(value="/allCardsToRenewal", method = RequestMethod.POST)
	public ResponseEntity<Object> allCardsToRenewal(){
		setRepositories();
		try {
			List<Card> cards = cardService.findAll();
			checkCards(cards);
			
			return new ResponseEntity<Object>(null, HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<Object>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	private void checkCards(List<Card> cards) {
		Calendar calendar = Calendar.getInstance();
		int now = calendar.get(Calendar.DAY_OF_MONTH);
		
		List<Card> cardModfied = new ArrayList<Card>();
		
		for(Card card : cards) {
			if (card.getAutomaticRenovation() != null) {
				
				if ( now == 1 ) {
					card.setRenovated(false);
				}
				
				if(card.getAutomaticRenovation() && card.getRenewalDay() <= now && (card.getRenovated() == null || !card.getRenovated())) {
					card.setRenovated(true);
					cardModfied.add(card);
				}
			}
		}
		
		if ( now == 1 && cards.size() > 0) {
			saveCards(cards);
		}
		else if (cardModfied.size() > 0) {
			saveCards(cardModfied);
			createExtract(cardModfied);
		}
	}
	
	private void saveCards(List<Card> cardModfied) {
		for ( Card card : cardModfied ) {
			cardService.save(card);
		}
	}
	
	private void createExtract(List<Card> cards) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String date = sdf.format(new Date(System.currentTimeMillis()));
		
		SimpleDateFormat sdfm = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String createDate = sdfm.format(new Date(System.currentTimeMillis()));
		
		TypePayment tp = null;
		
		List<TypePayment> typePayments = typePaymentService.findAll();
		for(TypePayment typePayment : typePayments)
			if(typePayment.getIdentificationType() == PaymentIdentificationEnum.LS || typePayment.getIdentificationType() == PaymentIdentificationEnum.PC) {
				tp = typePayment;
				break;
			}
		
		for(Card card : cards) {
			Extract extract = Extract.builder();
			extract.setReleaseDate(date);
			extract.setType("C");
			extract.setAmount(card.getLimit());
			extract.setAmountApproved(card.getCredict());
			extract.setStatus("A");
			extract.setCreateDate(createDate);
			extract.setUser(card.getUser());
			extract.setTypePayment(tp);
			extract.setCard(card);
			
			extractService.save(extract);
		}
	}

}
