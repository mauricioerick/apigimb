package br.com.gimb.ws.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.gimb.ws.model.NatureExpense;
import br.com.gimb.ws.service.NatureExpenseService;

@RestController
@RequestMapping("/admin")
public class NatureExpenseController extends BaseController{
	@Autowired
	NatureExpenseService natureExpenseService;
	
	@RequestMapping(value = "/allNatureExpenses")
	@ResponseBody
	public List<NatureExpense> allNatureExpense() {
		setRepositories();
		return natureExpenseService.findAll();
	}
	
	@RequestMapping(value = "/natureExpense/{natureExpenseId}", method = RequestMethod.GET)
	@ResponseBody
	public NatureExpense natureExpense(@PathVariable long natureExpenseId) {
		setRepositories();
		return natureExpenseService.findById(natureExpenseId);
	}
	
	@RequestMapping(value = "/natureExpenseDescription/{description}", method = RequestMethod.GET)
	@ResponseBody
	public List<NatureExpense> natureExpenseDescription(@PathVariable String description) {
		setRepositories();
		return natureExpenseService.findByDescription(description);
	}
	
	@RequestMapping(value = "/natureExpenseActive/{active}", method = RequestMethod.GET)
	@ResponseBody
	public List<NatureExpense> natureExpenseActive(@PathVariable String active) {
		setRepositories();
		return natureExpenseService.findByActive(active);
	}
	
	@RequestMapping(value = "/updateNatureExpense", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<NatureExpense> updateNatureExpense(@RequestBody NatureExpense natureExpense){
		setRepositories();
		try {
			natureExpenseService.save(natureExpense);
			return new ResponseEntity<NatureExpense>(natureExpense, HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<NatureExpense>(natureExpense, HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	@RequestMapping(value = "/saveNatureExpense", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<NatureExpense> saveNatureExpense(@RequestBody NatureExpense natureExpense){
		setRepositories();
		try {
			natureExpenseService.save(natureExpense);
			return new ResponseEntity<NatureExpense>(natureExpense, HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<NatureExpense>(natureExpense, HttpStatus.EXPECTATION_FAILED);
		}
	}
}
