package br.com.gimb.ws.controller;

import br.com.gimb.ws.DTO.ExportSettlementDTO;
import br.com.gimb.ws.enumerated.PaymentTypeEnum;
import br.com.gimb.ws.enumerated.PermissionWebEnum;
import br.com.gimb.ws.enumerated.ReferenceTypeEnum;
import br.com.gimb.ws.model.*;
import br.com.gimb.ws.service.*;
import br.com.gimb.ws.util.PermissionsUtil;
import br.com.gimb.ws.util.aws.AwsS3Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
public class SettlementController extends BaseController {

	@Autowired
	private CostCenterService costCenterService;

	@Autowired
	private UserService userService;

	@Autowired
	private SettlementService settlementService;

	@Autowired
	private CustomFieldService customFieldService;

	@Autowired
	private CustomPictureService customPictureService;

	@Autowired
	private ClientService clientService;

	@Autowired
	private EventService eventService;

	@Autowired
	private ExtractService extractService;

	@Autowired
	private TypePaymentService typePaymentService;

	@Autowired
	private SettlementCostCenterService settlementCostCenterService;

	@Autowired
	private AreaService areaService;

	@RequestMapping(value = "/admin/settlement", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object index() {
		setRepositories();
		return settlementService.findAll();
	}

	@RequestMapping(value = "/settlementWeb", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object settlementsWeb(@RequestBody Map<String, Object> params) {
		setRepositories();

		try {
			User user = null;
			List<AreaUser> areaUsers = null;
			Boolean isResumo = (Boolean) params.get("isResumo") == null ? false : (Boolean) params.get("isResumo");

			if (params.get("user") != null) {
				Map<String, Object> mapUser = (Map<String, Object>) params.get("user");
				user = userService.findById(Long.parseLong(mapUser.get("userId").toString()));
				if (PermissionsUtil.getPermissionValue(user.getProfile(), PermissionWebEnum.filtrarLiquidacaoArea)
						.equals("1")){
					areaUsers = areaService.findUsersByArea(user);
				}
				if (!isResumo && PermissionsUtil.getPermissionValue(user.getProfile(), PermissionWebEnum.filtrarLiquidacaoUsuario)
						.equals("0"))
					user = null;
			}
			List<Settlement> list = new ArrayList<>();
			if(!isResumo && areaUsers != null && areaUsers.size() > 0){
				for(AreaUser areaUser : areaUsers){
					list.addAll(settlementService.findByDateBetween(userService.findById(areaUser.getUserId()), params.get("startDate").toString(),
							params.get("endDate").toString()));
				}
			} else {
				list.addAll(settlementService.findByDateBetween(user, params.get("startDate").toString(),
						params.get("endDate").toString()));
			}

			List<String> ids = new ArrayList<>();
			for (Settlement settlement : list) {
				ids.add(String.valueOf(settlement.getSettlementId()));
				
				if ( settlement.getExtract() == null ) {
					Extract extract = extractService.findBySettlement(settlement);
					if ( extract != null) {
						extract.setSettlement(null);
						settlement.setExtract( extract );
					}
				}
			}
			
			List<CustomField> fields = customFieldService.findByReferenceTypeAndReferenceIdIn(ReferenceTypeEnum.LQ, ids);
			for (CustomField field : fields) {
				Optional<Settlement> optional = list.stream().filter(settlement -> 
					String.valueOf(settlement.getSettlementId()).equals(field.getReferenceId())
				).findFirst();
				
				if (optional.get() != null) {
					optional.get().getCustomFieldList().add(field);
				}
				
			}

			return list;
			
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@RequestMapping(value = "/settlementByDateBetween", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object settlementByDateBetween(@RequestBody Map<String, String> params) {
		setRepositories();
		String startDate = params.get("startDate");
		String endDate = params.get("endDate");

		return settlementService.findByDateBetweenAndClientAndUserAndEvent(startDate, endDate, null, null, null, null);
	}

	@RequestMapping(value = "/admin/exportSettlement", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object exportSettlement(@RequestBody ExportSettlementDTO params) {
		setRepositories();


		Client client = null;
		User user = null;
		List<Event> listEvent = null;
		
		if (params.clientDocument != null)
			client = clientService.findByDocument(params.clientDocument);
		if (params.userName != null)
			user = userService.findByUser(params.userName);
		if (params.eventDescription != null)
			listEvent = eventService.findByDescription(params.eventDescription);

		Long clientId = client != null ? client.getClientId() : null;
		Long userId = user != null ? user.getUserId() : null;
		Long eventId = listEvent != null && listEvent.size() > 0 ? ((Event) listEvent.get(0)).getEventId() : null;

		List<Map<String, Object>> resultList = new ArrayList<>();

		List<Settlement> settlementList = settlementService.findByDateBetweenAndClientAndUserAndEvent(params.startDate, params.endDate,
				clientId, userId, eventId, params.natureExpenseId);

		for (Settlement set : settlementList) {
			if (params.approveStatus != null)
				if (set.getExtract() == null || !set.getExtract().getStatus().equals(params.approveStatus))
					continue;

			set.setCustomFieldList(customFieldService.findByReferenceTypeAndReferenceId(ReferenceTypeEnum.LQ,
					String.valueOf(set.getSettlementId())));
			set.setCustomPictureList(customPictureService.findByReferenceTypeAndReferenceId(ReferenceTypeEnum.LQ,
					String.valueOf(set.getSettlementId())));

			resultList.add(settlementService.convertSettlementToMap(set, baseName()));
		}

		return resultList;
	}

	@RequestMapping(value = "/settlement/{id}", method = RequestMethod.GET)
	public Object findById(@PathVariable long id) {
		setRepositories();
		Settlement settlement = settlementService.findById(id);
		settlement.setCustomFieldList(customFieldService.findByReferenceTypeAndReferenceId(ReferenceTypeEnum.LQ,
				String.valueOf(settlement.getSettlementId())));
		settlement.setCustomPictureList(customPictureService.findByReferenceTypeAndReferenceId(ReferenceTypeEnum.LQ,
				String.valueOf(settlement.getSettlementId())));

		AwsS3Utils.generatePreSignedUrl(settlement.getCustomPictureList(), baseName());

		settlement.setExtract(extractService.findBySettlement(settlement));

		if (settlement.getExtract() != null)
			settlement.getExtract().setSettlement(null);

		return settlement;
	}

	@RequestMapping(value = "/createSettlement", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> create(@RequestBody Settlement settlement) {
		setRepositories();

		try {

			if (settlement.getSettlementIdOnLine() != null) {
				Settlement persistedSettlement = settlementService.findById(settlement.getSettlementIdOnLine());
				if (persistedSettlement != null) {
					persistedSettlement.setStatus(settlement.getStatus());
					settlementService.save( persistedSettlement );
				}
			} else {
				List<CustomField> customFields = settlement.getCustomFieldList();
				List<CustomPicture> customPictures = settlement.getCustomPictureList();
				List<SettlementCostCenter> costCenters = settlement.getCostCenterList();

				settlement.setCustomFieldList(null);
				settlement.setCustomPictureList(null);
				settlement.setCostCenterList(null);

				if (settlement.getTypePayment() == null) {
					if (settlement.getPaymentType() != null && settlement.getPaymentType() == PaymentTypeEnum.CC) {
						settlement.setTypePayment(typePaymentService.findById(1L));
					} else if (settlement.getPaymentType() != null && settlement.getPaymentType() == PaymentTypeEnum.ES) {
						settlement.setTypePayment(typePaymentService.findById(2L));
					}
				} else {
					settlement.setTypePayment(typePaymentService.findById(settlement.getTypePayment().getTypePaymentId()));
				}

				settlementService.save(settlement);

				if (customFields != null && customFields.size() > 0) {
					
					List<String> customFieldsToCompare = new ArrayList<>();
					
					for (CustomField customField : customFields) {
						
						Boolean add = true;
						
						for ( String c : customFieldsToCompare ) {
							if ( customField.getCustomField().equals( c ) ) {
								add = false;
								break;
							}
						}
						
						if ( add ) {
							customFieldsToCompare.add( customField.getCustomField() );
							customField.setReferenceId(String.valueOf(settlement.getSettlementId()));
							customFieldService.save(customField);
						}
					}
				}

				if (customPictures != null && customPictures.size() > 0) {
					int idx = 1;
					for (CustomPicture customPic : customPictures) {
						if (customPic.getPic() != null) {
							customPic.setReferenceId(String.valueOf(settlement.getSettlementId()));
							customPic = customPictureService.convertBase64ToImage(customPic, idx,
									AwsS3Utils.initializeS3Client(baseName()), baseName());
							customPictureService.save(customPic);
							idx++;
						}
					}
				}

				if (costCenters != null) {
					for (SettlementCostCenter costCenter : costCenters) {
						costCenter.setSettlement(settlement);
						settlementCostCenterService.save(costCenter);
					}
				}

				if (settlement.getTypePayment() != null && settlement.getTypePayment().getConsumeBalance()) {
					extractService.createExtractBySettlement(settlement);
				}
			}

			return new ResponseEntity<Object>(settlement, HttpStatus.CREATED);
		} catch (Exception e) {
			delete(settlement.getSettlementId());
			settlement = null;
		}

		return new ResponseEntity<Object>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@RequestMapping("/deleteSettlement/{id}")
	@ResponseBody
	public ResponseEntity<Object> delete(@PathVariable long id) {
		setRepositories();

		try {
			Settlement settlement = settlementService.findById(id);
			if (settlement == null)
				return new ResponseEntity<Object>(settlement, HttpStatus.NOT_FOUND);

			if (settlement.getCustomPictureList() != null && settlement.getCustomPictureList().size() > 0) {
				for (CustomPicture customPic : settlement.getCustomPictureList()) {
					if (customPic.getCustompictureId() > 0) {
						AwsS3Utils.deleteImg(customPic.getPicName(), baseName());
					}
				}
			}

			customFieldService.deleteByReferenceTypeAndReferenceId(ReferenceTypeEnum.LQ,
					String.valueOf(settlement.getSettlementId()));
			settlementService.delete(settlement);
		} catch (Exception e) {
			return new ResponseEntity<Object>(null, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<Object>(null, HttpStatus.OK);
	}

	@RequestMapping(value = "/updateSettlement/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Settlement> update(@RequestBody Settlement settlement, @PathVariable long id) {
		setRepositories();
		try {
			settlement.setSettlementId(id);
			settlementService.save(settlement);
		} catch (Exception e) {
			return new ResponseEntity<Settlement>(settlement, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<Settlement>(settlement, HttpStatus.OK);
	}

}
