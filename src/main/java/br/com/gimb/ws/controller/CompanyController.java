package br.com.gimb.ws.controller;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.gimb.ws.model.Company;
import br.com.gimb.ws.service.CompanyService;
import br.com.gimb.ws.util.aws.AwsS3Utils;

@RestController
public class CompanyController extends BaseController {

	@Autowired
	private CompanyService companyService;
	
	@RequestMapping(value = "/company/{id}", method = RequestMethod.GET)
	public Company findById(@PathVariable long id) {
		setRepositories();
		return companyService.findById(id);
	}

	@RequestMapping(value = "/companyImageLogo", method = RequestMethod.GET, produces = "text/plain")
	public String returnImageLogoLink() {
		setRepositories();
		Company company = companyService.findAll().get(0);
		return AwsS3Utils.generatePreSignedUrl(company.getLogoName(), baseName(), AwsS3Utils.initializeS3Client(baseName()));
	}
	
	@RequestMapping(value = "/saveCompany", method = RequestMethod.PUT)
	public ResponseEntity<Company> saveCompany(@RequestBody Company company) {
		setRepositories();
		try {
			if (company.getLogoBase64() != null && !company.getLogoBase64().isEmpty()) {
				byte[] data = Base64.decodeBase64(company.getLogoBase64());
				String mime = "jpg";
				String nome = company.getLogoName();
				AwsS3Utils.uploadImage(AwsS3Utils.initializeS3Client(baseName()), data, nome, baseName(), mime);
			}
			
			companyService.save(company);
			
		}catch (Exception e) {
			return new ResponseEntity<Company>(company, HttpStatus.CONFLICT);
		}

		return new ResponseEntity<Company>(company, HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/deleteLogo", method = RequestMethod.PUT)
	public ResponseEntity<Company> deleteLogo(@RequestBody Company company) {
		setRepositories();
		String nameLogo = company.getLogoName();
		
		try {
			if(nameLogo != null && !nameLogo.isEmpty()) {
				AwsS3Utils.deleteImg(nameLogo, baseName());
				company.setLogoName("");
				companyService.save(company);
			}
		}catch (Exception e) {
			return new ResponseEntity<Company>(company, HttpStatus.CONFLICT);
		}
		
		return new ResponseEntity<Company>(company, HttpStatus.CREATED);
	}
}
