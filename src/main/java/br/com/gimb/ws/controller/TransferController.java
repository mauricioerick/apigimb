package br.com.gimb.ws.controller;

import br.com.gimb.ws.enumerated.PermissionWebEnum;
import br.com.gimb.ws.model.*;
import br.com.gimb.ws.service.*;
import br.com.gimb.ws.util.PermissionsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
public class TransferController extends BaseController {
	
	@Autowired
	private TransferService transferService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ClientService clientService;
	
	@Autowired
	private TransferEventService transferEventService;
	
	@Autowired
	private EventService eventService;

	@Autowired
	private AreaService areaService;


	@RequestMapping(value = "/transfer/{id_tranfer}", method = RequestMethod.GET)
	public Transfer findById(@PathVariable long id_tranfer) {
		setRepositories();
		return transferService.findById(id_tranfer);
	}
	
	@RequestMapping(value = "/transfersByDateBetween", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Transfer> transfersByDateBetween(@RequestBody Map<String, Object> params) {
		setRepositories();
		String startDate = "";
		String endDate = "";
		Map<String, Object> mapUser = null;
		
		if (params != null) {
			startDate = (String) params.get("startDate");
			endDate = (String) params.get("endDate");
			mapUser = (Map<String, Object>) params.get("user");
		}
		
		User user = null;
		List<AreaUser> areaUsers = null;
		if(mapUser != null) {
			user = userService.findById(Long.parseLong(mapUser.get("userId").toString()));

			if(PermissionsUtil.getPermissionValue(user.getProfile(), PermissionWebEnum.filtrarApontamentoArea).equals("1")){
				areaUsers = areaService.findUsersByArea(user);
			}
			if(PermissionsUtil.getPermissionValue(user.getProfile(), PermissionWebEnum.filtrarApontamentoUsuario).equals("0"))
				user = null;
		}
		List<Transfer> retorno = new ArrayList<>();
		if(areaUsers != null  && areaUsers.size() > 0){
			for(AreaUser areaUser: areaUsers){
				retorno.addAll(transferService.findByDateBetween(startDate, endDate, userService.findById(areaUser.getUserId())));
			}
		}
		else retorno.addAll(transferService.findByDateBetween(startDate, endDate, user));
		return retorno;
	}
	
	@RequestMapping(value = "/createTransfer", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody	
	public ResponseEntity<Transfer> create(@RequestBody Transfer transfer) {
		setRepositories();
		try {
			List<TransferEvents> listEvents = transfer.getEventsList();
			transfer.setEventsList(null);
			
			transferService.save(transfer);
			
			if (listEvents != null) {
				for (TransferEvents transferEvents : listEvents) {
					Event event = null;
					if (transferEvents.getEvent() != null) {
						event = eventService.findById(transferEvents.getEvent().getEventId());
						transferEvents.setEvent(event);
						transferEvents.setTransfer(transfer);
						transferEventService.save(transferEvents);
					}
				}
			}
			
		} catch (Exception e) {
			transferService.delete(transfer);
			transfer = null;
			return new ResponseEntity<Transfer>(transfer, HttpStatus.EXPECTATION_FAILED);
		}
		
		transfer = ConfigReturn(transfer);
		
		return new ResponseEntity<Transfer>(transfer, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/update-transfer/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Transfer> update(@RequestBody Transfer transfer, @PathVariable long id) {
		setRepositories();

		try {
			transfer.setTransferId(id);
			List<TransferEvents> listEvents = transfer.getEventsList();
			transfer.setEventsList(null);
			
			transferService.save(transfer);
			
			if (listEvents != null) {
				for (TransferEvents transferEvents : listEvents) {
					Event event = null;
					if (transferEvents.getEvent() != null) {
						event = eventService.findById(transferEvents.getEvent().getEventId());
						transferEvents.setEvent(event);
						transferEvents.setTransfer(transfer);
						transferEventService.save(transferEvents);
					}
				}
			}
		} catch (Exception e) {
			return new ResponseEntity<Transfer>(transfer, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<Transfer>(transfer, HttpStatus.OK);
	}

	@RequestMapping(value = "/ponto-aberto-usuario/{id_user}", method = RequestMethod.GET)
	public Boolean existePontoEmAberto(@PathVariable long id_user) {
		setRepositories();
		User user = userService.findById(id_user);
		return transferService.findByEndTimeIsNull(user).size() > 0;
	}
	
	private Transfer ConfigReturn(Transfer transfer) {
		setRepositories();
		if (transfer != null) {
			User user = userService.findById(transfer.getUser().getUserId());
			Client client = null;
			
			if (transfer.getClient() != null) {
				client = clientService.findById(transfer.getClient().getClientId());
				client.setVehicles(null);
			}
			
			transfer.setClient(client);
			transfer.setUser(user);
			transfer.setEventsList(null);
		}
		
		return transfer;
	}
}
