package br.com.gimb.ws.controller;

import br.com.gimb.ws.DTO.AgendaDTO;
import br.com.gimb.ws.DTO.AgendaItemDTO;
import br.com.gimb.ws.model.Agenda;
import br.com.gimb.ws.model.AgendaItem;
import br.com.gimb.ws.service.AgendaItemService;
import br.com.gimb.ws.service.AgendaService;
import javassist.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/admin")
public class AgendaItemController {

    private AgendaService agendaService;
    private AgendaItemService agendaItemService;

    public AgendaItemController(AgendaService agendaService, AgendaItemService agendaItemService) {
        this.agendaService = agendaService;
        this.agendaItemService = agendaItemService;
    }

    @GetMapping("/agendaItem/{id}")
    public ResponseEntity<?> find(@PathVariable("id") Long id) throws NotFoundException {
        AgendaItem agendaItem = agendaItemService.findById(id);
        if(agendaItem != null) {
            return new ResponseEntity<>(new AgendaItemDTO(agendaItem), HttpStatus.OK);
        }

        throw new NotFoundException("Item da Agenda não existe");
    }


    @PutMapping("/agendaItem/{id}")
    public ResponseEntity<?> put(@RequestBody AgendaItemDTO agendaItemRequest, @PathVariable("id") Long id) throws NotFoundException {
        AgendaItem agendaItem = agendaItemService.findById(id);
        if(agendaItem != null) {
            if(agendaItem.getAgenda().getDate().equals(agendaItemRequest.getAgenda().getDate())) {
                agendaItemRequest.setAgendaItemId(agendaItem.getAgendaItemId());
                return new ResponseEntity<>(agendaItemService.save(agendaItemRequest.getAgendaItem()), HttpStatus.OK);
            } else {
                Optional<Agenda> agendaOptional = agendaService.findByUserIdAndDate(agendaItemRequest.getAgenda());
                if(agendaOptional.isPresent()) {
                    AgendaItem agendaItemOutherAgenda = agendaItemRequest.getAgendaItem();
                    agendaItemOutherAgenda.setAgendaId(agendaOptional.get().getAgendaId());
                    agendaItemOutherAgenda.setAgenda(agendaOptional.get());
                    return new ResponseEntity<>(agendaItemOutherAgenda, HttpStatus.OK);
                } else {
                    AgendaItem updatedAgendaItem = agendaItemRequest.getAgendaItem();
                    Agenda agenda = new Agenda(null, agendaItemRequest.getAgenda().getUserId(), agendaItemRequest.getAgenda().getDate());
                    agenda = agendaService.save(agenda);
                    updatedAgendaItem.setAgenda(agenda);
                    updatedAgendaItem.setAgendaId(agenda.getAgendaId());
                    return new ResponseEntity<>(agendaItemService.save(updatedAgendaItem), HttpStatus.OK);
                }
            }
        }

        throw new NotFoundException("Item da Agenda não existe");
    }

    @PutMapping("/agendaItem/status/{id}")
    public ResponseEntity<?> putStatus(@RequestBody AgendaItem agendaItemRequest, @PathVariable("id") Long id) throws NotFoundException {
        AgendaItem agendaItem = agendaItemService.findById(id);
        if(agendaItem != null) {
            agendaItem.setStatus(agendaItemRequest.getStatus());
            return new ResponseEntity<>(agendaItemService.save(agendaItem), HttpStatus.OK);
        }

        throw new NotFoundException("Item da Agenda não existe");
    }
}
