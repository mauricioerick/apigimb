package br.com.gimb.ws.controller;

import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.gimb.ws.enumerated.EmailDomainEnum;
import br.com.gimb.ws.service.EmailService;
import br.com.gimb.ws.util.Util;

@RestController
public class EmailController {
	
	@Autowired
	private EmailService emailService;
	
	private static String REPONSE = "{\"message\": \"%s\"}";

	@RequestMapping(value = "/sendmail/{domain}", method = RequestMethod.POST)
	public ResponseEntity<String> sendMail(@RequestHeader Map<String, String> header, @PathVariable String domain, @RequestBody Map<String, String> body) {
		String language = header.get("language") == null ? "pt" : header.get("language");
		
		try {
			emailService.sendmail(EmailDomainEnum.valueOf(domain), body);
		} catch (AddressException e) {
			return new ResponseEntity<String>(Util.msgBundle(language, REPONSE, "userOrPassInvalid"), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (MessagingException e) {
			return new ResponseEntity<String>(Util.msgBundle(language, REPONSE, "userOrPassInvalid"), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			return new ResponseEntity<String>(Util.msgBundle(language, REPONSE, "userOrPassInvalid"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<String>(Util.msgBundle(language, "tudo tranquilo no mamilo", "userOrPassInvalid"), HttpStatus.OK);
	}

}
