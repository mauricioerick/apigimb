package br.com.gimb.ws.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {
	
	@RequestMapping({
	"/",
	"/apiGIMB",
	"login",
    "client",
    "clientCreate",
    "clientDetail/{clientId}",
    "clientIntegracao/{situacao}",
    "removeFromClientVehicle/{vehicleId}",
    "vehicle",
    "vehicleCreate",
    "vehicleDetail/{vehicleId}",
    "user",
    "userCreate",
    "userDetail/{userId}",
    "userServices/{userId}",
    "event",
    "eventCreate",
    "eventDetail/{eventId}",
    "op/service",
    "op/serviceDetail/{serviceId}",
    "op/serviceCreate",
    "op/serviceUpdate/{serviceId}",
    "servicoWeb",
    "point-record",
    "point-record/{pointRecordId}",
    "point-record/novo",
    "action",
    "actionCreate",
    "actionDetail/{actionId}",
    "equipment",
    "equipmentCreate",
    "equipmentDetail/{equipmentId}",
    "expense",
    "expenseCreate",
    "expenseDetail/{equipmentId}",
    "settlement",
    "settlementDetail/{settlementId}",
    "settlementSummary",
    "ds/timeline",
    "ds/trace",
    "op/location",
    "ds/serviceChart",
    "ds/offendersChart",
    "ds/inventorysChart",
    "profile",
    "profileCreate",
    "profileDetail/{profileId}",
    "home",
    "ds/appointmentsChart",
    "observation",
    "observationCreate",
    "observationDetail/{observationId}",
    "objectType",
    "objectTypeCreate",
    "inventorySummary",
    "objectTypeDetail/{objectTypeId}",
    "storage",
    "storageCreate",
    "storageDetail/{storageId}",
    "editImage/{objId}",
    "editImageService/{objId}",
    "typePayment",
    "typePaymentCreate",
    "typePaymentDetail/{typePaymentId}",
    "extract",
    "extractDetail/{userId}",
    "companyDetail",
    "object",
    "objectDetail/{objectId}",
    "costCenter",
    "costCenterDetail/{costCenterId}",
    "natureExpense",
    "natureExpenseDetail/{natureExpenseId}",
    "area",
    "areaDetail/{areaId}",
    "document",
    "documentDetail/{documentId}",
    "costCenterCreate",
    "natureExpenseCreate",
    "cardDetail/{cardId}",
    "card",
    "cardCreate",
    "cardExtract",
    "cardExtractDetail/{cardId}/{startDate}/{endDate}",
    "cardExtractCreate",
    "tableParameters",
    "tableParametersDetail/{referenceTable}",
    "project",
    "projectDetail/{projectId}",
    "projectCreate",
    "agenda",
    "importXls",
    "tableForImport",
    "tool",
    "toolCreate",
    "toolDetail/{toolId}",
    "schedule",
    "scheduleCreate",
    "scheduleItemUpdate/{agendaItemId}"
    })
    public String index() {
        return "index";
    }
}
