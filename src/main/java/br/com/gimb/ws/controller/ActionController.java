package br.com.gimb.ws.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.gimb.ws.model.Action;
import br.com.gimb.ws.service.ActionService;

@RestController
@RequestMapping("/admin")
public class ActionController extends BaseController {

	@Autowired
	private ActionService actionService;

	@RequestMapping(value = "/action", method = RequestMethod.GET)
	@ResponseBody
	public List<Action> index() {
		setRepositories();

		return actionService.findAll();
	}

	@RequestMapping(value = "/actionByActive/{active}", method = RequestMethod.GET)
	public List<Action> actionByActive(@PathVariable String active) {
		setRepositories();
		return actionService.findByActive(active);
	}

	@RequestMapping(value = "/action/{id}", method = RequestMethod.GET)
	public Action findById(@PathVariable long id) {
		setRepositories();
		return actionService.findById(id);
	}

	@RequestMapping(value = "/actionCreate", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Action> create(@RequestBody Action action) {
		setRepositories();

		try {
			actionService.save(action);
		} catch (Exception e) {
			action = null;
			e.printStackTrace();
			return new ResponseEntity<Action>(action, HttpStatus.CONFLICT);
		}

		return new ResponseEntity<Action>(action, HttpStatus.CREATED);
	}

	@RequestMapping("/deleteAction/{id}")
	@ResponseBody
	public ResponseEntity<Action> delete(@PathVariable long id) {
		setRepositories();

		Action action = actionService.findById(id);

		try {
			actionService.delete(action);
		} catch (Exception e) {
			return new ResponseEntity<Action>(action, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<Action>(new Action(), HttpStatus.OK);
	}

	@RequestMapping(value = "/updateAction/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Action> update(@RequestBody Action action, @PathVariable long id) {
		setRepositories();

		try {
			action.setActionId(id);
			actionService.save(action);
		} catch (Exception e) {
			return new ResponseEntity<Action>(action, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<Action>(action, HttpStatus.OK);
	}

}
