package br.com.gimb.ws.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.gimb.ws.enumerated.ReferenceTypeEnum;
import br.com.gimb.ws.model.Area;
import br.com.gimb.ws.model.CustomPicture;
import br.com.gimb.ws.model.Document;
import br.com.gimb.ws.service.AreaService;
import br.com.gimb.ws.service.CustomPictureService;
import br.com.gimb.ws.service.DocumentService;
import br.com.gimb.ws.util.aws.AwsS3Utils;

@RestController
@RequestMapping("/admin")
public class DocumentController extends BaseController {
	@Autowired
	private DocumentService documentService;
	
	@Autowired
	private AreaService areaService;
	
	@Autowired
	private CustomPictureService customPictureService;
	
	@RequestMapping(value = "/allDocuments", method = RequestMethod.GET)
	@ResponseBody
	public List<Document> allDocuments() {
		setRepositories();
		try {
			return documentService.findAll();
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@RequestMapping(value = "/document/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Document document(@PathVariable long id) {
		setRepositories();
		try {
			Document document = documentService.findByDocumentId(id);
			document.setPicsList(customPictureService.findByReferenceTypeAndReferenceId(ReferenceTypeEnum.DC, String.valueOf(id)));
			AwsS3Utils.generatePreSignedUrl(document.getPicsList(), baseName());
			return document;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@RequestMapping(value = "/createDocument", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<Document> createDocument(@RequestBody Document document){
		setRepositories();
		try {
			List<Area> area = areaService.findByDescription(document.getArea().getDescription());
			document.setArea(area.get(0));
			documentService.save(document);
			
			List<CustomPicture> customPictures = document.getPicsList();
			
			if(customPictures != null && !customPictures.isEmpty()) {
				int idx = 1;
				for(CustomPicture c : customPictures) {
					c.setReferenceId(String.valueOf(document.getDocumentId()));
					c = customPictureService.convertBase64ToImage(c, idx, AwsS3Utils.initializeS3Client(baseName()), baseName());
					customPictureService.save(c);
					idx++;
				}
			}
			
			return new ResponseEntity<Document>(document, HttpStatus.OK);
		}catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Document>(document, HttpStatus.EXPECTATION_FAILED);
		}
	}
}
