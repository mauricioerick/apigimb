package br.com.gimb.ws.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.gimb.ws.model.Client;
import br.com.gimb.ws.model.Vehicle;
import br.com.gimb.ws.service.ClientService;
import br.com.gimb.ws.service.ContactService;
import br.com.gimb.ws.service.VehicleService;

@RestController()
@RequestMapping("/admin")
public class ClientController extends BaseController {

	@Autowired
	private ClientService clientService;

	@Autowired
	private VehicleService vehicleService;
	
	@Autowired
	private ContactService ContactsService;

	@RequestMapping(value = "/client", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object index() {
		setRepositories();

		List<Client> retorno = clientService.findAll();

		for (Client client : retorno) {
			if (client.getSituacao() == null) {
				client.setSituacao("ativo");
			}
		}

		return retorno;
	}

	@RequestMapping(value = "/clientByActive/{active}", method = RequestMethod.GET)
	public List<Client> clientByActive(@PathVariable String active) {
		setRepositories();
		
		List<Client> retorno = clientService.findByActive(active);
		for(Client client : retorno) {
			client.setVehicles(vehicleService.findVehicleByClientId(client));
			client.setContacts(ContactsService.findByClient(client));
		}
			

		return retorno;
	}

	@RequestMapping(value = "/clientIntegracao/{situacao}", method = RequestMethod.GET)
	@ResponseBody
	public Object clientIntegracao(@PathVariable String situacao) {
		setRepositories();
		
		List<Client> retorno = clientService.findAllBySituacao(situacao);
		for(Client client : retorno)
			client.setVehicles(vehicleService.findVehicleByClientId(client));

		return retorno;
	}

	@RequestMapping(value = "/client/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Object findById(@PathVariable long id) {
		setRepositories();

		Client retorno = clientService.findById(id);
		retorno.setVehicles(vehicleService.findVehicleByClientId(retorno));

		return retorno;
	}

	@RequestMapping(value = "/createClient", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Client> create(@RequestBody Client client) {
		setRepositories();

		try {
			clientService.save(client);

			if (client.getVehicles() != null && client.getVehicles().size() > 0) {
				for (Vehicle veiculo : client.getVehicles()) {
					veiculo.setClient(client);
					vehicleService.save(veiculo);
				}
			}
		} catch (Exception e) {
			return new ResponseEntity<Client>(client, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<Client>(client, HttpStatus.CREATED);
	}

	@RequestMapping("/deleteClient/{id}")
	@ResponseBody
	public ResponseEntity<Client> delete(@PathVariable long id) {
		setRepositories();

		Client client = clientService.findById(id);
		try {

			if (client == null)
				return new ResponseEntity<Client>(client, HttpStatus.NOT_FOUND);

			clientService.delete(client);
		} catch (Exception e) {
			return new ResponseEntity<Client>(client, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<Client>(new Client(), HttpStatus.OK);
	}

	@RequestMapping(value = "/updateClient/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Client> update(@RequestBody Client client, @PathVariable long id) {
		setRepositories();

		try {
			client.setClientId(id);
			clientService.save(client);
		} catch (Exception e) {
			return new ResponseEntity<Client>(client, HttpStatus.EXPECTATION_FAILED);
		}

		return new ResponseEntity<Client>(client, HttpStatus.OK);
	}

	@RequestMapping(value = "/clientById/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Client findClientById(@PathVariable long id) {
		setRepositories();

		Client retorno = clientService.findById(id);		

		return retorno;
	}
}
