package br.com.gimb.ws.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.gimb.ws.model.Client;
import br.com.gimb.ws.model.Contact;
import br.com.gimb.ws.service.ClientService;
import br.com.gimb.ws.service.ContactService;

@RestController()
@RequestMapping("/admin")
public class ContactController extends BaseController {
	
	@Autowired
	private ContactService contactsService;
	
	@Autowired
	private ClientService clientService;
	
	@RequestMapping(value="/contacts/{clientId}", method = RequestMethod.GET)
	@ResponseBody
	public List<Contact> contacts(@PathVariable Long clientId){
		setRepositories();
		
		Client client = clientService.findById(clientId);
		List<Contact> contacts = contactsService.findByClient(client);
		
		return contacts;
	}
	
	@RequestMapping(value="/contact/{contactId}", method = RequestMethod.GET)
	@ResponseBody
	public Object select(@PathVariable Long contactId){
		setRepositories();
		
		Contact contact = null;
		try {
			contact = contactsService.findById(contactId);
		}catch (Exception e) {
			return new ResponseEntity<Contact>(contact, HttpStatus.EXPECTATION_FAILED);	 
		}		
		return contact;
	}
	
	@RequestMapping(value="/deleteContact/{id}")
	@ResponseBody
	public ResponseEntity<Contact> delete(@PathVariable Long id){
		setRepositories();
		Contact contacts = contactsService.findById(id);
		try {
			if(contacts == null) return new ResponseEntity<Contact>(contacts, HttpStatus.NOT_FOUND);
			contactsService.delete(contacts);
		}catch (Exception e) {
			return new ResponseEntity<Contact>(contacts, HttpStatus.EXPECTATION_FAILED);			
		}
		return new ResponseEntity<Contact>(new Contact(), HttpStatus.OK);
	}
	
	
	@RequestMapping(value="/updateContacts/{clientId}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<Contact> update(@RequestBody Contact contacts, @PathVariable Long clientId){
		setRepositories();
		
		try {
			Client client = clientService.findById(clientId);
			contacts.setClient(client);
			contactsService.save(contacts);
			
		}catch (Exception e) {
			return new ResponseEntity<Contact>(contacts, HttpStatus.EXPECTATION_FAILED);
		}
		return new ResponseEntity<Contact>(contacts, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/createContact/{clientId}", method = RequestMethod.PUT)
	@ResponseBody
	public Object create(@RequestBody Map<String, Object> params, @PathVariable Long clientId){
		setRepositories();
		
		Client client = clientService.findById(clientId);
		
		String name = (String) params.get("name");
		String telefone = (String) params.get("phone");
		String obs = (String) params.get("observation");
		String cargo = (String) params.get("office");
		
		Contact contacts = new Contact();
		
		contacts.setName(name);
		contacts.setObservation(obs);
		contacts.setPhone(telefone);
		contacts.setOffice(cargo);
		contacts.setClient(client);
		
		try {
			contactsService.save(contacts);
		}catch (Exception e) {
			return new ResponseEntity<Contact>(contacts, HttpStatus.EXPECTATION_FAILED);
		}
		return contacts;
	}
}






















