package br.com.gimb.ws.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.gimb.ws.model.Area;
import br.com.gimb.ws.model.AreaUser;
import br.com.gimb.ws.service.AreaService;
import br.com.gimb.ws.service.AreaUserService;

@RestController
@RequestMapping("/admin")
public class AreaController extends BaseController{
	@Autowired
	private AreaService areaService;
	
	@Autowired
	AreaUserService areaUserService;
	
	@RequestMapping(value = "/allArea")
	@ResponseBody
	public List<Area> allArea(){
		setRepositories();
		return areaService.findAll();
	}
	
	@RequestMapping(value = "/area/{areaId}", method = RequestMethod.GET)
	@ResponseBody
	public Area area(@PathVariable long areaId){
		setRepositories();
		return areaService.findByAreaId(areaId);
	}
	
	@RequestMapping(value = "/areaActive/{active}", method = RequestMethod.GET)
	@ResponseBody
	public List<Area> areaActive(@PathVariable String active){
		setRepositories();
		return areaService.findByActive(active);
	}
	
	@RequestMapping(value = "/saveArea", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<Area> saveArea(@RequestBody Area area){
		setRepositories();
		try {			
			if(area.getUsers() != null && area.getUsers().size() > 0) {
				for(AreaUser areaUser : area.getUsers())
					areaUserService.save(areaUser);
			}
			areaService.save(area);
			return new ResponseEntity<Area>(area, HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<Area>(area, HttpStatus.EXPECTATION_FAILED);
		}
	}
}
