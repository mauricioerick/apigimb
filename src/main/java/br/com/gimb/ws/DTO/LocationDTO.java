package br.com.gimb.ws.DTO;

public class LocationDTO {
	private Double latitude;
	private Double longitude;
	private String traceStartTime;
	private long userId;
	private String name;
	private String actionStartTime;
	private String actionDescription;
	private String client;
	
	public LocationDTO(Double latitude, Double longitude, String traceStartTime, long userId, String name,
			String actionStartTime, String actionDescription, String client) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
		this.traceStartTime = traceStartTime;
		this.userId = userId;
		this.name = name;
		this.actionStartTime = actionStartTime;
		this.actionDescription = actionDescription;
		this.client = client;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getTraceStartTime() {
		return traceStartTime;
	}

	public void setTraceStartTime(String traceStartTime) {
		this.traceStartTime = traceStartTime;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getActionStartTime() {
		return actionStartTime;
	}

	public void setActionStartTime(String actionStartTime) {
		this.actionStartTime = actionStartTime;
	}

	public String getActionDescription() {
		return actionDescription;
	}

	public void setActionDescription(String actionDescription) {
		this.actionDescription = actionDescription;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}
}
