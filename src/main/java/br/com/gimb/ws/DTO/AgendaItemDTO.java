package br.com.gimb.ws.DTO;

import br.com.gimb.ws.model.Agenda;
import br.com.gimb.ws.model.AgendaItem;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

public class AgendaItemDTO {

    private Long agendaItemId;
    private Long agendaId;
    private Long clientId;
    private Long actionId;
    private Long vehicleId;
    private Long projectId;
    private String type;
    private Integer status;
    private String note;
    private Agenda agenda;

    public AgendaItemDTO() {
    }

    public AgendaItemDTO(AgendaItem agendaItem) {
        this(
                agendaItem.getAgendaItemId(),
                agendaItem.getAgendaId(),
                agendaItem.getClientId(),
                agendaItem.getActionId(),
                agendaItem.getVehicleId(),
                agendaItem.getProjectId(),
                agendaItem.getType(),
                agendaItem.getStatus(),
                agendaItem.getNote(),
                agendaItem.getAgenda()
        );
    }

    public AgendaItemDTO(Long agendaItemId, Long agendaId, Long clientId, Long actionId, Long vehicleId, Long projectId, String type, Integer status, String note, Agenda agenda) {
        this.agendaItemId = agendaItemId;
        this.agendaId = agendaId;
        this.clientId = clientId;
        this.actionId = actionId;
        this.vehicleId = vehicleId;
        this.projectId = projectId;
        this.type = type;
        this.status = status;
        this.note = note;
        this.agenda = agenda;
        this.agenda.setDate(agenda.getDate().plusDays(1));

    }

    public Long getAgendaItemId() {
        return agendaItemId;
    }

    public void setAgendaItemId(Long agendaItemId) {
        this.agendaItemId = agendaItemId;
    }

    public Long getAgendaId() {
        return agendaId;
    }

    public void setAgendaId(Long agendaId) {
        this.agendaId = agendaId;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Long getActionId() {
        return actionId;
    }

    public void setActionId(Long actionId) {
        this.actionId = actionId;
    }

    public Long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    public Agenda getAgenda() {
        return agenda;
    }

    public void setAgenda(Agenda agenda) {
        this.agenda = agenda;
    }

    @JsonIgnore
    public AgendaItem getAgendaItem() {
        return new AgendaItem(
                getAgendaItemId(),
                getAgendaId(),
                getClientId(),
                getActionId(),
                getVehicleId(),
                getProjectId(),
                getType(),
                getStatus(),
                getNote()
        );
    }
}
