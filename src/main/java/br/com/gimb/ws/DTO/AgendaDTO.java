package br.com.gimb.ws.DTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

public class AgendaDTO {

    private LocalDate date;
    private Long agendaItemId;
    private String companyName;
    private String actionDescription;
    private String vehicleDescription;
    private String projectName;
    private String type;
    private String note;
    private Integer status;

    public AgendaDTO() { }

    public AgendaDTO(LocalDate date, Long agendaItemId, String companyName, String actionDescription, String vehicleDescription,
                     String projectName, String type, String note, Integer status) {
        this.date = date;
        this.agendaItemId = agendaItemId;
        this.companyName = companyName;
        this.actionDescription = actionDescription;
        this.vehicleDescription = vehicleDescription;
        this.projectName = projectName;
        this.type = type;
        this.note = note;
        this.status = status;
    }

    @JsonFormat(pattern = "dd/MM/yyyy")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    public LocalDate getDate() {
        return date.plusDays(1);
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Long getAgendaItemId() {
        return agendaItemId;
    }

    public void setAgendaItemId(Long agendaItemId) {
        this.agendaItemId = agendaItemId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getActionDescription() {
        return actionDescription;
    }

    public void setActionDescription(String actionDescription) {
        this.actionDescription = actionDescription;
    }

    public String getVehicleDescription() {
        return vehicleDescription;
    }

    public void setVehicleDescription(String vehicleDescription) {
        this.vehicleDescription = vehicleDescription;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Boolean getStatus() {
        return status == 1 ?  true : false;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
