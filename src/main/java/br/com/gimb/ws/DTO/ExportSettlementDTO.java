package br.com.gimb.ws.DTO;

public class ExportSettlementDTO {
    public String startDate = null;
    public String endDate = null;
    public String clientDocument = null;
    public String userName = null;
    public String approveStatus = null;
    public String eventDescription = null;
    public String natureExpenseId = null;
}
