package br.com.gimb.ws.DTO;

import java.util.ArrayList;
import java.util.List;

import br.com.gimb.ws.model.Services;

public class UserServiceDTO {
	
	private String usuario;
	private String data;
	private Integer qtdeServicos;
	private List<Services> listaServicos;
	
	public UserServiceDTO() {
		super();
		listaServicos = new ArrayList<>();
	}

	public String getUsuario() {
		return usuario;
	}
	
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	public String getData() {
		return data;
	}
	
	public void setData(String data) {
		this.data = data;
	}
	
	public Integer getQtdeServicos() {
		return qtdeServicos == null ? 0 : qtdeServicos;
	}
	
	public void setQtdeServicos(Integer qtdeServicos) {
		this.qtdeServicos = qtdeServicos;
	}

	public List<Services> getListaServicos() {
		return listaServicos;
	}

	public void setListaServicos(List<Services> listaServicos) {
		this.listaServicos = listaServicos;
	}

}
