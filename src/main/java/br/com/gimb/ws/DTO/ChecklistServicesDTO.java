package br.com.gimb.ws.DTO;

import java.util.ArrayList;
import java.util.List;

public class ChecklistServicesDTO {
	
	private String checklistServicesId;
	private String checklistName;
	private String criticallyHigh;
	private String criticallyMedium;
	private String criticallyLow;
	
	public ChecklistServicesDTO() {
		super();		
	}

	public String getChecklistServicesId() {
		return checklistServicesId;
	}

	public void setChecklistServicesId(String checklistServicesId) {
		this.checklistServicesId = checklistServicesId;
	}

	public String getChecklistName() {
		return checklistName;
	}

	public void setChecklistName(String checklistName) {
		this.checklistName = checklistName;
	}

	public String getCriticallyHigh() {
		return criticallyHigh;
	}

	public void setCriticallyHigh(String criticallyHigh) {
		this.criticallyHigh = criticallyHigh;
	}

	public String getCriticallyMedium() {
		return criticallyMedium;
	}

	public void setCriticallyMedium(String criticallyMedium) {
		this.criticallyMedium = criticallyMedium;
	}

	public String getCriticallyLow() {
		return criticallyLow;
	}

	public void setCriticallyLow(String criticallyLow) {
		this.criticallyLow = criticallyLow;
	}

		
}
