package br.com.gimb.ws.DTO;

import java.io.Serializable;

import br.com.gimb.ws.enumerated.TimeLineTypeEnum;

public class TimeLineFeedDTO implements Serializable{
	private static final long serialVersionUID = -5391195356004988177L;
	private String user;
	private String message;
	private String date;
	private String time;
	private Long id;
	private TimeLineTypeEnum timeLineType;
	private String note;
	private String client;

	public TimeLineFeedDTO(String user, String message, String date, String time, Long id, String timeLineType, String note, String client) {
		this.user = user;
		this.message = message;
		this.date = date;
		this.time = time;
		this.id = id;
		this.timeLineType = TimeLineTypeEnum.valueOf(timeLineType);
		this.note = note;
		this.client = client;
	}
	
	public TimeLineFeedDTO(String message, String date, TimeLineTypeEnum timeLineType) {
		this.user = "";
		this.message = message;
		this.date = date;
		this.time = "";
		this.id = -1L;
		this.timeLineType = timeLineType;
		this.note = "";
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TimeLineTypeEnum getTimeLineType() {
		return timeLineType;
	}

	public void setTimeLineType(TimeLineTypeEnum timeLineType) {
		this.timeLineType = timeLineType;
	}

	public String getTypeDescription() {
		return (timeLineType == null || timeLineType == TimeLineTypeEnum.GEN || timeLineType == TimeLineTypeEnum.NON) ? "" : timeLineType.getDescription();
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

}
