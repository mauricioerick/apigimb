package br.com.gimb.ws.DTO;

import br.com.gimb.ws.model.Balance;
import br.com.gimb.ws.model.Card;
import br.com.gimb.ws.model.TypePayment;
import br.com.gimb.ws.model.User;

public class BalanceDTO {;
	
	private String day;
	
	private String month;
	
	private String year;
	
	private String lastCreditDate;
	
	private Double balance;
	
	private Double pendingBalance;

	private Double reprovedBalance;
	
	private User user;
	
	private TypePayment typePayment;
	
	private Card card;
	
	public Card getCard() {
		return card;
	}

	public void setCard(Card card) {
		this.card = card;
	}

	public static BalanceDTO builder() {
		return new BalanceDTO();
	}

	public String getDay() {
		return day;
	}

	public BalanceDTO setDay(String day) {
		this.day = day;
		return this;
	}

	public String getMonth() {
		return month;
	}

	public BalanceDTO setMonth(String month) {
		this.month = month;
		return this;
	}

	public String getYear() {
		return year;
	}

	public BalanceDTO setYear(String year) {
		this.year = year;
		return this;
	}
	
	public String getDate() {
		return String.format("%s/%s/%s", day, month, year);
	}

	public String getLastCreditDate() {
		return lastCreditDate;
	}

	public BalanceDTO setLastCreditDate(String lastCreditDate) {
		this.lastCreditDate = lastCreditDate;
		return this;
	}

	public Double getBalance() {
		return balance;
	}

	public BalanceDTO setBalance(Double balance) {
		this.balance = balance;
		return this;
	}

	public Double getPendingBalance() {
		return pendingBalance;
	}

	public BalanceDTO setPendingBalance(Double pendingBalance) {
		this.pendingBalance = pendingBalance;
		return this;
	}
	
	public Double getAvailableBalance() {
		if (balance == null) balance = 0d;
		if (pendingBalance == null) pendingBalance = 0d;
		return Double.sum(balance, pendingBalance);
	}

	public User getUser() {
		return user;
	}

	public BalanceDTO setUser(User user) {
		this.user = user;
		return this;
	}

	public TypePayment getTypePayment() {
		return typePayment;
	}

	public BalanceDTO setTypePayment(TypePayment typePayment) {
		this.typePayment = typePayment;
		return this;
	}

	public Double getReprovedBalance() {
		return reprovedBalance;
	}

	public BalanceDTO setReprovedBalance(Double reprovedBalance) {
		this.reprovedBalance = reprovedBalance;
		return this;
	}
	
	public BalanceDTO setObjBalance(Balance balance) {
		setDay(balance.getDay())
		.setMonth(balance.getMonth())
		.setYear(balance.getYear())
		.setBalance(balance.getBalance())
		.setUser(balance.getUser())
		.setTypePayment(balance.getTypePayment());
		
		return this;
	}

}
