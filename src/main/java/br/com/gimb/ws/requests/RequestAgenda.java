package br.com.gimb.ws.requests;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class RequestAgenda {

    private final DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    private String dtInicial;
    private String dtFinal;
    private String status;
    private Long userId;

    public RequestAgenda() { }

    public RequestAgenda(String dtInicial, String dtFinal, String status, Long userId) {
        this.dtInicial = dtInicial;
        this.dtFinal = dtFinal;
        this.status = status;
        this.userId = userId;
    }

    public String getDtInicial() {
        return dtInicial;
    }

    public LocalDate getDtInicialBySearch() throws ParseException {
        return LocalDate.parse(this.dtInicial, format);
    }

    public void setDtInicial(String dtInicial) {
        this.dtInicial = dtInicial;
    }

    public String getDtFinal() {
        return dtFinal;
    }

    public LocalDate getDtFinalBySearch() throws ParseException {
        return LocalDate.parse(this.dtFinal, format);
    }

    public void setDtFinal(String dtFinal) {
        this.dtFinal = dtFinal;
    }

    public String getStatus() {
        return status;
    }

    public Integer getStatusBySearch() {
        if(this.status.equalsIgnoreCase("active")) {
            return 1;
        } else if(this.status.equalsIgnoreCase("inactive")) {
            return 0;
        } else {
            return null;
        }
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
