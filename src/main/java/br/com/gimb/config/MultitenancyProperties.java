/**
 * Copyright 2018 onwards - Sunit Katkar (sunitkatkar@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package br.com.gimb.config;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("multitenancy.mtapp")
public class MultitenancyProperties {

	private String lclUsername;
	private String lclPassword;
	private List<DataSourceProperties> lclDataSources;
    
    private String prdUsername;
	private String prdPassword;
    private List<DataSourceProperties> prdDataSources;
	
	private String pocUsername;
	private String pocPassword;
    private List<DataSourceProperties> pocDataSources;

    public String getLclUsername() {
		return lclUsername;
	}

	public void setLclUsername(String lclUsername) {
		this.lclUsername = lclUsername;
	}

	public String getLclPassword() {
		return lclPassword;
	}

	public void setLclPassword(String lclPassword) {
		this.lclPassword = lclPassword;
	}

	public List<DataSourceProperties> getLclDataSources() {
		return lclDataSources;
	}

	public void setLclDataSources(List<DataSourceProperties> lclDataSources) {
		this.lclDataSources = lclDataSources;
	}

	public String getPrdUsername() {
		return prdUsername;
	}

	public void setPrdUsername(String prdUsername) {
		this.prdUsername = prdUsername;
	}

	public String getPrdPassword() {
		return prdPassword;
	}

	public void setPrdPassword(String prdPassword) {
		this.prdPassword = prdPassword;
	}

	public List<DataSourceProperties> getPrdDataSources() {
		return prdDataSources;
	}

	public void setPrdDataSources(List<DataSourceProperties> prdDataSources) {
		this.prdDataSources = prdDataSources;
	}

	public String getPocUsername() {
		return pocUsername;
	}

	public void setPocUsername(String pocUsername) {
		this.pocUsername = pocUsername;
	}

	public String getPocPassword() {
		return pocPassword;
	}

	public void setPocPassword(String pocPassword) {
		this.pocPassword = pocPassword;
	}

	public List<DataSourceProperties> getPocDataSources() {
		return pocDataSources;
	}

	public void setPocDataSources(List<DataSourceProperties> pocDataSources) {
		this.pocDataSources = pocDataSources;
	}

	public static class DataSourceProperties extends org.springframework.boot.autoconfigure.jdbc.DataSourceProperties {

        private String name;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
    }
}
