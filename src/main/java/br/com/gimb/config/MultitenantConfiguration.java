package br.com.gimb.config;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.MigrationInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.gimb.ws.enumerated.EnvironmentEnum;
import br.com.gimb.ws.util.Util;

@Configuration
public class MultitenantConfiguration {

    @Autowired
    private DataSourceProperties properties;
    
    @Autowired
    private MultitenancyProperties multiProperties;
    
    public static String LIST = "LIST";
    public static String USER = "USER";
    public static String PASS = "PASS";

    /**
     * Defines the data source for the application
     * @return
     */
    @Bean
    @ConfigurationProperties(
            prefix = "spring.datasource"
    )
    public DataSource dataSource() {
        // Create the final multi-tenant source.
        // It needs a default database to connect to.
        // Make sure that the default database is actually an empty tenant database.
        // Don't use that for a regular tenant if you want things to be safe!
    	MultitenantDataSource dataSource = new MultitenantDataSource();
        dataSource.setDefaultTargetDataSource(defaultDataSource());
        dataSource.setTargetDataSources(resolvedDataSources());
        // Call this to finalize the initialization of the data source.
        dataSource.afterPropertiesSet();
        
        return dataSource;
    }
    
    private Map<String, Object> environmentProperties() {
        EnvironmentEnum environment = EnvironmentEnum.LCL;
        try {
            if (System.getenv(Util.GIMB_ENV) != null)
            	environment = EnvironmentEnum.valueOf(System.getenv(Util.GIMB_ENV));			
		} catch (Exception e) {
			environment = EnvironmentEnum.LCL;
		}

        String username = "";
        String password = "";
		List<br.com.gimb.config.MultitenancyProperties.DataSourceProperties> dataSourceList = null;
		switch (environment) {
			case LCL:
				dataSourceList = multiProperties.getLclDataSources();
				username = multiProperties.getLclUsername();
				password = multiProperties.getLclPassword();
				break;
			case POC:
				dataSourceList = multiProperties.getPocDataSources();
				username = multiProperties.getPocUsername();
				password = multiProperties.getPocPassword();
				break;
			case PRD:
				dataSourceList = multiProperties.getPrdDataSources();
				username = multiProperties.getPrdUsername();
				password = multiProperties.getPrdPassword();
				break;
		}
		
		Map<String, Object> properties = new HashMap<>();
		properties.put(LIST, dataSourceList);
		properties.put(USER, username);
		properties.put(PASS, password);
		
		return properties;
    }

    /**
     * Creates the default data source for the application
     * @return
     */
    private DataSource defaultDataSource() {
    	Map<String, Object> envProp = environmentProperties();
    	String username = envProp.get(USER).toString();
    	String password = envProp.get(PASS).toString();
    	
    	DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create(this.getClass().getClassLoader())
                .driverClassName(properties.getDriverClassName())
                .url(properties.getUrl())
                .username(username)
                .password(password);

        if (properties.getType() != null)
            dataSourceBuilder.type(properties.getType());

        return dataSourceBuilder.build();
    }
    
    private Map<Object, Object> resolvedDataSources() {
    	Map<String, Object> envProp = environmentProperties();
    	List<br.com.gimb.config.MultitenancyProperties.DataSourceProperties> dataSourceList = (List<MultitenancyProperties.DataSourceProperties>) envProp.get(LIST);  
    	String username = envProp.get(USER).toString();
    	String password = envProp.get(PASS).toString();
    	Map<Object, Object> resolvedDataSources = new HashMap<>();
        
    	if (dataSourceList != null) {
        	for (DataSourceProperties dsProperties : dataSourceList) {
            	DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create(this.getClass().getClassLoader());
            	try {
            		dataSourceBuilder.driverClassName(properties.getDriverClassName())
            		.url(dsProperties.getUrl())
            		.username(username)
            		.password(password);

            		if (properties.getType() != null)
            			dataSourceBuilder.type(properties.getType());

            		resolvedDataSources.put(dsProperties.getName(), executeMigration(dataSourceBuilder.build()));
            	} catch (Exception e) {
            		e.printStackTrace();
            		return null;
            	}
            }
        }
        
        return resolvedDataSources;
    }
    
    private DataSource executeMigration(DataSource ds) {
    	try {
    		Boolean isEmpty = true;
    		Flyway flyway = Flyway.configure().dataSource(ds).locations("db/migration").load();
	        MigrationInfo current = flyway.info().current();
	        
	        final ResultSet tables = ds.getConnection().getMetaData().getTables(null, null, "user", null);
	        if (tables.next()) {
	        	isEmpty = false;
	        }
	        
			if (current == null && !isEmpty) 
				flyway.baseline(); // needed because flyway doesn't baseline empty schemas
			
			flyway.migrate();
	        
	        ds.getConnection().close();
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	return ds;
    }
}
