var appAccess = [
	{
		"id": "-1",
		"text": "",
		"label": "AppAccess.p",
		"children": [
			{
				"id": "0",
				"text": "",
				"label": "ServiceOrders",
				"children": [
					{
						"id": "0-0",
						"text": "",
						"label": "Access"
					}
				]
			},
			{
				"id": "1",
				"text": "",
				"label": "Settlements",
				"children": [
					{
						"id": "1-0",
						"text": "",
						"label": "Access"
					}
				]
			},
			{
				"id": "2",
				"text": "",
				"label": "PointRecords",
				"children": [
					{
						"id": "2-0",
						"text": "",
						"label": "Access"
					}
				]
			},
			{
				"id": "3",
				"text": "",
				"label": "Inventory",
				"children": [
					{
						"id": "3-0",
						"text": "",
						"label": "Access"
					}
				]
			}
		]
	}
];

var webAccess = [
	{
		"id": "-1",
		"text": "",
		"label": "WebAccess",
		"children": [
			{
				"id": "Cadastros",
				"text": "",
				"label": "Registrations",
				"children": [
					{
						"id": "Cadastros Atividade",
						"text": "",
						"label": "Actions"
					},
					{
						"id": "Cadastros Evento",
						"text": "",
						"label": "Events"
					},
					{
						"id": "Cadastros Equipamento",
						"text": "",
						"label": "Equipments"
					},
					{
						"id": "Cadastros Tipo Pagamento",
						"text": "",
						"label": "TypePayment"
					},
					{
						"id": "Cadastros Observacao",
						"text": "",
						"label": "Observation"
					},
					{
						"id": "Cadastros Cliente",
						"text": "",
						"label": "Clients"
					},
					{
						"id": "Cadastros Usuário",
						"text": "",
						"label": "User"
					},
					{
						"id": "Cadastros Perfil",
						"text": "",
						"label": "Profile"
					},
					{
						"id": "Cadastros Empresa",
						"text": "",
						"label": "Company"
					},
					{
						"id": "Cadastros Area",
						"text": "",
						"label": "area"
					},
					{
						"id": "Cadastros Projeto",
						"text": "",
						"label": "Project"
					},
					{
						"id": "Cadastros Ferramenta",
						"text": "",
						"label": "Tool.s"
					},
					{
                        "id": "Cadastros Agenda",
                        "text": "",
                        "label": "Schedule.s"
                    }
				]
			},
			{
				"id": "Operacional",
				"text": "",
				"label": "Operational",
				"children": [
					{
						"id": "Operacional Ordens de Serviço",
						"text": "",
						"label": "ServiceOrders.p",
					},
					{
						"id": "Operacional Localização",
						"text": "",
						"label": "Location"
					},
					{
						"id": "Operacional Documento",
						"text": "",
						"label": "document"
					},
					{
						"id": "Operacional Agenda",
						"text": "",
						"label": "agenda"
					},
					{
						"id": "Operacional Import",
						"text": "",
						"label": "importXls"
					},
				]
			},
			{
				"id": "Ponto",
				"text": "",
				"label": "Point",
				"children": [
					{
						"id": "Ponto Registro de Ponto",
						"text": "",
						"label": "PointRecords.p"
					}
				]
			},
			{
				"id": "Financeiro",
				"text": "",
				"label": "Financial",
				"children": [
					{
						"id": "Financeiro Cadastros",
						"text": "",
						"label": "Registrations",
						"children": [
							{
								"id": "Financeiro Cadastros Card",
								"text": "",
								"label": "card"
							},
							{
								"id": "Financeiro Cadastros Centro de Custo",
								"text": "",
								"label": "costCenter"
							},
							{
								"id": "Financeiro Cadastros Natureza de Gasto",
								"text": "",
								"label": "natureExpense"
							}
						]
					},
					{
						"id": "Financeiro Liquidações",
						"text": "",
						"label": "Settlements.p"
					},
					{
						"id": "Financeiro Despesas",
						"text": "",
						"label": "Expense"
					},
					{
						"id": "Financeiro Extrato Cartão",
						"text": "",
						"label": "CardExtract"
					},
					{
						"id": "Financeiro Extrato",
						"text": "",
						"label": "Extract"
					},
					{
						"id": "Financeiro Relatórios",
						"text": "",
						"label": "Reports",
						"children": [
							{
								"id": "Financeiro Relatórios Resumo de Liquidações",
								"text": "",
								"label": "SettlementsSummary"
							}
						]
					}
				]
			},
			{
				"id": "Inventario",
				"text": "",
				"label": "Inventory",
				"children": [
					{
						"id": "Inventario Cadastros",
						"text": "",
						"label": "Registrations",
						"children": [
							{
								"id": "Inventario Cadastros Tipo do Objeto",
								"text": "",
								"label": "ObjectType"
							},
							{
								"id": "Inventario Cadastros Estoque",
								"text": "",
								"label": "Storage"
							}
						]
					},
					{
						"id": "Inventario Cadastros Objeto",
						"text": "",
						"label": "Object"
					},
					{
						"id": "Inventario Relatórios",
						"text": "",
						"label": "Reports",
						"children": [
							{
								"id": "Inventario Relatórios Movimentações",
								"text": "",
								"label": "inventorySummary"
							}
						]
					}
				]
			},
			{
				"id": "Dashboard",
				"text": "",
				"label": "Dashboard",
				"children": [
					{
						"id": "Dashboard Linha do tempo",
						"text": "",
						"label": "Timeline"
					},
					{
						"id": "Dashboard Rastro",
						"text": "",
						"label": "Route"
					},
					{
						"id": "Dashboard Operacional",
						"text": "",
						"label": "Operational"
					},
					{
						"id": "Dashboard Apontamento",
						"text": "",
						"label": "Appointment"
					},
					{
						"id": "Dashboard Ofensores",
						"text": "",
						"label": "Offenders"
					},
					{
						"id": "Dashboard Inventario",
						"text": "",
						"label": "Inventory"
					}
				]
			}
		]
	}
];

var superUser =
{
	"id": "Super User Menu",
	"text": "",
	"label": "SuperUserMenu",
	"children": [
		{
			"id": "Table Parameters",
			"text": "",
			"label": "TableParameters"
		},
		{
			"id": "Tables For Imports",
			"text": "",
			"label": "TableImports"
		}
	]
};

function getHtmlSuperUser() {
	json = translateJson(superUser);
	html = geraHTML(superUser);
	return html;

}

function geraHTML(obj) {
	html = getHtmlList(obj);
	if (obj.children != null && obj.children instanceof Array) {
		for (var variable of obj.children) {
			html += geraHTML(variable);
		}
	}
	return html;
}

function getAppAccess() {
	json = translateJson(appAccess);

	return json;
}

function getWebAccess() {
	json = translateJson(webAccess);

	return json;
}

function translateJson(obj) {

	if (obj.label != null)
		obj.text = getLabel(obj.label);

	if ((obj.children != null) && (obj.children instanceof Array)) {
		for (var variable of obj.children) {
			translateJson(variable);
		}
	}
	else if (obj instanceof Array) {
		for (var vari of obj) {
			translateJson(vari);
		}
	}

	return obj;
}

function getHtmlList(obj) {
	htmlHeaderList = '<li data-toggle="collapse" data-target="#{0}" class="collapsed ng-scope bobbyVaiPraCasa">'
		+ '<a href="#"> <i class="{1}"></i> {{getLabel("{2}")}} <span class="arrow"></span> </a>'
		+ '</li>'
		+ '<ul class="sub-menu collapse" id="{3}">';

	htmlBodyList = '<li class="ng-scope bobbyVaiPraCasa" onclick="{0}">'
		+ '<a id="{1}" class="ng-binding ng-scope" href="{{appUrl}}{2}"> {{getLabel("{3}")}} </a>'
		+ '</li>';
	switch (obj.id) {
		case "Cadastros":
			htmlHeaderList = htmlHeaderList.replace("{0}", "cadastros");
			htmlHeaderList = htmlHeaderList.replace("{1}", "fa fa-pencil-square-o fa-lg");
			htmlHeaderList = htmlHeaderList.replace("{2}", obj.label);
			htmlHeaderList = htmlHeaderList.replace("{3}", "cadastros");
			return htmlHeaderList;
		case "Cadastros Atividade":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkAction.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkAction");
			htmlBodyList = htmlBodyList.replace("{2}", "action");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			return htmlBodyList;
		case "Cadastros Evento":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkEvent.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkEvent");
			htmlBodyList = htmlBodyList.replace("{2}", "event");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			return htmlBodyList;
		case "Cadastros Equipamento":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkEquipment.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkEquipment");
			htmlBodyList = htmlBodyList.replace("{2}", "equipment");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			return htmlBodyList;
		case "Cadastros Tipo Pagamento":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkTypePayment.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkTypePayment");
			htmlBodyList = htmlBodyList.replace("{2}", "typePayment");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			return htmlBodyList;
		case "Cadastros Observacao":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkObservation.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkObservation");
			htmlBodyList = htmlBodyList.replace("{2}", "observation");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			return htmlBodyList;
		case "Cadastros Cliente":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkClient.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkClient");
			htmlBodyList = htmlBodyList.replace("{2}", "client");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			return htmlBodyList;
		case "Cadastros Usuário":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkUser.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkUser");
			htmlBodyList = htmlBodyList.replace("{2}", "user");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			return htmlBodyList;
		case "Cadastros Perfil":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkProfile.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkProfile");
			htmlBodyList = htmlBodyList.replace("{2}", "profile");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			return htmlBodyList;
		case "Cadastros Empresa":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkCompany.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkCompany");
			htmlBodyList = htmlBodyList.replace("{2}", "companyDetail");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			return htmlBodyList;
		case "Cadastros Area":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkArea.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkArea");
			htmlBodyList = htmlBodyList.replace("{2}", "area");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			return htmlBodyList;
		case "Cadastros Projeto":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkProject.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkProject");
			htmlBodyList = htmlBodyList.replace("{2}", "project");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			return htmlBodyList;
		case "Cadastros Ferramenta":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkTool.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkTool");
			htmlBodyList = htmlBodyList.replace("{2}", "tool");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			return htmlBodyList;
        case "Cadastros Agenda":
            htmlBodyList = htmlBodyList.replace("{0}", "lnkSchedule.click()");
            htmlBodyList = htmlBodyList.replace("{1}", "lnkSchedule");
            htmlBodyList = htmlBodyList.replace("{2}", "schedule");
            htmlBodyList = htmlBodyList.replace("{3}", obj.label);
            return htmlBodyList;
		case "Operacional":
			htmlHeaderList = htmlHeaderList.replace("{0}", "operation");
			htmlHeaderList = htmlHeaderList.replace("{1}", "fa fa-gears fa-lg");
			htmlHeaderList = htmlHeaderList.replace("{2}", obj.label);
			htmlHeaderList = htmlHeaderList.replace("{3}", "operation");
			return htmlHeaderList;
		case "Operacional Ordens de Serviço":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkServices.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkServices");
			htmlBodyList = htmlBodyList.replace("{2}", "op/service");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			return htmlBodyList;
		case "Operacional Localização":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkLocation.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkLocation");
			htmlBodyList = htmlBodyList.replace("{2}", "op/location");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			return htmlBodyList;
		case "Operacional Documento":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkDocument.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkDocument");
			htmlBodyList = htmlBodyList.replace("{2}", "document");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			return htmlBodyList;
		case "Operacional Agenda":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkAgenda.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkAgenda");
			htmlBodyList = htmlBodyList.replace("{2}", "agenda");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			return htmlBodyList;
		case "Ponto":
			htmlHeaderList = htmlHeaderList.replace("{0}", "point");
			htmlHeaderList = htmlHeaderList.replace("{1}", "fa fa-clock-o fa-lg");
			htmlHeaderList = htmlHeaderList.replace("{2}", obj.label);
			htmlHeaderList = htmlHeaderList.replace("{3}", "point");
			return htmlHeaderList;
		case "Ponto Registro de Ponto":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkPointRecord.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkPointRecord");
			htmlBodyList = htmlBodyList.replace("{2}", "point-record");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			return htmlBodyList;
		case "Financeiro":
			htmlHeaderList = htmlHeaderList.replace("{0}", "financial");
			htmlHeaderList = htmlHeaderList.replace("{1}", "fa fa-money fa-lg");
			htmlHeaderList = htmlHeaderList.replace("{2}", obj.label);
			htmlHeaderList = htmlHeaderList.replace("{3}", "financial");
			return htmlHeaderList;
		case "Financeiro Liquidações":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkSettlements.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkSettlements");
			htmlBodyList = htmlBodyList.replace("{2}", "settlement");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			return htmlBodyList;
		case "Financeiro Despesas":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkExpenses.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkExpenses");
			htmlBodyList = htmlBodyList.replace("{2}", "expense");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			return htmlBodyList;
		case "Financeiro Extrato Cartão":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkCardExtract.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkCardExtract");
			htmlBodyList = htmlBodyList.replace("{2}", "cardExtract");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			return htmlBodyList;
		case "Financeiro Extrato":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkExtracts.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkExtracts");
			htmlBodyList = htmlBodyList.replace("{2}", "extract");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			return htmlBodyList;
		case "Financeiro Relatórios":
			htmlHeaderList = htmlHeaderList.replace("{0}", "reportsFinancial");
			htmlHeaderList = htmlHeaderList.replace("{1}", "fa fa-clone fa-lg");
			htmlHeaderList = htmlHeaderList.replace("{2}", obj.label);
			htmlHeaderList = htmlHeaderList.replace("sub-menu", "sub-menu1");
			htmlHeaderList = htmlHeaderList.replace("{3}", "reportsFinancial");
			return htmlHeaderList;
		case "Financeiro Relatórios Resumo de Liquidações":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkRepResume.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkRepResume");
			htmlBodyList = htmlBodyList.replace("{2}", "settlementSummary");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			htmlBodyList += '</ul>';
			return htmlBodyList;
		case "Financeiro Cadastros":
			htmlHeaderList = htmlHeaderList.replace("{0}", "registrationFinancial");
			htmlHeaderList = htmlHeaderList.replace("{1}", "fa fa-pencil-square-o fa-lg");
			htmlHeaderList = htmlHeaderList.replace("{2}", obj.label);
			htmlHeaderList = htmlHeaderList.replace("sub-menu", "sub-menu1");
			htmlHeaderList = htmlHeaderList.replace("{3}", "registrationFinancial");
			return htmlHeaderList;
		case "Financeiro Cadastros Centro de Custo":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkRegCostCenter.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkRegCostCenter");
			htmlBodyList = htmlBodyList.replace("{2}", "costCenter");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			return htmlBodyList;
		case "Financeiro Cadastros Natureza de Gasto":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkRegNatureExpense.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkRegNatureExpense");
			htmlBodyList = htmlBodyList.replace("{2}", "natureExpense");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			htmlBodyList += '</ul>';
			return htmlBodyList;
		case "Financeiro Cadastros Card":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkCard.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkCard");
			htmlBodyList = htmlBodyList.replace("{2}", "card");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			return htmlBodyList;
		case "Dashboard":
			htmlHeaderList = htmlHeaderList.replace("{0}", "dashboards");
			htmlHeaderList = htmlHeaderList.replace("{1}", "fa fa-dashboard fa-lg");
			htmlHeaderList = htmlHeaderList.replace("{2}", obj.label);
			htmlHeaderList = htmlHeaderList.replace("{3}", "dashboards");
			return htmlHeaderList;
		case "Dashboard Linha do tempo":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkTimeline.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkTimeline");
			htmlBodyList = htmlBodyList.replace("{2}", "ds/timeline");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			return htmlBodyList;
		case "Dashboard Rastro":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkTrace.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkTrace");
			htmlBodyList = htmlBodyList.replace("{2}", "ds/trace");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			return htmlBodyList;
		case "Dashboard Operacional":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkServiceChart.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkServiceChart");
			htmlBodyList = htmlBodyList.replace("{2}", "ds/serviceChart");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			return htmlBodyList;
		case "Dashboard Apontamento":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkAppointmentsChart.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkAppointmentsChart");
			htmlBodyList = htmlBodyList.replace("{2}", "ds/appointmentsChart");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			return htmlBodyList;
		case "Dashboard Ofensores":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkOffendersChart.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkOffendersChart");
			htmlBodyList = htmlBodyList.replace("{2}", "ds/offendersChart");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			return htmlBodyList;
			case "Dashboard Inventario":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkinventorysChart.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkinventorysChart");
			htmlBodyList = htmlBodyList.replace("{2}", "ds/inventorysChart");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			return htmlBodyList;
		case "Inventario":
			htmlHeaderList = htmlHeaderList.replace("{0}", "inventario");
			htmlHeaderList = htmlHeaderList.replace("{1}", "fa fa-list-ol fa-lg");
			htmlHeaderList = htmlHeaderList.replace("{2}", obj.label);
			htmlHeaderList = htmlHeaderList.replace("{3}", "inventario");
			return htmlHeaderList;
		case "Inventario Cadastros":
			htmlHeaderList = htmlHeaderList.replace("{0}", "inventoryRegistrations");
			htmlHeaderList = htmlHeaderList.replace("{1}", "fa fa-pencil-square-o fa-lg");
			htmlHeaderList = htmlHeaderList.replace("{2}", obj.label);
			htmlHeaderList = htmlHeaderList.replace("sub-menu", "sub-menu1");
			htmlHeaderList = htmlHeaderList.replace("{3}", "inventoryRegistrations");
			return htmlHeaderList;
		case "Inventario Cadastros Tipo do Objeto":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkObjectType.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkObjectType");
			htmlBodyList = htmlBodyList.replace("{2}", "objectType");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			return htmlBodyList;
		case "Inventario Cadastros Objeto":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkObject.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkObject");
			htmlBodyList = htmlBodyList.replace("{2}", "object");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			return htmlBodyList;
		case "Inventario Cadastros Estoque":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkStorage.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkStorage");
			htmlBodyList = htmlBodyList.replace("{2}", "storage");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			htmlBodyList += '</ul>';
			return htmlBodyList;
		case "Inventario Relatórios":
            htmlHeaderList = htmlHeaderList.replace("{0}", "reportsInventory");
            htmlHeaderList = htmlHeaderList.replace("{1}", "fa fa-clone fa-lg");
            htmlHeaderList = htmlHeaderList.replace("{2}", obj.label);
            htmlHeaderList = htmlHeaderList.replace("sub-menu", "sub-menu1");
            htmlHeaderList = htmlHeaderList.replace("{3}", "reportsInventory");
            return htmlHeaderList;
		case "Inventario Relatórios Movimentações":
            htmlBodyList = htmlBodyList.replace("{0}", "lnkRepInv.click()");
            htmlBodyList = htmlBodyList.replace("{1}", "lnkRepInv");
            htmlBodyList = htmlBodyList.replace("{2}", "inventorySummary");
            htmlBodyList = htmlBodyList.replace("{3}", obj.label);
            htmlBodyList += '</ul>';
            return htmlBodyList;
		case "Super User Menu":
			htmlHeaderList = htmlHeaderList.replace("{0}", "SuperUserMenu");
			htmlHeaderList = htmlHeaderList.replace("{1}", "fa fa-user-secret fa-lg");
			htmlHeaderList = htmlHeaderList.replace("{2}", obj.label);
			htmlHeaderList = htmlHeaderList.replace("{3}", "SuperUserMenu");
			return htmlHeaderList;
		case "Table Parameters":
			htmlBodyList = htmlBodyList.replace("{0}", "lnktableParameters.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnktableParameters");
			htmlBodyList = htmlBodyList.replace("{2}", "tableParameters");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			return htmlBodyList;
		case "Tables For Imports":
			htmlBodyList = htmlBodyList.replace("{0}", "lnktablesForImports.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnktablesForImports");
			htmlBodyList = htmlBodyList.replace("{2}", "tableForImport");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			htmlBodyList += '</ul>'
			return htmlBodyList;
		case "Operacional Import":
			htmlBodyList = htmlBodyList.replace("{0}", "lnkImports.click()");
			htmlBodyList = htmlBodyList.replace("{1}", "lnkImports");
			htmlBodyList = htmlBodyList.replace("{2}", "importXls");
			htmlBodyList = htmlBodyList.replace("{3}", obj.label);
			return htmlBodyList;
			
		default:
			break;
	}
}
