var numImagens = 0;
var numDivImagens = 0;
var numImagensChecklist = 0;
var numDivChecklist = 0;
var dictI = {};
var dictC = {};
var isEquipment = false;
var uploadNewImageSrc;
var dataUrl_;
var nameImageForEdit;
var idImageForEdit = 0;
var idSettlementForBack = 0;
var imageSettlement;
var imageService;
var idServiceForBack = 0;
var idImageForEditService;
var idxImgService = 0;
var idxImg = 0;
var jaCarregou = false;
var jaCarregouSettlement = false;
var base64ImageForEditing = "";
var base64ImageForEditingService = "";
var listService;
var nameImageForEditService = "";

function carregamento(div) {
	if (div == null) return;

	var str = "<div class=\"modal fade\" id=\"modal_carregando\" data-backdrop=\"static\">" +
		"<img src=\"apiGIMB/view/editImage/loading.gif\" style=\"width: 70px; height: 70px\"/>" +
		"</div>";
	div.innerHTML = str;
}

function moveImageTo(moveTo, elementImage, listImages) {
	if (moveTo === 'next') {
		idxImg++;
		if (idxImg == listImages.length) idxImg = 0;
	} else {
		idxImg--;
		if (idxImg < 0) idxImg = listImages.length - 1
	}

	var r = 'rotate(0deg)';

	elementImage.css({
		'-moz-transform': r,
		'-webkit-transform': r,
		'-o-transform': r,
		'-ms-transform': r
	});

	elementImage["0"].style.marginTop = "0px";
	elementImage.attr('src', listImages[idxImg].picPath);
}

function moveImageToService(moveTo, elementImage) {
	if (moveTo === 'next') {
		idxImgService++;
		if (idxImgService == listService.length) idxImgService = 0;
	} else {
		idxImgService--;
		if (idxImgService < 0) idxImgService = listService.length - 1;
	}

	var r = 'rotate(0deg)';

	elementImage.css({
		'-moz-transform': r,
		'-webkit-transform': r,
		'-o-transform': r,
		'-ms-transform': r
	});

	elementImage["0"].style.marginTop = "0px";

	if (listService[idxImgService].picMime == 'mp4' || listService[idxImgService].picMime == 'MP4') {
		$("#imageDetailService").attr("style", 'display: none');
		$("#videoDetailService").attr("src", listService[idxImgService].picPath);
		$("#imageDetailService").attr("src", '');
		$("#videoDetailService").attr("style", 'align: center; width: 100%; height: 100%; max-height: 500px; display: visiblity; outline: none;');
	}
	else {
		$("#videoDetailService").attr("style", 'display: none');
		$("#imageDetailService").attr("style", 'display: visiblity; transform: rotate(0deg);');
		$("#imageDetailService").attr("src", listService[idxImgService].picPath);
		$("#videoDetailService").attr("src", '');
	}

	nameImageForEditService = listService[idxImgService].picName
}

function deleteImage(source, lista, config, http, scope, modal) {
	var idx = 0;

	for (var i = 0; i < lista.length; i++) {
		if (source == lista[i].picPath) {
			idx = i;
			break;
		}
	}

	var image_id = lista[idx].custompictureId;

	http({
		method: 'DELETE',
		url: config.baseUrl + "/admin/deleteImage/" + image_id
	}).then(function (response) {
		lista.splice(idx, 1);

		modal.modal('hide');

	}, function (response) {
		alert(scope.getMessage('ErrorDeleteImg'))
	});
}

function deleteImageService(sourceImage, lista, config, http, modal, sourceVideo, videos, isChecklist) {
	var source;
	var nome;
	var isVideo = false;
	var idx = null;
	var isfix = false;
	var image_id = 0;
	var idxListService = 0;

	if (sourceImage != '' && sourceImage != null)
		source = sourceImage
	else {
		source = sourceVideo
		isVideo = true;
	}

	if (!isVideo) {
		for (var i = 0; i < lista.length; i++) {
			if (source == lista[i].picPath) {
				idx = i;
				nome = lista[idx].picName;
				image_id = lista[idx].picId;

				if (lista[i].picMime == 'gif' || lista[i].picMime == 'GIF')
					isVideo = true;
				break;
			}
		}

		if (idx == null) {
			for (var i = 0; i < lista.length; i++) {
				if (source == lista[i].fixPicPath) {
					idx = i;
					isfix = true;
					nome = lista[idx].fixPicName;
					image_id = lista[idx].picId;
					break;
				}
			}
		}
	}

	if (isVideo && videos != null && videos.length > 0) {
		for (var i = 0; i < videos.length; i++) {
			if (source == videos[i].picPath) {
				idx = i;
				nome = videos[idx].picName;
				image_id = videos[idx].picId;
				break;
			}
		}
	}

	else if (isVideo) {
		for (var i = 0; i < lista.length; i++) {
			if (source == lista[i].picPath) {
				idx = i;
				nome = lista[idx].picName;
				image_id = lista[idx].picId;
			}
		}
	}

	if (listService.length > 0 && listService != null) {
		for (var i = 0; i < listService.length; i++) {
			if (source == listService[i].picPath) {
				idxListService = i;
				break;
			}
		}
	}

	http({
		method: 'DELETE',
		url: config.baseUrl + "/deleteServicesImage/" + image_id,
		data: nome
	}).then(function (response) {

		if (isfix)
			location.reload();
		else if (isVideo && videos != null && videos.length > 0)
			videos.splice(idx, 1);

		else if (isChecklist) {
			listService.splice(idxListService, 1);

			if (lista[idx].fixPicPath != null)
				listService.splice(idxListService, 1);

			lista.splice(idx, 1);
		}

		else
			lista.splice(idx, 1);

		modal.modal('hide');


	}, function (response) {
		alert($scope.getMessage('ErrorDeleteImg'));
	});
}

function readURL(input, imageDetail, imageDetail_src, http, listaImagens, config, confirmUpdateImg) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			originalSrc = imageDetail_src;
			newSrc = e.target.result;
			imageDetail.attr('src', newSrc);
			updateImg(originalSrc, newSrc, http, listaImagens, config, confirmUpdateImg);
		}

		reader.readAsDataURL(input.files[0]);
	}
}

function toDataURL(src, callback, outputFormat) {
	var img = new Image();
	img.crossOrigin = 'Anonymous';
	img.onload = function () {
		var canvas = document.createElement('CANVAS');
		var ctx = canvas.getContext('2d');
		var dataURL;
		canvas.height = this.naturalHeight;
		canvas.width = this.naturalWidth;
		ctx.drawImage(this, 0, 0);
		dataURL = canvas.toDataURL(outputFormat);
		callback(dataURL);
	};

	img.src = src;
}

function updateImg(originalSrc, newSrc, http, listaImagens, config, confirmUpdateImg) {
	callback = function (dataUrl) {
		dataUrl = dataUrl.substr(dataUrl.indexOf('base64,') + 7, dataUrl.length);

		for (img of listaImagens) {
			if (originalSrc == img.picPath) {
				dataUrl_ = dataUrl;
				confirmUpdateImg.modal('show');
				break;
			}
		}
	}
	toDataURL(newSrc, callback);
}

function trocaImg(picId, config, http, scope, newImage, $q) {
	var defer = $q.defer();

	if (newImage != null) {
		dataUrl_ = newImage;
		dataUrl_ = dataUrl_.substr(dataUrl_.indexOf('base64,') + 7, dataUrl_.length);
	}

	http({
		method: 'PUT',
		url: config.baseUrl + "/admin/uploadCustomImage/" + picId,
		data: dataUrl_
	}).then(function (response) {
		dataUrl_ = "";

		defer.resolve(response.data);

	}, function (response) {
		alert(scope.getMessage('ErrorChangeImg'));
	});
	idImageForEdit = 0;

	return defer.promise;
}

function trocaImgService(newImage, http, scope, config, $q) {
	var defer = $q.defer();
	newImage = newImage.substr(newImage.indexOf('base64,') + 7, newImage.length);

	http({
		method: 'PUT',
		url: config.baseUrl + "/uploadServiceImage/" + nameImageForEditService,
		data: newImage
	}).then(function (response) {
		dataUrl_ = "";

		defer.resolve(response.data);

	}, function (response) {
		alert(scope.getMessage('ErrorChangeImg'))
	});
	idImageForEditService = 0;

	return defer.promise;
}

function abreModalNovaImg(img, modalNovaImg, files) {
	if (files && files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			uploadNewImageSrc = e.target.result;
			img.attr('src', uploadNewImageSrc);
			modalNovaImg.modal('show');
		}
		reader.readAsDataURL(files[0]);
	}
}

function addPicture(titulo, typeid, config, http, type, scope, imageService, listaImagensPrintLiq, modal_carregando) {

	modal_carregando.modal('show');

	callback = function (dataUrl) {
		dataUrl = dataUrl.substr(dataUrl.indexOf('base64,') + 7, dataUrl.length);

		serviceImage = {};
		serviceImage.pic = dataUrl;
		serviceImage.latPic = 0;
		serviceImage.lgtPic = 0;
		serviceImage.message = titulo.value;
		serviceImage.manually = null;
		serviceImage.webUpload = null;
		serviceImage.statusChecklist = null;
		serviceImage.statusFix = null;

		referenceType = {};
		referenceType.id = typeid;

		bodyData = {};
		bodyData.referenceType = referenceType;
		bodyData.serviceImage = serviceImage;

		http({
			method: 'PUT',
			url: config.baseUrl + "/admin/addCustomImage/" + type,
			data: bodyData
		}).then(function (response) {

			listaImagensPrintLiq.push(response.data);

			uploadNewImageSrc = "";

			titulo.value = '';

			modal_carregando.modal('hide');

		}, function (response) {
			alert(scope.getMessage('ErrorInsertImg'))
		});
	}
	toDataURL(uploadNewImageSrc, callback);
}

function rotateImage(graus, scope, el, div, modalContent) {
	var st = el.attr('style');
	var r = '';

	if (typeof st === 'undefined') {
		r = 'rotate(' + graus + 'deg)';
		el.addClass('imgDetailRotation');
		div.addClass('divModalDDRotation');
	} else {
		var r = 'deg'
		var rotation = '';

		for (i = 0; i < st.length; i++) {
			var cmp = st.substring(i - 3, i)
			if ((typeof cmp != 'undefined' && cmp != null && cmp != '') && cmp == r) {
				rotation = st.substring(i - 7, i - 3);
				if (rotation.includes('-'))
					rotation = '-' + rotation.replace(/[^\d]+/g, '');
				else
					rotation = rotation.replace(/[^\d]+/g, '');
				break;
			}
		}

		rotation = parseInt(rotation);

		if (typeof rotation !== 'undefined') graus = parseInt(graus) + rotation;
		else graus = parseInt(graus);

		var largura = el["0"].width;
		var altura = el["0"].height;

		if (graus % 180 != 0) {
			var dif = parseInt((largura - altura) / 2);
			el["0"].style.marginTop = "" + dif + "px";
		} else el["0"].style.marginTop = "0px";

		r = 'rotate(' + graus + 'deg)';
	}

	el.css({
		'-moz-transform': r,
		'-webkit-transform': r,
		'-o-transform': r,
		'-ms-transform': r
	});
}

function downloadImage(urlDownload, scope, http, $q) {
	var defer = $q.defer();
	base64ImageForEditing = "";
	nameImageForEdit = "";
	nameImageForEdit = imageSettlement.picName;
	idImageForEdit = imageSettlement.custompictureId;
	http({
		method: 'Get',
		url: urlDownload + "/admin/downloadImage/" + imageSettlement.picName,
		transformResponse: [
			function (data) {
				base64ImageForEditing = data;
			}
		]
	}).then(function (response) {
		defer.resolve(response.data);
	}, function (response) {
		alert(scope.getMessage('ErrorLoadData'))
	});
	return defer.promise;
}

function downloadImageService(urlDownload, scope, http, $q) {
	var defer = $q.defer();
	idImageForEditService = imageService.picId;

	http({
		method: 'Post',
		url: urlDownload + "/downloadImageService/" + nameImageForEditService,
		transformResponse: [
			function (data) {
				base64ImageForEditingService = data;
			}]

	}).then(function (response) {
		defer.resolve(response.data);
	}, function (response) {
		alert(scope.getMessage('ErrorLoadData'))
	});
	return defer.promise;
}

function prepareImageLists(object, config, values) {
	if (object.equipmentId != null) {
		isEquipment = true;
	}

	if (object.images != null && object.images != "" && object.images.length > 0) {
		var imgs;

		if (testJSON(object.images)) {
			var j = 1;
			imgs = JSON.parse(object.images);
			for (const image of imgs) {
				image.id = "txtImg" + j++;
				dictI[image.id] = image;

				addDivImage(config, values);
			}
		}
		// Esse item deve ser revisado pois deverá sumir com o tempo.
		else {
			if (isEquipment) {
				imgs = object.images.split(";");
			}
			else {
				imgs = object.images.split(",");
			}

			for (var i = 1; i <= imgs.length; i++) {
				var dataImg = imgs[i - 1].split(":");
				var obj = {};
				obj.id = "txtImg" + i;
				obj.VALUE = dataImg[0];
				obj.CHKEXP = dataImg[1] != null ? dataImg[1] : "0";

				dictI[obj.id] = obj;
				addDivImage(config, values);
			}
		}
	}
}

function prepareChecklistsLists(object, config, values) {
	if (object.checklist != null && object.checklist != "" && object.checklist.length > 0) {
		var check;

		if (testJSON(object.checklist)) {
			console.log('testJSON');
			var j = 1;
			checks = JSON.parse(object.checklist);
			for (const check of checks) {
				check.id = "txtCheck" + j++;
				dictC[check.id] = check;
				
				addDivChecklist(config, values);
			}
		}
		// Esse item deve ser revisado pois deverá sumir com o tempo.
		else {
			console.log('teste teste');
			var checklist = object.checklist.split(";");
			for (var i = 1; i <= checklist.length; i++) {
				if (checklist[i - 1] == null || checklist[i - 1] == '')
					continue;

				var obj = {};
				obj.id = "txtCheck" + i;
				obj.VALUE = checklist[i - 1];

				dictC[obj.id] = obj;				
				addDivChecklist(config, values);
			}
		}
	}
}

function strImages() {
	var imagesList = [];
	for (const id in dictI) {
		var img = dictI[id];

		if (img != null && img.VALUE != null && img.VALUE != "") {
			delete img.id;

			if (img.CHKEXP == null) img.CHKEXP = "0";
			if (img.CHKOBGT == null) img.CHKOBGT = "1";
			if (img.CHKGAL == null) img.CHKGAL = "0";

			imagesList.push(img);
		}
	}

	return JSON.stringify(imagesList);
}

function strChecklist() {
	var checklistList = [];
	for (const id in dictC) {
		var check = dictC[id];

		if (check != null && check.VALUE != null && check.VALUE != "") {
			delete check.id;

			if (check.CHKEXP == null) check.CHKEXP = "0";
			if (check.CHKOBGT == null) check.CHKOBGT = "1";
			if (check.CHKGAL == null) check.CHKGAL = "0";

			checklistList.push(check);
		}
	}

	return JSON.stringify(checklistList);
}

function addDivImage(config, values) {
	function clickDelEvent() {
		var div = event.target.parentElement.parentElement;
		deleteDivImage(div, "txtImg" + event.target.id.substr(6, event.target.id.length));
	}

	function keyUpEvent() {
		var edt = event.target;
		var obj = {};

		if (dictI[edt.id] != null) obj = dictI[edt.id];

		obj.id = edt.id;
		obj.VALUE = edt.value;
		dictI[obj.id] = obj;
	}

	function changeEvent() {
		var chk = event.target;
		var edt;
		var obj = {};

		if (chk.id.startsWith("chkExp")) {
			edt = document.getElementById(chk.id.replace("chkExp", "txtImg"));
			if (dictI[edt.id] != null) obj = dictI[edt.id];
			obj.CHKEXP = chk.checked == true ? "1" : "0";
		}

		if (chk.id.startsWith("chkObgt")) {
			edt = document.getElementById(chk.id.replace("chkObgt", "txtImg"));
			if (dictI[edt.id] != null) obj = dictI[edt.id];
			obj.CHKOBGT = chk.checked == true ? "1" : "0";
		}

		if (chk.id.startsWith("chkGal")) {
			edt = document.getElementById(chk.id.replace("chkGal", "txtImg"));
			if (dictI[edt.id] != null) obj = dictI[edt.id];
			obj.CHKGAL = chk.checked == true ? "1" : "0";
		}

		obj.id = edt.id;
		obj.VALUE = edt.value;

		dictI[obj.id] = obj;
	}

	if (numImagens <= values.MAX_DIV_IMAGES) {
		var divId = "divImg" + numDivImagens;
		var txtImgId = "txtImg" + numDivImagens;
		var chkExpId = "chkExp" + numDivImagens;
		var chkObgtId = "chkObgt" + numDivImagens;
		var chkGalId = "chkGal" + numDivImagens;
		var btnDelId = "btnDel" + numDivImagens;

		var begin = ' ' +
			'<div ';
		if (!isEquipment) {
			begin += 'class=\"row\"';
		}
		begin += ' id=\"' + divId + '\"> ' +
			'	 <div class=\"col-sm-3\" style=\"height: 34px;\"> ' +
			'		 <input id=\"' + txtImgId + '\" type=\"text\" class=\"formGroupInput form-control\"' +
			'			 placeholder=\"' + getLabel('Images.p') + '\" autocomplete=\"off\" style=\"max-width: 200px\" /> ' +
			'	 </div> ';
		if (config.expNoteDeb) {
			var expNota = ' <div class=\"col-sm-2\" style=\"height: 34px;\">' +
				'	   <label>' + getLabel('EXPNOTEDEB') + ' </label> ' +
				'	   <input class=\"form-check-input\" type=\"checkbox\" id=\"' + chkExpId + '\"> ' +
				'	</div> ';
			begin += expNota;
		}
		if (config.required) {
			var obgt = ' <div class=\"col-sm-2\" style=\"height: 34px;\">' +
				'	   <label>' + getLabel('Required') + ' </label> ' +
				'	   <input class=\"form-check-input\" type=\"checkbox\" checked id=\"' + chkObgtId + '\"> ' +
				'	</div> ';
			begin += obgt;
		}
		if (config.gallery) {
			var gal = ' <div class=\"col-sm-3\" style=\"height: 34px;\">' +
				'	   <label>' + getLabel('SelectGallery') + ' </label> ' +
				'	   <input class=\"form-check-input\" type=\"checkbox\" id=\"' + chkGalId + '\"> ' +
				'	</div> ';
			begin += gal;
		}
		var end = '   <div class=\"col-sm-1\" style=\"height: 34px; padding-left: 0px\">' +
			'       <button id=\"' + btnDelId + '\" class=\"formGroupInput btn-sm btn-danger\" tabIndex=\"-1\">-</button> ' +
			' 	</div>' +
			'</div>';
		begin += end;
		document.getElementById("pnlListImagens").innerHTML += begin;

		for (var i = 1; i <= numDivImagens; i++) {
			btnDelId = "btnDel" + i;
			var btn = document.getElementById(btnDelId);
			if (btn != null) {
				btn.addEventListener("click", clickDelEvent);
			}

			txtImgId = "txtImg" + i;
			chkExpId = "chkExp" + i;
			chkObgtId = "chkObgt" + i;
			chkGalId = "chkGal" + i;

			var edt = document.getElementById(txtImgId);
			var chkExp = document.getElementById(chkExpId);
			var chkObgt = document.getElementById(chkObgtId);
			var chkGal = document.getElementById(chkGalId);

			if (edt != null) {
				var obj = dictI[edt.id];
				if (obj != null) {
					edt.value = obj.VALUE;
					if (config.expNoteDeb)
						chkExp.checked = (obj.CHKEXP == "1");

					if (config.required)
						chkObgt.checked = (obj.CHKOBGT == "1");

					if (config.gallery)
						chkGal.checked = (obj.CHKGAL == "1");
				}

				edt.addEventListener("keyup", keyUpEvent);

				if (config.expNoteDeb)
					chkExp.addEventListener("change", changeEvent);

				if (config.required)
					chkObgt.addEventListener("change", changeEvent);

				if (config.gallery)
					chkGal.addEventListener("change", changeEvent);
			}
		}

		numImagens++;
		numDivImagens++;
	}
}

function deleteDivImage(div, propDelete) {
	delete dictI[propDelete];
	document.getElementById("pnlListImagens").removeChild(div);
}

function addDivChecklist(config, values) {
	function clickDelEvent() {
		var div = event.target.parentElement.parentElement;
		deleteDivChecklist(div, "txt" + event.target.id.substr(6, event.target.id.length));
	}

	function keyUpEvent() {
		var edt = event.target;
		var obj = {};

		if (dictC[edt.id] != null) obj = dictC[edt.id];

		obj.id = edt.id;
		obj.VALUE = edt.value;
		dictC[obj.id] = obj;
	}

	if (numImagensChecklist <= values.MAX_DIV_CHECKLISTS) {
		var divId = "divCheck" + numDivChecklist;
		var txtCheckId = "txtCheck" + numDivChecklist;
		var btnDelId = "btnDelCheck" + numDivChecklist;

		var begin = ' ' +
			' <div id=\"' + divId + '\"> ' +
			'	    <div class=\"col-sm-3\" style=\"height: 34px;\"> ' +
			'			<input id=\"' + txtCheckId + '\" type=\"text\" class=\"formGroupInput form-control\"' +
			' 			placeholder=\"' + getLabel('Images.p') + '\" autocomplete=\"off\" style=\"max-width: 200px\" /> ' +
			'		</div> ';
		if (config.expNoteDeb) {
			var expNota = ' <div class=\"col-sm-2\" style=\"height: 34px;\">' +
				'	   <label>' + getLabel('EXPNOTEDEB') + ' </label> ' +
				'	   <input class=\"form-check-input\" type=\"checkbox\" id=\"' + chkExpId + '\"> ' +
				'	</div> ';
			begin += expNota;
		}
		if (config.required) {
			var obgt = ' <div class=\"col-sm-2\" style=\"height: 34px;\">' +
				'	   <label>' + getLabel('Required') + ' </label> ' +
				'	   <input class=\"form-check-input\" type=\"checkbox\" checked id=\"' + chkObgtId + '\"> ' +
				'	</div> ';
			begin += obgt;
		}
		if (config.gallery) {
			var gal = ' <div class=\"col-sm-3\" style=\"height: 34px;\">' +
				'	   <label>' + getLabel('SelectGallery') + ' </label> ' +
				'	   <input class=\"form-check-input\" type=\"checkbox\" id=\"' + chkGalId + '\"> ' +
				'	</div> ';
			begin += gal;
		}
		var end = '		<div class=\"col-sm-1\" style=\"height: 34px; padding-left: 0px\"> ' +
			'			<button id=\"' + btnDelId + '\" class=\"btn-sm btn-danger formGroupInput\" tabIndex=\"-1\">-</button> ' +
			'		</div> ' +
			'	</div>';
		begin += end;
		document.getElementById("pnlListChecklist").innerHTML += begin;

		for (var i = 1; i <= numDivChecklist; i++) {
			btnDelId = "btnDelCheck" + i;
			var btn = document.getElementById(btnDelId);
			if (btn != null) {
				btn.addEventListener("click", clickDelEvent);
			}

			txtImgId = "txtCheck" + i;
			var edt = document.getElementById(txtImgId);
			if (edt != null) {
				var obj = dictC[edt.id];
				if (obj != null)
					edt.value = obj.VALUE;

				edt.addEventListener("keyup", keyUpEvent);
			}
		}

		numImagensChecklist++;
		numDivChecklist++;
	}
}

function deleteDivChecklist(div, propDelete) {
	delete dictC[propDelete];
	document.getElementById("pnlListChecklist").removeChild(div);
}

function setImageDetail(imgPath, index, list) {
	var el = $("#imageDetailService");
	var div = $("#divModalDD");
	listService = list;

	//$(".imgDetail").css('background-image', 'url(' + imgPath + ')');

	idxImgService = index;
	if (list[index].picMime == "mp4" || list[index].picMime == "MP4") {
		$("#imageDetailService").attr("style", 'display: none');
		$("#videoDetailService").attr("src", imgPath);
		$("#imageDetailService").attr("src", null);
		$("#videoDetailService").attr("style", 'align: center; width: 100%; height: 100%; max-height: 500px; display: visiblity;  outline: none;');
	}
	else {
		$("#videoDetailService").attr("style", 'display: none');
		$("#imageDetailService").attr("style", 'display: visiblity');
		$("#imageDetailService").attr("src", imgPath);
		$("#videoDetailService").attr("src", null);
	}



	var r = 'rotate(0deg)';

	$(el).css({
		'-moz-transform': r,
		'-webkit-transform': r,
		'-o-transform': r,
		'-ms-transform': r
	});

	$(el).removeClass('imgDetailRotation');
	$(div).removeClass('divModalDDRotation');

	nameImageForEditService = list[index].picName;
}

function setImageDetailSettlement(obj, index, list) {
	var el = $("#imageDetailSettlement");
	var div = $("#divModalDD");
	var imgPath = obj;
	$(".imageDetailSettlement").css('background-image', 'url(' + imgPath + ')');
	$("#imageDetailSettlement").attr("src", imgPath);

	for (var i = 0; i < list.length; i++) {
		if (list[i].picPath == obj) {
			idxImg = i;
			break;
		}
	}

	var r = 'rotate(0deg)';

	$(el).css({
		'-moz-transform': r,
		'-webkit-transform': r,
		'-o-transform': r,
		'-ms-transform': r
	});

	$(el).removeClass('imgDetailRotation');
	$(div).removeClass('divModalDDRotation');
	
	nomeImage = list[index].picName;
}

function setImageDetailDocument(path, index) {
	var el = $("#imageDetailDocument");
	var div = $("#divModalDD");
	$(".imageDetailDocument").css('background-image', 'url(' + path + ')');
	$("#imageDetailDocument").attr("src", path);
	
	idxImg = index;
	
	var r = 'rotate(0deg)';

	$(el).css({
		'-moz-transform': r,
		'-webkit-transform': r,
		'-o-transform': r,
		'-ms-transform': r
	});

	$(el).removeClass('imgDetailRotation');
	$(div).removeClass('divModalDDRotation');
}