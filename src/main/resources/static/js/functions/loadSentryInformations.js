function loadSentryInformations() {
  url = window.location.href;
  urlBase = url.substr(0, url.indexOf('apiGIMB'));
  urlBase += 'apiGIMB/';

  $http({
    method: 'GET',
    url: urlBase + '/configurations/sentryDSN',
    headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') }
  }).then((response) => {
    data = response.data;
    localStorage['sentryDSN'] = data['DSN']
    localStorage['ENV'] = data['ENV']
  }, (response) => {
    console.error(response);
  });
}