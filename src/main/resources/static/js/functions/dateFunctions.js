var dateFlatPickrOptions = {
	dateFormat: 'd/m/Y'
};

var timeFlatPickrOptions = {
	noCalendar: true,
	enableTime: true,
	dateFormat: 'd/m/Y H:i',
	time_24hr: true
};

function dateNowDDMMYYYY(){
	date = new Date()
	
    day  = pad(date.getDate().toString(), 2);
    month  = pad((date.getMonth()+1).toString(), 2) //+1 pois no getMonth Janeiro começa com zero.
    year = date.getFullYear();
    
    return day+"/"+month+"/"+year;
}

function timestampNowDDMMYYYY(){
	date = new Date()
	
    day  = pad(date.getDate().toString(), 2);
    month  = pad((date.getMonth()+1).toString(), 2) //+1 pois no getMonth Janeiro começa com zero.
    year = date.getFullYear();
    
    hour = pad(date.getHours(), 2);
    min = pad(date.getMinutes(), 2);
    sec = pad(date.getSeconds(), 2);
    
    return day+'/'+month+'/'+year+' '+hour+':'+min+':'+sec;
}

function dateToString(date) {
	day  = pad(date.getDate().toString(), 2);
    month  = pad((date.getMonth()+1).toString(), 2); //+1 pois no getMonth Janeiro começa com zero.
    year = date.getFullYear();
    
    return day+"/"+month+"/"+year;
}

function lastUtilDay() {
	date = new Date();
	if (date.getDay() == 0) 
		date = dateAdd(-2, date);
	else if (date.getDay() == 6) 
		date = dateAdd(-1, date);
	
	return dateToString(date);
}

function dateAdd(days, date) {
	date = date || new Date();
	
	return new Date(date.getTime() + (days * (24*60*60*1000)));
}

function getDateFromDateTimeString(date) {
	try {
		return date.substring(0, 10);
	} catch (e) {
		
	}
	return '';
}

function getTimeFromDateTimeString(date) {
	try {
		return date.substring(11, 16);
	} catch (e) {
		
	}
	return '';
}

function getTimeElapsedMilli(startTime, endTime) {
	
	let startMillis = new Date(moment(startTime, 'DD/MM/YYYY HH:mm:ss').format()).getTime();
	let endMillis = new Date(moment(endTime, 'DD/MM/YYYY HH:mm:ss').format()).getTime();
	
	let millis = (endMillis - startMillis);
	
	return millis <= 0 ? 0 : millis;
}

function getTimeElapsed(startTime, endTime) {
	let millis = getTimeElapsedMilli(startTime, endTime);
		
	let seconds = Math.floor((millis / 1000) % 60);
	let minutes = Math.floor(((millis / (1000*60)) % 60));
	let hours   = Math.floor(((millis / (1000*60*60)) % 24));
	let days    = Math.floor(((millis / (1000*60*60*24)) % 365));
	
	let strTime = 'Tempo: ' +  
		(hours.toString().length > 1 ? hours : '0'+ hours) + 
		':' + (minutes.toString().length > 1 ? minutes : minutes + '0') + 
		':' + (seconds.toString().length > 1 ? seconds : seconds + '0');
	if (days >= 1)
		strTime = 'Dia(s): ' + (days.toString().length > 1 ? days : '0'+ days) + 
		' Tempo: ' + (hours.toString().length > 1 ? hours : '0'+ hours) +
		':' + (minutes.toString().length > 1 ? minutes : minutes + '0') + 
		':' + (seconds.toString().length > 1 ? seconds : seconds + '0');
	
	return strTime;
}