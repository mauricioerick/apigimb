// If absolute URL from the remote server is provided, configure the CORS
// header on that server.
var url = 'http://localhost:8080/PDF/teste.pdf';

// Loaded via <script> tag, create shortcut to access PDF.js exports.
var pdfjsLib = require(['../js/pdfjs/pdf.js']); //window['../js/pdfjs/pdf'];

// The workerSrc property shall be specified.
pdfjsLib.GlobalWorkerOptions.workerSrc = '../js/pdfjs/pdf.worker.js';

// Asynchronous download of PDF
var loadingTask = pdfjsLib.getDocument(url);
loadingTask.promise.then(function (pdf) {

  // Fetch the first page
  var pageNumber = 1;
  pdf.getPage(pageNumber).then(function (page) {
    var scale = 1.5;
    var viewport = page.getViewport(scale);

    // Prepare canvas using PDF page dimensions
    var canvas = document.getElementById('cvPrint');
    var context = canvas.getContext('2d');
    canvas.height = viewport.height;
    canvas.width = viewport.width;

    // Render PDF page into canvas context
    var renderContext = {
      canvasContext: context,
      viewport: viewport
    };
    var renderTask = page.render(renderContext);
    renderTask.then(function () {
    });
  });
}, function (reason) {
  // PDF loading error
  console.error(reason);
});