/* Timeline */
var startDateTM = '';
var endDateTM = '';
var userSelected = '';
var chartsData = {};

function getStartDateTM() {
	return startDateTM;
}

function setStartDateTM(date) {
	startDateTM = date;
}

function getEndDateTM() {
	return endDateTM;
}

function setEndDateTM(date) {
	endDateTM = date;
}

function getUserSelected() {
	return userSelected;
}

function setUserSelected(user) {
	userSelected = user;
}

/* Trace */
var startDateTR = '';
var endDateTR = '';

function getStartDateTR() {
	return startDateTR;
}

function setStartDateTR(date) {
	startDateTR = date;
}

function getEndDateTR() {
	return endDateTR;
}

function setEndDateTR(date) {
	endDateTR = date;
}

function clearChartsData() {
	chartsData = {};
}

function setChartData(key, data) {
	chartsData[key] = data;
}

function configChartsEvents(document) {
	cArray = document.body.getElementsByTagName('canvas');
	for (canvas of cArray) {
		if (canvas.id.includes('modal')) break;
		canvas.addEventListener('click', function (event) {
			data = chartsData[event.target.id];

			document.getElementById('tittleContainer').innerHTML = '<h4 class="modal-title" align="center">' + data.tittle + '</h4>';

			canvasContainer = document.getElementById("canvasContainer");
			canvasContainer.innerHTML = '<canvas id="modalChartClone"></canvas>';
			canvas = canvasContainer.getElementsByTagName("canvas")[0];

			chart = new Chart(canvas.getContext('2d'), {
				type: data.type,
				options: {
					plugins: {
						datalabels: {
							color: (data.type == 'pie') ? 'white' : 'black',
							display: function (context) {
								return true;
							},
							font: {
								weight: 'bold'
							},
							formatter: function (value, context) {
								var lb = formatterHourGraphLabel(value, context);
								if (data.type != 'line')
									lb = formatterHourGraphLabel(value, context) + '\n' + percChart(context.dataset, context.datasetIndex, context.dataIndex);

								return lb;
							},
							clip: true,
							tittle: false
						}
					},
					tooltips: {
						custom: function (tooltip) {
							//tooltip.x = 0;
							//tooltip.y = 0;
						},
						mode: 'single',
						callbacks: {
							label: function (tooltipItems, data) {
								var l = formatterHourGraphLabel(data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index], chart.controller);
								return l;
							},
							beforeLabel: function (tooltipItems, data) {
								return percChart(data.datasets, tooltipItems.datasetIndex, tooltipItems.index);
							}
						}
					}
				}
			});

			chart.labelFormatter = data.labelFormatter;
			chart.data = data.data;
			chart.options.legend.display = false;
			if (data.type != 'bar')
				chart.options.legend.display = true;

			if (data.type != 'pie') {
				try {
					chart.options.scales.xAxes[0].display = true;
					chart.options.scales.xAxes[0].ticks.autoSkip = false;
					chart.options.scales.yAxes[0].ticks.beginAtZero = true;
					chart.options.scales.xAxes[0].ticks.major.autoSkip = false;
					chart.options.scales.xAxes[0].ticks.minor.autoSkip = false;
				}
				catch (err) {
					// Do nothing
				}
			}
			chart.update()

			$('#modalChart').modal()
		});

	}
}

function getHTMLCharCanvas(id, tittle, size) {
	if (tittle == null) tittle = "";

	return '' +
		'<div class="col-xs-' + size + '" style="padding: 5px !important; "> ' +
		'   <div class="col-xs-12" style="background-color: white; box-shadow: 1px 1px 3px 0px !important"> ' +
		'	   <h5 ng-hide="' + tittle + '==' + '\'\'' + '" align="center" style="overflow: hidden; white-space: nowrap;">' + tittle + '</h5> ' +
		'	   <canvas class="col-xs-12" id="chart' + id + '"></canvas> ' +
		'   </div> ' +
		'</div> ';
}

function formatterHourGraphLabel(value, context) {
	if (context.chart.labelFormatter == 'HOUR') {
		hour = parseInt(value);
		minute = Math.round(Math.abs(parseInt(value) - value) * 100);
		minute = Math.round(60 * (minute / 100));
		return hour + ':' + pad(minute, 2);
	}
	else
		return value;
}

function getRandomColor() {
	var letters = '0123456789ABCDEF';
	var color = '#';

	for (var i = 0; i < 6; i++)
		color += letters[Math.floor(Math.random() * 16)];

	return color;
}

function hexToRgbA(hex, alpha) {
	var c;
	if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
		c = hex.substring(1).split('');
		if (c.length == 3) {
			c = [c[0], c[0], c[1], c[1], c[2], c[2]];
		}
		c = '0x' + c.join('');
		return 'rgba(' + [(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',') + ', ' + alpha + ')';
	}
	throw new Error('Bad Hex');
}

function colorSelectorConf(colorSelId, defaulColor) {
	$('#' + colorSelId).ColorPicker({
		color: defaulColor,
		onShow: function (colpkr) {
			$(colpkr).fadeIn(500);
			return false;
		},
		onHide: function (colpkr) {
			$(colpkr).fadeOut(500);
			return false;
		},
		onChange: function (hsb, hex, rgb) {
			hex = '#' + hex;

			colorId = document.getElementById(colorSelId);
			colorId.style.background = hex;
			colorId.value = hex;
			colorId.dispatchEvent(new Event('change'));
		}
	});
}

function percChart(data, datasetIndex, index) {

	if (data instanceof Array) {
		sum = data[datasetIndex].data.reduce(add, 0);
		value = data[datasetIndex].data[index];
	} else {
		sum = data.data.reduce(add, 0);
		value = data.data[index];
	}

	function add(a, b) {
		return a + b;
	}

	return Math.round((value / sum * 100)) + '%';
}