function checkCanLoad() {
	if (localStorage.closeTime == null || parseInt(localStorage.closeTime) == 0)
		return true;

	var diff = (((Date.now() - parseInt(localStorage.closeTime)) / 1000) / 60);
	var result = diff <= 3;

	if (result)
		localStorage.removeItem('closeTime');
	else {
		localStorage.removeItem('bobby');
		localStorage.removeItem('company');
		localStorage.removeItem('permissionAccess');
		localStorage.removeItem('token');
	}

	return result;
}

function controllerHeadCheck(scope, location, juiceService, redirectService) {

	if (localStorage.getItem("bobby") && JSON.parse(localStorage.getItem("bobby")).superUser) {
		return true;
	}

	checkCanLoad();

	redirectService = redirectService || true;

	var result = true;

	scope.appUrl = "/";
	if (location.path().indexOf("apiGIMB") > -1) {
		scope.appUrl += "apiGIMB/";
	}

	if (!localStorage.getItem("token")) {
		location.path(juiceService + 'login');
		result = false;
	}

	if (!hasPermission() || (redirectService && localStorage.getItem("permissionAccess") == "false")) {
		location.path(juiceService.appUrl + 'home');
		result = false;
	}

	return result;
}

function hasPermission() {
	var webList = localStorage.getItem("bobby") && JSON.parse(JSON.parse(localStorage.getItem("bobby")).profile.webAccess);
	let name = location.pathname.replace('/apiGIMB/', '');
	var liberado = false;

	if (name == 'login' || name == 'home')
		liberado = true;
	else {
		for (var idx in webList) {
			html = webList[idx].html;

			if (html.includes('{{appUrl}}')) {
				str = html.substr(html.indexOf('href'), html.length - 1).replace('href="{{appUrl}}', '');
				lastIndex = str.indexOf('"');

				str = str.substr(0, lastIndex);

				if (str.length > 0 && name.includes(str)) {
					liberado = true;
					break;
				}
			}
		}
	}

	if (!liberado) {
		const edtImg = location.pathname.indexOf("/apiGIMB/editImage/") != -1;
		const edtImgService = location.pathname.indexOf("/apiGIMB/editImageService/") != -1;

		if (edtImg || edtImgService) {
			const user = JSON.parse(localStorage.bobby);
			const config_web = (user.profile != null && user.profile.configWeb != null && user.profile.configWeb.length > 0) ? JSON.parse(user.profile.configWeb) : [];

			const canEditImg = config_web["EDITPICSETTLEMENTE"] == '1';
			const canEditImgService = config_web["EDITPICSERVICE"] == '1';

			if ((edtImg && canEditImg) || (edtImgService && canEditImgService))
				liberado = true;
		}
	}

	return liberado;
}

function drawMenu(scope, compile) {
	if (localStorage.bobby != null) {
		let divMenuContent = "";
		let $el;

		let user = JSON.parse(localStorage.bobby);
		let listWeb = (user.profile != null && user.profile.webAccess != null && user.profile.webAccess.length > 0) ? JSON.parse(user.profile.webAccess) : [];

		if (listWeb.length > 0) {
			for (var idx in listWeb) {
				var obj = listWeb[idx]
				if (obj.id == "-1") continue;

				divMenuContent += obj.html
			}

			if (user.superUser) {
				divMenuContent += getHtmlSuperUser();
			}

			$('#listMenu').empty();
			$el = $('<ul id="menu-content" class="menu-content collapse out">'
				+ divMenuContent

				+ '<li class="ng-scope" >'
				+ '<a id="lnkHelper" class="ng-binding ng-scope" ng-controller="homeController" href="https://gimbhelp.freshdesk.com/support/home" target="_blank">'
				+ '<i class="glyphicon glyphicon-question-sign" aria-hidden="true"></i> ' + getLabel('getHelp')
				+ '</a>'
				+ '</li>'

				+ '<li class="ng-scope" style="padding: 15px">'
				+ '<a id="lnkClose" class="ng-binding ng-scope" ng-controller="homeController" href="#" ng-click="sair()">'
				+ '<i class="fa fa-sign-out fa-lg" aria-hidden="true"></i> ' + getLabel('Exit')
				+ '</a>'
				+ '</li>'
				+ '</ul>').appendTo('#listMenu');
		} else {
			$('#listMenu').empty();
			$el = $('<ul id="menu-content" class="menu-content collapse out">'

				+ '<li class="ng-scope" >'
				+ '<a id="lnkHelper" class="ng-binding ng-scope" ng-controller="homeController" href="https://gimbhelp.freshdesk.com/support/home" target="_blank">'
				+ '<i class="glyphicon glyphicon-question-sign" aria-hidden="true"></i> ' + getLabel('getHelp')
				+ '</a>'
				+ '</li>'

				+ '<li style="padding: 15px">'
				+ '<a id="lnkClose" href="#" ng-click="sair()">'
				+ '<i class="fa fa-sign-out fa-lg" aria-hidden="true"></i> ' + getLabel('Exit')
				+ '</a>'
				+ '</li>'
				+ '</ul>').appendTo('#listMenu');
		}
		compile($el)(scope);
	}
}
