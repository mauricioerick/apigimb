var numCampos = 0;
var numDivCampos = 0;
var dictF = {};

function addDivField() {
	function clickEvent() {
		var div = event.target.parentElement.parentElement;
		deleteDivField(div, "txtFld" + event.target.id.substr(11,event.target.id.length));
	}

    function keyUpEvent() {
        var edt = event.target;
        var sel = document.getElementById(edt.id.replace("txt", "sel"));
        var obj = {};

        if (dictF[edt.id] != null) obj = dictF[edt.id];

        obj.id = edt.id;
        obj.VALUE = edt.value;
        obj.TYPE = getDataTypeFrom(sel.value);

        var divFldValue = document.getElementById(sel.id.replace("selFld","divFldValue"));
        divFldValue.style.display = "none";
        if (obj.TYPE == "LST") {
			divFldValue.style.display = "block";
		} else if (obj.TYPE == "CHL") {
			divFldValue.style.display = "block";
		} else {
			txtValue.value = "";
		}

        dictF[obj.id] = obj;
    }

    function keyUpValueEvent() {
        var edt = event.target;
        var mainEdt = document.getElementById(edt.id.replace("txtFldValue", "txtFld"));
        var sel = document.getElementById(edt.id.replace("txtFldValue", "selFld"));
        var obj = {};

        if (dictF[mainEdt.id] != null) obj = dictF[mainEdt.id];

        obj.id = mainEdt.id;
        obj.DEFAULTVALUES = edt.value;
        obj.TYPE = getDataTypeFrom(sel.value);

        var divFldValue = document.getElementById(sel.id.replace("selFld","divFldValue"));
        divFldValue.style.display = "none";
        if (obj.TYPE == "LST") {
			divFldValue.style.display = "block";
		} else if (obj.TYPE == "CHL") {
			divFldValue.style.display = "block";
		} else {
			txtValue.value = "";
		}

        dictF[obj.id] = obj;
    }

    function changeEvent() {
        var sel = event.target;
		var edt = document.getElementById(sel.id.replace("sel", "txt"));
	    var obj = {};
        
        if (dictF[edt.id] != null) obj = dictF[edt.id];
        
        obj.id = edt.id;
		obj.VALUE = edt.value;
		obj.TYPE = getDataTypeFrom(sel.value);

        var divFldValue = document.getElementById(sel.id.replace("selFld", "divFldValue"));
        var txtValue = document.getElementById(sel.id.replace("selFld", "txtFldValue"));
        
		divFldValue.style.display = "none";
		
		if (obj.TYPE == "LST") {
			divFldValue.style.display = "block";
			txtValue.placeholder = getLabel('ListValues');
		} else if (obj.TYPE == "CHL") {
			divFldValue.style.display = "block";
			txtValue.placeholder = getLabel('CheckListValues');
		} else if (txtValue != null && txtValue.value.length > 0){
			txtValue.value = "";
		}

		dictF[obj.id] = obj;
    }

    if (numCampos <= 10) {
        var txtFldId = "txtFld" + numDivCampos;
        var selFldId = "selFld" + numDivCampos;
        var divFldValueId = "divFldValue" + numDivCampos;
        var divFldCheckValueId = "divFldCheckValue" + numDivCampos;  
        var txtFldValueId = "txtFldValue" + numDivCampos;
        var btnDelId = "btnDelField" + numDivCampos;
        var divFldCheckValue = "divFldCheckValue" + numDivCampos;
        var txtFldValueCheckId = "txtFldValueCheck" + numDivCampos;

        var str = '<div class=\"row\">'+
                  '	   <div class=\"col-sm-3\">'+
				  '		   <input id=\"' + txtFldId + '\" type=\"text\" class=\"formGroupInput form-control\"' + 
				  '			   placeholder=\"' + getLabel('FieldName') + '\" autocomplete=\"off\"' + 
				  '			   style=\"max-width: 200px\" />' +
                  '	   </div> ' +
                  '	   <div class=\"col-sm-2\" style=\"max-width: 250px\"> ' +
                  '		   <select id=\"' + selFldId + '\"  class=\"formGroupInput form-control text-box single-line\" autocomplete=\"off\"> ' +
                  '			   <option> ' + getLabel('Text') + ' </option> ' +
				  '			   <option> ' + getLabel('Number') + ' </option> ' +
                  '			   <option> ' + getLabel('Monetary') + ' </option> ' +
				  '			   <option> ' + getLabel('Date') + ' </option> ' +
                  '			   <option> ' + getLabel('Scanner') + ' </option> ' +
				  '			   <option> ' + getLabel('List') + ' </option> ' +
                  '			   <option> ' + getLabel('CheckList') + ' </option> ' +
                  '			   <option> ' + getLabel('Link') + ' </option> ' +
                  '		   </select> ' +
                  '	   </div> ' +
				  '	   <div id=\"' + divFldValueId + '\" class=\"col-sm-4\" style=\"max-width: 400px; display: none\" > ' +
				  '		   <input id=\"' + txtFldValueId + '\" type=\"text\" class=\"formGroupInput form-control\"' + 
				  '			   placeholder=\"' + getLabel('ListValues') + '\" autocomplete=\"off\"' + 
				  '			   style=\"max-width: 400px\" /> ' +
                  '	   </div> ' +
                  '	   <div id=\"' + divFldValueId + '\" class=\"col-sm-4\" style=\"max-width: 400px; display: none\" > ' +
				  '		   <input id=\"' + txtFldValueId + '\" type=\"text\" class=\"formGroupInput form-control\"' + 
				  '			   placeholder=\"' + getLabel('ListValues') + '\" autocomplete=\"off\"' + 
				  '			   style=\"max-width: 400px\" /> ' +
                  '	   </div> ' +
                  '	   <div class=\"col-sm-1\" style=\"height: 34px; padding-left: 0px\"> ' +
                  '		   <button id=\"' + btnDelId + '\" class=\"formGroupInput btn-sm btn-danger\" tabIndex=\"-1\">-</button> ' +
				  '	   </div> ' +
				  '</div>';
        document.getElementById("pnlListFields").innerHTML += str;

        for (let i = 1; i <= numDivCampos; i++) {
            btnDelId = "btnDelField" + i;

            var btn = document.getElementById(btnDelId);
            if (btn != null) {
                btn.addEventListener('click', clickEvent);
            }

            txtFldId = "txtFld" + i;
            selFldId = "selFld" + i;
            txtFldValueId = "txtFldValue" + i;
            var edt = document.getElementById(txtFldId);
            var sel = document.getElementById(selFldId);
            var txtValue = document.getElementById(txtFldValueId);
            var txtValueCheck = document.getElementById(txtFldValueCheckId);

            if (edt != null) {
                edt.addEventListener('keyup', keyUpEvent);

                sel.addEventListener('change', changeEvent);
                
                txtValue.addEventListener('keyup', keyUpValueEvent);
                
                var obj = dictF[edt.id];
                if (obj != null) {
                    edt.value = obj.VALUE;
                    sel.value = getDataTypeDescFrom(obj.TYPE);
                    sel.dispatchEvent(new Event('change'));

                    if (obj.DEFAULTVALUES != null && obj.DEFAULTVALUES.length > 0){
                    	txtValue.value = obj.DEFAULTVALUES;
                    }
                }
            }
        }

        numCampos++;
        numDivCampos++;
    }
}

function deleteDivField(div, propDelete) {
    delete dictF[propDelete];
    document.getElementById("pnlListFields").removeChild(div);
}

function strFields() {
    var fieldsList = [];
    for (const id in dictF) {
        var fld = dictF[id];

        if (fld != null && fld.VALUE != null && fld.VALUE != "") {
            delete fld.id;
            fieldsList.push(fld);
        }
    }
    return JSON.stringify(fieldsList);
}

function prepareFieldLists(object) {
    console.log('prepareFieldLists: ', object);
    if (object.fields != null && object.fields != "" && object.fields.length > 0) {
        var flds;
        if (testJSON(object.fields)) {
            var j = 1;
            flds = JSON.parse(object.fields);
            for (const field of flds) {
                field.id = "txtFld" + j++;
                dictF[field.id] = field;
                
                addDivField();
            }
        }
        //Esse item deve ser revisado pois deverá sumir com o tempo. 
        else {
            flds = object.fields.split(",");

            for (var i = 1; i <= flds.length; i++) {
                var data = flds[i - 1].split(":");
                var obj = {};
    
                obj.id = "txtFld" + i;
                obj.VALUE = data[0];
                obj.TYPE = data[1];
                obj.DEFAULTVALUES = "";
    
                if (data[1] == "LST")
                    obj.DEFAULTVALUES = data[2];
                else if(data[1] == 'CHL')
                	obj.DEFAULTVALUES = data[2];
                	
                dictF[obj.id] = obj;
                
                addDivField();
            }
        }
    }
}