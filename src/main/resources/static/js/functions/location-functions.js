let $http = angular.injector(["ng"]).get("$http");

var userId, selectedLocation
var listLocations = []

mapDailyTrace = {}
infowindow = new google.maps.InfoWindow();

markerIconUrl = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|'
markerIcon = {
		url: '',
		scaledSize: new google.maps.Size(18, 27)
	}
dataMarkers = [];

function loadDataEventsMapMarkers() {
	filterByUser = (obj) => {
		return obj.user.userId == userId;
	}
	services = [];
	transfers = [];
	settlements = [];
	
	url = window.location.href;
	urlBase = url.substr(0, url.indexOf('apiGIMB'));
	urlBase += 'apiGIMB/';
		
	inputDate = document.getElementById("startDate").value
	date = inputDate.length == 0 ? dateToString(new Date()) : inputDate;

	headers = {'Authorization': 'Bearer ' + localStorage.getItem('token')}

	// get services data to filter by user and show markers in map
	params = {'startDate': date, 'endDate': date, 'user': {'role': 'U'}};
	$http({
		method: 'PUT',
		url: urlBase + "servicoWeb",
		data: params,
		headers: headers
	}).then((response) => {
		if (Array.isArray(response.data)) {
			for (service of response.data) {
				if (service.user.userId == userId && service.latStartTime != 0 && service.lgtStartTime != 0) {
					text = '<b>OS</b> <br />' + service.action.description + '<br />' + getDateFromDateTimeString(service.startTime) + '<br />' + getTimeFromDateTimeString(service.startTime) + ' - ' + getTimeFromDateTimeString(service.endTime)
					setDataMarker({'lat': service.latStartTime, 'lng': service.lgtStartTime}, text, service.action.colorId);
				}
			}
		}
	}), (response) => { 

	}		

	// get transfers data to filter by user and show markers in map
	params = {'startDate': date, 'endDate': date};
	$http({
		method: 'PUT',
		url: urlBase + "transfersByDateBetween",
		data: params,
		headers: headers
	}).then((response) => {
		if (Array.isArray(response.data)) {
			for (transfer of response.data) {
				if (transfer.user.userId == userId) {
					for (tEvent of transfer.eventsList) {
						if (tEvent.latitude != 0 && tEvent.longitude != 0) {
							text = '<b>EVENTO</b> <br />' + tEvent.event.description + '<br />' + getDateFromDateTimeString(tEvent.startTime) + '<br />' + getTimeFromDateTimeString(tEvent.startTime) + ' - '+ getTimeFromDateTimeString(tEvent.endTime)
							setDataMarker({'lat': tEvent.latitude, 'lng': tEvent.longitude}, text, tEvent.event.colorId);
						}
					}
				}
			}
		}
	}), (response) => { 

	}	

	// get settlement data to filter by user and show markers in map
	params = {'startDate': date, 'endDate': date};
	$http({
		method: 'PUT',
		url: urlBase + "settlementWeb",
		data: params,
		headers: headers
	}).then((response) => {
		if (Array.isArray(response.data)) {
			for (settlement of response.data) {
				if (settlement.user.userId == userId && settlement.latitude != 0 && settlement.longitude != 0)  {
					text = '<b>LIQUIDAÇÃO</b> <br />' + settlement.event.description + '<br />' + getDateFromDateTimeString(settlement.startTime) + '<br />' + getTimeFromDateTimeString(settlement.startTime) 
					setDataMarker({'lat': settlement.latitude, 'lng': settlement.longitude}, text, settlement.event.colorId);
				}
			}
		}
	}), (response) => { 

	}
}

function setDataMarker(loc, text, colorId) {
	if (colorId == null || colorId.length == 0) colorId = getRandomColor();
	markerIcon.url = markerIconUrl + colorId.replace('#', '');
	
	marker = new MarkerWithLabel({
		map: mapDailyTrace,
		animation: google.maps.Animation.DROP,
		position: loc,
		icon: markerIcon,
		labelContent: "",
		labelClass: "",
		labelInBackground: false
	  });
	
	i = dataMarkers.length;
	google.maps.event.addListener(marker, 'click', (function(marker, i) {
	    return function() {
	      infowindow.setContent(text);
	      infowindow.open(mapDailyTrace, marker);
	    }
	  })(marker, i));
	
	dataMarkers.push(marker);
}

function loadTracesByUserAndDate() {
	url = window.location.href;
	urlBase = url.substr(0, url.indexOf('apiGIMB'));
	urlBase += 'apiGIMB';

	iDate = document.getElementById("startDate").value;
	date = iDate.length == 0 ? dateToString(new Date()) : iDate;

	$http({
		method: 'PUT',
		url: urlBase + '/findTracesByDate',
		data: {'date': date, 'id': userId.toString()},
		headers: {'Authorization': 'Bearer ' + localStorage.getItem('token')}
	}).then((response) => {
		getLocations(response.data)
	}, (response) => {
		// do nothing
	});
}

function showModalWindow(i) {
    selectedLocation = listLocations[i];
	userId = listLocations[i].userId;
	
	for (marker of dataMarkers)
		marker.setMap(null);
    
	$("#myModal").modal();
    $("input#startDate").val(listLocations[i].traceStartTime.substr(0,10));
	
    loadTracesByUserAndDate();
}

function getLocations(traces){
	var mapCoord = getTracers(traces)
    mapDailyTrace = mapCoord[0]
	mapDailyTrace.setCenter({lat:selectedLocation.latitude, lng:selectedLocation.longitude})
	var coordinatesByUser = mapCoord[1] 
	displayCoordinatesOnMap(coordinatesByUser);
}

function displayCoordinatesOnMap(coordinatesByUser) {	
    var values = Object.values(coordinatesByUser);
		
    var bounds = new google.maps.LatLngBounds();
    for (var i = 0; i < values.length; i++) {
        coordinates = values[i];
    	for (var j = 0; j < coordinates.length; j++)
    		bounds.extend(new google.maps.LatLng(coordinates[j].lat, coordinates[j].lng));
    }
		
	for (var i = 0; i < values.length; i++) {
		var polyline = new google.maps.Polyline({
		    path: values[i],
		    geodesic: true,
		    strokeColor: '#FF0000',
		    strokeOpacity: 1.0,
		    strokeWeight: 3
		  });
		polyline.setMap(mapDailyTrace);
    }
	
    setTimeout(function() {
        map.fitBounds(bounds)
    }, 200);
    
    loadDataEventsMapMarkers();
}