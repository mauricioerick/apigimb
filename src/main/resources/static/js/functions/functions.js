var dataInicioFiltroOS = '';
var dataFinalFiltroOS = '';
var textoFiltroOS = '';
var clientsOS = [];
var activitysOS = [];
var usersOS = [];
var projectOS = [];

var dataInicioFiltroLiq = '';
var dataFinalFiltroLiq = '';
var	clientLiq = [];
var	userLiq = [];
var	eventLiq = [];
var	typePaymentLiq = [];

var dataInicioFiltroCardExtract = '';
var dataFinalFiltroCardExtract = '';
var bannerCardExtract = [];
var emittingBankCardExtract = [];

var dataInicioFiltroCard = '';
var dataFinalFiltroCard = '';
var bannerCard = [];
var emittingBankCard = [];

var usersPointRecord = [];
var clientsPointRecord = [];
var situationPointRecord = '';
var dataInitialPointRecord = ''; 
var	dataFinalPointRecord = '';

var	startDateExpense = '';
var	endDateExpense = '';
var	clientsExpense = [];
var sequenceExpense = [];

var textActivity = '';
var optionActivity = '';

var textEvent = '';
var optionEvent = '';

var textEquipment = ''; 
var optionEquipment = '';

var textTypePayment = '';
var optionTypePayment = '';

var textObservation = '';
var optionObservation = '';

var textClient = '';
var optionClient = '';

var textUser = '';
var optionUser = '';

var textProfile = '';
var optionProfile = '';

var textArea = '';
var optionArea = '';

var textDocument = '';

var optionCostCenter = '';
var textCostCenter = '';

var textNatureExpense = '';
var optionNatureExpense = '';

var textObjectType = '';
var optionObjectType = '';

var textStorage = '';
var optionStorage = '';

var textObject = '';
var optionObject = '';

const monthNames = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
const monthNamesShort = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'];
var datePickerOptions = {
	dateFormat: 'dd/mm/yy',
	dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado', 'Domingo'],
	dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
	dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
	monthNames: monthNames,
	monthNamesShort: monthNamesShort,
	yearRange: '1900:+0',
	changeMonth: true,
	changeYear: true
};

function getMonthInFull(date) {
	const month = date.slice(3, 5);

	switch (month) {
		case '01':
			return monthNames[0];
		case '02':
			return monthNames[1];
		case '03':
			return monthNames[2];
		case '04':
			return monthNames[3];
		case '05':
			return monthNames[4];
		case '06':
			return monthNames[5];
		case '07':
			return monthNames[6];
		case '08':
			return monthNames[7];
		case '09':
			return monthNames[8];
		case '10':
			return monthNames[9];
		case '11':
			return monthNames[10];
		case '12':
			return monthNames[11];
		default:
			break;
	}
}

function getMonthAbbreviatedInLength(date) {
	const month = date.slice(3, 5);

	switch (month) {
		case '01':
			return monthNamesShort[0];
		case '02':
			return monthNamesShort[1];
		case '03':
			return monthNamesShort[2];
		case '04':
			return monthNamesShort[3];
		case '05':
			return monthNamesShort[4];
		case '06':
			return monthNamesShort[5];
		case '07':
			return monthNamesShort[6];
		case '08':
			return monthNamesShort[7];
		case '09':
			return monthNamesShort[8];
		case '10':
			return monthNamesShort[9];
		case '11':
			return monthNamesShort[10];
		case '12':
			return monthNamesShort[11];
		default:
			break;
	}
}

function getDataInicioFiltroOS() {
	return dataInicioFiltroOS;
}

function setDataInicioFiltroOS(data) {
	dataInicioFiltroOS = data;
}

function getDataFinalFiltroOS() {
	return dataFinalFiltroOS;
}

function setDataFinalFiltroOS(data) {
	dataFinalFiltroOS = data;
}

function getTextoFiltroOS() {
	return textoFiltroOS;
}

function setTextoFiltroOS(texto) {
	textoFiltroOS = texto;
}

function setProjectOS(data){
	projectOS = data;
}

function getProjectOS(){
	return projectOS;
}

function getDataInicioFiltroLiq() {
	return dataInicioFiltroLiq;
}

function setDataInicioFiltroLiq(data) {
	dataInicioFiltroLiq = data;
}

function getDataFinalFiltroLiq() {
	return dataFinalFiltroLiq;
}

function setDataFinalFiltroLiq(data) {
	dataFinalFiltroLiq = data;
}

function getDataInicioFiltroCardExtract() {
	return dataInicioFiltroCardExtract;
}

function setDataInicioFiltroCardExtract(data) {
	dataInicioFiltroCardExtract = data;
}

function getDataFinalFiltroCardExtract() {
	return dataFinalFiltroCardExtract;
}

function setDataFinalFiltroCardExtract(data) {
	dataFinalFiltroCardExtract = data;
}

function getBannerCardExtract() {
	return bannerCardExtract;
}

function getEmittingBankCardExtract() {
	return emittingBankCardExtract;
}

function setBannerCardExtract(param) {
	bannerCardExtract = param;
}

function setEmittingBankCardExtract(param) {
	emittingBankCardExtract = param;
}

function setClientsOS(params) {
	clientsOS = params;
}

function setActivitysOS(params) {
	activitysOS = params;
}

function setUsersOS(params) {
	usersOS = params;
}

function getClientsOS() {
	return clientsOS;
}

function getActivitysOS() {
	return activitysOS;
}

function getUsersOS() {
	return usersOS;
}

function getUsersPointRecord() {
	return usersPointRecord;
}

function setUsersPointRecord(param) {
	usersPointRecord = param;
}

function getClientsPointRecord() {
	return clientsPointRecord;
}

function setClientsPointRecord(param) {
	clientsPointRecord = param;
}

function setSituationPointRecord(param) {
	situationPointRecord = param;
}

function getSituationPointRecord() {
	return situationPointRecord;
}

function getDataFinalPointRecord(){
	return dataFinalPointRecord;
}

function  getDataInitialPointRecord(){
	return dataInitialPointRecord;
}

function setDataFinalPointRecord(param){
	dataFinalPointRecord = param;
}

function setDataInitialPointRecord(param){
	dataInitialPointRecord = param;
}

function setDataInicioFiltroCard(param){
	dataInicioFiltroCard = param;
}

function setDataFinalFiltroCard(param){
	dataFinalFiltroCard = param;
}

function setBannerCard(param) {
	bannerCard = param;
}

function setEmittingBankCard(param){
	emittingBankCard = param;
}

function getDataInicioFiltroCard(){
	return dataInicioFiltroCard;
}

function getDataFinalFiltroCard(){
	return dataFinalFiltroCard;
}

function getBannerCard() {
	return bannerCard;
}

function getEmittingBankCard(){
	return emittingBankCard;
}

function setClientLiq(params){
	clientLiq = params;
}

function setUserLiq(params){
	userLiq = params;
}

function setEventLiq(params){
	eventLiq = params;
}

function setTypePaymentLiq(params){
	typePaymentLiq = params;
}

function getClientLiq(){
	return clientLiq;
}

function getUserLiq(){
	return userLiq;
}

function getEventLiq(){
	return eventLiq;
}

function getTypePaymentLiq(){
	return typePaymentLiq;
}

function setDataInicioFiltroExpense(param){
	startDateExpense = param;
}

function setDataFinalFiltroExpense(param){
	endDateExpense = param;
}

function setClientExpense(param){
	clientsExpense = param;
}

function getDataInicioFiltroExpense(){
	return startDateExpense;
}

function getDataFinalFiltroExpense(){
	return endDateExpense;
}

function getClientExpense(){
	return clientsExpense;
}

function getSequenceExpense(){
	return sequenceExpense;
}
function setSequenceExpense(param){
	sequenceExpense = param;
}

function setTextActivity(param){
	textActivity = param;
}

function getTextActivity(){
	return textActivity;
}

function setOptionAcitivy(param){
	optionActivity = param;
}

function getOptionAcitivy(){
	return optionActivity;
}

function setTextEvent(param){
	textEvent = param;
}

function getTextEvent(){
	return textEvent;
}

function setOptionEvent(param){
	optionEvent = param;
}

function getOptionEvent(){
	return optionEvent;
}

function setTextEquipment(param){
	textEquipment = param;
}

function setOptionEquipment(param){
	optionEquipment = param;	
}

function getTextEquipment(){
	return textEquipment;
}

function getOptionEquipment(){
	return optionEquipment;	
}

function setTextTypePayment(param) {
	textTypePayment = param;
}

function getTextTypePayment() {
	return textTypePayment;
}

function setOptionTypePayment(param) {
	optionTypePayment = param;
}

function getOptionTypePayment() {
	return optionTypePayment;
}

function setTextObservation(param){
	textObservation = param;
}

function setOptionObservation(param){
	optionObservation = param;
}

function getTextObservation(){
	return textObservation;
}

function getOptionObservation(){
	return optionObservation;
}

function setTextClient(param){
	textClient = param;
}

function setOptionClient(param){
	optionClient = param;
}

function getTextClient(){
	return textClient;
}

function getOptionClient(){
	return optionClient;
}

function getTextUser(){
	return textUser;
}

function getOptionUser(){
	return optionUser;
}

function setTextUser(param){
	textUser = param;
}

function setOptionUser(param){
	optionUser = param;
}

function setTextProfile(param) {
	textProfile = param;
}

function setOptionProfile(param) {
	optionProfile = param;
}

function getTextProfile() {
	return textProfile;
}

function getOptionProfile() {
	return optionProfile;
}

function setTextArea(param){
	textArea = param;
}

function setOptionArea(param){
	optionArea = param;
}

function getTextArea(){
	return textArea;
}

function getOptionArea(){
	return optionArea;
}

function setTextDocument(param){
	textDocument = param;
}

function getTextDocument() {
	return textDocument;
}

function setTextCostCenter(param){
	textCostCenter = param;
}

function setOptionCostCenter(param){
	optionCostCenter = param;
}

function getTextCostCenter(){
	return textCostCenter;
}

function getOptionCostCenter(){
	return optionCostCenter;
}

function setOptionNatureExpense(param){
	optionNatureExpense = param;
}

function setTextNatureExpense(param){
	textNatureExpense = param;
}

function getOptionNatureExpense(){
	return optionNatureExpense;
}

function getTextNatureExpense(){
	return textNatureExpense;
}

function getTextObjectType(){
	return textObjectType;
}

function getOptionObjectType(){
	return optionObjectType;
}

function setTextObjectType(param){
	textObjectType = param;
}

function setOptionObjectType(param){
	optionObjectType = param;
}

function setOptionStorage(param) {
	optionStorage = param;
}

function setTextStorage(param) {
	textStorage = param;
}

function getOptionStorage() {
	return optionStorage;
}

function getTextStorage() {
	return textStorage;
}

function setOptionObject(param){
	optionObject = param;
}

function setTextObject(param){
	textObject = param;
}

function getOptionObject(){
	return optionObject;
}

function getTextObject(){
	return textObject;
}

function resetGlobalVariables() {
	setDataInicioFiltroOS('');
	setDataFinalFiltroOS('');
	setTextoFiltroOS('');
	setClientsOS([]);
	setActivitysOS([]);
	setUsersOS([]);
	setProjectOS([]);

	setDataInicioFiltroLiq('');
	setDataFinalFiltroLiq('');
	setClientLiq([]);
	setUserLiq([]);
	setEventLiq([]);
	setTypePaymentLiq([]);

	setDataInicioFiltroCard('');
	setDataFinalFiltroCard('');
	setBannerCard([]);
	setEmittingBankCard([]);
	
	setDataInicioFiltroCardExtract('');
	setDataFinalFiltroCardExtract('');
	setBannerCardExtract([]);
	setEmittingBankCardExtract([]);
	
	setDataFinalPointRecord('');
	setDataInitialPointRecord('');
	setSituationPointRecord('');
	setClientsPointRecord('');
	setUsersPointRecord('');
	
	setDataInicioFiltroExpense('');
	setDataFinalFiltroExpense('');
	setClientExpense([]);
	
	setTextActivity('');
	setOptionAcitivy('');
	
	setTextEvent('');
	setOptionEvent('');
	
	setTextEquipment('');
	setOptionEquipment('');

	setTextTypePayment('');
	setOptionTypePayment('');
	
	setTextObservation('');
	setOptionObservation('');
	
	setTextClient('');
	setOptionClient('');
	
	setTextUser('');
	setOptionUser('');
	
	setTextProfile('');
	setOptionProfile('');
	
	setTextArea('');
	setOptionArea('');
	
	setTextDocument('');
	
	setTextCostCenter('');
	setOptionCostCenter('');
	
	setTextNatureExpense('');
	setOptionNatureExpense('');
	
	setTextObjectType('');
	setOptionObjectType(''); 
	
	setOptionStorage('');
	setTextStorage('');
	
	setOptionObject('');
	setTextObject('');
}

function setValueBtn(obj) {
	if (obj.active.lenght > 0) {
		if (obj.active == 'Sim') {
			return "Inativar";
		} else if (obj.active == 'Não') {
			return "Ativar";
		} else {
			return "Inativar";
		}
	} else {
		if (obj.situacao == 'ativo') {
			return "Inativar";
		} else if (obj.situacao == "inativo") {
			return "Ativar";
		} else {
			return "Inativar";
		}
	}

}

function configPrint() {
	$("#divGeo").removeClass("print");

	$(".dontPrint").css("display", "none");

	$("html, body, #printa").css("height", "100%");
	$("html").css("overflowY", "scroll");

	setTimeout(retConfig(), 2000);
}

function retConfig() {
	$("html, body, #printa").css("height", "auto");
	$("html").css("overflowY", "auto");

	$(".dontPrint").css("display", "");

	$("#divGeo").addClass("print");
}

function imprimir(printPage) {
	$("#divBody").removeClass("print");

	$(".dontPrint").css("display", "none");

	$("html, body, #divBody").css("height", "100%");
	$("html").css("overflowY", "scroll");

	window.focus();
	window.print();

	$("html, body, #divBody").css("height", "auto");
	$("html").css("overflowY", "auto");

	$(".dontPrint").css("display", "");

	$("#divBody").addClass("print");
	return false;
}

function stringToDate(str) {
	var day = str.substring(0, 2);
	var month = str.substring(3, 5);
	var year = str.substring(6, 10);

	if (day.toString().length == 1) {
		day = "0" + day;
	}

	if (month.toString().length == 1) {
		month = "0" + month;
	}

	return month + '/' + day + '/' + year;
}

function dateToString(date) {
	var dia = date.getDate();
	var mes = date.getMonth() + 1;
	var ano = date.getFullYear();

	if (dia.toString().length == 1) {
		dia = "0" + dia;
	}

	if (mes.toString().length == 1) {
		mes = "0" + mes;
	}

	return dia + '/' + mes + '/' + ano;
}

function dateToStringPeriodo(date, tipo) {

	if (tipo == "i") {
		return moment(date).subtract(6, 'days').format('DD/MM/YYYY');
	} else {
		return moment(date).format('DD/MM/YYYY');
	}
}

function parseDate(input, reverse) {

	if (input.length > 10) {
		input = input.substring(0, 10);
	}

	var parts = reverse ? input.toString().split('-') : input.split('/');

	if (!reverse)
		return new Date(parts[2], parts[1] - 1, parts[0]);
	else
		return new Date(parts[0], parts[1] - 1, parts[2]);
}

function getLastDay(mounth, year) {
	var day;
	switch (parseInt(mounth)) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			day = "31";
			break;
		case 2:
			if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
				day = "29";
			} else {
				day = "28";
			}
			break;
		case 4:
		case 6:
		case 9:
		case 11:
			day = "30";
			break;
		default:
			day = "30";
			break;
	}

	return day
}

function getDataTypeFrom(typeDesc) {
	if (typeDesc == getLabel('Text')) {
		return "TXT"
	} else if (typeDesc == getLabel('Number')) {
		return "NUM"
	} else if (typeDesc == getLabel('Date')) {
		return "DAT"
	} else if (typeDesc == getLabel('List')) {
		return "LST"
	} else if (typeDesc == getLabel('Monetary')) {
		return "MNT"
	} else if (typeDesc == getLabel('Scanner')) {
		return "SCR"
	} else if (typeDesc == getLabel('CheckList')) {
		return "CHL"
	} else if (typeDesc == getLabel('Link')) {
		return "LNK"
	}

}

function getDataTypeDescFrom(type) {
	if (type == "TXT") {
		return getLabel('Text')
	} else if (type == "NUM") {
		return getLabel('Number')
	} else if (type == "DAT") {
		return getLabel('Date')
	} else if (type == "LST") {
		return getLabel('List')
	} else if (type == "MNT") {
		return getLabel('Monetary')
	} else if (type == "SCR") {
		return getLabel('Scanner')
	} else if (type == "CHL") {
		return getLabel('CheckList')
	} else if (type == "LNK") {
		return getLabel('Link')
	}
}

function capitalize(string) {
	return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}

function changeScreenHeightSize(el, percHeight) {
	percHeight = percHeight || 1.6
	$(el).css("max-height", window.screen.height / percHeight);
}

function validateForm() {
	var msg = '';

	iArray = $('*[required]');
	for (var element of iArray) {
		if ((element.nodeName.toLowerCase() == 'input' && element.value.length == 0) ||
			((element.nodeName.toLowerCase() == 'select' && (element.value.includes('undefined') || element.value.includes('?')))) ||
			((element.nodeName.toLowerCase() == 'textarea' && element.value.length == 0))) {

			$('#' + element.id).css({ 'border-width': '1px', 'border-style': 'solid', 'border-color': 'red' });

			for (label of element.labels) {
				if (msg.length > 0) msg += ', ';
				msg += label.textContent;
				break;
			}
		}
	}

	if (msg.length > 0) {
		msg = 'Necessário preencher campos obrigatórios: ' + msg;
		alert(msg);
		return false;
	}

	return true;
}

function testJSON(text) {
	if (typeof text !== "string") {
		return false;
	}
	try {
		JSON.parse(text);
		return true;
	}
	catch (error) {
		return false;
	}
}


function orderObject(vector, key) {
	
	vector.sort( (a, b) => {
		
		if (a[key] == undefined || b[key] == undefined ||
			a[key] == 	   null || b[key] == 	  null 
			) {
				return;
			}
		
		if ( a[key] > b[key] ) {
			return 1;
		}
		if ( a[key] < b[key] ) {
			return -1;
		}
		return 0;
	});
	
	return vector;
}









