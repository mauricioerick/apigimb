
var map;
var infoWindow;
var markersData = [];

function initialize() {
	var inicio = {
		lat : -21.286207,
		lng : -50.341709
	};

	var mapOptions = {
		center : inicio,
		zoom : 10,
		mapTypeId : 'roadmap',
	};

	map = new google.maps.Map(document.getElementById('map'), mapOptions);

	infoWindow = new google.maps.InfoWindow();
	google.maps.event.addListener(map, 'click', function() {
		infoWindow.close();
	});
}

function displayMarkers() {

	var pinColor = "6991FD";
	var pinInicio = "00E64D";
	var pinFinal = "FD7567";
	var pinOcorrencia = "FF8C00";

	var pinImage = new google.maps.MarkerImage(
			"http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor, 
			new google.maps.Size(21, 34),
			new google.maps.Point(0, 0), 
			new google.maps.Point(10, 34));

	var pinImageInicio = new google.maps.MarkerImage(
			"http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinInicio, 
					new google.maps.Size(21, 34),
					new google.maps.Point(0, 0), 
					new google.maps.Point(10, 34));

	var pinImageFinal = new google.maps.MarkerImage(
			"http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinFinal, 
					new google.maps.Size(21, 34),
					new google.maps.Point(0, 0), 
					new google.maps.Point(10, 34));

	var pinImageOcorrencia = new google.maps.MarkerImage(
			"http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinOcorrencia, 
			new google.maps.Size(21, 34),
			new google.maps.Point(0, 0), 
			new google.maps.Point(10, 34));

	var pinShadow = new google.maps.MarkerImage(
			"http://chart.apis.google.com/chart?chst=d_map_pin_shadow",
			new google.maps.Size(40, 37), 
			new google.maps.Point(0, 0),
			new google.maps.Point(12, 35));

	var bounds = new google.maps.LatLngBounds();

	for (var i = 0; i < markersData.length; i++) {
		var title = markersData[i].tipo;
		var pin = "";

		if (title.lastIndexOf("Imagem") > -1) {
			pin = pinImage;
		} else if (title.lastIndexOf("Fim") > -1) {
			pin = pinImageFinal;
		} else if (title.lastIndexOf("Início") > -1 || title.lastIndexOf("Registro") > -1) {
			pin = pinImageInicio;
		} else {
			pin = pinImageOcorrencia;
		}

		var latlng = new google.maps.LatLng(markersData[i].lat, markersData[i].lng);

		createMarker(latlng, title, markersData[i].lat, markersData[i].lng, pin, pinShadow);
		bounds.extend(latlng);
	}

	map.fitBounds(bounds);

	markersData = [];
}

function createMarker(latlng, nome, lat, lng, pin, shadow) {
	var marker = new google.maps.Marker({
		map : map,
		position : new google.maps.LatLng(lat, lng),
		title : nome,
		icon : pin,
		shadow : shadow
	});

	google.maps.event.addListener(marker, 'click', function() {

		var iwContent = '<div id="iw_container">' + 
						'	<div class="iw_title">' + nome + '</div>' + 
						'	<div class="iw_content">' + lat + '<br /> </div> ' + lng + 
						'</div>';

		infoWindow.setContent(iwContent);
		infoWindow.open(map, marker);
	});
}

function dadosMapa(imagens, marcadores) {

	google.maps.event.addDomListener(window, 'load', initialize);

	var mapPoint = {
		lat : marcadores.latStartTime != null ? marcadores.latStartTime : 0,
		lng : marcadores.lgtStartTime != null ? marcadores.lgtStartTime : 0,
		tipo : retornaTipo(marcadores, true)
	};
	if (mapPoint.lat != 0 || mapPoint.lng != 0)
		markersData.push(mapPoint);

	mapPoint = {
		lat : marcadores.latEndTime != null ? marcadores.latEndTime : 0,
		lng : marcadores.lgtEndTime != null ? marcadores.lgtEndTime : 0,
		tipo : retornaTipo(marcadores, false)
	};
	if (mapPoint.lat != 0 || mapPoint.lng != 0)
		markersData.push(mapPoint);

	if(marcadores.picsList != null){
		$(marcadores.picsList).each(
				function(idx, el) {
					var mapPoint = {
						lat : el.latPic != null ? el.latPic : 0,
						lng : el.lgtPic != null ? el.lgtPic : 0,
						tipo : "Imagem" + (el.message != null && el.message.length > 0 ? (": " + el.message) : (""))
					};
					if (mapPoint.lat != 0 || mapPoint.lng != 0)
						markersData.push(mapPoint);
				});
	}

	var user = JSON.parse(localStorage.getItem("bobby"));
	if (user.role != "C") {
		if(marcadores.eventsList != null){
			$(marcadores.eventsList).each(
					function(idx, el) {
						var mapPoint = {
							lat : el.latitude,
							lng : el.longitude,
							tipo : el.event.description
						};
						if (mapPoint.lat != 0 || mapPoint.lng != 0)
							markersData.push(mapPoint);
					});
		}
	}

	displayMarkers();
}

function mapDataCustomPictures(customPictureList, otherPoints) {

	google.maps.event.addDomListener(window, 'load', initialize);
	
	$(customPictureList).each(
			function(idx, customPicture) {

				var mapPoint = {
					lat : customPicture.latitude != null ? customPicture.latitude : 0,
					lng : customPicture.longitude != null ? customPicture.longitude : 0,
					tipo : "Imagem" + (customPicture.message != null && customPicture.message.length > 0 ? (": " + customPicture.message) : (""))
				};
				if (mapPoint.lat != 0 || mapPoint.lng != 0)
					markersData.push(mapPoint);
			});
	
	$(otherPoints).each(
			function(idx, point) {
				if (point.lat != 0 || point.lng != 0)
					markersData.push(point);
			});

	displayMarkers();
}

function getTracers(traces){
	var center = {
			lat : -15.77972,
			lng : -47.92972
		};

		var mapOptions = {
			center : center,
			zoom : 5,
			mapTypeId : 'roadmap',
		};

		var coordinatesByUser = {};
		var users = [];
		$(traces).each(
				function(_idx, trace) {
					var point = {
							lat : trace.latitude,
							lng : trace.longitude
					};
					
					var coordinates = coordinatesByUser[trace.user.user];
					if (coordinatesByUser[trace.user.user] == null) {
						coordinates = [];
						users.push(trace.user.user);
					}
					coordinates.push(point);
					
					coordinatesByUser[trace.user.user] = coordinates;
		});

		map = new google.maps.Map(document.getElementById('map'), mapOptions);

		infoWindow = new google.maps.InfoWindow();
		google.maps.event.addListener(map, 'click', function() {
			infoWindow.close();
		});

		return [map, coordinatesByUser]
}

function retornaTipo(obj, inicio) {
	if (inicio) {
		if (obj['serviceId'] != null)
			return 'Início da OS';
		else if (obj['transferId'] != null)
			return 'Início do Ponto';
		else
			return '';
	} else {
		if (obj['serviceId'] != null)
			return 'Fim da OS';
		else if (obj['transferId'] != null)
			return 'Fim do Ponto';
		else
			return '';
	}
}