/**
 * 
 */
var lbls = {
	"ChooseFromTheCustomerList"	: {
		"pt": "Escolher da lista de clientes",
		"en": "Choose from the customer list",
		"es": "Elegir de la lista de clientes"
	},

	"FillInTheRecipientData" : {
		"pt": "Preencher os dados do destinatário",
		"en": "Fill in the recipient data",
		"es": "Complete los datos del destinatario"
	},
	
	"InformationForGeneratingDebitMemoForThirdParties" : {
		"pt": "Informações para geração de nota de débito para terceiros",
		"en": "Information for generating debit memo for third parties",
		"es": "Información para generar nota de débito para terceros"
	},

	"OptionsForGeneratingDebitMemos" : {
		"pt": "Opções para a geração da nota de débito",
		"en": "Options for generating the debit memo",
		"es": "Opciones para generar la nota de débito"
	},

	"GenerateCustomerNote" : {
		"pt": "Gerar nota para cliente",
		"en": "Generate customer note",
		"es": "Generar nota de cliente"
	},

	"GenerateNoteForThirdParties" : {
		"pt": "Gerar nota para terceiros",
		"en": "Generate note for third parties",
		"es": "Generar nota para terceros"
	},

	"UserOrCard" : {
		"pt": "Usuário ou cartão",
		"en": "User or card",
		"es": "Usuario o tarjeta"
	},
		
	"ExpenseDebit": {
		"pt": "Debitar despesa",
		"en": "Charge expense",
		"es": "Gastos de carga"
	},
	
	"Finish":{
		"pt": "Finalizar",
		"en": "Finish",
		"es": "Terminar"
	},
	
	"changeDate": {
		"pt": "Muda data",
		"en": "Change date",
		"es": "Cambiar fecha"
	},
	
	"Received": {
		"pt": "Recebida",
		"en": "Received",
		"es": "Recibida"
	},

	"Deleted": {
		"pt": "Excluído",
		"en": "Deleted",
		"es": "Excluido"
	},

	"Partially": {
		"pt": "Parcialmente fechada",
		"en": "Partially closed",
		"es": "Parcialmente cerrado"
	},

	"send": {
		"pt": "Enviar",
		"en": "Send",
		"es": "Enviar"
	},

	"import": {
		"pt": "Importar",
		"en": "Import",
		"es": "Importar"
	},

	"importXls": {
		"pt": "Importar Excel",
		"en": "Import Excel",
		"es": "Importar Excel"
	},

	"downloadModel": {
		"pt": "Download do modelo",
		"en": "Download the model",
		"es": "Descargar el modelo"
	},

	"importExcel": {
		"pt": "Importar Excel",
		"en": "Import Excel",
		"es": "Importar Excel"
	},
	"TableImports": {
		"pt": "Tabelas para importação",
		"en": "Tables for import",
		"es": "tablas para importar"
	},

	"requiredCritically": {
		"pt": "Exigir criticidade",
		"en": "Require critically",
		"es": "Requiere criticidade"
	},

	"requirePhoto": {
		"text": {
			"pt": "Exigir foto",
			"en": "Require photo",
			"es": "Requiere foto"
		},

		"FIX": {
			"pt": "Arrumado",
			"en": "Tidy",
			"es": "Aseado"
		},

		"OK": {
			"pt": "OK",
			"en": "OK",
			"es": "OK"
		},

		"NOK": {
			"pt": "Não ok",
			"en": "No ok",
			"es": "No ok"
		}
	},

	"delivered": {
		"pt": "Entregue",
		"en": "Delivered",
		"es": "Entregado"
	},
	"agenda": {
		"pt": "Agenda",
		"en": "Agenda",
		"es": "Agenda"
	},
	"Week": {
		"pt": "Semana",
		"en": "Week",
		"es": "Semana"
	},
	"Month": {
		"pt": "Mês",
		"en": "Moth",
		"es": "Mes"
	},
	"Day": {
		"pt": "Dia",
		"en": "Day",
		"es": "Día"
	},

	"installTargetPerDay": {
		"pt": "Meta de instalações por dia",
		"en": "Install target per day",
		"es": "Instalar objetivo por día"
	},

	"totalInstallations": {
		"pt": "Total de instalações",
		"en": "Total installations",
		"es": "Instalaciones totales"
	},

	"availableDays": {
		"pt": "Dias disponíveis",
		"en": "Available days",
		"es": "Días disponibles"
	},

	"durationType": {
		"pt": "Tipo da duração",
		"en": "Duration type",
		"es": "Tipo de duración"
	},

	"initialDate": {
		"pt": "Data inicial",
		"en": "Initial date",
		"es": "La fecha de inicio"
	},

	"Duration": {
		"pt": "Duração",
		"en": "Duration",
		"es": "Duración"
	},

	"MovesInPeriod": {
		"pt": "Mostrar movimentações no período",
		"en": "Show moves in period",
		"es": "Mostrar movimientos en período"
	},

	"supportChanel": {
		"pt": "Canal de suporte",
		"en": "Support Channel",
		"es": "Canal de soporte"
	},

	"getHelp": {
		"pt": "Obter ajuda",
		"en": "Get help",
		"es": "Obtener ayuda"
	},

	"Helper": {
		"pt": "Ajuda",
		"en": "Help",
		"es": "Ayuda"
	},

	"closingBetween": {
		"pt": "Fechamento entre",
		"en": "Closing between",
		"es": "Cerrando entre"
	},

	"bank": {
		"pt": "Banco",
		"en": "Bank",
		"es": "Banco"
	},

	"InitialDateClosingDay": {
		"pt": "Data inicial do fechamento",
		"en": "Closing start date",
		"es": "Fecha de inicio de cierre"
	},

	"FinalDateClosingDay": {
		"pt": "Data final do fechamento",
		"en": "End date of closing",
		"es": "Fecha final de cierre"
	},

	"ParametersOfTable": {
		"pt": "Parametros da tabela: ",
		"en": "Parameters of table: ",
		"es": "Parámetros de la tabla: "
	},
	"SuperUserMenu": {
		"pt": "Super usuário",
		"en": "Super user",
		"es": "Super usuario"
	},
	"ReferenceTable": {
		"pt": "Tabela de referência",
		"en": "Reference Table",
		"es": "Tabla de Referencia"
	},
	"TableParameters": {
		"pt": "Parâmetros de tabelas",
		"en": "Table parameters",
		"es": "Parámetros de tabla"
	},
	"IdentificationType": {
		"pt": "Tipo de identificador",
		"en": "Identifier Type",
		"es": "Tipo de identificador"
	},
	"Typed": {
		"pt": "Digitado",
		"en": "Typed",
		"es": "Escrita"
	},
	"PrincipalCard": {
		"pt": "Cartão principal",
		"en": "Main card",
		"es": "Tarjeta principal"
	},
	"limit": {
		"pt": "Limite",
		"en": "Limit",
		"es": "Límite"
	},
	"generateDeliveryTerm": {
		"pt": "Gerar termo de entrega",
		"en": "Generate Delivery Term",
		"es": "Generar plazo de entrega"
	},
	"newCard": {
		"pt": "Novo cartão",
		"en": "New card",
		"es": "Nueva tarjeta"
	},

	"mainCard": {
		"pt": "Cartão principal",
		"en": "main card",
		"es": "tarjeta principal"
	},

	"creditRenewalDay": {
		"pt": "Dia da renovação do crédito",
		"en": "Credit renewal day",
		"es": "Día de renovación de crédito"
	},

	"expenseApproval": {
		"pt": "Aprovação de despesa",
		"en": "Expense Approval",
		"es": "Aprobación de gastos"
	},

	"approvedValue": {
		"pt": "Valor aprovado",
		"en": "Approved Value",
		"es": "Valor aprobado"
	},

	"fullValue": {
		"pt": "Valor integral",
		"en": "Full value",
		"es": "Valor total"
	},

	"creditApproval": {
		"pt": "Aprovação de crédito",
		"en": "Credit approval",
		"es": "Aprobación de crédito"
	},

	"automaticallyRenewCredit": {
		"pt": "Renova crédito automaticamente",
		"en": "Automatically renew credit",
		"es": "Renovar crédito automáticamente"
	},

	"blockPostingAfterClosing": {
		"pt": "Bloquear lançamento após fechamento",
		"en": "Block posting after closing",
		"es": "Bloquear publicación después del cierre"
	},

	"expirationDay": {
		"pt": "Dia do vencimento",
		"en": "Expiration day",
		"es": "Día de vencimiento"
	},

	"closingDay": {
		"pt": "Dia do fechamento ",
		"en": "Closing Day",
		"es": "Dia de clausura"
	},

	"closing": {
		"pt": "Fechamento ",
		"en": "Closing",
		"es": "Clausura"
	},

	"cardData": {
		"pt": "Dados do cartão",
		"en": "Card data",
		"es": "Datos de la tarjeta"
	},

	"emittingBank": {
		"pt": "Banco emissor",
		"en": "Emitting bank",
		"es": "Banco emisor"
	},

	"flag": {
		"pt": "Bandeira",
		"en": "Flag",
		"es": "Bandera"
	},

	"cardNumber": {
		"pt": "Número do cartão",
		"en": "Card number",
		"es": "Numero de tarjeta"
	},

	"card": {
		"pt": "Cartão",
		"en": "Card",
		"es": "Tarjeta",
		"p": {
			"pt": "Cartões",
			"en": "Cards",
			"es": "Tarjetas"
		}
	},
	"date": {
		"pt": "Data",
		"en": "Date",
		"es": "Fecha"
	},
	"photoDownload": {
		"pt": "Download de fotos",
		"en": "Photo download",
		"es": "Descarga de fotos"
	},
	"documentInformation": {
		"pt": "Informação do documento",
		"en": "Document information",
		"es": "Información del documento"
	},
	"requester": {
		"pt": "Solicitante",
		"en": "Requester",
		"es": "Solicitante"
	},
	"document": {
		"pt": "Documento",
		"en": "Document",
		"es": "Documento",

		"p": {
			"pt": "Documentos",
			"en": "Documents",
			"es": "Documentos"
		}
	},
	"responsible": {
		"pt": "Responsável",
		"en": "Responsible",
		"es": "Responsable"
	},
	"newUser": {
		"pt": "Novo usuário",
		"en": "New user",
		"es": "Nuevo usuario"
	},

	"to": {
		"pt": "até",
		"en": "to",
		"es": "a"
	},
	"areaData": {
		"pt": "Dados da área",
		"en": "Area data",
		"es": "Datos del área"
	},
	"area": {
		"pt": "Área",
		"en": "Area",
		"es": "Area"
	},

	"accountCode": {
		"pt": "Código da conta",
		"en": "Account Code",
		"es": "Código de cuenta"
	},

	"costCenterData": {
		"pt": "Dados do centro de custo",
		"en": "Cost center data",
		"es": "Datos del centro de costos"
	},

	"natureExpense": {
		"pt": "Natureza do gasto",
		"en": "Nature of expense",
		"es": "Naturaleza del gasto"
	},

	"natureExpenseData": {
		"pt": "Dados de natureza do gasto",
		"en": "Nature of expense data",
		"es": "Dados del Naturaleza del gasto"
	},

	"natureBound": {
		"pt": "Natureza vinculado ao's evento's",
		"en": "Nature bound to it's event's",
		"es": "La naturaleza unida a su evento"
	},

	"costCenter": {
		"pt": "Centro de custo",
		"en": "Cost center",
		"es": "Centro de coste"
	},

	"ErrorLoadingCostCenter": {
		"pt": "Erro ao carregar centro custo",
		"en": "Error loading cost center",
		"es": "Error al cargar el centro de costos"
	},

	"newLogo": {
		"pt": "Novo logo",
		"en": "New logo",
		"es": "nuevo logo"
	},

	"deleteLogo": {
		"pt": "Excluir logo",
		"en": "Delete logo",
		"es": "Eliminar logo"
	},

	"Logo": {
		"pt": "Logo",
		"en": "Logo",
		"es": "Logo"
	},

	"CompanyData": {
		"pt": "Dados da empresa",
		"en": "Company data",
		"es": "Datos de la empresa"
	},

	"Company": {
		"pt": "Empresa",
		"en": "Company",
		"es": "compañia"
	},

	"dontGetSignature": {
		"pt": "Assinatura não recolhida",
		"en": "Signature not collected",
		"es": "Firma no recopilada"
	},

	"Signature": {
		"pt": "Assinatura",
		"en": "signature",
		"es": "Suscripción"
	},
	"Launch": {
		"pt": "Lançamento",
		"en": "Launch",
		"es": "Liberar"
	},

	"Link": {
		"pt": "Link",
		"en": "Link",
		"es": "Link"
	},

	"CustomFields": {
		"pt": "Campos Personalizado",
		"en": "Custom Fields",
		"es": "Campo Personalizado"
	},

	"CheckList": {
		"pt": "CheckList",
		"en": "CheckList",
		"es": "CheckList"
	},

	"Pendente": {
		"pt": "Pendente",
		"en": "Pending",
		"es": "Pendiente"
	},

	"Aprovado": {
		"pt": "Aprovado",
		"en": "Approved",
		"es": "Aprobado"
	},

	"Desaprovado": {
		"pt": "Desaprovado",
		"en": "Disapproved",
		"es": "Desaprobado"
	},

	"Scanner": {
		"pt": "Scanner",
		"en": "Scanner",
		"es": "Scanner"
	},


	"Videos": {
		"pt": "Vídeos",
		"en": "Videos",
		"es": "Vídeos"
	},

	"Office": {
		"pt": "Cargo",
		"en": "Office",
		"es": "oficina"
	},

	"ContactName": {
		"pt": "Nome do contato",
		"en": "Contact name",
		"es": "Nombre de contacto"
	},

	"ContactData": {
		"pt": "Dados do contato",
		"en": "Contact Information",
		"es": "Datos del contacto"
	},

	"NewContacts": {
		"pt": "Novo contato",
		"en": "new contact",
		"es": "nuevo contacto"
	},

	"NewChecklist": {
		"pt": "Novo checklist",
		"en": "new checklist",
		"es": "nuevo checklist"
	},

	"Contatos": {
		"pt": "Contatos",
		"en": "Contacts",
		"es": "Contactos"
	},

	"Fotos": {
		"pt": "Fotos",
		"en": "Photos",
		"es": "Fotos"
	},
	"InserirFoto": {
		"pt": "Inserir",
		"en": "insert",
		"es": "insertar"
	},
	"SubstituirFoto": {
		"pt": "Substituir",
		"en": "Replace",
		"es": "reemplazar"
	},
	"ExcluirFoto": {
		"pt": "Deletar",
		"en": "Delete",
		"es": "borrar"
	},
	"EditarFoto": {
		"pt": "Editar",
		"en": "To edit",
		"es": "Editar"
	},
	"User": {
		"s": {
			"pt": "Usuário",
			"en": "User",
			"es": "Usuario"
		},
		"p": {
			"pt": "Usuários",
			"en": "Users",
			"es": "Usuarios"
		}
	},
	"Password": {
		"pt": "Senha",
		"en": "Password",
		"es": "Contraseña"
	},
	"Enter": {
		"pt": "Entrar",
		"en": "Enter",
		"es": "Entrar"
	},
	"Registrations": {
		"pt": "Cadastros",
		"en": "Registrations",
		"es": "Entradas"
	},
	"Operational": {
		"pt": "Operacional",
		"en": "Operational",
		"es": "Operacional"
	},
	"Financial": {
		"pt": "Financeiro",
		"en": "Financial",
		"es": "Financiero"
	},
	"Dashboard": {
		"pt": "Dashboard",
		"en": "Dashboard",
		"es": "Tablero"
	},
	"Exit": {
		"pt": "Sair",
		"en": "Exit",
		"es": "Salir"
	},
	"Actions": {
		"s": {
			"pt": "Atividade",
			"en": "Action",
			"es": "Actividade"
		},
		"p": {
			"pt": "Atividades",
			"en": "Actions",
			"es": "Actividades"
		}
	},
	"Events": {
		"s": {
			"pt": "Evento",
			"en": "Event",
			"es": "Evento"
		},
		"p": {
			"pt": "Eventos",
			"en": "Events",
			"es": "Eventos"
		}
	},
	"Equipments": {
		"s": {
			"pt": "Equipamento",
			"en": "Equipment",
			"es": "Equipo"
		},
		"p": {
			"pt": "Equipamentos",
			"en": "Equipments",
			"es": "Equipamientos"
		}
	},
	"OccurrencesDetailsByCustomer": {
		"pt": "Detalhes ocorrências por cliente",
		"en": "Occurrences details by customer",
		"es": "Detalles de ocurrencias por cliente"
	},
	"DetailsOccurrencesByEquipment": {
		"pt": "Detalhes ocorrências por equipamento",
		"en": "Details occurrences by equipment",
		"es": "Detalles de sucesos por equipo"
	},
	"Clients": {
		"negrao": {
			"s": {
				"pt": "Unidade",
				"en": "Unit",
				"es": "Unidad"
			},
			"p": {
				"pt": "Unidades",
				"en": "Units",
				"es": "Unidades"
			}
		},
		"raizen": {
			"s": {
				"pt": "Unidade",
				"en": "Unit",
				"es": "Unidad"
			},
			"p": {
				"pt": "Unidades",
				"en": "Units",
				"es": "Unidades"
			}
		},
		"pampili": {
			"s": {
				"pt": "Unidade",
				"en": "Unit",
				"es": "Unidad"
			},
			"p": {
				"pt": "Unidades",
				"en": "Units",
				"es": "Unidades"
			}
		},
		"pecompe": {
			"s": {
				"pt": "Unidade",
				"en": "Unit",
				"es": "Unidad"
			},
			"p": {
				"pt": "Unidades",
				"en": "Units",
				"es": "Unidades"
			}
		},
		"zenega": {
			"s": {
				"pt": "Cliente/Projeto",
				"en": "Customer/Project",
				"es": "Cliente/Proyecto"
			},
			"p": {
				"pt": "Clientes/Projetos",
				"en": "Customers/Projects",
				"es": "Clientes/Proyectos"
			}
		},
		"s": {
			"pt": "Cliente",
			"en": "Customer",
			"es": "Cliente"
		},
		"p": {
			"pt": "Clientes",
			"en": "Customers",
			"es": "Clientes"
		}
	},
	"ServiceOrders": {
		"s": {
			"pt": "Ordem de Serviço",
			"en": "Service Order",
			"es": "Orden de servicio"
		},
		"p": {
			"pt": "Ordens de Serviço",
			"en": "Service Orders",
			"es": "Orden de servicio"
		}
	},
	"Location": {
		"pt": "Localização",
		"en": "Location",
		"es": "Ubicación"
	},
	"Settlements": {
		"s": {
			"pt": "Liquidação",
			"en": "Settlement",
			"es": "Liquidación"
		},
		"p": {
			"pt": "Liquidações",
			"en": "Settlements",
			"es": "Liquidaciones"
		}
	},
	"Expense": {
		"pt": "Despesas",
		"en": "Expenses",
		"es": "Gastos"
	},
	"Reports": {
		"pt": "Relatórios",
		"en": "Reports",
		"es": "Informes"
	},
	"SettlementsSummary": {
		"pt": "Resumo de Liquidações",
		"en": "Summary of Settlements",
		"es": "Resumen de Liquidaciones"
	},
	"inventorySummary": {
		"pt": "Resumo de Inventario",
		"en": "Summary of Inventory",
		"es": "Resumen de Inventario"
	},

	"Timeline": {
		"pt": "Linha do Tempo",
		"en": "Timeline",
		"es": "Linea del tiempo"
	},
	"Route": {
		"pt": "Rota",
		"en": "Route",
		"es": "Ruta"
	},
	"Appointment": {
		"pt": "Apontamento",
		"en": "Appointment",
		"es": "Nota"
	},
	"Offenders": {
		"pt": "Ofensores",
		"en": "Offenders",
		"es": "Ofensores"
	},
	"Close": {
		"pt": "Fechar",
		"en": "Close",
		"es": "Cerrar"
	},
	"Client": {
		"pt": "Cliente",
		"en": "Client",
		"es": "Cliente"
	},
	"Equipment": {
		"pt": "Equipamento",
		"en": "Equipment",
		"es": "Equipo"
	},
	"New": {
		"pt": "Novo",
		"en": "New",
		"es": "Nuevo"
	},
	"Filter": {
		"pt": "Filtro",
		"en": "Filter",
		"es": "Filtrar"
	},
	"Description": {
		"pt": "Descrição",
		"en": "Description",
		"es": "Descripción"
	},
	"Details": {
		"pt": "Detalhes",
		"en": "Details",
		"es": "Detalles"
	},
	"ActionData": {
		"pt": "Dados da Atividade",
		"en": "Action Data",
		"es": "Datos de la Actividad"
	},
	"ToolData": {
		"pt": "Dados de Ferramenta",
		"en": "Tool Data",
		"es": "Datos de la Instrumento"
	},
	"Checklist": {
		"pt": "Checklist",
		"en": "Checklist",
		"es": "Lista de verificación"
	},
	"Color": {
		"pt": "Cor",
		"en": "Color",
		"es": "Color"
	},
	"ResourceCritically": {
		"pt": "Recursos",
		"en": "Resources",
		"es": "Recursos"
	},
	"Fields": {
		"pt": "Campos",
		"en": "Fields",
		"es": "Campos"
	},
	"FieldName": {
		"pt": "Nome do campo",
		"en": "Field name",
		"es": "Nombre del campo"
	},
	"Text": {
		"pt": "Texto",
		"en": "Text",
		"es": "Texto"
	},

	"Number": {
		"pt": "Número",
		"en": "Number",
		"es": "Número"
	},

	"Date": {
		"pt": "Data",
		"en": "Date",
		"es": "Fecha"
	},
	"List": {
		"pt": "Lista",
		"en": "List",
		"es": "Lista"
	},
	"ListValues": {
		"pt": "Valores da lista",
		"en": "List values",
		"es": "Valores de la lista"
	},
	"CheckListValues": {
		"pt": "Valores do checklist",
		"en": "Checklist values",
		"es": "Valores del checklist"
	},
	"LinkHere": {
		"pt": "Informe aqui o link",
		"en": "Enter the link here",
		"es": "Introduce el enlace aquí"
	},
	"ReturnToList": {
		"pt": "Retornar à lista",
		"en": "Return to list",
		"es": "Volver a la lista"
	},
	"Save": {
		"pt": "Salvar",
		"en": "Save",
		"es": "Guardar"
	},
	"EventData": {
		"pt": "Dados do Evento",
		"en": "Event Data",
		"es": "Datos del Evento"
	},
	"ChecklistData": {
		"pt": "Dados do Checklist",
		"en": "Checklist Data",
		"es": "Datos del checklist"
	},
	"ChecklistDataItem": {
		"pt": "Itens do Checklist",
		"en": "Checklist Item",
		"es": "Datos del checklist"
	},
	"UnproductiveTime": {
		"pt": "Tempo improdutivo",
		"en": "Unproductive time",
		"es": "Tiempo improductivo"
	},
	"AvailableTo": {
		"pt": "Dísponivel para",
		"en": "Available to",
		"es": "Dísponible para"
	},
	"SelectAnOption": {
		"pt": "Selecione uma opção",
		"en": "Select an option"
	},
	"SO": {
		"pt": "O.S.",
		"en": "S.O.",
		"es": "O.S."
	},
	"EquipmentData": {
		"pt": "Dados do Equipamento",
		"en": "Equipment Data",
		"es": "Datos del equipo"
	},
	"EstimatedTime": {
		"pt": "Tempo estimado",
		"en": "Estimated time",
		"es": "Hora prevista"
	},
	"Images": {
		"s": {
			"pt": "Imagem",
			"en": "Image",
			"es": "Imagen"
		},
		"p": {
			"pt": "Imagens",
			"en": "Images",
			"es": "Imágenes"
		}
	},
	"Active": {
		"s": {
			"pt": "Ativo",
			"en": "Active",
			"es": "Activo"
		},
		"p": {
			"pt": "Ativos",
			"en": "Active",
			"es": "Activos"
		}
	},
	"Inactive": {
		"s": {
			"pt": "Inativo",
			"en": "Inactive",
			"es": "Inactivo"
		},
		"p": {
			"pt": "Inativos",
			"en": "Inactive",
			"es": "Inactivo"
		}
	},
	"All": {
		"pt": "Todos",
		"en": "All",
		"es": "Todos"
	},
	"Name": {
		"pt": "Nome",
		"en": "Name",
		"es": "Nombre"
	},
	"Phone": {
		"pt": "Telefone",
		"en": "Phone",
		"es": "Teléfono"
	},
	"ProjectData": {
		"pt": "Dados do Projeto",
		"en": "Project Data",
		"es": "Datos del proyecto"
	},
	"ClientData": {
		"negrao": {
			"pt": "Dados da Unidade",
			"en": "Unit Data",
			"es": "Datos de la unidad"
		},
		"raizen": {
			"pt": "Dados da Unidade",
			"en": "Unit Data",
			"es": "Datos de la unidad"
		},
		"pampili": {
			"pt": "Dados da Unidade",
			"en": "Unit Data",
			"es": "Datos de la unidad"
		},
		"pecompe": {
			"pt": "Dados da Unidade",
			"en": "Unit Data",
			"es": "Datos de la unidad"
		},
		"zenega": {
			"pt": "Dados do Cliente/Projeto",
			"en": "Customer/Project Data",
			"es": "Datos del Cliente/Proyecto"
		},
		"pt": "Dados do Cliente",
		"en": "Customer Data",
		"es": "Datos del cliente"
	},
	"CompanyName": {
		"pt": "Razão Social",
		"en": "Company Name",
		"es": "Nombre de Compañía"
	},
	"BusinessName": {
		"pt": "Fantasia",
		"en": "Business name",
		"es": "Fantasía"
	},
	"ITINEIN": {
		"pt": "CPF/CNPJ",
		"en": "ITIN/EIN",
		"es": "SS/IVA"
	},
	"Address": {
		"pt": "Endereço",
		"en": "Address",
		"es": "Dirección"
	},
	"EIN": {
		"pt": "CNPJ",
		"en": "EIN",
		"es": "IVA"
	},
	"AccessPassword": {
		"pt": "Senha de acesso",
		"en": "Access password",
		"es": "Contraseña de acceso"
	},
	"Status": {
		"pt": "Situação",
		"en": "Status",
		"es": "Situación"
	},
	"NewEquipment": {
		"pt": "Novo Equipamento",
		"en": "New Equipment",
		"es": "Nuevo Equipamiento"
	},
	"NewInventory": {
		"pt": "Novo Inventário",
		"en": "New Inventory",
		"es": "Nuevo Inventario"
	},
	"NewCostCenter": {
		"pt": "Novo Centro de Custo",
		"en": "New Cost Center",
		"es": "Nuevo Centro de Costos"
	},
	"Remove": {
		"pt": "Remover",
		"en": "Remove",
		"es": "Retirar"
	},
	"EquipmentType": {
		"pt": "Tipo de Equipamento",
		"en": "Equipment Type",
		"es": "Tipo de Equipo"
	},
	"Identifier": {
		"pt": "Identificador",
		"en": "Identifier",
		"es": "Identificador"
	},
	"Brand": {
		"pt": "Marca",
		"en": "Brand",
		"es": "Marca"
	},
	"Model": {
		"pt": "Modelo",
		"en": "Model",
		"es": "Modelo"
	},
	"YearManufacture": {
		"pt": "Ano Fabricação",
		"en": "Manufacturing Year",
		"es": "Año de Fabricación"
	},
	"ModelYear": {
		"pt": "Ano Modelo",
		"en": "Model Year",
		"es": "Año modelo"
	},
	"Confirm": {
		"pt": "Confirmar",
		"en": "Confirm",
		"es": "Confirmar"
	},
	"Email": {
		"pt": "E-mail",
		"en": "E-mail",
		"es": "E-mail"
	},
	"Birth": {
		"pt": "Nasc.",
		"en": "Birth",
		"es": "Nac."
	},
	"WebAccess": {
		"s": {
			"pt": "Acessa Web",
			"en": "Web Access",
			"es": "Acceder a la web"
		},
		"p": {
			"pt": "Acessos a Web",
			"en": "Web Access",
			"es": "Acceder a la web"
		}
	},
	"AppAccess": {
		"s": {
			"pt": "Acessa App",
			"en": "App Access",
			"es": "Acceder a la app"
		},
		"p": {
			"pt": "Acessos ao App",
			"en": "App Access",
			"es": "Acceder a la app"
		}
	},
	"Yes": {
		"pt": "Sim",
		"en": "Yes",
		"es": "Sí"
	},
	"No": {
		"pt": "Não",
		"en": "No",
		"es": "No"
	},
	"UpdatePassword": {
		"pt": "Atualizar senha",
		"en": "Update password",
		"es": "Actualiza contraseña"
	},
	"Confirmation": {
		"pt": "Confirmação",
		"en": "Confirmation",
		"es": "Confirmación"
	},
	"ConfirmYourPassword": {
		"pt": "Confirme sua senha",
		"en": "Confirm your password",
		"es": "Confirmar la contraseña"
	},
	"Cancel": {
		"pt": "Cancelar",
		"en": "Cancel",
		"es": "Cancelar"
	},
	"Update": {
		"pt": "Atualizar",
		"en": "Update",
		"es": "Actualizar"
	},
	"Services": {
		"s": {
			"pt": "Serviço",
			"en": "Service",
			"es": "Servicio"
		},
		"p": {
			"pt": "Serviços",
			"en": "Services",
			"es": "Servicios"
		}
	},
	"UserServices": {
		"pt": "Serviços do Usuário",
		"en": "User Services",
		"es": "Servicios de Usuario"
	},
	"NoOfServices": {
		"pt": "Qtde. Serviços",
		"en": "No. of services",
		"es": "Cant. de servicios"
	},
	"BeginningOfSO": {
		"pt": "Início da OS",
		"en": "Beginning of SO",
		"es": "Inicio de OS"
	},
	"Occurrences": {
		"s": {
			"pt": "Ocorrência",
			"en": "Occurrence",
			"es": "Ocurrencia"
		},
		"p": {
			"pt": "Ocorrências",
			"en": "Occurrences",
			"es": "Ocurrencias"
		}
	},
	"EndOfSO": {
		"pt": "Fim da OS",
		"en": "End of SO",
		"es": "Fin de la OS"
	},
	"NumberOfSO": {
		"pt": "Quantidade de OS",
		"en": "Number of SO",
		"es": "Cantidad de OS"
	},
	"InitialDate": {
		"pt": "Data inicial",
		"en": "Initial date",
		"es": "Fecha inicio"
	},
	"FinalDate": {
		"pt": "Data final",
		"en": "Final date",
		"es": "Fecha final"
	},
	"Search": {
		"pt": "Pesquisar",
		"en": "Search",
		"es": "Buscar"
	},
	"ServiceInformation": {
		"pt": "Informações do serviço",
		"en": "Service information",
		"es": "Información del servicio"
	},
	"Back": {
		"pt": "voltar",
		"en": "back",
		"es": "regreso"
	},
	"Begin": {
		"pt": "Início",
		"en": "Begin",
		"es": "Inicio"
	},
	"End": {
		"pt": "Fim",
		"en": "End",
		"es": "Fin"
	},
	"Observation": {
		"s": {
			"pt": "Observação",
			"en": "Observation",
			"es": "Observación"
		},
		"p": {
			"pt": "Observações",
			"en": "Observations",
			"es": "Observaciones"
		}
	},
	"ObservationData": {
		"pt": "Dados da Observação",
		"en": "Observation Data",
		"es": "Datos de la observación"
	},
	"Map": {
		"pt": "Mapa",
		"en": "Map",
		"es": "Mapa"
	},
	"Print": {
		"pt": "Imprimir",
		"en": "Print",
		"es": "Impresión"
	},
	"ContainsIgnoredImage": {
		"pt": "Contém imagem ignorada",
		"en": "Contains ignored image",
		"es": "Contiene imagen ignorada"
	},
	"ContainsItemsCritical": {
		"pt": "Contém itens criticos",
		"en": "Contains critical items",
		"es": "Contiene elementos críticos"
	},
	"PartialClosure": {
		"pt": "Encerramento parcial",
		"en": "Partial Closure",
		"es": "Cierre parcial"
	},
	"Interruptions": {
		"pt": "Interrupções",
		"en": "Interruptions",
		"es": "interrupciones"
	},
	"IgnoredImages": {
		"pt": "Imagens ignoradas",
		"en": "Ignored Images"
	},
	"ZipCode": {
		"pt": "CEP",
		"en": "Zip code",
		"es": "Código postal"
	},
	"UpdateTime": {
		"pt": "Tempo de atualização",
		"en": "Update Time",
		"es": "Tiempo de actualización"
	},
	"ShowCaption": {
		"pt": "Mostrar legenda",
		"en": "Show caption",
		"es": "Mostrar leyenda"
	},
	"LastAction": {
		"pt": "Última atividade",
		"en": "Last action",
		"es": "Última Actividad"
	},
	"Updated": {
		"pt": "Atualizado em",
		"en": "Updated",
		"es": "Actualizado"
	},
	"ShowRoute": {
		"pt": "Mostrar rota",
		"en": "Show route",
		"es": "Mostrar la ruta"
	},
	"Amount": {
		"pt": "Valor",
		"en": "Amount",
		"es": "Valor"
	},
	"Payment": {
		"pt": "Pagamento",
		"en": "Payment",
		"es": "Pago"
	},
	"SettlementInformation": {
		"pt": "Informações da Liquidação",
		"en": "Settlement Information",
		"es": "Información de la liquidación"
	},

	"PaymentType": {
		"pt": "Tipo de Pagamento",
		"en": "Payment Type",
		"es": "Tipo de Pago"
	},

	"Type": {
		"pt": "Tipo",
		"en": "Type",
		"es": "Tipo"
	},

	"Period": {
		"pt": "Período",
		"en": "Period",
		"es": "Período"
	},
	"TotalAmount": {
		"pt": "Valor Total",
		"en": "Amount",
		"es": "Valor total"
	},
	"ExpenseData": {
		"pt": "Dados da Despesa",
		"en": "Expense Data",
		"es": "Datos del gasto"
	},
	"CardReleaseData": {
		"pt": "Dados lançamento do Cartão",
		"en": "Card Release Data",
		"es": "Datos de liberación de la tarjetao"
	},
	"Sequence": {
		"pt": "Sequência",
		"en": "Sequence",
		"es": "Siguiente"
	},
	"ImportSettlements": {
		"pt": "Importar Liquidações",
		"en": "Import Settlements",
		"es": "Importar Liquidaciones"
	},
	"Qty": {
		"pt": "Qtde.",
		"en": "Qty.",
		"es": "Cant."
	},
	"UnitValue": {
		"pt": "Valor Unit.",
		"en": "Unit. Value",
		"es": "Valor Unit."
	},
	"Insert": {
		"pt": "Inserir",
		"en": "Insert",
		"es": "Insertar"
	},
	"ImportSettlmentData": {
		"pt": "Importar dados de liquidação",
		"en": "Import settlement data",
		"es": "Importar datos de liquidación"
	},
	"NoRecordOfSettlements": {
		"pt": "Sem registro de liquidações",
		"en": "No record of settlements"
	},
	"ExportPhotos": {
		"pt": "Exportar Fotos",
		"en": "Export Photos",
		"es": "Exportar Fotos"
	},
	"GenerateStatement": {
		"pt": "Gerar Demonstrativo",
		"en": "Generate Statement",
		"es": "Generar demostración"
	},
	"GenerateDebitNote": {
		"pt": "Gerar Nota Débito",
		"en": "Generate Debit Note",
		"es": "Generar Nota Deuda"
	},
	"CancelExpense": {
		"pt": "Cancelar Despesa",
		"en": "Cancel Expense",
		"es": "Cancelar Gastos"
	},
	"Identification": {
		"pt": "Identificação",
		"en": "Identification",
		"es": "Identificación"
	},
	"PointRecords": {
		"s": {
			"pt": "Registro de Ponto",
			"en": "Point record",
			"es": "Registro de punto"
		},
		"p": {
			"pt": "Registros de ponto",
			"en": "Point records",
			"es": "Registros de punto"
		}
	},
	"FilterData": {
		"pt": "Filtrar Dados",
		"en": "Filter Data",
		"es": "Filtrar Datos"
	},
	"EventRecord": {
		"pt": "Registro de Evento",
		"en": "Event Record",
		"es": "Registro de Evento"
	},
	"NotesOfTheDay": {
		"pt": "Apontamentos do dia ",
		"en": "Notes of the day",
		"es": "Apuntes del día"
	},
	"TotalSettlement": {
		"pt": "Total das liquidações",
		"en": "Total settlement",
		"es": "Total de las liquidaciones"
	},
	"RemoveItemFromTheDay": {
		"pt": "Remover item de description do dia date ?",
		"en": "Remove item from description the day date ?",
		"es": "Eliminar elemento de description del día de date?"
	},
	"ServiceOrdersPerClient": {
		"negrao": {
			"pt": "Ordens de serviço por unidade",
			"en": "Service orders per unit",
			"es": "Orden de servicio por unidad"
		},
		"raizen": {
			"pt": "Ordens de serviço por unidade",
			"en": "Service orders per unit",
			"es": "Orden de servicio por unidad"
		},
		"pampili": {
			"pt": "Ordens de serviço por unidade",
			"en": "Service orders per unit",
			"es": "Orden de servicio por unidad"
		},
		"pecompe": {
			"pt": "Ordens de serviço por unidade",
			"en": "Service orders per unit",
			"es": "Orden de servicio por unidad"
		},
		"zenega": {
			"pt": "Ordens de serviço por cliente/projeto",
			"en": "Service orders per customer/project",
			"es": "Orden de servicio por cliente/proyecto"
		},
		"pt": "Ordens de serviço por cliente",
		"en": "Service orders per customer",
		"es": "Órdenes de servicio por cliente"
	},
	"ServiceOrdersPerDay": {
		"pt": "Ordens de serviço por dia",
		"en": "Service orders per day",
		"es": "Órdenes de servicio por día"
	},
	"ActionsPerClient": {
		"negrao": {
			"pt": "Atividades por unidade",
			"en": "Action per unit",
			"es": "Actividades por unidad"
		},
		"raizen": {
			"pt": "Atividades por unidade",
			"en": "Action per unit",
			"es": "Actividades por unidad"
		},
		"pampili": {
			"pt": "Atividades por unidade",
			"en": "Action per unit",
			"es": "Actividades por unidad"
		},
		"pecompe": {
			"pt": "Atividades por unidade",
			"en": "Action per unit",
			"es": "Actividades por unidad"
		},
		"zenega": {
			"pt": "Estados por cliente/projeto",
			"en": "States per customer /project",
			"es": "Estados por cliente/proyecto"
		},
		"pt": "Atividades por cliente",
		"en": "Action per customer",
		"es": "Actividades por cliente"
	},
	"ActionsPerUser": {
		"pt": "Atividades por usuário",
		"en": "Action per user",
		"es": "Actividades por usuario"
	},
	"TotalBy": {
		"pt": "Total por",
		"en": "Total by",
		"es": "Total por"
	},
	"Quantity": {
		"pt": "Quantidade",
		"en": "Quantity",
		"es": "Cantidad"
	},
	"Hour": {
		"s": {
			"pt": "Hora",
			"en": "Hour",
			"es": "Hora"
		},
		"p": {
			"pt": "Horas",
			"en": "Hours",
			"es": "Horas"
		}
	},
	"NoRegistry": {
		"pt": "Sem registros",
		"en": "No registry",
		"es": "Sin registros"
	},
	"Graphic": {
		"pt": "Gráfico",
		"en": "Graphic",
		"es": "Gráfico"
	},
	"Pizza": {
		"pt": "Pizza",
		"en": "Pizza",
		"es": "Pizza"
	},
	"Bars": {
		"pt": "Barras",
		"en": "Bars",
		"es": "Bares"
	},
	"Edit": {
		"pt": "Editar",
		"en": "Edit",
		"es": "Editar"
	},
	"To": {
		"pt": "à",
		"en": "to",
		"es": "para"
	},
	"Download": {
		"pt": "Download",
		"en": "Download",
		"es": "Descargar"
	},
	"COMPARATIVE": {
		"pt": "COMPARATIVO",
		"en": "COMPARATIVE",
		"es": "COMPARATIVO"
	},
	"OffendersItensByClient": {
		"negrao": {
			"pt": "Ocorrências por unidade",
			"en": "Occurrences by unit",
			"es": "Ocurrencias por unidade"
		},
		"raizen": {
			"pt": "Ocorrências por unidade",
			"en": "Occurrences by unit",
			"es": "Ocurrencias por unidade"
		},
		"pt": "Ocorrências por cliente",
		"en": "Occurrences by customer",
		"es": "Ocurrencias por cliente"
	},
	"OffendersItensByEquipment": {
		"pt": "Ocorrências por equipamento",
		"en": "Occurrences by equipment",
		"es": "Ocurrencias de equipo"
	},
	"OffendersItens": {
		"pt": "Principais ofensores",
		"en": "Top Offenders",
		"es": "Delincuentes principales"
	},
	"SUMMARYBYEVENT": {
		"pt": "RESUMO POR EVENTO",
		"en": "SUMMARY BY EVENT",
		"es": "RESUMEN POR EVENTO"
	},
	"SettlementData": {
		"pt": "Dados da Liquidação",
		"en": "Settlement Data",
		"es": "Datos de la liquidación"
	},
	"Summary": {
		"pt": "Resumo",
		"en": "Summary",
		"es": "Resumen"
	},
	"InventorybyStorages": {
		"negrao": {
			"pt": "Inventario por local de armazenamento",
			"en": "Inventory by storage location",
			"es": "Inventario por ubicación de almacenamiento"
		},
		"raizen": {
			"pt": "Inventario por local de armazenamento",
			"en": "Inventory by storage location",
			"es": "Inventario por ubicación de almacenamiento"
		},
		"pt": "Inventario por local de armazenamento",
			"en": "Inventory by storage location",
			"es": "Inventario por ubicación de almacenamiento"
	},




	"AdditionalData": {
		"pt": "Dados Adicionais",
		"en": "Additional Data",
		"es": "Datos adicionales"
	},
	"DisplaysFormula": {
		"pt": "Exibe fórmula no demonstrativo",
		"en": "Displays formula in the statement",
		"es": "Muestra fórmula en la declaración."
	},
	"EXPNOTEDEB": {
		"pt": "EXP. NOTA DEB.",
		"en": "EXP. NOTE DEB.",
		"es": "EXP. NOTA DEB."
	},
	"Delete": {
		"pt": "Excluir",
		"en": "Delete",
		"es": "Borrar"
	},
	"NoSettlementRecords": {
		"pt": "Sem registros de liquidações",
		"en": "No settlement records",
		"es": "Sin registros de liquidaciones"
	},
	"TotalValueOfSettlements": {
		"pt": "Valor total das liquidações",
		"en": "Total value of settlements",
		"es": "Valor total de las liquidaciones"
	},
	"Import": {
		"pt": "Importar",
		"en": "Import",
		"es": "Importación"
	},
	"EXPENSESDEMO": {
		"pt": "DEMONSTRATIVO DE DESPESAS",
		"en": "EXPENSES DEMO",
		"es": "DEMOSTRATIVO DE GASTOS"
	},
	"BOARDCOMPUTERS": {
		"pt": "COMPUTADORES DE BORDO",
		"en": "BOARD COMPUTERS",
		"es": "COMPUTADORES DE BORDO"
	},
	"TOTALVALUEOFDESCRIPTIONS": {
		"pt": "VALOR TOTAL DOS DESCRITIVOS",
		"en": "TOTAL VALUE OF DESCRIPTIONS",
		"es": "VALOR TOTAL DE LOS DESCRIPCION"
	},
	"Adjusted": {
		"pt": "Corrigido",
		"en": "Adjusted",
		"es": "Corregido"
	},
	"ServiceGeolocation": {
		"pt": "Geolocalização do Serviço",
		"en": "Service Geolocation",
		"es": "Geolocalización del Servicio"
	},
	"Latitude": {
		"pt": "Latitude",
		"en": "Latitude",
		"es": "Latitud"
	},
	"Longitude": {
		"pt": "Longitude",
		"en": "Longitude",
		"es": "Longitud"
	},
	"Obs": {
		"pt": "Obs.",
		"en": "Obs.",
		"es": "Obs."
	},
	"UserData": {
		"pt": "Dados do Usuário",
		"en": "User Data",
		"es": "Datos del usuario"
	},
	"BirthDate": {
		"pt": "Data de nascimento",
		"en": "Birth Date",
		"es": "Fecha de nacimiento"
	},
	"Vehicle": {
		"pt": "Equipamento",
		"en": "Equipment",
		"es": "Equipamiento"
	},
	"ServiceData": {
		"pt": "Dados do Serviço",
		"en": "Service Data",
		"es": "Datos del Servicio"
	},
	"Reg": {
		"pt": "Inscr.",
		"en": "Reg.",
		"es": "Inscr."
	},
	"RegID": {
		"pt": "Inscr./RG",
		"en": "Reg./ID",
		"es": "Inscr./DNI"
	},
	"Site": {
		"pt": "Site",
		"en": "Site",
		"es": "Sitio"
	},
	"ViewSettlement": {
		"pt": "Visualizar Liquidação",
		"en": "View Settlement",
		"es": "Ver la liquidación"
	},
	"DuplicateExpense": {
		"pt": "Duplicar Despesa",
		"en": "Duplicate Expense",
		"es": "Duplicar el gasto"
	},
	"EditExpense": {
		"pt": "Editar Despesa",
		"en": "Edit Expense",
		"es": "Editar el gasto"
	},
	"RemoveExpense": {
		"pt": "Remover Despesa",
		"en": "Remove Expense",
		"es": "Quitar el gasto"
	},
	"CO": {
		"pt": "A/C",
		"en": "C/o",
		"es": "A/C"
	},
	"Access": {
		"pt": "Acessa",
		"en": "Access",
		"es": "Accesos"
	},
	"Profile": {
		"s": {
			"pt": "Perfil",
			"en": "Profile",
			"es": "Perfil"
		},
		"p": {
			"pt": "Perfis",
			"en": "Profiles",
			"es": "Perfiles"
		}
	},
	"ProfileData": {
		"pt": "Dados do Perfil",
		"en": "Profile Data",
		"es": "Datos del Perfil"
	},
	"Permission": {
		"s": {
			"pt": "Permissão",
			"en": "Permission",
			"es": "Permiso"
		},
		"p": {
			"pt": "Permissões",
			"en": "Permissions",
			"es": "Permisos"
		}
	},
	"None": {
		"pt": "Nenhum",
		"en": "None",
		"es": "Ninguno"
	},
	"General": {
		"pt": "Geral",
		"en": "General",
		"es": "General"
	},
	"Species": {
		"pt": "Espécie",
		"en": "Species",
		"es": "Especies"
	},
	"CreditCard": {
		"pt": "Cartão de Crédito",
		"en": "Credit Card",
		"es": "Tarjeta de Crédito"
	},
	"InProgress": {
		"pt": "Em andamento",
		"en": "In progress",
		"es": "En proceso"
	},
	"WaitingForApproval": {
		"pt": "Aguardando aprovação",
		"en": "Waiting for approval",
		"es": "Esperando aprobación"
	},
	"AwaitingPayment": {
		"pt": "Aguardando pagamento",
		"en": "Awaiting payment",
		"es": "Esperando el pago"
	},
	"PartialPayment": {
		"pt": "Pagamento Parcial",
		"en": "Partial Payment",
		"es": "Pago Parcial"
	},
	"PaidOut": {
		"pt": "Pago",
		"en": "Paid out",
		"es": "Pagado"
	},
	"Canceled": {
		"pt": "Cancelado",
		"en": "Canceled",
		"es": "Cancelado"
	},
	"NoAppointment": {
		"pt": "SEM APONTAMENTO",
		"en": "NO APPOINTMENT",
		"es": "SIN PUNTITO"
	},
	"Monetary": {
		"pt": "Monetário",
		"en": "Monetary",
		"es": "Monetario"
	},
	"selectAnOption": {
		"pt": "Selecione uma opção",
		"en": "Select an option",
		"es": "Seleccione una opción"
	},
	"UserProfile": {
		"s": {
			"pt": "Perfil de Usuário",
			"en": "User Profile",
			"es": "Perfil de Usuario"
		},
		"p": {
			"pt": "Perfis de Usuário",
			"en": "User Profiles",
			"es": "Perfiles de Usuario"
		}
	},
	"Parameter": {
		"s": {
			"pt": "Parâmetro",
			"en": "Parameter",
			"es": "Parámetro"
		},
		"p": {
			"pt": "Parâmetros",
			"en": "Parameters",
			"es": "Parámetros"
		}
	},
	"Setting": {
		"s": {
			"pt": "Configuração",
			"en": "Setting",
			"es": "Configuración"
		},
		"p": {
			"pt": "Configurações",
			"en": "Settings",
			"es": "Ajustes"
		}
	},
	"WorkHours": {
		"pt": "Expediente",
		"en": "Work hours",
		"es": "Horas laborales"
	},
	"Inventory": {
		"pt": "Inventário",
		"en": "Inventory",
		"es": "Inventario"
	},
	"Observation": {
		"pt": "Observação",
		"en": "Observation",
		"es": "Observación"
	},
	"Critically": {
		"pt": "Criticidade",
		"en": "Critically",
		"es": "Criticidad"
	},
	"UseSystem": {
		"pt": "Usa sistema / sub-sistema",
		"en": "Uses system / sub-system",
		"es": "Utiliza sistema / subsistema"
	},
	"ObjectType": {
		"s": {
			"pt": "Tipo de Objeto",
			"en": "Object Type",
			"es": "Tipo de objeto"
		},
		"p": {
			"pt": "Tipos de Objeto",
			"en": "Object Types",
			"es": "Tipos de objetos"
		}
	},
	"ObjectTypeData": {
		"pt": "Dados do Tipo de Objeto",
		"en": "Object Type Data",
		"es": "Datos del Tipo de Objeto"
	},
	"Object": {
		"s": {
			"pt": "Objeto",
			"en": "Object",
			"es": "Objeto"
		},
		"p": {
			"pt": "Objetos",
			"en": "Objects",
			"es": "Objetos"
		}
	},
	"ObjectData": {
		"pt": "Dados do Objeto",
		"en": "Object Data",
		"es": "Datos del Objeto"
	},
	"Patrimony": {
		"pt": "Patrimônio",
		"en": "Patrimony",
		"es": "Patrimonio"
	},
	"SerialNumber": {
		"pt": "Número de Série",
		"en": "Serial Number",
		"es": "Numero de Serie"
	},
	"Series": {
		"pt": "Série",
		"en": "Series",
		"es": "Serie"
	},
	"Moves": {
		"pt": "Movimentações",
		"en": "Moves",
		"es": "Se mueve"
	},
	"Storage": {
		"s": {
			"pt": "Local de estoque",
			"en": "Storage",
			"es": "Local de inventario"
		},
		"p": {
			"pt": "Locais de estoque",
			"en": "Storages",
			"es": "Locales de inventario"
		}
	},
	"StorageData": {
		"pt": "Dados do Local de Estoque",
		"en": "Storage Data",
		"es": "Datos de Local de Inventario"
	},
	"Required": {
		"pt": "Obrigatório",
		"en": "Required",
		"es": "Obligatorio"
	},
	"SelectGallery": {
		"pt": "Seleciona galeria",
		"en": "Select gallery",
		"es": "Selecciona galería"
	},
	"PointClosure": {
		"pt": "ENCERRAMENTO DE PONTO",
		"en": "POINT CLOSURE",
		"es": "CIERRE DE PUNTO"
	},
	"EventClosure": {
		"pt": "ENCERRAMENTO DE EVENTO",
		"en": "EVENT CLOSURE",
		"es": "CIERRE DE EVENTO"
	},
	"TIME": {
		"pt": "TEMPO",
		"en": "TIME",
		"es": "TIEMPO"
	},
	"Time": {
		"pt": "Tempo",
		"en": "Time",
		"es": "Tiempo"
	},
	"ISSUANCEDATE": {
		"pt": "DATA EMISSÃO",
		"en": "ISSUANCE DATE",
		"es": "FECHA DE EMISIÓN"
	},
	"DUEDATE": {
		"pt": "DATA VENCIMENTO",
		"en": "DUE DATE",
		"es": "FECHA DE VENCIMIENTO"
	},
	"DEBITNOTE": {
		"pt": "NOTA DE DÉBITO",
		"en": "DEBIT NOTE",
		"es": "NOTA DE DÉBITO"
	},
	"EMITTER": {
		"pt": "EMISSOR",
		"en": "EMITTER",
		"es": "EMISOR"
	},
	"ADDRESS": {
		"pt": "ENDEREÇO",
		"en": "ADDRESS",
		"es": "DIRECCIÓN"
	},
	"PHONE": {
		"pt": "TELEFONE",
		"en": "PHONE",
		"es": "TELÉFONO"
	},
	"ZIPCODE": {
		"pt": "CEP",
		"en": "ZIPCODE",
		"es": "CÓDIGO POSTAL"
	},
	"CITY": {
		"pt": "MUNICÍPIO",
		"en": "CITY",
		"es": "MUNICIPIO"
	},
	"STATREG": {
		"pt": "INSC. EST.",
		"en": "STAT. REG.",
		"es": "INSC. EST."
	},
	"RECEIVER": {
		"pt": "DESTINATÁRIO",
		"en": "RECEIVER",
		"es": "RECEPTOR"
	},
	"COD": {
		"pt": "CÓD.",
		"en": "COD.",
		"es": "CÓD."
	},
	"DESCRIPTION": {
		"pt": "DESCRIÇÃO",
		"en": "DESCRIPTION",
		"es": "DESCRIPCIÓN"
	},
	"QUANT": {
		"pt": "QUANT.",
		"en": "QUANT.",
		"es": "CANT."
	},
	"UNITARY": {
		"pt": "UNITÁRIO",
		"en": "UNITARY",
		"es": "UNIDAD"
	},
	"TOTAL": {
		"pt": "TOTAL",
		"en": "TOTAL",
		"es": "TOTAL"
	},
	"NOTES": {
		"pt": "ANOTAÇÕES",
		"en": "NOTES",
		"es": "NOTAS"
	},
	"BANKDATA": {
		"pt": "DADOS BANCÁRIOS",
		"en": "BANK DATA",
		"es": "DATOS BANCARIOS"
	},
	"CA": {
		"pt": "C/C",
		"en": "C/A",
		"es": "C/C"
	},
	"AG": {
		"pt": "AG.",
		"en": "AG.",
		"es": "AG."
	},
	"TOTALVALUEBYEXTENSION": {
		"pt": "VALOR TOTAL POR EXTENSO",
		"en": "TOTAL VALUE BY EXTENSION",
		"es": "VALOR TOTAL POR EXTENSO"
	},
	"TOTALGENERALOFTHENOTE": {
		"pt": "TOTAL GERAL DA NOTA",
		"en": "TOTAL GENERAL OF THE NOTE",
		"es": "TOTAL GENERAL DE LA NOTA"
	},
	"City": {
		"pt": "Cidade",
		"en": "City",
		"es": "Ciudad"
	},
	"DebitNoteInformation": {
		"pt": "Informações da Nota de Débito",
		"en": "Debit Note Information",
		"es": "Información de la nota de débito"
	},
	"IssuanceDate": {
		"pt": "Data Emissão",
		"en": "Issuance Date",
		"es": "Fecha de Emisión"
	},
	"DueDate": {
		"pt": "Data Vencimento",
		"en": "Due Date",
		"es": "Fecha de Vencimiento"
	},
	"Notes": {
		"pt": "Anotações",
		"en": "Notes",
		"es": "Notas"
	},
	"SendTo": {
		"pt": "Enviar Para",
		"en": "Send to",
		"es": "Enviar Para"
	},
	"App": {
		"pt": "App",
		"en": "App",
		"es": "App"
	},
	"Web": {
		"pt": "Web",
		"en": "Web",
		"es": "Web"
	},
	"CreatedBy": {
		"pt": "Criado Por",
		"en": "Created By",
		"es": "Creado Por"
	},
	"TypePayment": {
		"s": {
			"pt": "Tipo de Pagamento",
			"en": "Type Payment",
			"es": "Tipo de Pago"
		},
		"p": {
			"pt": "Tipos de Pagamento",
			"en": "Payment Types",
			"es": "Tipos de Pago"
		}
	},
	"TypeLaunch": {
		"s": {
			"pt": "Tipo do lançamento",
			"en": "Type launch",
			"es": "Tipo de liberación"
		},
		"p": {
			"pt": "Tipos do lançamento",
			"en": "Launch Types",
			"es": "Tipos de liberación"
		}
	},
	"PaymentTypeData": {
		"pt": "Dados do Tipo de Pagamento",
		"en": "Payment Type Data",
		"es": "Datos del Tipo de Pago"
	},
	"ConsumeBalance": {
		"pt": "Consome Saldo",
		"en": "Consume Balance",
		"es": "Consumo Saldo"
	},
	"PaymentIdentifier": {
		"pt": "Identificador de Pagamento",
		"en": "Payment Identifier",
		"es": "Identificador de Pago"
	},
	"RequestIdentifier": {
		"pt": "Solicita Identificador",
		"en": "Request Identifier",
		"es": "Solicita Identificador"
	},
	"Mask": {
		"pt": "Máscara",
		"en": "Mask",
		"es": "Máscara"
	},
	"CostCenterOrigin": {
		"pt": "Origem Centro de Custo",
		"en": "Origin Cost Center",
		"es": "Centro de costos de origen"
	},
	"IdentifierMask": {
		"pt": "Máscara do Identificador",
		"en": "Identifier Mask",
		"es": "Máscara del identificador"
	},
	"CardExtract": {
		"pt": "Extrato do Cartão",
		"en": "Card Extract",
		"es": "Extracto de tarjeta"
	},
	"Extract": {
		"pt": "Extrato",
		"en": "Extract",
		"es": "Extrato"
	},
	"Credit": {
		"pt": "Crédito",
		"en": "Credit",
		"es": "Crédito"
	},
	"Balance": {
		"pt": "Saldo",
		"en": "Balance",
		"es": "Saldo"
	},
	"OutstandingBalance": {
		"pt": "Pendente de Aprovação",
		"en": "Pending Approval",
		"es": "Aprobación pendiente"
	},
	"BalanceAvailable": {
		"pt": "Saldo Disponível",
		"en": "Balance Available",
		"es": "Saldo Disponible"
	},
	"Info": {
		"pt": "Info.",
		"en": "Info.",
		"es": "Info."
	},
	"Debit": {
		"pt": "Débito",
		"en": "Debito",
		"es": "Débito"
	},
	"Approved": {
		"pt": "Aprovado",
		"en": "Approved",
		"es": "Aprobrado"
	},
	"Pending": {
		"pt": "Pendente",
		"en": "Pending",
		"es": "Pendiente"
	},
	"Disapproved": {
		"pt": "Reprovado",
		"en": "Disapproved",
		"es": "Reprobado"
	},
	"CreditAdded": {
		"pt": "Crédito Adicionado",
		"en": "Credit Added",
		"es": "Créditos añadidos"
	},
	"SettlementPending": {
		"pt": "Liquidações Pendente",
		"en": "Settlement Pending",
		"es": "Liquidaciones pendientes"
	},
	"Approve": {
		"pt": "Aprovar",
		"en": "Approve",
		"es": "Aprobar"
	},
	"Disapprove": {
		"pt": "Reprovar",
		"en": "Disapprove",
		"es": "Reprobar"
	},
	"ApprovePendindSettlements": {
		"pt": "Aprovar liquidações pendentes",
		"en": "Approve pending settlements",
		"es": "Aprobar las liquidaciones pendientes"
	},
	"ApproveSettlements": {
		"pt": "Aprova liquidações",
		"en": "Approve settlements",
		"es": "Aprobar las liquidaciones"
	},
	"AllowChangeSettlementStatus": {
		"pt": "Permite modificar o status de liquidação",
		"en": "Allows you to change the settlement status",
		"es": "Permite modificar el estado de liquidación"
	},
	"Point": {
		"pt": "Ponto",
		"en": "Point",
		"es": "Ponto"
	},
	"PointRecordInformation": {
		"pt": "Informações do registro de ponto",
		"en": "Point record information",
		"es": "Información del registro de punto"
	},
	"PointEvents": {
		"pt": "Eventos do ponto",
		"en": "Point events",
		"es": "Eventos del punto"
	},
	"AddEvent": {
		"pt": "Adicionar Evento",
		"en": "Add Event",
		"es": "Añadir Evento"
	},
	"Opened": {
		"pt": "Em aberto",
		"en": "Opened",
		"es": "En abierto"
	},
	"Finalized": {
		"pt": "Finalizado",
		"en": "Finalized",
		"es": "Finalizado"
	},
	"EditEvent": {
		"pt": "Editar Evento",
		"en": "Edit Event",
		"es": "Editar El Evento"
	},
	"RequestKm": {
		"pt": "Solicitar Km",
		"en": "Request Km",
		"es": "Solicitar Km"
	},
	"KMStart": {
		"pt": "Km Início",
		"en": "Km Start",
		"es": "Km Inicio"
	},
	"FinalKm": {
		"pt": "Km Final",
		"en": "Final Km",
		"es": "Km Final"
	},
	"KmTraveled": {
		"pt": "Km Percorrido",
		"en": "Km Traveled",
		"es": "Km Recorridos"
	},
	"Final": {
		"pt": "Final",
		"en": "Final",
		"es": "Final"
	},
	"ReportInformation": {
		"pt": "Informações do laudo",
		"en": "Report Information",
		"es": "Información del laudo"
	},
	"Feedback": {
		"pt": "Parecer",
		"en": "Feedback",
		"es": "Opinion"
	},
	"TechnicalAnalysis": {
		"pt": "Análise técnico",
		"en": "Technical analysis",
		"es": "Análisis técnico"
	},
	"Conclusion": {
		"pt": "Conclusão",
		"en": "Conclusion",
		"es": "Conclusión"
	},
	"Report": {
		"pt": "Laudo",
		"en": "Report",
		"es": "Laudo"
	},
	"UserNotes": {
		"pt": "Observações do usuário: {note}",
		"en": "User notes: {note}",
		"es": "Observaciones del usuario: {note}"
	},
	"TotalOutstandingBalance": {
		"pt": "Total Pendente",
		"en": "Total Pending",
		"es": "Total Pendiente"
	},
	"TotalBalanceAvailable": {
		"pt": "Total Disponível",
		"en": "Total Available",
		"es": "Total Disponible"
	},
	"TotalBalanceDisapproved": {
		"pt": "Total Reprovado",
		"en": "Total Disapproved",
		"es": "Total Reprobado"
	},
	"Project": {
		"p": {
			"pt": "Projetos",
			"en": "Projects",
			"es": "Proyectos"
		},
		"s": {
			"pt": "Projeto",
			"en": "Project",
			"es": "Proyecto"
		}
	},
	"Tool": {
		"p": {
			"pt": "Ferramentas",
			"en": "Tools",
			"es": "Instrumentos"
		},
		"s": {
			"pt": "Ferramenta",
			"en": "Tool",
			"es": "Instrumento"
		}
	},
	"TotalBalanceRefund": {
		"pt": "Total Reembolsável",
		"en": "Total Refundable",
		"es": "Total Reembolsable"
	},
	"MainCostCenter": {
		"pt": "Centro de custo principal",
		"en": "Main cost center",
		"es": "Centro de costos principal"
	},
	"parameters": {
		"pt": "Parâmetros",
		"en": "Parameters",
		"es": "Parámetros"
	},
	"ListSelection": {
		"pt": "Seleção da lista",
		"en": "List Selection",
		"es": "Selección de lista"
	},
	"DeleteOS": {
	  "pt": "Excluir Ordem de Serviço",
	  "en": "Delete Service",
	  "es": "Eliminar Orden de Servicio"
	},
	"Schedule": {
	    "p": {
	        "pt": "Agendas",
            "en": "Schedules",
            "es": "Calendarios"
	    },
	    "s": {
	        "pt": "Agenda",
            "en": "Schedule",
            "es": "Calendario"
	    }
	},
	"ScheduleData": {
		"pt": "Dados da Agenda",
		"en": "Schedule Data",
		"es": "Datos de la Agenda",
	}
};

var messages = {
		
	"fillInAllFields" : {
		"pt": "Preencha todos os campos",
		"en": "Fill in all fields",
		"es": "Rellena todos los campos"
	},
		
	"ConfirmExpenseDebit": {
		"pt": "Confirmar débito de despesa?",
		"en": "Confirm expense debit?",
		"es": "Confirmar débito de gastos?"
	},
		
	"ConfirmFinish": {
	  "pt": "Confirmar finalização?",
	  "en": "Confirm checkout?",
	  "es": "Confirmar pago?"
	},
	
	"downloadOrImport": {
	  "pt": "Download de modelo ou importação",
	  "en": "Template download or import",
	  "es": "Descarga o importación de plantillas"
	},
	
  "confirmDeleteImports": {
	  "pt": "Confirmar deleção do Import?",
	  "en": "Confirm deletion of Import?",
	  "es": "¿Confirmar la eliminación de Importar?"	
  },
  
	"dayLimitExceeded":{
		"pt": "Limite de dias excedido",
		"en": "Day limit exceeded",
		"es": "Límite de día excedido"
	},
	
  "veryDistantDates": {
    "pt": "Datas muito distantes",
    "en": "Very distant dates",
    "es": "Fechas muy lejanas"
  },

  "loadProjectsError": {
    "pt": "Erro ao carregar os projetos, verifique! Erro: ",
    "en": "Error loading projects, check this! Error: ",
    "es": "Error al cargar proyectos, verifique! Error: "
  },

  "howManyEquipment": {
    "pt": "Quantos equipamentos compõem o projeto?",
    "en": "How many pieces of equipment make up the project?",
    "es": "Cuántos equipos componen el proyecto?"
  },

  "deliveryTermTitle": {
    "pt": "TERMO DE ENTREGA E RESPONSABILIDADE DE CARTÃO PARA USO CORPORATIVO",
    "en": "DELIVERY AGREEMENT AND LIABILITY CARD FOR CORPORATE USE",
    "es": "ACUERDO DE ENTREGA Y TARJETA DE RESPONSABILIDAD PARA USO CORPORATIVO"
  },

  "deliveryTermPrimaryMessage": {
    "pt": "Pelo presente termo o funcionário (NOME), CPF (CPF) declara ter recebido da empresa (NOME EMPRESA), situada no endereço (ENDERECO), inscrita no CNPJ: (CNPJ), o cartão abaixo descrito:",
    "en": "By this term the employee (NOME), CPF (CPF) declares to have received from the company (NOME EMPRESA), located at the address (ENDERECO), registered with the CNPJ: (CNPJ), the following card:",
    "es": "Por este término, el empleado (NOME), CPF (CPF) declara haber recibido de la compañía (NOME EMPRESA), ubicada en (ENDERECO), registrada en el CNPJ: (CNPJ), la siguiente tarjeta:"
  },

  "erroToConvertDateToStr": {
    "pt": "Erro ao converter a data",
    "en": "Error converting date",
    "es": "Error al convertir la fecha"
  },

  "ErrorLoadingApiCard": {
    "pt": "Erro ao carregar cartão da api",
    "en": "Error loading api card",
    "es": "Error al cargar la tarjeta api"
  },

  "deliveryTermSecondMessage": {
    "pt": "O funcionário acima indicado, doravante denominado (USUARIO), declara-se ciente das condições de uso do cartão, conforme abaixo:",
    "en": "The aforementioned employee, hereinafter referred to as (USUARIO), declares to be aware of the conditions of use of the card, as follows:",
    "es": "El empleado antes mencionado, en adelante denominado (USUARIO), declara conocer las condiciones de uso de la tarjeta, de la siguiente manera:"
  },

  "deliveryTermCardDetails": {
    "pt": "numero: (NUMERO CARTAO)<br/>" +
      "bandeira: (BANDEIRA)<br/>" +
      "banco emissor: (BANCO EMISSOR)",
    "en": "number: (NUMERO CARTAO) <br/> " +
      "flag: (BANDEIRA) <br/>" +
      "issuing bank: (BANCO EMISSOR) <br/>",
    "es": "número: (NUMERO CARTAO) <br/> " +
      "bandera: (BANDEIRA) <br/>" +
      "banco emisor: (BANCO EMISSOR)"
  },

  "deliveryTermCondition": {
    "pt": "1- O (USUARIO) declara-se ciente de que recebe o cartão para utilização exclusiva a serviço da empresa, tendo em vista a necessidade decorrente da atividade exercida pelo (USUARIO)<br/>" +
      "2- O (USUARIO) é a única pessoa autorizada e responsável pela utilização e conservação do cartão acima descrito, sendo vedada qualquer hipótese de cessão ou empréstimo do cartão a terceiros<br/>" +
      "3- Ao término da prestação de serviços ou do contrato individual de trabalho, o (USUARIO) compromete-se a devolver o cartão em perfeito estado, por ocasião da comunicação do desligamento, independentemente de quem tenha partido a iniciativa<br/>" +
      "4- Se o cartão for danificado ou inutilizado por emprego inadequado, mau uso, negligência ou extravio a empresa cobrará o valor de um cartão, estando desde já autorizado o desconto no pagamento do (USUARIO), seja a título de salários ou de sua rescisão, conforme artigo 462, § 1º da CLT.",
    "en": "1- The (USUARIO) declares aware that he receives the card for exclusive use in the service of the company, considering the necessity arising from the activity performed by the (USUARIO) <br/> " +
      "2- The (USUARIO) is the only person authorized and responsible for the use and retention of the card described above, being forbidden any assignment or loan of the card to third parties <br/>" +
      "3- Upon termination of the service or individual employment contract, the (USUARIO) undertakes to return the card in perfect condition upon notification of termination, regardless of who initiated the initiative <br/>" +
      "4- If the card is damaged or rendered unusable by improper use, misuse, neglect or mishandling, the company will charge the value of a card, being already authorized the discount on the payment of the (USUARIO), either as salaries or his termination, pursuant to article 462, § 1 of the CLT.",
    "es": "1- El (USUARIO) declara consciente de que recibe la tarjeta para uso exclusivo en el servicio de la empresa, teniendo en cuenta la necesidad derivada de la actividad realizada por el (USUARIO) <br/> " +
      "2- El (USUARIO) es la única persona autorizada y responsable del uso y retención de la tarjeta descrita anteriormente, quedando prohibida cualquier cesión o préstamo de la tarjeta a terceros <br/>" +
      "3- Al rescindir el servicio o el contrato de trabajo individual, el (USUARIO) se compromete a devolver la tarjeta en perfectas condiciones, previa notificación de rescisión, independientemente de quién inició la iniciativa <br/>" +
      "4- Si la tarjeta se daña o queda inutilizable por mal uso, mal uso, negligencia o mal manejo, la empresa cobrará el valor de una tarjeta, ya que está autorizada para pagar el pago (USUARIO), ya sea como salario o terminación, de conformidad con el artículo 462, § 1 del CLT."
  },

  "deliveryTermAccordingEnclosure": {
    "pt": "Declaro estar ciente e de acordo com as cláusulas acima.",
    "en": "I declare to be aware and in accordance with the above clauses.",
    "es": "I declare to be aware and in accordance with the above clauses."
  },

  "errorLoadingCardReleases": {
    "pt": "Erro ao carregar os lançamentos de cartão",
    "en": "Error loading card releases",
    "es": "Error al cargar lanzamientos de tarjetas"
  },

  "formattedDate": {
    "pt": "(DIA) de (MES) de (ANO)",
    "en": "(MES) (DIA), (ANO)",
    "es": "(DIA) de (MES) de (ANO)"
  },

  "Months": {
    "0": {
      "pt": "Janeiro",
      "en": "January",
      "es": "Enero"
    },
    "1": {
      "pt": "Fevereiro",
      "en": "February",
      "es": "Febrero"
    },
    "2": {
      "pt": "Março",
      "en": "March",
      "es": "Marzo"
    },
    "3": {
      "pt": "Abril",
      "en": "April",
      "es": "Abril"
    },
    "4": {
      "pt": "Maio",
      "en": "May",
      "es": "Mayo"
    },
    "5": {
      "pt": "Junho",
      "en": "June",
      "es": "Junio"
    },
    "6": {
      "pt": "Julho",
      "en": "July",
      "es": "Julio"
    },
    "7": {
      "pt": "Agosto",
      "en": "August",
      "es": "Agosto"
    },
    "8": {
      "pt": "Setembro",
      "en": "September",
      "es": "Septiembre"
    },
    "9": {
      "pt": "Outubro",
      "en": "October",
      "es": "Octubre"
    },
    "10": {
      "pt": "Novembro",
      "en": "November",
      "es": "Noviembre"
    },
    "11": {
      "pt": "Dezembro",
      "en": "December",
      "es": "Diciembre"
    }
  },


  "ApprovedGreaterThanZero": {
    "pt": "Valor aprovado precisa ser maior que zero",
    "en": "Approved value must be greater than zero",
    "es": "El valor aprobado debe ser mayor que cero"
  },

  "ApprovedAmountMustBeLess": {
    "pt": "Valor aprovado precisa ser menor/igual valor da despesa",
    "en": "Approved amount must be less/equal expense amount",
    "es": "El monto aprobado debe ser un monto de gastos menor/igual"
  },

  "YouMustSelectCostCenter": {
    "pt": "É preciso selecionar um Centro de Custo para confirmar!",
    "en": "You must select a Cost Center to confirm!",
    "es": "¡Debe seleccionar un Centro de costos para confirmar!"
  },

  "YouMustSelectObjectType": {
    "pt": "É preciso selecionar um tipo de object para confirmar!",
    "en": "You must select a object type to confirm!",
    "es": "¡Debe seleccionar un tipo de objeto para confirmar!"
  },

  "selectCard": {
    "pt": "-- Selecione Cartão --",
    "en": "-- Select Card --",
    "es": "-- Seleccionar tarjeta --"
  },

  "selectReferenceTable": {
    "pt": "Selecione a Tabela de Referência",
    "en": "Select Reference Table",
    "es": "Selecione la tabla de Referencia"
  },

  "fillInTheFields": {
    "pt": "Preencha os campos",
    "en": "Fill in the fields",
    "es": "Rellena los campos"
  },

  "youMustSelectNewCard": {
    "pt": "Você tem que selecionar um novo cartão",
    "en": "Tienes que seleccionar una nueva tarjeta",
    "es": "You have to select a new card"
  },
  "SelectCard": {
    "pt": "-- Selecionar o cartão --",
    "en": "-- Select card --",
    "es": "-- Seleccionar tarjetal --"
  },

  "SelectCardMain": {
    "pt": "-- Selecionar o cartão principal --",
    "en": "-- Select main card --",
    "es": "-- Seleccionar tarjeta principal --"
  },

  "YouMustSelectCostCenter": {
    "pt": "É preciso selecionar um Centro de Custo para confirmar!",
    "en": "You must select a Cost Center to confirm!",
    "es": "¡Debe seleccionar un Centro de costos para confirmar!"
  },

  "operationAborted": {
    "pt": "Operação Abortada",
    "en": "Operation Aborted",
    "es": "Operación Abortada"
  },

  "expenseStatementInformation": {
    "pt": "Informação do Demonstrativo Despesa",
    "en": "Expense Statement Information",
    "es": "Información del estado de gastos"
  },

  "addLogo": {
    "pt": "Adicionar logo",
    "en": "Add logo",
    "es": "Añadir logo"
  },

  "confirmDeleteLogo": {
    "pt": "Confirmar exclusão da logo",
    "en": "Confirm Logo Deletion",
    "es": "Confirmar eliminación de logotipo"
  },

  "addedCredit": {
    "pt": "Crédito Adicionado",
    "en": "Added Credit",
    "es": "Crédito agregado",
  },

  "addedDebit": {
    "pt": "Débito Adicionado",
    "en": "Added Debit",
    "es": "Deuda agregada",
  },

  "addresshint": {
    "pt": "RUA NOME_RUA, 10, BAIRRO - CIDADE, ESTADO",
    "en": "STREET NAME_STREET, 10, NEIGHBORHOOD - CITY, STATE",
    "es": "CALLE NOMBRE_CALLE, 10, BARRIO - CIUDAD, ESTADO"
  },

  "listEmpity": {
    "pt": "Lista vazia",
    "en": "List empity",
    "es": "lista vacía"
  },

  "ConfirmImgEdit": {
    "pt": "Confirmar edição da imagem?",
    "en": "Confirm image editing?",
    "es": "Confirmar la edición de la imagen?"
  },
  "ErrorLoadingExpensesNature": {
    "pt": "Erro ao carregar as Natureza de Gastos...",
    "en": "Error loading Expense Nature ...",
    "es": "Error al cargar Expense Nature ..."
  },
  "ErrorLoadingExpenseNature": {
    "pt": "Erro ao carregar a Natureza de Gasto...",
    "en": "Error loading Expense Nature ...",
    "es": "Error al cargar Expense Nature ..."
  },
  "SelectNature": {
    "pt": "-- Selecione natureza --",
    "en": "-- Select nature --",
    "es": "-- Seleccionar naturaleza --"
  },
  "SelectCostCenter": {
    "pt": "-- Selecione o centro de custo --",
    "en": "-- Select cost center --",
    "es": "-- Seleccionar centro de costos --"
  },
  "SelectObjectType": {
    "pt": "-- Selecione o tipo de objeto --",
    "en": "-- Select object type --",
    "es": "-- Seleccionar tipo de objeto --"
  },
  "ErrorLoadData": {
    "pt": "Erro ao carregar dados!",
    "en": "Error loading data!",
    "es": "¡Error al cargar datos!"
  },
  "ErrorSavingData": {
    "pt": "Erro ao salvar dados! Erro: ",
    "en": "Error saving data! Error: ",
    "es": "¡Error al guardar los datos! Error: "
  },

  "ErrorSavingDataWithoutParam": {
    "pt": "Erro ao salvar dados!",
    "en": "Error saving data!",
    "es": "¡Error al guardar los datos!"
  },
  "ErrorDeletingData": {
    "pt": "Erro ao excluir dados. \n Erro: ",
    "en": "Error deleting data. \n Error: ",
    "es": "Error al eliminar datos. \n Error:"
  },
  "DataSavedSuccessfully": {
    "pt": "Dados salvos com sucesso!",
    "en": "Data saved successfully!",
    "es": "¡Datos guardados con éxito!"
  },
  "ErrorLoadAction": {
    "s": {
      "pt": "Erro ao carregar Atividade!",
      "en": "Error loading Action!",
      "es": "¡Error al cargar la actividad!"
    },
    "p": {
      "pt": "Erro ao carregar Atividades!",
      "en": "Error loading Actions!",
      "es": "¡Error al cargar las actividades!"
    }
  },
  "ActionCreatedSuccessfully": {
    "pt": "Atividade criada com sucesso",
    "en": "Action created successfully",
    "es": "Actividad creada con éxito"
  },
  "ErrorCreatingAction": {
    "pt": "Erro ao criar Atividade! Erro: ",
    "en": "Error creating Action! Error: ",
    "es": "¡Error al crear la Actividad! Error: "
  },
  "EnterYourUsernameAndPassword": {
    "pt": "informe seu usuário e senha para acessar",
    "en": "enter your username and password to access",
    "es": "Ingrese su nombre de usuario y contraseña para acceder"
  },
  "SelectEquipmentType": {
    "pt": "Selecione o Tipo de Equipamento",
    "en": "Select Equipment Type",
    "es": "Seleccione el tipo de equipo"
  },
  "SelectFieldType": {
    "pt": "Selecione o Tipo do campo",
    "en": "Select Equipment Type",
    "es": "Seleccione el tipo de equipo"
  },
  "CustomerCreated": {
    "negrao": {
      "pt": "Unidade criada com sucesso!",
      "en": "Unit created successfully!",
      "es": "¡Unidad creada con éxito!"
    },
    "raizen": {
      "pt": "Unidade criada com sucesso!",
      "en": "Unit created successfully!",
      "es": "¡Unidad creada con éxito!"
    },
    "pampili": {
      "pt": "Unidade criada com sucesso!",
      "en": "Unit created successfully!",
      "es": "¡Unidad creada con éxito!"
    },
    "pecompe": {
      "pt": "Unidade criada com sucesso!",
      "en": "Unit created successfully!",
      "es": "¡Unidad creada con éxito!"
    },
    "zenega": {
      "pt": "Cliente/Projeto criada com sucesso!",
      "en": "Customer/Project created successfully!",
      "es": "¡Cliente/Proyecto creada con éxito!"
    },
    "pt": "Cliente criado com sucesso!",
    "en": "Customer created successfully!",
    "es": "¡Cliente creado con éxito!"
  },
  "ErrorCreatedCustomer": {
    "pt": "Erro ao criar cadastro. Erro: ",
    "en": "Error creating record. Error: ",
    "es": "Error al crear el registro. Error: "
  },
  "ErrorCreatedVehicle": {
    "pt": "Erro ao criar veículo",
    "en": "Error creating vehicle",
    "es": "Error al crear un vehículo"
  },
  "CustomerSuccessfullyUpdated": {
    "negrao": {
      "pt": "Unidade companyName atualizado com sucesso!",
      "en": "Unit companyName successfully updated!",
      "es": "¡Unidad companyName actualizado con éxito!"
    },
    "raizen": {
      "pt": "Unidade companyName atualizado com sucesso!",
      "en": "Unit companyName successfully updated!",
      "es": "¡Unidad companyName actualizado con éxito!"
    },
    "pampili": {
      "pt": "Unidade companyName atualizado com sucesso!",
      "en": "Unit companyName successfully updated!",
      "es": "¡Unidad companyName actualizado con éxito!"
    },
    "pecompe": {
      "pt": "Unidade companyName atualizado com sucesso!",
      "en": "Unit companyName successfully updated!",
      "es": "¡Unidad companyName actualizado con éxito!"
    },
    "zenega": {
      "pt": "Cliente/Projeto companyName atualizado com sucesso!",
      "en": "Customer/Project companyName successfully updated!",
      "es": "¡Cliente/Proyecto companyName actualizado con éxito!"
    },
    "pt": "Cliente companyName atualizado com sucesso!",
    "en": "Customer companyName successfully updated!",
    "es": "¡Cliente companyName actualizado con éxito!"
  },
  "ErrorUpdatingClient": {
    "negrao": {
      "pt": "Erro ao atualizar unidade ",
      "en": "Error updating unit ",
      "es": "Error al actualizar la unidad "
    },
    "raizen": {
      "pt": "Erro ao atualizar unidade ",
      "en": "Error updating unit ",
      "es": "Error al actualizar la unidad "
    },
    "pampili": {
      "pt": "Erro ao atualizar unidade ",
      "en": "Error updating unit ",
      "es": "Error al actualizar la unidad "
    },
    "pecompe": {
      "pt": "Erro ao atualizar unidade ",
      "en": "Error updating unit ",
      "es": "Error al actualizar la unidad "
    },
    "zenega": {
      "pt": "Erro ao atualizar cliente/projeto ",
      "en": "Error updating customer/project ",
      "es": "Error al actualizar lo cliente/proyecto "
    },
    "pt": "Erro ao atualizar cliente ",
    "en": "Error updating customer ",
    "es": "Error al actualizar al cliente "
  },
  "VehicleCanNotBeRemoved": {
    "pt": "O veículo não pode ser removido pois está sendo utilizado!",
    "en": "The vehicle can not be removed because it is being used!",
    "es": "¡El vehículo no puede ser retirado porque está siendo utilizado!"
  },
  "ErrorDeletingVehicle": {
    "pt": "Erro ao excluir veículo. \n Erro: ",
    "en": "Error deleting vehicle. \n Error: ",
    "es": "Error al borrar vehículo. \n Error:"
  },
  "ErrorLoadingServices": {
    "pt": "Erro ao carregar serviços. \n ",
    "en": "Error loading services. \n ",
    "es": "Error al cargar servicios. \n"
  },
  "ErrorLoadingUsers": {
    "pt": "Erro ao carregar usuários!",
    "en": "Error loading users!",
    "es": "Error al cargar usuarios!"
  },
  "ErrorLoadingEvents": {
    "pt": "Erro ao carregar eventos!",
    "en": "Error loading events!",
    "es": "Error al cargar eventos!"
  },
  "ErrorLoadingPaymentTypes": {
    "pt": "Erro ao carregar os tipos de pagamento!",
    "en": "Error loading payment types!",
    "es": "¡Error al cargar los tipos de pago!"
  },
  "ErrorLoadingClients": {
    "negrao": {
      "pt": "Erro ao carregar unidades!",
      "en": "Error loading units!",
      "es": "Error al cargar unidades!"
    },
    "raizen": {
      "pt": "Erro ao carregar unidades!",
      "en": "Error loading units!",
      "es": "Error al cargar unidades!"
    },
    "pampili": {
      "pt": "Erro ao carregar unidades!",
      "en": "Error loading units!",
      "es": "Error al cargar unidades!"
    },
    "pecompe": {
      "pt": "Erro ao carregar unidades!",
      "en": "Error loading units!",
      "es": "Error al cargar unidades!"
    },
    "zenega": {
      "pt": "Erro ao carregar clientes/projetos!",
      "en": "Error loading customers/projects!",
      "es": "Error al cargar clientes/proyectos!"
    },
    "pt": "Erro ao carregar clientes!",
    "en": "Error loading customers!",
    "es": "Error al cargar clientes!"
  },
  "ErrorLoadingTimeline": {
    "pt": "Erro ao carregar dados de linha do tempo. \n",
    "en": "Error loading data from the timeline. \n",
    "es": "Error al cargar datos de línea de tiempo. \n"
  },
  "ErrorLoadingEquipment": {
    "s": {
      "pt": "Erro ao carregar equipamento!",
      "en": "Error loading equipment!",
      "es": "¡Error al cargar el equipo!"
    },
    "p": {
      "pt": "Erro ao carregar equipamentos!",
      "en": "Error loading equipments!",
      "es": "¡Error al cargar el equipos!"
    }
  },
  "EquipmentRecordedSuccessfully": {
    "pt": "Equipamento gravado com sucesso!",
    "en": "Equipment recorded successfully!",
    "es": "¡Equipo grabado con éxito!"
  },
  "ErrorWritingEquipment": {
    "pt": "Erro ao gravar equipamento! Erro: ",
    "en": "Error writing equipment! Error: ",
    "es": "¡Error al grabar equipo! de error:"
  },
  "AlertEquipmentNotFound": {
	"pt": "Equipamento não encontrado, para o identificador: ",
    "en": "Type of equipment not found, for identifier",
    "es": "Tipo de equipo no encontrado, para identificador"
  },
  "ErrorLoadingEvent": {
    "s": {
      "pt": "Erro ao carregar evento!",
      "en": "Error loading event!",
      "es": "Error al cargar evento!"
    },
    "p": {
      "pt": "Erro ao carregar eventos!",
      "en": "Error loading events!",
      "es": "Error al cargar eventos!"
    }
  },
  "EventRecordedSuccessfully": {
    "pt": "Evento gravado com sucesso",
    "en": "Event recorded successfully",
    "es": "Evento registrado con éxito"
  },
  "ErrorWritingEvent": {
    "pt": "Erro ao gravar evento! Erro: ",
    "en": "Error writing event! Error: ",
    "es": "Error al escribir el evento! Error: "
  },
  "ConfirmSaveExpense": {
    "pt": "Confirmar gravar despesa?",
    "en": "Confirm save expense?",
    "es": "Confirmar grabar gastos?"
  },
  "ConfirmExcludeExpense": {
    "pt": "Confirmar excluir despesa?",
    "en": "Confirm exclude expense?",
    "es": "Confirmar excluir gastos?"
  },
  "CancelExpense": {
    "pt": "Cancelar despesa?",
    "en": "Cancel expense?",
    "es": "Cancelar gastos?"
  },
  "NoInformationCanBeEdited": {
    "pt": "Nenhuma informação poderá ser editada após o cancelamento",
    "en": "No information can be edited after the cancellation",
    "es": "Ninguna información podrá ser editada después de la cancelación"
  },
  "ImportAllSelectedSettlements": {
    "pt": "Importar todas as liquidações selecionadas para a despesa?",
    "en": "Import all selected settlements to the expense?",
    "es": "Importar todas las liquidaciones seleccionadas para el gasto?"
  },
  "ConfirmDeleteOS": {
    "pt": "Quer excluir a Ordem de Serviço selecionada?",
    "en": "Do you want to delete the selected Service?",
    "es": "¿Quer excluir a Ordem de Serviço selecionada?"
  },
  "WarningDeleteOS": {
    "pt": "Atenção! Após a exclusão a Ordem de Serviço não poderá ser recuperada.",
    "en": "Warning! After deletion, the Service cannot be recovered.",
    "es": "¡Atención! Después de la eliminación, la orden de servicio no se puede recuperar"
  },
  "ConfirmDeleteImg": {
    "pt": "Quer excluir a imagem?",
    "en": "Do you want to delete the image?",
    "es": "¿Quiere eliminar la imagen?"
  },
  "ConfirmChangeImg": {
    "pt": "Confirmar a troca da imagem?",
    "en": "Confirm the change of image?",
    "es": "¿Confirmar cambio de imagen?"
  },
  "SuccefulChangeImg": {
    "pt": "Imagem trocada com sucesso",
    "en": "Image successfully switched",
    "es": "Imagen intercambiada con éxito"
  },
  "ErrorChangeImg": {
    "pt": "Imagem trocada com sucesso",
    "en": "Image successfully switched",
    "es": "Imagen intercambiada con éxito"
  },
  "ErrorInsertImg": {
    "pt": "Erro na inserção da imagem",
    "en": "Error inserting the image",
    "es": "Error en la inserción de la imagen"
  },
  "SucessInsertImg": {
    "pt": "Sucesso na inserção da imagem",
    "en": "Success in image insertion",
    "es": "Éxito en la inserción de la imagen"
  },
  "ErrorDeleteImg": {
    "pt": "Erro ao excluir imagem",
    "en": "Error deleting photo",
    "es": "Error al eliminar la foto"
  },
  "SucessDeleteImg": {
    "pt": "Sucesso ao excluir imagem",
    "en": "Success deleting the image",
    "es": "Éxito al eliminar la imagen"
  },
  "ErrorWhileLoadingExpense": {
    "s": {
      "pt": "Erro ao carregar despesa!",
      "en": "Error while loading expense!",
      "es": "¡Error al cargar gasto!"
    },
    "p": {
      "pt": "Erro ao carregar despesas!",
      "en": "Error while loading expenses!",
      "es": "Error al cargar gastos!"
    }
  },
  "ExpenseRecordedSuccessfully": {
    "pt": "Despesa gravada com sucesso",
    "en": "Expense recorded successfully",
    "es": "Gasto grabado con éxito"
  },
  "ErrorSavingExpense": {
    "pt": "Erro ao gravar despesa! Erro:",
    "en": "Error saving expense! Error:",
    "es": "¡Error al registrar gastos! de error:"
  },
  "CustomerPeriodInformed": {
    "negrao": {
      "pt": "Unidade e período precisam ser informados!",
      "en": "Unit and period need to be informed!",
      "es": "¡Unidad y período necesitan ser informados!"
    },
    "raizen": {
      "pt": "Unidade e período precisam ser informados!",
      "en": "Unit and period need to be informed!",
      "es": "¡Unidad y período necesitan ser informados!"
    },
    "pampili": {
      "pt": "Unidade e período precisam ser informados!",
      "en": "Unit and period need to be informed!",
      "es": "¡Unidad y período necesitan ser informados!"
    },
    "pecompe": {
      "pt": "Unidade e período precisam ser informados!",
      "en": "Unit and period need to be informed!",
      "es": "¡Unidad y período necesitan ser informados!"
    },
    "pt": "Cliente e período precisam ser informados!",
    "en": "Customer and period need to be informed!",
    "es": "¡El cliente y el período necesitan ser informados!"
  },
  "DateOccurrenceAndUnitValueInformed": {
    "pt": "Data, ocorrência e valor unitário precisam ser informados!",
    "en": "Date, occurrence and unit value need to be informed!",
    "es": "La fecha, la ocurrencia y el valor unitario deben ser informados!"
  },
  "InformedDateIncorrect": {
    "pt": "Data informada incorreta",
    "en": "Informed date incorrect",
    "es": "Fecha informada incorrecta"
  },
  "relExpenseMsgPt1": {
    "pt": "Segue o orçamento descritivo das despesas referente a prestação de servicos de",
    "en": "Below is the descriptive budget of the expenses related to the provision of",
    "es": "Sigue el presupuesto descriptivo de los gastos relativos a la prestación de servicios de"
  },
  "relExpenseMsgPt2": {
    "pt": "assessoria relacionadas aos",
    "en": "advisory services related to",
    "es": "asesoramiento sobre los"
  },
  "relExpenseMsgPt3": {
    "pt": ", alusivo ao período",
    "en": ", referring to the period",
    "es": ", alusivo al período"
  },
  "relExpenseMsgPt4": {
    "pt": "de startDate a endDate juntamente com a nota de debito para pagamento, de",
    "en": "from startDate to endDate together with the debit note for payment",
    "es": "de startDate a endDate junto con la nota de pago para el pago, de"
  },
  "relExpenseMsgPt5": {
    "pt": "acordo com os responsaveis",
    "en": "agreement with those responsible",
    "es": "acuerdo con los responsables"
  },
  "ErrorLoadingSettlements": {
    "pt": "Erro ao carregar liquidações! \n",
    "en": "Error loading settlements! \n",
    "es": "¡Error al cargar liquidaciones! \n"
  },
  "PasswordsAreNotTheSame": {
    "pt": "As senhas não são iguais",
    "en": "Passwords are not the same",
    "es": "Las contraseñas no son similares"
  },
  "ErrorWritingUser": {
    "pt": "Erro ao gravar usuário! \nErro: Usuário já cadastrado",
    "en": "Error writing user! \nError: User already registered",
    "es": "Error al grabar usuario! \nErro: Usuario ya registrado"
  },
  "PasswordChangedSuccessfully": {
    "pt": "Senha alterada com sucesso",
    "en": "Password changed successfully",
    "es": "Contraseña alterada con éxito"
  },
  "ErrorChangingUserPassword": {
    "pt": "Erro ao alterar senha do usuário! Erro: ",
    "en": "Error changing user password! Error:",
    "es": "Error al cambiar la contraseña del usuario! Error:"
  },
  "ErrorWorkHourInvalid": {
    "pt": "Expediente inválido!",
    "en": "Invalid work hours!",
    "es": "Horas de trabajo inválidas!"
  },
  "EnterDueDate": {
    "pt": "Informe a data de vencimento!",
    "en": "Enter due date!",
    "es": "Introduzca la fecha de vencimiento!"
  },
  "FiltrarRegistroPorUsuario": {
    "pt": "Filtrar registro por usuário",
    "en": "Filter by user",
    "es": "Filtrar registro por usuario"
  },
  "FiltrarRegistroPorArea": {
    "pt": "Filtrar registro por área",
    "en": "Filter by area",
    "es": "Filtrar registro por área"
  },
  "MessageInfoExtract": {
    "pt": "Referente ao evento de {event} registrado em {data} para o cliente {client}.\n" +
      "Pagamento realizado com {typePayment}\n" +
      "Lançado por {user} - ID interno {id}\n" +
      "Registro integrado em {createdData}",
    "en": "Referring to {event} event logged on {date} for client {client}.\n" +
      "Payment made with {typePayment}\n" +
      "Released by {user} - Internal ID {id}\n" +
      "Integrated record in {createdData}",
    "es": "En el evento de {event} registrado en {fecha} para el {client}.\n" +
      "Pago realizado con {typePayment}\n" +
      "Lanzado por {user} - ID interno {id}\n" +
      "Registro integrado en {createdData}",
  },
  "NecessaryInformObservation": {
    "pt": "Necessário informar observação",
    "en": "Necessary to inform observation",
    "es": "Necesario informar la observación"
  },
  "StartBiggerThanEnd": {
    "pt": "Início maior que fim",
    "en": "Start bigger than end",
    "es": "Inicio más largo que fin"
  },
  "UserNotMatchPointRecord": {
    "pt": "Usuário logado não corresponde a esse registro de ponto",
    "en": "Logged-in user does not match this point record",
    "es": "El usuario registrado no coincide con este registro de punto"
  },
  "NeedStartTime": {
    "pt": "Horário Inicio precisa ser preenchido",
    "en": "Start time needs to be filled in",
    "es": "Horario inicial necesita ser llenado"
  },
  "ClosePoint": {
    "pt": "Deseja encerrar o ponto",
    "en": "Do you want to close the point",
    "es": "Desea cerrar el punto"
  },
  "OpenEventsWithOutEndTime": {
    "pt": "Os eventos\n {eventos}estão em aberto(sem hora final), preencha-os antes de finalizar o ponto",
    "en": "The events\n {eventos}are open (no end time), fill them out before finalizing the point",
    "es": "Los eventos\n {eventos}están abiertos (sin hora final), llévelos antes de finalizar el punto"
  },
  "and": {
    "pt": "e",
    "en": "and",
    "es": "y"
  },
  "EventOutDate": {
    "pt": "Os eventos\n {eventos}estão fora do período",
    "en": "Events\n {eventos}are out of date",
    "es": "Los eventos\n {eventos}están fuera del período"
  },
  "NeedKm": {
    "pt": "Necessário digitar o valor do ",
    "en": "Required to enter the value of the ",
    "es": "Es necesario introducir el valor "
  },
  "RequiredFields": {
    "pt": "É necessário informar os campos: ",
    "en": "You must enter the fields: ",
    "es": "Es necesario informar a los campos: "
  },
  "ExtractToExel": {
    "pt": "Extrair para excel",
    "en": "Extract to excel",
    "es": "extracto para excel"
  }
};

function getLabel(field) {
  if (field == null || field.trim() == '')
    return '';

  language = localStorage.language;
  client = window.location.hostname.split('.')[0];
  arr = field.split('.')
  if (arr.length == 2)
    return lbls[arr[0]][client] != null ? lbls[arr[0]][client][arr[1]][language] : lbls[arr[0]][arr[1]][language];
  else {
    l = lbls[arr[0]][client] || lbls[arr[0]];

    if (l[language] != null)
      return l[language];
    else
      return l['s'][language] != null ? l['s'][language] : '';
  }
}

function getLabelParameter(json) {
  language = localStorage.language;
  return json[language];
}

function getMessage(message) {
  if (message == null || message.trim() == '')
    return '';

  language = localStorage.language;
  client = window.location.hostname.split('.')[0];
  arr = message.split('.')
  if (arr.length == 2)
    return messages[arr[0]][client] != null ? messages[arr[0]][client][arr[1]][language] : messages[arr[0]][arr[1]][language];
  else {
    l = messages[arr[0]][client] || messages[arr[0]];

    if (l[language] != null)
      return l[language];
    else
      return l['s'][language] != null ? l['s'][language] : '';
  }
}

function replaceMsg(msg, objs) {
  for (var name in objs) {
    msg = msg.replace(name, objs[name]);
  }

  return msg;
}
