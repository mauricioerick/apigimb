appGIMB.filter("serviceDateFilter", function() {
    return function(servicos, dtInicio, dtFinal, texto) {
    	
    	if(!dtInicio || !dtFinal) {
            return servicos;
        }
    	
    	setDataInicioFiltroOS(dtInicio);
    	setDataFinalFiltroOS(dtFinal);
    	setTextoFiltroOS(texto)
    	
    	var filtered = [];
        
        var from_date = parseDate(dtInicio, false);
        var to_date = parseDate(dtFinal, false);
        
        angular.forEach(servicos, function(item) {

        	var startTime = parseDate(item.startTime, false);
        	
            if(startTime >= from_date && startTime <= to_date) {
                filtered.push(item);
            }
        });

        return filtered;
    };
});