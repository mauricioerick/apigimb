appGIMB.factory("tokenInterceptor", function($q, $location, $rootScope){
	if ($rootScope.allServices || $rootScope.scrollTopServiceId) {
        if (!location.path().contains('service')) {
            $rootScope.allServices = null;
            $rootScope.scrollTopServiceId = null;
        }
    }
    if ($rootScope.settlements || $rootScope.scrollTopSettlementId) {
        if (!location.path().contains('settlement')) {
            $rootScope.settlements = null;
            $rootScope.scrollTopSettlementId = null;
        }
	}
	
	return{
		'request': function(config){
			config.headers.Authorization = 'Bearer ' + localStorage.getItem("token");
			config.headers.Role = localStorage.getItem("permissionAccess");
			return config;
		},
		'response': function(response) {
			if(response.status==401) {

				var login = "";
				var url = $location.path();

				login = "/apiGIMB/login";

				$location.path(login);
			}
			
			if (localStorage.getItem("permissionAccess") == "false") {
				$(".bobbyVaiPraCasa").prop('disabled', true);
				$(".bobbyVaiPraCasa").css('display', 'none');
			} else {
				$(".bobbyVaiPraCasa").css('display', '');
				$(".bobbyVaiPraCasa").prop('disabled', false);
			}

			return response;
		},
		'responseError': function(rejection) {
			if(rejection.status==401){

				var login = "/apiGIMB/login";

				$location.path(login);

			}
			return rejection;
		}
	}
});
