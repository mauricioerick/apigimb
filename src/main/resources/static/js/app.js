var appGIMB = angular.module("appGIMB", ['angularTrix', 'ngMaterial', 'ngMessages', 'ngRaven', 'ngRoute', 'ngPrint', 'frapontillo.bootstrap-duallistbox']);

appGIMB.directive('accessPermission', function () {
	return {
		restrict: 'A',
		link: function ($scope, element, attrs) {
			if (attrs.accessPermission === 'display-none') {
				element.attr('display', 'none');
			}
		}
	}
});

appGIMB.directive('rotate', function () {
	return {
		restrict: 'A',
		link: function (scope, element, attrs) {
			scope.$watch(attrs.degrees, function (rotateDegrees) {
				var r = 'rotate(' + rotateDegrees + 'deg)';
				element.css({
					'-moz-transform': r,
					'-webkit-transform': r,
					'-o-transform': r,
					'-ms-transform': r
				});
			});
		}
	}
});

appGIMB.config(function ($httpProvider, $routeProvider, $locationProvider) {

	if (!$httpProvider.defaults.headers.get) {
		$httpProvider.defaults.headers.get = {};
	}

	$httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
	$httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
	$httpProvider.defaults.headers.get['Pragma'] = 'no-cache';

	$routeProvider
		.when("/apiGIMB/client", { templateUrl: '/apiGIMB/view/Client/client.html', controller: 'clientController' })
		.when("/apiGIMB/clientCreate", { templateUrl: '/apiGIMB/view/Client/clientDetail.html', controller: 'clientController' })
		.when("/apiGIMB/clientDetail/:clientId", { templateUrl: '/apiGIMB/view/Client/clientDetail.html', controller: 'clientController' })
		.when("/apiGIMB/op/service", { templateUrl: '/apiGIMB/view/Services/services.html', controller: 'servicesController' })
		.when("/apiGIMB/op/serviceCreate", { templateUrl: '/apiGIMB/view/Services/servicesCreateOrUpdate.html', controller: 'servicesController' })
		.when("/apiGIMB/op/serviceUpdate/:serviceId", { templateUrl: '/apiGIMB/view/Services/servicesCreateOrUpdate.html', controller: 'servicesController' })
		.when("/apiGIMB/op/serviceDetail/:serviceId", { templateUrl: '/apiGIMB/view/Services/servicesDetail.html', controller: 'servicesController' })
		.when("/apiGIMB/op/location", { templateUrl: "/apiGIMB/view/Location/location.html", controller: "locationController" })
		.when("/apiGIMB/point-record", { templateUrl: "/apiGIMB/view/PointRecord/pointRecords.html", controller: "pointRecordController" })
		.when("/apiGIMB/point-record/:pointRecordId", { templateUrl: "/apiGIMB/view/PointRecord/pointRecordDetail.html", controller: "pointRecordController" })
		.when("/apiGIMB/point-record/novo", { templateUrl: "/apiGIMB/view/PointRecord/pointRecordDetail.html", controller: "pointRecordController" })
		.when("/apiGIMB/vehicle", { templateUrl: '/apiGIMB/view/Vehicle/veiculo.html', controller: 'vehicleController' })
		.when("/apiGIMB/vehicleCreate", { templateUrl: '/apiGIMB/view/Vehicle/veiculoCreate.html', controller: 'vehicleController' })
		.when("/apiGIMB/vehicleDetail/:vehicleId", { templateUrl: '/apiGIMB/view/Vehicle/veiculoDetail.html', controller: 'vehicleController' })
		.when("/apiGIMB/login", { templateUrl: '/apiGIMB/view/User/login.html', controller: 'loginController' })
		.when("/apiGIMB/home", { templateUrl: '/apiGIMB/view/HomePage/home.html', controller: 'homeController' })
		.when("/apiGIMB/user", { templateUrl: "/apiGIMB/view/User/user.html", controller: "userController" })
		.when("/apiGIMB/userCreate", { templateUrl: '/apiGIMB/view/User/userDetail.html', controller: 'userController' })
		.when("/apiGIMB/userDetail/:userId", { templateUrl: "/apiGIMB/view/User/userDetail.html", controller: "userController" })
		.when("/apiGIMB/userServices/:userId", { templateUrl: "/apiGIMB/view/User/userServices.html", controller: "userServicesController" })
		.when("/apiGIMB/event", { templateUrl: "/apiGIMB/view/Event/event.html", controller: "eventController" })
		.when("/apiGIMB/eventCreate", { templateUrl: "/apiGIMB/view/Event/eventDetail.html", controller: "eventController" })
		.when("/apiGIMB/eventDetail/:eventId", { templateUrl: "/apiGIMB/view/Event/eventDetail.html", controller: "eventController" })
		.when("/apiGIMB/action", { templateUrl: "/apiGIMB/view/Action/action.html", controller: "actionController" })
		.when("/apiGIMB/actionCreate", { templateUrl: "/apiGIMB/view/Action/actionDetail.html", controller: "actionController" })
		.when("/apiGIMB/actionDetail/:actionId", { templateUrl: "/apiGIMB/view/Action/actionDetail.html", controller: "actionController" })
		.when("/apiGIMB/equipment", { templateUrl: "/apiGIMB/view/Equipment/equipment.html", controller: "equipmentController" })
		.when("/apiGIMB/equipmentCreate", { templateUrl: "/apiGIMB/view/Equipment/equipmentDetail.html", controller: "equipmentController" })
		.when("/apiGIMB/equipmentDetail/:equipmentId", { templateUrl: "/apiGIMB/view/Equipment/equipmentDetail.html", controller: "equipmentController" })
		.when("/apiGIMB/expense", { templateUrl: "/apiGIMB/view/Expense/expense.html", controller: "expenseController" })
		.when("/apiGIMB/expenseCreate", { templateUrl: "/apiGIMB/view/Expense/expenseDetail.html", controller: "expenseController" })
		.when("/apiGIMB/expenseDetail/:expenseId", { templateUrl: "/apiGIMB/view/Expense/expenseDetail.html", controller: "expenseController" })
		.when("/apiGIMB/settlement", { templateUrl: "/apiGIMB/view/Settlement/settlements.html", controller: "settlementController" })
		.when("/apiGIMB/settlementDetail/:settlementId", { templateUrl: "/apiGIMB/view/Settlement/settlementDetail.html", controller: "settlementController" })
		.when("/apiGIMB/settlementSummary", { templateUrl: "/apiGIMB/view/Settlement/settlementSummary.html", controller: "settlementSummary" })
		.when("/apiGIMB/ds/timeline", { templateUrl: "/apiGIMB/view/Dashboards/timeline.html", controller: "dsTimelineController" })
		.when("/apiGIMB/ds/trace", { templateUrl: "/apiGIMB/view/Dashboards/trace.html", controller: "dsTraceController" })
		.when("/apiGIMB/ds/serviceChart", { templateUrl: "/apiGIMB/view/Dashboards/serviceChart.html", controller: "dsServiceChartController" })
		.when("/apiGIMB/ds/offendersChart", { templateUrl: "/apiGIMB/view/Dashboards/offendersChart.html", controller: "dsOffendersChartController" })
		.when("/apiGIMB/ds/inventorysChart", { templateUrl: "/apiGIMB/view/Dashboards/inventoryChart.html", controller: "dsInventorysChartController" })
		.when("/apiGIMB/ds/appointmentsChart", { templateUrl: "/apiGIMB/view/Dashboards/appointmentsChart.html", controller: "dsAppointmentsChartController" })
		.when("/apiGIMB/profile", { templateUrl: "/apiGIMB/view/Profile/profile.html", controller: "profileController" })
		.when("/apiGIMB/profileCreate", { templateUrl: "/apiGIMB/view/Profile/profileDetail.html", controller: "profileController" })
		.when("/apiGIMB/profileDetail/:profileId", { templateUrl: "/apiGIMB/view/Profile/profileDetail.html", controller: "profileController" })
		.when("/apiGIMB/observation", { templateUrl: "/apiGIMB/view/Observation/observation.html", controller: "observationController" })
		.when("/apiGIMB/observationCreate", { templateUrl: "/apiGIMB/view/Observation/observationDetail.html", controller: "observationController" })
		.when("/apiGIMB/observationDetail/:observationId", { templateUrl: "/apiGIMB/view/Observation/observationDetail.html", controller: "observationController" })
		.when("/apiGIMB/object", { templateUrl: "/apiGIMB/view/Iventory/Object/object.html", controller: "objectController" })
		.when("/apiGIMB/objectDetail/:objectId", { templateUrl: "/apiGIMB/view/Iventory/Object/objectDetail.html", controller: "objectController" })
		.when("/apiGIMB/objectType", { templateUrl: "/apiGIMB/view/Iventory/ObjectType/objectType.html", controller: "objectTypeController" })
		.when("/apiGIMB/objectTypeCreate", { templateUrl: "/apiGIMB/view/Iventory/ObjectType/objectTypeDetail.html", controller: "objectTypeController" })
		.when("/apiGIMB/objectTypeDetail/:objectTypeId", { templateUrl: "/apiGIMB/view/Iventory/ObjectType/objectTypeDetail.html", controller: "objectTypeController" })
		.when("/apiGIMB/storage", { templateUrl: "/apiGIMB/view/Iventory/Storage/storage.html", controller: "storageController" })
		.when("/apiGIMB/storageCreate", { templateUrl: "/apiGIMB/view/Iventory/Storage/storageDetail.html", controller: "storageController" })
		.when("/apiGIMB/storageDetail/:storageId", { templateUrl: "/apiGIMB/view/Iventory/Storage/storageDetail.html", controller: "storageController" })
		.when("/apiGIMB/editImage/:objId", { templateUrl: "/apiGIMB/view/editImage/editImage.html", controller: "editImageController" })
		.when("/apiGIMB/editImageService/:objId", { templateUrl: "/apiGIMB/view/editImageService/editImageService.html", controller: "editImageServiceController" })
		.when("/apiGIMB/typePayment", { templateUrl: "/apiGIMB/view/TypePayment/typePayment.html", controller: "typePaymentController" })
		.when("/apiGIMB/typePaymentCreate", { templateUrl: "/apiGIMB/view/TypePayment/typePaymentDetail.html", controller: "typePaymentController" })
		.when("/apiGIMB/typePaymentDetail/:typePaymentId", { templateUrl: "/apiGIMB/view/TypePayment/typePaymentDetail.html", controller: "typePaymentController" })
		.when("/apiGIMB/extract", { templateUrl: "/apiGIMB/view/Extract/extract.html", controller: "extractController" })
		.when("/apiGIMB/extractDetail/:userId", { templateUrl: "/apiGIMB/view/Extract/extractDetail.html", controller: "extractController" })
		.when("/apiGIMB/companyDetail", { templateUrl: "/apiGIMB/view/Company/companyDetail.html", controller: "companyController" })
		.when("/apiGIMB/costCenter", { templateUrl: "/apiGIMB/view/CostCenter/costCenter.html", controller: "costCenterController" })
		.when("/apiGIMB/costCenterCreate", { templateUrl: "/apiGIMB/view/CostCenter/costCenterDetail.html", controller: "costCenterController" })
		.when("/apiGIMB/costCenterDetail/:costCenterId", { templateUrl: "/apiGIMB/view/CostCenter/costCenterDetail.html", controller: "costCenterController" })
		.when("/apiGIMB/natureExpense", { templateUrl: "/apiGIMB/view/NatureExpense/natureExpense.html", controller: "natureExpenseController" })
		.when("/apiGIMB/natureExpenseDetail/:natureExpenseId", { templateUrl: "/apiGIMB/view/NatureExpense/natureExpenseDetail.html", controller: "natureExpenseController" })
		.when("/apiGIMB/natureExpenseCreate", { templateUrl: "/apiGIMB/view/NatureExpense/natureExpenseDetail.html", controller: "natureExpenseController" })
		.when("/apiGIMB/area", { templateUrl: "/apiGIMB/view/Area/area.html", controller: "areaController" })
		.when("/apiGIMB/areaDetail/:areaId", { templateUrl: "/apiGIMB/view/Area/areaDetail.html", controller: "areaController" })
		.when("/apiGIMB/areaCreate", { templateUrl: "/apiGIMB/view/Area/areaDetail.html", controller: "areaController" })
		.when("/apiGIMB/document", { templateUrl: "/apiGIMB/view/Document/document.html", controller: "documentController" })
		.when("/apiGIMB/documentDetail/:documentId", { templateUrl: "/apiGIMB/view/Document/documentDetail.html", controller: "documentController" })
		.when("/apiGIMB/cardDetail/:cardId", { templateUrl: "/apiGIMB/view/Card/cardDetail.html", controller: "cardController" })
		.when("/apiGIMB/card", { templateUrl: "/apiGIMB/view/Card/card.html", controller: "cardController" })
		.when("/apiGIMB/cardCreate", { templateUrl: "/apiGIMB/view/Card/cardDetail.html", controller: "cardController" })
		.when("/apiGIMB/cardExtract", { templateUrl: "/apiGIMB/view/CardExtract/cardExtract.html", controller: "cardExtractController" })
		.when("/apiGIMB/cardExtractDetail/:cardId/:startDate/:endDate", { templateUrl: "/apiGIMB/view/CardExtract/cardExtractDetail.html", controller: "cardExtractController" })
		.when("/apiGIMB/tableParameters", { templateUrl: "/apiGIMB/view/TableParameter/tableParameter.html", controller: "tableParameterController" })
		.when("/apiGIMB/tableParametersDetail/:referenceTable", { templateUrl: "/apiGIMB/view/TableParameter/tableParameterDetail.html", controller: "tableParameterController" })
		.when("/apiGIMB/project", { templateUrl: '/apiGIMB/view/Project/project.html', controller: 'projectController' })
		.when("/apiGIMB/projectDetail/:projectId", { templateUrl: '/apiGIMB/view/Project/projectDetail.html', controller: 'projectDetailController' })
		.when("/apiGIMB/projectCreate", { templateUrl: '/apiGIMB/view/Project/projectDetail.html', controller: 'projectDetailController' })
		.when("/apiGIMB/agenda", { templateUrl: '/apiGIMB/view/Calendar/calendar.html', controller: 'calendarController' })
		.when("/apiGIMB/importXls", { templateUrl: '/apiGIMB/view/ImportXls/importXls.html', controller: 'importXlsController'})
		.when("/apiGIMB/tableForImport", { templateUrl: '/apiGIMB/view/TablesForImport/tablesForImport.html', controller: 'tableForImportController'})
		.when("/apiGIMB/inventorySummary", { templateUrl: "/apiGIMB/view/Iventory/Object/inventorySummary.html", controller: "objectController" })
		.when("/apiGIMB/tool", { templateUrl: "/apiGIMB/view/Tool/tool.html", controller: "toolController" })
		.when("/apiGIMB/toolCreate", { templateUrl: "/apiGIMB/view/Tool/toolDetail.html", controller: "toolController" })
		.when("/apiGIMB/toolDetail/:toolId", { templateUrl: "/apiGIMB/view/Tool/toolDetail.html", controller: "toolController" })
		.when("/apiGIMB/schedule", { templateUrl: "/apiGIMB/view/Agenda/agenda.html", controller: "agendaController" })
		.when("/apiGIMB/scheduleCreate", { templateUrl: "/apiGIMB/view/Agenda/agendaCreate.html", controller: "agendaController", controllerAs: '$ctrl' })
		.when("/apiGIMB/scheduleItemUpdate/:agendaItemId", { templateUrl: "/apiGIMB/view/Agenda/agendaUpdate.html", controller: "agendaController", controllerAs: '$ctrl' })
		.otherwise({ redirectTo: "/apiGIMB/login" });

	$locationProvider.html5Mode(true);
});

appGIMB.config(function ($httpProvider) {
	$httpProvider.interceptors.push("tokenInterceptor");
});

function Ctrl($scope) {
	$scope.rotate = function (angle) { $scope.angle = angle; };
}
