appGIMB.controller("mainController", function($scope, $http, $location, juiceService, $compile, config){
	var client = window.location.hostname.split('.')[0];
	$scope.reportList = {
		'header':'/apiGIMB/view/Report/'+ client +'/Header.html', 
		'headerOnly': '/apiGIMB/view/Report/'+ client +'/HeaderOnly.html', 
		'headerCompany': '/apiGIMB/view/Report/'+ client +'/HeaderCompanyInfo.html', 
		'relExpenseDebitNote': '/apiGIMB/view/Report/'+ client +'/relExpenseDebitNote.html', 
		'relExpenseDetail': '/apiGIMB/view/Report/'+ client +'/relExpenseDetail.html',
		'reportService': '/apiGIMB/view/Report/'+ client +'/reportService.html',
		'reportServiceDefault': '/apiGIMB/view/Services/servicesPrint.html' 
	};

	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');

	$scope.appUrl = "/";
	loadUrlLogo();

	if($location.path().indexOf("apiGIMB") > -1) {
		$scope.appUrl += "apiGIMB/";
	}
	
	$scope.getLabel = function(field) {
		return getLabel(field);
	};
	
	$scope.getMessage = function(message) {
		return getMessage(message);
	};
	
	$scope.replaceMsg = function(msg, objs) {
		return replaceMsg(msg, objs);
	};

	$scope.drawMenuList = function(){
		if ($('#listMenu').html().length == 0) drawMenu($scope, $compile);
	};

	for (const key in $scope.reportList) {
		if ($scope.reportList.hasOwnProperty(key)) {
			doesFileExist($scope.reportList[key], key);
		}
	}
	
	function doesFileExist(fileName, key)
	{
		var urlToFile = fileName;

		var xhr = new XMLHttpRequest();
		xhr.onload = function() {
			if (this.status == 404) {
				$scope.reportList[key] = urlToFile.replace(client, 'default');
			}
		};
		
		xhr.open('HEAD', urlToFile);
		xhr.send();
	}

	function loadUrlLogo() {

		$http.get(config.baseUrl + "/companyImageLogo/")
		.then(function(response){
			$scope.urlLogo = response.data;
		});
	}
});
