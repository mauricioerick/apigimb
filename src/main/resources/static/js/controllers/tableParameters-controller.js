appGIMB.controller("tableParameterController",
	function ($scope, $http, $routeParams, $location, config, juiceService, geralAPI) {
		$('form').attr('autocomplete', 'off');
		$('input[type=text]').attr('autocomplete', 'off');
		if (!controllerHeadCheck($scope, $location, juiceService))
			return;

		$('#divMenu').show();

		// changeScreenHeightSize('#divTableAction');

		numCampos = 1;
		numDivCampos = 1;
		dictF = {};

		$scope.tableParameters = [];

		$scope.optActive = "active";

		$scope.tableSelected = "";

		$scope.currentField = {};

		$scope.setCurrentField = (field) => {
			$scope.currentField = field;
		}

		$scope.listType= [
			{
				value:"Text",
				label:getLabel("Text")
			},
			{
				value:"Number",
				label:getLabel("Number")
			},
			{
				value:"Date",
				label:getLabel("Date")
			}
		];

		$scope.openDetail = () => {
			$('#modalNewTableParameter').modal('hide');
			window.location.href = $scope.appUrl+"tableParametersDetail/"+$scope.tableSelected;
		};

		$scope.removeItem = (field) => {
			var index = $scope.tableParameter.jsonStructure.indexOf(field);
			if(index != -1){
				$scope.tableParameter.jsonStructure.splice(index, 1);
			}
		};
		$scope.addField = () => {
			$("#modalNewField").modal('hide');
			$scope.removeItem($scope.currentField);
			$scope.tableParameter.jsonStructure.push($scope.currentField);
			$scope.currentField={};
		}

		var error = $scope.getMessage('ErrorSavingData');

		if ($routeParams.referenceTable) {
			carregarTableParameter($routeParams.referenceTable);
		} else {
			carregarTableList();
			carregarTableParameters();
		}

		function carregarTableList() {
			$http.get(config.baseUrl + "/admin/listTables").then(
				function (response) {
					$scope.tableList = response.data;
				},
				function (response) {
					alert($scope.getMessage('ErrorLoadData'));
				}
			);
		}

		function carregarTableParameter(refereceTable){
			$http.get(config.baseUrl + "/admin/tableParameterByTableReference/" + refereceTable).then(
				function (response) {
					$scope.tableParameter = response.data;
					
					if($scope.tableParameter == "")
						buildNewTableParameter(refereceTable);
					
					else 
						$scope.tableParameter.jsonStructure = JSON.parse($scope.tableParameter.jsonStructure);
				},
				function (response) {
					alert($scope.getMessage('ErrorLoadData'));
				}
			);
		}
		
		function buildNewTableParameter(tableReference) {
			const now = new Date();
			const createDate = now.getDate() + "/" + now.getMonth() + 1 + "/" + now.getFullYear();
			const createUser = JSON.parse(localStorage.getItem("bobby"));
			const jsonStructure = [];
			
			$scope.tableParameter = {tableReference, createDate, createUser, jsonStructure};
		}

		function carregarTableParameters() {
			$http.get(config.baseUrl + "/admin/tableParameterByActive/true").then(
				function (response) {
					$scope.tableParameters = response.data;
				},
				function (response) {
					alert($scope.getMessage('ErrorLoadData'));
				}
			);
		}

		$scope.saveTableParameter = function (obj) {
			if (!validateForm())
				return;

			var url = config.baseUrl + "/admin/createTableParameter";

			$http.put(url, obj).then(
				function (response) {
					if (response.status >= 200 && response.status <= 299) {
						alert($scope.getMessage('DataSavedSuccessfully'));
						refreshPage();
						$location.path(juiceService.appUrl + 'action');
					} else {
						refreshPage();
						alert(error + response.status + "\n" + response.message);
					}
				},
				function (response) {
					refreshPage();
					alert(error + response.status + "\n" + response.message);
				}
			);
		};
		
		$scope.viewTableParameter = (tableParameter) => {
			return (tableParameter.jsonStructure.length > 0) && (tableParameter.jsonStructure != "[]");
		}

	}
);