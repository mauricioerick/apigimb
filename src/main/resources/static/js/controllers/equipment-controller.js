appGIMB.controller("equipmentController", 
	function($scope, $http, $routeParams, $location, config, juiceService, geralAPI, values, $compile) {

		$('form').attr('autocomplete', 'off');
		$('input[type=text]').attr('autocomplete', 'off');
		
		if (!controllerHeadCheck($scope, $location, juiceService))
			return;

		changeScreenHeightSize('#divTableEquipment')

		$scope.checkedUncheckedAll = false;
		$scope.equipment = { active : true };
		$scope.equipment.colorId = getRandomColor();
		colorSelectorConf('colorSel', $scope.equipment.colorId)
		$scope.newObjectType = "";
		$scope.objectTypesToSelect = [];
		$scope.equipments = [];
		$scope.allEquipments = [];
		$scope.checklists = [];
		$scope.checklistServices = [];
		$scope.checklistService = {};
		$scope.checklistItems = [];
		$scope.checklistItem = {};
		$scope.objectTypesSelected = [];

		$scope.optActive = "active";

		var requiredPhotos = {
			'ok': false,
			'nok': false,
			'fix': false
		}

		dictI = {};
		dictC = {};
		var configui = { expNoteDeb: false, required:false, gallery: false};
		
		numImagens = 1;
		numDivImagens = 1;
		
		numImagensChecklist = 1;
		numDivChecklist = 1;

		var error = $scope.getMessage('ErrorSavingData');

		if ($routeParams.equipmentId) {
			loadEquipment($routeParams.equipmentId);
		} else {
			loadEquipments();
		}

		function loadObjectType() {
			$http.get(config.baseUrl + "/admin/objectTypeByActive/true").then(
				(response) => {
					let list = response.data;
					for(let objectType in $scope.equipment.objectTypes){
						let locatedobj = list.find(val => val.objectTypeId == objectType.objectTypeId);
						if(locatedobj){
							list.splice(list.indexOf(locatedobj),1);
						}
					}
					$scope.objectTypesToSelect = list;
				}, (responseError) => {
					console.error(`${getLabel('ErrorLoadingCostCenter')} ${responseError}`);
				}
			)
		}

		$scope.checkUncheckAll = () => {
			$scope.objectTypesToSelect.forEach(objectType => {
				objectType.checked = $scope.checkedUncheckedAll;
				$scope.changeCheck(objectType);
			});
		}

		$scope.changeCheck = (objectType) => {						
			if (objectType.checked) {
				$scope.objectTypesSelected.push(objectType);				
			} else {
				var idx = $scope.objectTypesSelected.indexOf(objectType);
				$scope.objectTypesSelected.splice(idx, 1);
			}


			console.log('objectType selected: ', $scope.objectTypesSelected);
		}

		function loadEquipment(equipmentId) {			
			$http.get(config.baseUrl + "/admin/equipment/" + equipmentId).then(
				function(response) {
					console.log('Equipment: ', response.data);
					$scope.equipment = response.data;
					$scope.checklists = $scope.equipment.checklistsServices;					
					
					if ($scope.equipment.colorId == null)
						$scope.equipment.colorId = getRandomColor();

					colorSelectorConf('colorSel', $scope.equipment.colorId);

					prepareLists();
					loadObjectType();								
				},
				function(response) {
					alert($scope.getMessage('ErrorLoadData'));
				}
			);
		}

		function loadEquipments() {
			$http.get(config.baseUrl + "/admin/equipment").then(
				function(response) {
					$scope.allEquipments = response.data;
					
					$scope.query = getTextEquipment();
					$scope.optActive = getOptionEquipment().length > 0 ? getOptionEquipment() : 'active';
					
					filterEquipmentByStatus();
				}, function(response) {
					alert($scope.getMessage('ErrorLoadData'));
				}
			);
		}

		function prepareLists() {
			prepareImageLists($scope.equipment, configui, values);
			prepareChecklistsLists($scope.equipment, config, values);
		}

		$scope.saveEquipment = function(obj) {
			console.log('saveEquipment: ', obj);
			if (!validateForm())
				return;

			var url = config.baseUrl + "/admin/createEquipment";
			if (obj.equipmentId != null && obj.equipmentId > 0)
				url = config.baseUrl + "/admin/updateEquipment/" + obj.equipmentId;

			obj.images = strImages();
			obj.checklist = strChecklist();

			$http.put(url, obj).then(
				function(response) {
					if (response.status >= 200 && response.status <= 299) {
						alert($scope.getMessage('DataSavedSuccessfully'));
						refreshPage();
						$location.path(juiceService.appUrl + 'equipment');
					} else {
						refreshPage();
						alert(error + response.status + "\n" + response.message);
					}

				},
				function(response) {
					refreshPage();
					alert(error + response.status + "\n" + response.message);
				}
			);
		};

		$scope.addNewObjectType = () => {
			if ($scope.objectTypesSelected.length > 0) {
				for (let i = 0; i < $scope.objectTypesSelected.length; i++) {				
					$scope.newObjectType = JSON.stringify($scope.objectTypesSelected[i]);					
	
					$scope.equipment.objectTypes.push(JSON.parse($scope.newObjectType));
	
					let objectTypeToRemove = $scope.objectTypesToSelect.find((val) => val.objectTypeId == JSON.parse($scope.newObjectType).objectTypeId );
	
					$scope.objectTypesToSelect.splice($scope.objectTypesToSelect.indexOf(objectTypeToRemove), 1);
					$scope.newObjectType = "";					
				}

				$scope.objectTypesSelected = [];
			} else {
				if ($scope.newObjectType === "") {
					return alert(getMessage('YouMustSelectObjectType'));
				}
			}			
			
			$('#modalInventory').modal('hide');
		}

		$scope.removeObjectType = (objectType) => {
			const newObjectList = $scope.equipment.objectTypes.filter(objectTypeLocated => objectTypeLocated.objectTypeId !== objectType.objectTypeId);
			$scope.equipment.objectTypes = newObjectList;
			$scope.objectTypesToSelect.push(objectType);
		}

		$scope.addDivImage = function() {
			addDivImage(configui, values);
		};

					
		$scope.addDivChecklist = function() {
			addDivChecklist(configui, values);
		};

		function refreshPage() {
			$scope.equipment = {};
		}

		$scope.filterEquipmentByStatus = function() {
			filterEquipmentByStatus();
		};

		function filterEquipmentByStatus() {
			$scope.equipments = $scope.allEquipments.filter(
				function(equipment) {
					if ($scope.optActive == "all")
						return true;
					else if ($scope.optActive == "active" && equipment.active == true)
						return true;
					else if ($scope.optActive == "inactive" && equipment.active == false)
						return true;

					return false;
				}
			);
			
			setOptionEquipment($scope.optActive);
			
			if($scope.optActive == 'active')
				document.getElementById('optActive').value = true;
			else if($scope.optActive == 'inactive')
				document.getElementById('optInactive').value = true;
			else 
				document.getElementById('optAll').value = true;
			
			
			$scope.equipments = orderObject( $scope.equipments, 'description' );
		}

		$scope.trataEquipmentInativo = function(objEquipment) {
			if (objEquipment.active == true)
				return $scope.getLabel('Active');
			else
				return $scope.getLabel('Inactive');
		};

		$scope.mudaSituacao = function(objEquipment) {
			objEquipment.active = objEquipment.active == true ? false : true;

			$http.put(config.baseUrl + '/admin/updateEquipment/' + objEquipment.equipmentId, objEquipment).then(
				function(response) {
					alert($scope.getMessage('DataSavedSuccessfully'));
					loadEquipments();
				},
				function(response) {
					alert($scope.getMessage('ErrorSavingDataWithoutParam'));
				}
			);
		};

		$scope.retornaClasse = function(objEquipment) {
			if (objEquipment.active == true) {
				return 'btnativo';
			} else {
				return 'btninativo';
			}
		};
		
		$scope.setTextEquipment = () => {
			setTextEquipment($scope.query);
		}


		// TRATAMENTO CHECKLIST - SISTEMA
		$('#modalChecklist').on('hide.bs.modal', function(event) {
			console.log('fechou modal');
			refreshChecklist();

		});

		$scope.novoChecklist = function () {
			$scope.checklistService = {};
		}

		function refreshChecklist() {
			$scope.checklistService = {};
		}

		function addChecklistList(obj) {
			console.log('add list:', obj);
			if ($scope.checklists.indexOf(obj) == -1)
				$scope.checklists.push(obj);
	
			refreshChecklist();

			loadEquipment($routeParams.equipmentId);
		}

		function removeChecklistList(obj) {
			$scope.checklists.splice($scope.checklists.indexOf(obj), 1);

			refreshChecklist();

			loadEquipment($routeParams.equipmentId);
		};

		$scope.checklistDescription = function (checklist) {
			var desc = checklist.checklistName == null ? '' : checklist.checklistName;
	
			return desc;
		}		

		$scope.addNewChecklistService = function (checklistService) {			
			var urlSave = config.baseUrl + '/admin/createChecklistServices/';
			var newChecklist = true;

			if (checklistService.checklistName == '' || checklistService.checklistName == null) {
				alert('Informe a descrição do checklist!');

			}  else {

				if (checklistService.checklistId > 0) {
					urlSave = config.baseUrl + '/admin/updateChecklistServices/'
						+ checklistService.checklistId;
	
					newChecklist = false;
				}

				if ($scope.equipment.equipmentId > 0) {
					checklistService.equipmentEquipmentId = $scope.equipment.equipmentId;
					checklistService.active = true;
					// checklistService.checklistService.checklistMainId = null;
	
					$http({
						method: 'PUT',
						url: urlSave,
						data: checklistService
					}).then(function (response) {
						console.log("Gravado com sucesso: ", response.data);
	
						addChecklistList(checklistService);
					},
						function (error) {
							alert($scope.getMessage('ErrorSavingDataWithoutParam'));
					});
	
				} else {
					addChecklistList(checklistService);
				}
			}									
		}

		$scope.editChecklist = function (checklistService) {
			console.log('editChecklist: ', checklistService);
			$scope.checklistService = checklistService;
		}

		$scope.removeChecklist = function (checklistService) {
			if ($scope.equipment.equipmentId) {
				$http({
					method: 'PUT',
					url: config.baseUrl
						+ '/admin/removeFromEquipmentChecklist/'
						+ checklistService.checklistId,

					data: checklistService
				}).then(function (response) {
					if (response.status == "409") {
						alert($scope.getMessage('VehicleCanNotBeRemoved'));
					} else if (response.status >= "200"
						&& response.status <= "299") {
						removeChecklistList(checklistService);
					}
				},
					function (response) {
						alert($scope.getMessage('ErrorDeletingData') + response.status);
					});
			} else {
				removeChecklistList(checklistService);
			}
		};		

		$scope.addItensChecklist = function (checklistService) {
			loadChecklistItems(checklistService.checklistId);
			var txtChecklistItemsName = document.getElementById("itemName").value;
			
			if (txtChecklistItemsName != null && txtChecklistItemsName != '') {
				document.getElementById("itemName").value = '';	
			}

			$scope.titleH4 = checklistService.checklistName;
			$scope.idChecklistPanel = checklistService.checklistId;
			document.getElementById("checklistServiceIdTxt").value = checklistService.checklistId;
		}

		function deleteDivChecklistServices(div, propDelete) {
			delete dictC[propDelete];
			document.getElementById("pnlListChecklistServices").removeChild(div);
		}

		// TRATAMENTO CHECKLIST ITEMS - SUB SISTEMA
		$scope.checklistItemsDescription = function (checklistItems) {
			var desc = checklistItems.itemName == null ? '' : checklistItems.itemName;
	
			return desc;
		}

		function loadCheckBoxRequiredPhotos() {
			$('#OK').prop("checked", requiredPhotos.ok);
			$('#NOK').prop("checked", requiredPhotos.nok);
			$('#FIX').prop("checked", requiredPhotos.fix);
		}
		
		function loadChecklistItems(id) {
			var urlSave = config.baseUrl + '/admin/checklistItemsByChecklistId/';

			if (id) {
				$http.get(urlSave+id).then(
					(response) => {
						$scope.checklistItems = response.data;
						console.log('loadChecklistServiceItems: ', $scope.checklistItems);	
						$('#OK').prop("checked", false);
						$('#NOK').prop("checked", false);
						$('#FIX').prop("checked", false);

					}, (responseError) => {
						console.log(responseError);
						console.error(`${getLabel('ErrorLoadingCostCenter')} ${responseError}`);
					}
				)
			}
		}

		function returnRequiredPhotos() {
			requiredPhotos = {
				'ok': $("#OK").is(':checked'),
				'nok': $("#NOK").is(':checked'),
				'fix': $("#FIX").is(':checked')		
			}
			
			return requiredPhotos;
		}

		function refreshChecklistItems() {
			$scope.checklistItem = {};
			
			$('#OK').prop("checked", false);
			$('#NOK').prop("checked", false);
			$('#FIX').prop("checked", false);
		}

		function addChecklistItemsList(obj) {
			console.log('add items list:', obj);
			if ($scope.checklistItems.indexOf(obj) == -1)
				$scope.checklistItems.push(obj);
	
			refreshChecklistItems();

			loadChecklistItems(obj.checklistServicesId);
		}

		$scope.addNewChecklistItems = function (item) {

			if (item.itemName == '' || item.itemName == null) {
				alert($scope.getMessage('ErrorSavingDataWithoutParam'));
			} else {
				var urlSave = config.baseUrl + '/admin/createChecklistItems/';
				var checklistId = document.getElementById("checklistServiceIdTxt").value;
				item.checklistServicesId = parseInt(checklistId);
				item.active = true;

				item.requiredPhotos = JSON.stringify( returnRequiredPhotos() );

				console.log('addNewChecklistItems: ', item);
				// var newChecklist = true;

				if (item.itemId > 0) {
					urlSave = config.baseUrl + '/admin/updateChecklistItems/'
						+ item.itemId;

					// newChecklist = false;
				}

				if (item.checklistServicesId > 0) {
					$http({
						method: 'PUT',
						url: urlSave,
						data: item
					}).then(function (response) {
						console.log("Gravado com sucesso: ", response.data);

						addChecklistItemsList(item);
					},
						function (error) {
							alert($scope.getMessage('ErrorSavingDataWithoutParam'));
					});

				} else {
					addChecklistItemsList(item);
				}
			}						
		}

		$scope.editChecklistItems = function (item) {
			console.log('editChecklistItems: ', item);
			$scope.checklistItem = item;

			if (item.requiredPhotos != null) {
				requiredPhotos = JSON.parse(item.requiredPhotos.toLowerCase());
			}			

			loadCheckBoxRequiredPhotos();
		}

		$scope.removeChecklistItem = function (item) {
			console.log('removeChecklistItem: ', item);
			if ($scope.equipment.equipmentId) {
				$http({
					method: 'PUT',
					url: config.baseUrl
						+ '/admin/removeChecklistItem/'
						+ item.itemId,

					data: item
				}).then(function (response) {
					if (response.status == "409") {
						alert($scope.getMessage('VehicleCanNotBeRemoved'));
					} else if (response.status >= "200"
						&& response.status <= "299") {
						removeChecklistItemList(item);
					}
				},
					function (response) {
						alert($scope.getMessage('ErrorDeletingData') + response.status);
					});
			} else {
				removeChecklistItemList(item);
			}
		};
		
		function removeChecklistItemList(obj) {
			$scope.checklistItems.splice($scope.checklistItems.indexOf(obj), 1);

			refreshChecklistItems();

			loadChecklistItems(obj.checklistServicesId);
		};


		/*
		** IMPORT CHECKSLIST-SERVICES 
		*/

		if ($routeParams.equipmentId != null) {
			loadImportsByClassName();
			
			function loadImportsByClassName() {
				$http.get(`${config.baseUrl}/admin/importsByClass/CHECKLISTSERVICES`).then(
					function(response){				
						console.log('imports: ', response.data);
						$scope.imports = response.data
					},
					function(response){
						alert($scope.getMessage('ErrorLoadData'));
					}
				);
			}

			$('#modalImportXls').on('hide.bs.modal', function(event) {
				clearModal();
			});	
			
			function clearModal() {
				document.getElementById('uploadXls').value = '';
				document.getElementById('divImport').innerHTML = '';
				$scope.imports = null;
			}

			$(document).ready(function() {
				$('#uploadXls').change(function(){
					if(this.files && this.files[0]){
						insertIconXls(this.files[0].name);
						readTheFileUrl(this.files[0]);
					}
				});
			});

			function insertIconXls(name){
				const inner = `<img id="xlsImported" src="apiGIMB/view/TablesForImport/xls.png" style="max-height: 50px; max-width: 50px;">
					<br/>
					<span>${name}</span>
					<br/>
					<div  ng-click="importXls()" > 
						<i class = "glyphicon glyphicon-send"></i>
						<input type="button" value="${getLabel('send')}" class="btn formGroupInput">
					</div>
				`;
					
				const $str = $(inner).appendTo('#divImport');
				$compile($str)($scope);
			}

			$scope.importXls = () => {
				if ($scope.imports != null) {			
					$http.post(config.baseUrl + '/admin/importXLSTableChecklistService/' + $routeParams.equipmentId, $scope.imports).then(
						function(response){
							console.log('Retorno importXls: ', response);
							
							// if (response.data.length > 0) {
							// 	var identificadores = '';
							// 	for (let i = 0; i < response.data.length; i++) {
							// 		identificadores = identificadores + ', ' + response.data[i].plate;
							// 	}
							// 	alert('Equipamento não encontrado, para o identificador: - ' + identificadores);						
							// }
							
							alert($scope.getMessage('DataSavedSuccessfully'));
							loadEquipment($routeParams.equipmentId);

							$('#modalImportXls').modal('hide');
							
						},
						function(response){
							alert($scope.getMessage('ErrorLoadData'));							
						}
					);
				}
				else {
					alert($scope.getMessage('ErrorLoadData'));
				}
			}

			function readTheFileUrl(file){
				var reader = new FileReader();
				reader.readAsDataURL(file);
				reader.onload = (e) => {
					$scope.imports.filePath = e.target.result.substr(e.target.result.indexOf('base64,') + 7, e.target.result.length);;
				}
			}
		}
	}	
);