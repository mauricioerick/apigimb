appGIMB.controller("expenseController", function ($scope, $http, $routeParams, $location, config, juiceService) {

	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');
	if (!controllerHeadCheck($scope, $location, juiceService)) return;

	$('#divMenu').show();
	$("#searchStartDate").datepicker(datePickerOptions);
	$("#searchEndDate").datepicker(datePickerOptions);
	$("#txtStartDate").datepicker(datePickerOptions);
	$("#txtEndDate").datepicker(datePickerOptions);
	$("#txtItemDate").datepicker(datePickerOptions);
	$("#dtIssuance").datepicker(datePickerOptions);
	$("#dtDue").datepicker(datePickerOptions);
	
	$("#dtInicio").datepicker(datePickerOptions);
	$("#dtFinal").datepicker(datePickerOptions);

	changeScreenHeightSize('#divTableExpense');

	var canRunReload = false;

	$scope.searchStartDate = dateToStringPeriodo(new Date(), "i");
	$scope.searchEndDate = dateToStringPeriodo(new Date(), "f");

	$scope.statementExpense = ""
	$scope.company = localStorage.getItem['company']

	$scope.expense = {};
	$scope.currentExpenseItem = {};
	$scope.expenses = [];
	$scope.allExpenses = [];

	$scope.users = [];
	$scope.clients = [];
	$scope.events = [];

	$scope.userFilterSet = {};
	$scope.eventFilterSet = {};

	$scope.settlements = [];
	$scope.totalValueSettlements = 0;
	
	$scope.clients = [];
	$scope.clientsSelected = getClientExpense();
	$scope.clientsNames = "";
	$scope.showClient = false;
	
	$scope.sequences = [];
	$scope.sequencesSelected = getSequenceExpense();
	$scope.sequenceNames = "";
	$scope.showSequence = false;
	
	$scope.showEvents = false;
	$scope.showUsers = false;
	$scope.usersSelected = [];
	$scope.eventsSelected = [];
	
	$scope.changeExpenseDate = 0;
	
	$scope.loadingSettlements = false;

	const today = new Date();
	const dtIni = getDataInicioFiltroExpense();
	const dtFin = getDataFinalFiltroExpense();
	$scope.dtInicio = dtIni.length == 0 ? dateToStringPeriodo(today, "i") : dtIni;
	$scope.dtFinal = dtFin.length == 0 ? dateToStringPeriodo(today, "f") : dtFin;
	
	setDataInicioFiltroExpense($scope.dtInicio);
	setDataFinalFiltroExpense($scope.dtFinal);

	/*$scope.noteInformationForThirdParties = {
		'tradingName'  			: '',
		'registration' 			: '',
		'document'	   			: '',
		'address'	   			: '',
		'city'		   			: '',
		'isNoteForThirdParties' : false
	}*/

	var isNoteForThirdParties = false;
	$scope.filIlnFieldsOrSelectFromTheList = 'selectFromList';
	$scope.clientForDebitNote = {};

	document.getElementById('dtInicio').readOnly = true;
	document.getElementById('dtFinal').readOnly = true;
	
	/* Report data*/
	$scope.detailTotal = 0;
	/**/

	/* Report DebitNote*/
	$scope.debitNote = {
		issuanceDate: dateToString(new Date()),
		note: '',
		urlLogo: ''
	};
	/* */

	if ($routeParams.expenseId) {
		loadExpense($routeParams.expenseId);
		loadDebitNoteSeries();
	}
	else if ($location.path().indexOf("Create") < 0) {
		loadExpenses();
	}
	else {
		$('#modal_carregando').modal('show');
		loadUsers()
		loadEvents();
		loadClients();
	}

	function confirmStatementExpense() {
		let expenseMessageBody = document.getElementById('expenseMessageBody');
		expenseMessageBody.innerHTML = $scope.expense.demonstrativeText
	}

	function loadExpense(eventId) {
		$http.get(config.baseUrl + "/admin/expense/" + eventId)
			.then(function (response) {
				
				getProfile();
				
				$scope.expense 				= response.data;
				$scope.clientForDebitNote 	= $scope.expense['client'];

				{ // Config data to print
					$scope.company = JSON.parse(localStorage.getItem("company"));
					$scope.reportNumber = pad($scope.expense.expenseId, 4) + '/' + $scope.expense.startDate.substring(6, 10);
					$scope.reportDate = '';
				}

				if ($scope.expense.status == 'CANCEL' || $scope.expense.status == 'PAID') {
					document.getElementById('fieldset').disabled = 'disabled';
				} else {
					loadUsers()
					loadClients();
					loadEvents();
				}
			}, function (response) {
				alert($scope.getMessage('ErrorLoadData'));
			});
	}
	
	function getProfile() {
		const user = JSON.parse(localStorage.getItem("bobby"));
		const profile = user.profile;
		objConfigWeb = JSON.parse(profile.configWeb);
		$scope.changeExpenseDate = objConfigWeb['CHANGEEXPENSEDATE'];
	}

	function loadDebitNoteSeries(){
		$http.get(config.baseUrl + "/admin/debitNoteSeries/")
			.then(function (response) {
				$scope.series = response.data;
				$scope.seriesKeys = Object.keys($scope.series);

			}, function (response) {
				alert($scope.getMessage('ErrorLoadData'));
			});
	}

	function loadExpenses() {
		$('#modal_carregando').modal('show');

		const startDate = $scope.dtInicio;
		const endDate = $scope.dtFinal;
		const params = {startDate, endDate};

		setDataInicioFiltroExpense(startDate);
		setDataFinalFiltroExpense(endDate);
		setClientExpense($scope.clientsSelected);
		setSequenceExpense($scope.sequencesSelected);
		
		$http({
			method: 'PUT',
			url: config.baseUrl + "/admin/expenseWeb",
			data: params
		}).then(function (response) {
			$scope.allExpenses = response.data;
			loadSequences($scope.allExpenses);
			canRunReload = true;
			listFilter();
			fillLists();
			loadDatePicker();
			
			$('#modal_carregando').modal('hide');
		}, function (response) {

			$('#modal_carregando').modal('hide');
			alert($scope.getMessage('ErrorLoadData'));
		});
	}

	function loadUsers() {
		$http.get(config.baseUrl + "/admin/ds/listUsers")
			.then(function (response) {
				$scope.users = response.data;
				if ($scope.users != null)
					$scope.userFilterSet = $scope.users[0];
			}, function (error) {
				// do nothing
			});
	}

	function loadClients() {
		$http.get(config.baseUrl + "/admin/clientByActive/true")
			.then(function (response) {
				$scope.clients = response.data;
				$('#modal_carregando').modal('hide');
			}, function (error) {
				// do nothing
				$('#modal_carregando').modal('hide');
			});
	}

	function loadEvents() {
		$http.get(config.baseUrl + "/admin/event")
			.then(function (response) {
				$scope.events = response.data;
			}, function (error) {
				//mensagem de erro
			});
	}

	function searchSettlements() {
		
		$scope.loadingSettlements = true;
		
		var params = {};
		params["startDate"] = $scope.expense.startDate;
		params["endDate"] = $scope.expense.endDate;

		$http.put(config.baseUrl + "/settlementByDateBetween", params)
			.then(function (response) {
				$scope.settlements = response.data;

				if($scope.settlements.length > 0){
					filterListSettlement();
					
					$scope.settlements.forEach(function (set) {
						set.checked = true;
					});
					
					document.getElementById("chkSelectAllSettlements").checked = true;
					$scope.calcTotalValueSettlement();
				}
				
				$scope.loadingSettlements = false;
				
			}, function (response) {
				$scope.loadingSettlements = false;
			});
	}

	function saveExpense(expense, showMessage, refresh) {
		if (expense.status == null)
			expense.status = 'OPEN';

		url = "";
		if (expense.expenseId > 0) {
			url = config.baseUrl + "/admin/updateExpense/" + expense.expenseId;
		}
		else {
			url = config.baseUrl + "/admin/createExpense";
			$scope.expense.createUser = JSON.parse(localStorage.getItem("bobby"));
			$scope.expense.createDate = timestampNowDDMMYYYY();
		}

		// sorting items by date
		if (expense.items != null) {
			$scope.expense.items = $scope.expense.items.sort(function (item1, item2) {
				return (item1.event.description > item2.event.description) ? 1 : -1;
			});

			$scope.expense.items = $scope.expense.items.sort(function (item1, item2) {
				d1 = new Date(stringToDate(item1.date));
				d2 = new Date(stringToDate(item2.date));

				return d1.getTime() - d2.getTime();
			});
		}

		$http.put(url, expense)
			.then(function (response) {
				if (response.status >= 200 && response.status <= 299) {
					$scope.expense = response.data;
					if (showMessage)
						alert($scope.getMessage('DataSavedSuccessfully'));

					{ // Config data to print
						$scope.company = JSON.parse(localStorage.getItem("company"));
						$scope.reportNumber = pad($scope.expense.expenseId, 4) + '/' + $scope.expense.startDate.substring(6, 10);
						$scope.reportDate = '';
					}

					if ($scope.expense.status == 'CANCEL')
						document.getElementById('fieldset').disabled = 'di sabled';

					if (refresh)
						refreshPage();
				} else {
					if (showMessage)
						alert($scope.getMessage('ErrorSavingData') + response.status + "\n" + response.message);
				}
			}, function (response) {
				if (showMessage)
					alert($scope.getMessage('ErrorSavingData') + response.status + "\n" + response.message);
			});
	}

	function deleteExpense(expense) {
		url = config.baseUrl + "/admin/deleteExpense/" + expense.expenseId;

		$http.put(url, expense)
			.then(function (response) {
				refreshPage();
			}, function (response) {
				// do nothing
			});
	}

	function cancelExpense(expense) {
		expense.status = 'CANCEL';
		saveExpense(expense, false, true);
	}

	function refreshPage() {
		$scope.expense = {};
		$location.path(juiceService.appUrl + 'expense');
	}

	$scope.saveExpense = function (expense) {
		if (expense.client == null || expense.client.clientId == 0 || expense.startDate == null || expense.endDate == null) {
			alert($scope.getMessage('CustomerPeriodInformed'));
			return;
		}

		saveExpense(expense, true);
	};

	$scope.deleteExpense = function (expense) {
		deleteExpense(expense);
	};

	$scope.cancelExpense = function (expense) {
		cancelExpense(expense);
	};

	$scope.addExpenseItem = function () {
		item = $scope.currentExpenseItem;

		if (item.date == null || item.event == 0 || item.unitValue == null) {
			alert($scope.getMessage('DateOccurrenceAndUnitValueInformed'));
			return;
		}

		if ($scope.expense.items == null)
			$scope.expense.items = [];

		item.quantity = item.quantity == null ? 1 : item.quantity;

		item.totalValue = (parseFloat(item.quantity) * parseFloat(item.unitValue));

		if (item.expenseItemId == null || item.expenseItemId == 0) {
			item.createUser = JSON.parse(localStorage.getItem("bobby"));
			item.createDate = timestampNowDDMMYYYY();
		}

		if ($scope.expense.items.indexOf(item) < 0) {
			$scope.expense.items.push(item);
		}

		$scope.currentExpenseItem = {};

		saveExpense($scope.expense, false);
		document.getElementById("txtItemDate").focus();
	};

	$scope.editExpenseItem = function (item) {
		$scope.currentExpenseItem = item;
	};

	$scope.duplicateExpenseItem = function (item) {
		other = {};
		other.date = item.date;
		other.event = item.event;
		other.quantity = item.quantity;
		other.unitValue = item.unitValue;
		other.settlement = null;

		$scope.currentExpenseItem = other;
		$scope.addExpenseItem();
	};

	$scope.deleteExpenseItem = function (item) {
		$scope.expense.items.splice($scope.expense.items.indexOf(item), 1);
		$scope.currentExpenseItem = {};

		saveExpense($scope.expense, false);
	};

	$scope.searchSettlements = function () {
		searchSettlements();
	};

	$scope.selectAllSettlement = function () {
		$scope.settlements.forEach(function (set) {
			set.checked = !set.checked;
		});
		$scope.calcTotalValueSettlement();
	};

	$scope.calcTotalValueSettlement = function () {
		$scope.totalValueSettlements = 0;
		$scope.settlements.forEach(function (set) {
			if (set.checked)
				$scope.totalValueSettlements += set.amount;
		});
		$scope.totalValueSettlements = $scope.totalValueSettlements.toFixed(2)
	};

	$scope.importSettlement = function () {
		if ($scope.expense.items == null)
			$scope.expense.items = [];

		$scope.settlements.forEach(function (set) {
			if (set.checked) {
				item = {};
				item.date = set.date;
				item.event = set.event;
				item.quantity = 1;
				item.unitValue = set.amount;
				item.settlement = set;

				item.createUser = JSON.parse(localStorage.getItem("bobby"));
				item.createDate = timestampNowDDMMYYYY();

				item.totalValue = (parseFloat(item.quantity) * parseFloat(item.unitValue));

				$scope.expense.items.push(item);
			}
		});
		saveExpense($scope.expense, false);

		// dismiss all open modals
		$('.modal').modal('hide');
		$scope.closeImportSettlements();
	};

	$scope.closeImportSettlements = function () {
		if ($scope.users != null)
			$scope.userFilterSet = $scope.users[0];
		$scope.eventFilterSet = {};
		$scope.settlements = [];
	};

	$scope.reload = function () {
		if (canRunReload) {
			$scope.searchStartDate = document.getElementById("searchStartDate").value;
			$scope.searchEndDate = document.getElementById("searchEndDate").value;

			loadExpenses();
		}
	};

	$scope.validateNumericInput = function (event) {
		validateNumericInput(event, event.target.value);
	};

	$scope.cleanCurrentItem = function () {
		$scope.currentExpenseItem = {};
	};

	$scope.hasPicsToExport = function () {
		for (idx in $scope.expense.items) {
			if ($scope.expense.items[idx].settlement != null)
				return true;
		}

		return false;
	};

	$scope.printDetail = function () {
		changeStatusExpenseToWaitingApproval($scope.expense);
		saveExpense($scope.expense);
		confirmStatementExpense();
		consolidateDataDetail();

		setTimeout(function () {
			var e = document.getElementById('printDetail');
			e.click();
		}, 500);
	};

	function consolidateDataDetail() {
		$scope.detailTotal = 0;
		items = $scope.expense.items;

		var columns = [];

		dataAmount = {};
		dataQty = {};
		dataDisplay = {};

		for (idx in items) {
			item = items[idx];

			dailyAmount = dataAmount[item.date];
			dailyQty = dataQty[item.date];

			if (dailyAmount == null) dailyAmount = {};
			if (dailyQty == null) dailyQty = {};

			if (dailyAmount[item.event.description] == null) dailyAmount[item.event.description] = 0;
			if (dailyQty[item.event.description] == null) dailyQty[item.event.description] = 0;

			dailyAmount[item.event.description] = Math.round((item.totalValue + dailyAmount[item.event.description]) * 100) / 100;
			dailyQty[item.event.description] = Math.round((item.quantity + dailyQty[item.event.description]) * 100) / 100;

			if (dataDisplay[item.event.description] == null){
				dataDisplay[item.event.description] = {
					'displayFormula' : item.event.displayFormula == 1 ? 1 : 0,
					'requestkm'      : item.event.requestKm ? 1 : 0
				}
			}
			
			dataAmount[item.date] = dailyAmount;
			dataQty[item.date] = dailyQty;

			if (!columns.includes(item.event.description)) columns.push(item.event.description);
		}

		detailItems = [];
		dates = Object.keys(dataAmount);
		for (var idx in dates) {
			date = dates[idx];

			keys = Object.keys(dataAmount[date]);
			var arr = [];
			var total = 0;
			for (var i in keys) {
				key = keys[i];
				arr.push({ settlement: key, quantity: dataQty[date][key], amount: dataAmount[date][key] });
				total = Math.round((total + dataAmount[date][key]) * 100) / 100;
			}
			arr.push({ settlement: "TOTAL", amount: total });

			$scope.detailTotal = Math.round(($scope.detailTotal + total) * 100) / 100;
			detailItems.push({ date: date, values: arr });
		}
		columns = columns.sort(function (e1, e2) {
			return (e1 > e2) ? 1 : -1;
		});
		columns.unshift('DATA');
		columns.push('TOTAL');

		$scope.detailTotal = formatMoney($scope.detailTotal);

		divWidth = 100 / columns.length;

		divTableItem = document.getElementById('divTableItem');
		divTableItem.innerHTML = strDivTittle($scope.expense.client.tradingName);
		for (var idx in columns) {
			divTableItem.innerHTML += strDivHeader(divWidth, columns[idx].toUpperCase());
		}

		dataTotal = {};

		for (var i in detailItems) {
			var item = detailItems[i];
			divTableItem.innerHTML += strDivRow(divWidth, item.date, false);

			for (var j in columns) {
				if (j == 0) continue;
				var column = columns[j];

				if (dataTotal[column] == null) dataTotal[column] = 0;

				var find = false;
				for (var idx in item.values) {
					value = item.values[idx];

					if (value.settlement == column) {
						label = formatMoney(value.amount);
						if (dataDisplay[value.settlement] != null && 
							dataDisplay[value.settlement] != undefined && 
							dataDisplay[value.settlement]['displayFormula'] == 1){
							
							label = '(' + value.quantity + ' x ' + formatMoney(value.amount / value.quantity) + ') ' + formatMoney(value.amount) + '';
							
							if(dataDisplay[value.settlement]['requestkm'] == 1){
								label = '(' + value.quantity + 'km x ' + formatMoney(value.amount / value.quantity) + ') ' + formatMoney(value.amount);
							}
						}
						
						divTableItem.innerHTML += strDivRow(divWidth, label, column == 'TOTAL');
						find = true;

						dataTotal[column] = Math.round((value.amount + dataTotal[column]) * 100) / 100;
						break;
					}
				}

				if (!find)
					divTableItem.innerHTML += strDivRow(divWidth, '-', false);
			}
		}

		divTableItem.innerHTML += strDivRow(divWidth, ' - ', false);
		for (var j in columns) {
			if (j == 0) continue;
			divTableItem.innerHTML += strDivRow(divWidth, formatMoney(dataTotal[columns[j]]), true);
		}
	}

	function strDivTittle(client) {
		return '' +
			'<div style="border: 1px solid #000 !important; text-align: center; font-size: 14px; font-weight: bold; background-color: #eee !important;"> ' +
			'' + client +
			'</div>';
	}

	function strDivHeader(width, label) {
		return '' +
			'<div style="height: 100% !important; width: ' + width + '% !important; border: 1px solid #000 !important; text-align: center; ' + (width >= 14 ? 'font-size: 13px' : 'font-size: 10px') + '; font-weight: bold"> ' +
			'	' + label + ' ' +
			'</div>';
	}

	function strDivRow(width, label, bold) {
		var font_size = 8;
		
		if(width >= 14){
			font_size = 10
		}
		
		if(label.length >= 21){
			font_size = 8;
		}
		
		return '' +
			'<div style="height: 100% !important; width: ' + width + '% !important; border: 1px solid #000 !important; text-align: center; font-size: '+font_size+'px; ' + (bold == true ? 'font-weight: bold;' : '') + ' "> ' +
			'	' + label + ' ' +
			'</div>';
	}
	
	$scope.printDebitNote = _=> {
		
		let unfilledFields = false;
		
		prepareForprint().then(()=>{
			$scope.debitNote.sequence 		= pad($scope.expense.debitNote.sequence, 5);
			$scope.debitNote.series 		= $scope.expense.debitNote.series;
			$scope.debitNote.company 		= $scope.company;
			$scope.debitNote.client	 		= $scope.clientForDebitNote
			$scope.debitNote.listDebit 		= [];
			$scope.debitNote.total 			= 0;
			$scope.debitNote.formatedTotal 	= '';

				
				for (const item of $scope.expense.items) {
					var debit = {};

					if ($scope.debitNote.listDebit.some(d => d.description == item.event.description)) {
						debit = $scope.debitNote.listDebit.find(d => d.description == item.event.description);
						debit.quantity += item.quantity;
						debit.total += item.totalValue;
						var unit = debit.total / debit.quantity;
						debit.unit = unit.toFixed(2);
						$scope.debitNote.total += item.totalValue;
					} else {
						debit.code = '';
						debit.description = item.event.description;
						debit.quantity = item.quantity;
						debit.total = item.totalValue;
						debit.unit = item.totalValue / item.quantity;
						$scope.debitNote.listDebit.push(debit);
						$scope.debitNote.total += item.totalValue;
					}
				}

				$scope.debitNote.formatedTotal = formatMoney($scope.debitNote.total);

				$scope.debitNote.totalValueFull = $scope.debitNote.formatedTotal.extenso(true);

				$scope.debitNote.listBank = [
					{
						name: 'BRADESCO',
						agency: '0110-4',
						cc: '19245-7'
					},
					{
						name: 'SANTANDER',
						agency: '0008',
						cc: '13014649-6'
					}
				];

				fillDebitNoteReport();

				$('#modalExtraInformationDebitNote').modal('hide');

				$scope.$apply();
				var e = document.getElementById('printDebitNote');
				e.click();
					
			});
		
		//showModalConfirmDebitNote();	
		$scope.debitExpense($scope.expense);
	};

	function showModalConfirmDebitNote() {
		$("#modalConfirmDebit").modal('show');			
	}


	var prepareForprint = () => {
		return new Promise(async (resolve,reject)  =>  {
			try {
				if(!$scope.expense.debitNote) {
					var params = {};
					if($scope.debitNote.series == 'new' && $scope.debitNote.typedSeries != ''){
						params.series = $scope.debitNote.typedSeries;
						params.sequence = 1;
					} else {
						params.series = $scope.debitNote.series;
						params.sequence = $scope.series[$scope.debitNote.series]+1;
					}

					var response = await $http({
						method: 'PUT',
						url: config.baseUrl + "/admin/createDebitNote",
						data: params
					});
					$scope.expense.debitNote = response.data;
					saveExpense($scope.expense);
				}
				resolve();
			} catch (e) {
				reject(e);
			}
		});
	}
	function fillDebitNoteReport() {
		var count = $scope.debitNote.listDebit.length;
		for (let index = count; index < 23; index++) {
			var debit = {};
			debit.code = '';
			debit.description = '';
			debit.quantity = '';
			debit.total = '';
			debit.unit = '';
			$scope.debitNote.listDebit.push(debit);
		}
	}

	$scope.formatMoney = function (valor) {
		return formatMoney(valor);
	};

	$scope.dueDateInvalid = function (param) {
		alert($scope.getMessage('EnterDueDate'));
	};

	/* ---------------------- */
	/* View Events Validate   */
	/* ---------------------- */
	$scope.validateDate = function ($event) {
		if ($scope.currentExpenseItem.date != null && $scope.currentExpenseItem.date.length > 0) {
			try {
				itemDate = new Date(stringToDate($scope.currentExpenseItem.date));
				startDate = new Date(stringToDate($scope.expense.startDate));
				endDate = new Date(stringToDate($scope.expense.endDate));

				if (itemDate < startDate || itemDate > endDate)
					throw '';
			} catch (exception) {
				alert($scope.getMessage('InformedDateIncorrect'));
				$scope.currentExpenseItem.date = '';
				$event.target.focus();
			}
		}
	};
	
	$scope.showModalWindow = () => {
		fillLists();
		$("#parametersModal").modal({ backdrop: 'static', keyboard: false });
	}
	
	function fillLists(){
		for(var expense of $scope.allExpenses)
			if($scope.clients.indexOf(expense.client.tradingName) <= -1)
				$scope.clients.push(expense.client.tradingName);		
	}
	
	$scope.buscar = () => {
		loadExpenses();
	}
	
	$scope.showClientsSelect = () => {
		$scope.showClient = !$scope.showClient;
		
		if($scope.showClient)
			$("#clientTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
		else
			$("#clientTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
	}
	
	$scope.showSequencesSelect = () => {
		$scope.showSequence = !$scope.showSequence;
		
		if($scope.showSequence) {
			$("#sequencesTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
		}
		else{
			$("#sequencesTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
		}
	}
	
	function listFilter(){
		let listaExpenseFiltrada = [];
		
		$scope.allExpenses.forEach(expense => {
			listaExpenseFiltrada.push(expense);
			let removeuElemento = false;
			
			if($scope.clientsSelected.length > 0){
				let idx = 1;
				
				$scope.clientsSelected.forEach(client => {
					if(expense.client.tradingName == client) return true;
					if(expense.client.tradingName != client && idx == $scope.clientsSelected.length){
						let indexExpense = listaExpenseFiltrada.indexOf(expense);
						listaExpenseFiltrada.splice(indexExpense);
						removeuElemento = true;
					}
					idx ++;
				});
			}
			
			if($scope.sequencesSelected.length > 0 && !removeuElemento) {
				let idx = 1;
				
				if(expense.debitNote == null){
					let indexExpense = listaExpenseFiltrada.indexOf(expense);
					listaExpenseFiltrada.splice(indexExpense);
				}
				else {
					$scope.sequencesSelected.forEach(sequence => {
						if(sequence == expense.debitNote.sequence) return true;
						if(sequence != expense.debitNote.sequence && idx == $scope.sequencesSelected.length) {
							let indexExpense = listaExpenseFiltrada.indexOf(expense);
							listaExpenseFiltrada.splice(indexExpense);
						}
						idx ++;
					});
				}
			}
			
		});
		
		$scope.expenses = [];
		listaExpenseFiltrada.forEach(expense => $scope.expenses.push(expense));
		
		fillLabelsParameters();
	}
	
	function fillLabelsParameters(){
		$scope.clientsNames = "";
		$scope.sequenceNames = "";
		
		if($scope.clientsSelected.length > 0)
			for(var client of $scope.clientsSelected)
				if(!$scope.clientsNames.includes(client))
					$scope.clientsNames += $scope.clientsNames.length == 0 ? client : ", " + client;
		
		if($scope.sequencesSelected.length > 0){
			for(var sequence of $scope.sequencesSelected){
				if(!$scope.sequenceNames.includes(sequence)){
					$scope.sequenceNames += $scope.sequenceNames.length == 0 ? sequence.toString() : ", " + sequence; 
				}
			}
		}
	}
	
	function loadDatePicker(){
		$("#dtInicio").datepicker(datePickerOptions);
		$("#dtFinal").datepicker(datePickerOptions);
	}
	
	$scope.showUsersSelect = () => {
		$scope.showUsers = !$scope.showUsers;
		
		if($scope.showUsers){
			$('#userTriangle').removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
		} else {
			$('#userTriangle').removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
		}
	}
	
	$scope.showEventsSelect = () => {
		$scope.showEvents = !$scope.showEvents;
		
		if($scope.showEvents){
			$('#eventTriangle').removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
		} else {
			$('#eventTriangle').removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
		}
	}
	
	function filterListSettlement() {
		let listaLiquidacaoFiltrados = [];
		
		$scope.settlements.forEach(settlement => {
			listaLiquidacaoFiltrados.push(settlement);
			let removeuElemento = false;
			
			if($scope.usersSelected.length > 0) {
				let idx = 1;
				
				$scope.usersSelected.forEach(user => {
					if(settlement.user.userId == user.userId) return true;
					if(settlement.user.userId != user.userId && idx == $scope.usersSelected.length){
						let indexSettlement = listaLiquidacaoFiltrados.indexOf(settlement);
						listaLiquidacaoFiltrados.splice(indexSettlement);
						removeuElemento = true;
					}
					idx ++;
				});
			}
			
			if($scope.eventsSelected.length > 0 && !removeuElemento) {
				let idx = 1;
				
				$scope.eventsSelected.forEach(event => {
					if(settlement.event.eventId == event.eventId) return true;
					if(settlement.event.eventId != event.eventId && idx == $scope.eventsSelected.length) {
						let indexSettlement = listaLiquidacaoFiltrados.indexOf(settlement);
						listaLiquidacaoFiltrados.splice(indexSettlement);
						removeuElemento = true;
					}
					idx ++;
				});
			}
		});
		
		$scope.settlements = [];
		listaLiquidacaoFiltrados.forEach(l => $scope.settlements.push(l));
	}
	
	$scope.finishExpense = (expense) => {
		expense.status = 'PAID';
		saveExpense(expense, true, true);
	}
	
	$scope.debitExpense = (expense) => {
		expense.status = 'WAIT_PAYMENT';
		saveExpense(expense, true, false);
	}
	
	function loadSequences(expenses) {
		$scope.sequences = [];
		
		for(var expense of expenses) {
			if(expense.debitNote != null) {
				if($scope.sequences.indexOf(expense.debitNote.sequence) < 0){
					$scope.sequences.push(expense.debitNote.sequence);
				}
			}
		}
	}
	
	function changeStatusExpenseToWaitingApproval(expense){
		expense.status = 'WAIT_APPR';
	}

	$scope.generateTheNoteForThirdParties = _=> {
		isNoteForThirdParties = true;
		$('#informationforgeneratingdebitmemoforthirdparties').modal('show');
	}

});
