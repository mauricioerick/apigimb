appGIMB.controller("actionController",
	function ($scope, $http, $routeParams, $location, config, juiceService, geralAPI) {
		$('form').attr('autocomplete', 'off');
		$('input[type=text]').attr('autocomplete', 'off');
		if (!controllerHeadCheck($scope, $location, juiceService))
			return;

		$('#divMenu').show();

		// changeScreenHeightSize('#divTableAction');

		numCampos = 1;
		numDivCampos = 1;
		dictF = {};

		$scope.action = { active: true };
		$scope.action.colorId = getRandomColor();
		colorSelectorConf('colorSel', $scope.action.colorId);
		$scope.actions = [];
		$scope.allActions = [];
		$scope.optActive = '';
		
		var requiredPhotos = {
			'ok': false,
			'nok': false,
			'fix': false
		}
		
		$scope.optActive = "active";

		var error = $scope.getMessage('ErrorSavingData');

		if ($routeParams.actionId) {
			carregarAtividade($routeParams.actionId);
		} else {
//			carregarAtividades();
			loadActions();
		}

		function carregarAtividade(actionId) {
			$http.get(config.baseUrl + "/admin/action/" + actionId).then(
				function (response) {
					$scope.action = response.data;

					if ($scope.action.colorId == null)
						$scope.action.colorId = getRandomColor();

					colorSelectorConf('colorSel', $scope.action.colorId);
					loadActions();
					prepareFieldLists($scope.action);
					
					requiredPhotos = JSON.parse(response.data.requiredPhotos.toLowerCase());
					
					loadCheckBoxRequiredPhotos();
				},
				function (response) {
					alert($scope.getMessage('ErrorLoadData'));
				}
			);
		}
		
		function loadCheckBoxRequiredPhotos() {
			$('#OK').prop("checked", requiredPhotos.ok);
			$('#NOK').prop("checked", requiredPhotos.nok);
			$('#FIX').prop("checked", requiredPhotos.fix);
		}

		function carregarAtividades() {
			$http.get(config.baseUrl + "/admin/action").then(
				function (response) {
					$scope.actions = response.data;
					loadActions();
				},
				function (response) {
					alert($scope.getMessage('ErrorLoadData'));
				}
			);
		}

		$scope.actions = carregarAtividades();

		$scope.saveAction = function (obj) {			
			if (!validateForm())
				return;

			var url = config.baseUrl + "/admin/actionCreate";

			if (obj.actionId != null || obj.actionId > 0)
				url = config.baseUrl + "/admin/updateAction/" + obj.actionId;

			obj.fields = strFields();			
			
			obj.requiredPhotos = JSON.stringify( returnRequiredPhotos() );
			console.log('saveAction: ', obj);
			$http.put(url, obj).then(
				function (response) {
					if (response.status >= 200 && response.status <= 299) {
						alert($scope.getMessage('DataSavedSuccessfully'));
						refreshPage();
						$location.path(juiceService.appUrl + 'action');
					} else {
						refreshPage();
						alert(error + response.status + "\n" + response.message);
					}
				},
				function (response) {
					refreshPage();
					alert(error + response.status + "\n" + response.message);
				}
			);
		};
		
		function returnRequiredPhotos() {
			requiredPhotos = {
				'ok': $("#OK").is(':checked'),
				'nok': $("#NOK").is(':checked'),
				'fix': $("#FIX").is(':checked')		
			}
			
			return requiredPhotos;
		}

		$scope.addDivField = function () {
			addDivField();
		};

		$scope.addDivResources = function () {
			addDivFieldResource();
		};

		function refreshPage() {
			$scope.action = {};
		}

		$scope.filterActionByStatus = function () {
			filterActionByStatus();
		};

		function filterActionByStatus() {
			$scope.actions = $scope.allActions.filter(
				function (action) {
					if ($scope.optActive == "all")
						return true;
					else if ($scope.optActive == "active" && action.active == true)
						return true;
					else if ($scope.optActive == "inactive" && action.active == false)
						return true;

					return false;
				}
			);
			
			setOptionAcitivy($scope.optActive);
			
			if($scope.optActive == 'all')
				document.getElementById('optAll').checked = true;
			else if($scope.optActive == 'active')
				document.getElementById('optActive').checked = true;
			else
				document.getElementById('optInactive').checked = true;
			
		
			$scope.actions = orderObject( $scope.actions, 'description' );
		}

		$scope.retornaClasse = function (objAction) {
			if (objAction.active == true) {
				return 'btnativo';
			} else {
				return 'btninativo';
			}
		};

		$scope.trataAtivoInativo = function (objAction) {
			if (objAction.active == true) {
				return $scope.getLabel('Active');
			} else {
				return $scope.getLabel('Inactive');
			}
		};

		$scope.mudaSituacao = function (objAction) {
			objAction.active = objAction.active == true ? false : true;

			$http.put(config.baseUrl + '/admin/updateAction/' + objAction.actionId, objAction).then(
				function (response) {
					alert($scope.getMessage('DataSavedSuccessfully'));
					loadActions();
				},
				function (response) {
					alert($scope.getMessage('ErrorSavingDataWithoutParam'));
				}
			);
		};

		function loadActions() {
			$http.get(config.baseUrl + "/admin/action").then(
				function (response) {
					$scope.allActions = response.data;
					
					$scope.query = getTextActivity();
					$scope.optActive = getOptionAcitivy().length <= 0 ? 'active' : getOptionAcitivy();
					
					filterActionByStatus();
				},
				function (response) {
					alert($scope.getMessage('ErrorLoadData'));
				}
			);
		}
		
		$scope.setTextActivity = () => {
			setTextActivity(document.getElementById('query').value);
		}
	
});