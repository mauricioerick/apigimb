appGIMB.controller("settlementController", function ($scope, $rootScope, $http, geralAPI, $location, juiceService, $routeParams, config, $q) {

	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');
	if (!controllerHeadCheck($scope, $location, juiceService)) return;

	$scope.pathImg = config.pathImg;
	$('.ipt').attr('disabled', 'disabled');
	$('#divMenu').show();
	// $("body").css("overflowY", "auto");

	changeScreenHeightSize('#divTableSettlement')

	$scope.settlement = {};
	$scope.settlements = [];
	$scope.title = $scope.getLabel('Settlements.p');
	$scope.itemEmpty = {};
	$scope.User = {};
	$scope.listaImagensPrintLiq = [];
	var reloadSettlementFalse = false;
	$scope.array = []
	$scope.srcImageOK = 'apiGIMB/view/imagesCheckList/ok.png';
	$scope.srcImageNOK = 'apiGIMB/view/imagesCheckList/nok.png';
	$scope.customFieldList = [];

	if (document.getElementById('dtInicio'))
		document.getElementById('dtInicio').readOnly = true;
		
	if (document.getElementById('dtFinal'))
		document.getElementById('dtFinal').readOnly = true;
	
	var today = new Date();
	var dt = new Date();
	var yesterday = new Date(dt.setDate(dt.getDate() - 30));

	var user;

	var canRunReload = false;

	var dtIni = getDataInicioFiltroLiq();
	var dtFin = getDataFinalFiltroLiq();

	var orgSrcImg;

	$scope.dtInicio 					= dtIni.length == 0 ? dateToStringPeriodo(today, "i") : dtIni;
	$scope.dtFinal 						= dtFin.length == 0 ? dateToStringPeriodo(today, "f") : dtFin;
	$scope.loadingSettlementsToAprov 	= false;
	
	$scope.clientsSelected = getClientLiq();
	$scope.usersSelected = getUserLiq();
	$scope.eventsSelected = getEventLiq();
	$scope.typePaymentsSelected = getTypePaymentLiq();

	$scope.clients = [];
	$scope.users = [];
	$scope.events = [];
	$scope.typePayments = [];
	
	$scope.clientsNames = "";
	$scope.usersNames = "";
	$scope.eventsNames = "";
	$scope.typePaymentsNames = "";
	$scope.options = new Array( getLabel('card'), getLabel('User') );
	$scope.selectedCardOrUser = '';
	$scope.cards = [];
	$scope.cardFilterSet = {};

	setDataInicioFiltroLiq($scope.dtInicio);
	setDataFinalFiltroLiq($scope.dtFinal);

	if (!$routeParams.settlementId) 
		load();
	else {
		$rootScope.scrollTopSettlementId = $routeParams.settlementId;  
		loadSettlement($routeParams.settlementId);
	}

	$scope.showApproveSettlementButton = function () {
		let user = JSON.parse(localStorage.bobby);
		let configWeb = JSON.parse(user.profile.configWeb);
		return configWeb != null && configWeb.APPROVESETTLEMENTACCESS == 1;
	};

	function load() {
		$('#modal_carregando').modal('show');
		
		var params = {};
		params["startDate"] = $scope.dtInicio;
		params["endDate"] = $scope.dtFinal;
		params["user"] = JSON.parse(localStorage.bobby);
		
		setDataInicioFiltroLiq(params["startDate"]);
		setDataFinalFiltroLiq(params["endDate"]);
		setClientLiq($scope.clientsSelected);
		setUserLiq($scope.usersSelected);
		setEventLiq($scope.eventsSelected);
		setTypePaymentLiq($scope.typePaymentsSelected);
		
		$scope.settlements = $rootScope.settlements;
		if ($scope.settlements == null || $scope.settlements.length == 0) {
			$http({
				method: 'PUT',
				url: config.baseUrl + "/settlementWeb",
				data: params
			}).then(function (response) {
				$scope.settlements = response.data;
				$rootScope.settlements = $scope.settlements; 
				
				processData();
			}, function (response) {
				alert($scope.getMessage('ErrorLoadData'));
	
				$('#modal_carregando').modal('hide');
			});
		} else {
			processData();
			setTimeout(() => {
				var container = document.getElementById('tbSettlements');
				var row = document.getElementById($rootScope.scrollTopSettlementId);
				
				if (container && row) {
					$('#tbSettlements').animate({
						scrollTop: row.offsetTop
					}, 800, null);
					
					var rowDivName = '#'+$rootScope.scrollTopSettlementId;
					$(rowDivName).css({ transform: 'scale(1.05, 1.05)', backgroundColor: '#d3d3d3', 'fontWeight': 'bold' });
					setTimeout(() => {
						$(rowDivName).css({ transform: 'scale(1, 1)', backgroundColor: '', 'fontWeight': '' });
						$rootScope.scrollTopSettlementId = null;
					}, 800);
				}
			}, 500);
		}
	}
	
	function processData() {
		canRunReload = true;
		if($scope.settlements != null && $scope.settlements.length > 0){
			listFilter();
			fillLabelsParameters();
			fillLists();
		}	
		loadDatePicker();
		$('#modal_carregando').modal('hide');
	}

	function loadSettlement(settlementId) {

		getAndConfigUser();

		idSettlementForBack = settlementId;
		$http.get(config.baseUrl + "/settlement/" + settlementId)
			.then(function (response) {
				$scope.settlement = response.data;

				for (custom of $scope.settlement.customFieldList) {
					if (custom.customFieldValue.indexOf("[{") != -1) {
						var arr = custom.customFieldValue;
						$scope.array = JSON.parse(arr);
					} else {
						$scope.customFieldList.push(custom)
					}
				}

				imputImages();

				{ // Config data to print
					$scope.company = JSON.parse(localStorage.getItem("company"));
					$scope.reportNumber = $scope.settlement.settlementId;
					$scope.reportDate = $scope.settlement.date;
				}

				var count = 0;

				if ($scope.itemEmpty) {
					$scope.itemEmpty.latitude = 0;
					$scope.itemEmpty.longitude = 0;

					$scope.itemEmpty.message = '';
					$scope.itemEmpty.pic = null;

					$scope.itemEmpty.picMime = 'jpg';
					$scope.itemEmpty.picName = '';
					$scope.itemEmpty.picPath = '/imagens/default/bkg_white.jpg';
				}

				$($scope.settlement.customPictureList).each(function (idx, el) {
					if (count == 20) {
						count = 0;

						for (var i = 0; i < 4; i++) {
							$scope.listaImagensPrintLiq.push($scope.itemEmpty);
						}

						$scope.listaImagensPrintLiq.push(el);
						count++;
					} else {
						$scope.listaImagensPrintLiq.push(el);
						count++
					}
				});

				$scope.settlement.userLogin = user;

			}, function (error) {
				console.log(error);
			});
	}

	$scope.returnDescription = function (description) {
		if (description === 'Cartão de Crédito') {
			return getLabel('CreditCard');
		} else if (description === 'Espécie') {
			return getLabel('Species');
		} else {
			return '';
		}
	};

	$scope.moveImageToPrevSettlement = function () {
		moveImageTo('prev', $("#imageDetailSettlement"), $scope.listaImagensPrintLiq);
	}

	$scope.moveImageToNextSettlement = function () {
		moveImageTo('next', $("#imageDetailSettlement"), $scope.listaImagensPrintLiq);
	}

	$scope.rotateMenosSettlement = function () {
		rotateImage('-90', $scope, $("#imageDetailSettlement"), $("#divModalDD"), $('#modalContent'));
	}

	$scope.rotateMaisSettlement = function () {
		rotateImage('90', $scope, $("#imageDetailSettlement"), $("#divModalDD"), $('#modalContent'));
	}

	$scope.downloadImage = function () {
		downloadImg();
	}


	$scope.verificaCarregamentoDeleteSettlement = function (cs) {
		return cs.DELETEPICSETTLEMENTE == '1';
	}

	$scope.verificaCarregamentoTrocaSettlement = function (cs) {
		return cs.CHANGEPICSETTLEMENTE == '1';
	}

	$scope.verificaCarregamentoEditarSettlement = function (cs) {
		return cs.EDITPICSETTLEMENTE == '1';
	}

	$scope.dadosMapa = function (settlement) {
		var points = [{ "lat": settlement.latitude, "lng": settlement.longitude, "tipo": "Registro" }];
		mapDataCustomPictures(settlement.customPictureList, points);
	}

	$scope.veridicarBtn = function () {
		var user = JSON.parse(localStorage.getItem("bobby"));
		var configWeb = (user.profile != null && user.profile.configWeb != null && user.profile.configWeb.length > 0) ? JSON.parse(user.profile.configWeb) : [];

		return (configWeb.INSERTPICSETTLEMENTE == '0')
	}

	$scope.reload = function () {
		if (canRunReload) {
			setDataInicioFiltroLiq(document.getElementById("dtInicio").value);
			setDataFinalFiltroLiq(document.getElementById("dtFinal").value);

			load();
		}
	}

	$scope.setText = function ($event) {
		setTextoFiltroLiq(event.target.value);
	}

	$scope.loadSettlement = function (id) {
		loadSettlement(id);
	}


	$scope.deletePic = function () {
		$('#modal_carregando').modal('show');

		deleteImage(document.getElementById('imageDetailSettlement').getAttribute('src'),
			$scope.listaImagensPrintLiq,
			config,
			$http,
			$scope,
			$('#modal_carregando')
		);
	}

	function imputImages() {
		$('#imgInput').change(function () {
			orgSrcImg = document.getElementById('imageDetailSettlement').src;

			readURL(this,
				$('#imageDetailSettlement'),
				document.getElementById('imageDetailSettlement').src,
				$http,
				$scope.listaImagensPrintLiq,
				config,
				$('#modalConfirmUpdateImage'));
		});

		$('#newImgSettlement').change(function () {
			abreModalNovaImg($('#ImgNew'), $('#modalNewImg'), this.files)

			var $el = $('#newImgSettlement');
			$el.wrap('<form>').closest('form').get(0).reset();
			$el.unwrap();
		});
	}

	$scope.trocaPic = function () {
		var picId;

		$('#modal_carregando').modal('show');

		for (img of $scope.listaImagensPrintLiq) {
			if (orgSrcImg == img.picPath) {
				picId = img.custompictureId;
				break;
			}
		}

		trocaImg(picId,
			config,
			$http,
			$scope,
			null,
			$q).then(function (data) {
				$('#modal_carregando').modal('hide');

				setTimeout(() => {
					window.location.reload();
				}, 200);
			});
	}

	$scope.addPic = function () {
		$('#modal_carregando').modal('show');

		addPicture(document.getElementById('title_image'),
			$routeParams.settlementId,
			config,
			$http,
			"LQ",
			$scope,
			$('.imgService'),
			$scope.listaImagensPrintLiq,
			$('#modal_carregando')
		);
	}

	function downloadImg() {
		$scope.objId = $scope.listaImagensPrintLiq[idxImg].custompictureId;

		var cropImage = document.getElementById('cropImageSettlement');

		if (cropImage.href.indexOf($scope.objId) != -1) cropImage.href = cropImage.href;
		else cropImage.href += $scope.objId;

		$('#modalImgDetail').modal('hide');

		setTimeout(() => {
			cropImage.click();
		}, 400);
	}

	$scope.setImageDetailSettlement = function (obj, index) {
		setImageDetailSettlement(obj, index, $scope.listaImagensPrintLiq);
	}

	//Modal Pending Settlements
	$scope.userFilterSet = {};
	$scope.eventFilterSet = {};

	$scope.openModalPendingSettlment = function () {
		carregarOcorrencias();
		loadUsers();
		loadCards();
		
		//abrir modal
		$('#modalPendingSettlements').modal();
	};

	function carregarOcorrencias() {
		$http.get(config.baseUrl + "/admin/event").then(
			function (response) {
				$scope.events = response.data.sort((a, b) => a.description.toLowerCase().localeCompare(b.description.toLowerCase()));
			}, function (response) {
				alert($scope.getMessage('ErrorLoadData'));
			}
		);
	}
	
	function loadCards() {
		$http.get(config.baseUrl + "/admin/allCards")
			.then(function (response) {
				$scope.cards = response.data;
			}, function (response) {
				alert($scope.getMessage('ErrorLoadData'));
		});
	}

	function loadUsers() {
		$http.get(config.baseUrl + "/admin/user").then(function (response) {
			$scope.users = response.data.sort((a, b) => a.user.toLowerCase().localeCompare(b.user.toLowerCase()));
		}, function (response) {
			alert($scope.getMessage('ErrorLoadData'));
		});
	}

	$scope.searchSettlements = function () {
		loadPendingExtract();
	};

	function loadPendingExtract() {
		$scope.loadingSettlementsToAprov = true;
		
		var header = {
			userId: $scope.userFilterSet.userId,
			eventId: $scope.eventFilterSet.eventId,
			cardId: $scope.cardFilterSet.cardId
		};
		
		$http({
			method: 'GET',
			url: config.baseUrl + "/admin/extract/find-all-pending",
			headers: header
		}).then(
			function (response) {
				$scope.pendingExtract 				= response.data;
				$scope.loadingSettlementsToAprov 	= false;
			},
			function (response) {
				// do nothin
				$scope.loadingSettlementsToAprov = false;
			}
		);
	}

	$scope.formatMoney = function (value) {
		return formatMoney(value);
	};

	$scope.addToApproveDisapproveList = function () {
		$scope.pendingExtract.forEach(function (pendingExtract) {
			pendingExtract.settlement.checked = !pendingExtract.settlement.checked;
		});
	};

	$scope.approve = function () {
		$scope.pendingExtract.forEach(function (pendingExtract) {
			if (pendingExtract.settlement.checked) {
				pendingExtract.status = 'A';
			}
		});

		updateExtractList();
	};

	$scope.disapprove = function () {
		$scope.pendingExtract.forEach(function (pendingExtract) {
			if (pendingExtract.settlement.checked) {
				pendingExtract.status = 'R';
			}
		});

		updateExtractList();
	};

	function updateExtractList() {
		const url = config.baseUrl + '/admin/extract/update-extract-list';

		$http.put(url, $scope.pendingExtract).then(
			function (response) {
				if (response.status >= 200 && response.status <= 299) {
					alert($scope.getMessage('DataSavedSuccessfully'));
				} else {
					alert(error + response.status + "\n" + response.message);
				}

			},
			function (response) {
				alert(error + response.status + "\n" + response.message);
			}
		);
	}

	$scope.statusByFull = function (status) {
		switch (status) {
			case "A":
				return getLabel('Approved');
			case "P":
				return getLabel('Pending');
			case "R":
				return getLabel('Disapproved');

			default:
				break;
		}
	};
	
	$scope.showModalWindow = () => {
		fillLists();
		$("#parametersModal").modal({ backdrop: 'static', keyboard: false });
	}
	
	
	function fillLists(){
		for(var settlement of $scope.settlements){
			if (settlement.client && settlement.user && settlement.typePayment && settlement.event) {
				if($scope.clients.indexOf(settlement.client.tradingName) <= -1)
					$scope.clients.push(settlement.client.tradingName);
			
				if($scope.users.indexOf(settlement.user.name) <= -1)
					$scope.users.push(settlement.user.name);
				
				if($scope.typePayments.indexOf(settlement.typePayment.description) <= -1)
					$scope.typePayments.push(settlement.typePayment.description);
				
				if($scope.events.indexOf(settlement.event.description) <= -1)
					$scope.events.push(settlement.event.description);
			} 
		}
	}
	
	$scope.buscar = () => {
		$rootScope.settlements = null;
		load();
	}
	
	$scope.showClientsSelect = () => {
		$scope.showClient = !$scope.showClient;
		
		if($scope.showClient)
			$("#clientTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
		else 
			$("#clientTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
	}
	
	$scope.showUserSelect = () => {
		$scope.showUsers = !$scope.showUsers;
		
		if($scope.showUsers)
			$("#userTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
		else 
			$("#userTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
	}
	
	$scope.showEventsSelect = () => {
		$scope.showEvents = !$scope.showEvents;
		
		if($scope.showEvents)
			$("#eventsTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
		else 
			$("#eventsTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
	}
	
	$scope.showTypePaymentsSelect = () => {
		$scope.showTypePayments = !$scope.showTypePayments;
		
		if($scope.showTypePayments)
			$("#typePaymentsTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
		else 
			$("#typePaymentsTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
	}
	
	function listFilter(){
		let listaSettlementFiltrada = [];
		
		$scope.settlements.forEach(settlement => {
			listaSettlementFiltrada.push(settlement);
			let removeElemento = false;
			
			if($scope.clientsSelected.length > 0){
				let idx = 1;
				
				$scope.clientsSelected.forEach(client => {
					if(settlement.client.tradingName == client) return true;
					if(settlement.client.tradingName != client && idx == $scope.clientsSelected.length){
						let indexSettlement = listaSettlementFiltrada.indexOf(settlement);
						listaSettlementFiltrada.splice(indexSettlement);
						removeElemento = true;
					}
					idx++;
				});
			}
			
			if($scope.usersSelected.length > 0 && !removeElemento){
				let idx = 1;
				
				$scope.usersSelected.forEach(user => {
					if(settlement.user.name == user) return true;
					if(settlement.user.name != user && idx == $scope.usersSelected.length){
						let indexSettlement = listaSettlementFiltrada.indexOf(settlement);
						listaSettlementFiltrada.splice(indexSettlement);
						removeElemento = true;
					}
					idx ++;
				});
			}
			

			if($scope.eventsSelected.length > 0 && !removeElemento){
				let idx = 1;
				
				$scope.eventsSelected.forEach(event => {
					if(settlement.event.description == event) return true;
					if(settlement.event.description != event && idx == $scope.eventsSelected.length){
						let indexSettlement = listaSettlementFiltrada.indexOf(settlement);
						listaSettlementFiltrada.splice(indexSettlement);
						removeElemento = true;
					}
					idx++;
				});
			}


			if($scope.typePaymentsSelected.length > 0 && !removeElemento){
				let idx = 1;
				
				$scope.typePaymentsSelected.forEach(typePayment => {
					if(settlement.typePayment.description == typePayment) return true;
					if(settlement.typePayment.description != typePayment && idx == $scope.typePaymentsSelected.length){
						let indexSettlement = listaSettlementFiltrada.indexOf(settlement);
						listaSettlementFiltrada.splice(indexSettlement);
						removeElemento = true;
					}
					idx ++;
				});
			}
			
		});
		
		fillLabelsParameters();
		
		$scope.settlements = [];
		listaSettlementFiltrada.forEach(settlement => $scope.settlements.push(settlement));
		
		$scope.settlements = sortSettlementByDate( $scope.settlements );
	}
	
	function sortSettlementByDate( list ) {
		list.sort( (a, b) => {
			
			const date_a = a['date'].split('/');
			const date_b = b['date'].split('/');
			
			const compare_a = new Date( date_a[2], date_a[1] - 1, date_a[0] ); 
			const compare_b = new Date( date_b[2], date_b[1] - 1, date_b[0] );
				
			if (compare_a > compare_b) {
				return -1;
			}
			
			if (compare_a < compare_b) {
				return 1;
			}
			
			return 0;
		});
		
		return list;
	}
	
	function fillLabelsParameters(){
		$scope.clientsNames = "";
		if($scope.clientsSelected.length > 0){
			for(i in $scope.clientsSelected){
				var client = $scope.clientsSelected[i];
				if(!$scope.clientsNames.includes(client))
					$scope.clientsNames += $scope.clientsNames.length == 0 ? client : ", "  + client;
			}
		}
			
		$scope.usersNames = "";
		if($scope.usersSelected.length > 0){
			for(i in $scope.usersSelected){
				var user = $scope.usersSelected[i];
				if(!$scope.usersNames.includes(user))
					$scope.usersNames += $scope.usersNames.length == 0? user : ", " + user;
			}
		}

		$scope.eventsNames = "";
		if($scope.eventsSelected.length > 0){
			for(i in $scope.eventsSelected){
				var event = $scope.eventsSelected[i];
				if(!$scope.eventsNames.includes(event))
					$scope.eventsNames += $scope.eventsNames.length == 0? event : ", " + event;
			}
		}

		$scope.typePaymentsNames = "";
		if($scope.typePaymentsSelected.length > 0){
			for(i in $scope.typePaymentsSelected){
				var typePayment = $scope.typePaymentsSelected[i];
				if(!$scope.typePaymentsNames.includes(typePayment))
					$scope.typePaymentsNames += $scope.typePaymentsNames.length == 0? typePayment : ", " + typePayment;
			}
		}
	}
	
	function loadDatePicker(){
		$("#dtInicio").datepicker(datePickerOptions);
		$("#dtFinal").datepicker(datePickerOptions);
	} 
	
	function editDateSettlement( configWeb ) {
		if(configWeb['CHANGEDATESETTLEMENT'] == 1){
			$('#date').removeClass('ipt');
			$('#date').removeAttr("disabled");
			$("#date").datepicker(datePickerOptions);
		}
	}

	function getAndConfigUser() {
		var user = JSON.parse(localStorage.bobby);
		this.user = user
		
		$scope.config_web = 
			(user.profile != null && user.profile.configWeb != null && user.profile.configWeb.length > 0) 
			? JSON.parse(user.profile.configWeb) 
			: [];
		
		editDateSettlement( $scope.config_web );
	}
	
	$scope.saveSettlement = () => {
		$http.put(config.baseUrl + '/updateSettlement/' + $scope.settlement.settlementId, $scope.settlement)
		.then(
				function(response) {
					
					alert($scope.getMessage('DataSavedSuccessfully'));
					
				}, function(response){
					alert(error + response.status + "\n" + response.message);s
				}
		);
	}
	
});
