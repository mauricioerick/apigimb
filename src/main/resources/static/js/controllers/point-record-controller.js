appGIMB.controller("pointRecordController", function ($scope, $http, $location, juiceService, $routeParams, config) {

	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');
	if (!controllerHeadCheck($scope, $location, juiceService)) return;

	$scope.pathImg = config.pathImg;
	$('.ipt').attr('disabled', 'disabled');
	$('#divMenu').show();
	// $("body").css("overflowY", "auto");

	dateFlatPickrOptions.locale = localStorage["language"];

	// changeScreenHeightSize('#divTableSettlement');

	$scope.novo = false;
	$scope.pointRecord = {};
	$scope.allPointRecords = [];
	$scope.pointRecords = [];
	$scope.optBy = getSituationPointRecord().length <= 0 ? 'all' : getSituationPointRecord();

	let today = new Date();
	let user = JSON.parse(localStorage.bobby);
	
	const dtIni = getDataFinalPointRecord();
	const dtFin = getDataInitialPointRecord();

	$scope.dtInicio = dtIni.length <= 0 ? dateToStringPeriodo(today, "i") : dtIni;
	$scope.dtFinal = dtFin.length <= 0 ? dateToStringPeriodo(today, "f") : dtFin;
	
	$scope.users = [];
	$scope.clients = [];
	
	$scope.usersSelected = getUsersPointRecord();
	$scope.clientsSelected = getClientsPointRecord();
	
	$scope.usersNames = "";
	$scope.clientsNames = "";
	$scope.statusNames = "";
	$scope.showUsers = false;
	$scope.showClients = false;
	
	document.getElementById('dtInicio').readOnly = true;
	document.getElementById('dtFinal').readOnly = true;

	if (!$routeParams.pointRecordId)
		load();
	else if ($routeParams.pointRecordId != 'novo')
		loadPointRecord($routeParams.pointRecordId);
	else
		novo();

	function novo() {
		$http.get(config.baseUrl + "/admin/user/" + user.userId).then(
			function (response) {
				$scope.pointRecord.user = response.data;
			}, function (response) {
				alert($scope.getMessage('ErrorLoadData'));
			}
		);

		$scope.novo = true;
		$scope.pointRecord = {};
		$scope.pointRecord.startTime = timestampNowDDMMYYYY();
		$scope.pointRecord.endTime = null;
		$scope.pointRecord.latStartTime = 0;
		$scope.pointRecord.latEndTime = 0;
		$scope.pointRecord.lgtStartTime = 0;
		$scope.pointRecord.lgtEndTime = 0;
		$scope.pointRecord.status = 'N';
		$scope.pointRecord.eventsList = [];

		carregaSeForNovo();
	}

	function carregaSeForNovo() {
		timeFlatPickrOptions.locale = localStorage["language"];
		timeFlatPickrOptions.onChange = function (selectedDates, dateStr, instance) { formatDateTimeValue(selectedDates, dateStr, instance); };
		timeFlatPickrOptions.onClose = function (selectedDates, dateStr, instance) { formatDateTimeValue(selectedDates, dateStr, instance); };
		timeFlatPickrOptions.onOpen = function (selectedDates, dateStr, instance) {
			if (instance.input.id == 'inicio') {

				instance.config.defaultHour = $scope.pointRecord.startTime.substring(11, 13);
				instance.config.defaultMinute = $scope.pointRecord.startTime.substring(14, 16);

				if ($scope.pointRecord.endTime != null) {
					instance.config.minTime = null;
					instance.config.maxTime = $scope.pointRecord.endTime.substring(11, 16);
				}

			} else if (instance.input.id == 'final') {
				instance.config.defaultHour = $scope.pointRecord.startTime.substring(11, 13);
				instance.config.defaultMinute = $scope.pointRecord.startTime.substring(14, 16);
				instance.config.minTime = $scope.pointRecord.startTime.substring(11, 16);
				instance.config.maxTime = null;

			} else if (instance.input.id == 'inicioEvento') {

				instance.config.defaultHour = $scope.pointRecord.startTime.substring(11, 13);
				instance.config.defaultMinute = $scope.pointRecord.startTime.substring(14, 16);
				instance.config.minTime = $scope.pointRecord.startTime.substring(11, 16);

				if ($scope.pointRecord.endTime != null) {
					instance.config.maxTime = $scope.pointRecord.endTime.substring(11, 16);
				}

			} else if (instance.input.id == 'finalEvento') {

				if ($scope.pointEvent.startTime != null && $scope.pointEvent.startTime.length > 0) {
					instance.config.defaultHour = $scope.pointEvent.startTime.substring(11, 13);
					instance.config.defaultMinute = $scope.pointEvent.startTime.substring(14, 16);
					instance.config.minTime = $scope.pointEvent.startTime.substring(11, 16);
				} else {
					instance.config.defaultHour = $scope.pointRecord.startTime.substring(11, 13);
					instance.config.defaultMinute = $scope.pointRecord.startTime.substring(14, 16);
					instance.config.minTime = $scope.pointRecord.startTime.substring(11, 16);
				}

				if ($scope.pointRecord.endTime != null) {
					instance.config.maxTime = $scope.pointRecord.endTime.substring(11, 16);
				}

			}
		};

		$('#inicio').removeAttr('disabled');
		$('#inicio').flatpickr(timeFlatPickrOptions);
		$('#inicio').css({ 'background-color': '#ffffff', 'color': '#555' });

		$('#final').removeAttr('disabled');
		$('#final').flatpickr(timeFlatPickrOptions);
		$('#final').css({ 'background-color': '#ffffff', 'color': '#555' });

		$('#observation').removeAttr('disabled');

		$('#inicioEvento').flatpickr(timeFlatPickrOptions);
		$('#finalEvento').flatpickr(timeFlatPickrOptions);

		$http.get(config.baseUrl + "/admin/eventByActive/true").then(
			function (response) {
				$scope.eventList = response.data;
			}, function (response) {
				alert($scope.getMessage('ErrorLoadData'));
			}
		);
	}

	$scope.createUrlDownload = () => {

		const initDate = $scope.dtInicio;
		const finalDate = $scope.dtFinal;

		const domain = 'POINT';

		return config.baseUrl
			+ '/export-excel/' + domain
			+ '?startDate=' + initDate
			+ '&endDate=' + finalDate
			+ '&users=' + $scope.usersSelected
			+ '&user=' + (JSON.parse(localStorage.bobby).userId | '')
			+ '&language=' + localStorage["language"];
	};

	//Trazer todos os registros de Ponto
	function load() {
		$('#modal_carregando').modal('show');

		var params = {};
		params["startDate"] = $scope.dtInicio;
		params["endDate"] = $scope.dtFinal;
		params["user"] = JSON.parse(localStorage.bobby);

		setUsersPointRecord($scope.usersSelected);
		setClientsPointRecord($scope.clientsSelected);
		setSituationPointRecord($scope.optBy);
		setDataFinalPointRecord($scope.dtInicio);
		setDataInitialPointRecord($scope.dtFinal);
		
		$http({
			method: 'PUT',
			url: config.baseUrl + "/transfersByDateBetween",
			data: params
		}).then(function (response) {
			$scope.allPointRecords = response.data;
			$scope.filterPointRecordBy();
			fillLabelsParameters();
			fillLists();
			loadDatePicker();
			
			$('#modal_carregando').modal('hide');
		}, function (response) {
			alert($scope.getMessage('ErrorLoadData'));

			$('#modal_carregando').modal('hide');
		});
	}

	$scope.filterPointRecordBy = function () {
		$scope.pointRecords = $scope.allPointRecords.filter(function (pr) {
			if ($scope.optBy == "all")
				return true;
			else if (pr.status == $scope.optBy)
				return true;

			return false;
		});
		
		setSituationPointRecord($scope.optBy);

		if ($scope.optBy == "all")
			document.getElementById('filterAll').checked = true;
		else if ($scope.optBy == 'P')
			document.getElementById('filterOpened').checked = true;
		else if ($scope.optBy == 'T')
			document.getElementById('filterFinalized').checked = true;
		
		filtrarLista();
	};

	$scope.verificaPontoPendente = function () {
		$http.get(config.baseUrl + '/ponto-aberto-usuario/' + user.userId).then(
			function (response) {
				if (!response.data)
					$location.path(juiceService.appUrl + 'point-record/novo');
				else
					alert('Já existe um ponto em aberto para esse usuário!');
			}, function (response) {
				alert($scope.getMessage('ErrorLoadData'));
			}
		);
	};

	$scope.limpaHora = function (limparPonto) {
		let date = timestampNowDDMMYYYY();
		if (limparPonto) {

			if ($scope.pointRecord.status === 'N') {
				document.querySelector("#inicio")._flatpickr.setDate(date.substring(0, date.length - 3));
				$scope.pointRecord.startTime = date;
			}


			document.querySelector("#final")._flatpickr.clear();

		} else {

			if (!$scope.editandoEvento) {
				document.querySelector("#inicioEvento")._flatpickr.setDate(date.substring(0, date.length - 3));
				$scope.pointEvent.startTime = date;
			}

			document.querySelector("#finalEvento")._flatpickr.clear();
		}
	};

	$scope.verificaUsuario = function (pointRecord) {
		let user = JSON.parse(localStorage.bobby);
		if (user.userId != pointRecord.user.userId && pointRecord.endTime == null)
			alert(getMessage('UserNotMatchPointRecord'));
		else
			$location.path(juiceService.appUrl + 'point-record/' + pointRecord.transferId);
	};

	$scope.dadosMapa = function (settlement) {
		var points = [{ "lat": settlement.latitude, "lng": settlement.longitude, "tipo": "Registro" }];
		mapDataCustomPictures(settlement.customPictureList, points);
	};

	//Point Record Details
	function loadPointRecord(pointRecordId) {
		$http.get(config.baseUrl + "/transfer/" + pointRecordId)
			.then(function (response) {
				$scope.pointRecord = response.data;
				$scope.novo = $scope.pointRecord.status === 'P';

				if ($scope.novo) carregaSeForNovo();

				{ // Config data to print
					$scope.company = JSON.parse(localStorage.getItem("company"));
					$scope.reportNumber = $scope.pointRecord.transferId;
					$scope.reportDate = $scope.pointRecord.startTime.substring(0, 10);
				}

			}, function (error) {
				//mensagem de erro
			});
	}

	$scope.dateTimeFormat = function (dateTime) {
		if (dateTime != null && dateTime.length > 0 && dateTime.length >= 15)
			return dateTime.substring(0, (dateTime.length - 3));

		return dateTime;
	};

	$scope.hidePointEventsDiv = function () {
		return ($scope.pointRecord != null && $scope.pointRecord.eventsList != null && $scope.pointRecord.eventsList.length > 0) || $scope.novo;
	};

	$scope.openObservacaoModal = function (pointEvent) {
		$scope.note = pointEvent.note;
	};

	$scope.dadosMapa = function (pointRecord) {
		dadosMapa(pointRecord.eventsList, pointRecord);
	};

	$scope.reload = function (point) {
		load();
	};

	$scope.novoEvento = function () {
		$scope.pointEvent = {};
		$scope.editandoEvento = false;

		novoOuPodeEditarEvento();

		$scope.pointEvent.startTime = $scope.pointRecord.startTime;

		$('#modalEventoPonto').modal();
	};

	$scope.editaEvento = function (pointEvent) {
		$scope.pointEvent = pointEvent;

		$scope.editandoEvento = true;

		novoOuPodeEditarEvento();

		$('#modalEventoPonto').modal();
	};

	function novoOuPodeEditarEvento() {
		$('#inicioEvento').removeAttr('disabled');
		$('#inicioEvento').css({ 'background-color': '#ffffff', 'color': '#555' });

		$('#finalEvento').removeAttr('disabled');
		$('#finalEvento').css({ 'background-color': '#ffffff', 'color': '#555' });

		$('#observaocaoEventoPonto').removeAttr('disabled');
	}

	function formatDateTimeValue(selectedDates, dateStr, instance) {
		if (instance.input.id == 'inicio') {

			$scope.pointRecord.startTime = dateStr.length > 0 ? dateStr + ':00' : '';

			if ($scope.pointRecord.endTime != null && $scope.pointRecord.endTime.length > 0 &&
				$scope.pointRecord.startTime != null && $scope.pointRecord.startTime.length > 0)
				atualizaTempoGasto(instance);

		} else if (instance.input.id == 'final') {

			$scope.pointRecord.endTime = dateStr.length > 0 ? dateStr + ':00' : '';

			if ($scope.pointRecord.endTime != null && $scope.pointRecord.endTime.length > 0 &&
				$scope.pointRecord.startTime != null && $scope.pointRecord.startTime.length > 0)
				atualizaTempoGasto(instance);

		} else if (instance.input.id == 'inicioEvento') {
			$scope.pointEvent.startTime = dateStr.length > 0 ? dateStr + ':00' : '';

			if ($scope.pointEvent.endTime != null && $scope.pointEvent.endTime.length > 0 &&
				$scope.pointEvent.startTime != null && $scope.pointEvent.startTime.length > 0)
				atualizaTempoGasto(instance);

		} else if (instance.input.id == 'finalEvento') {
			$scope.pointEvent.endTime = dateStr.length > 0 ? dateStr + ':00' : '';

			if ($scope.pointEvent.endTime != null && $scope.pointEvent.endTime.length > 0 &&
				$scope.pointEvent.startTime != null && $scope.pointEvent.startTime.length > 0)
				atualizaTempoGasto(instance);
		}
	}

	function atualizaTempoGasto(instance) {
		if (instance.input.id == 'inicio' || instance.input.id == 'final') {

			$scope.pointRecord.timeElapsed = getTimeElapsed($scope.pointRecord.startTime, $scope.pointRecord.endTime);
			document.getElementById('tempoGasto').value = $scope.pointRecord.timeElapsed;


		} else if (instance.input.id == 'inicioEvento' || instance.input.id == 'finalEvento') {

			$scope.pointEvent.timeElapsed = getTimeElapsed($scope.pointEvent.startTime, $scope.pointEvent.endTime);

		}

	}

	$scope.adicionarEvento = function () {
		if (!validateForm())
			return;

		if ($scope.pointEvent.event.requestKm == null || !$scope.pointEvent.event.requestKm || $scope.pointEvent.kmStart == null || $scope.pointEvent.kmStart.length == 0) {
			$scope.pointEvent.kmStart = 0;
			$scope.pointEvent.kmEnd = 0;
			$scope.pointEvent.kmTraveled = $scope.pointEvent.kmEnd - $scope.pointEvent.kmStart;
		} else {
			$scope.pointEvent.kmStart = $scope.pointEvent.kmStart;
		}


		if ($scope.pointEvent.event.requestKm != null && $scope.pointEvent.event.requestKm && $scope.pointEvent.event.requestKm && $scope.pointEvent.kmStart == 0) {
			alert(getMessage('NeedKm') + getLabel('KMStart'));
			return;
		}

		$scope.pointEvent.latitude = 0;
		$scope.pointEvent.longitude = 0;
		$scope.pointRecord.eventsList.push($scope.pointEvent);

		$('#modalEventoPonto').modal('toggle');
	};

	$scope.editarEvento = function () {
		if (!validateForm())
			return;

		if ($scope.pointEvent.event.requestKm == null || !$scope.pointEvent.event.requestKm || $scope.pointEvent.kmEnd == null || $scope.pointEvent.kmEnd.length == 0) {
			$scope.pointEvent.kmEnd = 0;
			$scope.pointEvent.kmTraveled = $scope.pointEvent.kmEnd - $scope.pointEvent.kmStart;
		} else {
			$scope.pointEvent.kmEnd = $scope.pointEvent.kmEnd;
			$scope.pointEvent.kmTraveled = $scope.pointEvent.kmEnd - $scope.pointEvent.kmStart;
		}

		let startMillis = new Date(moment($scope.pointEvent.startTime, 'DD/MM/YYYY HH:mm:ss').format()).getTime();
		let endMillis = $scope.pointEvent.endTime != null ? new Date(moment($scope.pointEvent.endTime, 'DD/MM/YYYY HH:mm:ss').format()).getTime() : 0;

		if (startMillis > endMillis && endMillis > 0) {
			alert(getMessage('StartBiggerThanEnd'));
			return;
		}

		if ($scope.pointEvent.startTime.length > 0 && $scope.pointEvent.startTime.length < 16)
			$scope.pointEvent.startTime = $scope.pointEvent.startTime + ':00';

		if ($scope.pointEvent.endTime != null) {
			if ($scope.pointEvent.endTime.length > 0 && $scope.pointEvent.endTime.length < 16)
				$scope.pointEvent.endTime = $scope.pointEvent.endTime + ':00';

			if ($scope.pointEvent.event.requestKm != null && $scope.pointEvent.event.requestKm && $scope.pointEvent.event.requestKm && $scope.pointEvent.endTime.length > 0 && $scope.pointEvent.kmEnd == 0) {
				alert(getMessage('NeedKm') + getLabel('FinalKm'));
				return;
			}
		}

		$('#modalEventoPonto').modal('toggle');
	};

	$scope.checkParaSalvar = function () {
		if ($scope.pointRecord.startTime.length < 16) {
			alert(getMessage('NeedStartTime'));
		} else if ($scope.pointRecord.endTime != null) {
			if (confirm(getMessage('ClosePoint'))) {

				if (checkHorarioParaSalvar()) salvar();
			}
		} else {
			salvar();
		}
	};

	function checkHorarioParaSalvar() {
		let validado = true;
		let semHoraFinal = false;
		let foraDoPeriodo = false;
		let eventosSemHora = '';
		let eventosForaPeriodo = '';
		if ($scope.pointRecord.eventsList.length > 0) {
			let startMillis = new Date(moment($scope.pointRecord.startTime, 'DD/MM/YYYY HH:mm:ss').format()).getTime();
			let endMillis = new Date(moment($scope.pointRecord.endTime, 'DD/MM/YYYY HH:mm:ss').format()).getTime();

			$scope.pointRecord.eventsList.forEach(transferEvent => {
				if (transferEvent.endTime == null || transferEvent.endTime.length == 0) {

					eventosSemHora += transferEvent.event.description + ', \n';
					semHoraFinal = true;

				} else {

					let startEventoMillis = new Date(moment(transferEvent.startTime, 'DD/MM/YYYY HH:mm:ss').format()).getTime();
					let endEventoMillis = new Date(moment(transferEvent.endTime, 'DD/MM/YYYY HH:mm:ss').format()).getTime();

					if (startEventoMillis < startMillis || endEventoMillis > endMillis) {
						eventosForaPeriodo += transferEvent.event.description + ', \n';

						foraDoPeriodo = true;
					}

				}

			});

			if (semHoraFinal) {
				let msg = getMessage('OpenEventsWithOutEndTime').replace('{eventos}', eventosSemHora);
				alert(msg);
				validado = false;
			}

			if (foraDoPeriodo) {
				let msg = getMessage('EventOutDate').replace('{eventos}', eventosForaPeriodo);
				msg += ' ' + $scope.pointRecord.startTime.substring(0, $scope.pointRecord.startTime.length - 3) +
					' ' + getMessage('and') + ' ' + $scope.pointRecord.endTime.substring(0, $scope.pointRecord.endTime.length - 3);
				alert(msg);
				validado = false;
			}
		}

		return validado;
	}

	function salvar() {

		var url = config.baseUrl + "/createTransfer";

		if ($scope.pointRecord.transferId != null || $scope.pointRecord.transferId > 0)
			url = config.baseUrl + "/update-transfer/" + $scope.pointRecord.transferId;

		if ($scope.pointRecord.endTime != null && $scope.pointRecord.endTime.length > 0)
			$scope.pointRecord.status = 'T';
		else if ($scope.pointRecord.status === 'N')
			$scope.pointRecord.status = 'P';

		$http.put(url, $scope.pointRecord).then(
			function (response) {
				if (response.status >= 200 && response.status <= 299) {
					alert($scope.getMessage('DataSavedSuccessfully'));
					$location.path(juiceService.appUrl + 'point-record');
				} else {
					alert(error + response.status + "\n" + response.message);
				}

			},
			function (response) {
				alert(error + response.status + "\n" + response.message);
			}
		);
	};

	$(document).ready(function () {

		if (!$scope.novo && $scope.pointRecord.eventsList != null && $scope.pointRecord.eventsList.length > 0) initialize();
		// $("html, body").css("overflowY", "auto");

		setTimeout(function () {
			$("#btnMapa").trigger("click");
		}, 2000);
	});
	
	$scope.showModalWindow = () => {
		fillLists();
		$("#parametersModal").modal({ backdrop: 'static', keyboard: false });
	}
	
	function fillLists(){
		for(var pointsRecord of $scope.allPointRecords){
			if(pointsRecord.user != null && $scope.users.indexOf(pointsRecord.user.name) <= -1)
				$scope.users.push(pointsRecord.user.name);
			
			if(pointsRecord.client != null && $scope.clients.indexOf(pointsRecord.client.tradingName) <= -1)
				$scope.clients.push(pointsRecord.client.tradingName);
		}
	}
	
	$scope.showUserSelect = () => {
		$scope.showUsers = !$scope.showUsers; 
		
		if ($scope.showUsers)
			$("#userTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up')
		else
			$("#userTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down')
	}
	
	$scope.showClientsSelect = () => {
		$scope.showClients = !$scope.showClients;
		
		if ($scope.showClients)
			$("#clientTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up')
		else
			$("#clientTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down')
	}
	
	$scope.buscar = () => {
		load();
	}
	
	function filtrarLista(){
		let listaPointRecordsFiltrados = [];
		
		$scope.pointRecords.forEach(pointsRecords => {
			listaPointRecordsFiltrados.push(pointsRecords);
			let removeuElemento = false;
			
			if($scope.usersSelected.length > 0){
				let idx = 1;
				
				$scope.usersSelected.forEach(user => {
					if(pointsRecords.user.name == user) return true;
					if(pointsRecords.user.name != user && idx == $scope.usersSelected.length){
						let indexPointRecord = listaPointRecordsFiltrados.indexOf(pointsRecords);
						listaPointRecordsFiltrados.splice(indexPointRecord);
						removeuElemento = true;
					}
					idx ++;
				});
			}
			
			if($scope.clientsSelected.length > 0){
				let idx = 1;
				
				$scope.clientsSelected.forEach(client => {
					if(pointsRecords.client.tradingName == client) return true;
					if(pointsRecords.client.tradingName != client && idx == $scope.clientsSelected.length){
						let indexPointRecord = listaPointRecordsFiltrados.indexOf(pointsRecords);
						listaPointRecordsFiltrados.splice(indexPointRecord);
						removeuElemento = true;
					}
					
					idx ++;
				});
			}
		});
		
		$scope.pointRecords = [];
		listaPointRecordsFiltrados.forEach(pr => $scope.pointRecords.push(pr));
	}
	
	function fillLabelsParameters(){
		$scope.usersNames = "";
		$scope.clientsNames = "";
		$scope.statusNames = "";
		
		if($scope.usersSelected.length > 0){
			for(i in $scope.usersSelected){
				const user = $scope.usersSelected[i];
				if(!$scope.usersNames.includes(user))
					$scope.usersNames += $scope.usersNames.length == 0 ? user : "," + user;   
			}
		}
		
		if($scope.clientsSelected.length > 0){
			for(i in $scope.clientsSelected){
				const client = $scope.clientsSelected[i];
				if(!$scope.clientsNames.includes(client))
					$scope.clientsNames += $scope.clientsNames.length == 0 ? client : "," + client;   
			}
		}
		
		var filterAll = document.getElementById('filterAll').checked;
		var filterOpened = document.getElementById('filterOpened').checked;
		var filterFinalized = document.getElementById('filterFinalized').checked;
		
		if(filterOpened)
			$scope.statusNames = getLabel('Opened');
		else if(filterFinalized)
			$scope.statusNames = getLabel('Finalized');
		else
			$scope.statusNames = getLabel('All');
	}
	
	
	function loadDatePicker(){
		$("#dtInicio").datepicker(datePickerOptions);
		$("#dtFinal").datepicker(datePickerOptions);
	}
});