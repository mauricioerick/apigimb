appGIMB.controller("editImageController", function ($scope, $http, $location, juiceService, config, $q, $routeParams) {

	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');
	if (!controllerHeadCheck($scope, $location, juiceService)) return;

	$('.ipt').attr('disabled', 'disabled');
	$('#divMenu').show();
	$("body").css("overflowY", "auto");

	$scope.nomeImg = nomeImage;
	$scope.srcImageEdit = '';
	$scope.idSettlement = idSettlementForBack;

	var exibeBotaoCancel = true;
	var newImage = null;
	img = document.getElementById('imageEdit');
	$scope.srcImageEdit = "apiGIMB/view/editImage/carregando.gif";

	img.style.cssText = "margin-left: 270px;";

	if (!$routeParams.objId) alert('não contém o ID');
	else obtemObject($routeParams.objId);

	function obtemObject(idImageSettlement) {
		$http({
			method: 'GET',
			url: config.baseUrl + "/admin/customPicture/" + idImageSettlement,
		}).then(function (response) {
			imageSettlement = response.data;

			downloadImage(config.baseUrl, $scope, $http, $q).then(function (data) {

				$scope.srcImageEdit = base64ImageForEditing;
				img.style.cssText = "margin-left: none;";
				img.onload = function () { callDarkroom(); };
				exibeBotaoCancel = false;
			});

		}, function (response) {
			alert(scope.getMessage('Error'))
		});
	}

	$scope.cancelaAEdicaoSettlement = function () {
		voltar();
	}

	$scope.alterar = function () {
		$('#modalConfirmImageEdite').modal('hide');
		$('#modal_carregando').modal('show');

		trocaImg(idImageForEdit, config, $http, $scope, newImage, $q).then(function (data) {
			$('#modal_carregando').modal('hide');
			voltar();
		});
	}

	function callDarkroom() {
		var dkrm = new Darkroom('#imageEdit', {
			minWidth: 100,
			minHeight: 100,
			maxWidth: 800,
			maxHeight: 800,
			backgroundColor: '#FFF',

			plugins: {
				save: {
					callback: function () {
						this.darkroom.selfDestroy();
						newImage = dkrm.canvas.toDataURL();
						fileStorageLocation = newImage;
						$('#modalConfirmImageEdite').modal('show');
					}
				},
				crop: {
					quickCropKey: 67,
				},
			}
		});
	}

	$scope.voltar = function () {
		voltar();
	}

	function voltar() {
		$location.path(juiceService.appUrl + "settlementDetail/" + imageSettlement.referenceId);

		setTimeout(() => {
			window.location.reload();
		}, 400);
	}

	$scope.testaCarregamento = function () {
		return exibeBotaoCancel;
	}
});
