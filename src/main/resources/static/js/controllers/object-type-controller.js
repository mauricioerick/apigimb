appGIMB.controller("objectTypeController",
	function($scope, $http, $routeParams, $location, config, juiceService, geralAPI, values) {

		$('form').attr('autocomplete', 'off');
		$('input[type=text]').attr('autocomplete', 'off');
		if (!controllerHeadCheck($scope, $location, juiceService))
			return;

		var error = $scope.getMessage('ErrorSavingData');
		var configui = { expNoteDeb: false, required:true, gallery: true};

		numImagens = 1;
		numDivImagens = 1;

		numCampos = 1;
		numDivCampos = 1;

		dictI = {};
		dictF = {};

		$('#divMenu').show();

		changeScreenHeightSize('#divTableObjectType');

		$scope.objectType = { active : true };
		$scope.objectType.colorId = getRandomColor();
		colorSelectorConf('colorSel', $scope.objectType.colorId);
		$scope.objectTypes = [];
		$scope.allObjectTypes = [];
		
		$scope.optActive = "active";

		if ($routeParams.objectTypeId) {
			loadObjectType($routeParams.objectTypeId);
		} else {
			loadObjtecTypes();
		}

		function loadObjectType(objectTypeId) {
			$http.get(config.baseUrl + "/admin/objectType/" + objectTypeId).then(
				function(response) {
					$scope.objectType = response.data;
					if ($scope.objectType.colorId == null)
						$scope.objectType.colorId = getRandomColor();

					colorSelectorConf('colorSel', $scope.objectType.colorId);
											
					prepareLists();
                                            
				},
				function(response) {
					alert($scope.getMessage('ErrorLoadData'));
				}
			);
		}

		function prepareLists() {
			prepareImageLists($scope.objectType, configui, values);
			prepareFieldLists($scope.objectType);
		}

		function loadObjtecTypes() {
			$http.get(config.baseUrl + "/admin/objectType").then(
				function(response) {
					$scope.allObjectTypes = response.data;
					
					$scope.query = getTextObjectType();
					$scope.optActive = getOptionObjectType().length > 0 ? getOptionObjectType() : 'active'; 
					
					filterObjectTypeByStatus();
				}, function(response) {
					alert($scope.getMessage('ErrorLoadData'));
				}
			);
		}

		$scope.saveObjectType = function(obj) {
			if (!validateForm())
				return;

			var url = config.baseUrl + "/admin/createObjectType";
			if (obj.observationId > 0)
				url = config.baseUrl + "/admin/updateObjectType/" + obj.objectTypeId;
			
			
			obj.images = strImages();
			obj.fields = strFields();

			$http.put(url, obj).then(
				function(response) {
					if (response.status >= 200 && response.status <= 299) {
						alert($scope
								.getMessage('DataSavedSuccessfully'));
						refreshPage();
						$location
								.path(juiceService.appUrl + 'objectType');
					} else {
						refreshPage();
						alert(error + response.status + "\n" + response.message);
					}

				},
				function(response) {
					refreshPage();
					alert(error + response.status + "\n" + response.message);
				}
			);
		};

		$scope.addDivImage = function() {
			addDivImage(configui, values);
		};

		$scope.addDivField = function() {
			addDivField();
		};

		function refreshPage() {
			$scope.objectType = {};
		}

		$scope.filterObjectTypeByStatus = function() {
			filterObjectTypeByStatus();
		};

		function filterObjectTypeByStatus() {
			$scope.objectTypes = $scope.allObjectTypes.filter(isTrue);
			
			setOptionObjectType($scope.optActive);
			
			if($scope.optActive == 'active')
				document.getElementById('optActive').checked = true;
			else if ($scope.optActive == 'inactive')
				document.getElementById('optInactive').checked = true;
			else
				document.getElementById('optAll').checked = true;
		}
					
		function isTrue(event){
			if ($scope.optActive == "all")
				return true;
			else if ($scope.optActive == "active" && event.active == true)
				return true;
			else if ($scope.optActive == "inactive" && event.active == false)
				return true;

			return false;
		}

		$scope.trataAtivoInativo = function(objectType) {
			if (objectType.active == true)
				return $scope.getLabel('Active');
			else
				return $scope.getLabel('Inactive');
		};

		$scope.retornaClasse = function(objectType) {
			if (objectType.active == true)
				return 'btnativo';
			else
				return 'btninativo';
		};

		$scope.mudaSituacao = function(obj) {

			obj.active = obj.active == true ? false : true;
			
			$http.put(config.baseUrl + "/admin/updateObjectType/"+ obj.objectTypeId, obj).then(
				function(response) {
					alert($scope.getMessage('DataSavedSuccessfully'));
					loadObjtecTypes();
				},
				function(response) {
					alert($scope.getMessage('ErrorSavingDataWithoutParam'));
				}
			);
		};
		
		document.getElementById('query').onchange = function() {
			setTextObjectType($scope.query);
		}
	}
);