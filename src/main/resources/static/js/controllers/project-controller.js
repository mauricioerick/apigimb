appGIMB.controller("projectController", function ($scope, $http, $routeParams, $location, config, juiceService, geralAPI) {

	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');
	if (!controllerHeadCheck($scope, $location, juiceService))
		return;

	$('#divMenu').show();

	const OPT_ACTIVE = {
		active: 'active',
		all: 'all',
		inactive: 'inactive',
	}

	$scope.state = {
		projectsFiltered: [],
		rawProjects: [],
		querySearch: '',
		optActive: OPT_ACTIVE.active
	};

	getProjects();

	function showModal(value) {
		$('#modal_carregando').modal(value ? 'show' : 'hide');
	}

	async function getProjects() {
		showModal(true)
		const userId = JSON.parse(localStorage.getItem("bobby")).userId;
		
		try {
			await $http
				.get(`${config.baseUrl}/admin/projects/byArea/${userId}`)
				.then(response => process(response));
		} catch (error) {
			console.log(error);
		} finally {
			showModal(false)
		}

		function process(response) {
			(response.status > 400)
				? error(response)
				: success(response)
		}

		function success(response) {
			$scope.state.rawProjects = response.data;
			$scope.state.projectsFiltered = response.data.filter(project => project.active === true);
		}

		function error(response) {
			console.error(`${getMessage('loadProjectsError')} ${response.data.error}`);
		}
	}

	$scope.searchBy = () => {
		const { rawProjects, querySearch, optActive } = $scope.state;

		$scope.state.projectsFiltered = rawProjects.filter(project => {
			switch (optActive) {
				case OPT_ACTIVE.all:
					return project.projectName.includes(querySearch.toUpperCase());

				case OPT_ACTIVE.active:
					return project.active === true && project.projectName.includes(querySearch.toUpperCase())

				case OPT_ACTIVE.inactive:
					return project.active === false && project.projectName.includes(querySearch.toUpperCase())
			}
		});
	}

	$scope.filterProjectsByStatus = () => {
		const { optActive, rawProjects } = $scope.state;

		switch (optActive) {
			case OPT_ACTIVE.active:
				$scope.state.projectsFiltered = rawProjects.filter(project => project.active === true);
				break;

			case OPT_ACTIVE.inactive:
				$scope.state.projectsFiltered = rawProjects.filter(project => project.active === false);
				break;

			case OPT_ACTIVE.all:
				$scope.state.projectsFiltered = rawProjects;
				break;

			default:
				console.log('Projeto sem status')
		}
		
		$scope.state.projectsFiltered = orderObject( $scope.state.projectsFiltered, 'projectName' );
	}

	$scope.setActive = async projectParam => {

		const project = {
			...projectParam,
			active: !projectParam.active
		};

		showModal(true)
		try {
			await $http
				.put(`${config.baseUrl}/admin/projects/${project.projectId}`, project)
				.then(response => process(response));
		} catch (error) {
			console.error('Error message');
		} finally {
			showModal(false)
		}

		function process(response) {
			(response.status > 400)
				? error(response)
				: success(response)
		}

		function success(response) {
			replaceElementInList(response.data, ['rawProjects', 'projectsFiltered']);
			$scope.filterProjectsByStatus();
		}

		function replaceElementInList(element, listToReplace = []) {
			if (listToReplace.length === 0) return;

			listToReplace.forEach(el => {
				const list = Object.getOwnPropertyDescriptor($scope.state, el).value;
				const idxOfElement = list.indexOf(list.find(project => project.projectId === element.projectId));

				$scope.state[el].splice(idxOfElement, 1, element);
			})
		}

		function error(response) {
			console.error(`${getMessage('loadProjectsError')} ${response.data.error}`);
		}
	}
});
