appGIMB.controller("eventController",
	function ($scope, $http, $routeParams, $location, config, juiceService, geralAPI, values) {
		$('form').attr('autocomplete', 'off');
		$('input[type=text]').attr('autocomplete', 'off');
		if (!controllerHeadCheck($scope, $location, juiceService))
			return;

		numImagens = 1;
		numDivImagens = 1;

		$('body').css("overflowY", "hidden");

		numCampos = 1;
		numDivCampos = 1;

		dictI = {};
		dictF = {};

		var configui = { expNoteDeb: true, required: false, gallery: false };

		var error = $scope.getMessage('ErrorSavingData');

		$('#divMenu').show();

		loadAllNatureExpenses();
		changeScreenHeightSize('#divTableEvent');

		$scope.event = { active: true };
		$scope.event.colorId = getRandomColor();
		colorSelectorConf('colorSel', $scope.event.colorId);

		$scope.events = [];
		$scope.allEvents = [];

		$scope.allNatureExpenses = [];
		$scope.natureExpense = ""

		$scope.optActive = "active";

		if ($routeParams.eventId) {
			carregarOcorrencia($routeParams.eventId);
		} else {
			carregarOcorrencias();
		}

		function carregarOcorrencia(eventId) {
			$http.get(config.baseUrl + "/admin/event/" + eventId).then(
				function (response) {
					$scope.event = response.data;

					if ($scope.event.colorId == null)
						$scope.event.colorId = getRandomColor();

					colorSelectorConf('colorSel', $scope.event.colorId);
					appearOnList($scope.event);

					{
						$('.selectpicker').selectpicker('val', $scope.event.appearsOnList);
						$scope.selectChange();
					}
					prepareLists();
				},
				function (response) {
					alert($scope.getMessage('ErrorLoadData'));
				});
		}

		async function loadAllNatureExpenses() {
			await $http.get(config.baseUrl + "/admin/natureExpenseActive/true").then(
				(response) => {
					$scope.allNatureExpenses = response.data;
				},
				(responseError) => {
					console.error(`${$scope.getMessage(ErrorLoadingExpensesNature)} ${responseError}`);
				}
			);
		}

		async function laodNatureExpense(id) {
			await $http.get(`${config.baseUrl}/admin/natureExpense/${id}`).then(
				(response) => {
					$scope.natureExpense = response.data;
				}, (responseError) => {
					console.error(`${$scope.getMessage(ErrorLoadingExpenseNature)} ${responseError}`);
				}
			)
		}

		$scope.verifyNatureExpense = (expense1, expense2) => {
			if ((expense1 === null || expense1 === undefined) || (expense2 === null || expense2 === undefined))
				return ""

			return expense1 === expense2 ? "selected" : "";
		}

		function prepareLists() {
			prepareImageLists($scope.event, configui, values);
			prepareFieldLists($scope.event);
		}

		function carregarOcorrencias() {
			$http.get(config.baseUrl + "/admin/event").then(
				function (response) {
					$scope.allEvents = response.data;
					
					$scope.query = getTextEvent();
					$scope.optActive = getOptionEvent().length > 0 ? getOptionEvent() : 'active';
					
					filterEventByStatus();
				}, function (response) {
					alert($scope.getMessage('ErrorLoadData'));
				}
			);
		}

		$scope.saveEvent = function (obj) {
			if (!validateForm())
				return;

			if (obj.natureExpense == null || obj.natureExpense.natureExpenseId === "")
				obj.natureExpense = null

			var url = config.baseUrl + "/admin/createEvent";
			if (obj.eventId > 0)
				url = config.baseUrl + "/admin/updateEvent/" + obj.eventId;

			obj.images = strImages();
			obj.fields = strFields();

			sendObj = { "language": localStorage.language, "event": obj };

			$http.put(url, sendObj).then(
				function (response) {
					if (response.status >= 200 && response.status <= 299) {
						alert($scope.getMessage('DataSavedSuccessfully'));
						refreshPage();
						$location.path(juiceService.appUrl + 'event');
					} else {
						refreshPage();
						alert(error + response.status + "\n" + response.message);
					}
				},
				function (response) {
					refreshPage();
					alert(error + response.status + "\n" + response.message);
				}
			);
		};

		$scope.selectChange = function () {
			var arrVal = $('.selectpicker').selectpicker('val');
			var achouLiq = false;
			for (var i = 0; i < arrVal.length; i++) {
				if (arrVal[i] == $scope.getLabel('Settlements')) {
					achouLiq = true;
					break;
				}
			}

			var x = document.getElementById("pnlLiquidacao");
			x.style.display = "none";
			if (achouLiq) {
				x.style.display = "block";
			} else {
				document.getElementById("pnlListImagens").innerHTML = "";
				document.getElementById("pnlListFields").innerHTML = "";
			}
		};

		$scope.addDivImage = function () {
			addDivImage(configui, values);
		};

		$scope.addDivField = function () {
			addDivField();
		};

		function refreshPage() {
			$scope.event = {};
		}

		$scope.filterEventByStatus = function () {
			filterEventByStatus();
		};

		function filterEventByStatus() {
			$scope.events = $scope.allEvents.filter(isTrue);
			
			setOptionEvent($scope.optActive);
			
			
			$scope.events = orderObject( $scope.events, 'description' );
		}

		function isTrue(event) {
			if ($scope.optActive == "all")
				return true;
			else if ($scope.optActive == "active" && event.active == true)
				return true;
			else if ($scope.optActive == "inactive" && event.active == false)
				return true;

			return false;
		}

		$scope.trataAtivoInativo = function (objEvent) {
			if (objEvent.active == true)
				return $scope.getLabel('Active');
			else
				return $scope.getLabel('Inactive');
		};

		$scope.retornaClasse = function (objEvent) {
			if (objEvent.active == true)
				return 'btnativo';
			else
				return 'btninativo';
		};

		$scope.mudaSituacao = function (objEvent) {

			objEvent.active = objEvent.active == true ? false : true;

			appearOnList(objEvent);

			sendObj = { "language": localStorage.language, "event": objEvent };

			$http.put(config.baseUrl + "/admin/updateEvent/" + objEvent.eventId, sendObj).then(
				function (response) {
					alert($scope.getMessage('DataSavedSuccessfully'));
					carregarOcorrencias();
				},
				function (response) {
					alert($scope.getMessage('ErrorSavingDataWithoutParam'));
				}
			);
		};

		function appearOnList(event) {
			for (var idx in event.appearsOnList) {
				event.appearsOnList[idx] = getLabel(event.appearsOnList[idx]);
			}
		}
		
		$scope.setTextEvent = () => {
			setTextEvent(document.getElementById('query').value)
		}
});