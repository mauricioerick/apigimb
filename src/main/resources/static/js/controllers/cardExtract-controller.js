appGIMB.controller("cardExtractController", function ($scope, $http, $routeParams, $location, config, juiceService) {

	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');
	if (!controllerHeadCheck($scope, $location, juiceService)) return;

	$("#dtInicio").datepicker(datePickerOptions);
	$("#dtFinal").datepicker(datePickerOptions);
	
	document.getElementById('dtInicio').readOnly = true;
	document.getElementById('dtFinal').readOnly = true;
	
	const today = new Date();
	
	$scope.bannerNames 		 = "";
	$scope.emittingBankNames = "";
	$scope.totalExtract 	 = 0.0;
	
	$scope.showBanners 		= false;
	$scope.showEmittingBank = false;
	
	$scope.banners = [];
	$scope.emittingBanks = [];

	$scope.clientsNames 	= "";
	$scope.activitysNames 	= "";
	$scope.dtInicio 		= "";
	$scope.dtFinal 			= "";
	$scope.clientsSelected  = [];
	$scope.activitysSelected= [];
	$scope.showActivitys 	= false;
	$scope.showClients 		= false;
	$scope.clients 			= [];
	$scope.activitys 		= [];

	const URL = {
		cardExtract: ($location.$$url.split('/')[2] == 'cardExtract') ? true : false,
		cardExtractDetail: ($location.$$url.split('/')[2] == 'cardExtractDetail') ? true : false,
	}

	$scope.search = {
		startDate: getDataInicioFiltroCardExtract().length == 0 ? dateToStringPeriodo(new Date(), "i") : getDataInicioFiltroCardExtract(),
		endDate: getDataFinalFiltroCardExtract().length == 0 ? dateToStringPeriodo(new Date(), "f") : getDataFinalFiltroCardExtract(),
		cardSelected: "",
		filteredCards: [],
		bannersSelected: getBannerCardExtract(),
		emittingBankSelected: getEmittingBankCardExtract()
	};

	$scope.cardExtract = {
		idCardParam: $routeParams.cardId,
		extractList: [],
	}

	$scope.loadExtractCardPerPeriod = async () => {
		$('#modal_carregando').modal('show');

		try {
			var filteredList = await getExtractPerPeriod($scope.search);

			if (URL['cardExtract']){
				var map = new Map();
				moment.locale(localStorage["language"]);
				console.log(localStorage["language"]);
				filteredList.forEach((x)=>{
					var initialDate = moment(x.releaseDate, "DD/MM/YYYY hh:mm:ss");
					var lateDate = moment(x.releaseDate, "DD/MM/YYYY hh:mm:ss");
					if(initialDate.date() < x.card.invoiceCloseDate){
						initialDate.month(initialDate.month()-1);
					} else {
						lateDate.month(lateDate.month()+1);
					}
					initialDate.date(getLastDay(initialDate.month(),initialDate.year()) > x.card.invoiceCloseDate ? x.card.invoiceCloseDate: getLastDay(initialDate.month()));
					lateDate.date(getLastDay(lateDate.month(),lateDate.year()) > x.card.invoiceCloseDate ? x.card.invoiceCloseDate : getLastDay(lateDate.month()))
					var key = initialDate.format("YYYY-MM-DD")+lateDate.format("YYYY-MM-DD")+x.card.cardNumber;
					map.set(key, {
						"startDate" 	: initialDate.format("DD-MM-YYYY"),
						"endDate"		: lateDate.format("DD-MM-YYYY"),
						"period"		: lateDate.format("MMM/YYYY"),
						"card"			: x.card
					});
				});
				$scope.extractCardList = [];
				map.forEach((value, key) => {
					$scope.extractCardList.push(value);
				});
						
				listFilter();
				fillLabelsParameters()
				loadDatePicker();
				fillLists();

			} else {
				$scope.extractCardList 	= filteredList;
				$scope.totalExtract 	= calculeTotalExtract().toFixed(2);
			}

			if (URL['cardExtractDetail']) {
				getClients();
				getActivitys();	
			}

			$scope.expenseVisible = $scope.extractCardList;

		} catch (error) {
			console.log(error);
		}

		$('#modal_carregando').modal('hide')
		$scope.$apply();
	};

	$scope.formatDateToUrl = (date) => {
		return date.replace(new RegExp("/", 'g'),"-");
	};

	async function loadCardExtractPerPeriod() {
		const { startDate, endDate } = $routeParams;

		const card = await getCardById($scope.cardExtract.idCardParam);

		$scope.search = {
			startDate:moment(startDate, "DD-MM-YYYY").format("DD/MM/YYYY"),
			endDate: moment(endDate, "DD-MM-YYYY").format("DD/MM/YYYY"),
			query: "",
			cardSelected: card,
			filteredCards: [],
		};
		$scope.loadExtractCardPerPeriod();
	}


	if (URL['cardExtract']) {
		$scope.loadExtractCardPerPeriod();
	} else {
		loadCardExtractPerPeriod();
	}

	async function getExtractPerPeriod(period) {
		const { startDate, endDate, cardSelected, query } = period;

		if(URL['cardExtract']){
			setDataInicioFiltroCardExtract(startDate);
			setDataFinalFiltroCardExtract(endDate);
			setBannerCardExtract($scope.search.bannersSelected);
			setEmittingBankCardExtract($scope.search.emittingBankSelected);
		}

		const params = {
			startDate: startDate,
			endDate: endDate,
			type: ['D', 'C'],
			status: ['A', 'P'],
			cardExtract: true
		}

		var path = "findAllBetweenReleaseDate";
		if($scope.search.cardSelected){
			path +="Card";
			params.card = cardSelected.cardId;
		}

		const response = await $http({
			method: 'PUT',
			url: `${config.baseUrl}/admin/extract/${path}`,
			data: params
		})

		return response.data;
	}

	async function getCardById(cardId) {
		try {
			const response = await $http({
				method: 'GET',
				url: `${config.baseUrl}/admin/card/${cardId}`,
			});

			return response.data;
		} catch (error) {
			console.error(`${getMessage('ErrorLoadingApiCard')}: ${error}`);
		}
	}

	function applyCardFilter(cardExtractList) {
		return new Promise(resolve => {
			if (!cardExtractList) return;

			if ($scope.search.cardSelected) {
				var card = typeof $scope.search.cardSelected == "string" ? JSON.parse($scope.search.cardSelected) : $scope.search.cardSelected;
				const filterList = cardExtractList.filter(releaseCard => releaseCard.card.cardId === card.cardId);
				resolve(filterList);
			} else {
				resolve(cardExtractList);
			}
		}, reject => {
			reject(console.error(getMessage('errorLoadingCardExtract')));
		})
	}



	function convertDateUsingYearAndMonthOnly(date) {
		try {
			const convertedDate = date.split('-').reverse().join('').substr(0, 6);

			return convertedDate
		} catch (error) {
			console.error(getMessage('erroToConvertDateToStr'));
		}
	}
	
	
	$scope.showModalWindow = () => {
		fillLists();
		$("#parametersModal").modal({ backdrop: 'static', keyboard: false });
	}

	$scope.showModalWindowDetail = _=> {
		loadDatePicker();
		getClients();
		getActivitys();
		$("#parametersModalDetail").modal({ backdrop: 'static', keyboard: false });
	}
	
	function fillLists(){
		$scope.extractCardList.forEach( card => {
			if($scope.banners.indexOf(card.card.cardBanner) <= -1)
				$scope.banners.push(card.card.cardBanner);
			
			if($scope.emittingBanks.indexOf(card.card.emittingBank) <= -1)
				$scope.emittingBanks.push(card.card.emittingBank);
		});
	}

	$scope.showBannerSelect = () => {
		$scope.showBanners = !$scope.showBanners;
		
		if($scope.showBanners)
			$("#bannerTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
		else
			$("#bannerTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
	}
	
	$scope.showEmittingBankSelect = () => {
		$scope.showEmittingBank = !$scope.showEmittingBank;

		if ($scope.showEmittingBank)
			$("#bankTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
		else
			$("#bankTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
	}
	
	function listFilter() {
		let listaCardsFiltrados = [];
		
		$scope.extractCardList.forEach(card => {
			listaCardsFiltrados.push(card);
			let removeuElemento = false;
			
			if($scope.search.bannersSelected.length > 0){
				let idx = 1;
				
				$scope.search.bannersSelected.forEach(banner => {
					if(card.card.cardBanner == banner) return true;
					if(card.card.cardBanner != banner && idx == $scope.search.bannersSelected.length){
						let indexCard = listaCardsFiltrados.indexOf(card);
						listaCardsFiltrados.splice(indexCard);
						removeuElemento = true;
					}
					idx ++;
				});
			}
			
			if($scope.search.emittingBankSelected.length > 0){
				let idx = 1;
				
				$scope.search.emittingBankSelected.forEach( emittingBank => {
					if(card.card.emittingBank == emittingBank) return true;
					if(card.card.emittingBank != emittingBank && idx == $scope.search.emittingBankSelected.length){
						let indexCard = listaCardsFiltrados.indexOf(card);
						listaCardsFiltrados.splice(indexCard);
						removeuElemento = true;
					}
					
					idx ++ ;
				});
			}
				
		});
		
		$scope.extractCardList = [];
		listaCardsFiltrados.forEach(card => $scope.extractCardList.push(card));
	
		fillLabelsParameters();
	}
	
	function fillLabelsParameters(){
		$scope.bannerNames = "";
		$scope.emittingBankNames = "";
		
		if($scope.search.bannersSelected.length > 0)
			$scope.search.bannersSelected.forEach( banner => {
				if(!$scope.bannerNames.includes(banner))
					$scope.bannerNames += ($scope.bannerNames.length > 0) ?  ", " + banner : banner; 
			});
		
		if($scope.search.emittingBankSelected.length > 0)
			$scope.search.emittingBankSelected.forEach( emittingBank => {
				if(!$scope.emittingBankNames.includes(emittingBank))
					$scope.emittingBankNames += ($scope.emittingBankNames.length > 0) ? ", " + emittingBank  : emittingBank; 
			});
	}
	
	$scope.buscar = () => {
		$scope.loadExtractCardPerPeriod();
	}

	function loadDatePicker(){
		$("#dtInicio").datepicker(datePickerOptions);
		$("#dtFinal").datepicker(datePickerOptions);
		$("#searchStartDate").datepicker(datePickerOptions);
		$("#searchEndDate").datepicker(datePickerOptions);
	}
	
	function calculeTotalExtract() {
		let total = 0;
		for (var extract of $scope.extractCardList) {	
			total += extract.amount;
		}
		return total;
	}


	$scope.applyMultipleSelectionFilter = _=> {
		let listExpensesFiltered = [];

		$scope.extractCardList.forEach(extract => {
			let removedElement = false;
			listExpensesFiltered.push(extract);

			if($scope.clientsSelected.length > 0) {
				let idx = 1;

				$scope.clientsSelected.forEach(client => {
					if (extract['settlement'] != null) {
						if (extract['settlement'].client['clientId'] == client['clientId']) return true;
						if (extract['settlement'].client['clientId'] != client['clientId'] && idx == $scope.clientsSelected.length) {
							let indexExtract = listExpensesFiltered.indexOf(extract);
							listExpensesFiltered.splice(indexExtract);
							removedElement = true;
						}
						idx++;
					}
				});
			}

			if($scope.activitysSelected.length > 0 && !removedElement) {
				let idx = 1;

				$scope.activitysSelected.forEach(activity => {
					if (extract['settlement'] != null) {
						if (extract['settlement']['event']['eventId'] == activity['eventId']) return true;
						if (extract['settlement']['event']['eventId']!= activity['eventId'] && idx == $scope.activitysSelected.length) {
							let indexExtract = listExpensesFiltered.indexOf(extract);
							listExpensesFiltered.splice(indexExtract);
							removedElement = true;
						}
						idx++;
					}
				});
			}
		});

		$scope.expenseVisible = [];
		listExpensesFiltered.forEach(expense => $scope.expenseVisible.push(expense));

		fillLabelsParameters();
	}

	function getClients() {
		let idsIncludedInClients = [];
		$scope.clients = [];

		for(let expense of $scope.extractCardList) {
			if (expense.settlement != null)
			if (idsIncludedInClients.indexOf(expense.settlement.client.clientId) < 0){
				$scope.clients.push(expense.settlement.client);
				idsIncludedInClients.push(expense.settlement.client.clientId);
			} 
		}
	}

	function getActivitys() {
		let idsIncludedInActivitys = [];
		$scope.activitys= [];

		for(let expense of $scope.extractCardList) {
			if (expense.settlement != null)
			if (idsIncludedInActivitys.indexOf(expense.settlement['event'].eventId) < 0){
				$scope.activitys.push(expense.settlement['event']);
				idsIncludedInActivitys.push(expense.settlement['event'].eventId);
			} 
		}
	}

	function fillLabelsParameters() {
		$scope.clientsNames 	= "";
		$scope.activitysNames 	= "";

		if ($scope.activitysSelected.length > 0) {
			for (let activity of $scope.activitysSelected) {
				if (!$scope.activitysNames.includes(activity.description)) {
					$scope.activitysNames += $scope.activitysNames.length == 0 ? activity.description: ", " + activity.description;
				}
			}
		}

		if ($scope.clientsSelected.length > 0) {
			for (let client of $scope.clientsSelected) {
				if (!$scope.clientsNames.includes(client.companyName)) {
					$scope.clientsNames += $scope.clientsNames.length == 0 ? client.companyName : ", " + client.companyName;
				}
			}
		}
	}

	$scope.showClientSelect = _=> {
		$scope.showClients = !$scope.showClients;

		if ($scope.showClients) {
			$("#clientTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up')
		}
		else {
			$("#clientTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down')
		}
	}

	$scope.showActivitysSelect = _=> {
		$scope.showActivitys = !$scope.showActivitys;

		if ($scope.showActivitys) {
			$("#acitivtyTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up')
		}
		else {
			$("#acitivtyTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down')
		}
	}
	
});
