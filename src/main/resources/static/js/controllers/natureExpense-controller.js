appGIMB.controller("natureExpenseController", function ($scope, $http, $routeParams, $location, config, juiceService, geralAPI) {
	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');
	if (!controllerHeadCheck($scope, $location, juiceService))
		return;

	$('#divMenu').show();

	$scope.natureExpense = { active: true };
	$scope.optActive = "active";
	$scope.natureExpense.colorId = getRandomColor();
	$scope.natureExpenses = [];
	$scope.allNatureExpenses = [];

	$scope.allEvents = [];

	loadallEvents();

	colorSelectorConf('colorSel', $scope.natureExpense.colorId);

	if ($routeParams.natureExpenseId)
		loadNatureExpense($routeParams.natureExpenseId);
	else
		loadNatureExpenses();

	function loadNatureExpense(id) {
		$('#modal_carregando').modal('show');

		$http.get(config.baseUrl + '/admin/natureExpense/' + id).then(
			function (response) {
				$scope.natureExpense = response.data;

				$('#modal_carregando').modal('hide');
			},
			function (response) {
				alert($scope.getMessage('ErrorLoadData'));
				$('#modal_carregando').modal('hide');
			}
		);
	}

	function loadNatureExpenses() {
		$('#modal_carregando').modal('show');

		$http.get(config.baseUrl + '/admin/allNatureExpenses').then(
			function (response) {
				$scope.allNatureExpenses = response.data;
				
				$scope.query = getTextNatureExpense();
				$scope.optActive = getOptionNatureExpense().length > 0 ? getOptionNatureExpense() : 'active'; 
				
				filterNatureExpenseByStatus();

				$('#modal_carregando').modal('hide');
			},
			function () {
				alert($scope.getMessage('ErrorLoadData'));
				$('#modal_carregando').modal('hide');
			}
		);
	}


	$scope.filterNatureExpenseByStatus = function () {
		filterNatureExpenseByStatus();
	}

	function filterNatureExpenseByStatus() {
		$scope.natureExpenses = $scope.allNatureExpenses.filter(function (natureExpense) {
			if ($scope.optActive == 'all')
				return true;

			else if ($scope.optActive == 'active' && natureExpense.active == true)
				return true;

			else if ($scope.optActive == 'inactive' && natureExpense.active == false)
				return true;

			else
				return false;
		});
		
		setOptionNatureExpense($scope.optActive);
		
		if($scope.optActive == 'active')
			document.getElementById('optActive').checked = true;
		else if($scope.optActive == 'inactive')
			document.getElementById('optInactive').checked = true;
		else
			document.getElementById('optAll').checked = true;
	}

	$scope.retornaClasse = function (objNatureExpense) {
		if (objNatureExpense.active == true)
			return 'btnativo';

		else
			return 'btninativo';
	}

	$scope.trataAtivoInativo = function (objNatureExpense) {
		if (objNatureExpense.active == true)
			return $scope.getLabel('Active');

		else
			return $scope.getLabel('Inactive');
	}

	function loadallEvents() {
		$http.get(config.baseUrl + "/admin/event").then(
			function (response) {
				$scope.allEvents = response.data;
				$('#modal_carregando').modal('hide');
			}, function (response) {
				alert($scope.getMessage('ErrorLoadData'));
				$('#modal_carregando').modal('hide');
			}
		);
	}

	$scope.mudaSituacao = function (objNatureExpense) {
		$('#modal_carregando').modal('show');

		let eventosVinculados = "";

		$scope.allEvents.forEach(event => {
			if (event.natureExpense !== null) {
				if (event.natureExpense.natureExpenseId === objNatureExpense.natureExpenseId) {
					eventosVinculados += `${event.eventId}, `;
					$('#modal_carregando').modal('hide');
				}
			}
		});

		if (eventosVinculados !== "") {
			alert(`${getLabel('natureBound')} ${eventosVinculados} ${getMessage('operationAborted')}`);
			// alert("Existem vinculos");
			return false;
		}

		objNatureExpense.active = objNatureExpense.active == true ? false : true;

		$http.put(config.baseUrl + '/admin/updateNatureExpense', objNatureExpense).then(
			function (response) {
				loadNatureExpenses();
				$('#modal_carregando').modal('hide');
			},
			function (response) {
				alert($scope.getMessage('ErrorSavingDataWithoutParam'));
				$('#modal_carregando').modal('hide');
			}
		);
	}

	$scope.saveNatureExpense = function (objNatureExpense) {
		if (!validateForm())
			return;
		$('#modal_carregando').modal('show');
		var url;

		if (objNatureExpense.natureExpenseId != null && objNatureExpense.natureExpenseId > 0)
			url = config.baseUrl + '/admin/updateNatureExpense';
		else
			url = config.baseUrl + '/admin/saveNatureExpense';

		$http.put(url, objNatureExpense).then(
			function (response) {
				$('#modal_carregando').modal('hide');

				setTimeout(function () {
					$('#backNatureExpenseList').trigger('click');
				}, 500);
			},
			function (response) {
				alert($scope.getMessage('ErrorSavingDataWithoutParam'));
			}
		);
	}
	
	$scope.setTextNatureExpense = () => {
		setTextNatureExpense($scope.query);
	}
});