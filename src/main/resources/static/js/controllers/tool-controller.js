appGIMB.controller("toolController",
function ($scope, $http, $routeParams, $location, config, juiceService, geralAPI) {
    $('form').attr('autocomplete', 'off');
    $('input[type=text]').attr('autocomplete', 'off');
    if (!controllerHeadCheck($scope, $location, juiceService))
        return;

    $('#divMenu').show();

    numCampos = 1;
    numDivCampos = 1;
    dictF = {};

    $scope.tool = { active: true };
    $scope.tools = [];
    $scope.allTools = [];
    $scope.optActive = '';
    
    $scope.optActive = "active";

    var error = $scope.getMessage('ErrorSavingData');

    if ($routeParams.toolId) {
        carregarFerramenta($routeParams.toolId);
    } else {
        loadTools();
    }

    function carregarFerramenta(toolId) {
        $http.get(config.baseUrl + "/admin/tool/" + toolId).then(
            function (response) {
                $scope.tool = response.data;
                // console.log();

                loadTools();
            },
            function (response) {
                alert($scope.getMessage('ErrorLoadData'));
            }
        );
    }

    function carregarFerramentas() {
        $http.get(config.baseUrl + "/admin/tool").then(
            function (response) {
                $scope.tools = response.data;
                loadTools();
            },
            function (response) {
                alert($scope.getMessage('ErrorLoadData'));
            }
        );
    }

    $scope.tools = carregarFerramentas();

    $scope.saveTool = function (obj) {
        if (!validateForm())
            return;

        var url = config.baseUrl + "/admin/toolCreate";

        if (obj.toolId != null || obj.toolId > 0)
            url = config.baseUrl + "/admin/updateTool/" + obj.toolId;

        $http.put(url, obj).then(
            function (response) {
                if (response.status >= 200 && response.status <= 299) {
                    alert($scope.getMessage('DataSavedSuccessfully'));
                    refreshPage();
                    $location.path(juiceService.appUrl + 'tool');
                } else {
                    refreshPage();
                    alert(error + response.status + "\n" + response.message);
                }
            },
            function (response) {
                refreshPage();
                alert(error + response.status + "\n" + response.message);
            }
        );
    };
    

    function refreshPage() {
        $scope.tool = {};
    }

    $scope.filterToolByStatus = function () {
        filterToolByStatus();
    };

    function filterToolByStatus() {
        $scope.tools = $scope.allTools.filter(
            function (tool) {
                console.log('filterToolByStatus: ', tool);
                if ($scope.optActive == "all")
                    return true;
                else if ($scope.optActive == "active" && tool.active == true)
                    return true;
                else if ($scope.optActive == "inactive" && tool.active == false)
                    return true;

                return false;
            }
        );
        
        setOptionAcitivy($scope.optActive);
        
        if($scope.optActive == 'all')
            document.getElementById('optAll').checked = true;
        else if($scope.optActive == 'active')
            document.getElementById('optActive').checked = true;
        else
            document.getElementById('optInactive').checked = true;
        
    
        $scope.tools = orderObject( $scope.tools, 'description' );
    }

    $scope.retornaClasse = function (objTool) {
        if (objTool.active == true) {
            return 'btnativo';
        } else {
            return 'btninativo';
        }
    };

    $scope.trataAtivoInativo = function (objTool) {
        if (objTool.active == true) {
            return $scope.getLabel('Active');
        } else {
            return $scope.getLabel('Inactive');
        }
    };

    $scope.mudaSituacao = function (objTool) {
        objTool.active = objTool.active == true ? false : true;

        $http.put(config.baseUrl + '/admin/updateTool/' + objTool.toolId, objTool).then(
            function (response) {
                alert($scope.getMessage('DataSavedSuccessfully'));
                loadTools();
            },
            function (response) {
                alert($scope.getMessage('ErrorSavingDataWithoutParam'));
            }
        );
    };

    function loadTools() {
        $http.get(config.baseUrl + "/admin/tool").then(
            function (response) {
                $scope.allTools = response.data;
                
                $scope.query = getTextActivity();
                $scope.optActive = getOptionAcitivy().length <= 0 ? 'active' : getOptionAcitivy();
                
                filterToolByStatus();
            },
            function (response) {
                alert($scope.getMessage('ErrorLoadData'));
            }
        );
    }
    
    $scope.setTextActivity = () => {
        setTextActivity(document.getElementById('query').value);
    }

});