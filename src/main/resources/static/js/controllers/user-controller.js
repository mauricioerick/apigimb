appGIMB.controller("userController", function ($scope, $http, $routeParams,
	$location, config, juiceService, geralAPI) {

	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');
	if (!controllerHeadCheck($scope, $location, juiceService))
		return;

	$('#divMenu').show();
	$("#aniversario").datepicker(datePickerOptions);

	changeScreenHeightSize('#divTableUser')

	$scope.user = {
		active: true
	};
	$scope.user.colorId = getRandomColor();
	colorSelectorConf('colorSel', $scope.user.colorId)
	$scope.allUsers = [];
	$scope.users = [];
	$scope.profiles
	$scope.userLog = '';
	$scope.userEdit = '';

	$scope.costCentersToSelect = [];
	$scope.costCenters = [];
	$scope.costCenterByUser = [];
	$scope.cards = [];
	$scope.cardsNotInclude = [];
	$scope.company = {};

	$scope.newCostCenter = "";
	$scope.newCard = null;
	$scope.newCardSelected = "";
	$scope.cardSelectToPrintTerm = {};
	
	$scope.companyLogo = '';
	
	$scope.optActive = "active";
	
	loadCostCenters();

	loadCostCenters();

	if ($routeParams.userId) {
		loadUser($routeParams.userId);
		loadProfiles();
	} else {
		loadUsers();
		loadProfiles();
	}

	$scope.costCenterIdSelected = "";
	$scope.cardIdMainSelected = "";

	$scope.onChangeCostCenter = () => {
		let costCenter = document.getElementById('mainCostCenter');
		let option = costCenter.options[costCenter.selectedIndex].value;

		$scope.user.costCenter = $scope.costCenters.find(cc => cc.costCenterId == option);
		$scope.save($scope.user, false);
	}

	function loadProfiles() {
		$http.get(config.baseUrl + "/admin/profile").then(function (response) {
			$scope.profiles = response.data;
		}, function (response) {
			alert("Erro ao carregar perfis!");
		});
	}

	$scope.addNewCostCenter = () => {
		if ($scope.newCostCenter === "") {
			return alert(getMessage('YouMustSelectCostCenter'));
		}

		$scope.user.costCenters.push(JSON.parse($scope.newCostCenter));
		$scope.save($scope.user, false);

		let costCenterToRemove = $scope.costCentersToSelect.find((val) => val.costCenterId == JSON.parse($scope.newCostCenter).costCenterId );

		$scope.costCentersToSelect.splice($scope.costCentersToSelect.indexOf(costCenterToRemove), 1);
		$scope.newCostCenter = "";

		$('#modalCostCenter').modal('hide');
	}

	function loadCostCenters() {
		$http.get(config.baseUrl + "/admin/costCenterActive/true").then(
			(response) => {
				$scope.costCenters = response.data;
			}, (responseError) => {
				console.error(`${getLabel('ErrorLoadingCostCenter')} ${responseError}`);
			}
		)
	}

	function loadCostsCentersNotSelected() {
		$scope.costCenters.forEach(costCenter => {
			let locatedCC = $scope.costCenterByUser.find(cc => {
				if (cc.costCenterId === costCenter.costCenterId) {
					return cc;
				}
			});

			if (!locatedCC) {
				$scope.costCentersToSelect.push(costCenter);
			}
		})
	}

	function loadUser(userId) {
		var user = JSON.parse(localStorage.getItem("bobby"));
		$scope.userLog = (user.user != null && user.user.length > 0) ? user.user : '';

		$http.get(config.baseUrl + "/admin/user/" + userId).then(
			function (response) {
				$scope.user = response.data;
				$scope.costCenterByUser = response.data.costCenters;
				loadCostsCentersNotSelected();
				$scope.user.userWeb = !response.data.userWeb ? "0" : "1";

				$scope.costCenterIdSelected = $scope.user.costCenter ? $scope.user.costCenter.costCenterId.toString() : '';
				$scope.cardIdMainSelected = $scope.user.cardMain ? $scope.user.cardMain.cardId.toString(): ''; 
				
				if ($scope.user.colorId == null)
					$scope.user.colorId = getRandomColor();

				$scope.userEdit = $scope.user.user;
				
				inflateCards();
				getCompany();

				colorSelectorConf('colorSel', $scope.user.colorId)
			}, function (response) {
				alert($scope.getMessage('ErrorLoadData'));
			});
	}

	$scope.removeCostCenter = costCenter => {
		const newCostCenterList = $scope.costCenterByUser.filter(costCenterLocated => costCenterLocated.costCenterId !== costCenter.costCenterId);
		$scope.costCenterByUser = newCostCenterList;

		$scope.user.costCenters = JSON.parse(angular.toJson(newCostCenterList));
		$scope.costCentersToSelect.push(costCenter);
		$scope.save($scope.user, false);
	}

	function loadUsers() {
		var user = JSON.parse(localStorage.getItem("bobby"));
		$scope.userLog = (user.user != null && user.user.length > 0) ? user.user : '';

		$http.get(config.baseUrl + "/admin/user").then(function (response) {
			$scope.allUsers = response.data;
			
			$scope.query = getTextUser();
			$scope.optActive = getOptionUser().length > 0 ? getOptionUser() : 'active'; 
			
			filterUsersByStatus();
		}, function (response) {
			alert($scope.getMessage('ErrorLoadData'));
		});
	}

	$scope.save = function (obj, showAlert) {
		if (!validateForm())
			return;

		if ((obj.costCenter) && (obj.costCenter.costCenterId === ""))
			obj.costCenter = null

		var url = config.baseUrl + "/admin/createUser";
		if (obj.userId != null && obj.userId > 0)
			url = config.baseUrl + "/admin/updateUser/" + obj.userId;

		obj.userWeb = obj.userWeb == "1" ? true : false;

		if (obj.birthDateString != null && obj.birthDateString.length > 0)
			obj.birthDate = convertDate(obj.birthDateString);

		$http.put(url, obj)
			.then(function (response) {
				if (response.status >= 200 && response.status <= 299) {
					$scope.costCenterByUser = response.data.costCenters;
					showAlert ? alert($scope.getMessage('DataSavedSuccessfully')) : false;

					const splitedUrl = url.split('/');
					(splitedUrl[splitedUrl.length - 1] === 'createUser') ? $location.path(`${juiceService.appUrl}userDetail/${response.data.userId}`) : false;
				} else if (response.status == 409) {
					// refreshPage();
					alert($scope.getMessage('ErrorWritingUser'));
				}
				else {
					alert($scope.getMessage('ErrorSavingData') + response.status + "\n" + response.message);
				}

			}, function (response) {
				alert($scope.getMessage('ErrorSavingData') + response.status + "\n" + response.message);
			});
	};

	$scope.updatePassword = function (obj) {
		var pass = document.getElementById("password").value;
		obj.pass = pass;

		obj.userWeb = obj.userWeb == "1" ? true : false;

		if (obj.birthDateString != null && obj.birthDateString.length > 0)
			obj.birthDate = convertDate(obj.birthDateString);

		$http.put(config.baseUrl + "/admin/updateUserPassword", obj).then(
			function (response) {
				var msg = "";

				if (response.status >= 200 && response.status <= 299)
					msg = $scope.getMessage('PasswordChangedSuccessfully');
				else
					msg = $scope.getMessage('ErrorChangingUserPassword')
						+ response.status + "\n" + response.message;

				alert(msg);
				$scope.clearPassFields();
				document.getElementById("btnCancelPassUpdate").click();

				loadUser(obj.userId)
			},
			function (response) {
				alert($scope.getMessage('ErrorChangingUserPassword')
					+ response.status + "\n" + response.message);
				refreshPage();

				$scope.clearPassFields();
				document.getElementById("btnCancelPassUpdate").click();
			});
	};

	$scope.mudaSituacao = function (objUser) {
		objUser.active = objUser.active == true ? false : true;

		$http.put(config.baseUrl + '/admin/updateUser/' + objUser.userId,
			objUser).then(function (response) {
				alert($scope.getMessage('DataSavedSuccessfully'));
				loadUsers()
			}, function (response) {
				alert($scope.getMessage('ErrorSavingDataWithoutParam'));
			});
	}

	$scope.filterUsersByStatus = function () {
		filterUsersByStatus();
	};

	function filterUsersByStatus() {
		$scope.users = $scope.allUsers.filter(function (user) {
			if ($scope.optActive == "all")
				return true;
			else if ($scope.optActive == "active" && user.active == true)
				return true;
			else if ($scope.optActive == "inactive" && user.active == false)
				return true;

			return false;
		});
		
		setOptionUser($scope.optActive);
		
		if($scope.optActive == 'active')
			document.getElementById('optActive').checked = true;
		else if($scope.optActive == 'inactive')
			document.getElementById('optInactive').checked = true;
		else
			document.getElementById('optAll').checked = true;
		
		
		$scope.users = orderObject( $scope.users, 'name' );
		
	}

	$scope.validatePassword = function () {
		var pass = document.getElementById("password").value;
		var passCheck = document.getElementById("passwordCheck").value;

		if (pass.length > 0 && pass == passCheck)
			return true;

		return false;
	};

	$scope.clearPassFields = function () {
		document.getElementById("password").value = "";
		document.getElementById("passwordCheck").value = "";

		$scope.validatePassword();
	};

	function refreshPage() {
		$scope.user = {};
	}

	$scope.retornaClasse = function (objUser) {
		if (objUser.active == true) {
			return 'btnativo'
		} else {
			return 'btninativo'
		}
	}

	$scope.trataAtivoInativo = function (objUser) {
		if (objUser.active == true) {
			return $scope.getLabel('Active');
		} else {
			return $scope.getLabel('Inactive');
		}
	}

	function convertDate(data) {
		var date = [];
		date = data.split("/");

		return new Date(date[2] + "-" + date[1] + "-" + date[0] + " 12:00:00")
			.getTime();
	}

	$scope.verificaAlteracao = function (userLog, userEdit) {
		if ($scope.user.user == 'GIMB.ADMIN' || $scope.user.user == 'gimb.admin') {
			if (userLog == 'GIMB.ADMIN' || userLog == 'gimb.admin')
				return true;
			else
				return false;
		}
		else
			return true
	}

	$scope.verificaAtivarDesativar = function (userLog, userEdit) {
		if (userEdit == "GIMB.ADMIN" || userEdit == 'gimb.admin') {
			if (userLog == 'GIMB.ADMIN' || userLog == 'gimb.admin')
				return true;

			else
				return false;
		}
		else
			return true;

	}
	
	function inflateCards(){
		var url = config.baseUrl + '/admin/avaibleCards';
		
		$http.get(url).then(
			function(response){
				$scope.cards = response.data;				
				loadCardsNotInclude();
	
			}, function(response){
				alert($scope.getMessage('ErrorLoadData'));
			}
		);
	}
	
	$scope.onChangeCard = function(){
		let card = document.getElementById('mainCard');
		let option = card.options[card.selectedIndex].value;

		$scope.user.cardMain = $scope.cards.find(cc => cc.cardId == option);
		$scope.save($scope.user, false);
	}
	
	
	function loadCardsNotInclude(){
		var contains = false;
		for(var c of $scope.cards){
			for(var u of $scope.user.cards)
				if(u.cardId == c.cardId)
					contains = true;
			
			if(!contains)
				$scope.cardsNotInclude.push(c);
			else
				contains = false;
		}
	}
	
	$scope.removeCard = function(card){
		$scope.user.cards.splice($scope.user.cards.indexOf(card), 1);
		$scope.cardsNotInclude.push(card);
		linkCard(card.cardId, 0);
	}
	
	$scope.addNewCard = () => {
		if ($scope.newCard == null)
			return alert(getMessage('youMustSelectNewCard'));

		$scope.user.cards.push($scope.newCard);
		linkCard($scope.newCard.cardId, $scope.user.userId);

		$scope.cardsNotInclude.splice($scope.cardsNotInclude.indexOf($scope.newCard), 1);
		$scope.newCard = null;
		$scope.newCardSelected = "";

		$('#modalNewCard').modal('hide');
	}
	
	$scope.changeNewCard = function(){
		let card = document.getElementById('addCard');
		let option = card.options[card.selectedIndex].value;

		$scope.newCard = $scope.cardsNotInclude.find(cc => cc.cardId == option);
		$scope.newCardSelected = $scope.newCard.cardId.toString();
	}
	
	function getCompany(){
		let url = config.baseUrl + '/company/1';
		$http.get(url).then(
			function(response){
				$scope.company = response.data;
				getLogoCompany();
			},
			function(response){
				alert($scope.getMessage('ErrorLoadData'));
			}
		);
	}
	
	function getLogoCompany(){
		let url = config.baseUrl + '/companyImageLogo';
		$http.get(url).then(
			function(response){
				$scope.companyLogo = response.data;
			},
			function(response){
				alert($scope.getMessage('ErrorLoadData'));
				return '';
			}
		);
	}
	
	function loadingMessagesToPrint(){
		let html = '';
		
		$scope.deliveryTermTitle = $scope.getMessage('deliveryTermTitle');
		$scope.deliveryTermPrimaryMessage = $scope.getMessage('deliveryTermPrimaryMessage');
		$scope.deliveryTermCardDetails = $scope.getMessage('deliveryTermCardDetails');
		$scope.deliveryTermSecondMessage = $scope.getMessage('deliveryTermSecondMessage');
		$scope.deliveryTermAccordingEnclosure = $scope.getMessage('deliveryTermAccordingEnclosure');
		$scope.deliveryTermCondition = $scope.getMessage('deliveryTermCondition');
		$scope.nameCompany = $scope.company.companyName;
		$scope.getDateFormatte = $scope.getMessage('formattedDate');
		
		$scope.deliveryTermPrimaryMessage = $scope.deliveryTermPrimaryMessage.replace("(NOME)", $scope.user.name);
		$scope.deliveryTermPrimaryMessage = $scope.deliveryTermPrimaryMessage.replace("(CPF)", $scope.user.cpf);
		$scope.deliveryTermPrimaryMessage = $scope.deliveryTermPrimaryMessage.replace("(NOME EMPRESA)", $scope.company.companyName);
		$scope.deliveryTermPrimaryMessage = $scope.deliveryTermPrimaryMessage.replace("(ENDERECO)", $scope.company.address);
		$scope.deliveryTermPrimaryMessage = $scope.deliveryTermPrimaryMessage.replace("(CNPJ)", $scope.company.registration);
		
		$scope.deliveryTermCardDetails = $scope.deliveryTermCardDetails.replace("(NUMERO CARTAO)", $scope.cardSelectToPrintTerm.cardNumber);
		$scope.deliveryTermCardDetails = $scope.deliveryTermCardDetails.replace("(BANDEIRA)", $scope.cardSelectToPrintTerm.cardBanner);
		$scope.deliveryTermCardDetails = $scope.deliveryTermCardDetails.replace("(BANCO EMISSOR)", $scope.cardSelectToPrintTerm.emittingBank);
		
		$scope.deliveryTermSecondMessage = $scope.deliveryTermSecondMessage.replace("(USUARIO)", $scope.user.name);
		
		$scope.deliveryTermCondition = $scope.deliveryTermCondition.replace("(USUARIO)", $scope.user.name);
		$scope.deliveryTermCondition = $scope.deliveryTermCondition.replace("(USUARIO)", $scope.user.name);
		$scope.deliveryTermCondition = $scope.deliveryTermCondition.replace("(USUARIO)", $scope.user.name);
		$scope.deliveryTermCondition = $scope.deliveryTermCondition.replace("(USUARIO)", $scope.user.name);
		$scope.deliveryTermCondition = $scope.deliveryTermCondition.replace("(USUARIO)", $scope.user.name);
		 
		let date = new Date();
		
		let day = date.getDate();
		let month = date.getMonth();
		let year = date.getFullYear();
		
		$scope.getDateFormatte = $scope.getDateFormatte.replace("(DIA)", day);
		$scope.getDateFormatte = $scope.getDateFormatte.replace("(MES)", $scope.getMessage('Months.' + month));
		$scope.getDateFormatte = $scope.getDateFormatte.replace("(ANO)", year);
		
		html = `<div class="divPrincipal" id = 'printDiv'> 
					<div style="border-bottom:2px solid #FFF; margin-bottom:5px !important; text-align: center !important; font-size: 16px !important; display: inline-block !important; width:100% !important;  margin-top:0 !important;">
						<img src="${$scope.companyLogo}" style="max-height: 150px; max-width: 150px"/>
					</div>
		
					<div style="font-weight: bolder;">${$scope.deliveryTermTitle}</div>
					<br/>
					<span>${$scope.deliveryTermPrimaryMessage}</span>
					<br/>
					<br/>
					<span>${$scope.deliveryTermCardDetails}</span>
					<br/>
					<br/>
					<span>${$scope.deliveryTermSecondMessage}</span>
					<br/>
					<br/>
					<span>${$scope.deliveryTermCondition}</span>
					<br/>
					<br/>
					<span>${$scope.deliveryTermAccordingEnclosure}</span>
					<br/>
					<span>${$scope.getDateFormatte}</span>
					<br/>
					<br/>
					<br/>
					<br/>
					<span>__________________________________</span>                                  
					<br/>
					<span>${$scope.company.companyName}</span>
					<br/>
					<br/>
					<br/>												
					 <span>_________________________________</span>
					<br/><span>${$scope.user.name}</span>
			</div>'`
			
		var divForPrint = document.getElementById('divForPrint');
		divForPrint.innerHTML = html;
		
		document.getElementById('btnPrintTerm').click(); 
		
		divForPrint.removeChild(document.getElementById('printDiv'));
	}
	
	$scope.setSelectCardToPrint = function(card) {
		$scope.cardSelectToPrintTerm = card;
		loadingMessagesToPrint();
	}

	 async function linkCard(card, user) {
		 const response = await $http({ method: 'POST', url: `${config.baseUrl}/admin/card/linkUser/${card}&${user}` });
		 if (response.status < 200 || response.status > 299) {
		 	console.log(response);
		 }
	}

	 
	 $scope.setTextUser = () => {
		 setTextUser($scope.query);
	 }

});
