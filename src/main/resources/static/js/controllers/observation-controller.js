appGIMB.controller("observationController",
	function($scope, $http, $routeParams, $location, config, juiceService) {

		$('form').attr('autocomplete', 'off');
		$('input[type=text]').attr('autocomplete', 'off');
		if (!controllerHeadCheck($scope, $location, juiceService))
			return;

		var error = $scope.getMessage('ErrorSavingData');

		$('#divMenu').show();

		changeScreenHeightSize('#divTableObservation');

		$scope.observation = { active : true };
		$scope.observations = [];
		$scope.allObservations = [];
		
		$scope.optActive = "active";

		if ($routeParams.observationId) {
			loadObservation($routeParams.observationId);
		} else {
			loadObservations();
		}

		function loadObservation(observationId) {

			$http.get(config.baseUrl + "/admin/observation/" + observationId).then(
				function(response) {
					$scope.observation = response.data;
											
					appearOnList($scope.observation);
					$('.selectpicker').selectpicker('val', $scope.observation.appearsOnList);
				},
				function(response) {
					alert($scope.getMessage('ErrorLoadData'));
				}
			);
		}

		function loadObservations() {
			$http.get(config.baseUrl + "/admin/observation").then(
				function(response) {
					$scope.allObservations = response.data;
					$scope.query = getTextObservation();
					$scope.optActive = getOptionObservation().length > 0 ? getOptionObservation() : 'active';
					
					filterObservationByStatus();
				}, function(response) {
					alert($scope.getMessage('ErrorLoadData'));
				}
			);
		}

		$scope.saveObservation = function(obj) {
			if (!validateForm())
				return;

			var url = config.baseUrl + "/admin/createObservation";
			if (obj.observationId > 0)
				url = config.baseUrl + "/admin/updateObservation/" + obj.observationId;
				
			sendObj = {"language": localStorage.language, "observation": obj};

			$http.put(url, sendObj).then(
				function(response) {
					if (response.status >= 200 && response.status <= 299) {
						alert($scope.getMessage('DataSavedSuccessfully'));
						refreshPage();
						$location.path(juiceService.appUrl + 'observation');
					} else {
						refreshPage();
						alert(error + response.status + "\n" + response.message);
					}
				},
				function(response) {
					refreshPage();
					alert(error + response.status + "\n" + response.message);
				}
			);
		};

		function refreshPage() {
			$scope.observation = {};
		}

		$scope.filterObservationByStatus = function() {
			filterObservationByStatus();
		};

		function filterObservationByStatus() {
			$scope.observations = $scope.allObservations.filter(isTrue);
			
			if($scope.optActive == 'active')
				document.getElementById('optActive').checked = true;
			else if ($scope.optActive == 'inactive')
				document.getElementById('optInactive').checked = true;
			else
				document.getElementById('optAll').checked = true;
			
			setOptionObservation($scope.optActive);
			
			
			$scope.observations = orderObject( $scope.observations, 'description' );
			
		}
		
		function isTrue(event){
			if ($scope.optActive == "all")
				return true;
			else if ($scope.optActive == "active" && event.active == true)
				return true;
			else if ($scope.optActive == "inactive" && event.active == false)
				return true;

			return false;
		}

		$scope.trataAtivoInativo = function(objObservation) {
			if (objObservation.active == true)
				return $scope.getLabel('Active');
			else
				return $scope.getLabel('Inactive');
		};

		$scope.retornaClasse = function(objObservation) {
			if (objObservation.active == true)
				return 'btnativo';
			else
				return 'btninativo';
		};

		$scope.mudaSituacao = function(obj) {

			obj.active = obj.active == true ? false : true;
			
			appearOnList(obj);
			
			sendObj = {"language": localStorage.language, "observation": obj};
			
			$http.put( config.baseUrl + "/admin/updateObservation/" + obj.observationId, sendObj ).then(
				function(response) {
					alert($scope.getMessage('DataSavedSuccessfully'));
					loadObservations();
				},
				function(response) {
					alert($scope.getMessage('ErrorSavingDataWithoutParam'));
				}
			);
		};
					
		function appearOnList(obj){
			for (var idx in obj.appearsOnList) {
				obj.appearsOnList[idx] = getLabel(obj.appearsOnList[idx]); 
			}
		}
		
		$scope.setTextObservation = () => {
			setTextObservation($scope.query);
		}
	}
);