appGIMB.controller("areaController", function ($scope, $http, $routeParams, $location, config, juiceService, geralAPI) {
	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');
	if (!controllerHeadCheck($scope, $location, juiceService))
		return;

	$('#divMenu').show();
	
	$scope.area = {active: true};
	$scope.optActive = "active";
	$scope.area.colorId = getRandomColor();
	$scope.allArea = [];
	$scope.areas = [];
	$scope.allUsers = [];
	var indexList = null;
	
	colorSelectorConf('colorSel', $scope.area.colorId);
	
	if($routeParams.areaId)
		loadArea($routeParams.areaId);
	else if($location.path().indexOf("areaCreate") <= -1)
		loadAreas();
	else
		inflateAllUsers();
	
	
	function loadArea(areaId){
		$('#modal_carregando').modal('show');
		
		$http.get(config.baseUrl + '/admin/area/' + areaId).then(
			function(response){
				$scope.area = response.data;
				inflateAreaUsers();
				inflateAllUsers();
				
				$('#modal_carregando').modal('hide');
			},
			function(response){
				$('#modal_carregando').modal('hide');
				setTimeout(function () {
					alert($scope.getMessage('ErrorLoadData'));
				}, 500);
			}
		);
		
	}
	
	function loadAreas(){
		$('#modal_carregando').modal('show');
		
		$http.get(config.baseUrl + '/admin/allArea').then(
			function(response){
				$scope.allArea = response.data;
				
				$scope.query = getTextArea();
				$scope.optActive = getOptionArea().length > 0 ? getOptionArea() : 'active'; 
				
				filterAreaByStatus();
				inflateAllUsers();
				
				$('#modal_carregando').modal('hide');
			},
			function(response){
				$('#modal_carregando').modal('hide');
				setTimeout(function () {
					alert($scope.getMessage('ErrorLoadData'));
				}, 500);
			}
		);
	}
	
	$scope.filterAreaByStatus = function(){
		filterAreaByStatus();
	}
	
	function filterAreaByStatus(){
		$scope.areas = $scope.allArea.filter(function(area) {
			if($scope.optActive == 'all')
				return true;
			
			else if($scope.optActive == 'active' && area.active == true)
				return true;
			
			else if($scope.optActive == 'inactive' && area.active == false)
				return true;
			
			else 
				return false;
		});
		
		setOptionArea($scope.optActive);
		
		if($scope.optActive == 'active')
			document.getElementById('optActive').checked = true;
		else if($scope.optActive == 'inactive')
			document.getElementById('optInactive').checked = true;
		else
			document.getElementById('optAll').checked = true;
		
		$scope.areas = orderObject( $scope.areas, 'description' );
	}
	
	$scope.retornaClasse = function(objArea){
		if(objArea.active == true)
			return 'btnativo';
		
		else 
			return 'btninativo';
	}
	
	$scope.trataAtivoInativo = function(objArea){
		if(objArea.active == true)
			return $scope.getLabel('Active');
		
		else
			return $scope.getLabel('Inactive');
	}
	
	$scope.mudaSituacao = function(objArea){
		$('#modal_carregando').modal('show');
		
		objArea.active = objArea.active == true ? false : true;
		
		$http.put(config.baseUrl + '/admin/saveArea', objArea).then(
				function(response){
					loadAreas();
					$('#modal_carregando').modal('hide');
				},
				function(response){
					$('#modal_carregando').modal('hide');
					setTimeout(function () {
						alert($scope.getMessage('ErrorSavingDataWithoutParam'));
					}, 500);
				}
		);
	}
	
	$scope.saveArea = function(objArea){
		$('#modal_carregando').modal('show');
		
		$http.put(config.baseUrl + '/admin/saveArea', objArea).then(
				function(response){
					$('#modal_carregando').modal('hide');
					
					setTimeout(function () {
						$('#backAreaList').trigger('click');
					}, 500);
				},
				function(response){
					$('#modal_carregando').modal('hide');
					setTimeout(function () {
						alert($scope.getMessage('ErrorSavingDataWithoutParam'));
					}, 500);
				}
		);
	}
	
	$scope.addUsers = function(){
		var id = document.getElementById('user').value;
		var newUser = {};
		
		if($scope.area.users == null || $scope.area.users == undefined)
			$scope.area.users = [];
		
		for(user of $scope.allUsers){
			if(user.userId == id){
				newUser.userId = user.userId;
				newUser.areaId = $scope.area.areaId;
				
				if(document.getElementById('responsible').value == "true")
					newUser.responsible = true;
				else
					newUser.responsible = false;
				
				newUser.name = user.name;
				newUser.user = user.user;
				
				break;
			}
		}
		
		if(indexList != null)
			$scope.area.users[indexList] = newUser;
		else
			$scope.area.users.push(newUser);
		
		clearFields();
	}
	
	function inflateAllUsers(){
		$('#modal_carregando').modal('show');
		
		$http.get(config.baseUrl + '/admin/user').then(
			function(response){
				$scope.allUsers = response.data;
				setTimeout(function () {
					$('#modal_carregando').modal('hide');
				}, 500);
			},
			function(response){
				$('#modal_carregando').modal('hide');
				setTimeout(function () {
					alert($scope.getMessage('ErrorLoadData'));
				}, 500);
			}
		);
	}
	
	$scope.removeUser = function(id){
		$('#modal_carregando').modal('show');
		
		var userRemove;
		for(var i = 0; i < $scope.area.users.length; i++){
			if($scope.area.users[i].userId == id){
				userRemove = $scope.area.users[i];
				$scope.area.users.splice(i, 1);
				break;
			}
		}
		
		if(userRemove != null && userRemove != undefined){
			$http.put(config.baseUrl + '/admin/removeAreaUser', userRemove).then(
					function(response){
						$('#modal_carregando').modal('hide');
					},
					function(response){
						$('#modal_carregando').modal('hide');
						alert($scope.getMessage('ErrorLoadData'));
					}
			)
		}
	}
	
	function inflateAreaUsers(){
		$http.get(config.baseUrl + '/admin/areaUserByArea/' + $scope.area.areaId).then(
				function(response){
					if(response.data.length > 0){
						$scope.area.users = response.data;
					}
				}, function(response){
					$('#modal_carregando').modal('hide');
					setTimeout(function () {
						alert($scope.getMessage('ErrorLoadData'));
					}, 500);
				}
		); 
	}
	
	$scope.editUser = function(user, index){
		document.getElementById('user').value = user.userId;
		document.getElementById('responsible').value = user.responsible;
		indexList = index;
	}
	
	$('#modalUser').on('hide.bs.modal', function(event) {
		clearFields();
	})
	
	function clearFields(){
		document.getElementById('user').value = 1;
		document.getElementById('responsible').value = false;
		document.getElementById('user').focus();
		indexList = null;
	}
	
	$scope.setTextArea = () => {
		setTextArea($scope.query);
	}
});