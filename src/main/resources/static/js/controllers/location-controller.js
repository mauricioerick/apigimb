appGIMB.controller("locationController", function($scope, $http, geralAPI, $location, juiceService,  $routeParams, config){

	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');
	if (!controllerHeadCheck($scope, $location, juiceService)) return;

	var marker, i, location, mapLoc, myTimer, lastOpenInfo
	var init = true
	var infobox = []
	var markers = []
	var bounds = new google.maps.LatLngBounds()

	var markerIcon = {
		url: 'http://image.flaticon.com/icons/svg/252/252025.svg',
		scaledSize: new google.maps.Size(30, 30)
	};

	$('.ipt').attr('disabled', 'disabled');

	$("#interval").val($("#interval option:first").val())
	$scope.intervalRefresh = $("#interval option:first").val()
	$scope.checkboxShowLabel = true

	$scope.appUrl = "/";
	$scope.pathImg = config.pathImg;

	if ($location.path().indexOf("apiGIMB") > -1)
	$scope.appUrl += "apiGIMB/";

	if (!localStorage.getItem("token"))
	$location.path(juiceService + 'login');

	$('#divMenu').hide();
	$("body").css("overflowY", "auto");
	$("#startDate").datepicker(datePickerOptions);

	load();
	timeOutSet()

	function load() {
		$http({
			method: 'GET',
			url: config.baseUrl + "/admin/op/findAllByGroupUser"
		}).then(function(response) {
			$scope.locations = response.data;
			listLocations = response.data
			if (init)
				initializeMap();
			else
				reloadMarkers()
		}, function(response) {
			alert($scope.getMessage('ErrorLoadData'));
		});
	}

	function initializeMap() {
		var center = {
			lat : -15.77972,
			lng : -47.92972
		};

		var mapOptions = {
			center : center,
			zoom : 5,
			mapTypeId : 'roadmap',
		};

		mapLoc = new google.maps.Map(document.getElementById('location'), mapOptions)

        var centerControlDiv = document.getElementById('subjectCase1');

        centerControlDiv.index = 1;
		mapLoc.controls[google.maps.ControlPosition.RIGHT_TOP].push(centerControlDiv);

		setMarkers()

		mapLoc.fitBounds(bounds)
	}

	function setMarkers() {
		for (i = 0; i < $scope.locations.length; i++) {
			location = $scope.locations[i]
			var loc = {lat : location.latitude, lng : location.longitude}
			marker = new MarkerWithLabel({
											map: mapLoc,
											animation: google.maps.Animation.DROP,
											position: loc,
											icon: markerIcon,
											labelContent:$scope.checkboxShowLabel ? capitalize(location.name) : "",
											labelClass:$scope.checkboxShowLabel ? "my-custom-class-for-label" : "",
											labelInBackground: false
										  });
		  markers.push(marker);

		  bounds.extend(new google.maps.LatLng(location.latitude, location.longitude));

		  infobox[i] = new InfoBox({
				content: createDiv($scope.locations[i], i),
				disableAutoPan: false,
				maxWidth: 150,
				pixelOffset: new google.maps.Size(-140, 0),
				zIndex: null,
				boxStyle: {
					background: "url('//rawgit.com/googlemaps/v3-utility-library/master/infobox/examples/tipbox.gif') no-repeat",
					opacity: 1,
					width: "280px"
				},
				closeBoxMargin: "12px 4px 2px 2px",
				closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
				infoBoxClearance: new google.maps.Size(1, 1)
			});

		    google.maps.event.addListener(marker, 'click', (function(marker, i) {
	        return function() {
				if (lastOpenInfo != undefined)
					infobox[lastOpenInfo].close()

				lastOpenInfo = i
			  	infobox[i].open(mapLoc, marker)
        	  	mapLoc.panTo(infobox[i].getPosition())
	        }
	      })(marker, i));
		}
	}

	function reloadMarkers() {
		for (var i=0; i<markers.length; i++) {

			markers[i].setMap(null);
			infobox[i].close()
		}

		markers = [];

		setMarkers();

		timeOutSet()
	}

	function createDiv(objLocation, i) {
		var contentString = '<div id="infobox">'+
								'<div style="margin-top: 10px;"><h4>' + objLocation.name +  '</h4></div>';

		if (objLocation.client != null) {
			contentString += '  '+
								'<div>' + $scope.getLabel('LastAction')+': <br>' + objLocation.client + '</div>' +
								'<div>' + objLocation.actionDescription + '</div>' +
								'<div>' + objLocation.actionStartTime + '</div><br>';
		}

		contentString += '  ' +
								'<div> ' +
								'   <label style="font-size: 10px; font-weight: bold">'+ $scope.getLabel('Updated') +': ' + objLocation.traceStartTime + '</label>' +
								'	<button style="float: right" class="btn btn-secondary btn-sm" onclick="showModalWindow(' + i + ')">'+$scope.getLabel('ShowRoute')+'</button>'+
								'</div>'+
		  					'</div>'

		return contentString
	}

	$scope.reloadWithMarkerLabels = function() {
		reloadMarkers()
	}

	$scope.reloadTimeOut = function() {
		timeOutSet()
	}

	$scope.reloadTracesByUserAndDate = function() {
		loadTracesByUserAndDate()
	}

	$scope.reload = function() {
		load()
	}

	function timeOutSet() {
		clearTimeout(myTimer)

		myTimer = setTimeout(function(){
			init = false
			load()
		}, $scope.intervalRefresh);
	}
});
