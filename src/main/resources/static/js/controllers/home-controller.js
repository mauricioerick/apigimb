appGIMB.controller("homeController", function ($scope, $http, $location, config, $route, juiceService) {

	$(document).ready(function () {
		const localStorageUser = JSON.parse(localStorage.getItem('bobby'));
		const localStorageCompany = JSON.parse(localStorage.getItem('company'));

		const loggedUser = localStorageUser.user;
		const userId = localStorageUser.userId;
		const companyName = localStorageCompany.companyName;
		const TradingName = localStorageCompany.tradingName;

		Raven.setTagsContext({
			loggedUSer: loggedUser,
			userId: userId,
			companyName: companyName,
			tradingName: TradingName
		});
	});

	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');

	$scope.access = config.permissionAccess;
	$('#divMenu').show();
	$scope.title = "GIMB";

	window.addEventListener("beforeunload", function (e) {
		localStorage.closeTime = Date.now();
	});

	checkCanLoad();

	$scope.sair = function () {
		// reset global variables
		resetGlobalVariables();

		localStorage.removeItem('bobby');
		localStorage.removeItem('company');
		localStorage.removeItem('permissionAccess');
		localStorage.removeItem('token');

		$('#listMenu').empty();
		$route.reload();
		$location.path(juiceService.appUrl + 'login');
	};
});
