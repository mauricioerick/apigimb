appGIMB.controller("calendarController",
	function ($scope, $http, $routeParams, $location, config, juiceService, geralAPI) {
		$('form').attr('autocomplete', 'off');
		$('input[type=text]').attr('autocomplete', 'off');
		if (!controllerHeadCheck($scope, $location, juiceService))
			return;

		$('#divMenu').show();

		$("#calendar").attr('src',window.location.origin.replace(':8100', '') +":4200");

		$(window).resize(()=>{
			const menuWidth = $("#divMenu").width();
			const windowWidth = $(window).width();
			const windowHeight = $(window).height();
			const calElement = $("#calendar");
			calElement.css('margin-left',menuWidth+'px');
			calElement.css('width',(windowWidth-menuWidth)+'px');
			calElement.css('height',windowHeight);
		});

		$(window).resize();


});
