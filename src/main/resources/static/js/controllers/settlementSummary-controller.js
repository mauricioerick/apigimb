appGIMB.controller("settlementSummary", function($scope, $http, geralAPI, $location, juiceService,  $routeParams, config){

	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');
	if (!controllerHeadCheck($scope, $location, juiceService)) return;
	
	$scope.pathImg = config.pathImg;
	$('.ipt').attr('disabled', 'disabled');
	$('#divMenu').show();
	$("body").css("overflowY", "auto");
	$("#startDate").datepicker(datePickerOptions);
  	$("#endDate").datepicker(datePickerOptions);

  	$scope.settlements = [];
  	
	$scope.users = [];
	$scope.userSelected = [];

	$scope.clients = [];
	$scope.clientSelected = [];
	
	$scope.identifiers = [];
	$scope.identifierSelected = [];

	$scope.events = [];
	
	$scope.data = {};

	$scope.title = $scope.getLabel('SettlementsSummary');

	var canRunReload = false;

	$scope.dtInicio = dateToStringPeriodo(new Date(), "i");
	$scope.dtFinal =  dateToStringPeriodo(new Date(), "f");
	
	document.getElementById('dtInicio').readOnly = true;
	document.getElementById('dtFinal').readOnly = true;
	
	$scope.showUsers = false;
	$scope.showClients = false;
	$scope.showIdentifiers = false;
	
	$scope.clientsNames = "";
	$scope.usersNames = "";
	$scope.identifiersNames = "";
	
	{ // Config data to print
		$scope.company = JSON.parse(localStorage.getItem("company"));
	}

	load();

	function load() {
		$('#modal_carregando').modal('show');
		
		var params = {};
		params["startDate"] = $scope.startDate;
		params["endDate"] = $scope.endDate;
		params["user"] = null;
		params["client"] = 0;
		params["isResumo"] = true;

		$http({
			method: 'PUT',
			url: config.baseUrl + "/settlementWeb",
			data: params
		}).then(function(response) {
			$scope.settlements = response.data;
			if($scope.settlements.length > 0){
				filterLists(params);
				fillLists();
			}				
			
			loadDatePicker();
			canRunReload = true;
			
			setTimeout(function() {$('#modal_carregando').modal('hide');}, 1000);
			
		}, function(response) {
			alert($scope.getMessage('ErrorLoadData'));
		});
	}

	function consolidateData(settlements, params) {
		$scope.data = {};
		var data = {};
		data['users'] = {};
		data['clients'] = {};
		data['events'] = {};
		data['paymentIds'] = {};
		data['total'] = 0;

		var client = params['client'];
		var user = params['user'];

		$(settlements).each(
				function(idx, settlement) {
					if (settlement.extract != null && settlement.extract.status == 'R') return;
					
					if ((client == 0 || client == settlement.client.clientId) && (user == null || user.user == settlement.user.user)) {
						var users = data['users'];
						if (users == null) users = {};
						var totUse = users[settlement.user.user];
						if (totUse == null) totUse = 0;
						totUse += settlement.amount;
						users[settlement.user.user] = Math.round(totUse * 100) / 100;
						data['users'] = users;

						var clients = data['clients'];
						if (clients == null) clients = {};
						var totCli = clients[settlement.client.tradingName];
						if (totCli == null) totCli = 0;
						totCli += settlement.amount;
						clients[settlement.client.tradingName] = Math.round(totCli * 100) / 100;
						data['clients'] = clients;

						var events = data['events'];
						if (events == null) events = {};
						var totEve = events[settlement.event.description];
						if (totEve == null) totEve = 0;
						totEve += settlement.amount;
						events[settlement.event.description] = Math.round(totEve * 100) / 100;
						data['events'] = events;

						var str = '$';
						if (settlement.paymentId != null && settlement.paymentId.length > 0)
							str = settlement.paymentId;

						var paymentIds = data['paymentIds'];
						if (paymentIds == null) paymentIds = {};
						var totPay = paymentIds[str];
						if (totPay == null) totPay = 0;
						totPay += settlement.amount;
						paymentIds[str] = Math.round(totPay * 100) / 100;
						data['paymentIds'] = paymentIds;

						var total = data['total'];
						if (total == null) total = 0;
						total += settlement.amount;
						data['total'] = Math.round(total * 100) / 100;
					}
				}
		);

		var users = [];
		var clients = [];
		var events = [];
		var paymentIds = [];

		var keys = Object.keys(data['users']);
		var values = Object.values(data['users']);

		for (var i = 0; i < keys.length; i++)
			users.push({key: keys[i], value: parseFloat(values[i]).toFixed(2)});

		keys = Object.keys(data['clients']);
		values = Object.values(data['clients']);

		for (var i = 0; i < keys.length; i++)
			clients.push({key: keys[i], value: parseFloat(values[i]).toFixed(2)});

		keys = Object.keys(data['events']);
		values = Object.values(data['events']);

		for (var i = 0; i < keys.length; i++)
			events.push({key: keys[i], value: parseFloat(values[i]).toFixed(2)});

		keys = Object.keys(data['paymentIds']);
		values = Object.values(data['paymentIds']);

		for (var i = 0; i < keys.length; i++)
			paymentIds.push({key: keys[i], value: parseFloat(values[i]).toFixed(2)});

		sortArray = function(a, b) {
			if (parseFloat(a.value) > parseFloat(b.value))
				return -1;

			if (parseFloat(a.value) < parseFloat(b.value))
				return 1;

			return 0;
		}

		$scope.data['users'] = users.sort(sortArray);
		$scope.data['clients'] = clients.sort(sortArray);
		$scope.data['events'] = events.sort(sortArray);
		$scope.data['paymentIds'] = paymentIds.sort(sortArray);

		$scope.data['total'] = data['total'];
		
		fillLablesParameters();
	}

	$scope.reload = function() {
		if (canRunReload) {
			const initDate = document.getElementById("dtInicio").value;
			const finalDate = document.getElementById("dtFinal").value; 
			
			$scope.startDate = initDate; 
			$scope.endDate = finalDate; 

			load();
		}
	}

	$scope.timeIntervalText = function() {
		return document.getElementById("dtInicio").value + ' - ' + document.getElementById("dtFinal").value;
	}

	$scope.identifierText = function() {
		return document.getElementById("identifier").value;
	}
	
	$scope.showModalWindow = () => {
		$("#parametersModal").modal({ backdrop: 'static', keyboard: false });
	}
	
	$scope.showClientsSelect = () => {
		$scope.showClient = !$scope.showClient;
		
		if($scope.showClient)
			$("#clientTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
		else
			$("#clientTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
	}
	
	$scope.showUserSelect = () => {
		$scope.showUsers = !$scope.showUsers;
		
		if($scope.showUsers)
			$("#userTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
		else
			$("#userTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
	}
	
	$scope.showIdentifierSelect = () => {
		$scope.showIdentifiers = !$scope.showIdentifiers;
		
		if($scope.showIdentifiers)
			$("#identifierTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
		else
			$("#identifierTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
	}
	
	function fillLablesParameters(){
		$scope.clientsNames = "";
		$scope.usersNames = "";
		$scope.identifiersNames = "";
		
		if($scope.clientSelected.length > 0)
			for(client of $scope.clientSelected)
				if(!$scope.clientsNames.includes(client))
					$scope.clientsNames += $scope.clientsNames.length > 0 ? ", " + client : client;  	
					
		if($scope.userSelected.length > 0)
			for(user of $scope.userSelected)
				if(!$scope.usersNames.includes(user))
					$scope.usersNames += $scope.usersNames.length > 0 ? ", " + user : user;
		
		if($scope.identifierSelected.length > 0)
			for(identifier of $scope.identifierSelected)
				if(!$scope.identifiersNames.includes(identifier))
					$scope.identifiersNames += $scope.identifiersNames.length > 0 ? ", " + identifier : identifier;
	}
	
	function filterLists(params){
		let listaSettlementsFiltrados = [];
		
		$scope.settlements.forEach(settlement => {
			listaSettlementsFiltrados.push(settlement);
			let removeuElemento = false;
			
			if($scope.clientSelected.length > 0){
				let idx = 1;
				
				$scope.clientSelected.forEach(client => {
					if(settlement.client.tradingName == client) return true;
					if(settlement.client.tradingName != client && idx == $scope.clientSelected.length){
						let idexSettlement = listaSettlementsFiltrados.indexOf(settlement);
						listaSettlementsFiltrados.splice(idexSettlement);
						removeuElemento = true;
					}
					idx++;
				});
			}
			
			if($scope.userSelected.length > 0 && !removeuElemento){
				let idx = 1;
				
				$scope.userSelected.forEach(user => {
					if(settlement.user.name == user) return true;
					if(settlement.user.name != user && idx == $scope.userSelected.length){
						let indexSettlement = listaSettlementsFiltrados.indexOf(settlement);
						listaSettlementsFiltrados.splice(indexSettlement);
						removeuElemento = true;
					}
					idx++;
				});
			}
			
			if($scope.identifierSelected.length > 0 && !removeuElemento){
				let idx = 1;
				const idtfr = (settlement.paymentId == null || settlement.paymentId.length <= 0) ? '$' : settlement.paymentId;  

				$scope.identifierSelected.forEach(identifier => {
					if(idtfr == identifier) return true;
					if(idtfr != identifier && idx == $scope.identifierSelected.length) {
						let indexSettlement = listaSettlementsFiltrados.indexOf(settlement);
						listaSettlementsFiltrados.splice(indexSettlement);
						removeuElemento = true;
					}
					idx++;
				});
			}
		});
		
		consolidateData(listaSettlementsFiltrados, params);
	}
	
	function fillLists(){
		$scope.settlements.forEach(settlement => {
			if($scope.users.indexOf(settlement.user.name) <= -1)
				$scope.users.push(settlement.user.name);
			
			if($scope.clients.indexOf(settlement.client.tradingName) <= -1)
				$scope.clients.push(settlement.client.tradingName);
			
			const identifierPayment = (settlement.paymentId == null || settlement.paymentId.length <= 0) ? "$" : settlement.paymentId; 
			
			if($scope.identifiers.indexOf(identifierPayment) <= -1)
				$scope.identifiers.push(identifierPayment);
			
		});
	}
	
	function loadDatePicker(){
		$("#dtInicio").datepicker(datePickerOptions);
		$("#dtFinal").datepicker(datePickerOptions);
	}

});
