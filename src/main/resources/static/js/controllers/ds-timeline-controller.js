appGIMB.controller("dsTimelineController", function($scope, $http, geralAPI, $location, juiceService,  $routeParams, config){

	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');
	if (!controllerHeadCheck($scope, $location, juiceService)) return;

	$scope.pathImg = config.pathImg;
	$('.ipt').attr('disabled', 'disabled');
	$('#divMenu').show();
	$("body").css("overflowY", "auto");
	
	$scope.dtInicio = dateToStringPeriodo(new Date(), "i");
	$scope.dtFinal =  dateToStringPeriodo(new Date(), "f");
	
	document.getElementById('dtInicio').readOnly = true;
	document.getElementById('dtFinal').readOnly = true;

	$scope.timeslines = [];
	
	$scope.users = [];
	$scope.userSelected = [];

	$scope.clients = [];
	$scope.clientSelected = [];
	
	$scope.showClient = false;
	$scope.showUsers = false;
	
	$scope.clientsNames = "";
	$scope.usersNames = "";

	$scope.feeds = [];
	$scope.feedsFromDate = [];
	$scope.resume = [];
	
	$scope.selectedDate = "";

	$scope.title = $scope.getLabel('Timeline');

	var canRunReload = false;
	var startDate = getStartDateTM();
	var endDate = getEndDateTM();

	$scope.startDate = startDate.length == 0 ? dateToStringPeriodo(new Date(), "i") : startDate;
	$scope.endDate =  endDate.length == 0 ? dateToStringPeriodo(new Date(), "f") : endDate;

	setStartDateTM($scope.startDate);
	setEndDateTM($scope.endDate);

	load(null);
	loadUsers();
	loadClients();

	function load(date) {
		var params = {};

		if (date == null) {
			$scope.resume = [];
			
			groupByDate = 'true';
			startDate = $scope.dtInicio;
			endDate = $scope.dtFinal;
			
			params = {...params, groupByDate, startDate, endDate};
		}
		else {
			params = {};
			
			groupByDate = 'false';
			startDate = date;
			endDate = date;
			
			params = {...params, groupByDate, startDate, endDate};
		}

		const userLogado = JSON.parse(localStorage.bobby);
		const userLogadoId = userLogado.userId;
		
		if ($scope.userSelected.length > 0)			
			params["user"] = $scope.userSelected.join();
		
		if(userLogadoId != null && userLogadoId != 'undefined') params["userLogadoId"] = userLogadoId; 
		
		if ($scope.clientSelected.length > 0)
			params["client"] = $scope.clientSelected.join();

		$http({
			method: 'put',
			url: config.baseUrl + "/admin/ds/timeLineFeedsByDateBetween",
			data: params
		}).then(function(response) {
			if (date == null) {
				$scope.feeds = response.data;
				fillLablesParameters();
				
				if($scope.feeds.lenght > 0){
					for (const feed of $scope.feeds) feed.message = returnTitle(feed.message);
					compileResume($scope.feeds);	
				}
		
			}
			else {
				$scope.feedsFromDate = response.data;

				for (const feed of $scope.feedsFromDate) {
					feed.message = returnTitle(feed.message);
				}
			}
			loadDatePicker();
			
			canRunReload = true;
		}, function(response) {
			alert($scope.getMessage('ErrorLoadData'));
		});
	}

	function compileResume(feeds) {
		var res = $scope.resume;
		for (var feed of feeds) {
			var arr = feed.message.split('\n');
			for (var str of arr) {
				if (str.length > 0) {
					var a = str.split(':');
					var type = null;

					for (var i in res) {
						var t = res[i];
						if (t.type == returnTitle(a[0])) {
							type = t;
							break;
						}
					}

					if (type == null) {
						type = { type : returnTitle(a[0]), qt : parseInt(a[1].trim()) };
						res.push(type);
					}
					else
						type.qt += parseInt(a[1].trim());
				}
			}
		}
	}

	function loadUsers() {
		$http.get(config.baseUrl + "/admin/ds/listUsers")
		.then(function(response){
			if(response.data.length > 0){				
				for( user of response.data){
					$scope.users.push(user.name);
				}
			}
			
		}, function(error){
			//mensagem de erro
		});
	}

	function loadClients() {
		$http.get(config.baseUrl + "/admin/ds/listClients")
		.then(function(response){
			if(response.data.length > 0){
				for( client of response.data)
					$scope.clients.push(client.tradingName);					
			}
		}, function(error){
			//mensagem de erro
		});
	}

	$scope.reload = function() {
		if (canRunReload) {
			setStartDateTM(document.getElementById("startDate").value);
			setEndDateTM(document.getElementById("endDate").value);

			load(null);
		}
	};

	$scope.loadFromDate = function(date) {
		$scope.selectedDate = date;
		load(date);
		
		$scope.selectUserFromFeed({'user': ''});
	};

	$scope.makeLink = function(feed) {
		console.log('makeLink: ', feed);
		var url = "";

		if (feed.timeLineType == "SER")
			url = config.baseUrl + "/op/serviceDetail/"+feed.id;
		else if (feed.timeLineType == "SET")
			url = config.baseUrl + "/settlementDetail/"+feed.id;

		if (url.length > 0)
			window.open(url, '_blank');
	};
	
	$scope.selectUserFromFeed = function(feed) {
		element = document.getElementById('query');
		setTimeout(() => {
			element.value = feed.user;
			element.focus();

			var event = new Event('change');
			element.dispatchEvent(event);
		}, 50);
	};

	function returnTitle(title) {
		title = title.replace('ENCERRAMENTO DE PONTO', $scope.getLabel('PointClosure'));
		title = title.replace('ENCERRAMENTO DE EVENTO', $scope.getLabel('EventClosure'));
		title = title.replace('KM INICIO', $scope.getLabel('KMStart')).toUpperCase();
		title = title.replace('KM FINAL', $scope.getLabel('FinalKm')).toUpperCase();
		title = title.replace('KM PERCORRIDO', $scope.getLabel('KmTraveled')).toUpperCase();
		title = title.replace('TEMPO', $scope.getLabel('TIME'));
		title = title.replace('Ordens de serviço', $scope.getLabel('ServiceOrders.p'));
		title = title.replace('Registros de ponto', $scope.getLabel('PointRecords.p'));
		title = title.replace('Liquidações', $scope.getLabel('Settlements.p'));
		
		return title;
	}
	
	$scope.showModalWindow = () => {
		$("#parametersModal").modal({ backdrop: 'static', keyboard: false });
	}
	
	$scope.showUserSelect = () => {
		$scope.showUsers = !$scope.showUsers;
		
		if($scope.showUsers)
			$("#userTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
		else
			$("#userTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
	}
	
	$scope.showClientsSelect = () => {
		$scope.showClient = !$scope.showClient;
		
		if($scope.showClient)
			$("#clientTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
		else
			$("#clientTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
	}

	function fillLablesParameters() {
		$scope.clientsNames = "";
		$scope.usersNames = "";
		
		if($scope.clientSelected.length > 0)
			for(client of $scope.clientSelected)
				if(!$scope.clientsNames.includes(client))
					$scope.clientsNames += ($scope.clientsNames.length > 0) ? (", " + client) : client;
					
		if($scope.userSelected.length > 0)
			for(user of $scope.userSelected)
				if(!$scope.usersNames.includes(user))
					$scope.usersNames += ($scope.usersNames.length > 0) ? (", " + user) : user;
	}
	
	$scope.buscar = () => {
		load(null);
	}
	
	function loadDatePicker(){
		$("#dtInicio").datepicker(datePickerOptions);
		$("#dtFinal").datepicker(datePickerOptions);
	}
});