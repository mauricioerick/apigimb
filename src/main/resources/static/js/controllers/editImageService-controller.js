appGIMB.controller("editImageServiceController", function($scope, $http, $location, juiceService, config, $q, $routeParams){
	
	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');
	if (!controllerHeadCheck($scope, $location, juiceService)) return;
	
	$('.ipt').attr('disabled', 'disabled');
	$('#divMenu').show();
	$("body").css("overflowY", "auto");
	
	//$scope.nomeImg = nomeImage;
	$scope.srcImageEdit = '';
	
	var exibeBotaoCancel = true;
	var newImage = null;
	img = document.getElementById('imageEditService');
	$scope.srcImageEditService = "apiGIMB/view/editImage/carregando.gif";
	
	img.style.cssText = "margin-left: 270px;"; 
	
	if (!$routeParams.objId) alert('não contém o ID');
	else obtemObject($routeParams.objId);
	
	function obtemObject(idImageService){
		$http({
			 method: 'GET',
			 url: config.baseUrl + "/pictureService/" + idImageService,
		 }).then(function(response){
			 imageService = response.data;
			 
			 downloadImageService(config.baseUrl, $scope, $http, $q).then(function(data){
				 	$scope.srcImageEditService = base64ImageForEditingService;
					img.style.cssText = "margin-left: none;";
					img.onload = function() { callDarkroom(); };
					exibeBotaoCancel = false;
			 });
				
		}, function(response){
			 alert(scope.getMessage('Error'))
		 });
		
		$http({
			 method: 'GET',
			 url: config.baseUrl + "/getServiceWithImageId/" + idImageService,
		 }).then(function(response){
			 idServiceForBack = response.data;
				
		}, function(response){
			 alert(scope.getMessage('Error'))
		 });
	}
	
	$scope.cancelaAEdicaoService = function(){
		voltar();
	}
	
	$scope.alterar = function(){ 
		$('#modalConfirmImageEdite').modal('hide');
		$('#modal_carregando').modal('show');
		
		trocaImgService(newImage, $http, $scope, config, $q).then(function(data){
			$('#modal_carregando').modal('hide');
			voltar();
		});
	}
	
	function callDarkroom() {
		var dkrm = new Darkroom('#imageEditService', {
			  minWidth: 100,
			  minHeight: 100,
			  maxWidth: 800,
			  maxHeight: 800,
			  backgroundColor: '#FFF',
			  
			  plugins: {
			    save: {
			    	callback: function() {
			            this.darkroom.selfDestroy();
			            newImage = dkrm.canvas.toDataURL();
			            fileStorageLocation = newImage;
			            $('#modalConfirmImageEdite').modal('show');
			        }
			    },
			    crop: {
			    	quickCropKey: 67,
			    },
			  }
		});
	}
	
	$scope.voltar = function () {
		voltar();
	}
	
	function voltar(){
		$location.path(juiceService.appUrl+"op/serviceDetail/" + idServiceForBack);
		
		setTimeout(() => {
			window.location.reload();
		}, 400);
	}
	
	$scope.testaCarregamento = function (){
		return exibeBotaoCancel;
	}
});
