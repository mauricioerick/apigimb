appGIMB.controller("profileController", function ($scope, $http, $routeParams, $location, config, juiceService, geralAPI) {

	if (!controllerHeadCheck($scope, $location, juiceService))
		return;

	$('#divMenu').show();

	$scope.profile = {
		active: true
	};

	$scope.profile.colorId = getRandomColor();
	colorSelectorConf('colorSel', $scope.profile.colorId)

	$scope.profiles = [];
	$scope.allProfiles = [];

	$scope.optActive = "active";

	$scope.appAccess = false;
	$scope.webAccess = false;
	$scope.parameters = false;
	$scope.horaInicio = null;
	$scope.horaFim = null;

	var treeApp;
	var treeWeb;

	if ($routeParams.profileId) {
		loadProfile($routeParams.profileId);
	} else {
		loadProfiles();
	}

	function loadProfile(profileId) {
		$http.get(config.baseUrl + "/admin/profile/" + profileId)
			.then(function (response) {
				$scope.profile = response.data;

				if ($scope.profile.colorId == null)
					$scope.profile.colorId = getRandomColor();

				colorSelectorConf('colorSel', $scope.profile.colorId)

				var valueApp = $scope.profile.appAccess.split(",");
				var listWeb = $scope.profile.webAccess.length > 0 ? JSON.parse($scope.profile.webAccess) : [];

				var valueWeb = [];

				for (var idx in listWeb) {
					var obj = listWeb[idx];

					if (obj.id == "-1" || obj.id == "Cadastros" || obj.id == "Financeiro" || obj.id == "Financeiro Relatórios" || obj.id == "Financeiro Cadastros" || obj.id == 'Operacional' || obj.id == 'Ponto' || obj.id == "Dashboard" || obj.id == "Inventario" || obj.id == "Inventario Cadastros") continue;

					valueWeb.push(obj.id);
				}

				treeApp = new Tree('.app', {
					data: getAppAccess(),
					values: valueApp
				});

				treeWeb = new Tree('.web', {
					data: getWebAccess(),
					values: valueWeb
				});

				$('#approveSettlementAccess').attr('checked', false);
				$('#allowChangeSettlementStatus').attr('checked', false);
				$('#insertPicService').attr('checked', false);
				$('#changePicService').attr('checked', false);
				$('#deletePicService').attr('checked', false);
				$('#editPicService').attr('checked', false);
				$('#insertPicSettlemente').attr('checked', false);
				$('#changePicSettlemente').attr('checked', false);
				$('#deletePicSettlemente').attr('checked', false);
				$('#editPicSettlemente').attr('checked', false);
				$('#servicesAccess').attr('checked', false);
				$('#deleteOS').attr('checked', false);
				$('#settlementAccess').attr('checked', false);
				$('#appointmentAccess').attr('checked', false);
				$('#servicesAccessArea').attr('checked', false);
				$('#settlementAccessArea').attr('checked', false);
				$('#appointmentAccessArea').attr('checked', false);
				$('#editPicSettlemente').attr('checked', false);
				$('#editPicService').attr('checked', false);
				$('#changeExpenseDate').attr('checked', false);
				$scope.horaInicio = '';
				$scope.horaFim = '';

				if ($scope.profile.configWeb != null) {
					objConfigWeb = JSON.parse($scope.profile.configWeb);

					if (objConfigWeb['APPROVESETTLEMENTACCESS'] == '1')
						document.getElementById('approveSettlementAccess').checked = 'true';

					if (objConfigWeb['ALLOWCHANGESETTLEMENTSTATUS'] == '1')
						document.getElementById('allowChangeSettlementStatus').checked = 'true';

					if (objConfigWeb['INSERTPICSETTLEMENTE'] == '1')
						document.getElementById('insertPicSettlemente').checked = "true";

					if (objConfigWeb['CHANGEPICSETTLEMENTE'] == '1')
						document.getElementById('changePicSettlemente').checked = "true";

					if (objConfigWeb['DELETEPICSETTLEMENTE'] == '1')
						document.getElementById('deletePicSettlemente').checked = "true";

					if (objConfigWeb['DELETEOS'] == '1')
						document.getElementById('deleteOS').checked = "true";

					if (objConfigWeb['INSERTPICSERVICE'] == '1')
						document.getElementById('insertPicService').checked = "true";

					if (objConfigWeb['CHANGEPICSERVICE'] == '1')
						document.getElementById('changePicService').checked = "true";

					if (objConfigWeb['DELETEPICSERVICE'] == '1')
						document.getElementById('deletePicService').checked = "true";

					if (objConfigWeb['FILTRARSERVICOUSUARIO'] == '1')
						document.getElementById('servicesAccess').checked = "true";

					if (objConfigWeb['FILTRARLIQUIDACAOUSUARIO'] == '1')
						document.getElementById('settlementAccess').checked = "true";

					if (objConfigWeb['FILTRARAPONTAMENTOUSUARIO'] == '1')
						document.getElementById('appointmentAccess').checked = "true";

					if (objConfigWeb['FILTRARSERVICOAREA'] == '1')
						document.getElementById('servicesAccessArea').checked = "true";

					if (objConfigWeb['FILTRARLIQUIDACAOAREA'] == '1')
						document.getElementById('settlementAccessArea').checked = "true";

					if (objConfigWeb['FILTRARAPONTAMENTOAREA'] == '1')
						document.getElementById('appointmentAccessArea').checked = "true";

					if (objConfigWeb['STARTHOUR'] != null && objConfigWeb['STARTHOUR'] != 'Invalid date')
						document.getElementById('startHour').value = '' + objConfigWeb['STARTHOUR'];

					if (objConfigWeb['ENDHOUR'] != null && objConfigWeb['ENDHOUR'] != 'Invalid date')
						document.getElementById('endHour').value = '' + objConfigWeb['ENDHOUR'];

					if (objConfigWeb['EDITPICSETTLEMENTE'] == '1') 
						document.getElementById('editPicSettlemente').checked = "true";

					if (objConfigWeb['EDITPICSERVICE'] == '1') 
						document.getElementById('editPicService').checked = "true";
					
					if(objConfigWeb['CHANGEEXPENSEDATE'] == '1') 
						document.getElementById('changeExpenseDate').checked = "true";
					
					if(objConfigWeb['CHANGEDATESETTLEMENT'] == '1') 
						document.getElementById('changeDateSettlement').checked = "true";

				}

			}, function (response) {
				alert($scope.getMessage('ErrorLoadData'));
			});
	}

	function loadProfiles() {
		$http.get(config.baseUrl + "/admin/profile").then(function (response) {
			$scope.allProfiles = response.data;

			$scope.query = getTextProfile();
			$scope.optActive = getOptionProfile().length > 0 ? getOptionProfile() : 'active';

			filterProfileByStatus();
		}, function (response) {
			alert($scope.getMessage('ErrorLoadData'));
		});
	}

	$scope.profiles = loadProfiles();

	$scope.saveProfile = function (obj) {
		if (!validateForm())
			return;

		if (invalidDate()) {
			alert(getMessage('ErrorWorkHourInvalid'));
			return;
		}

		newProfile = true;
		var url = config.baseUrl + "/admin/profileCreate";
		if (obj.profileId > 0)
			url = config.baseUrl + "/admin/profileUpdate/" + obj.profileId;

		var listArray = []
		if (treeWeb != null && treeWeb.selectedNodes != null) {
			listArray = treeWeb.selectedNodes;
			var root = "";
			for (node of listArray) {
				node['html'] = ""
				delete node['text'];
				if (node.id == "-1") continue;
				if (root.length == 0)
					root = node.id;

				if (!node.id.includes(root)) {
					node.html += '</ul>';
					root = node.id;
				}

				node.html += getHtmlList(node);
			}

			if (listArray.length > 0) {
				var lastElement = listArray[listArray.length - 1]

				if (lastElement.html == null)
					lastElement.html = "";

				lastElement.html += '</ul>';
			}
		}

		obj.appAccess = treeApp != null ? treeApp.values.toString() : "";
		obj.webAccess = treeWeb != null ? JSON.stringify(listArray) : "";

		var insertPic_Settlemente = 0;
		var changePic_Settlemente = 0;
		var deletePic_Settlemente = 0;
		var editPic_Settlemente = 0;
		var approveSettlement = 0;
		var changeSettlementStatus = 0;
		var changeDateSettlement = 0;

		if (document.getElementById('insertPicSettlemente').checked == 'checked' || document.getElementById('insertPicSettlemente').checked == 'true' || document.getElementById('insertPicSettlemente').checked == 1)
			insertPic_Settlemente = 1;

		if (document.getElementById('changePicSettlemente').checked == 'checked' || document.getElementById('changePicSettlemente').checked == 'true' || document.getElementById('changePicSettlemente').checked == 1)
			changePic_Settlemente = 1;

		if (document.getElementById('deletePicSettlemente').checked == 'checked' || document.getElementById('deletePicSettlemente').checked == 'true' || document.getElementById('deletePicSettlemente').checked == 1)
			deletePic_Settlemente = 1;

		if (document.getElementById('editPicSettlemente').checked == 'checked' || document.getElementById('editPicSettlemente').checked == 'true' || document.getElementById('editPicSettlemente').checked == 1)
			editPic_Settlemente = 1;

		if (document.getElementById('approveSettlementAccess').checked == 'checked' || document.getElementById('approveSettlementAccess').checked == 'true' || document.getElementById('approveSettlementAccess').checked == 1)
			approveSettlement = 1;

		if (document.getElementById('allowChangeSettlementStatus').checked == 'checked' || document.getElementById('allowChangeSettlementStatus').checked == 'true' || document.getElementById('allowChangeSettlementStatus').checked == 1)
			changeSettlementStatus = 1;
		
		if (document.getElementById('changeDateSettlement').checked == 'checked' || document.getElementById('changeDateSettlement').checked == 'true' || document.getElementById('changeDateSettlement').checked == 1)
			changeDateSettlement = 1;
		
		var insertPic_Service = 0;
		var changePic_Service = 0;
		var deletePic_Service = 0;
		var editPic_Service = 0;
		var deleteOS = 0;

		if (document.getElementById('deleteOS').checked == 'checked' || document.getElementById('deleteOS').checked == 'true' || document.getElementById('deleteOS').checked == 1)
			deleteOS = 1;

		if (document.getElementById('insertPicService').checked == 'checked' || document.getElementById('insertPicService').checked == 'true' || document.getElementById('insertPicService').checked == 1)
			insertPic_Service = 1;

		if (document.getElementById('changePicService').checked == 'checked' || document.getElementById('changePicService').checked == 'true' || document.getElementById('changePicService').checked == 1)
			changePic_Service = 1;

		if (document.getElementById('deletePicService').checked == 'checked' || document.getElementById('deletePicService').checked == 'true' || document.getElementById('deletePicService').checked == 1)
			deletePic_Service = 1;

		if (document.getElementById('editPicService').checked == 'checked' || document.getElementById('editPicService').checked == 'true' || document.getElementById('editPicService').checked == 1)
			editPic_Service = 1;


		var filtrarServicoUsuario_ = 0;
		var filtrarLiquidacaoUsuario_ = 0;
		var filtrarApontamentoUsuario_ = 0;

		if (document.getElementById('servicesAccess').checked == 'checked' || document.getElementById('servicesAccess').checked == 'true' || document.getElementById('servicesAccess').checked == 1)
			filtrarServicoUsuario_ = 1;

		if (document.getElementById('settlementAccess').checked == 'checked' || document.getElementById('settlementAccess').checked == 'true' || document.getElementById('settlementAccess').checked == 1)
			filtrarLiquidacaoUsuario_ = 1;

		if (document.getElementById('appointmentAccess').checked == 'checkedsave' || document.getElementById('appointmentAccess').checked == 'true' || document.getElementById('appointmentAccess').checked == 1)
			filtrarApontamentoUsuario_ = 1;


		var filtrarServicoArea_ = 0;
		var filtrarLiquidacaoArea_ = 0;
		var filtrarApontamentoArea_ = 0;
		var changeExpenseDate_ = 0;
		
		if (document.getElementById('servicesAccessArea').checked == 'checked' || document.getElementById('servicesAccessArea').checked == 'true' || document.getElementById('servicesAccessArea').checked == 1)
			filtrarServicoArea_ = 1;

		if (document.getElementById('settlementAccessArea').checked == 'checked' || document.getElementById('settlementAccessArea').checked == 'true' || document.getElementById('settlementAccessArea').checked == 1)
			filtrarLiquidacaoArea_ = 1;

		if (document.getElementById('appointmentAccessArea').checked == 'checkedsave' || document.getElementById('appointmentAccessArea').checked == 'true' || document.getElementById('appointmentAccessArea').checked == 1)
			filtrarApontamentoArea_ = 1;
		
		if (document.getElementById('changeExpenseDate').checked == 'checkedsave' || document.getElementById('changeExpenseDate').checked == 'true' || document.getElementById('changeExpenseDate').checked == 1)
			changeExpenseDate_ = 1;

		obj.config = JSON.stringify({
			startHour: moment($scope.horaInicio).format('HH:mm'),
			endHour: moment($scope.horaFim).format('HH:mm'),
			editPicService: editPic_Service,
			editPicSettlemente: editPic_Settlemente
		});


		obj.configWeb = JSON.stringify({
			approveSettlementAccess: approveSettlement,
			allowChangeSettlementStatus: changeSettlementStatus,
			insertPicService: insertPic_Service,
			deleteOS: deleteOS,
			changePicService: changePic_Service,
			deletePicService: deletePic_Service,
			editPicService: editPic_Service,
			insertPicSettlemente: insertPic_Settlemente,
			changePicSettlemente: changePic_Settlemente,
			deletePicSettlemente: deletePic_Settlemente,
			editPicSettlemente: editPic_Settlemente,
			filtrarServicoUsuario: filtrarServicoUsuario_,
			filtrarLiquidacaoUsuario: filtrarLiquidacaoUsuario_,
			filtrarApontamentoUsuario: filtrarApontamentoUsuario_,
			filtrarServicoArea: filtrarServicoArea_,
			filtrarLiquidacaoArea: filtrarLiquidacaoArea_,
			filtrarApontamentoArea: filtrarApontamentoArea_,
			startHour: moment($scope.horaInicio).format('HH:mm'),
			endHour: moment($scope.horaFim).format('HH:mm'),
			changeExpenseDate: changeExpenseDate_,
			changeDateSettlement: changeDateSettlement
		});


		$http.put(url, obj)
			.then(function (response) {
				if (response.status >= 200 && response.status <= 299) {
					alert($scope.getMessage('DataSavedSuccessfully'));
					$scope.profile = response.data;
					loadProfile($scope.profile.profileId);

					var user = JSON.parse(localStorage.getItem("bobby"));
					if (user.profile && user.profile.profileId == $scope.profile.profileId) {
						user.profile = $scope.profile;
						localStorage.setItem("bobby", JSON.stringify(user));
					}
					
				} else {
					alert($scope.getMessage('ErrorSavingData') + response.status + "\n" + response.message);
				}
			}, function (response) {
				refreshPage();
				alert($scope.getMessage('ErrorSavingData') + response.status + "\n" + response.message);
			});
	};

	$scope.showParameters = function () {
		$scope.parameters = $scope.parameters ? false : true

		if ($scope.parameters)
			$("#parametersPermission").removeClass('glyphicon-menu-down')
				.addClass('glyphicon-menu-up')
		else
			$("#parametersPermission").removeClass('glyphicon-menu-up').addClass(
				'glyphicon-menu-down')
	}

	$scope.showAppAccess = function () {
		$scope.appAccess = $scope.appAccess ? false : true

		if ($scope.appAccess)
			$("#appPermission").removeClass('glyphicon-menu-down')
				.addClass('glyphicon-menu-up')
		else
			$("#appPermission").removeClass('glyphicon-menu-up').addClass(
				'glyphicon-menu-down')
	}

	$scope.showWebAccess = function () {
		$scope.webAccess = $scope.webAccess ? false : true

		if ($scope.webAccess)
			$("#webPermission").removeClass('glyphicon-menu-down')
				.addClass('glyphicon-menu-up')
		else
			$("#webPermission").removeClass('glyphicon-menu-up').addClass(
				'glyphicon-menu-down')
	}

	$scope.filterProfileByStatus = function () {
		filterProfileByStatus();
	};

	function filterProfileByStatus() {
		$scope.profiles = $scope.allProfiles.filter(function (profile) {
			if ($scope.optActive == "all")
				return true;
			else if ($scope.optActive == "active" && profile.active == true)
				return true;
			else if ($scope.optActive == "inactive" && profile.active == false)
				return true;

			return false;
		});

		setOptionProfile($scope.optActive);

		if ($scope.optActive == 'active' && !!document.getElementById('optActive'))
			document.getElementById('optActive').checked = true;
		else if ($scope.optActive == 'inactive')
			document.getElementById('optInactive').checked = true;
		else {
			if (document.getElementById('optAll')) {
				document.getElementById('optAll').checked =
					document.getElementById('optAll')
						? true
						: false
			}
		}
		
		
		$scope.profiles = orderObject( $scope.profiles, 'description' );
		
	}

	$scope.retornaClasse = function (objProfile) {
		if (objProfile.active == true) {
			return 'btnativo'
		} else {
			return 'btninativo'
		}
	}

	$scope.trataAtivoInativo = function (objProfile) {
		if (objProfile.active == true) {
			return $scope.getLabel('Active');
		} else {
			return $scope.getLabel('Inactive');
		}
	}

	$scope.mudaSituacao = function (objProfile) {
		objProfile.active = objProfile.active == true ? false : true;

		$http
			.put(
				config.baseUrl + '/admin/profileUpdate/'
				+ objProfile.profileId, objProfile)
			.then(function (response) {
				alert($scope.getMessage('DataSavedSuccessfully'));
				carregarProfile()
			}, function (response) {
				alert($scope.getMessage('ErrorSavingDataWithoutParam'));
			});
	}

	function carregarProfile() {
		$http.get(config.baseUrl + "/admin/profile").then(function (response) {
			$scope.allProfile = response.data;
			filterProfileByStatus();
		}, function (response) {
			alert($scope.getMessage('ErrorLoadData'));
		});
	}

	function invalidDate() {
		if (($('#startHour')[0]).validity.badInput || ($('#endHour')[0]).validity.badInput) {
			return true;
		}

		if (($scope.horaInicio != null && $scope.horaInicio.toString() == 'Invalid Date') || $scope.horaInicio == undefined)
			$scope.horaInicio = null;

		if (($scope.horaFim != null && $scope.horaFim.toString() == 'Invalid Date') || $scope.horaFim == undefined)
			$scope.horaFim = null;

		if (($scope.horaInicio != null && $scope.horaFim == null) || ($scope.horaFim != null && $scope.horaInicio == null))
			return true;

		return false;
	}

	$scope.setTextProfile = () => {
		setTextProfile($scope.query);
	}

});
