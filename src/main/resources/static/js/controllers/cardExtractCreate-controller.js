appGIMB.controller("cardExtractCreateController", function ($scope, $http, $routeParams, $location, config, juiceService) {

	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');
	if (!controllerHeadCheck($scope, $location, juiceService)) return;

	$("#searchStartDateCard").datepicker(datePickerOptions);
	$("#searchEndDateCard").datepicker(datePickerOptions);

	if ($routeParams.cardReleaseId) {
		console.log($routeParams.cardReleaseId)
	}
	else if ($location.path().indexOf("Create") < 0) {
		// loadExpenses();
	}
	else {
		// loadUsers()
		// loadClients();
		// loadEvents();
	}
});
