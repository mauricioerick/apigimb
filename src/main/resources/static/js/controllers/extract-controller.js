appGIMB.controller("extractController", function ($scope, $http, $location, config, juiceService) {
	"use strict";

	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');
	$('#divMenu').show();

	if (!controllerHeadCheck($scope, $location, juiceService))
		return;

	var reloadData = false;
	let balances = [];
	$scope.cards = [];

	let orderBy = "date";
	const orderByEnum = {
		DATE: 'date',
		USER: 'user',
	}

	$scope.orderBy = "date";

	var lastExtractStatus;

	$scope.userSelected = "";
	
	$scope.showDivSelectUser = false;
	$scope.showDivSelectCard = false;

	$scope.totalByTypePayment 		= [];
	$scope.users 					= [];
	$scope.usersSelectDetailExtract = [];

	orderByShowArrow(orderByEnum.DATE);
	
	var setTypePaymentChange = function() {
		const valueOfTypePaymentSelected = document.getElementById('selFilterSetTypePayment').value;
		var tp = {};
		for(var pt of $scope.paymentTypes)
			if(pt.typePaymentId == valueOfTypePaymentSelected){
				tp = pt;
				break;
			}
		
		$scope.showDivSelectUser = false;
		$scope.showDivSelectCard = false;
		
		if(tp.identificationType == 'TP' || tp.identificationType == null || tp.identificationType == undefined){
			$scope.showDivSelectUser = true;
		}
		else{
			$scope.showDivSelectCard = true;
		}
	}

	$scope.dataUser = {
		user: null,
		typePayment: null,
	}

	$scope.enableClick = extract => {
		const openModal = ((extract.type == 'D') && (extract.status == 'P')) ? true : false;
		openModal ? $('#creditApproval').modal('show') : false;
		return openModal;
	}

	$scope.openApprovalExpense = extract => {

		const { user, amount, settlement, note, releaseDate } = extract;
		$scope.extract = extract;

		$scope.approvalExpense = {
			user: user,
			date: releaseDate,
			event: settlement ? settlement.event : {},
			amount: amount || 0,
			amountApproved: amount || 0,
			note: note || '',
		}
	}

	$scope.saveApproveExpense = () => {
		const { amount, amountApproved, note } = $scope.approvalExpense;

		if (amountApproved > amount) {
			alert(getMessage('ApprovedAmountMustBeLess'));
			return;
		}

		if (amountApproved <= 0) {
			alert(getMessage('ApprovedGreaterThanZero'));
			return;
		}

		$scope.extract = {
			...$scope.extract,
			note,
			amountApproved
		}

		const extractInEdition = $scope.extractList.find(extract => extract.extractId === $scope.extract.extractId);
		extractInEdition.status = 'A'
		extractInEdition.amountApproved = $scope.approvalExpense.amountApproved;

		$scope.confirmApprove($scope.extract, true);
	}

	var divLoading = document.getElementById('divLoading');
	carregamento(divLoading);

	changeScreenHeightSize('#divTableTypePayment');

	// Begin Extract List
	loadUsers();
	loadPaymentTypes();
	loadBalances();

	$scope.changeOrdering = () => {
		changeArrowOrderBy();
		orderBy === orderByEnum.DATE ? orderExtractListFilteredBy(orderByEnum.DATE) : orderExtractListFilteredBy(orderByEnum.USER);
	}

	function orderExtractListFilteredBy(orderType) {
		$scope.extractListFiltered.sort((currentExtract, nextExtract) => {
			switch (orderType) {
				case orderByEnum.DATE:
					const currentDate = currentExtract.releaseDate.split('/').reverse().join('');
					const nextDate = nextExtract.releaseDate.split('/').reverse().join('');
					return currentDate > nextDate ? 1 : currentDate < nextDate ? -1 : 0;
				case orderByEnum.USER:
					const currentName = currentExtract.user.name;
					const nextName = nextExtract.user.name;
					return currentName > nextName ? 1 : currentName < nextName ? -1 : 0;
				default:
					console.error(`${orderType} sort method does not exist...`);
			}
		})
	}

	function changeArrowOrderBy() {
		orderBy === "date" ? orderByShowArrow(orderByEnum.USER) : orderByShowArrow(orderByEnum.DATE);
	}

	function orderByShowArrow(type) {
		if (type === orderByEnum.USER) {
			document.getElementById('arrow-down-user').style.display = "";
			document.getElementById('arrow-down-date').style.display = "none";
			orderBy = type;
		}
		if (type === orderByEnum.DATE) {
			document.getElementById('arrow-down-date').style.display = "";
			document.getElementById('arrow-down-user').style.display = "none";
			orderBy = type;
		}
	}

	$scope.orderByShowArrow = type => {
		orderByShowArrow(type);
	}

	$scope.verifyUserSelected = user => {
		return ($scope.dataUser.user && user.user == $scope.dataUser.user.user) ? 'selected' : '';
	}

	function loadUsers() {
		$scope.usersSelected = [];

		$http.get(config.baseUrl + "/admin/user")
			.then(function (response) {
				$scope.users = response.data;
				$scope.users.sort((a, b) => a.user.toLowerCase().localeCompare(b.user.toLowerCase()));

			}, function (error) {
				alert($scope.getMessage('ErrorLoadingUsers'));
			});
	}

	function loadPaymentTypes() {
		$scope.paymentTypes = [];
		$scope.paymentTypesSelected = [];

		$http.get(config.baseUrl + "/admin/typePaymentByActive/true").then(
			function (response) {
				$scope.paymentTypes = response.data.filter(p => p.consumeBalance).sort((a, b) => a.description.toLowerCase().localeCompare(b.description.toLowerCase()));
			},
			function (error) {
				alert($scope.getMessage('ErrorLoadingPaymentTypes'));
			}
		);
	}

	function loadBalances() {
		var usersString = $scope.usersSelected.map((x) => x.userId).join(";");

		$http({
			method: 'GET',
			url: config.baseUrl + "/admin/balance",
			headers: {
				'userid': usersString
			}
		}).then(
			function (response) {
				balances = response.data;
				adjustData();
				loadCards();
			},
			function (response) {
				alert($scope.getMessage('ErrorLoadData'));
			}
		);
	}

	function adjustData() {
		$scope.summaryExtract = [];

		filteredList();

		// ordenar os saldos pelo nome do usuario
		balances.sort(function (b1, b2) {
			let name1 = b1.card ? b1.card.userName : b1.user.name;
			let name2 = b2.card ? b2.card.userName : b2.user.name;

			if (name1 > name2)
				return 1;

			if (name2 > name1)
				return -1;

			return 0;
		});

		for (const balance of balances) {
			var summary = balance;
			
			if( summary.user != null ){
				$scope.usersSelectDetailExtract.push(summary.user);
			}
			
			summary.balanceFormatted = formatMoney(summary.balance);
			summary.pendingBalanceFormatted = formatMoney(summary.pendingBalance);
			summary.availableBalanceFormatted = formatMoney(summary.availableBalance);
			summary.availablePositive = (summary.availableBalance > 0);

			$scope.summaryExtract.push(summary);
		}

		$scope.setTotalByTypePayment();
	}

	$scope.setTotalByTypePayment = function () {
		let available = 'available';
		let pending = 'pending';
		let reproved = 'reproved';

		$scope.totalByTypePayment = [];

		let list = $scope.summaryExtractFiltered != null ? $scope.summaryExtractFiltered : $scope.summaryExtract;

		for (const summary of list) {

			let available = 0;
			let refund = 0;

			if (summary.availableBalance > 0)
				available = summary.availableBalance;
			else
				refund = Math.abs(summary.availableBalance);

			let typePaymentDesc = summary.typePayment.description;

			if ($scope.totalByTypePayment.length == 0) {
				$scope.totalByTypePayment.push({
					'typePayment': summary.typePayment.typePaymentId,
					typePaymentDesc: summary.typePayment.description,
					pending: summary.pendingBalance,
					reproved: summary.reprovedBalance,
					available: available,
					refund: refund
				});
			} else {
				let objTotalByTypePayment = $scope.totalByTypePayment.find(obj => obj.typePayment === summary.typePayment.typePaymentId);

				if (objTotalByTypePayment == null) {
					$scope.totalByTypePayment.push({
						'typePayment': summary.typePayment.typePaymentId,
						typePaymentDesc: summary.typePayment.description,
						pending: summary.pendingBalance,
						reproved: summary.reprovedBalance,
						available: available,
						refund: refund
					});
				} else {
					objTotalByTypePayment.pending += summary.pendingBalance;
					objTotalByTypePayment.reproved += summary.reprovedBalance;
					objTotalByTypePayment.available += available;
					objTotalByTypePayment.refund += refund;
				}
			}

		}
	};

	$scope.reload = function () {
		if ($scope.showUsers)
			$scope.showUsersSelect();

		if ($scope.showTypePayment)
			$scope.showPaymentTypesSelect();

		$("#parametersModal").modal('hide')
		loadBalances();
	};

	function filteredList() {
		function filter(balance) {	

			if ($scope.paymentTypesSelected.length > 0) {
				let filtered = $scope.paymentTypesSelected.filter(function (typePayment) {
					if (typePayment.typePaymentId != balance.typePayment.typePaymentId)
						return false;
					else
						return true;
				});

				if (filtered.length == 0)
					return false;
			}

			return true;
		}

		if (balances != null) {
			fillLabelsParameters();
			balances = balances.filter(filter);
		}
	}

	function fillLabelsParameters() {
		$scope.usersNames = "";
		if ($scope.usersSelected.length > 0) {
			for (const user of $scope.usersSelected) {
				if (!$scope.usersNames.includes(user.user)) {
					$scope.usersNames += $scope.usersNames.length == 0 ? user.user : ", " + user.user;
				}
			}
		}

		$scope.paymentTypesNames = "";
		if ($scope.paymentTypesSelected.length > 0) {
			for (const typePayment of $scope.paymentTypesSelected) {
				if (!$scope.paymentTypesNames.includes(typePayment.description)) {
					$scope.paymentTypesNames += $scope.paymentTypesNames.length == 0 ? typePayment.description : ", " + typePayment.description;
				}
			}
		}
	}

	$scope.openExtractDetailModal = function(summary, isCard) {
		const { user, typePayment, card } = summary;

		$scope.dataUser = {
			...$scope.dataUser,
			user,
			typePayment: typePayment.description,
			card
		}

		$scope.typePayment = typePayment;
		
		if(!isCard) {
			$scope.userSelected = user.userId.toString();
			loadExtract();
		}
		else {
			$scope.userSelected = "";
			loadExtractsCard();
		}	
	};

	$scope.showUsersSelect = function () {
		$scope.showUsers = !$scope.showUsers;

		if ($scope.showUsers)
			$("#userTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up')
		else
			$("#userTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down')
	};

	$scope.showPaymentTypesSelect = function () {
		$scope.showTypePayment = !$scope.showTypePayment;

		if ($scope.showTypePayment)
			$("#typePaymentTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up')
		else
			$("#typePaymentTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down')
	};

	$scope.showModalWindow = function () {
		$("#parametersModal").modal({ backdrop: 'static', keyboard: false }).unbind('click');
	};
	// End Extract List

	// Extract Detail
	$("#dtInicio").datepicker(datePickerOptions);
	$("#dtFinal").datepicker(datePickerOptions);
	$("#dtCredito").datepicker(datePickerOptions);
	changeScreenHeightSize('#divExtractList');
	var error = $scope.getMessage('ErrorSavingData');
	var today = new Date();

	$scope.extract = {};
	$scope.extractList = [];
	$scope.extractListFiltered = [];
	$scope.optType = "all";
	$scope.optStatus = "all";

	$scope.dtInicio = $scope.dtInicio == null ? dateToStringPeriodo(today, "i") : $scope.dtInicio;
	$scope.dtFinal = $scope.dtFinal == null ? dateToStringPeriodo(today, "f") : $scope.dtFinal;

	$scope.loadExtract = function () {
		loadExtract();
	};

	function loadExtract() {

		var params = {};

		var type = [];
		var status = [];

		var radioDeb = document.getElementById('radioTypeDebit');
		var radioCred = document.getElementById('radioTypeCredit');

		var radioStatusAprovado = document.getElementById('radioStatusAprovado');
		var radioStatusDesaprovado = document.getElementById('radioStatusDesaprovado');
		var radioStatusPendente = document.getElementById('radioStatusPendente');

		if (radioDeb.checked)
			type.push('D');

		if (radioCred.checked)
			type.push('C');

		if (radioStatusAprovado.checked)
			status.push('A');

		if (radioStatusDesaprovado.checked)
			status.push('R');

		if (radioStatusPendente.checked)
			status.push('P');

		let userParam = null;

		if ($scope.userSelected === "") {
			userParam = null;
		} else {
			const userToSave = $scope.usersSelectDetailExtract.find(userLocated => userLocated.userId == $scope.userSelected)
			userParam = userToSave;
		}

		params["startDate"]   = $scope.dtInicio;
		params["endDate"]     = $scope.dtFinal;
		params["user"] 		  = userParam;
		params["typePayment"] = $scope.typePayment;
		params["type"] 		  = type;
		params["status"]      = status;

		setTimeout(function () {
			$('#modal_carregando').modal('show');
		}, 0);

		$http({
			method: 'PUT',
			url: config.baseUrl + "/admin/extract/findAllBetweenReleaseDate",
			data: params
		}).then(function (response) {
			$scope.extractList = response.data;

			filterExtractByType();
			filterExtractByStatus();

			orderExtractListFilteredBy(orderBy);

			if (!$('#extractDetail').is(':visible'))
				$('#extractDetail').modal();

			// createUrlDownload();
			$('#modal_carregando').modal('hide');

		}, function (response) {
			alert($scope.getMessage('ErrorLoadData'));
			$('#modal_carregando').modal('hide');
		});
		$('#modal_carregando').modal('hide');
	}
	
	$scope.loadExtractsCard = function (){
		loadExtractsCard();
	}
	
	function loadExtractsCard() {
		
		var params = {};

		var type = [];

		var radioDeb = document.getElementById('radioTypeDebitCard');
		var radioCred = document.getElementById('radioTypeCreditCard');

		if (radioDeb.checked)
			type.push('D');

		if (radioCred.checked)
			type.push('C');

		let userParam = null;

		if ($scope.userSelected === "") {
			userParam = null;
		} else {
			const userToSave = $scope.usersSelectDetailExtract.find(userLocated => userLocated.userId == $scope.userSelected)
			userParam = userToSave;
		}

		params["startDate"] = $scope.dtInicio;
		params["endDate"] = $scope.dtFinal;
		params["typePayment"] = $scope.typePayment.typePaymentId;
		params["type"] = type;
		params["card"] = $scope.dataUser.card.cardId;
		params["user"] = $scope.userSelected;
		
		setTimeout(function () {
			$('#modal_carregando').modal('show');
		}, 0);

		$http({
			method: 'PUT',
			url: config.baseUrl + "/admin/extract/findAllBetweenReleaseDateCard",
			data: params
		}).then(function (response) {
			$scope.extractList = response.data;

			filterExtractByType();
			filterExtractByStatus();

			orderExtractListFilteredBy(orderBy);

			if (!$('#extractDetailCard').is(':visible'))
				$('#extractDetailCard').modal();

			$('#modal_carregando').modal('hide');

		}, function (response) {
			alert($scope.getMessage('ErrorLoadData'));
			$('#modal_carregando').modal('hide');
		});
		$('#modal_carregando').modal('hide');
	}

	$scope.returnDescription = function (extract) {
		if (extract.settlement != null) {
			return extract.settlement.event.description;
		} else {
			return ((extract.type == 'C') ? getMessage('addedCredit') : getMessage('addedDebit'));
		}
	}

	function loadSettlement() {
		$http.get(config.baseUrl + "/settlement/" + $scope.extract.settlement.settlementId)
			.then(function (response) {
				$scope.extract.settlement = response.data;
				$scope.listaImagensPrintLiq = $scope.extract.settlement.customPictureList;

				openModalInfo();
			}, function (error) {
				// mensagem de erro
			});
	}

	function openModalInfo() {
		$('#modalInfo').modal();
	}

	$scope.addExtractToModel = function (extract) {
		$scope.extract = extract;
		lastExtractStatus = extract.status;
		if ($scope.extract.settlement != null && $scope.extract.settlement.customPictureList == null) {
			loadSettlement();

			if ($scope.extract.type == 'D') {
				$scope.rows = 5;
				let info = getMessage('MessageInfoExtract')
					.replace('{event}', extract.settlement.event.description)
					.replace('{data}', extract.settlement.startTime)
					.replace('{client}', extract.settlement.client.tradingName)
					.replace('{typePayment}', extract.typePayment.description)
					.replace('{user}', extract.user.name)
					.replace('{id}', extract.settlement.settlementId)
					.replace('{createdData}', extract.releaseDate);

				if (extract.settlement.note.length > 0) {
					info += '\n------------------------------------------------------------------------------\n';
					info += getLabel('UserNotes').replace('{note}', extract.settlement.note);

					$scope.rows = 10;
				}

				$scope.extract.info = info;
			}
		} else {
			$scope.listaImagensPrintLiq = ($scope.extract.settlement != null) ? $scope.extract.settlement.customPictureList : null;
			openModalInfo();
		}
	};

	function saveExtractDB() {
		const url = config.baseUrl + '/admin/extract/updateExtract';

		$http.put(url, $scope.extract).then(
			function (response) {
				if (!(response.status >= 200 && response.status <= 299)) {
					alert(error + response.status + "\n" + response.message);
				} else {
					const extractEditable = $scope.extractList.find(extractLocated => extractLocated.extractId == response.data.extractId);
					extractEditable.status = response.data.status;
					extractEditable.amountApproved = response.data.amountApproved;

					$scope.extract = response.data;
					$scope.dataUser.user = response.data.user;
					alert($scope.getMessage('DataSavedSuccessfully'));
				}
			},
			function (response) {
				alert(error + response.status + "\n" + response.message);
			}
		);
	}

	$scope.dismissModalNote = function () {
		$scope.extract.status = lastExtractStatus;
	}

	$scope.saveExtract = function (approve) {
		if (approve == false) {
			if ($('#modalNote').is(':visible')) {
				if ($scope.extract.note != null && $scope.extract.note.length > 0) {
					saveExtractDB();
					$('#modalNote').modal('toggle');
				} else {
					alert($scope.getMessage('NecessaryInformObservation'));
				}
			} else if ($scope.extract.status === 'R') {
				$("#modalNote").modal({ backdrop: 'static' });
			}
		} else if (approve == true) {
			saveExtractDB();
			$('#creditApproval').modal('hide');
		}
	};

	function createExtract(callDirectly) {
		const valueOfTypePaymentSelected = document.getElementById('selFilterSetTypePayment').value;
		var tp = {};
		for(var pt of $scope.paymentTypes)
			if(pt.typePaymentId == valueOfTypePaymentSelected){
				tp = pt;
				break;
			}
		
		const isCardSel = !(tp.identificationType == 'TP' || tp.identificationType == null || tp.identificationType == undefined);
		
		if(isCardSel){
			var card = {};
			const cardSel = $scope.dataUser.card.cardId;
			
			for(var c of $scope.cards)
				if (c.cardId == cardSel){
					card = c;
					break;
				}
			
			$scope.newExtract.card = card;
		}
		
		const url = config.baseUrl + '/admin/extract/createExtract';
		
		$http.post(url, $scope.newExtract)
		.then(
			function (response) {
				if (response.status >= 200 && response.status <= 299) {
					alert($scope.getMessage('DataSavedSuccessfully'));

					if ($('#extractDetail').is(':visible'))
						loadExtract();
					
					if ($('#extractDetailCard').is(':visible'))
						loadExtractsCard();

					loadBalances();
				} else {
					alert(error + response.status + "\n" + response.message);
				}
			},
			function (response) {
				alert(error + response.status + "\n" + response.message);
			}
		);
	}
	

	$scope.filterExtractByType = function () {
		filterExtractByType();
		orderExtractListFilteredBy(orderBy);
	};

	$scope.mostrarNome = extract => {
		console.log(extract);
	}

	function filterExtractByType() {
		let temp = $scope.extractList.filter(matchType);
		$scope.extractListFiltered = temp.filter(matchStatus);
	}

	function matchType(extract) {
		if ($scope.optType == 'all')
			return true;
		else if ($scope.optType == 'D' && extract.type == 'D')
			return true;
		else if ($scope.optType == "C" && extract.type == 'C')
			return true;
		return false;
	}

	$scope.filterExtractByStatus = function () {
		filterExtractByStatus();
		orderExtractListFilteredBy(orderBy);
	};

	function filterExtractByStatus() {
		let temp = $scope.extractList.filter(matchStatus);
		$scope.extractListFiltered = temp.filter(matchType);
	}

	function matchStatus(extract) {
		if ($scope.optStatus == 'all')
			return true;
		else if ($scope.optStatus == 'A' && extract.status == 'A')
			return true;
		else if ($scope.optStatus == 'P' && extract.status == 'P')
			return true;
		else if ($scope.optStatus == 'R' && extract.status == 'R')
			return true;

		return false;
	}

	$scope.formatMoney = function (value) {
		return formatMoney(value);
	};

	$scope.dayWithMonth = function (date) {
		const day = date.slice(0, 2);
		return day + ' ' + getMonthInFull(date);
	};

	$scope.statusByFull = function (status) {
		switch (status) {
			case "A":
				return getLabel('Approved');
			case "P":
				return getLabel('Pending');
			case "R":
				return getLabel('Disapproved');

			default:
				break;
		}
	};

	$scope.openCreditModal = function (fromBalance, editExtract) {
		let createUser = JSON.parse(localStorage.getItem("bobby"));
		
		$('#selFilterSetUser').prop('disabled', 'disabled');
		$('#selFilterSetTypePayment').prop('disabled', 'disabled');
		$('#selFilterSetCard').prop('disabled', 'disabled');
		if (!fromBalance) {
			$scope.typePayment = null;
			$('#selFilterSetUser').prop('disabled', false);
			$('#selFilterSetTypePayment').prop('disabled', false);
			$('#selFilterSetCard').prop('disabled', false);
		}

		$scope.newExtract = editExtract || {
			releaseDate: null,
			type: 'C',
			amount: null,
			typePayment: $scope.typePayment,
			status: 'A',
			createDate: timestampNowDDMMYYYY(),
			createUser: createUser,
			amountApproved: 0.0,
			user: $scope.dataUser.user,
			note: '',
			info: '',
			card: ($scope.dataUser != null && $scope.dataUser.card != null ? $scope.dataUser.card : null)
		};

		if (fromBalance && editExtract) {
			$scope.changeLauchOption(editExtract.type);
		}

		$scope.credit = $scope.newExtract.amount;

		$('#selFilterSetUser').css({ 'border-width': '1px', 'border-style': 'solid', 'border-color': '#e7e7e7' });
		$('#selFilterSetTypePayment').css({ 'border-width': '1px', 'border-style': 'solid', 'border-color': '#e7e7e7' });
		$('#dtCredito').css({ 'border-width': '1px', 'border-style': 'solid', 'border-color': '#e7e7e7' });
		$('#newCreditValue').css({ 'border-width': '1px', 'border-style': 'solid', 'border-color': '#e7e7e7' });
		$('#newNote').css({ 'border-width': '1px', 'border-style': 'solid', 'border-color': '#e7e7e7' });

		setTimeout(() => {
			setTypePaymentChange();
			
			$('#modalAddCredit').modal();
			$scope.$apply();
		}, 200);
	};

	const releaseDateGreaterToday = () => {

		const releaseDate = moment($scope.newExtract.releaseDate, "DD/MM/YYYY");
		const today = new Date();
		today.setHours(0, 0, 0, 0);

		if ( !releaseDate.isValid() || releaseDate.isAfter(today)) {
			alert(getMessage('InformedDateIncorrect'));
			throw new Exception();
		}
	}

	$scope.validations = function () {
		if (!validateForm())
			return;

		// Nao permitir lancamento com data maior que hoje
		try {
			releaseDateGreaterToday();
		} catch (error) {
			return;
		}

		$scope.newExtract = {
			...$scope.newExtract,
			amount: $scope.credit.toFixed(2),
			info: $scope.newExtract.note,
			amountApproved: $scope.credit.toFixed(2),
		}

		createExtract();

		$('#modalAddCredit').modal('toggle');
	};

	function canApprove() {
		const user = JSON.parse(localStorage['bobby']);
		const configWeb = JSON.parse(user['profile']['configWeb']);

		return configWeb['APPROVESETTLEMENTACCESS'] == 1;
	};

	function canChangeApprove() {
		const user = JSON.parse(localStorage['bobby']);
		const configWeb = JSON.parse(user['profile']['configWeb']);

		return configWeb['ALLOWCHANGESETTLEMENTSTATUS'] == 1;
	};

	$scope.getCursorApprove = function (extract) {
		if (extract.type == 'D') {
			if (extract.status == 'P' && canApprove())
				return 'pointer';
			else if (extract.status != 'P' && canChangeApprove())
				return 'pointer';
		}

		return 'default';
	}

	$scope.getColorApprove = function (extract) {
		return (extract.status == 'P' || extract.status == 'A') ? 'rgba(0, 128, 0, 1)' : 'rgba(0, 128, 0, 0.3)';
	};

	$scope.getColorDisapprove = function (extract) {
		return (extract.status == 'P' || extract.status == 'R') ? 'rgba(255, 0, 0, 1)' : 'rgba(255, 0, 0, 0.3)';
	};

	$scope.getColorStatus = function (extract) {
		if (extract.status == 'A')
			return '7d7d7d'
		else if (extract.status == 'P')
			return 'darkorange'
		else if (extract.status == 'R')
			return 'red'

		return '#FFF'
	};

	$scope.confirmApprove = (extract, approve) => {
		const { type, status } = extract;

		if (type == 'D') {
			if ((status == 'P' && canApprove()) || status != 'P' && canChangeApprove()) {
				reloadData = true;
				lastExtractStatus = status;

				extract = {
					...extract,
					status: approve ? 'A' : 'R',
					amountApproved: approve ? extract.amountApproved : 0.0,
					approveDate: timestampNowDDMMYYYY(),
					approveUser: JSON.parse(localStorage['bobby']),
				}

				$scope.extract = extract;
				$scope.saveExtract(approve);
			}
		}
	}

	$('#extractDetail').on('show.bs.modal', function () {
		reloadData = false;
	});

	$('#extractDetail').on('hidden.bs.modal', function () {
		if (reloadData) {
			$scope.reload();
			reloadData = false;
		}
	});
	
	function createUrlDownloadCard(){
		if ($scope.dataUser == null || $scope.dataUser.card == null) 
			return
		
		const radioDeb = document.getElementById('radioTypeDebitCard');
		const radioCred = document.getElementById('radioTypeCreditCard');
		
		var str = '';

		const initDate = formataData($scope.dtInicio);
		const finalDate = formataData($scope.dtFinal);
		
		const user = $scope.userSelected;
		
		var domain = 'EXTRACTCARD';
		
		str = config.baseUrl + '/export-excel/' + domain + '?startDate=' + initDate + '&endDate=' + finalDate + '&language=' + localStorage["language"]
		 + '&cardId=' + $scope.dataUser.card.cardId;
		
		if(user != '' && user != null && user != undefined)
			str +=  '&userId=' + user;
		
		if($scope.typePayment != null && $scope.typePayment.typePaymentId != '' && $scope.typePayment.typePaymentId != null && $scope.typePayment.typePaymentId != undefined)
			str += '&typePayment=' + $scope.typePayment.typePaymentId;
		
		if(radioDeb.checked)
			if(str.indexOf('type=D') <= -1)
				str += '&type=D';
		
		if(radioCred.checked)
			if(str.indexOf('type=C') <= -1)
				str += '&type=C';
		
		return str;
	}

	function createUrlDownload() {
		var radioDeb = document.getElementById('radioTypeDebit');
		var radioCred = document.getElementById('radioTypeCredit');

		var radioStatusAprovado = document.getElementById('radioStatusAprovado');
		var radioStatusDesaprovado = document.getElementById('radioStatusDesaprovado');
		var radioStatusPendente = document.getElementById('radioStatusPendente');

		var str = '';

		var initDate = formataData($scope.dtInicio);
		var finalDate = formataData($scope.dtFinal);

		let saldo = 0;
		let pendenteAprovacao = 0;
		let saldoDisponivel = 0;
		var calculatedUsers = [];

		$scope.extractListFiltered.map(extract => {

			var locatedExtract = null;
			
			if(extract.user){
				var userCalculed = calculatedUsers.find(calculedUserId => extract.user.userId === calculedUserId);
				if (!userCalculed) {
					locatedExtract = $scope.summaryExtractFiltered.find(summary =>	 summary.user ? summary.user.userId === extract.user.userId : false);
				}
				calculatedUsers.push(extract.user.userId);
			} else {
				locatedExtract = $scope.summaryExtractFiltered.find(summary => summary.card ? summary.card.cardId === extract.card.cardId : false);
			}
		
			if( locatedExtract != null ){
				saldo += parseFloat(locatedExtract.balanceFormatted && (locatedExtract.balanceFormatted).replace('.', '').replace(',', '.'));
				pendenteAprovacao += parseFloat((locatedExtract.pendingBalanceFormatted).replace('.', '').replace(',', '.'));
				saldoDisponivel += parseFloat((locatedExtract.availableBalanceFormatted).replace('.', '').replace(',', '.'));
			}

		})

		saldo = saldo.toLocaleString('pt-BR');
		pendenteAprovacao = pendenteAprovacao.toLocaleString('pt-BR');
		saldoDisponivel = saldoDisponivel.toLocaleString('pt-BR');

		const user = ($scope.dataUser.user) ? $scope.dataUser.user.name : getLabel('All').toUpperCase();
		const userId = ($scope.dataUser.user) ? $scope.dataUser.user.userId : 0;

		var domain = 'EXTRACT';
		str = config.baseUrl + '/export-excel/' + domain + '?startDate=' + initDate + '&endDate=' + finalDate + '&userId=' + userId + '&language=' + localStorage["language"] +
			'&saldo=' + saldo + '&pendenteAprovacao=' + pendenteAprovacao + '&saldoDisponivel=' + saldoDisponivel + '&name=' + user;

		if (radioDeb.checked) {
			if (str.indexOf('type=D') != -1)
				str = str;

			else
				str += '&type=D';

		}

		if (radioCred.checked) {
			if (str.indexOf('type=C') != -1)
				str = str;

			else
				str += '&type=C';
		}


		if (radioStatusAprovado.checked) {
			if (status.indexOf('status=A') != -1)
				str = str;

			else
				str += '&status=A';

		}

		if (radioStatusPendente.checked) {
			if (status.indexOf('status=P') != -1)
				str = str;

			else
				str += '&status=P';
		}

		if (radioStatusDesaprovado.checked) {
			if (status.indexOf('status=R') != -1)
				str = str;

			else
				str += '&status=R';
		}

		return str;
	}


	$scope.createUrlDownload = function () {
		return createUrlDownload();
	}
	
	$scope.createUrlDownloadCard = function(){
		return createUrlDownloadCard();
	}

	function formataData(data) {
		var newDate = data.replace('/', '-');
		newDate = newDate.replace('/', '-');
		return newDate;
	}

	$scope.exibiBotaoExtractExcel = function () {
		return $scope.extractList != null && $scope.extractList.length > 0;
	}

	$scope.changeLauchOption = function (typeLauch) {
		$scope.newExtract = {
			...$scope.newExtract,
			type: typeLauch,
		}

		document.getElementById('creditOption').style.fontFamily = 'gilroy';
		document.getElementById('creditOption').style.opacity = 0.5;
		document.getElementById('debitOption').style.fontFamily = 'gilroy';
		document.getElementById('debitOption').style.opacity = 0.5;

		if (typeLauch == 'C') {
			document.getElementById('creditOption').style.fontFamily = 'gilroyBold';
			document.getElementById('creditOption').style.opacity = 1;
		}
		else if (typeLauch == 'D') {
			document.getElementById('debitOption').style.fontFamily = 'gilroyBold';
			document.getElementById('debitOption').style.opacity = 1;
		}
	}

	$("#selFilterSetTypePayment").on('change', setTypePaymentChange);
	
	$("#selFilterSetCard").on('change', function(){
		$scope.dataUser.card = {};
		const cardId = document.getElementById('selFilterSetCard').value;
		
		for(var card of $scope.cards)
			if(card.cardId == cardId){
				$scope.dataUser.card = card;
				break;
			}
	});
	
	function loadCards(){
		var url = config.baseUrl + '/admin/allCards';
		
		$http.get(url).then(
				function(response){
					$scope.cards = response.data;
				}, function(response){
					alert(error + response.status + "\n" + response.message);
				}
		);
	}
});
