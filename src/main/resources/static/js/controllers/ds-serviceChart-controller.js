appGIMB.controller("dsServiceChartController", function ($scope, $http, $location, config, juiceService) {

  $('form').attr('autocomplete', 'off');
  $('input[type=text]').attr('autocomplete', 'off');
  if (!controllerHeadCheck($scope, $location, juiceService, false)) return;

  $scope.pathImg = config.pathImg;
  $('.ipt').attr('disabled', 'disabled');
  $('#divMenu').show();
  $("body").css("overflowY", "auto");
  $("#dtInicio").datepicker(datePickerOptions);
  $("#dtFinal").datepicker(datePickerOptions);

  $scope.listServices = [];

  $scope.users = [];
  $scope.usersSelected = [];
  $scope.usersNames = "";

  $scope.actions = [];
  $scope.actionsSelected = [];
  $scope.actionsNames = "";

  $scope.clients = [];
  $scope.clientsSelected = [];
  $scope.clientsNames = "";

  $scope.equipments = [];
  $scope.equipmentsSelected = [];
  $scope.equipmentsNames = "";

  $scope.showUsers = false;
  $scope.showActions = false;
  $scope.showClients = false;
  $scope.showEquipments = false;

  $scope.quantity = true;
  $scope.hour = false;

  var filtredServiceList = [];
  var filtredServiceActionList = [];
  var canRunReload = false;

  var today = new Date();

  var dtIni = getDataInicioFiltroOS();
  var dtFin = getDataFinalFiltroOS();

  defaultColors = {};

  $scope.dtInicio = dtIni.length == 0 ? dateToStringPeriodo(today, "i") : dtIni;
  $scope.dtFinal = dtFin.length == 0 ? dateToStringPeriodo(today, "f") : dtFin;

  setDataInicioFiltroOS($scope.dtInicio);
  setDataFinalFiltroOS($scope.dtFinal);

  var ctxByClient = document.getElementById("chartByClient").getContext('2d');
  var chartByClient = new Chart(ctxByClient, {
    type: 'pie',
    options: {
      plugins: {
        datalabels: {
          color: 'white',
          display: function (context) {
            return context.dataset.data[context.dataIndex] >= 1;
          },
          font: {
            weight: 'bold'
          },
          clip: true,
          title: false,
          formatter: function (value, context) {
            var lb = formatterHourGraphLabel(value, context) + '\n' + percChart(context.dataset, context.datasetIndex, context.dataIndex);
            return lb;
          }
        }
      },
      tooltips: {
        custom: function (tooltip) {
          //tooltip.x = 0;
          //tooltip.y = 0;
        },
        mode: 'single',
        callbacks: {
          label: function (tooltipItems, data) {
            var l = data.labels[tooltipItems.index] + ':' + formatterHourGraphLabel(data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index], chartByClient.controller);
            return l;
          },
          beforeLabel: function (tooltipItems, data) {
            return percChart(data.datasets, tooltipItems.datasetIndex, tooltipItems.index);
          }
        }
      }
    }
  });

  var ctxByDay = document.getElementById("chartByDay").getContext('2d');
  var chartByDay = new Chart(ctxByDay, {
    type: 'bar',
    options: {
      plugins: {
        datalabels: {
          color: 'black',
          display: function (context) {
            return true;
          },
          font: {
            weight: 'bold'
          },
          clip: true,
          title: false,
          formatter: function (value, context) {
            var lb = formatterHourGraphLabel(value, context) + '\n' + percChart(context.dataset, context.datasetIndex, context.dataIndex);
            return lb;
          }
        }
      },
      tooltips: {
        custom: function (tooltip) {
          //tooltip.x = 0;
          //tooltip.y = 0;
        },
        mode: 'single',
        callbacks: {
          label: function (tooltipItems, data) {
            var l = formatterHourGraphLabel(data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index], chartByClient.controller);
            return l;
          },
          beforeLabel: function (tooltipItems, data) {
            return percChart(data.datasets, tooltipItems.datasetIndex, tooltipItems.index);
          }
        }
      }
    }
  });

  var ctxByAction = document.getElementById("chartByAction").getContext('2d');
  var chartByAction = new Chart(ctxByAction, {
    type: 'pie',
    options: {
      plugins: {
        datalabels: {
          color: 'white',
          display: function (context) {
            return context.dataset.data[context.dataIndex] >= 1;
          },
          font: {
            weight: 'bold'
          },
          clip: true,
          title: false,
          formatter: function (value, context) {
            var lb = formatterHourGraphLabel(value, context) + '\n' + percChart(context.dataset, context.datasetIndex, context.dataIndex);
            return lb;
          }
        }
      },
      tooltips: {
        custom: function (tooltip) {
          //tooltip.x = 0;
          //tooltip.y = 0;
        },
        mode: 'single',
        callbacks: {
          label: function (tooltipItems, data) {
            var l = data.labels[tooltipItems.index] + ':' + formatterHourGraphLabel(data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index], chartByClient.controller);
            return l;
          },
          beforeLabel: function (tooltipItems, data) {
            return percChart(data.datasets, tooltipItems.datasetIndex, tooltipItems.index);
          }
        }
      }
    }
  });

  var ctxByActionAndClient = document.getElementById("chartByActionAndClient").getContext('2d');
  var chartByActionAndClient = new Chart(ctxByActionAndClient, {
    type: 'horizontalBar',
    options: {
      scales: {
        xAxes: [{
          stacked: true
        }],
        yAxes: [{
          stacked: true
        }]
      },
      plugins: {
        datalabels: {
          color: 'black',
          display: function (context) {
            return context.dataset.data[context.dataIndex] >= 1;
          },
          font: {
            weight: 'bold'
          },
          clip: true,
          title: false,
          formatter: function (value, context) {
            var lb = formatterHourGraphLabel(value, context) + '\n' + percChart(context.dataset, context.datasetIndex, context.dataIndex);
            return lb;
          }
        }
      },
      tooltips: {
        custom: function (tooltip) {
          //tooltip.x = 0;
          //tooltip.y = 0;
        },
        mode: 'single',
        callbacks: {
          label: function (tooltipItems, data) {
            var l = data.datasets[tooltipItems.datasetIndex].label + ':' + formatterHourGraphLabel(data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index], chartByClient.controller);
            return l;
          },
          beforeLabel: function (tooltipItems, data) {
            return percChart(data.datasets, tooltipItems.datasetIndex, tooltipItems.index);
          }
        }
      }
    }
  });

  var ctxByActionAndUser = document.getElementById("chartByActionAndUser").getContext('2d');
  var chartByActionAndUser = new Chart(ctxByActionAndUser, {
    type: 'radar',
    options: {
      plugins: {
        datalabels: {
          display: function (context) {
            return false;
          },
          formatter: function (value, context) {
            return formatterHourGraphLabel(value, context)
          }
        }
      },
      tooltips: {
        custom: function (tooltip) {
          //tooltip.x = 0;
          //tooltip.y = 0;
        },
        mode: 'single',
        callbacks: {
          label: function (tooltipItems, data) {
            var l = data.datasets[tooltipItems.datasetIndex].label + ':' + formatterHourGraphLabel(data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index], chartByClient.controller);
            return l;
          },
          beforeLabel: function (tooltipItems, data) {
            return percChart(data.datasets, tooltipItems.datasetIndex, tooltipItems.index);
          }
        }
      }
    }
  });

  var ctxByEquipment = document.getElementById("chartByEquipment").getContext('2d');
  var chartByEquipment = new Chart(ctxByEquipment, {
    type: 'polarArea',
    options: {
      plugins: {
        datalabels: {
          color: 'black',
          display: function (context) {
            return context.dataset.data[context.dataIndex] >= 1;
          },
          font: {
            weight: 'bold'
          },
          clip: true,
          title: false,
          formatter: function (value, context) {
            var lb = formatterHourGraphLabel(value, context) + '\n' + percChart(context.dataset, context.datasetIndex, context.dataIndex);
            return lb;
          }
        }
      },
      tooltips: {
        custom: function (tooltip) {
          //tooltip.x = 0;
          //tooltip.y = 0;
        },
        mode: 'single',
        callbacks: {
          label: function (tooltipItems, data) {
            var l = data.labels[tooltipItems.index] + ':' + formatterHourGraphLabel(data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index], chartByClient.controller);
            return l;
          },
          beforeLabel: function (tooltipItems, data) {
            return percChart(data.datasets, tooltipItems.datasetIndex, tooltipItems.index);
          }
        }
      }
    }
  });

  var ctxByClientClone = document.getElementById("chartByClientClone").getContext('2d');
  var chartByClientClone = new Chart(ctxByClientClone, {
    type: 'pie',
    options: {
      plugins: {
        datalabels: {
          color: 'white',
          display: function (context) {
            return context.dataset.data[context.dataIndex] >= 1;
          },
          font: {
            weight: 'bold'
          },
          clip: true,
          title: false,
          formatter: function (value, context) {
            var lb = formatterHourGraphLabel(value, context) + '\n' + percChart(context.dataset, context.datasetIndex, context.dataIndex);
            return lb;
          }
        }
      },
      tooltips: {
        custom: function (tooltip) {
          //tooltip.x = 0;
          //tooltip.y = 0;
        },
        mode: 'single',
        callbacks: {
          label: function (tooltipItems, data) {
            var l = formatterHourGraphLabel(data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index], chartByClient.controller);
            return l;
          },
          beforeLabel: function (tooltipItems, data) {
            return percChart(data.datasets, tooltipItems.datasetIndex, tooltipItems.index);
          }
        }
      }
    }
  });

  var ctxByDayClone = document.getElementById("chartByDayClone").getContext('2d');
  var chartByDayClone = new Chart(ctxByDayClone, {
    type: 'bar',
    options: {
      plugins: {
        datalabels: {
          color: 'black',
          display: function (context) {
            return context.dataset.data[context.dataIndex] >= 1;
          },
          font: {
            weight: 'bold'
          },
          clip: true,
          title: false,
          formatter: function (value, context) {
            var lb = formatterHourGraphLabel(value, context) + '\n' + percChart(context.dataset, context.datasetIndex, context.dataIndex);
            return lb;
          }
        }
      },
      tooltips: {
        custom: function (tooltip) {
          //tooltip.x = 0;
          //tooltip.y = 0;
        },
        mode: 'single',
        callbacks: {
          label: function (tooltipItems, data) {
            var l = formatterHourGraphLabel(data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index], chartByClient.controller);
            return l;
          },
          beforeLabel: function (tooltipItems, data) {
            return percChart(data.datasets, tooltipItems.datasetIndex, tooltipItems.index);
          }
        }
      }
    }
  });

  var ctxByActionAndClientClone = document.getElementById("chartByActionAndClientClone").getContext('2d');
  var chartByActionAndClientClone = new Chart(ctxByActionAndClientClone, {
    type: 'horizontalBar',
    options: {
      scales: {
        xAxes: [{
          stacked: true
        }],
        yAxes: [{
          stacked: true
        }]
      },
      plugins: {
        datalabels: {
          color: 'black',
          display: function (context) {
            return context.dataset.data[context.dataIndex] >= 1;
          },
          font: {
            weight: 'bold'
          },
          clip: true,
          title: false,
          formatter: function (value, context) {
            var lb = formatterHourGraphLabel(value, context) + '\n' + percChart(context.dataset, context.datasetIndex, context.dataIndex);
            return lb;
          }
        }
      },
      tooltips: {
        custom: function (tooltip) {
          //tooltip.x = 0;
          //tooltip.y = 0;
        },
        mode: 'single',
        callbacks: {
          label: function (tooltipItems, data) {
            var l = formatterHourGraphLabel(data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index], chartByClient.controller);
            return l;
          },
          beforeLabel: function (tooltipItems, data) {
            return percChart(data.datasets, tooltipItems.datasetIndex, tooltipItems.index);
          }
        }
      }
    }
  });

  var ctxByActionClone = document.getElementById("chartByActionClone").getContext('2d');
  var chartByActionClone = new Chart(ctxByActionClone, {
    type: 'pie',
    options: {
      plugins: {
        datalabels: {
          color: 'white',
          display: function (context) {
            return context.dataset.data[context.dataIndex] >= 1;
          },
          font: {
            weight: 'bold'
          },
          clip: true,
          title: false,
          formatter: function (value, context) {
            var lb = formatterHourGraphLabel(value, context) + '\n' + percChart(context.dataset, context.datasetIndex, context.dataIndex);
            return lb;
          }
        }
      },
      tooltips: {
        custom: function (tooltip) {
          //tooltip.x = 0;
          //tooltip.y = 0;
        },
        mode: 'single',
        callbacks: {
          label: function (tooltipItems, data) {
            var l = formatterHourGraphLabel(data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index], chartByClient.controller);
            return l;
          },
          beforeLabel: function (tooltipItems, data) {
            return percChart(data.datasets, tooltipItems.datasetIndex, tooltipItems.index);
          }
        }
      }
    }
  });

  var ctxByActionAndUserClone = document.getElementById("chartByActionAndUserClone").getContext('2d');
  var chartByActionAndUserClone = new Chart(ctxByActionAndUserClone, {
    type: 'radar',
    options: {
      plugins: {
        datalabels: {
          display: function (context) {
            return false;
          },
          formatter: function (value, context) {
            return formatterHourGraphLabel(value, context)
          }
        }
      },
      tooltips: {
        custom: function (tooltip) {
          //tooltip.x = 0;
          //tooltip.y = 0;
        },
        mode: 'single',
        callbacks: {
          label: function (tooltipItems, data) {
            var l = formatterHourGraphLabel(data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index], chartByClient.controller);
            return l;
          },
          beforeLabel: function (tooltipItems, data) {
            return percChart(data.datasets, tooltipItems.datasetIndex, tooltipItems.index);
          }
        }
      }
    }
  });

  var ctxByEquipmentClone = document.getElementById("chartByEquipmentClone").getContext('2d');
  var chartByEquipmentClone = new Chart(ctxByEquipmentClone, {
    type: 'polarArea',
    options: {
      plugins: {
        datalabels: {
          color: 'black',
          display: function (context) {
            return context.dataset.data[context.dataIndex] >= 1;
          },
          font: {
            weight: 'bold'
          },
          clip: true,
          title: false,
          formatter: function (value, context) {
            var lb = formatterHourGraphLabel(value, context) + '\n' + percChart(context.dataset, context.datasetIndex, context.dataIndex);
            return lb;
          }
        }
      },
      tooltips: {
        custom: function (tooltip) {
          //tooltip.x = 0;
          //tooltip.y = 0;
        },
        mode: 'single',
        callbacks: {
          label: function (tooltipItems, data) {
            var l = formatterHourGraphLabel(data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index], chartByClient.controller);
            return l;
          },
          beforeLabel: function (tooltipItems, data) {
            return percChart(data.datasets, tooltipItems.datasetIndex, tooltipItems.index);
          }
        }
      }
    }
  });

  loadUsers()
  loadActions()
  loadClients()
  loadEquipments()

  setTimeout(function () {
    load()
  }, 500);

  function load() {
	$('#modal_carregando').modal('show');
	
			
	var params = {};
    params["startDate"] = getDataInicioFiltroOS();
    params["endDate"] = getDataFinalFiltroOS();
    params["user"] = JSON.parse(localStorage.getItem("bobby"))

    $http({
    	method: 'PUT',
    	url: config.baseUrl + "/servicoWeb",
    	data: params
    }).then(function(response) {
    	$scope.listServices = response.data;
    	filteredList()
    	generateChartByClient()
    	generateChartByDay()
    	generateChartByAction()
    	generateChartByClientAndAction()
    	generateChartByEquipment()
    	generateChartByActionAndUser()
    	canRunReload = true;

		$('#modal_carregando').modal('hide');
    }, function(response) {
    	alert($scope.getMessage('ErrorLoadingServices') + response.message);

		$('#modal_carregando').modal('hide');
    });
  }

  function loadUsers() {
    $http.get(config.baseUrl + "/admin/user")
      .then(function (response) {
        $scope.users = response.data;
        $scope.users.sort((a, b) => a.user.toLowerCase().localeCompare(b.user.toLowerCase()));
        for (user of $scope.users)
          defaultColors[user.name] = user.colorId == null ? hexToRgbA(getRandomColor(), 0.7) : hexToRgbA(user.colorId, 0.7);

      }, function (error) {
        alert($scope.getMessage('ErrorLoadingUsers'));
      });
  }

  function loadActions() {
    $http.get(config.baseUrl + "/admin/action")
      .then(function (response) {
        $scope.actions = response.data;
        $scope.actions.sort((a, b) => a.description.toLowerCase().localeCompare(b.description.toLowerCase()));

        for (action of $scope.actions)
          defaultColors[action.description] = action.colorId == null ? hexToRgbA(getRandomColor(), 0.7) : hexToRgbA(action.colorId, 0.7);

      }, function (response) {
        alert($scope.getMessage('ErrorLoadAction.p'));
      });
  }

  function loadClients() {
    $http.get(config.baseUrl + "/admin/client")
      .then(function (response) {
        $scope.clients = response.data;
        $scope.clients.sort((a, b) => a.tradingName.toLowerCase().localeCompare(b.tradingName.toLowerCase()));

        for (client of $scope.clients)
          defaultColors[client.tradingName] = client.colorId == null ? hexToRgbA(getRandomColor(), 0.7) : hexToRgbA(client.colorId, 0.7);

      }, function (response) {
        alert($scope.getMessage('ErrorLoadingClients'));
      });
  }

  function loadEquipments() {
    $http.get(config.baseUrl + "/admin/equipment")
      .then(function (response) {
        $scope.equipments = response.data;
        $scope.equipments.sort((a, b) => a.description.toLowerCase().localeCompare(b.description.toLowerCase()));

        for (equipment of $scope.equipments)
          defaultColors[equipment.description] = equipment.colorId == null ? hexToRgbA(getRandomColor(), 0.7) : hexToRgbA(equipment.colorId, 0.7);

      }, function (response) {
        alert($scope.getMessage('ErrorLoadingEquipment.p'));
      });
  }

  $scope.reload = function () {
    if (canRunReload) {
      setDataInicioFiltroOS(document.getElementById("dtInicio").value);
      setDataFinalFiltroOS(document.getElementById("dtFinal").value);

      if ($scope.showUsers)
        $scope.showUsersSelect()

      if ($scope.showActions)
        $scope.showActionsSelect()

      if ($scope.showClients)
        $scope.showClientSelect()

      if ($scope.showEquipments)
        $scope.showEquipmentSelect()

      $("#parametersModal").modal('hide')
      load();
    }
  }

  function filteredList() {
    function filter(service) {
      /*$scope.usersSelected.userId > 0 && $scope.usersSelected.userId != service.user.userId*/
      if ($scope.usersSelected.length > 0) {
        var filtered = $scope.usersSelected.filter(function (user) {
          if (user.userId != service.user.userId)
            return false
          else
            return true
        })

        if (filtered.length == 0)
          return false
      }

      /*$scope.actionsSelected.actionId > 0 && $scope.actionsSelected.actionId != service.action.actionId*/
      if ($scope.actionsSelected.length > 0) {
        var filtered = $scope.actionsSelected.filter(function (action) {
          if (action.actionId != service.action.actionId)
            return false
          else
            return true
        })

        if (filtered.length == 0)
          return false
      }

      if ($scope.clientsSelected.length > 0) {
        var filtered = $scope.clientsSelected.filter(function (client) {
          if (client.clientId != service.client.clientId)
            return false
          else
            return true
        })

        if (filtered.length == 0)
          return false
      }

      if ($scope.equipmentsSelected.length > 0) {
        var filtered = $scope.equipmentsSelected.filter(function (equipment) {
          if (equipment.equipmentId != service.vehicle.equipment.equipmentId)
            return false
          else
            return true
        })

        if (filtered.length == 0)
          return false
      }

      return true;
    }

    function filterWithoutAction(service) {
      if ($scope.usersSelected.length > 0) {
        var filtered = $scope.usersSelected.filter(function (user) {
          if (user.userId != service.user.userId)
            return false
          else
            return true
        })

        if (filtered.length == 0)
          return false
      }

      if ($scope.clientsSelected.length > 0) {
        var filtered = $scope.clientsSelected.filter(function (client) {
          if (client.clientId != service.client.clientId)
            return false
          else
            return true
        })

        if (filtered.length == 0)
          return false
      }

      if ($scope.equipmentsSelected.length > 0) {
        var filtered = $scope.equipmentsSelected.filter(function (equipment) {
          if (equipment.equipmentId != service.vehicle.equipment.equipmentId)
            return false
          else
            return true
        })

        if (filtered.length == 0)
          return false
      }

      return true;
    }

    if ($scope.listServices != null) {
      $scope.usersNames = ""
      $scope.actionsNames = ""
      $scope.clientsNames = ""
      $scope.equipmentsNames = ""
      fillLabelsParameters()
      filtredServiceList = $scope.listServices.filter(filter);
      filtredServiceActionList = $scope.listServices.filter(filterWithoutAction);
    }

  }

  function fillLabelsParameters() {
    if ($scope.usersSelected.length > 0) {
      for (const i in $scope.usersSelected) {
        var object = $scope.usersSelected[i]
        if (!$scope.usersNames.includes(object.user)) {
          $scope.usersNames += $scope.usersNames.length == 0 ? object.name : ", " + object.name
        }
      }
    } else {
      $scope.usersNames = ""
    }

    if ($scope.actionsSelected.length > 0) {
      for (const i in $scope.actionsSelected) {
        var object = $scope.actionsSelected[i]
        if (!$scope.actionsNames.includes(object.description)) {
          $scope.actionsNames += $scope.actionsNames.length == 0 ? object.description : ", " + object.description
        }
      }
    } else {
      $scope.actionsNames = ""
    }

    if ($scope.clientsSelected.length > 0) {
      for (const i in $scope.clientsSelected) {
        var object = $scope.clientsSelected[i]
        if (!$scope.clientsNames.includes(object.tradingName)) {
          $scope.clientsNames += $scope.clientsNames.length == 0 ? object.tradingName : ", " + object.tradingName
        }
      }
    } else {
      $scope.clientsNames = ""
    }

    if ($scope.equipmentsSelected.length > 0) {
      for (const i in $scope.equipmentsSelected) {
        var object = $scope.equipmentsSelected[i]
        if (!$scope.equipmentsNames.includes(object.description)) {
          $scope.equipmentsNames += $scope.equipmentsNames.length == 0 ? object.description : ", " + object.description
        }
      }
    } else {
      $scope.equipmentsNames = ""
    }
  }

  function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';

    for (var i = 0; i < 6; i++)
      color += letters[Math.floor(Math.random() * 16)];

    return color;
  }

  function changeToHour(obj) {
    for (let index in obj) {
      var hours = obj[index]
      hours = ((hours / (1000 * 60 * 60)) % 24)
      hours = hours.toFixed(2)
      hours = parseFloat(hours)
      obj[index] = hours
    }
  }

  function generateChartByClient() {
    var service, values, keys
    var colors = []
    var totals = {}

    for (let index in filtredServiceList) {
      service = filtredServiceList[index]

      if ($scope.quantity)
        totals[service.client.tradingName] = totals[service.client.tradingName] == null ? 1 : totals[service.client.tradingName] + 1
      else
        totals[service.client.tradingName] = totals[service.client.tradingName] == null ? service.timeElapsedMilli : totals[service.client.tradingName] + service.timeElapsedMilli
    }

    if (!$scope.quantity)
      changeToHour(totals)

    values = Object.values(totals);
    keys = Object.keys(totals);

    for (key of keys)
      colors.push(defaultColors[key])

    removeData(chartByClient)
    datalabelsConf = { align: 'start', anchor: 'end' };

    var data = {
      labels: keys,
      datasets: [{
        data: values,
        backgroundColor: colors,
        borderWidth: 1,
        datalabels: datalabelsConf
      }]
    }

    chartByClient.options.legend.display = false
    chartByClient["labelFormatter"] = $scope.quantity ? '' : 'HOUR';
    addData(chartByClient, keys, data)

    chartByClientClone.data.labels = [];
    chartByClientClone.data.labels = chartByClient.data.labels
    chartByClientClone.data.datasets = [];
    chartByClientClone.data = chartByClient.data
    chartByClientClone["labelFormatter"] = $scope.quantity ? '' : 'HOUR';
    chartByClientClone.update()
  }

  function generateChartByDay() {
    var service, values, keys
    var colors = []
    var totals = {}

    for (let index in filtredServiceList) {
      service = filtredServiceList[index]
      var date = service.startTime.substr(0, 5)

      if ($scope.quantity)
        totals[date] = totals[date] == null ? 1 : totals[date] + 1
      else
        totals[date] = totals[date] == null ? service.timeElapsedMilli : totals[date] + service.timeElapsedMilli
    }

    if (!$scope.quantity)
      changeToHour(totals)

    values = Object.values(totals);
    keys = Object.keys(totals);

    for (key of keys)
      colors.push(hexToRgbA(getRandomColor(), 0.7))

    removeData(chartByDay)

    datalabelsConf = { align: 'end', anchor: 'start' };
    var data = {
      labels: keys,
      datasets: [{
        data: values,
        backgroundColor: colors,
        borderWidth: 1,
        datalabels: datalabelsConf
      }]
    }
    chartByDay.options.legend.display = false
    chartByDay.options.scales.yAxes[0].ticks.beginAtZero = true;
    chartByDay["labelFormatter"] = $scope.quantity ? '' : 'HOUR';
    addData(chartByDay, keys, data)

    chartByDayClone.data.labels = []
    chartByDayClone.data.labels = chartByDay.data.labels
    chartByDayClone.data.datasets = []
    chartByDayClone.data = chartByDay.data
    chartByDayClone.options.legend.display = false
    chartByDayClone.options.scales.yAxes[0].ticks.beginAtZero = true;
    chartByDayClone["labelFormatter"] = $scope.quantity ? '' : 'HOUR';
    chartByDayClone.update()
  }

  function generateChartByAction() {
    var service, values, keys
    var colors = []
    var totals = {}

    for (let index in filtredServiceActionList) {
      service = filtredServiceActionList[index]

      if ($scope.quantity)
        totals[service.action.description] = totals[service.action.description] == null ? 1 : totals[service.action.description] + 1
      else
        totals[service.action.description] = totals[service.action.description] == null ? service.timeElapsedMilli : totals[service.action.description] + service.timeElapsedMilli
    }

    if (!$scope.quantity)
      changeToHour(totals)

    values = Object.values(totals);
    keys = Object.keys(totals);

    for (key of keys)
      colors.push(defaultColors[key])

    removeData(chartByAction)

    datalabelsConf = { align: 'start', anchor: 'end' };
    var data = {
      labels: keys,
      datasets: [{
        data: values,
        backgroundColor: colors,
        borderWidth: 1,
        datalabels: datalabelsConf
      }]
    }
    chartByAction.options.legend.display = false
    chartByAction["labelFormatter"] = $scope.quantity ? '' : 'HOUR';
    addData(chartByAction, keys, data)

    chartByActionClone.data.labels = []
    chartByActionClone.data.labels = chartByAction.data.labels
    chartByActionClone.data.datasets = []
    chartByActionClone.data = chartByAction.data
    chartByActionClone["labelFormatter"] = $scope.quantity ? '' : 'HOUR';
    chartByActionClone.update()
  }

  function generateChartByClientAndAction() {
    var service, keys
    var clients = {}
    var actionList = {}

    actions = [];
    for (let index in filtredServiceList) {
      service = filtredServiceList[index]
      if (!actions.includes(service.action.description)) actions.push(service.action.description);
    }

    for (let index in filtredServiceList) {
      service = filtredServiceList[index]
      if (!Object.keys(clients).indexOf(service.client.tradingName) > -1) {
        actionList = {}
        for (let index = 0; index < actions.length; index++) {
          act = actions[index]
          actionList[act] = 0
        }
        clients[service.client.tradingName] = actionList
      }
    }

    for (let index in filtredServiceList) {
      service = filtredServiceList[index]
      if (Object.keys(clients).indexOf(service.client.tradingName) > -1) {
        var act = clients[service.client.tradingName]
        if (Object.keys(act).indexOf(service.action.description) > -1) {
          if ($scope.quantity)
            act[service.action.description] = act[service.action.description] + 1
          else
            act[service.action.description] = act[service.action.description] + service.timeElapsedMilli
        }
      }
    }

    keys = Object.keys(clients);

    var datasets = [];
    datalabelsConf = { align: 'end', anchor: 'start' };
    for (const idx in clients) {
      client = clients[idx];

      if (!$scope.quantity)
        changeToHour(client)

      actions = Object.keys(client);
      for (const i in actions) {
        action = actions[i];

        dataset = getDatasetByLabel(datasets, action);
        if (dataset == null) {
          dataset = { data: [], backgroundColor: defaultColors[action], label: action, datalabels: datalabelsConf };
          datasets.push(dataset);
        }

        dataset.data.push(client[action]);
      }
    }

    removeData(chartByActionAndClient)
    var data = {
      labels: keys,
      datasets: datasets
    }

    chartByActionAndClient.options.legend.display = false
    chartByActionAndClient["labelFormatter"] = $scope.quantity ? '' : 'HOUR';
    addData(chartByActionAndClient, keys, data)

    chartByActionAndClientClone.data.labels = []
    chartByActionAndClientClone.data.labels = chartByActionAndClient.data.labels
    chartByActionAndClientClone.data.datasets = []
    chartByActionAndClientClone.data = chartByActionAndClient.data
    chartByActionAndClientClone["labelFormatter"] = $scope.quantity ? '' : 'HOUR';
    chartByActionAndClientClone.update()
  }

  function generateChartByEquipment() {
    var service, values, keys
    var colors = []
    var totals = {}

    for (let index in filtredServiceList) {
      service = filtredServiceList[index]
      if (service.vehicle !== null && service.vehicle.equipment !== null) {
        if ($scope.quantity)
          totals[service.vehicle.equipment.description] = totals[service.vehicle.equipment.description] == null ? 1 : totals[service.vehicle.equipment.description] + 1
        else
          totals[service.vehicle.equipment.description] = totals[service.vehicle.equipment.description] == null ? service.timeElapsedMilli : totals[service.vehicle.equipment.description] + service.timeElapsedMilli
      }
    }

    if (!$scope.quantity)
      changeToHour(totals)

    values = Object.values(totals);
    keys = Object.keys(totals);

    for (key of keys)
      colors.push(defaultColors[key])

    removeData(chartByEquipment)

    datalabelsConf = { align: 'start', anchor: 'end' };
    var data = {
      labels: keys,
      datasets: [{
        data: values,
        backgroundColor: colors,
        borderWidth: 1,
        datalabels: datalabelsConf
      }]
    }
    chartByEquipment.options.legend.display = false
    chartByEquipment["labelFormatter"] = $scope.quantity ? '' : 'HOUR';
    addData(chartByEquipment, keys, data)

    chartByEquipmentClone.data.labels = []
    chartByEquipmentClone.data.labels = chartByEquipment.data.labels
    chartByEquipmentClone.data.datasets = []
    chartByEquipmentClone.data = chartByEquipment.data
    chartByEquipmentClone["labelFormatter"] = $scope.quantity ? '' : 'HOUR';
    chartByEquipmentClone.update()
  }

  function generateChartByActionAndUser() {
    var service, keys
    var users = {}
    var actionList = {}

    actions = [];
    for (let index in filtredServiceList) {
      service = filtredServiceList[index]
      if (!actions.includes(service.action.description)) actions.push(service.action.description);
    }

    for (let index in filtredServiceList) {
      service = filtredServiceList[index]
      if (!Object.keys(users).indexOf(service.user.name) > -1) {
        actionList = {}
        for (let index = 0; index < actions.length; index++) {
          act = actions[index]
          actionList[act] = 0
        }
        users[service.user.name] = actionList
      }
    }

    for (let index in filtredServiceList) {
      service = filtredServiceList[index]
      if (Object.keys(users).indexOf(service.user.name) > -1) {
        var act = users[service.user.name]
        if (Object.keys(act).indexOf(service.action.description) > -1) {
          if ($scope.quantity)
            act[service.action.description] = act[service.action.description] + 1
          else
            act[service.action.description] = act[service.action.description] + service.timeElapsedMilli

        }
      }
    }

    keys = Object.keys(users);

    var datasets = [];
    for (const idx in users) {
      user = users[idx];

      if (!$scope.quantity)
        changeToHour(user)

      actions = Object.keys(user);
      for (const i in actions) {
        action = actions[i];

        dataset = getDatasetByLabel(datasets, action);
        if (dataset == null) {

          backColor = defaultColors[action].replace(0.7, 0.5);
          colorBorder = defaultColors[action].replace(0.7, 1);

          dataset = { data: [], backgroundColor: backColor, borderColor: colorBorder, pointBackgroundColor: colorBorder, label: action };
          datasets.push(dataset);
        }

        dataset.data.push(user[action]);
      }
    }

    removeData(chartByActionAndUser)

    var data = {
      labels: keys,
      datasets: datasets
    }

    chartByActionAndUser.options.legend.display = false
    chartByActionAndUser.options.elements.line.tension = 0.4
    chartByActionAndUser["labelFormatter"] = $scope.quantity ? '' : 'HOUR';
    addData(chartByActionAndUser, keys, data)

    chartByActionAndUserClone.data.labels = []
    chartByActionAndUserClone.data.labels = chartByActionAndUser.data.labels
    chartByActionAndUserClone.data.datasets = []
    chartByActionAndUserClone.data = chartByActionAndUser.data
    chartByActionAndUserClone.options.elements.line.tension = 0.4
    chartByActionAndUserClone["labelFormatter"] = $scope.quantity ? '' : 'HOUR';
    chartByActionAndUserClone.update()
  }

  function getDatasetByLabel(datasets, label) {
    var dataset = null;

    for (const i in datasets) {
      ds = datasets[i];

      if (ds.label == label) {
        dataset = ds;
        break;
      }
    }

    return dataset;
  }

  function addData(chart, label, data) {
    chart.data.labels.push(label)
    chart.data = data
    chart.update();
  }

  function removeData(chart) {
    chart.data.labels.pop()
    chart.data.datasets.pop()
    chart.update();
  }

  function getRBGRandom() {
    var number = Math.floor((Math.random() * 255) + 1)
    for (var i = 0; i < 2; i++) {
      number += ", " + Math.floor((Math.random() * 255) + 1)
    }

    return number
  }

  $scope.showModalWindow = function () {
    $("#parametersModal").modal({
      backdrop: 'static',
      keyboard: false
    }).unbind('click')
  }

  $scope.showModalChartByClient = function () {
    $("#chartByClientModal").modal()
  }

  $scope.showModalChartByDay = function () {
    $("#chartByDayModal").modal()
  }

  $scope.showModalChartByActionAndClient = function () {
    $("#chartByActionAndClientModal").modal()
  }

  $scope.showModalChartByAction = function () {
    $("#chartByActionModal").modal()
  }

  $scope.showModalChartByActionAndUser = function () {
    $("#chartByActionAndUserModal").modal()
  }

  $scope.showModalChartByEquipment = function () {
    $("#chartByEquipmentModal").modal()
  }

  $scope.printChart = function (charId) {
    var img = document.createElement('img');
    img.src = document.getElementById(charId).toDataURL();

    var div = document.getElementById('printChart');
    div.innerHTML = '';
    div.appendChild(img);

    setTimeout(function () {
      var e = document.getElementById('btnPrinterChart');
      e.click();
    }, 500)
  }

  $scope.downloadChart = function (charId) {
    var dwn = document.getElementById('dwnPrinterChart');
    dwn.href = document.getElementById(charId).toDataURL();
    dwn.click();
  }

  $scope.showUsersSelect = function () {
    $scope.showUsers = $scope.showUsers ? false : true

    if ($scope.showUsers)
      $("#userTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up')
    else
      $("#userTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down')
  }

  $scope.showActionsSelect = function () {
    $scope.showActions = $scope.showActions ? false : true

    if ($scope.showActions)
      $("#actionTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up')
    else
      $("#actionTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down')
  }

  $scope.showClientSelect = function () {
    $scope.showClients = $scope.showClients ? false : true

    if ($scope.showClients)
      $("#clientTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up')
    else
      $("#clientTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down')
  }

  $scope.showEquipmentSelect = function () {
    $scope.showEquipments = $scope.showEquipments ? false : true

    if ($scope.showEquipments)
      $("#equipmentTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up')
    else
      $("#equipmentTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down')
  }

  $scope.selectQuantity = function () {
    $scope.quantity = true
    $scope.hour = false
  }

  $scope.selectHour = function () {
    $scope.quantity = false
    $scope.hour = true
  }

  $("#chartClient").css({ "box-shadow": "1px 1px 3px 0px", "background-color": "white" });
  $("#chartDay").css({ "box-shadow": "1px 1px 3px 0px", "background-color": "white" });
  $("#chartAction").css({ "box-shadow": "1px 1px 3px 0px", "background-color": "white" });
  $("#chartActionAndClient").css({ "box-shadow": "1px 1px 3px 0px", "background-color": "white" });
  $("#chartActionAndUser").css({ "box-shadow": "1px 1px 3px 0px", "background-color": "white" });
  $("#chartEquipment").css({ "box-shadow": "1px 1px 3px 0px", "background-color": "white" });
});
