appGIMB.controller("servicesController", function ($scope, $rootScope, $http, geralAPI, $location, juiceService, $routeParams, config, $sce) {

	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');
	if (!controllerHeadCheck($scope, $location, juiceService, false)) return;

	$scope.pathImg = config.pathImg;
	$('.ipt').attr('disabled', 'disabled');
	$('#divMenu').show();

	changeScreenHeightSize("#divTableServices");
	$scope.checkedUncheckedAll = false;
	$scope.indexImage = 0;
	$scope.servico = {};
	$scope.selectedImage = {};
	$scope.servicos = [];
	$scope.servicosSelected = [];
	$scope.servicosToPrint = [];
	$scope.servicosCount = 0;
	$scope.title = "Serviços";
	$scope.listaImagensPrint = [];
	$scope.listaImagensPrintReport = [];
	$scope.itemEmpty = {};
	$scope.optBy = "all";
	$scope.customFieldList = [];
	$scope.thumbs = [];
	$scope.videos = [];
	$scope.listaImagensCheck = [];
	$scope.array = [];
	$scope.srcImageOK = 'apiGIMB/view/imagesCheckList/ok.png';
	$scope.srcImageNOK = 'apiGIMB/view/imagesCheckList/nok.png';
	$scope.signatureService = '';
	$scope.checklistServices = [];
	$scope.checklistServicesWithCritically = [];
	$scope.checklistServicesWithCriticallyTotal = {};
	
	$scope.clientsNames = "";
	$scope.activitysNames = "";
	$scope.userNames = "";
	$scope.projectNames = "";
	
	$scope.clients = [];
	$scope.activitys = [];
	$scope.users = [];
	$scope.projects = [];
	
	$scope.clientsSelected = getClientsOS();
	$scope.activitysSelected = getActivitysOS();
	$scope.usersSelected = getUsersOS();
	$scope.projectSelected = getProjectOS();
	
	$scope.showClient = false;
	$scope.showActivitys = false;
	$scope.showUsers = false;
	$scope.showProject = false;
	$scope.statusOs = '';

	var localUser = JSON.parse(localStorage.getItem("bobby"));
	var configWeb = (localUser.profile != null && localUser.profile.configWeb != null && localUser.profile.configWeb.length > 0) ? JSON.parse(localUser.profile.configWeb) : [];

	var INSERTPICSERVICE = (configWeb.INSERTPICSERVICE == '0');
	var DELETEOS = (configWeb.DELETEOS == '1');
	
	if (document.getElementById('dtInicio'))
		document.getElementById('dtInicio').readOnly = true;
		
	if (document.getElementById('dtFinal'))
		document.getElementById('dtFinal').readOnly = true;

	$scope.toggle = function (imagem) {
		let imageParam = imagem;

		imageParam['chkPrint'] = imageParam['chkPrint'] ? false : true;

		$http.put(config.baseUrl + '/updatePictureService/' + imageParam.picId, imageParam).then(
			success => {
				return true;
			},
			error => {
				console.error(error);
			}
		);
		makeListaImagensPrintReport();
	};

	$scope.isToPrint = function (imagem) {
		const item = $scope.listaImagensPrint.find(img => img.picId === imagem.picId);

		return item.chkPrint;
	};

	var canRunReload = false;
	
	var orgSrcImg;	
	var uploadNewImageSrc;

	var today = new Date();
	var dt = new Date();

	var dtIni = getDataInicioFiltroOS();
	var dtFin = getDataFinalFiltroOS();


	$scope.dtInicio = dtIni.length == 0 ? dateToStringPeriodo(today, "i") : dtIni;
	$scope.dtFinal = dtFin.length == 0 ? dateToStringPeriodo(today, "f") : dtFin;

	setDataInicioFiltroOS($scope.dtInicio);
	setDataFinalFiltroOS($scope.dtFinal);

	loadClientList();
	loadActionList();
	loadUserList();

	if (!$routeParams.serviceId) {
		load();
	} else {					
		$rootScope.scrollTopServiceId = $routeParams.serviceId;
		loadService($routeParams.serviceId, true).then();			
	}

	function load() {
		$('#modal_carregando').modal('show');
	
		var params = {};
		params["startDate"] = $scope.dtInicio;
		params["endDate"] = $scope.dtFinal;
		params["user"] = localUser;
		
		setClientsOS($scope.clientsSelected);
		setActivitysOS($scope.activitysSelected);
		setUsersOS($scope.usersSelected);
		setProjectOS($scope.projectSelected);
		setDataInicioFiltroOS(params["startDate"]);
		setDataFinalFiltroOS(params["endDate"]);
		
		$scope.allServices = $rootScope.allServices;
		
		if ($scope.allServices == null || $scope.allServices.length == 0) {
			$http({
				method: 'PUT',
				url: config.baseUrl + "/servicoWeb",
				data: params
			}).then(function (response) {
				console.log('Serviços: ', response.data);
				$scope.allServices = response.data;
				$rootScope.allServices = $scope.allServices; 
				processData();
			}, function (response) {
				alert($scope.getMessage('ErrorLoadData'));
	
				$('#modal_carregando').modal('hide');
			});
		} else {
			console.log('proccessData');
			processData();
			setTimeout(() => {
				var container = document.getElementById('tbServices');
				var row = document.getElementById($rootScope.scrollTopServiceId);
				
				if (container && row) {
					$('#tbServices').animate({
						scrollTop: row.offsetTop
					}, 800, null);
					
					var rowDivName = '#'+$rootScope.scrollTopServiceId;
					$(rowDivName).css({ transform: 'scale(1.05, 1.05)', backgroundColor: '#d3d3d3', 'fontWeight': 'bold' });
					setTimeout(() => {
						$(rowDivName).css({ transform: 'scale(1)', backgroundColor: '', 'fontWeight': '' });
						$rootScope.scrollTopServiceId = null;
					}, 800);
				}
			}, 500);
		}
	}
	
	function processData() {
		filterServicesBy()
		canRunReload = true;
		
		loadDatePicker();
		
		fillLists();
		listFilter();

		$('#modal_carregando').modal('hide');
	}

	async function loadService(idServico, showModal) {
		if (showModal) {
			$('#modal_carregando').modal('show');
		}
		idServiceForBack = idServico;

		$scope.config_web = (localUser.profile != null && localUser.profile.configWeb != null && localUser.profile.configWeb.length > 0) ? JSON.parse(localUser.profile.configWeb) : [];

		console.log('url: ', config.baseUrl + "/servico/" + idServico);
		var response = await $http.get(config.baseUrl + "/servico/" + idServico);
		$scope.servico = response.data;
		processRetornoServico($scope.servico);
		var serv = $scope.servico;
		$scope.servicosToPrint.push(serv);
		console.log('loadService: ', $scope.servicosToPrint);

		if ($scope.servico.action.usaSistemaSubSistema != null && $scope.servico.action.usaSistemaSubSistema) {
			var picsList = $scope.servicosToPrint[0].picsList;

			var ids = [];
			for (let i = 0; i < picsList.length; i++) {
				var checklistItems = picsList[i].checklistItems;
				
				if (checklistItems != null) {				
					ids.push(checklistItems.checklistServicesId);
				}			
			}

			var novaArr = ids.filter((este, i) => ids.indexOf(este) === i);
			console.log(novaArr);

			if (novaArr.length > 0) {
				for (let x = 0; x < novaArr.length; x++) {
					await $http.get(config.baseUrl + "/admin/checklistService/" + novaArr[x]).then(
						function (res) {
							var checklistService = res.data;				
							$scope.checklistServices.push(checklistService);							
							if (showModal && x >= novaArr.length) {
								setTimeout(() => $('#modal_carregando').modal('hide'), 1200);
							}
						}
					);
					
				}				
			}

			await loadChecklistWithCritically($routeParams.serviceId);
			await loadChecklistWithCriticallyTotal($routeParams.serviceId);
		}		

		if (showModal) {
			setTimeout(() => $('#modal_carregando').modal('hide'), 1200);
		}
	}

	async function loadChecklistWithCritically(id) {
		await $http.get(config.baseUrl + "/admin/checklistServiceCritically/" + id).then(
			function (res) {
				var checklistService = res.data;
				console.log('DEBUG: ', checklistService);				
				$scope.checklistServicesWithCritically = checklistService;
			}
		);
	}

	async function loadChecklistWithCriticallyTotal(id) {
		await $http.get(config.baseUrl + "/admin/checklistServiceCriticallyTotal/" + id).then(
			function (res) {
				var checklistService = res.data;
				console.log('DEBUG loadChecklistWithCriticallyTotal: ', checklistService);				
				$scope.checklistServicesWithCriticallyTotal = checklistService;
			}
		);
	}

	$scope.listCustomFields = function (servico) {
		let listFields = '';
		let listCustomFields = servico.customFieldList;
		listCustomFields.map((field) => {
			let value = field.customFieldValue;
			if (value.length > 20) {
				value = value.substring(0, 19) + '...';
			}
			listFields += `<span style="font-size: 10px !important"><span style="font-size: 9px !important; font-family: gilroyBold">${field.customField}:</span> ${value}</span><br/>`;
		});

		$scope.trustedHtml = $sce.trustAsHtml(listFields);
	}
	
	$scope.functionCritically = function() {
		var show = false;
		
		for (var doc of $scope.documents)
			if(doc.optionDownloadSelect) show = true;
		
		return show; 
	}
	
	function processRetornoServico(servico) {
		$scope.listaImagensPrintReport = [];
		$scope.listaImagensPrint = [];

		getSignatureService(servico.serviceId);
		
		for (custom of servico.customFieldList) {
			if (custom.customFieldValue.indexOf("[{") != -1) {
				var arr = custom.customFieldValue;
				$scope.array = JSON.parse(arr);
			}
		}
		
		if (servico.status == 'P' || servico.status == 'R') {
			servico.client = $scope.clientList.find(c => c.clientId == servico.client.clientId);
			$scope.equipmentList = servico.client.vehicles;
			$scope.clientSelected = servico.client == null ? '' : servico.client.clientId.toString();
			$scope.actionSelected = servico.action == null ? '' : servico.action.actionId.toString();
			$scope.equipmentSelected = servico.vehicle == null ? '' : servico.vehicle.vehicleId.toString();
			$scope.userSelected = servico.user == null ? '' : servico.user.userId.toString();
		
			$scope.changeAction();
			for (customField of servico.customFieldList) {
				for (cf of $scope.customFieldList) {
					if (customField.customField == cf.VALUE) {
						cf.DATA = customField.customFieldValue;
						break;
					}
				}
			}
		} else {
		
			imputImages();
		}
		
		{
			$scope.company = JSON.parse(localStorage.getItem("company"));
			$scope.reportNumber = servico.serviceId;
			$scope.reportDate = servico.startTime.substr(0, 10);
		}
		
		var count = 0;
		
		if ($scope.itemEmpty) {
			$scope.itemEmpty.latPic = 0;
			$scope.itemEmpty.lgtPic = 0;
		
			$scope.itemEmpty.message = '';
			$scope.itemEmpty.pic = null;
			$scope.itemEmpty.picId = 0;
		
			$scope.itemEmpty.picMime = 'jpg';
			$scope.itemEmpty.picName = '';
			$scope.itemEmpty.picPath = '/imagens/default/bkg_white.jpg';
			$scope.itemEmpty.servico = null;
		}
		
		$(servico.picsList).each(function (idx, el) {
			if ((servico.action.checklist == null || servico.action.checklist == false) && count == 20) {
				count = 0;
			} 
			$scope.listaImagensPrint.push(el);
			count++;
		});
		
		makeListaImagensPrintReport();
		servico.userLogin = localUser;
		
		preencheListaDeVideos();
		
		$scope.statusOs = buildStatus(servico.status);
		
		servico.company = $scope.company;
		servico.reportNumber = $scope.reportNumber;
		servico.reportDate = $scope.reportDate;
		servico.statusOs = $scope.statusOs;
		servico.listaImagensPrintReport = $scope.listaImagensPrintReport;
		servico.listaImagensPrint = $scope.listaImagensPrint;
	}
	//passar campo na url como parametro pra fazer o click do imprimir
	function preencheListaDeVideos() {
		for (i = 0; i < $scope.listaImagensPrint.length; i++) {
			if ($scope.listaImagensPrint[i].picPath != null && $scope.servico.action.checklist) {
				if ($scope.listaImagensPrint[i].picMime == 'MP4' || $scope.listaImagensPrint[i].picMime == 'mp4' || $scope.listaImagensPrint[i].picMime == 'gif' || $scope.listaImagensPrint[i].picMime == 'GIF')
					$scope.videos.push($scope.listaImagensPrint[i]);
				else {
					$scope.listaImagensCheck.push($scope.listaImagensPrint[i]);
					if ($scope.listaImagensPrint[i].fixPicPath != null && $scope.listaImagensPrint[i].fixPicPath.length > 0) {
						var obj = {}
						obj.picPath = $scope.listaImagensPrint[i].fixPicPath;
						obj.picMime = $scope.listaImagensPrint[i].picMime;
						obj.picName = $scope.listaImagensPrint[i].fixPicName;
						obj.picId = $scope.listaImagensPrint[i].picId;

						$scope.listaImagensCheck.push(obj);
					}
				}
			}
		}
	}

	$scope.moveImageToServicePrev_ = function () {
		$scope.indexImage--;

		if ($scope.indexImage === $scope.listaImagensPrint.length) $scope.indexImage = 0;

		if ($scope.indexImage < 0) $scope.indexImage = $scope.listaImagensPrint.length - 1;

		$scope.selectedImage = $scope.listaImagensPrint[$scope.indexImage];

		moveImageToService('prev', $("#imageDetailService"));
	}

	$scope.moveImageToServiceNext_ = function () {
		$scope.indexImage++;

		if ($scope.indexImage === $scope.listaImagensPrint.length) $scope.indexImage = 0;

		$scope.selectedImage = $scope.listaImagensPrint[$scope.indexImage];
		moveImageToService('next', $("#imageDetailService"));
	}

	$scope.rotateServiceMenos = function () {
		rotateImage('-90', $scope, $("#imageDetailService"), $("#divModalDD"), $('#modalContent'));
	}

	$scope.rotateServiceMais = function () {
		rotateImage('90', $scope, $("#imageDetailService"), $("#divModalDD"), $('#modalContent'));
	}

	$scope.downloadImage = function () {
		downloadImg();
	}

	$scope.verificaCarregamentoDelete = function (c) {
		return c.DELETEPICSERVICE == '1';
	}

	$scope.verificaCarregamentoTroca = function (c) {
		var mime;

		if (listService != null && listService.length > 0)
			mime = listService[idxImgService].picMime;

		if (mime == 'mp4' || mime == 'MP4' || mime == 'gif' || mime == 'GIF')
			return false;
		else
			return c.CHANGEPICSERVICE == '1';
	}

	$scope.verificaCarregamentoEditar = function (c) {
		var mime;

		if (listService != null && listService.length > 0)
			mime = listService[idxImgService].picMime;

		if (mime == 'mp4' || mime == 'MP4' || mime == 'gif' || mime == 'GIF')
			return false;
		else
			return c.EDITPICSERVICE == '1';
	}

	$scope.verificaRotacaoServiceMp4 = function () {
		var mime;

		if (listService != null && listService.length > 0)
			mime = listService[idxImgService].picMime;

		if (mime == 'mp4' || mime == 'MP4' || mime == 'gif' || mime == 'GIF') {
			$('#columSlideService').attr('style', 'margin-left: 90px;');
			return false;
		}
		else {
			$('#columSlideService').attr('style', '');
			return true;
		}
	}


	function imputImages() {
		$('#newImg').change(function () {

			if (this.files && this.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					uploadNewImageSrc = '';
					uploadNewImageSrc = e.target.result;
					$('#ImgNew').attr('src', uploadNewImageSrc);
					$('#modalNewImg').modal('show');
				};
				reader.readAsDataURL(this.files[0]);
			}

			var $el = $('#newImg');
			$el.wrap('<form>').closest('form').get(0).reset();
			$el.unwrap();
		});

		$('#imgInputService').change(function () {
			orgSrcImg = document.getElementById('imageDetailService').src;

			readURL(this,
				$('#imageDetailService'),
				document.getElementById('imageDetailService').src,
				$http,
				$scope.listaImagensPrint,
				config,
				$('#modalConfirmUpdateImage'));
		});
	}

	$scope.reload = function () {
		if (canRunReload) {
			setDataInicioFiltroOS(document.getElementById("dtInicio").value);
			setDataFinalFiltroOS(document.getElementById("dtFinal").value);

			load();
		}
	};

	$scope.loadService = function (id) {
		loadService(id, false);
	};

	$scope.setText = function ($event) {
		setTextoFiltroOS(event.target.value);
	};

	$scope.dadosMapa = function (marcadores) {
		dadosMapa(marcadores.picsList, marcadores);
	};

	$scope.veridicarBtn = function () {
		return INSERTPICSERVICE;
	};

	$scope.canDeleteOS = function () {
		return DELETEOS;
	};

	$scope.printService = function (divName) {
		console.log('printService ' + divName);
		var popupWin = window.open('', '_blank', 'width=300,height=300');
		popupWin.document.open();
		popupWin.document.write(divName);
		popupWin.document.close();
	}

	$scope.print = function (servico) {
		console.log('scope.print');
		$http({
			method: 'PUT',
			url: config.baseUrl + "/printPage"
		}).then(function (response) {
			$scope.servicos = response.data;
		}, function (response) {
			alert($scope.getMessage('ErrorLoadData') + response.status + "\n" + response.message);
		});
	};

	$scope.ocultarDivParaCliente = function () {
		return (localUser.role == "C") ? true : false;
	};

	$scope.ocultarDivOcorrencia = function () {
		return (localUser.role == "C" || ($scope.servico.serviceId > 0 && $scope.servico.eventsList.length == 0)) ? true : false;
	};

	$scope.interruptionText = function () {
		var i = 1;
		var str = $scope.servico.startTime;
		for (var idx in $scope.servico.interruptionList) {
			var interruption = $scope.servico.interruptionList[idx];

			str += ' - ' + interruption.startTime + '\n';

			if (interruption.endTime != null)
				str += interruption.endTime;
			i++;
		}

		if ($scope.servico.endTime != null)
			str += ' - ' + $scope.servico.endTime;

		var ta = document.getElementById('taInterruptions');
		ta.style.height = (i * 23) + 'px';

		return str;
	};

	$scope.hideImageDiv = function (imagem) {
		if ((imagem.picPath != null && imagem.picPath.length > 0) || (imagem.fixPicPath != null && imagem.fixPicPath.length > 0)) {
			return true;
		} else {
			return false;
		}
	};

	$scope.hideAllImageDiv = function (servico) {
		var show = false;
		for (var idx in $scope.listaImagensPrint) {
			var imagem = $scope.listaImagensPrint[idx];

			if (!$scope.servico.action.checklist && ((imagem.picPath != null && imagem.picPath.length > 0) || (imagem.fixPicPath != null && imagem.fixPicPath.length > 0))) {
				show = true;
				break;
			}
		}

		return show;
	};

	$scope.hideAllChecklistDiv = function () {
		var show = false;

		if ($scope.listaImagensPrint != null) {
			for (const image of $scope.listaImagensPrint) {
				if ($scope.servico.action != null && $scope.servico.action.checklist && image.message != null && image.message.length > 0) {
					show = true;
					break;
				}
			}
		}

		return show;
	};

	$scope.hideChecklistDiv = function (imagem) {
		if (imagem != null && imagem.statusChecklist != null) {
			return true;
		} else {
			return false;
		}
	};

	$scope.hideImage = function (imagem) {
		return imagem != null && imagem.length > 0 ? true : false;
	};

	$scope.returnOkNok = function (value) {
		if (value == null)
			return "";

		return value ? "OK" : "NOK";
	};

	$scope.returnCriticallyChecklist = function (value) {
		if (value == null)
			return "";

		if (value === "A") {
			return "ALTO";
		} else if (value === "M") {
			return "MEDIO";
		} else if (value === "B") {
			return "BAIXO";
		}
	};

	function classButtonCritically() {
		$scope.classeDinamica = 'btn btn-default';
	}

	$scope.stringToDate = function (date) {
		return stringToDate(date);
	}

	$scope.deletePic = function () {
		$('#modalImgDetailService').modal('hide');

		var div = document.getElementById('div_services_principal');
		carregamento(div);

		$('#modal_carregando').modal('show');

		deleteImageService(document.getElementById('imageDetailService').getAttribute('src'),
			$scope.listaImagensPrint,
			config,
			$http,
			$('#modal_carregando'),
			document.getElementById('videoDetailService').getAttribute('src'),
			$scope.videos,
			$scope.servico.action.checklist
		);
	}

	$scope.addPic = function () {

		$("#modalNewImg").modal('hide');

		var div = document.getElementById('div_services_principal');
		carregamento(div);

		$('#modal_carregando').modal('show');

		callback = function (dataUrl) {
			dataUrl = dataUrl.substr(dataUrl.indexOf('base64,') + 7, dataUrl.length);

			titulo = document.getElementById('title_image').value;
			obs = document.getElementById('obs_image').value;

			serviceImage = {};
			serviceImage.pic = dataUrl;
			serviceImage.latPic = 0;
			serviceImage.lgtPic = 0;
			serviceImage.message = titulo;
			serviceImage.note = obs;
			serviceImage.manually = null;
			serviceImage.webUpload = null;
			serviceImage.statusChecklist = null;
			serviceImage.statusFix = null;
			serviceImage.picMime = 'jpg';
			serviceImage.critically = null;
			console.log('serviceImage: ', serviceImage);

			bodyData = {};
			bodyData.service = $scope.servico;
			bodyData.serviceImage = serviceImage;			

			$http({
				method: 'PUT',
				url: config.baseUrl + "/addServiceImage",
				data: bodyData
			}).then(function (response) {
				console.log('addServiceImage: ', response.data);
				$scope.listaImagensPrint.push(response.data);

				var titulo = document.getElementById('title_image');
				titulo.value = '';

				var obs = document.getElementById('obs_image');
				obs.value = '';

				$('#modal_carregando').modal('hide');

			}, function (response) {
				alert($scope.getMessage('ErrorInsertImg') + response.status + "\n" + response.message);
			});
		};
		toDataURL(uploadNewImageSrc, callback);
	};


	function readURL(input, imageDetail, imageDetail_src, http, listaImagens, config, confirmUpdateImg) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				originalSrc = imageDetail_src;
				newSrc = e.target.result;
				imageDetail.attr('src', newSrc);
				updateImg(originalSrc, newSrc, http, listaImagens, config, confirmUpdateImg);
			};

			reader.readAsDataURL(input.files[0]);
		}
	}


	function toDataURL(src, callback, outputFormat) {
		var img = new Image();
		img.crossOrigin = 'Anonymous';
		img.onload = function () {
			var canvas = document.createElement('CANVAS');
			var ctx = canvas.getContext('2d');
			var dataURL;
			canvas.height = this.naturalHeight;
			canvas.width = this.naturalWidth;
			ctx.drawImage(this, 0, 0);
			dataURL = canvas.toDataURL(outputFormat);
			callback(dataURL);
		};
		img.src = src;
		if (img.complete || img.complete === undefined) {
			img.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";
			img.src = src;
		}
	}

	$scope.hideAllChecklistVideo = function () {
		var show = false;

		if ($scope.videos != null && $scope.videos.length > 0)
			show = true

		return show;

	}

	var dataUrl_;

	function updateImg(originalSrc, newSrc, http, listaImagens, config, confirmUpdateImg) {

		callback = function (dataUrl) {
			dataUrl = dataUrl.substr(dataUrl.indexOf('base64,') + 7, dataUrl.length);

			for (const img of listaImagens) {
				if (originalSrc == img.picPath || originalSrc == img.fixPicPath) {
					dataUrl_ = dataUrl;
					confirmUpdateImg.modal('show');
					break;
				}
			}
		}
		toDataURL(newSrc, callback);
	}

	function trocaImg(picName, config, http, modal) {
		http({
			method: 'PUT',
			url: config.baseUrl + "/uploadServiceImage/" + picName,
			data: dataUrl_
		}).then(function (response) {
			modal.modal('hide');
			location.reload();

		}, function (response) {
			alert($scope.getMessage('ErrorChangeImg') + response.status + "\n" + response.message);
		});
	}

	$scope.trocaPic = function () {
		$('#modalImgDetailService').modal('hide');

		var div = document.getElementById('div_services_principal');
		carregamento(div);

		$('#modal_carregando').modal('show');

		for (const img of $scope.listaImagensPrint) {
			if (orgSrcImg == img.picPath) {
				trocaImg(img.picName, config, $http, $('#modal_carregando'));
				break;
			}

			if (orgSrcImg == img.fixPicPath) {
				trocaImg(img.fixPicName, config, $http, $('#modal_carregando'));
				break;
			}
		}
	}	

	function downloadImg() {
		var objId = "/";

		if ($scope.servico.action.checklist)
			objId = "/" + listService[idxImgService].picId;

		else
			objId = "/" + $scope.listaImagensPrint[idxImgService].picId;

		var cropImage = document.getElementById('cropImageService');

		if (cropImage.href.indexOf(objId) != -1) cropImage.href = cropImage.href;
		else cropImage.href += objId;

		$('#modalImgDetailService').modal('hide');

		setTimeout(() => {
			cropImage.click();
		}, 400);
	}

	$scope.openNoteImage = function(valor) {
		console.log('note: ', valor);
		document.getElementById('note-image').value = valor;
		openNoteImage(document.getElementById('note-image').value);
	} 

	$scope.setImageDetail = function (obj, index) {
		$scope.indexImage = index;
		$scope.selectedImage = $scope.listaImagensPrint[index];
		setImageDetail(obj, index, $scope.listaImagensPrint);
	};

	$scope.setImageDetailCheckList = function (obj, index, list) {
		let pic = $scope.listaImagensPrint[index];
		let idx = list.findIndex(picObj => picObj.picId == pic.picId);
		setImageDetail(obj, idx, list);
	};

	$scope.filterServicesBy = function () {
		filterServicesBy();
	};

	function filterServicesBy() {
		$scope.servicos = $scope.allServices.filter((service) => {
			if ($scope.optBy == "all")
				return true;
			else if ($scope.optBy == "app" && service.createdByUser == null)
				return true;
			else if ($scope.optBy == "web" && service.createdByUser != null)
				return true;

			return false;
		}
		);
	};

	// Create Service
	$scope.createService = function () {
		return config.baseUrl + '/op/serviceCreate';
	};

	$scope.redirectTo = function (service) {
		if (service.status != 'P' && service.status != 'R') {
			return config.baseUrl + '/op/serviceDetail/' + service.serviceId;
		} else {
			return config.baseUrl + '/op/serviceUpdate/' + service.serviceId;
		}
	};

	$scope.selectClient = function (clientSelected) {
		$scope.servico.client = $scope.clientList.find(c => c.clientId == clientSelected);
		$scope.equipmentList = $scope.servico.client.vehicles;
	};

	$scope.selectAction = function (actionSelected) {
		$scope.servico.action = $scope.actionList.find(a => a.actionId == actionSelected);

		$scope.changeAction();
	};

	$scope.selectEquipment = function (equipmentSelected) {
		equipmentSelected = equipmentSelected.replace(/\s+/g, '');
		$scope.servico.vehicle = $scope.equipmentList.find(e => e.vehicleId == equipmentSelected);
	};

	$scope.selectUser = function (userSelected) {
		$scope.servico.user = $scope.usersList.find(u => u.userId == userSelected);
	};

	function loadClientList() {
		$http.get(config.baseUrl + "/admin/clientByActive/true").then(
			function (response) {
				$scope.clientList = response.data;
			},
			function (response) {
				alert($scope.getMessage('ErrorLoadData') + response.status + "\n" + response.message);
			}
		);
	}

	function loadActionList() {
		$http.get(config.baseUrl + "/admin/actionByActive/true").then(
			function (response) {
				$scope.actionList = response.data;
			},
			function (response) {
				alert($scope.getMessage('ErrorLoadData') + response.status + "\n" + response.message);
			}
		);
	}

	function loadUserList() {
		$http.get(config.baseUrl + "/admin/userByActive/true").then(
			function (response) {
				$scope.usersList = response.data;
			},
			function (response) {
				alert($scope.getMessage('ErrorLoadData') + response.status + "\n" + response.message);
			}
		);
	}

	$scope.saveService = function () {
		if (!validateForm())
			return;

		var url = config.baseUrl + "/createService";

		$scope.servico.status = 'P';
		$scope.servico.startTime = timestampNowDDMMYYYY();
		$scope.servico.createdByUser = JSON.parse(localStorage.getItem("bobby"));

		if ($scope.customFieldList.length > 0) {
			customFields = [];
			for (customField of $scope.customFieldList) {
				customFields.push({
					customField: customField.VALUE,
					customFieldValue: customField.DATA
				});
			}
			$scope.servico.customFieldList = customFields;
		}

		$http.put(url, $scope.servico).then(
			function (response) {
				if (response.status >= 200 && response.status <= 299) {
					alert($scope.getMessage('DataSavedSuccessfully'));
					$scope.servico = {};
					$location.path(juiceService.appUrl + 'op/service');
				} else {
					alert($scope.getMessage('ErrorSavingData') + response.status + "\n" + response.message);
				}
			},
			function (response) {
				alert($scope.getMessage('ErrorSavingData') + response.status + "\n" + response.message);
			}
		);
	};

	$scope.changeAction = function () {
		if ($scope.servico.action != null) {
			$scope.customFieldList = JSON.parse($scope.servico.action.fields);
			for (customField of $scope.customFieldList) {
				customField.DATA = "";
			}
		}
	};

	function makeListaImagensPrintReport() {		
		$scope.listaImagensPrintReport = [];
		// console.log("$scope.listaImagensPrint: ", $scope.listaImagensPrint);

		for (sp of $scope.listaImagensPrint) {
			console.log("for sp: ", sp);
			if (sp.picPath != null && sp.picPath.length > 0 && sp.chkPrint === true)
				if (sp.checklistItems != null) {
					$scope.listaImagensPrintReport.push({ url: sp.picPath, message: sp.message, note: sp.note, critically: sp.critically, sistema: sp.checklistItems.checklistServices.checklistName });
				} else {
					$scope.listaImagensPrintReport.push({ url: sp.picPath, message: sp.message, note: sp.note, critically: sp.critically });
				}
				

			if (sp.fixPicPath != null && sp.fixPicPath.length > 0 && sp.chkPrint === true)
				$scope.listaImagensPrintReport.push({ url: sp.fixPicPath, message: sp.message, note: getLabel('Adjusted'), critically: sp.critically });
		}

		console.log('function makeListaImagensPrintReport: ', $scope.listaImagensPrintReport);
	}

	$scope.updateService = function () {
		console.log('update service print rel');
		$http.put(config.baseUrl + "/updateService/" + $scope.servico.serviceId, $scope.servico).then(
			function (response) {
				if (response.status >= 200 && response.status <= 299) {
					makeListaImagensPrintReport();

					var e = document.getElementById('printReportService');
					e.click();
				} else {
					console.error($scope.getMessage('ErrorSavingData') + response.status + "\n" + response.message);
				}
			},
			function (response) {
				console.error($scope.getMessage('ErrorSavingData') + response.status + "\n" + response.message);
			}
		);

		$('#modalExtraInformationReportService').modal('hide');

	};

	$scope.verificaExibicaoCheck = function (customField) {
		return customField.customFieldValue.indexOf("[{") != -1 ? false : true;
	}

	$scope.returnNumberOfRows = function () {
		return $scope.selectedImage.note != null && $scope.selectedImage.note.length > 0 ? 5 : 1;
	};

	$scope.openObsModal = function () {
		$('#modalEdtObservacao').modal();
	};

	$scope.saveNote = function () {
		$http.put(config.baseUrl + '/updatePictureService/' + $scope.selectedImage.picId, $scope.selectedImage).then(
			success => {
				$scope.selectedImage = success.data;
			},
			error => {
				console.error(error);
			}
		);
	};

	function getSignatureService(idServico) {
		var nomeImage = "SIGNATURE_" + idServico;
		if ($scope.servico.action.signature && $scope.servico.path_signature != null && $scope.servico.path_signature.length > 0 && idServico != null) {
			$http({
				method: 'GET',
				url: config.baseUrl + '/admin/downloadImage/' + nomeImage,
				transformResponse: [
					function (data) {
						$scope.signatureService = data;
					}
				]
			}).then(function (response) {

			}, function (response) {
				alert("Erro ao obter assinaturea");
			});
		}
	}
	
	$scope.showModalWindow = () =>{
		fillLists();
		$("#parametersModal").modal({ backdrop: 'static', keyboard: false });
	}
		
	function fillLists(){
		for(var service of $scope.servicos){
			if($scope.clients.indexOf(service.client.tradingName) <= -1)
				$scope.clients.push(service.client.tradingName);
			
			if($scope.activitys.indexOf(service.action.description) <= -1)
				$scope.activitys.push(service.action.description);
			
			if($scope.users.indexOf(service.user.name) <= -1)
				$scope.users.push(service.user.name);
			
			if(service.project != null && $scope.projects.indexOf(service.project.projectName) <= -1)
				$scope.projects.push(service.project.projectName);
		}
	}

	$scope.showClientsSelect = () => {
		$scope.showClient = !$scope.showClient;
		
		if($scope.showClient)
			$("#clientTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
		else
			$("#clientTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
	}
	
	$scope.showActivitysSelect = () => {
		$scope.showActivitys = !$scope.showActivitys;
		
		if($scope.showActivitys)
			$("#activitysTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
		else
			$("#activitysTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
	}
	
	$scope.showUserSelect = () => {
		$scope.showUsers = !$scope.showUsers;
		
		if($scope.showUsers)
			$("#userTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
		else
			$("#userTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
	}
	
	$scope.showProjectSelect = () => {
		$scope.showProject = !$scope.showProject;
		
		if($scope.showProject)
			$("#projectTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
		else
			$("#projectTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
			
	}

	$scope.buscar = () => {
		$rootScope.allServices = null;
		load();
	}
	
	function listFilter(){
		let listaServicesFiltrados = [];
		
		$scope.servicos.forEach(service => {
			service.checked = false;
			listaServicesFiltrados.push(service);
			let removeuElemento = false;
			
			if($scope.clientsSelected.length > 0){
				let idx = 1;
				
				$scope.clientsSelected.forEach(client => {
					if(service.client.tradingName == client) return true;
					if(service.client.tradingName != client && idx == $scope.clientsSelected.length){
						let indexService = listaServicesFiltrados.indexOf(service);
						listaServicesFiltrados.splice(indexService);
						removeuElemento = true;
					}
					idx ++;
				});
			}
			
			if($scope.activitysSelected.length > 0 && !removeuElemento){
				let idx = 1;
				
				$scope.activitysSelected.forEach(activity => {
					if(service.action.description == activity) return true;
					if(service.action.description != activity && idx ==  $scope.activitysSelected.length){
						let indexService = listaServicesFiltrados.indexOf(service);
						listaServicesFiltrados.splice(indexService);
						removeuElemento = true;
					}
					idx ++;
				});
			}
			
			if($scope.usersSelected.length > 0 && !removeuElemento){
				let idx = 1;
				
				$scope.usersSelected.forEach(user => {
					if(service.user.name == user) return true;
					if(service.user.name != user && idx == $scope.usersSelected.length){
						let indexService = listaServicesFiltrados.indexOf(service);
						listaServicesFiltrados.splice(indexService);
						removeuElemento = true;
					}
					idx ++;
				});
			}
			
			if($scope.projectSelected.length > 0 && !removeuElemento){
				let idx = 1;
				
				$scope.projectSelected.forEach(project => {
					if(service.project != null && service.project.projectName == project) return true;
					if(service.project == null || service.project.projectName != project && idx == $scope.projectSelected.length){
						let indexService = listaServicesFiltrados.indexOf(service);
						listaServicesFiltrados.splice(indexService);
						removeuElemetno = true;
					}
					idx++;
				});
			}
		});
		
		$scope.servicos = [];
		listaServicesFiltrados.forEach(service => $scope.servicos.push(service));
		
		fillLabelsParameters();
	}
	
	
	function fillLabelsParameters(){
		$scope.clientsNames = "";
		$scope.activitysNames = "";
		$scope.userNames = "";
		$scope.projectNames = "";
		
		if($scope.clientsSelected.length > 0){
			for(i in $scope.clientsSelected){
				var client = $scope.clientsSelected[i];
				if(!$scope.clientsNames.includes(client))
					$scope.clientsNames += $scope.clientsNames.length == 0 ? client : ", " + client;
			}
		}
		
		if($scope.activitysSelected.length > 0){
			for(i in $scope.activitysSelected){
				var activity = $scope.activitysSelected[i];
				if(!$scope.activitysNames.includes(activity))
					$scope.activitysNames += $scope.activitysNames.length == 0 ? activity: ", " + activity;
			}
		}
		
		if($scope.usersSelected.length > 0){
			for(i in $scope.usersSelected){
				var user = $scope.usersSelected[i];
				if(!$scope.userNames.includes(user))
					$scope.userNames += $scope.userNames.length == 0 ? user : ", " + user;
			}
		}
		
		if($scope.projectSelected.length > 0){
			for(i in $scope.projectSelected){
				var projectName = $scope.projectSelected[i];
				if(!$scope.projectNames.includes(projectName))
					$scope.projectNames += $scope.projectNames.length == 0 ? projectName : ", " + projectName;  
			}
		}
	}
	
	function loadDatePicker(){
		$("#dtInicio").datepicker(datePickerOptions);
		$("#dtFinal").datepicker(datePickerOptions);
	}
	
	$scope.buildStatus = (status) => {
		return buildStatus(status);
	}
	
	function buildStatus(status) { 
		if(status=='O'){
			return $scope.getLabel('Opened');
		}
			
		else if(status=='A'){
			return $scope.getLabel('Partially');
		}
			
		else if(status=='P'){
			return $scope.getLabel('Pending');
		}

		else if(status=='D'){
			return $scope.getLabel('Deleted');
		}
			
		else{
			return $scope.getLabel('Received');
		}
	}

	$scope.checkUncheckAll = () => {
		$scope.servicos.forEach(servico => {
			servico.checked = $scope.checkedUncheckedAll;
			$scope.changeCheck(servico);
		});
	}
	
	$scope.changeCheck = (servico) => {
		if (servico.checked) {
			$scope.servicosSelected.push(servico);
		} else {
			var idx = $scope.servicosSelected.indexOf(servico);
			$scope.servicosSelected.splice(idx, 1);
		}
	}
	
	$scope.printMultiplesServices = async () => {
		console.log('Print lista op/service');
		var arr = [...$scope.servicos.filter(s => s.checked == true)];
		arr.push($scope.servicos[$scope.servicos.length-1]);
		try {
			var lastId;
			$('#modal_carregando').modal('show');
			for (servico of arr) {
				lastId = servico.serviceId;
				await loadService(lastId, false);
			}
			
			var count = 0;
			for (servico of $scope.servicosToPrint) {
				$scope.servico = servico;
				processRetornoServico($scope.servico);
				
				var e = document.getElementById('printService'+servico.serviceId);
				console.log('serviço: ' + e);
				if (count < $scope.servicosSelected.length) {
					document.title = servico.serviceId;
					await new Promise((resolve, reject) => {
						setTimeout(() => {
							e.click();
							resolve();
						}, 3000);
					});
				}

				count++;
			}

			document.title = 'GIMB';
			cleanLists();

			// workaraound to refresh screen and clean checked itens in table
			await loadService(lastId, false);

			$('#modal_carregando').modal('hide');
		} catch (e) {
			console.error(e);
			$scope.servicosToPrint = [];
			$('#modal_carregando').modal('hide');
		}
	}

	$scope.deleteSelectedOS = async () => {
		try {
			var servico = $scope.servicosSelected[0];
			if (servico) {
				$('#modal_carregando').modal('show');
				var response = await  $http.delete(config.baseUrl + "/deleteService/" + servico.serviceId)
				if (response.status && response.status == 200) {
					$rootScope.allServices = null;
					cleanLists();
					load();
				}

				$('#modal_carregando').modal('hide');
			}
		} catch (error) {
			$('#modal_carregando').modal('hide');
		}
	}

	$scope.deleteCurrentOS = () => {
		try {
			setTimeout(async () => {
				var response = await $http.delete(config.baseUrl + "/deleteService/" + $scope.servico.serviceId);
				if (response.status && response.status == 200) {
					$rootScope.allServices = null;
					$location.path(juiceService.appUrl + 'op/service');

					// workaraound to refresh screen
					await loadService($scope.servico.serviceId, false)
				}
			}, 1000);
		} catch (error) {
			console.error(error);
		}
	}

	function cleanLists() {
		$scope.servicos.forEach(s => s.checked = false);
		$scope.servicosSelected = [];
		$scope.servicosToPrint = [];
	}
	
});
