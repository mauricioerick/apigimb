appGIMB.controller("clientController", function ($scope, $http, $routeParams, $location, config, juiceService, geralAPI, $compile) {

	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');
	if (!controllerHeadCheck($scope, $location, juiceService))
		return;

	// $('body').css("overflowY", "auto");
	$('#divMenu').show();

	changeScreenHeightSize('#divTableClient');
	loadCostCenters();

	function load(show) {
		$('#modal_carregando').modal(show ? 'show' : 'hide');
	}

	$scope.client = {
		active: true
	};
	$scope.client.colorId = getRandomColor();
	colorSelectorConf('colorSel', $scope.client.colorId)
	$scope.allClients = [];
	$scope.clients = [];
	$scope.veiculos = [];
	$scope.equipments = [];
	$scope.vehicle = {};
	var idEdit = null;

	$scope.costCentersToSelect = [];
	$scope.costCenters = [];
	$scope.costCenterByClient = [];
	$scope.newCostCenter = "";

	$scope.optActive = "active";

	$scope.costCenterIdSelected = "";

	if ($routeParams.clientId != null) {
		loadClient($routeParams.clientId);
	} else {
		carregarClientes();
	}

	$scope.loadClients = function () {
		carregarClientes();
		loadEquipments();
	};

	$scope.onChangeCostCenter = () => {
		let costCenter = document.getElementById('mainCostCenter');
		let option = costCenter.options[costCenter.selectedIndex].value;

		$scope.client.costCenter = $scope.costCenters.find(cc => cc.costCenterId == option);
		$scope.salvar($scope.client, false);
	}

	$scope.addNewCostCenter = () => {
		if ($scope.newCostCenter === "") {
			return alert(getMessage('YouMustSelectCostCenter'));
		}

		$scope.client.costCenters.push(JSON.parse($scope.newCostCenter));
		$scope.salvar($scope.client, false);

		let costCenterToRemove = $scope.costCentersToSelect.find((val) => val.costCenterId == JSON.parse($scope.newCostCenter).costCenterId );

		$scope.costCentersToSelect.splice($scope.costCentersToSelect.indexOf(costCenterToRemove), 1);
		$scope.newCostCenter = "";

		$('#modalCostCenter').modal('hide');
	}

	$scope.removeCostCenter = (costCenter) => {
		const newCostCenterList = $scope.costCenterByClient.filter(costCenterLocated => costCenterLocated.costCenterId !== costCenter.costCenterId);
		$scope.costCenterByClient = newCostCenterList;

		$scope.client.costCenters = JSON.parse(angular.toJson(newCostCenterList));
		$scope.costCentersToSelect.push(costCenter);
		$scope.salvar($scope.client, false);
	}

	$scope.verifyCostCenter = (costCenter1, costCenter2) => {
		if ((costCenter1 === null || costCenter1 === undefined) || (costCenter2 === null || costCenter2 === undefined))
			return ""

		return costCenter1 === costCenter2 ? "selected" : "";
	}

	function carregarClientes() {
		$http({
			method: 'GET',
			url: config.baseUrl + '/admin/client'
		}).then(function (response) {
			$scope.allClients = response.data;
			
			$scope.query = getTextClient();
		 	$scope.optActive = getOptionClient().length > 0 ? getOptionClient() : 'active';
			
			filterClientsByStatus();
			loadEquipments();
		}, function (response) {
			console.log('erro ao carregar:', response);
			$location.path(juiceService.appUrl + 'login');
		});
	};

	function loadCostCenters() {
		$http.get(config.baseUrl + "/admin/costCenterActive/true").then(
			(response) => {
				$scope.costCenters = response.data;
			}, (responseError) => {
				console.error(`${getLabel('ErrorLoadingCostCenter')} ${responseError}`);
			}
		)
	}

	function loadCostsCentersNotSelected() {
		for (const costCenter of $scope.costCenters) {
			let locatedCC = $scope.costCenterByClient.find(cc => {
				if ((cc.costCenterId === costCenter.costCenterId) || (($scope.client.costCenter) && costCenter.costCenterId === $scope.client.costCenter.costCenterId)) {
					return cc;
				}
			});

			if (!locatedCC) {
				$scope.costCentersToSelect.push(costCenter);
			}
		}
	}

	function loadClient(id) {
		idEdit = null;
		$http({
			method: 'GET',
			url: config.baseUrl + '/admin/client/' + id
		}).then(function (response) {
			$scope.client = response.data;
			$scope.costCenterByClient = response.data.costCenters;
			loadCostsCentersNotSelected();

			$scope.costCenterIdSelected = $scope.client.costCenter ? $scope.client.costCenter.costCenterId.toString() : '';

			$scope.veiculos = $scope.client.vehicles;
			$scope.costCenterByClient = response.data.costCenters;

			if ($scope.client.colorId == null) $scope.client.colorId = getRandomColor();

			colorSelectorConf('colorSel',
				$scope.client.colorId)

			loadEquipments();
		}, function (response) {
			console.log('erro ao carregar:', response);
			$scope.allClients = response.data;
			filterClientsByStatus();
		});

		carregaContatos(id);
	}

	function carregaContatos() {
		var id = $routeParams.clientId;

		$http({
			method: 'GET',
			url: config.baseUrl + '/admin/contacts/' + id
		}).then(function (response) {
			$scope.contacts = response.data;
		}, function (response) {

		});
	}

	$scope.salvar = function (objClient, showAlert) {

		if (!validateForm())
			return;

		if ($scope.veiculos.length > 0) {
			$scope.client.vehicles = $scope.veiculos;
		}

		if (($scope.client.costCenter) && ($scope.client.costCenter.costCenterId === ""))
			$scope.client.costCenter = null

		var url = config.baseUrl + "/admin/createClient";

		$http({
			method: 'PUT',
			url: url,
			data: $scope.client
		}).then(function (response) {

			$scope.costCenterByClient = response.data.costCenters;
			showAlert ? alert($scope.getMessage('DataSavedSuccessfully')) : false;

			const splitedUrl = url.split('/');
			(splitedUrl[splitedUrl.length - 1] === 'createClient') ? $location.path(`${juiceService.appUrl}clientDetail/${response.data.clientId}`) : false;

			// refreshPage();
			// alert($scope.getMessage('DataSavedSuccessfully'));
		}, function (response) {
			alert(error + response.status);
		});
	};

	$scope.addVehicle = function (objVehicle) {
		if ($scope.client.clientId) {
			objVehicle.clientId = $scope.client.clientId;
			$http({
				method: 'PUT',
				url: config.baseUrl + '/admin/createVehicle',
				data: objVehicle
			}).then(function (response) {
				addVeiList(objVehicle);
			}, function (response) {
				// mensagem
			});

		} else {
			addVeiList(objVehicle);
		}
	}

	$scope.addVehicle = function (objVehicle) {
		var urlSave = config.baseUrl + '/admin/createVehicle/';
		var newVehicle = true;

		if (objVehicle.vehicleId > 0) {
			urlSave = config.baseUrl + '/admin/updateVehicle/'
				+ objVehicle.vehicleId;
			newVehicle = false;
		}

		if ($scope.client.clientId) {
			objVehicle.clientId = $scope.client.clientId;
			$http({
				method: 'PUT',
				url: urlSave,
				data: objVehicle
			})
				.then(
					function (response) {

						objVehicle = response.data;

						$http(
							{
								method: 'PUT',
								url: config.baseUrl
									+ '/admin/updateVehicleToClient/'
									+ objVehicle.vehicleId,
								data: $scope.client
							}).then(
								function (response) {

								}, function (error) {

								});

						if (newVehicle)
							addVeiList(objVehicle);

						refreshVeiculo();
					},
					function (error) {
						alert($scope
							.getMessage('ErrorSavingDataWithoutParam'));
					});

		} else {
			addVeiList(objVehicle);
		}
	}

	$scope.mudaSituacao = function (objClient) {
		objClient.active = objClient.active == true ? false
			: true;

		$http
			.put(
				config.baseUrl + '/admin/updateClient/'
				+ objClient.clientId, objClient)
			.then(
				function (response) {
					alert($scope
						.getMessage('DataSavedSuccessfully'));
					carregarClientes();
				},
				function (response) {
					alert($scope
						.getMessage('ErrorSavingDataWithoutParam'));
				});
	}

	function addVeiList(obj) {
		if ($scope.veiculos.indexOf(obj) == -1)
			$scope.veiculos.push(obj);

		refreshVeiculo();
	}

	$scope.editVehicle = function (objVehicle) {
		console.log('client');
		$scope.vehicle = objVehicle;
	}

	$scope.novoVehicle = function () {
		$scope.vehicle = {};
	}

	$('#modalVehicle').on('hide.bs.modal', function(event) {
		refreshVeiculo();
	});

	$scope.removeVei = function (objVehicle) {
		if ($scope.client.clientId) {
			$http({
				method: 'PUT',
				url: config.baseUrl
					+ '/admin/removeFromClientVehicle/'
					+ objVehicle.vehicleId,
				data: objVehicle
			}).then(function (response) {
				if (response.status == "409") {
					alert($scope.getMessage('VehicleCanNotBeRemoved'));
				} else if (response.status >= "200"
					&& response.status <= "299") {
					removeVeiList(objVehicle);
				}
			},
				function (response) {
					alert($scope.getMessage('ErrorDeletingData') + response.status);
				});
		} else {
			removeVeiList(objVehicle);
		}
	};

	$scope.vehicleDescription = function (vehicle) {
		var desc = vehicle.brand == null ? '' : vehicle.brand;
		if (desc.length > 0)
			desc += ' - ';
		desc += vehicle.model == null ? '' : vehicle.model;
		if (desc.length > 0)
			desc += ' - ';
		desc += vehicle.plate == null ? '' : vehicle.plate;

		return desc;
	}

	$scope.filterClientsByStatus = function () {
		filterClientsByStatus();
	};

	function filterClientsByStatus() {
		$scope.clients = $scope.allClients.filter(function (
			client) {
			if ($scope.optActive == "all")
				return true;
			else if ($scope.optActive == "active"
				&& client.active == true)
				return true;
			else if ($scope.optActive == "inactive"
				&& client.active == false)
				return true;

			return false;
		});
		
		setOptionClient($scope.optActive);
		
		if($scope.optActive == 'active')
			document.getElementById('optActive').checked = true;
		else if($scope.optActive == 'inactive')
			document.getElementById('optInactive').checked = true;
		else
			document.getElementById('optAll').checked = true;
		
		
		$scope.clients = orderObject( $scope.clients, 'tradingName' );
	}

	function removeVeiList(obj) {
		$scope.veiculos.splice($scope.veiculos.indexOf(obj), 1);
	};

	function refreshPage() {
		$scope.client = {};
		$scope.clients = [];
		$scope.allClients = [];
		$scope.veiculos = [];
		$scope.vehicle = {};
		$scope.porra = [];
	}	

	function refreshVeiculo() {
		$scope.vehicle = {};
	}

	function loadEquipments() {
		$http
			.get(config.baseUrl + "/admin/equipment")
			.then(
				function (response) {
					$scope.equipments = response.data;
				},
				function (response) {
					alert($scope
						.getMessage('ErrorLoadingEquipment.p'));
				});
	}

	$scope.retornaClasse = function (objCli) {
		if (objCli.active == true) {
			return 'btnativo'
		} else {
			return 'btninativo'
		}
	}

	$scope.trataAtivoInativo = function (objCli) {
		if (objCli.active == true) {
			return $scope.getLabel('Active')
		} else {
			return $scope.getLabel('Inactive')
		}
	}

	$scope.addContact = function () {
		var url;

		if (idEdit != null) {
			url = config.baseUrl + "/admin/updateContacts/" + $scope.client.clientId;
		} else {
			url = config.baseUrl + "/admin/createContact/" + $scope.client.clientId;
		}

		const name = document.getElementById('edtName').value;
		const phone = document.getElementById('edtPhone').value;
		const office = document.getElementById('edtOffice').value;
		const observation = document.getElementById('edtOBS').value;
		const contactId = idEdit;

		if (name == "" || office == "") {
			alert($scope.getMessage('RequiredFields') + getLabel('ContactName') + ', ' + getLabel('Office'));
		}
		else {
			const params = { name, phone, office, observation, contactId }

			$http.put(url, params).then(function (response) {
				if (idEdit == null)
					$scope.contacts.push(response.data);
				else
					location.reload();

				$('#modalContacts').modal('hide');
			},
				function (response) {
					alert($scope.getMessage('ErrorSavingData'));
				});
		}
	}

	$scope.editContact = function (contact) {
		if (contact != null || contact != undefined) {

			$('#modalContacts').modal('show');

			const name = document.getElementById('edtName');
			const phone = document.getElementById('edtPhone');
			const office = document.getElementById('edtOffice');
			const obs = document.getElementById('edtOBS');

			name.value = contact.name;
			phone.value = contact.phone;
			office.value = contact.office;
			obs.value = contact.observation;

			idEdit = contact.contactId;
		}
		else {
			$('#modalContacts').modal('show');

			const name = document.getElementById('edtName');
			const phone = document.getElementById('edtPhone');
			const office = document.getElementById('edtOffice');
			const obs = document.getElementById('edtOBS');

			name.value = '';
			phone.value = '';
			office.value = '';
			obs.value = '';

			idEdit = null;
		}
	}

	$scope.removeContact = function (contact) {
		var idx = 0;

		for (var i = 0; i < $scope.contacts.length; i++) {
			if ($scope.contacts[i].contactId == contact.contactId) {
				idx = i;
				break;
			}
		}

		const url = config.baseUrl + '/admin/deleteContact/' + contact.contactId;

		$http.put(url).then(function (response) {
			$scope.contacts.splice(idx, 1);

		}, function (response) {
			alert($scope.getMessage('ErrorSavingData'));
		});
	}
	
	$scope.setTextClient = () => {
		setTextClient($scope.query);
	}


	/*
	* IMPORT EQUIPMENTS
	*/

	$scope.client.colorId = getRandomColor();
	colorSelectorConf('colorSel', $scope.client.colorId);

	function loadEquipments() {
		$http.get(config.baseUrl + "/admin/equipment")
			.then(function (response) {
				$scope.equipments = response.data;
			}, function (response) {
				alert("Erro ao carregar equipamentos!");
			});
	}

	function carregarCliente(id) {
		$http({ method: 'GET', url: config.baseUrl + '/admin/client/' + id })
			.then(function (response) {
				$scope.client = response.data;
				$scope.veiculos = $scope.client.vehicles;
				if ($scope.client.colorId == null)
					$scope.client.colorId = getRandomColor();

				colorSelectorConf('colorSel', $scope.client.colorId)

				loadEquipments();
			}, function (response) {

			});
	};
	
	if ($routeParams.clientId != null) {
		loadImportsByClassName();
		
		function loadImportsByClassName(){
			load(true);
			
			$http.get(`${config.baseUrl}/admin/importsByClass/EQUIPMENT`).then(
				function(response){				
					$scope.imports = response.data
					load(false);
				},
				function(response){
					alert($scope.getMessage('ErrorLoadData'));
					load(false);
				}
			);
		}

		$('#modalImportXls').on('hide.bs.modal', function(event) {
			clearModal();
		});	
		
		function clearModal() {
			document.getElementById('uploadXls').value = '';
			document.getElementById('divImport').innerHTML = '';
			$scope.imports = null;
		}

		$(document).ready(function() {
			$('#uploadXls').change(function(){
				if(this.files && this.files[0]){
					insertIconXls(this.files[0].name);
					readTheFileUrl(this.files[0]);
				}
			});
		});

		function insertIconXls(name){
			const inner = `<img id="xlsImported" src="apiGIMB/view/TablesForImport/xls.png" style="max-height: 50px; max-width: 50px;">
				<br/>
				<span>${name}</span>
				<br/>
				<div  ng-click="importXls()" > 
					<i class = "glyphicon glyphicon-send"></i>
					<input type="button" value="${getLabel('send')}" class="btn formGroupInput">
				</div>
			`;
				
			const $str = $(inner).appendTo('#divImport');
			$compile($str)($scope);
		}

		$scope.importXls = () => {
			load(true);	
			
			if ($scope.imports != null) {			
				$http.post(config.baseUrl + '/admin/importXLSTableVehicle/' + $routeParams.clientId, $scope.imports).then(
					function(response){
						console.log('Retorno importXls: ', response);	
						
						if (response.data.length > 0) {
							var identificadores = '';
							for (let i = 0; i < response.data.length; i++) {
								identificadores = identificadores + ', ' + response.data[i].plate;
							}
							alert('Equipamento não encontrado, para o identificador: - ' + identificadores);						
						}
						
						alert($scope.getMessage('DataSavedSuccessfully'));
						carregarCliente($routeParams.clientId);

						$('#modalImportXls').modal('hide');

						load(false);
					},
					function(response){
						alert($scope.getMessage('ErrorLoadData'));
						load(false);
					}
				);
			}
			else {
				alert($scope.getMessage('ErrorLoadData'));
			}
		}

		function readTheFileUrl(file){
			var reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = (e) => {
				$scope.imports.filePath = e.target.result.substr(e.target.result.indexOf('base64,') + 7, e.target.result.length);;
			}
		}
	}

});
