appGIMB.controller("cardController", function ($scope, $http, $routeParams, $location, config, juiceService, geralAPI) {
	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');
	if (!controllerHeadCheck($scope, $location, juiceService))
		return;

	$('#divMenu').show();
	
	$scope.card = {active: true};
	$scope.optActive = "active";
	$scope.card.colorId = getRandomColor();
	colorSelectorConf('colorSel', $scope.card.colorId);
	$scope.cards = [];
	$scope.cardsVisible = [];
	$scope.viewCreditRenewalDay = false;

	$scope.daysOfMonth = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30];

	$http({
		method: 'GET',
		url: `${config.baseUrl}/admin/tableParameterByTableReference/card`
	}).then((response) => {
		$scope.parameters = response.data;
		$scope.parameters.jsonStructure = JSON.parse($scope.parameters.jsonStructure);

		$scope.parameters.jsonStructure.forEach( p => {
			p.label = getLabelParameter(p.field);
		});
	});

	$scope.users = [];
	$scope.banners = [];
	$scope.emittingBanks = [];
	$scope.bannerNames = "";
	$scope.emittingBankNames = "";
	
	colorSelectorConf('colorSel', $scope.card.colorId);
	$scope.bannersSelected = getBannerCard();
	$scope.emittingBankSelected = getEmittingBankCard();
	$scope.dtInicio = getDataInicioFiltroCard();
	$scope.dtFinal = getDataFinalFiltroCard();
	
	if($routeParams.cardId)
		loadCard($routeParams.cardId);
	else
		loadCards();
	
	function loadCard(cardId){
		$('#modal_carregando').modal('show');
		
		$http.get(config.baseUrl + '/admin/card/' + cardId).then(
				function(response){
					$scope.card = response.data;
					
					$scope.card.automaticRenovation ? $scope.viewCreditRenewalDay = true : $scope.viewCreditRenewalDay = false;
			
					loadDatePicker();
					
					$('#modal_carregando').modal('hide');
				},
				function(response){
					$('#modal_carregando').modal('hide');
					setTimeout(function () {
						alert($scope.getMessage('ErrorLoadData'));
					}, 500);
				}
		);
	}
	
	function loadCards(){
		const init = document.getElementById('dtInicio').value.split('/'); 
		const final =	document.getElementById('dtFinal').value.split('/');
		const dtInicio = init[0].length > 0 ? init[0] : ''; 
		const dtFinal = final[0].length > 0 ? final[0] : '';
		
		setDataInicioFiltroCard($scope.dtInicio);
		setDataFinalFiltroCard($scope.dtFinal);
		setBannerCard($scope.bannersSelected);
		setEmittingBankCard($scope.emittingBankSelected);
		
		const url = `${config.baseUrl}/admin/getCardBetweentData/${dtInicio}&${dtFinal}`;
		
		$http.get(url).then(
			function(response){
				$scope.cards = response.data;
				filterCardByStatus();
				loadDatePicker();
			},
			function(response){
				alert($scope.getMessage('ErrorLoadData'));
			}
		);
	}
	
	$scope.filterCardByStatus = function(){
		filterCardByStatus();
	}
	
	function filterCardByStatus(){
		$scope.cardsVisible = $scope.cards.filter(function(card) {
			if($scope.optActive == 'all')
				return true;
			
			else if($scope.optActive == 'active' && card.active == true)
				return true;
			
			else if($scope.optActive == 'inactive' && card.active == false)
				return true;
			
			else 
				return false;
		});
		
		filtrarLista();
	}
	
	$scope.retornaClasse = function(objCard){
		if(objCard.active == true)
			return 'btnativo';
		
		else 
			return 'btninativo';
	}
	
	$scope.trataAtivoInativo = function(objCard){
		if(objCard.active == true)
			return $scope.getLabel('Active');
		
		else
			return $scope.getLabel('Inactive');
	}
	
	$scope.mudaSituacao = function(objCard){
		$('#modal_carregando').modal('show');
		
		objCard.active = objCard.active == true ? false : true;
		
		$http.put(config.baseUrl + '/admin/saveCard', objCard).then(
				function(response){
					loadCards();
					$('#modal_carregando').modal('hide');
				},
				function(response){
					$('#modal_carregando').modal('hide');
					setTimeout(function () {
						alert($scope.getMessage('ErrorSavingDataWithoutParam'));
					}, 500);
				}
		);
	}
	
	$scope.saveCard = function(objCard){
		if (!validateForm())
			return;
		
		else{
			$('#modal_carregando').modal('show');

			if($scope.parameters){
				const customValues = {
					jsonVersion : $scope.parameters.jsonVersion
				};
				
				objCard = {
					...objCard,
					customValues
				};
			}
			
			if(objCard.limit == '' || objCard.limit == null || objCard.limit == undefined){
				objCard.limit == 0;
			}
				
			if(objCard.credict == '' || objCard.credict == null || objCard.credict == undefined){
				objCard.credict == 0;
			}

			$http.put(config.baseUrl + '/admin/saveCard', objCard).then(
					function(response){
						$('#modal_carregando').modal('hide');
						
						setTimeout(function () {
							$('#backCardList').trigger('click');
						}, 500);
					},
					function(response){
						$('#modal_carregando').modal('hide');
						setTimeout(function () {
							alert($scope.getMessage('ErrorSavingDataWithoutParam'));
						}, 500);
					}
			);
		}
	}
	
	$scope.changeViewCreditRenewalDay = function(){
		$scope.viewCreditRenewalDay = !$scope.viewCreditRenewalDay;
	}
	
	$scope.buscar = () => {
		loadCards();
	}
	
	function filtrarLista(){
		let listaCardsFiltrados = [];
		
		$scope.cardsVisible.forEach(card => {
			listaCardsFiltrados.push(card);
			let removeuElemento = false;
			
			if($scope.bannersSelected.length > 0){
				let idx = 1;
				
				$scope.bannersSelected.forEach(banner => {
					if(card.cardBanner == banner) return true;
					if(card.cardBanner != banner && idx == $scope.bannersSelected.length){
						let indexCard = listaCardsFiltrados.indexOf(card);
						listaCardsFiltrados.splice(indexCard);
						removeuElemento = true;
					}
					idx ++;
				});
			}
			
			if($scope.emittingBankSelected.length > 0 && !removeuElemento){
				let idx = 1;
				$scope.emittingBankSelected.forEach(emitting => {
					if(emitting == card.emittingBank) return true;
					if(emitting != card.emittingBank && idx == $scope.emittingBankSelected.length){
						let indexCard = listaCardsFiltrados.indexOf(card);
						listaCardsFiltrados.splice(indexCard);
						removeuElemento = true;
					}
					idx ++;
				});
			}
		});
		
		$scope.cardsVisible = [];
		listaCardsFiltrados.forEach(card => $scope.cardsVisible.push(card));
		
		fillLabelsParameters();
	}
	
	function fillLabelsParameters(){
		$scope.bannerNames = "";
		if($scope.bannersSelected.length > 0){
			for(i in $scope.bannersSelected){
				var banner = $scope.bannersSelected[i];
				if(!$scope.bannerNames.includes(banner))
					$scope.bannerNames +=  $scope.bannerNames.length == 0 ? banner : ", " + banner; 
			}
		}
		
		$scope.emittingBankNames = "";
		if($scope.emittingBankSelected.length > 0){
			for(i in $scope.emittingBankSelected){
				var emittingBank = $scope.emittingBankSelected[i];
				if(!$scope.emittingBankNames.includes(emittingBank))
					$scope.emittingBankNames +=  $scope.emittingBankNames.length == 0 ? emittingBank : ", " + emittingBank; 
			}
		}
	}
	
	function getBanner(){
		//armazenando todas as bandeiras disponiveis nos cartoes
		for(var card of $scope.cards)
			if($scope.banners.indexOf(card.cardBanner) <= -1)
				$scope.banners.push(card.cardBanner);
	}
	
	function getBank(){
		//armazenando todos os bancos disponiveis nos cartoes
		for(var card of $scope.cards)
			if($scope.emittingBanks.indexOf(card.emittingBank) <= -1)
				$scope.emittingBanks.push(card.emittingBank);
	}
	
	$scope.showModalWindow = function(){
		getBanner();
		getBank();
		$("#parametersModal").modal({ backdrop: 'static', keyboard: false });
	}
	
	$scope.showUsersSelect = function () {
		$scope.showUsers = $scope.showUsers ? false : true

		if ($scope.showUsers)
			$("#userTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up')
		else
			$("#userTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down')
	}
	
	$scope.showBannerSelect = function () {
		$scope.showBanners = $scope.showBanners ? false : true

		if ($scope.showBanners)
			$("#bannerTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up')
		else
			$("#bannerTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down')
	}
	
	$scope.showEmittingBankSelect = function () {
		$scope.showEmittingBank = $scope.showEmittingBank ? false : true

		if ($scope.showEmittingBank)
			$("#bankTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up')
		else
			$("#bankTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down')
	}
	
	function loadDatePicker(){
		$("#dtInicio").datepicker(datePickerOptions);
		$("#dtFinal").datepicker(datePickerOptions);
	}
});