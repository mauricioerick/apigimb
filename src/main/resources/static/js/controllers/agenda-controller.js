appGIMB.controller("agendaController", function
    ($scope, $http, $routeParams, $location, config, juiceService, geralAPI) {
    $('form').attr('autocomplete', 'off');
    $('input[type=text]').attr('autocomplete', 'off');
    if (!controllerHeadCheck($scope, $location, juiceService))
        return;

    $('#divMenu').show();
    $("#dtDate").datepicker(datePickerOptions);
    $("#dtInicio").datepicker(datePickerOptions);
    $("#dtFinal").datepicker(datePickerOptions);

    const userLogado = JSON.parse(localStorage.bobby);
    const userLogadoId = userLogado.userId;

    //Agenda
    $scope.allSchedules = [];
    $scope.search = {
        dtInicial: getDateNow(),
        dtFinal: getDateNow(),
        status: 'active'
    };

    //Agenda Create
    $scope.form = {
        date: getDateNow(),
        userId: userLogadoId,
        agendaItems: []
    }
    $scope.item = {
        clientId: "",
        actionId: "",
        vehicleId: "",
        projectId: "",
        status: 1
    }

    //Agenda Update
    $scope.agendaItem = {};

    var error = $scope.getMessage('ErrorSavingData');

    $scope.init = function () {
        loadSchedules();
    }

    $scope.initCreate = function () {
        loadOptions();
    }

    $scope.initUpdate = function () {
        if ($routeParams.agendaItemId) {
            loadOptions();
            loadAgendaItem($routeParams.agendaItemId);
        }
    }

    $scope.reload = () => loadSchedules();

    function loadSchedules() {
        $http.get(getUrlAgenda()).then(
            function (response) {
                $scope.allSchedules = response.data;
            },
            function (response) {
                alert($scope.getMessage('ErrorLoadData'));
            }
        );
    }

    function loadOptions() {
        Promise.all([
            $http.get(config.baseUrl + '/admin/client'),
            $http.get(config.baseUrl + '/admin/action'),
            $http.get(config.baseUrl + '/admin/vehicle'),
            $http.get(config.baseUrl + '/admin/projects'),
        ]).then((res) => {
            $scope.clients = res[0].data;
            $scope.actions = res[1].data;
            $scope.vehicles = res[2].data;
            $scope.projects = res[3].data;
        }).catch((err) => {
            alert($scope.getMessage('ErrorLoadData'));
        });
    }

    function loadAgendaItem(agendaItemId) {
        $http.get(config.baseUrl + '/admin/agendaItem/' + agendaItemId).then(
            function (response) {
                $scope.agendaItem = response.data;
            },
            function (response) {
                alert($scope.getMessage('ErrorLoadData'));
            }
        )
    }

    $scope.retornaClasse = function (objSchedule) {
        if (objSchedule.status == true) {
            return 'btnativo';
        } else {
            return 'btninativo';
        }
    };

    $scope.trataAtivoInativo = function (objSchedule) {
        if (objSchedule.status == true) {
            return $scope.getLabel('Active');
        } else {
            return $scope.getLabel('Inactive');
        }
    };

    $scope.mudaSituacao = function (objSchedule) {
        console.log(objSchedule);
        let statusAgendaItem = objSchedule.status == true ? 0 : 1;

        $http.put(config.baseUrl + '/admin/agendaItem/status/' + objSchedule.agendaItemId, {status: statusAgendaItem}).then(
            function (response) {
                alert($scope.getMessage('DataSavedSuccessfully'));
                loadSchedules();
            },
            function (response) {
                alert($scope.getMessage('ErrorSavingDataWithoutParam'));
            }
        );
    };

    function getDateNow() {
        moment.locale(localStorage["language"]);
        return moment().format('L');
    }

    function getUrlAgenda() {
        let url = `${config.baseUrl}/admin/agenda?dtInicial=${$scope.search.dtInicial}&dtFinal=${$scope.search.dtFinal}&status=${$scope.search.status}&userId=${userLogadoId}`;
        return url;
    }

    $scope.addItem = function () {
        if($scope.item.clientId || $scope.item.actionId || $scope.item.vehicleId) {
            let itemFinal = Object.assign({}, $scope.item);
            itemFinal['clienteName'] = getClientById(itemFinal.clientId);
            itemFinal['actionName'] = getActionById(itemFinal.actionId);
            itemFinal['vehicleName'] = getVehicleById(itemFinal.vehicleId);
            itemFinal['projectName'] = getProjectById(itemFinal.projectId);

            console.log(itemFinal);
            $scope.form.agendaItems.push(itemFinal);
            $scope.item = {
                clientId: "",
                actionId: "",
                vehicleId: "",
                projectId: "",
                status: 1
            }
        }
    }

    $scope.delete = function (agenda) {
        let index = $scope.form.agendaItems.indexOf(agenda);
        console.log(index);
        if(index > -1) {
            $scope.form.agendaItems.splice(index, 1);
        }
    }


    function getClientById(id) {
        let client = $scope.clients.filter(client => client.clientId == id)
        if(client && client.length > 0) {
            return client[0].companyName;
        } else {
            return "";
        }
    }

    function getActionById(id) {
        let action = $scope.actions.filter(action => action.actionId == id);
        if(action && action.length > 0) {
            return action[0].description;
        } else {
            return "";
        }
    }

    function getVehicleById(id) {
        let vehicle = $scope.vehicles.filter(vehicle => vehicle.vehicleId == id);
        if(vehicle && vehicle.length > 0) {
            return vehicle[0].brand + " - " + vehicle[0].plate
        }

    }

    function getProjectById(id) {
        let projetc = $scope.projects.filter(project => project.projectId == id);
        if(projetc && projetc.length > 0) {
            return projetc[0].projectName;
        }
    }

    $scope.saveAgenda = function () {
        console.log('Save Agenda');
        if ($scope.form.agendaItems.length === 0)
            return;

        $http.post(`${config.baseUrl}/admin/agenda`, $scope.form).then(
            function (response) {
                if (response.status >= 200 && response.status <= 299) {
                    alert($scope.getMessage('DataSavedSuccessfully'));
                    $location.path(juiceService.appUrl + 'schedule');
                } else {
                    alert(error + response.status + "\n" + response.message);
                }
            },
            function (response) {
                alert(error + response.status + "\n" + response.message);
            })
    }

    $scope.saveAgendaItem = function () {
        $http.put(`${config.baseUrl}/admin/agendaItem/${$scope.agendaItem.agendaItemId}`, $scope.agendaItem).then(
            function (response) {
                if (response.status >= 200 && response.status <= 299) {
                    alert($scope.getMessage('DataSavedSuccessfully'));
                    $location.path(juiceService.appUrl + 'schedule');
                } else {
                    alert(error + response.status + "\n" + response.message);
                }
            },
            function (response) {
                alert(error + response.status + "\n" + response.message);
            })
    }




});
