appGIMB.controller("tableForImportController", function ($scope, $http, $routeParams, geralAPI, juiceService, config, $location, $compile) {

	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');
	if (!controllerHeadCheck($scope, $location, juiceService)) return;

	$('#divMenu').show();
	
	var tablesNotIncludeTheList = ['imports'];
	$scope.importsForRemove = null;
	$scope.editImports = null;
	$scope.tableList = [];
	$scope.allImports = [];
	$scope.imports = {};
	$scope.tableReferenceForEdit = '';
	
	loadAllImports();
	loadTablesToImport();
	
	function showLoad(show) {
		$('#modal_carregando').modal(show ? 'show' : 'hide');
	}
	
	function loadTablesToImport(){
		$http.get(config.baseUrl + '/admin/allTablesForImports').then(
			function(response){
				if(response.data != null && response.data.length > 0){
					filterTablesForImports(response.data);
				}
			},
			function(response){
				alert(response.status + "\n" + response.message);
			}
		);
	}
	
	function filterTablesForImports(list){
		$scope.tableList = list.filter( (table)  => {
			if(tablesNotIncludeTheList.indexOf(table.toUpperCase()) <= -1) {
				return true;
			}
		});
	}
	
	function loadAllImports(){
		$http.get(config.baseUrl + '/admin/allImports').then(
			function(response){
				$scope.allImports = response.data;
				insertImportsIntoTableNotIncludeTheList( $scope.allImports );
			},
			function(response){
				alert(response.status + "\n" + response.message);
			}
		);
	}
	
	function insertImportsIntoTableNotIncludeTheList(imports){
		for( var table of imports ){
			tablesNotIncludeTheList.push(table.nameClass.toUpperCase());
		}
	}
	
	function inner(fileName){
		return `<div id="icon" style="cursor: pointer; margin-left: 80px; margin-top: 10px; margin-button: 10px; max-height: 160px; max-width: 160px;">
					<img id="xlsImported" src="apiGIMB/view/TablesForImport/xls.png" style="max-height: 50px; max-width: 50px;">
					<br\>
					<span>${fileName}</span>
				</div>`;
	}
	
	function clearXlsImported(){
		document.getElementById('divForInner').innerHTML = "";
		$scope.imports.nameClass == ''
		document.getElementById('inputXls').value = '';
		$scope.editImports = null;
	}
	
	$scope.save = () => {
		showLoad(true);
		
		const imports = ( $scope.editImports != null ) ? $scope.editImports : $scope.imports;
			
		$http.put(config.baseUrl + '/admin/createImports', imports).then(
			function(response){
				clearXlsImported();
				reorganizeLsitsAfterInsert();
				
				showLoad(false);
			},
			function(response){
				alert(response.status + "\n" + response.message);
				showLoad(false);
			}
		);
	}
	
	$(document).ready(function() {
		$('#inputXls').change(function() {
			if(this.files && this.files[0]){
				insertIconXls(this.files[0].name);
				readTheFileUrl(this.files[0]);
				insertMimeInImports(this.files[0].name);
			}
		});	
	});
	
	function insertMimeInImports(name){
		const mime = name.split('.');
		$scope.imports.mime = mime[ mime.length - 1 ];
	}
	
	function insertIconXls(fileName){
		var $str = $(inner(fileName)).appendTo('#divForInner');
		$compile($str)($scope);
	}
	
	function readTheFileUrl(file) {
		var reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = (e) => {
			if($scope.editImports != null)
				$scope.editImports.filePath = e.target.result;
			else
				$scope.imports.filePath = e.target.result;
		};
	}
	
	$scope.disableSaveButton = () => {
		return ($scope.imports.nameClass == '' && document.getElementById('inputXls').value == ''); 
	} 
	
	$scope.editImport = (imports) => {
		$scope.editImports = imports;
		$("#modalNewTableForImport").modal('show');
	}
	
	$scope.remove = async (importForRemove) => {
		showLoad(true);
		
		var response = null;
		
		try {
			response = await $http.post(`${config.baseUrl}/admin/deleteImports`, importForRemove);
		}
		catch(error){
			
			alert(error);
			
		} finally {
			
			if(response != null){
				if(response.status < 400){
					reorganizeListsAfterRemoving();
				} 
				else {
					alert(response.status + "\n" + response.message);
				}
			}
			else {
				alert('response is null');
			}
			
			showLoad(false);
		}
	}
	
	function reorganizeListsAfterRemoving() {
		loadAllImports();
		loadTablesToImport();
	}
	
	function reorganizeLsitsAfterInsert(){
		loadAllImports();
		loadTablesToImport();
	}
	
	$('#modalNewTableForImport').on('hide.bs.modal', function (event) {
		clearXlsImported();
	});
	
	$('#modalConfirmDelete').on('hide.bs.modal', function (event) {
		$scope.importsForRemove = null;
	});
	
	$scope.showModalConfirmRemove = (imports) => {
		$scope.importsForRemove = imports;
		$("#modalConfirmDelete").modal('show');
	}
	
});