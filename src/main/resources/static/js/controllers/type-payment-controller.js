appGIMB.controller("typePaymentController", function ($scope, $http, $routeParams, $location, config, juiceService, values) {
    "use strict";

    $('form').attr('autocomplete', 'off');
    $('input[type=text]').attr('autocomplete', 'off');
    $('#divMenu').show();

    if (!controllerHeadCheck($scope, $location, juiceService))
        return;

    numImagens = 1;
    numDivImagens = 1;

    numCampos = 1;
    numDivCampos = 1;

    var error = $scope.getMessage('ErrorSavingData');
    $scope.typePayment = { active: true, colorId: getRandomColor() };
    $scope.typePaymentList = [];
    $scope.allPaymentTypes = [];

    $scope.identificationTypes = [
        {
            "description": getLabel('Typed'),
            "value": 'TP'
        },
        {
            "description": getLabel('PrincipalCard'),
            "value": 'PC'
        },
        {
            "description": getLabel('ListSelection'),
            "value": 'LS'
        }
    ];

    $scope.costCenterOrigins = [
        {
            "description": getLabel('Clients.s'),
            "value": 'CLI'
        },
        {
            "description": getLabel('User.s'),
            "value": 'USU'
        },
        {
            "description": getLabel('ListSelection'),
            "value": 'SEL'
        }
    ];

    var configui = { expNoteDeb: true, required: false, gallery: false };

    changeScreenHeightSize('#divTableTypePayment');
    colorSelectorConf('colorSel', $scope.typePayment.colorId);

    if ($routeParams.typePaymentId) {
        loadTypePayment($routeParams.typePaymentId);
    } else {
        loadPaymentTypes();
    }

    function loadTypePayment(typePaymentId) {

        $http.get(config.baseUrl + "/admin/typePayment/" + typePaymentId).then(
            function (response) {
                $scope.typePayment = response.data;

                if ($scope.typePayment.colorId == null)
                    $scope.typePayment.colorId = getRandomColor();

                colorSelectorConf('colorSel', $scope.typePayment.colorId);
                prepareLists();
            },
            function (response) {
                alert($scope.getMessage('ErrorLoadData') + response);
            }
        );
    }

    function prepareLists() {
        prepareImageLists($scope.typePayment, configui, values);
        prepareFieldLists($scope.typePayment);
    }

    function loadPaymentTypes() {
        $http.get(config.baseUrl + "/admin/paymentTypeList").then(
            function (response) {
                $scope.allPaymentTypes = response.data;
                
                $scope.query = getTextTypePayment();
                $scope.optActive = getOptionTypePayment().length > 0 ? getOptionTypePayment() : 'active';
                
                filterTypePaymentByStatus();
            }, function (response) {
                alert($scope.getMessage('ErrorLoadData'));
            }
        );
    }

    $scope.saveTypePayment = function (obj) {
        if (!validateForm())
            return;

        var url = config.baseUrl + "/admin/createTypePayment";
        if (obj.typePaymentId > 0)
            url = config.baseUrl + "/admin/updateTypePayment/" + obj.typePaymentId;

        obj.images = strImages();
        obj.fields = strFields();

        if ($scope.typePayment.costCenterOrigin === "")
            $scope.typePayment.costCenterOrigin = null

        $http.put(url, obj).then(
            function (response) {
                if (response.status >= 200 && response.status <= 299) {
                    alert($scope.getMessage('DataSavedSuccessfully'));
                    refreshPage();
                    $location.path(juiceService.appUrl + 'typePayment');
                } else {
                    refreshPage();
                    alert(error + response.status + "\n" + response.message);
                }

            },
            function (response) {
                refreshPage();
                alert(error + response.status + "\n" + response.message);
            }
        );
    };

    function refreshPage() {
        $scope.typePayment = {};
    }

    $scope.filterTypePaymentByStatus = function () {
        filterTypePaymentByStatus();
    };

    function filterTypePaymentByStatus() {
        $scope.typePaymentList = $scope.allPaymentTypes.filter(
            function (event) {
                if ($scope.optActive == "all")
                    return true;
                else if ($scope.optActive == "active" && event.active == true)
                    return true;
                else if ($scope.optActive == "inactive" && event.active == false)
                    return true;

                return false;
            }
        );
        
        setOptionTypePayment($scope.optActive);
        
        if($scope.optActive == 'active')
        	document.getElementById('optActive').checked = true;
        else if($scope.optActive == 'all')
        	document.getElementById('optAll').checked = true;
        else 
        	document.getElementById('optInactive').checked = true;
        
        $scope.typePaymentList = orderObject( $scope.typePaymentList, 'description' );
        
    }

    $scope.trataAtivoInativo = function (objectType) {
        if (objectType.active == true)
            return $scope.getLabel('Active');
        else
            return $scope.getLabel('Inactive');
    };

    $scope.retornaClasse = function (objectType) {
        if (objectType.active == true)
            return 'btnativo';
        else
            return 'btninativo';
    };

    $scope.mudaSituacao = function (obj) {

        obj.active = obj.active == true ? false : true;

        $http.put(config.baseUrl + "/admin/updateTypePayment/" + obj.typePaymentId, obj).then(
            function (response) {
                alert($scope.getMessage('DataSavedSuccessfully'));
                loadPaymentTypes();
            },
            function (response) {
                alert($scope.getMessage('ErrorSavingDataWithoutParam'));
            }
        );
    };

    $scope.addDivImage = function () {
        addDivImage(configui, values);
    };

    $scope.addDivField = function () {
        addDivField();
    };
    
    $scope.setTextTypePayment = () => {
    	setTextTypePayment($scope.query);
    }
    
});