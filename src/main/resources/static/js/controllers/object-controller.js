appGIMB.controller("objectController",
	function($scope, $http, $routeParams, $location, config, juiceService, geralAPI, values) {

		$('form').attr('autocomplete', 'off');
		$('input[type=text]').attr('autocomplete', 'off');
		$('.ipt').attr('disabled', 'disabled');
		if (!controllerHeadCheck($scope, $location, juiceService))
			return;

		$("#dtInicio").datepicker(datePickerOptions);
		$("#dtFinal").datepicker(datePickerOptions);
		$scope.dtInicio = $scope.dtInicio == null ? dateToStringPeriodo(new Date(), "i") : $scope.dtInicio;
		$scope.dtFinal = $scope.dtFinal == null ? dateToStringPeriodo(new Date(), "f") : $scope.dtFinal;
		$scope.showAll = true;
		var error = $scope.getMessage('ErrorSavingData');
		var configui = { expNoteDeb: false, required:true, gallery: true};

		numImagens = 1;
		numDivImagens = 1;

		numCampos = 1;
		numDivCampos = 1;

		dictI = {};
		dictF = {};

		$('#divMenu').show();

		changeScreenHeightSize('#divTableObject');

		$scope.object = { active : true };
		$scope.objects = [];
		$scope.allObjects = [];
		$scope.customFieldList = [];
		$scope.array = [];
		$scope.listaImagensPrintLiq = [];
		
		$scope.optActive = "active";

		if ($routeParams.objectId) {
			loadObject($routeParams.objectId);
		} else {
			loadObjtecTypes();
		}

		function loadObject(objectId) {
			$http.get(config.baseUrl + "/admin/object/" + objectId).then(
				function(response) {
					$scope.object = response.data;

					$($scope.object.customFieldList).each(function (idx, el) {
						if (el.customFieldValue.indexOf("[{") != -1) {
							var arr = el.customFieldValue;
							$scope.array = JSON.parse(arr);
						} else {
							$scope.customFieldList.push(el);
						}
					});

					var count = 0;

					$($scope.object.customPictureList).each(function (idx, el) {
						if (count == 20) {
							count = 0;

							for (var i = 0; i < 4; i++) {
								$scope.listaImagensPrintLiq.push($scope.itemEmpty);
							}

							$scope.listaImagensPrintLiq.push(el);
							count++;
						} else {
							$scope.listaImagensPrintLiq.push(el);
							count++
						}
					});
                                            
				},
				function(response) {
					alert($scope.getMessage('ErrorLoadData'));
				}
			);
		}

		function loadObjtecTypes() {
			$('#modal_carregando').modal('show');
			$http.get(config.baseUrl + "/admin/object").then(
				function(response) {
					$scope.allObjects = response.data;
					
					$scope.query = getTextObject();
					$scope.optActive = getOptionObject().length > 0 ? getOptionObject() : 'active'; 
					
					filterObjectByStatus();
					$('#modal_carregando').modal('hide');
				}, function(response) {
					alert($scope.getMessage('ErrorLoadData'));
					$('#modal_carregando').modal('hide');
				}
			);
		}

		$scope.createUrlDownload = () => {

			const initDate = formataData($scope.dtInicio);
			const finalDate = formataData($scope.dtFinal);

			const domain = 'INVENTORY';

			return config.baseUrl
			+ '/export-excel/' + domain
			+ '?startDate=' + initDate
			+ '&endDate=' + finalDate
			+ '&language=' + localStorage["language"]
			+ '&groupBy='+ ($scope.showAll ? "all" : "groupBy");
		};

		$scope.saveObject = function(obj) {
			if (!validateForm())
				return;

			var url = config.baseUrl + "/admin/createObject";
			if (obj.observationId > 0)
				url = config.baseUrl + "/admin/updateObject/" + obj.objectId;
			
			
			obj.images = strImages();
			obj.fields = strFields();

			$http.put(url, obj).then(
				function(response) {
					if (response.status >= 200 && response.status <= 299) {
						alert($scope
								.getMessage('DataSavedSuccessfully'));
						refreshPage();
						$location
								.path(juiceService.appUrl + 'object');
					} else {
						refreshPage();
						alert(error + response.status + "\n" + response.message);
					}

				},
				function(response) {
					refreshPage();
					alert(error + response.status + "\n" + response.message);
				}
			);
		};

		function refreshPage() {
			$scope.object = {};
		}

		$scope.filterObjectByStatus = function() {
			filterObjectByStatus();
		};

		function filterObjectByStatus() {
			$scope.objects = $scope.allObjects.filter(isTrue);
			
			setOptionObject($scope.optActive);
			
			if($scope.optActive == 'active')
				document.getElementById('optActive').checked = true;
			else if($scope.optActive == 'inactive')
				document.getElementById('optInactive').checked = true;
			else
				document.getElementById('optAll').checked = true;
		}
					
		function isTrue(event){
			if ($scope.optActive == "all")
				return true;
			else if ($scope.optActive == "active" && event.active == true)
				return true;
			else if ($scope.optActive == "inactive" && event.active == false)
				return true;

			return false;
		}

		$scope.trataAtivoInativo = function(object) {
			if (object.active == true)
				return $scope.getLabel('Active');
			else
				return $scope.getLabel('Inactive');
		};

		$scope.retornaClasse = function(object) {
			if (object.active == true)
				return 'btnativo';
			else
				return 'btninativo';
		};

		$scope.dateTimeFormat = function (dateTime) {
			if (dateTime != null && dateTime.length > 0 && dateTime.length >= 15)
				return dateTime.substring(0, (dateTime.length - 3));

			return dateTime;
		};

		$scope.mudaSituacao = function(obj) {

			obj.active = obj.active == true ? false : true;
			
			$http.put(config.baseUrl + "/admin/updateObject/"+ obj.objectId, obj).then(
				function(response) {
					alert($scope.getMessage('DataSavedSuccessfully'));
					loadObjtecTypes();
				},
				function(response) {
					alert($scope.getMessage('ErrorSavingDataWithoutParam'));
				}
			);
		};

        $scope.setImageDetailObject = function (obj, index) {
            setImageDetailObject(obj, index, $scope.listaImagensPrintLiq);
        };

		function formataData(data) {
			var newDate = data.replace('/', '-');
			newDate = newDate.replace('/', '-');
			return newDate;
		}

        function setImageDetailObject(obj, index, list) {
            var el = $("#imageDetailObject");
            var div = $("#divModalDD");
            var imgPath = obj;
            $(".imageDetailObject").css('background-image', 'url(' + imgPath + ')');
            $("#imageDetailObject").attr("src", imgPath);

            for (var i = 0; i < list.length; i++) {
                if (list[i].picPath == obj) {
                    idxImg = i;
                    break;
                }
            }

            var r = 'rotate(0deg)';

            $(el).css({
                '-moz-transform': r,
                '-webkit-transform': r,
                '-o-transform': r,
                '-ms-transform': r
            });

            $(el).removeClass('imgDetailRotation');
            $(div).removeClass('divModalDDRotation');
        }
        
        $scope.setTextObject = () => {
        	setTextObject($scope.query);
        }
	}
);
