appGIMB.controller("dsAppointmentsChartController", function ($scope, $http, $routeParams, $location, config, juiceService, geralAPI) {

	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');
	if (!controllerHeadCheck($scope, $location, juiceService, false)) return;

	$scope.pathImg = config.pathImg;
	$('.ipt').attr('disabled', 'disabled');
	$('#divMenu').show();
	$("body").css("overflowY", "auto");
	$("#dtInicio").datepicker(datePickerOptions);
	$("#dtFinal").datepicker(datePickerOptions);

	$scope.users = []
	$scope.usersSelected = []
	$scope.usersNames = ""

	$scope.events = []
	$scope.eventsSelected = []
	$scope.eventsNames = ""

	$scope.graphType = 'bar';
	selectedGraphType = 'bar';

	$scope.listTransfers = []
	$scope.listServices = []
	filteredListUser = {}
	filteredListEvent = {}
	canRunReload = false;

	dtIni = lastUtilDay();
	dtFin = lastUtilDay();

	defaultColors = {};

	$scope.dtInicio = dtIni;
	$scope.dtFinal = dtFin;

	configUIComponents();
	loadUsers();
	loadEvents();
	loadActions();
	loadTransfer();

	function configUIComponents() {
		$('input[type=radio][id=radioGraphTypePie]').change(function () {
			$scope.graphType = 'pie';
			selectedGraphType = 'pie';
		});

		$('input[type=radio][id=radioGraphTypeBar]').change(function () {
			$scope.graphType = 'bar';
			selectedGraphType = 'bar';
		});
	}

	function loadTransfer() {
		var params = {};

		params["startDate"] = $scope.dtInicio;
		params["endDate"] = $scope.dtFinal;
		params["user"] = JSON.parse(localStorage.getItem("bobby"))

		$http({
			method: 'PUT',
			url: config.baseUrl + "/transfersByDateBetween",
			data: params
		}).then(function (response) {
			$scope.listTransfers = response.data;

			loadServices();
		}, function (response) {
			alert($scope.getMessage('ErrorLoadingServices') + response.message);
		});
	}

	function loadServices() {
		$('#modal_carregando').modal('show');
		

		var params = {};
		params["startDate"] = $scope.dtInicio;
		params["endDate"] = $scope.dtFinal;
		params["user"] = JSON.parse(localStorage.getItem("bobby"))

		$http({
			method: 'PUT',
			url: config.baseUrl + "/servicoWeb",
			data: params
		}).then(function (response) {
			$scope.listServices = response.data;

			filterList();
			createCompareUserCharts();
			createChartsByUser();

			canRunReload = true;

			$('#modal_carregando').modal('hide');
		}, function (response) {
			alert($scope.getMessage('ErrorLoadingServices') + response.message);

			$('#modal_carregando').modal('hide');
		});
	}

	function loadUsers() {
		$http.get(config.baseUrl + "/admin/user")
			.then(function (response) {
				$scope.users = response.data;
				$scope.users.sort((a, b) => a.name.toLowerCase().localeCompare(b.name.toLowerCase()));

				for (user of $scope.users)
					defaultColors[user.name] = user.colorId == null ? hexToRgbA(getRandomColor(), 0.7) : hexToRgbA(user.colorId, 0.7);

			}, function (error) {
				alert($scope.getMessage('ErrorLoadingUsers'));
			});
	}

	function loadEvents() {
		$http.get(config.baseUrl + "/admin/event")
			.then(function (response) {
				$scope.events = response.data;
				$scope.events.sort((a, b) => a.description.toLowerCase().localeCompare(b.description.toLowerCase()));

				for (event of $scope.events)
					defaultColors[event.description] = event.colorId == null ? hexToRgbA(getRandomColor(), 0.7) : hexToRgbA(event.colorId, 0.7);

			}, function (response) {
				alert($scope.getMessage('ErrorLoadingEvents'));
			});
	}

	function loadActions() {
		$http.get(config.baseUrl + "/admin/action")
			.then(function (response) {
				$scope.actions = response.data;
				$scope.actions.sort((a, b) => a.description.toLowerCase().localeCompare(b.description.toLowerCase()));

				for (action of $scope.actions)
					defaultColors[action.description] = action.colorId == null ? hexToRgbA(getRandomColor(), 0.7) : hexToRgbA(action.colorId, 0.7);

			}, function (response) {
				alert($scope.getMessage('ErrorLoadAction.p'));
			});
	}

	function filterList() {
		function filter(object) {
			/*
			 * Filtro generico, filtra tanto lista de transfer quanto lista de services
			 * A idententificação de cada tipo é feito pelo id
			 * Ex:
			 * 	object == transfer quando existe transferId
			 * 	object == service quando existe serviceId
			 * */
			if ($scope.usersSelected.length > 0) {
				filtered = $scope.usersSelected.filter(function (user) {
					// object.user - igual para service e transfer
					return (user.userId == object.user.userId)
				})

				if (filtered.length == 0) return false;
			}

			if ($scope.eventsSelected.length > 0) {
				// Filtro de event feito somento quando object == transfer
				if (object.transferId != null) {
					object.eventsList = object.eventsList.filter(function (eventItem) {
						for (event of $scope.eventsSelected) {
							if (eventItem.event.eventId == event.eventId)
								return true
						}
					});

					if (object.eventsList == null || object.eventsList.length == 0) return false;
				}
			}

			return true;
		}

		$scope.usersNames = ""
		$scope.eventsNames = ""
		fillLabelsParameters()

		filteredListUser = {}
		filteredListEvent = {}

		// filtering transfer data (to get event time) 
		if ($scope.listTransfers != null) {
			preFilteredList = {};
			filtred = $scope.listTransfers.filter(filter);

			// verifica divergencia
			for (transfer of filtred) {

				var evemillis = 0;
				if (transfer.eventsList != null) {
					for (event of transfer.eventsList)
						evemillis += event.timeElapsedMilli;
				}
				if ($scope.listServices != null) {
					services = $scope.listServices.filter(filter);
					for (service of services) {
						if(moment(service.startTime).isAfter(moment(transfer.startTime)) && moment(service.endTime).isBefore(moment(transfer.endTime)) && service.user.userId === transfer.user.userId)
							evemillis += service.timeElapsedMilli
					}
				}

				diff = transfer.timeElapsedMilli - evemillis
				if (diff > 0) {
					transfEvent = { event: { 'description': $scope.getLabel('NoAppointment') }, 'timeElapsedMilli': diff }
					defaultColors[$scope.getLabel('NoAppointment')] = hexToRgbA('#FF0000', 0.7)
					transfer.eventsList.push(transfEvent);
				}
			}
			// fim verifica divergencia

			for (transfer of filtred) {
				lst = preFilteredList[transfer.user.name];
				if (lst == null) lst = [];
				lst.push(transfer);
				preFilteredList[transfer.user.name] = lst;
			}

			// sum time
			keys = Object.keys(preFilteredList).sort((a, b) => a.toLowerCase().localeCompare(b.toLowerCase()));
			for (key of keys) {
				eventsTime = [];
				lst = preFilteredList[key];
				if (lst != null) {
					for (transfer of lst) {
						if (transfer.eventsList != null) {
							for (event of transfer.eventsList) {
								time = eventsTime[event.event.description];
								if (time == null) time = 0
								time += (((event.timeElapsedMilli / 1000) / 60) / 60);
								eventsTime[event.event.description] = Math.round(time * 100) / 100;

								timeGeneral = filteredListEvent[event.event.description];
								if (timeGeneral == null) timeGeneral = 0
								timeGeneral += (((event.timeElapsedMilli / 1000) / 60) / 60);
								filteredListEvent[event.event.description] = Math.round(timeGeneral * 100) / 100;
							}
						}
					}
				}

				filteredListUser[key] = eventsTime;
			}
		}

		// filtering services data (to get time of actions)
		if ($scope.listServices != null) {
			preFilteredList = {};
			filtred = $scope.listServices.filter(filter);
			for (service of filtred) {
				lst = preFilteredList[service.user.name];
				if (lst == null) lst = [];
				lst.push(service);
				preFilteredList[service.user.name] = lst;
			}

			// sum time
			keys = Object.keys(preFilteredList).sort((a, b) => a.toLowerCase().localeCompare(b.toLowerCase()));
			for (key of keys) {
				actionsTime = [];
				lst = preFilteredList[key];
				if (lst != null) {
					for (service of lst) {
						time = actionsTime[service.action.description];
						if (time == null) time = 0
						time += (((service.timeElapsedMilli / 1000) / 60) / 60);
						actionsTime[service.action.description] = Math.round(time * 100) / 100;

						timeGeneral = filteredListEvent[service.action.description];
						if (timeGeneral == null) timeGeneral = 0
						timeGeneral += (((service.timeElapsedMilli / 1000) / 60) / 60);
						filteredListEvent[service.action.description] = Math.round(timeGeneral * 100) / 100;

					}
				}

				for (action of Object.keys(actionsTime)) {
					if (filteredListUser[key] == null) filteredListUser[key] = {};
					filteredListUser[key][action] = actionsTime[action];
				}
			}
		}

		//sorting all values by description
		for (user of Object.keys(filteredListUser)) {
			times = filteredListUser[user];
			filteredListUser[user] = {};

			for (desc of Object.keys(times).sort((d1, d2) => d1.localeCompare(d2)))
				filteredListUser[user][desc] = times[desc];
		}

		times = filteredListEvent;
		filteredListEvent = {};
		for (desc of Object.keys(times).sort((d1, d2) => d1.localeCompare(d2)))
			filteredListEvent[desc] = times[desc];
	}

	function createCompareUserCharts() {
		div = document.getElementById('generalChartContainer');
		div.innerHTML = '';

		div.innerHTML += getHTMLCharCanvas('compare', $scope.getLabel('COMPARATIVE'), 12);

		datasets = [];
		labels = [];
		for (key of Object.keys(filteredListUser)) {
			eventsTime = filteredListUser[key];
			for (dk of Object.keys(eventsTime))
				if (!labels.includes(dk)) labels.push(dk);
		}

		labels = labels.sort((l1, l2) => l1.localeCompare(l2));

		for (key of Object.keys(filteredListUser)) {
			eventsTime = filteredListUser[key];

			dataValues = [];
			for (label of labels)
				dataValues.push(eventsTime[label] != null ? eventsTime[label] : 0);

			color = defaultColors[key];
			datasets.push({ label: key, data: dataValues, backgroundColor: color, borderColor: color, fill: false, datalabels: { align: 'end', anchor: 'end' } })
		}

		var data = {
			labels: labels,
			datasets: datasets
		}

		context = document.getElementById('chart' + 'compare').getContext('2d');
		chart = new Chart(context, {
			type: 'line',
			options: {
				plugins: {
					datalabels: {
						color: 'black',
						font: { weight: 'bold' },
						display: function (context, value) {
							return context.dataset.data[context.dataIndex] >= 1;
						},
						formatter: function (value, context) {
							var lb = formatterHourGraphLabel(value, context);
							return lb;
						}
					}
				},
				tooltips: {
					custom: function (tooltip) {
						//tooltip.x = 0;
						//tooltip.y = 0;
					},
					mode: 'single',
					callbacks: {
						label: function (tooltipItems, data) {
							return formatterHourGraphLabel(data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index], chart.controller);
						},
						beforeLabel: function (tooltipItems, data) {
							return percChart(data.datasets, tooltipItems.datasetIndex, tooltipItems.index);
						}
					}
				}
			}
		});
		labelFormatter = 'HOUR';
		chart['labelFormatter'] = labelFormatter;
		chart.data = data;
		chart.options.legend.display = true
		chart.options.scales.xAxes[0].display = true;
		chart.options.scales.xAxes[0].ticks.autoSkip = false;
		chart.options.scales.yAxes[0].ticks.beginAtZero = false;
		chart.options.scales.xAxes[0].ticks.major.autoSkip = false;
		chart.options.scales.xAxes[0].ticks.minor.autoSkip = false;
		chart.update();

		setChartData('chart' + 'compare', { 'data': data, 'tittle': $scope.getLabel('COMPARATIVE'), 'labelFormatter': labelFormatter, type: 'line' });


	}

	function createChartsByUser() {
		div = document.getElementById('chartsContainer');
		div.innerHTML = '';

		var graphSize = 4;
		if (selectedGraphType == 'bar')
			graphSize = 6;

		keys = Object.keys(filteredListUser).sort((a, b) => a.toLowerCase().localeCompare(b.toLowerCase()));
		keys.unshift($scope.getLabel('SUMMARYBYEVENT'));
		idx = 1;
		for (key of keys) {
			div.innerHTML += getHTMLCharCanvas(idx, key, graphSize);
			idx++
		}

		configChartsEvents(document)

		eventsColor = {};

		for (var i = 1; i < idx; i++) {
			dataValues = [];
			dataKeys = [];

			if (i == 1) {
				dataValues = Object.values(filteredListEvent);
				dataKeys = Object.keys(filteredListEvent);
			}
			else {
				eventsTime = filteredListUser[keys[i - 1]];
				dataValues = Object.values(eventsTime);
				dataKeys = Object.keys(eventsTime);
			}

			colors = [];
			for (let i = 0; i < dataKeys.length; i++) {
				color = defaultColors[dataKeys[i]];
				colors.push(color);
			}


			datalabelsConf = { align: 'end', anchor: 'start' };
			if (selectedGraphType == 'pie') datalabelsConf = { align: 'start', anchor: 'end' };

			var data = {
				labels: dataKeys,
				datasets: [{
					data: dataValues,
					backgroundColor: colors,
					datalabels: datalabelsConf
				}]
			}

			context = document.getElementById('chart' + i).getContext('2d');
			chart = new Chart(context, {
				type: selectedGraphType,
				options: {
					plugins: {
						datalabels: {
							color: (selectedGraphType == 'pie') ? 'white' : 'black',
							display: function (context) {
								return true;
							},
							font: {
								weight: 'bold'
							},
							formatter: function (value, context) {
								var lb = formatterHourGraphLabel(value, context) + '\n' + percChart(context.dataset, context.datasetIndex, context.dataIndex);
								return lb;

							},
							clip: true,
							tittle: false
						}
					},
					tooltips: {
						custom: function (tooltip) {
							//tooltip.x = 0;
							//tooltip.y = 0;
						},
						mode: 'single',
						callbacks: {
							label: function (tooltipItems, data) {
								return formatterHourGraphLabel(data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index], chart.controller);
							},
							beforeLabel: function (tooltipItems, data) {
								return percChart(data.datasets, tooltipItems.datasetIndex, tooltipItems.index);
							}
						}
					}
				}
			});
			labelFormatter = 'HOUR';
			chart['labelFormatter'] = labelFormatter;
			chart.data = data;
			chart.options.legend.display = false;
			if (selectedGraphType == 'bar') {
				chart.options.scales.xAxes[0].display = false;
				chart.options.scales.yAxes[0].ticks.beginAtZero = true;
			}

			chart.update()

			setChartData('chart' + i, { 'data': data, 'tittle': keys[i - 1], 'labelFormatter': labelFormatter, type: selectedGraphType });
		}
	}

	function fillLabelsParameters() {
		if ($scope.usersSelected.length > 0) {
			for (i in $scope.usersSelected) {
				user = $scope.usersSelected[i]
				if (!$scope.usersNames.includes(user.name))
					$scope.usersNames += $scope.usersNames.length == 0 ? user.name : ", " + user.name
			}
		}
		else {
			$scope.usersNames = ""
		}

		if ($scope.eventsSelected.length > 0) {
			for (i in $scope.eventsSelected) {
				object = $scope.eventsSelected[i]
				if (!$scope.eventsNames.includes(object.description))
					$scope.eventsNames += $scope.eventsNames.length == 0 ? object.description : ", " + object.description
			}
		}
		else {
			$scope.eventsNames = ""
		}
	}

	$scope.reload = function () {
		if (canRunReload) {
			setDataInicioFiltroOS(document.getElementById("dtInicio").value);
			setDataFinalFiltroOS(document.getElementById("dtFinal").value);

			if ($scope.showUsers)
				$scope.showUsersSelect()

			if ($scope.showEvents)
				$scope.showEventsSelect()

			$("#parametersModal").modal('hide')
			loadTransfer();
		}
	}

	$scope.changeGrapthType = function (event) {
		selectedGraphType = $scope.grapthType;
	}

	$scope.showModalWindow = function () {
		$("#parametersModal").modal({ backdrop: 'static', keyboard: false }).unbind('click')
	}

	$scope.printChart = function (charId) {
		var img = document.createElement('img');
		img.src = document.getElementById(charId).toDataURL();

		var div = document.getElementById('printChart');
		div.innerHTML = '';
		div.appendChild(img);

		setTimeout(function () {
			var e = document.getElementById('btnPrinterChart');
			e.click();
		}, 500)
	}

	$scope.downloadChart = function (charId) {
		var dwn = document.getElementById('dwnPrinterChart');
		dwn.href = document.getElementById(charId).toDataURL();
		dwn.click();
	}

	$scope.showUsersSelect = function () {
		$scope.showUsers = $scope.showUsers ? false : true

		if ($scope.showUsers)
			$("#userTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up')
		else
			$("#userTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down')
	}

	$scope.showEventsSelect = function () {
		$scope.showEvents = $scope.showEvents ? false : true
	}

});