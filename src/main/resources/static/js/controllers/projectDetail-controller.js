appGIMB.controller("projectDetailController", function ($scope, $http, $routeParams, geralAPI, juiceService, config, $location, $compile) {

	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');
	if (!controllerHeadCheck($scope, $location, juiceService)) return;

	$('#divMenu').show();

	$("#dtInicio").datepicker(datePickerOptions);
	$("#dtInicioFiltered").datepicker(datePickerOptions);
	$("#dtFinalFiltered").datepicker(datePickerOptions);
	
	$scope.project = {
		durationType: 'D',
		active: true
	};
	
	$scope.newEquipment = {};
	$scope.equipmentsListToSelectInModal = [];
	$scope.clients = [];
	$scope.services = [];
	$scope.initialDateFiltered = dateToStringPeriodo(today, "i");
	$scope.finalDateFiltered = dateToStringPeriodo(today, "f");
	$scope.urlToExtractProjectInExcel = config.baseUrl + '/export-excel/SERVICE?&language=' + localStorage["language"]
										+ '&projectId='+ $routeParams.projectId;
	$scope.areas = [];
	
	var equipmentAdded = [];
	var today = new Date();
	var selFldId = 0;
	
	function showModal(value) {
		$('#modal_carregando').modal(value ? 'show' : 'hide');
	}
	
	if($routeParams.projectId){
		loadData($routeParams.projectId);
	} else {
		loadData(null);
	}
	
	async function loadData(projectId){
		showModal(true);
		
		var responseProject = null;
		var responseClient = null;
		var responseArea = null;
		
		try {
			
			if(projectId != null){
				responseProject = await $http.get(`${config.baseUrl}/admin/projects/${projectId}`);
			}
			
			responseClient = await $http.get(`${config.baseUrl}/admin/clientByActive/${true}`);
			responseArea = await $http.get(`${config.baseUrl}/admin//areaActive/${true}`);
			
		} catch(error){
			alert($scope.getMessage('ErrorLoadData'));
		} finally {
			
			 (responseClient.status < 400)
				? $scope.clients = responseClient.data
				: alert($scope.getMessage('ErrorLoadData'));
			
			(responseArea.status < 400)
				? $scope.areas = responseArea.data
			 	: alert($scope.getMessage('ErrorLoadData'));
				
			
			if (responseProject != null){
				if(responseProject.status < 400){
					$scope.project = responseProject.data;
					
					if($scope.project.equipment == undefined || $scope.project.equipment == null){					
						$scope.project.equipment = '';
					}
					else {
						assemblesTheEquipmentDiv();
						for(equipment of JSON.parse($scope.project.equipment)){						
							addEquipmentToTheProject(equipment);
						}
					}
					
					getServicesByProject($scope.project);
					changeSelectedClient();
					
				}else {
					alert($scope.getMessage('ErrorLoadData'))
				}
			}
			
			getTotalInstallationAndInstallPerDay();
			
			showModal(false);
		}
	}
	
	function getTotalInstallationAndInstallPerDay(){
		$scope.project.totalInstallations = getTotalInstallations($scope.project);
		$scope.project.installTargetPerDay = getInstallTargetPerDay($scope.project);
	}
	
	function fillListsEquipment(client){
		
		const list = client.vehicles;
		for(vehicle of list){
			const e = {};
			e['EQUIPMENTID'] = vehicle.equipment.equipmentId;
			e['EQUIPMENTAMOUNT'] = 1;
			e['DESCRIPTION'] = vehicle.equipment.description;
			
			if(!containsObjectInProject(e) && !containsObjectInListToSelect(e)){
				$scope.equipmentsListToSelectInModal.push(e);
			}
		}	 
	}
	
	function containsObjectInProject(e){
		if($scope.project.equipment == undefined || $scope.project.equipment == null || $scope.project.equipment == "[]"){
			return false;
		}
		
		const list = JSON.parse($scope.project.equipment);
		
		for(equipment of list){
			if(e['EQUIPMENTID'] == equipment['EQUIPMENTID'] 
			   && e['DESCRIPTION'] == equipment['DESCRIPTION']){
				return true;
			}
		}
		
		return false;
	}
	
	function containsObjectInListToSelect(e){
		const list = $scope.equipmentsListToSelectInModal;
			
		for(equipment of list){
			if(e['EQUIPMENTID'] == equipment['EQUIPMENTID'] 
			   && e['DESCRIPTION'] == equipment['DESCRIPTION']){
				return true;
			}
		}
		
		return false;
	}
	
	function saveProject(project, load) {
		showModal(true);
		
		project.durationType = document.getElementById('durationType').value;
		project.equipment = equipmentObjectToString();
		
		$http.post(config.baseUrl + '/admin/projects', project).then(
				function(response) {
					showModal(false);
					
					if(load){
						alert($scope.getMessage('DataSavedSuccessfully'));
						window.location.href = config.baseUrl + '/projectDetail/' + response.data.projectId;
					}
				},
				function(response) {
					showModal(true);
					alert($scope.getMessage('ErrorLoadData'))
				}
		);
	}
	
	$scope.saveProject = (project) => {
		saveProject(project, true);
	}
	
	function addDivEquipment(equipment){
		selFldId++;
		const idDiv = 'idDiv' + selFldId;
		const idEquipmentDiv = 'idEquipmentDiv' + selFldId;
		const idEquipmentAmount = 'idEquipmentAmount' + selFldId;
		const idButtonRemoveEquipment = 'idButtonRemoveEquipment' + selFldId;
		
		const description = equipment['DESCRIPTION'];
		const ngmodelToInputNumber = '$scope.amount' + selFldId + ' = equipment["EQUIPMENTAMOUNT"];';
		eval(ngmodelToInputNumber);
		
		var $str = $('<div class=\"row\" id="'+idDiv+'">'+
        	'   <div class=\"col-sm-5\">'+
	        '	   <div class=\"col-sm-2\" style=\"width: 300px\"> ' +
	        '		   <input id=\"' + idEquipmentDiv + '\"  class=\"formGroupInput form-control text-box single-line\" autocomplete=\"off\" style=\"max-width: 300px; background-color: white; color: black\"' +
	        '					value="'+description+'" readonly/> ' +
	        '	   </div> ' +
	        '	   <input type=\"number\" id=\"' + idEquipmentAmount + '\" class=\"formGroupInput form-control\" min="1" ' + 
	        '		   style=\"max-width: 100px; background-color: white; color: black\" ng-change="changeAmountEquipment('+selFldId+')" ng-model="amount'+selFldId+'" min="1"/>' +
	        '   </div> ' +
		  
	        '   <div class=\"col-sm-2\" style=\"height: 34px; padding-left: 0px\"> ' +
	        '	   <button id=\"' + idButtonRemoveEquipment + '\" class=\"formGroupInput btn-sm btn-danger\" tabIndex=\"-1\" ng-click="removeEquipment('+selFldId+')">-</button>' +
	        '   </div> ' +
	        '</div>').appendTo('#pnlListEquipments');
		
		$compile($str)($scope);
	}
	
	$scope.addNewEquipment = (newEquipment) => {
		
		const equipment = returnEquipmentById(newEquipment['EQUIPMENTID'], newEquipment['EQUIPMENTAMOUNT']);
		(equipment == null) 
			? alert($scope.getMessage('ErrorLoadData')) 
			: newEquipment = equipment;
		
		const wasRemoved = removeEquipmentFromTheEquipmentList(newEquipment['EQUIPMENTID']);
		(wasRemoved)
			? console.log('removido')
			: alert($scope.getMessage('ErrorLoadData'));	
			
		addDivEquipment(newEquipment);
			
		cleanModalAddEquipment();
		addEquipmentToTheProject(newEquipment);
		
		if($scope.project.projectAvailableDays != null 
				&& $scope.project.projectAvailableDays != undefined
				&& $scope.project.projectAvailableDays > 0) {
			
			getTotalInstallationAndInstallPerDay();
		}
		
		if($scope.project.projectId != null && $scope.project.projectId != undefined){
			saveProject($scope.project, false);
		}
		
		$scope.newEquipment = {};
	}
	
	function returnEquipmentById(newEquipmentId, newEquipmentAmount){
		for(equipment of $scope.equipmentsListToSelectInModal){
			if(equipment['EQUIPMENTID'] == parseInt(newEquipmentId)){
				if(newEquipmentAmount != null){
					equipment['EQUIPMENTAMOUNT'] = newEquipmentAmount;
				}
				return equipment; 
			}
		}
		
		return null;
	}
	
	function removeEquipmentFromTheEquipmentList(equipmentId) {
		for(var i = 0; i<$scope.equipmentsListToSelectInModal.length; i++){
			const equipment = $scope.equipmentsListToSelectInModal[i];
			if(equipment['EQUIPMENTID'] == equipmentId){
				$scope.equipmentsListToSelectInModal.splice(i, 1);
				return true; 
			}
		}
		
		return false;
	}
	
	function cleanModalAddEquipment(){
		document.getElementById('addEquipment').value = '';
		document.getElementById('addEquipmentAmount').value = '';
	}
	
	$scope.removeEquipment = (index) => {
		const idEquipmentDiv = 'idEquipmentDiv' + index;
		const idDiv = 'idDiv' + index;
		
		const equipmentDescription = document.getElementById(idEquipmentDiv).value;
		const equipment = returnEquipmentByDescription(equipmentDescription);
		
		if(equipment == null){
			alert($scope.getMessage('ErrorLoadData'));
		}
		else {
			addEquipmentToEquipmentsListToSelectInModal(equipment);
			removeDivFromPnlListEquipments(idDiv);
			removeEquipmentFromTheListOfEquipmentAdded(equipment);
		}
		
		if($scope.project.projectId != null && $scope.project.projectId != undefined){
			saveProject($scope.project, false);
		}
		
		getTotalInstallationAndInstallPerDay();
	}
	
	function returnEquipmentByDescription(equipmentDescription){
		for(equipment of equipmentAdded){
			if(equipment['DESCRIPTION'] == equipmentDescription){
				return equipment;
			} else {
				continue;
			}
		}
		
		return null;
	}
	
	function addEquipmentToEquipmentsListToSelectInModal(equipment){
		$scope.equipmentsListToSelectInModal.push(equipment);
	}
	
	function removeDivFromPnlListEquipments(divFromRemove){
		document.getElementById(divFromRemove).parentNode.removeChild(document.getElementById(divFromRemove));
	}
	
	function assemblesTheEquipmentDiv() {
		const equipmentList = JSON.parse($scope.project.equipment);
		
		for(equipment of equipmentList){			
			addDivEquipment(equipment);
		}
	}
	
	function addEquipmentToTheProject(equipment){
		var equipmentForAdded = {};
		equipmentForAdded['EQUIPMENTID'] = equipment['EQUIPMENTID'];
		equipmentForAdded['EQUIPMENTAMOUNT'] = equipment['EQUIPMENTAMOUNT'];
		equipmentForAdded['DESCRIPTION'] = equipment['DESCRIPTION'];
		
		equipmentAdded.push(equipment);
	}
	
	$scope.changeAmountEquipment = (selFldId) => {
		const idToTheDiv = 'idEquipmentAmount' + selFldId;
		const amountEquipment = parseInt(document.getElementById(idToTheDiv).value);
		
		const idEquipmentDiv = 'idEquipmentDiv' + selFldId;
		const descriptionEquipment = document.getElementById(idEquipmentDiv).value;
		const equipment = returnEquipmentByDescription(descriptionEquipment);
		
		changeEquipmentInTheEquipmentAdded(equipment['EQUIPMENTID'], amountEquipment);
		getTotalInstallationAndInstallPerDay();
	}
	
	function changeEquipmentInTheEquipmentAdded(id, amountEquipment){
		for(var i=0; i< equipmentAdded.length; i++){
			const equipment = equipmentAdded[i];
			if(equipment['EQUIPMENTID'] == id){
				equipmentAdded[i]['EQUIPMENTAMOUNT'] = amountEquipment;
				break;
			}
		}
	}
	
	function equipmentObjectToString(){
		const equipmentReturn = JSON.stringify(equipmentAdded);
		return equipmentReturn;
	}
	
	function removeEquipmentFromTheListOfEquipmentAdded(equipment){
		for(var i =0; i< equipmentAdded.length; i++){
			const equipmentFromAdded = equipmentAdded[i];
			
			if(equipmentFromAdded['EQUIPMENTID'] == equipment['EQUIPMENTID']){
				equipmentAdded.splice(i, 1);
				break;
			}
		}
	}
	
	function getTotalInstallations(project){
		var amount = 0;
		for(equipment of equipmentAdded){
			amount += equipment['EQUIPMENTAMOUNT'];
		}
		
		return amount;
	}
	
	function getInstallTargetPerDay(project){
		const amount = getTotalInstallations(project);
		const targetPerDay = amount / project.projectAvailableDays;
		
		if(project.projectAvailableDays == undefined){
			return 0;
		}
			
		return parseFloat(targetPerDay.toFixed(2));
	}
	
	$scope.changeAvailableDays = (availableDays) => {
		const duration = durationInDays();
		
		if (availableDays > duration){
			alert($scope.getMessage("dayLimitExceeded"));
			$scope.project.projectAvailableDays = duration;
		}
		
		if(equipmentAdded.length > 0){
			getTotalInstallationAndInstallPerDay();
		}
	}
	
	$scope.changeDuration = () => {
		if($scope.project.initialDate != null 
				&& $scope.project.initialDate != undefined
				&& $scope.project.initialDate != ""){
			
			$scope.project.endDate = getEndDate();
		}
	}
	
	function durationInDays() {
		if($scope.project.durationType == "D"){
			return $scope.project.projectTime;
		}
		else if($scope.project.durationType == "M"){
			return 30 * $scope.project.projectTime;
		}
		else {
			return 7 * $scope.project.projectTime;
		}
	}
	
	function getEndDate() {
		const initialDate = $scope.project.initialDate;
		const dayMonthYearString = $scope.project.initialDate.split("/");
		var dayMonthYear = [];
		
		for(var i=0; i<dayMonthYearString.length; i++){
			dayMonthYear[i] = parseInt(dayMonthYearString[i]);
		}
		
		const duration = durationInDays();
		var day = dayMonthYear[0];
		var month = dayMonthYear[1];
		var year = dayMonthYear[2];
		
		for(var i=0; i<duration; i++){
			day ++;
			
			const amountDay = amountDayOfMonth(month, year);
			
			if(day > amountDay){
				month ++;
				day = 1;
			}
			
			if( month > 12){
				year ++;
				month = 1;
			}
		}
		
		if ( day < 10 ) {
			day = '0' + day;
		}
		
		if ( month < 10 ){
			month = '0' + month;
		}
		
		const finalDateString = day + "/" + month + "/" + year;
		
		return finalDateString;
	}
	
	function amountDayOfMonth(month, year) {
		var days = [30, 31];
		
		if(month == 2) {
			//verifica se o ano é bissexto
			//sao bissextos todos os anos divisíveis por 4, excluindo os que sejam divisíveis por 100, porém não os que sejam divisíveis por 400.
			return ((year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0))) ? 29 : 28;
		} else {
			return days[(month + (month < 7 ? 1 : 0)) % 2];
		}
	}
	
	function getServicesByProject(project) {
		$http.put(config.baseUrl + '/findByProject', project).then(
			function(response) {
				
				if(response.data.length > 0){
					for (service of response.data){
						if(service.status != 'O' && service.status != 'A' && service.status != 'P'){
							if(service.statusCheckList != null && service.statusCheckList){
								$scope.services.push(service);
							} else if (service.statusCheckList == null){
								$scope.services.push(service);
							}
						}
					}
				}
				
				buildGraphGeneral('chartGeneral');
				buildGraphByPeriod('chartFiltered', 'chartToInnerHtml');
				buildGraphVehicle('chartVehicles');
			},
			function(response) {
				alert($scope.getMessage('ErrorLoadData'));
			}
		); 
	}
	
	function buildGraphGeneral(id) {
		const delivered = $scope.services.length;
		const pendding = getTotalInstallations($scope.project) - delivered;
		
		const context = document.getElementById(id);
		const chart = new Chart(context, {
			type: 'pie',
			data: {
				labels: [$scope.getLabel('delivered'), $scope.getLabel('Pendente')],
				datasets: [{
					data: [delivered, pendding],
					backgroundColor: ["rgba(0, 255, 0, 0.3)", "rgba(255, 255, 0, 0.3)"],
					borderColor: ["rgba(0, 255, 0, 0.9)", "rgba(255, 255, 0, 0.9)"],
					borderWidth: 1
				}],	
			}
		});
	}
	
	function buildGraphByPeriod(idCanvas, idDiv) {
		var width = "800";
		var height = "450";
		
		if(idDiv == 'divChartFiltered'){
			width = "100%";
			height = "100%";
		}
		
		document.getElementById(idDiv).innerHTML = '';
		document.getElementById(idDiv).innerHTML = '<canvas id="'+idCanvas+'" width="'+width+'" height="'+height+'"></canvas>'
		
		const dtInitial = $scope.initialDateFiltered;
		const dtFinal = $scope.finalDateFiltered;
		var servicesFiltered = [];
		
		for(service of $scope.services){
			if(formmatDateToFiltered(service.startTime) >= formmatDateToFiltered(dtInitial) && formmatDateToFiltered(service.endTime) <= formmatDateToFiltered(dtFinal)){
				servicesFiltered.push(service);
			}
		}
		
		const delivered = servicesFiltered.length;
		const pendding = getTotalInstallations($scope.project) - delivered;
		
		const context = document.getElementById(idCanvas);
		const chart = new Chart(context, {
			type: 'pie',
			data: {
				labels: [$scope.getLabel('delivered'), $scope.getLabel('Pendente')],
				datasets: [{
					data: [delivered, pendding],
					backgroundColor: ["rgba(0, 255, 0, 0.3)", "rgba(255, 255, 0, 0.3)"],
					borderColor: ["rgba(0, 255, 0, 0.9)", "rgba(255, 255, 0, 0.9)"],
					borderWidth: 1
				}],	
			}
		});
	}
	
	function buildGraphVehicle(id) {
		const dataEquipment = getEquipment($scope.services);
		
		const context = document.getElementById(id);
		const chart = new Chart(context, {
			type: 'bar',
			data: dataEquipment,
			options: {
		        scales: {
		            yAxes: [{
		                ticks: {
		                    beginAtZero: true
		                }
		            }]
		        }
		    }
		});
	}
	
	function getEquipment(services){
		var labels = []; 
		var datasets = [];
		var dataDelivered = [];
		var dataPending = [];
		
		for(var i=0; i<equipmentAdded.length; i++){
			const equipment =  equipmentAdded[i];
			
			labels.push(equipment['DESCRIPTION'])
			
			var delivered = 0;
			
			for(service of services){
				if(service.vehicle.equipment.equipmentId == equipment['EQUIPMENTID']) {
					delivered ++;
				}
			}
			
			dataDelivered[i] = delivered;
			dataPending[i] = equipment['EQUIPMENTAMOUNT'] - delivered;
		}
		
		var objDelivered = {
				label: $scope.getLabel('delivered'),
				data: dataDelivered,
				backgroundColor: 'rgba(0, 255, 0, 0.3)',
				borderColor: 'rgba(0, 255, 0, 0.9)',
				borderWidth: 1
		};
		
		var objPending = {
				label: $scope.getLabel('Pendente'),
				data: dataPending,
				backgroundColor: 'rgba(255, 255, 0, 0.3)',
				borderColor: 'rgba(255, 255, 0, 0.9)',
				borderWidth: 1
		};
		
		datasets = [objDelivered, objPending];
		
		return { labels, datasets };
	}
	
	$scope.openModal = (option) => {
		
		if(option == 1){
			$("#modalGraphGeneral").modal({
			    show: true
			});
			
			buildGraphGeneral('modalChartGeneral');
		}
		
		else if(option == 3){
			$("#modalGraphVehicle").modal({
			    show: true
			});
			
			buildGraphVehicle('modalChartVehicle');
		}
		
		else {
			$("#modalGraphFiltered").modal({
			    show: true
			});
			
			buildGraphByPeriod('modalChartFiltered', 'divChartFiltered');
		}
	}
	
	function formmatDateToFiltered(date) {
		const d = date.split('/');
		const d_ = d[2].split(' '); 
		const dateToReturn = new Date(d_[0] ,  d[1] - 1 , d[0]); 
		return dateToReturn;
	}
	
	$scope.changeDateFiltered = () => {
		var width =  document.getElementById('modalChartFiltered').style.width;
		var height =  document.getElementById('modalChartFiltered').style.height;
		
		width = width.replace(/[^\d]+/g, '')
		height = height.replace(/[^\d]+/g, '')
		
		document.getElementById('modalChartFiltered').getContext('2d').clearRect(0, 0, width, height);
		
		buildGraphByPeriod('modalChartFiltered', 'divChartFiltered');
		buildGraphByPeriod('chartFiltered', 'chartToInnerHtml');
	}
	
	$scope.changeSelectedClient = () => {
		changeSelectedClient();
	}
	
	function changeSelectedClient() {
		$scope.equipmentsListToSelectInModal = []
		const client = returnClientById($scope.project.client.clientId);
		fillListsEquipment(client);
	}
	
	function returnClientById(clientId){
		var clients = $scope.clients;
		
		for(client of clients) {
			if(client.clientId == clientId){
				return client; 
			}
		}
		
		return null;
	}

});
