appGIMB.controller("companyController", function($scope, $http, $routeParams, $location, config, juiceService, geralAPI, $q) {
	
	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');
	if (!controllerHeadCheck($scope, $location, juiceService)) 
		return;
	
	$scope.company = {};
	var iconAddLogo = 'apiGIMB/view/Company/button_add_logo.png' 
	$scope.srcLogo = iconAddLogo;
	document.getElementById("addLogoMessage").style.visibility = "visible";
	
	var image = '';
	
	loadCompany();
	
	function loadCompany() {
		$('#modal_carregando').modal('show');
		
		var url = config.baseUrl + '/company/' + '1';
		
		$http.get(url).then(function(response){
			$scope.company = response.data;
			
			if($scope.company.logoName != null && $scope.company.logoName.length > 0)
				$scope.srcLogo = getLogo();
			else
				$('#modal_carregando').modal('hide');
			
		}, function(response) {
			alert($scope.getMessage('ErrorLoadData'));
		});
	}
	
	$scope.saveCompany = function(company){
		$('#modal_carregando').modal('show');
		
		var urlSave = config.baseUrl + '/saveCompany';
		
		image = document.getElementById('logoImg');
		var addLogo = config.baseUrl+iconAddLogo.substr(iconAddLogo.indexOf('apiGIMB')+7, iconAddLogo.length);
		
		if(image.src !=  addLogo){
			if (image.src != null && image.src.length > 0)
				company.logoBase64 = image.src.substr(image.src.indexOf('base64,')+7, image.src.length);
			
			if(company.logoName == null || company.logoName.length <= 0)
				company.logoName = formatName(company.tradingName);
		}
		else
			company.logoName = ""; 
		
		
		$http.put(urlSave, company).then(function(response){
			$('#modal_carregando').modal('hide');
			alert($scope.getMessage('DataSavedSuccessfully'));
		}, function(response){
			alert(error + response.status + "\n" + response.message);
		});
	}
	
	function getLogo(){
		var urlGetLogo = config.baseUrl + '/admin/downloadImage/' + $scope.company.logoName;
		console.log('getLogo: ', urlGetLogo);
		var defer = $q.defer();
		
		$http({ method: 'Get',
			url: urlGetLogo,
			transformResponse: [
				function (data) {
					$scope.srcLogo = data;
					$('#modal_carregando').modal('hide');
					document.getElementById("addLogoMessage").style.visibility = "hidden";
				}
			]
		}).then(function(response) {
			defer.resolve(response.data);
		}, function(response) {
			alert(scope.getMessage('ErrorLoadData'))
		});	 
		
	 return defer.promise;
	}
	
	$('#insertLogo').change(function () {
		abreModalNovaImg($('#NewLogo'), $('#modalNewLogo'), this.files)
	});
	
	$scope.addLogo = function () {
		$('#modalNewLogo').modal('hide');
		document.getElementById('logoImg').src = document.getElementById('NewLogo').src;
		document.getElementById("addLogoMessage").style.visibility = "hidden";
	}
	
	function formatName( stringAccent ) {
		  var string = stringAccent;
			var mapaAcentosHex 	= {
					 	a : /[\xE0-\xE6]/g,
				        A : /[\xC0-\xC6]/g,
				        e : /[\xE8-\xEB]/g,
				        E : /[\xC8-\xCB]/g,
				        i : /[\xEC-\xEF]/g,
				        I : /[\xCC-\xCF]/g,
				        o : /[\xF2-\xF6]/g,
				        O : /[\xD2-\xD6]/g,
				        u : /[\xF9-\xFC]/g,
				        U : /[\xD9-\xDC]/g,
				        c : /\xE7/g,
				        C : /\xC7/g,
				        n : /\xF1/g,
				        N : /\xD1/g
			};

			for ( var letra in mapaAcentosHex ) {
				var expressaoRegular = mapaAcentosHex[letra];
				string = string.replace( expressaoRegular, letra );
			}
			
			return 'LOGO_' + string.toUpperCase().replace(" ", "_");
	}
	
	$(function(){
		$('#logoImg').mouseenter(function(){ 
			var addLogo = config.baseUrl+iconAddLogo.substr(iconAddLogo.indexOf('apiGIMB')+7, iconAddLogo.length);
			if(document.getElementById('logoImg').src != addLogo)
				document.getElementById("botaoExcluirLogo").style.visibility = "visible";
		 });
	});
	
	$(function(){
		$('#logoImg').mouseout(function(){
			var addLogo = config.baseUrl+iconAddLogo.substr(iconAddLogo.indexOf('apiGIMB')+7, iconAddLogo.length);
			if(document.getElementById('logoImg').src != addLogo)
				document.getElementById("botaoExcluirLogo").style.visibility = "hidden";
		 });
	});
	
	$(function(){
		$('#botaoExcluirLogo').mouseenter(function(){
			var addLogo = config.baseUrl+iconAddLogo.substr(iconAddLogo.indexOf('apiGIMB')+7, iconAddLogo.length);
			if(document.getElementById('logoImg').src != addLogo)
				document.getElementById("botaoExcluirLogo").style.visibility = "visible";
		 });
	});	
	
	$scope.deleteLogo = function(company){
		if(company.logoName != null && company.logoName != ""){
			$('#modal_carregando').modal('show');
			
			var urlSave = config.baseUrl + '/deleteLogo';
			
			$http.put(urlSave, company).then(function(response){
				$('#modal_carregando').modal('hide');
				document.getElementById('logoImg').src = iconAddLogo;
				$scope.company.logoName = "";
				document.getElementById("botaoExcluirLogo").style.visibility = "hidden";
				document.getElementById("addLogoMessage").style.visibility = "visible";
				
			}, function(response){
				alert(error + response.status + "\n" + response.message);
			});
		}
		else{
			document.getElementById('logoImg').src = iconAddLogo;
			document.getElementById("botaoExcluirLogo").style.visibility = "hidden";
			document.getElementById("addLogoMessage").style.visibility = "visible";
		}
	}
	
	$scope.openModal = function(){
		document.getElementById("botaoExcluirLogo").style.visibility = "hidden";
		$("#modalConfirmDeleteLogo").modal('show');
	}
});