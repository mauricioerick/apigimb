appGIMB.controller("clientDetailController", function ($scope, $http, $routeParams, geralAPI, juiceService, config, $location, $compile) {

	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');
	if (!controllerHeadCheck($scope, $location, juiceService)) return;

	$('#divMenu').show();

	$scope.client = {};
	$scope.equipments = [];
	$scope.veiculos = [];
	$scope.imports = null;

	$scope.client = {
		active: true
	};
	$scope.client.colorId = getRandomColor();
	colorSelectorConf('colorSel', $scope.client.colorId)

	$scope.costCentersToSelect = [];
	$scope.costCenters = [];
	$scope.costCenterByClient = [];
	$scope.newCostCenter = "";

	$scope.optActive = "active";

	$scope.costCenterIdSelected = "";

	function load(show) {
		$('#modal_carregando').modal(show ? 'show' : 'hide');
	}

	if ($routeParams.clientId != null) {
		carregarCliente($routeParams.clientId);
		loadEquipments();
	}

	$scope.filterClientsByStatus = function () {
		filterClientsByStatus();
	};

	function filterClientsByStatus() {
		$scope.clients = $scope.allClients.filter(function (
			client) {
			if ($scope.optActive == "all")
				return true;
			else if ($scope.optActive == "active"
				&& client.active == true)
				return true;
			else if ($scope.optActive == "inactive"
				&& client.active == false)
				return true;

			return false;
		});
		
		setOptionClient($scope.optActive);
		
		if($scope.optActive == 'active')
			document.getElementById('optActive').checked = true;
		else if($scope.optActive == 'inactive')
			document.getElementById('optInactive').checked = true;
		else
			document.getElementById('optAll').checked = true;
		
		
		$scope.clients = orderObject( $scope.clients, 'tradingName' );
	}

	$scope.salvar = function (objClient, showAlert) {
		console.log('salvar');
		if (!validateForm())
			return;

		if ($scope.veiculos.length > 0) {
			$scope.client.vehicles = $scope.veiculos;
		}

		if (($scope.client.costCenter) && ($scope.client.costCenter.costCenterId === ""))
			$scope.client.costCenter = null

		var url = config.baseUrl + "/admin/createClient";

		$http({
			method: 'PUT',
			url: url,
			data: $scope.client
		}).then(function (response) {
			console.log('salvar:', response);
			$scope.costCenterByClient = response.data.costCenters;
			showAlert ? alert($scope.getMessage('DataSavedSuccessfully')) : false;

			const splitedUrl = url.split('/');
			(splitedUrl[splitedUrl.length - 1] === 'createClient') ? $location.path(`${juiceService.appUrl}clientDetail/${response.data.clientId}`) : false;

			// refreshPage();
			// alert($scope.getMessage('DataSavedSuccessfully'));
		}, function (response) {
			alert(error + response.status);
		});
	};

	function carregarCliente(id) {
		$http({ method: 'GET', url: config.baseUrl + '/admin/client/' + id })
			.then(function (response) {
				$scope.client = response.data;
				$scope.veiculos = $scope.client.vehicles;
				if ($scope.client.colorId == null)
					$scope.client.colorId = getRandomColor();

				colorSelectorConf('colorSel', $scope.client.colorId)

				loadEquipments();
			}, function (response) {
				console.log('cliente dettalhe: ', response);
			});
	};

	$scope.salvar = function (objClient) {
		if ($scope.veiculos.length > 0) {
			$scope.client.vehicles = $scope.veiculos;
		}

		$http({
			method: 'PUT',
			url: config.baseUrl + '/admin/createClient',
			data: $scope.client
		}).then(function (response) {
			//message de sucesso
			refreshPage();
			$location.path(juiceService.appUrl + 'clientes');
		}, function (error) {
			//mensagem de erro
		});
	};

	$scope.addVehicle = function (objVehicle) {
		var urlSave = config.baseUrl + '/admin/createVehicle/';
		var newVehicle = true;

		if (objVehicle.vehicleId > 0) {
			urlSave = config.baseUrl + '/admin/updateVehicle/' + objVehicle.vehicleId;
			newVehicle = false;
		}

		if ($scope.client.clientId) {
			objVehicle.clientId = $scope.client.clientId;
			$http({
				method: 'PUT',
				url: urlSave,
				data: objVehicle
			}).then(function (response) {

				objVehicle = response.data;

				$http({
					method: 'PUT',
					url: config.baseUrl + '/admin/updateVehicleToClient/' + objVehicle.vehicleId,
					data: $scope.client
				}).then(function (response) {

				}, function (error) {

				});

				if (newVehicle)
					addVeiList(objVehicle);

				refreshVeiculo();
			}, function (error) {
				alert("Erro ao criar veículo");
			});

		} else {
			addVeiList(objVehicle);
		}
	}	

	$scope.vehicleDescription = function (vehicle) {		
		var desc = vehicle.brand == null ? '' : vehicle.brand;
		if (desc.length > 0) desc += ' - ';
		desc += vehicle.model == null ? '' : vehicle.model;
		if (desc.length > 0) desc += ' - ';
		desc += vehicle.plate == null ? '' : vehicle.plate;

		return desc;
	}

	// $scope.editVehicle = function (objVehicle) {
	// 	console.log('abrir edição ',objVehicle);
	// 	$scope.vehicle = objVehicle;
	// }

	// $scope.novoVehicle = function () {
	// 	$scope.vehicle = {};
	// }	

	$scope.removeVei = function (objVehicle) {
		if ($scope.client.clientId) {
			$http({
				method: 'PUT',
				url: config.baseUrl + '/admin/removeFromClientVehicle/' + objVehicle.vehicleId,
				data: objVehicle
			})
				.then(function (response) {

					if (response.status == "409") {
						alert("O veículo não pode ser removido pois está sendo utilizado!");
					} else if (response.status >= "200" && response.status <= "299") {
						removeVeiList(objVehicle);
					}

				}, function (response) {
					alert("Erro ao excluir veículo. \nErro: " + response.status);
				});
		} else {
			removeVeiList(objVehicle);
		}
	};

	function addVeiList(obj) {
		if ($scope.veiculos.indexOf(obj) == -1)
			$scope.veiculos.push(obj);

		refreshVeiculo();
	}

	function removeVeiList(obj) {
		$scope.veiculos.splice($scope.veiculos.indexOf(obj), 1);
	};

	function refreshPage() {
		$scope.client = {};
		$scope.veiculos = [];
	}

	function refreshVeiculo() {
		$scope.vehicle = {};
	}

	function loadEquipments() {
		$http.get(config.baseUrl + "/admin/equipment")
			.then(function (response) {
				$scope.equipments = response.data;
			}, function (response) {
				alert("Erro ao carregar equipamentos!");
			});
	}

	/*
	* IMPORT EQUIPMENTS
	*/
	
	// if ($routeParams.clientId != null) {
	// 	loadImportsByClassName();
		
	// 	function loadImportsByClassName(){
	// 		load(true);
			
	// 		$http.get(`${config.baseUrl}/admin/importsByClass/EQUIPMENT`).then(
	// 			function(response){				
	// 				$scope.imports = response.data
	// 				load(false);
	// 			},
	// 			function(response){
	// 				alert($scope.getMessage('ErrorLoadData'));
	// 				load(false);
	// 			}
	// 		);
	// 	}

	// 	$('#modalImportXls').on('hide.bs.modal', function(event) {
	// 		clearModal();
	// 	});	
		
	// 	function clearModal() {
	// 		document.getElementById('uploadXls').value = '';
	// 		document.getElementById('divImport').innerHTML = '';
	// 		$scope.imports = null;
	// 	}

	// 	$(document).ready(function() {
	// 		$('#uploadXls').change(function(){
	// 			if(this.files && this.files[0]){
	// 				insertIconXls(this.files[0].name);
	// 				readTheFileUrl(this.files[0]);
	// 			}
	// 		});
	// 	});

	// 	function insertIconXls(name){
	// 		const inner = `<img id="xlsImported" src="apiGIMB/view/TablesForImport/xls.png" style="max-height: 50px; max-width: 50px;">
	// 			<br/>
	// 			<span>${name}</span>
	// 			<br/>
	// 			<div  ng-click="importXls()" > 
	// 				<i class = "glyphicon glyphicon-send"></i>
	// 				<input type="button" value="${getLabel('send')}" class="btn formGroupInput">
	// 			</div>
	// 		`;
				
	// 		const $str = $(inner).appendTo('#divImport');
	// 		$compile($str)($scope);
	// 	}

	// 	$scope.importXls = () => {
	// 		load(true);	
			
	// 		if ($scope.imports != null) {			
	// 			$http.post(config.baseUrl + '/admin/importXLSTableVehicle/' + $routeParams.clientId, $scope.imports).then(
	// 				function(response){
	// 					console.log('Retorno importXls: ', response.data);	
						
	// 					if (response.data.length > 0) {
	// 						var identificadores = '';
	// 						for (let i = 0; i < response.data.length; i++) {
	// 							identificadores = identificadores + ', ' + response.data[i].plate;
	// 						}
	// 						alert('Equipamento não encontrado, para o identificador: - ' + identificadores);						
	// 					}
						
	// 					alert($scope.getMessage('DataSavedSuccessfully'));
	// 					carregarCliente($routeParams.clientId);

	// 					$('#modalImportXls').modal('hide');

	// 					load(false);
	// 				},
	// 				function(response){
	// 					alert($scope.getMessage('ErrorLoadData'));
	// 					load(false);
	// 				}
	// 			);
	// 		}
	// 		else {
	// 			alert($scope.getMessage('ErrorLoadData'));
	// 		}
	// 	}

	// 	function readTheFileUrl(file){
	// 		var reader = new FileReader();
	// 		reader.readAsDataURL(file);
	// 		reader.onload = (e) => {
	// 			$scope.imports.filePath = e.target.result.substr(e.target.result.indexOf('base64,') + 7, e.target.result.length);;
	// 		}
	// 	}
	// }
	
});
