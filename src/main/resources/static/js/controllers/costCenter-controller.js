appGIMB.controller("costCenterController", function($scope, $http,
		$routeParams, $location, config, juiceService, geralAPI) {
	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');
	if (!controllerHeadCheck($scope, $location, juiceService))
		return;

	$('#divMenu').show();

	$scope.costCenter = {
		active : true
	};
	$scope.costCenters = [];
	$scope.optActive = "active";
	$scope.allCostCenter = [];
	$scope.costCenter.colorId = getRandomColor();
	
	colorSelectorConf('colorSel', $scope.costCenter.colorId);

	if ($routeParams.costCenterId)
		loadCostCenter($routeParams.costCenterId);
	else if($location.path().indexOf("costCenterCreate") <= -1)
		loadCostCenters();

	function loadCostCenter(id) {
		$('#modal_carregando').modal('show');
		
		$http.get(config.baseUrl + "/admin/costCenter/" + id).then(
				function(response) {
					$scope.costCenter = response.data;
					$('#modal_carregando').modal('hide');
				}, function(response) {
					alert($scope.getMessage('ErrorLoadData'));
				});
	}

	function loadCostCenters() {
		$('#modal_carregando').modal('show');
		
		var url = config.baseUrl + "/admin/allCostCenter";
		$http.get(url).then(function(response) {
			$scope.allCostCenter = response.data;
			
			$scope.query = getTextCostCenter();
			$scope.optActive = getOptionCostCenter().length > 0 ? getOptionCostCenter() : 'active'; 
			
			filterCostCenterByStatus();
			$('#modal_carregando').modal('hide');
		}, function(response) {
			alert($scope.getMessage('ErrorLoadData'));
		});
	}

	$scope.filterCostCenterByStatus = function() {
		filterCostCenterByStatus();
	}

	function filterCostCenterByStatus() {
		$scope.costCenters = $scope.allCostCenter.filter(function(costCenter) {
			if ($scope.optActive == "all")
				return true;
			else if ($scope.optActive == "active" && costCenter.active == true)
				return true;
			else if ($scope.optActive == "inactive"
					&& costCenter.active == false)
				return true;

			return false;
		});
		
		setOptionCostCenter($scope.optActive);
		
		if($scope.optActive == 'active')
			document.getElementById('optActive').checked = true;
		else if ($scope.optActive == 'inactive')
			document.getElementById('optInactive').checked = true;
		else
			document.getElementById('optAll').checked = true;
	}

	$scope.retornaClasse = function(objCostCenter) {
		if (objCostCenter.active == true) {
			return 'btnativo';
		} else {
			return 'btninativo';
		}
	};

	$scope.trataAtivoInativo = function(objCostCenter) {
		if (objCostCenter.active == true)
			return $scope.getLabel('Active');
		else
			return $scope.getLabel('Inactive');
	};

	$scope.mudaSituacao = function(objCostCenter) {
		$('#modal_carregando').modal('show');
		
		objCostCenter.active = objCostCenter.active == true ? false : true;

		$http.put(
				config.baseUrl + '/admin/updateCostCenter/'
						+ objCostCenter.costCenterId, objCostCenter).then(
				function(response) {
					loadCostCenters();
					$('#modal_carregando').modal('hide');
				}, function(response) {
					alert($scope.getMessage('ErrorSavingDataWithoutParam'));
				});
	};

	$scope.saveCostCenter = function(objCostCenter) {
		if (!validateForm())
			return;
		$('#modal_carregando').modal('show');
		
		var url;
		if(objCostCenter.costCenterId != null && objCostCenter.costCenterId > 0)
			url = config.baseUrl + '/admin/updateCostCenter/' + objCostCenter.costCenterId;
		else 
			url = config.baseUrl + '/admin/saveCostCenter';
		
		$http.put(url, objCostCenter).then(
				function(response){
					$('#modal_carregando').modal('hide');
					
					setTimeout(function () {
						$('#backCostCenterList').trigger('click');
					}, 500);
					
				}, function(response){
					alert($scope.getMessage('ErrorSavingDataWithoutParam'));
				}
		);
	};
	
	$scope.setTextCostCenter = () => {
		setTextCostCenter($scope.query);
	}
});