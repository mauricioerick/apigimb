appGIMB.controller("importXlsController", function ($scope, $http, $routeParams, geralAPI, juiceService, config, $location, $compile) {

	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');
	if (!controllerHeadCheck($scope, $location, juiceService)) return;

	$('#divMenu').show();
	
	$scope.allImports = [];
	$scope.imports = null;
	
	loadAllImports();
	
	function load(show) {
		$('#modal_carregando').modal(show ? 'show' : 'hide');
	}
	
	function loadAllImports(){
		load(true);
		
		$http.get(`${config.baseUrl}/admin/allImports`).then(
			function(response){
				console.log(response.data);
				$scope.allImports = response.data
				load(false);
			},
			function(response){
				alert($scope.getMessage('ErrorLoadData'));
				load(false);
			}
		);
	}
	
	$scope.urlDownloadModel = (imports) => {
		return config.baseUrl + '/downloadModel/?filePath=' + imports.filePath + '&mime=' + imports.mime;
	}
	
	$scope.openTableModal = (imports) => {
		$scope.imports = imports;
		$("#tableModal").modal('show');
	}
	
	$(document).ready(function() {
		$('#uploadXls').change(function(){
			if(this.files && this.files[0]){
				insertIconXls(this.files[0].name);
				readTheFileUrl(this.files[0]);
			}
		});
	});
	
	$('#tableModal').on('hide.bs.modal', function(event) {
		clearModal();
	});
	
	function clearModal() {
		document.getElementById('uploadXls').value = '';
		document.getElementById('divImport').innerHTML = '';
		$scope.imports = null;
	}
	
	function insertIconXls(name){
		const inner = `<img id="xlsImported" src="apiGIMB/view/TablesForImport/xls.png" style="max-height: 50px; max-width: 50px;">
			<br/>
			<span>${name}</span>
			<br/>
			<div  ng-click="importXls()" > 
				<i class = "glyphicon glyphicon-send"></i>
				<input type="button" value="${getLabel('send')}" class="btn formGroupInput">
			</div>
		`;
			
		const $str = $(inner).appendTo('#divImport');
		$compile($str)($scope);
	}
	
	$scope.importXls = () => {
		load(true);
		
		if($scope.imports != null){
			$http.post(config.baseUrl + '/admin/transformXLSTable', $scope.imports).then(
				function(response){
					alert($scope.getMessage('DataSavedSuccessfully'));
					load(false);
				},
				function(response){
					alert($scope.getMessage('ErrorLoadData'));
					load(false);
				}
			);
		}
		else {
			alert($scope.getMessage('ErrorLoadData'));
		}
	}
	
	function readTheFileUrl(file){
		var reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = (e) => {
			$scope.imports.filePath = e.target.result.substr(e.target.result.indexOf('base64,') + 7, e.target.result.length);;
		}
	}
	
});