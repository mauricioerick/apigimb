appGIMB.controller("dsInventorysChartController", function ($scope, $http, $location, config, juiceService) {

	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');
	if (!controllerHeadCheck($scope, $location, juiceService, false)) return;

	$scope.pathImg = config.pathImg;
	$('.ipt').attr('disabled', 'disabled');
	$('#divMenu').show();
	$("body").css("overflowY", "auto");
	$("#dtInicio").datepicker(datePickerOptions);
	$("#dtFinal").datepicker(datePickerOptions);

	$scope.users = [];
	$scope.usersSelected = [];
	$scope.usersNames = "";
	$scope.quantity = true;
	$scope.inventorys = [];
	$scope.inventorysSelected = [];
	$scope.inventorysNames = "";

	$scope.clients = [];
	$scope.clientsSelected = [];
	$scope.clientsNames = "";

	$scope.storages = [];
	$scope.storagesSelected = [];
	$scope.storagesNames = "";

	$scope.maximumRecords = 10;

	$scope.graphType = 'pie';
	selectedGraphType = 'pie';

	$scope.listaInventory = [];

	listaItens = [];

	listaEquipamentos = [];
	listaStorages = [];
	listaObjects = [];

	itensPorEquipamento = {};
	itensPorinventorye = {};
	itensPorStorages = {}

	canRunReload = false;

	dtIni = lastUtilDay();
	dtFin = lastUtilDay();

	defaultColors = {};

	$scope.dtInicio = dtIni;
	$scope.dtFinal = dtFin;

	$scope.showUsers = false;
	$scope.showinventory = false;
	$scope.showEquipments = false;
	$scope.showClient = false;
	$scope.showStorage  = false;

	chartItemsColor = '#b71c1c';

	configUIComponents();

	$scope.translate = function (word) {
		return getLabel(word);
	}

	$scope.reload = function () {
		dtaInicio = document.getElementById("dtInicio").value;
		dtaFim = document.getElementById("dtFinal").value;
		if (canRunReload) {
			setDataInicioFiltroOS(document.getElementById("dtInicio").value);
			setDataFinalFiltroOS(document.getElementById("dtFinal").value);

			if ($scope.showUsers)
				$scope.showUsersSelect()

			if ($scope.showEvents)
				$scope.showEventsSelect()

			$("#parametersModal").modal('hide')
			loadTransfer();
		}
	}

	$scope.buscar = function () {
		$('#modal_carregando').modal('show');
		
		var params = {};

		dtaInicio = document.getElementById("dtInicio").value;
		dtaFim = document.getElementById("dtFinal").value;

		params["startDate"] = dtaInicio;
		params["endDate"] = dtaFim;
		params["user"] = JSON.parse(localStorage.getItem("bobby"));
		$http({
			method: 'GET',
			url: config.baseUrl + "/admin/object",
			data: params
		}).then(function (resposta) {
			$scope.listaInventory = resposta.data;
			preparaInformacoes();
			createChartInventory();
			
	//	createChartInventory();
		$('#modal_carregando').modal('hide');
	}, function (resposta) {
		alert($scope.getMessage('ErrorLoadingServices') + resposta.message);

		$('#modal_carregando').modal('hide');
	});

			$http({
				method: 'GET',
				url: config.baseUrl + "/admin/storage",
				data: params
			}).then(function (resposta) {
				$scope.listaStorages = resposta.data;
				
		//	createChartTemp();
		//	createChartInventory();
			$('#modal_carregando').modal('hide');
		}, function (resposta) {
			alert($scope.getMessage('ErrorLoadingServices') + resposta.message);

			$('#modal_carregando').modal('hide');
		});
	}
	function createChartInventory() {
		var service, dataValues, dataKeys
		var colors = []
		var totals = {}
	
		div = document.getElementById('chartTemps');
		div.innerHTML = '';

		div.innerHTML += getHTMLCharCanvas('temps', $scope.getLabel('InventorybyStorages'), 12);

		// dataKeys = [];
		// dataValues = [];

		// listaItens
		// for (i in listaItens) {
		// 	const item = listaItens[i];
		// 	dataKeys.push(item.chave);
		// 	console.log(item.chave + " chave ")
		// 	dataValues.push(item.valor);
		// 	console.log(item.valor + " valor")
		// 	if (i == $scope.maximumRecords - 1) break;
		// }
		
		listaItens = $scope.listaInventory;
		for (let index in $scope.listaInventory) {
			inv = listaItens[index]
	  
			if ($scope.quantity)
			  totals[inv.storage.description] = totals[inv.storage.description] == null ? 1 : totals[inv.storage.description] + 1;
			
			  else
			  totals[inv.storage.description] = totals[inv.storage.description] == null ? inv.timeElapsedMilli : totals[inv.storage.description] + inv.timeElapsedMilli
		  }
	  
		  if (!$scope.quantity)
			changeToHour(totals)
	  
		  dataValues = Object.values(totals);
		  dataKeys = Object.keys(totals);
	  

		
		alpha = 1;
		var color;
		for (let i = 0; i < dataKeys.length; i++) {
		// color = defaultColors[dataKeys[i]];
		 colors.push(color != null ? hexToRgbA(color, 0.5) : hexToRgbA(getRandomColor(), 0.5));
		alpha -= 0.7;
		}


		datalabelsConf = { align: 'end', anchor: 'start' };
		if (selectedGraphType == 'pie') datalabelsConf = { align: 'start', anchor: 'end' };

		var data = {
			labels: dataKeys,
			datasets: [{
				data: dataValues,
				backgroundColor: colors,
				datalabels: datalabelsConf
			}]
		}

		context = document.getElementById('chart' + 'temps').getContext('2d');
		chart = new Chart(context, {
			type: selectedGraphType,
			options: {
				plugins: {
					datalabels: {
						color: (selectedGraphType == 'pie') ? 'white' : 'black',
						display: function (context) {
							return true;
						},
						font: {
							weight: 'bold'
						},
						formatter: function (value, context) {
							var lb = formatterHourGraphLabel(value, context) + '\n' + percChart(context.dataset, context.datasetIndex, context.dataIndex);
							return lb;

						},
						clip: true,
						tittle: false
					}
				},
				tooltips: {
					custom: function (tooltip) {
						//tooltip.x = 0;
						//tooltip.y = 0;
					},
					mode: 'single',
					callbacks: {
						label: function (tooltipItems, data) {
							return formatterHourGraphLabel(data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index], chart.controller);
						},
						beforeLabel: function (tooltipItems, data) {
							return percChart(data.datasets, tooltipItems.datasetIndex, tooltipItems.index);
						}
					}
				}
			}
		});
		// labelFormatter = 'HOUR';
		// chart['labelFormatter'] = labelFormatter;
		chart.data = data;
		chart.options.legend.display = true; 
		if (selectedGraphType == 'bar') {
			chart.options.scales.xAxes[0].display = false;
			chart.options.scales.yAxes[0].ticks.beginAtZero = true;
			chart.options.legend.display = false;
		}

		chart.update();
		setChartData('chart' + 'itens', { 'data': data, 'tittle': $scope.getLabel('InventorybyStorages'), 'labelFormatter': null, type: selectedGraphType });
		var h2 = document.getElementById("totais");
		h2.style.display = "block";

	}




	function createChartTemp() {
		div = document.getElementById('chartTemps');
		div.innerHTML = '';

		div.innerHTML += getHTMLCharCanvas('Temp', $scope.getLabel('InventorybyStorages'), 12);
		labelTemp = listaStorages;

		
		dataKeys = [];
		dataValues = [];
	
		console.log("objetocs" + listaObjects);
		for (i in listaObjects) {
			const storage = listaObjects[i];
			console.log(" invent" + storage)
			dataKeys.push(storage.chave);
			
			dataValues.push(storage.valor);
			console.log("valor valorinvent" + storage.valor)
		
			


		//	if (i == $scope.maximumRecords - 1) break;
		}



		dataInventory = listaInventory;
		console.log("temp" +labelTemp)
		  new Chart(document.getElementById("chartTemp"),{
			type:'pie',
			data:{
			labels:labelTemp,
			datasets:[{"label":"",
			data:[3,3,7],
			background:["rgb(255, 99, 132)","rgb(54, 162, 235)","rgb(255, 205, 86)"]}]}});

	}

	function createChartStorage() {
		div = document.getElementById('chartStorage');
		div.innerHTML = '';

		div.innerHTML += getHTMLCharCanvas('storage', $scope.getLabel('InventorybyStorages'), 12);

		dataKeys = [];
		dataValues = [];
		
		
		for (i in listaStorages) {
			const storage = listaStorages[i];
			dataKeys.push(storage.chave);
			console.log("cahve storage" + storage.chave)
			dataValues.push(storage.valor);
			console.log("valor storage" + storage.valor)
		
			


		//	if (i == $scope.maximumRecords - 1) break;
		}

		colors = [];
		for (let i = 0; i < dataKeys.length; i++) {
		//color = defaultColors[dataKeys[i]];
			colors.push(color != null ? hexToRgbA(color, 0.7) : hexToRgbA(getRandomColor(), 0.7));
		}

		datalabelsConf = { align: 'end', anchor: 'start' };
		if (selectedGraphType == 'pie') datalabelsConf = { align: 'start', anchor: 'end' };

		var data = {
			labels: dataKeys,
			datasets: [{
				data: dataValues,
  				backgroundColor: colors,
				datalabels: datalabelsConf
			}]
		}

		context = document.getElementById('chart' + 'storage').getContext('2d');
		chart = new Chart(context, {
			type: selectedGraphType,
			options: {
				plugins: {
					datalabels: {
						color: (selectedGraphType == 'pie') ? 'white' : 'black',
						display: function (context) {
							return true;
						},
						font: {
							weight: 'bold'
						},
						formatter: function (value, context) {
							var lb = formatterHourGraphLabel(value, context) + '\n' + percChart(context.dataset, context.datasetIndex, context.dataIndex);
							return lb;

						},
						clip: true,
						tittle: false
					}
				},
				tooltips: {
					custom: function (tooltip) {
						//tooltip.x = 0;
						//tooltip.y = 0;
					},
					mode: 'single',
					callbacks: {
						label: function (tooltipItems, data) {
							return formatterHourGraphLabel(data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index], chart.controller);
						},
						beforeLabel: function (tooltipItems, data) {
							return percChart(data.datasets, tooltipItems.datasetIndex, tooltipItems.index);
						}
					}
				}
			}
		});
		// labelFormatter = 'HOUR';
		// chart['labelFormatter'] = labelFormatter;
		chart.data = data;
		chart.options.legend.display = false;
		if (selectedGraphType == 'bar') {
			chart.options.scales.xAxes[0].display = false;
			chart.options.scales.yAxes[0].ticks.beginAtZero = true;
		}

		chart.update();
		setChartData('chart' + 'inventory', { 'data': data, 'tittle': $scope.getLabel('InventorybyStorages'), 'labelFormatter': null, type: selectedGraphType });
	}


	function filtraRegistros() {
		let listaStoragesFiltrados = [];

		$scope.listaStorages.forEach(storage => {
			listaStoragesFiltrados.push(storage); // Insere o servico em um array auxiliar
			let removeuElemento = false;

			if ($scope.usersSelected.length > 0 && removeuElemento == false) { // Caso usuario inseriu algum filtro de usuario
				let idx = 1; // variavel de index (auxiliar)
				$scope.usersSelected.forEach(usuario => { // Para cada usuario inserido no filtro verifica se existe no servico corrente
					if (usuario.userId == servico.user.userId) return true; // caso tiver, sai da iteracao
					if (usuario.userId != servico.user.userId && idx === $scope.usersSelected.length) { // caso nenhum usuario pertencer ao servico, e for a ultima iteracao
						let indexServico = listaStoragesFiltrados.indexOf(servico); // pega o index do servico no array
						listaStoragesFiltrados.splice(indexServico, 1); // remove o servico do array
						removeuElemento = true;
					};
					idx++;
				});
			}

			if ($scope.storagesSelected.length > 0 && removeuElemento == false) {
				let idx = 1;
				$scope.storagesSelected.forEach(storagies => {
					if (storagies.inventoryId == storagies.storage.inventoryId) return true;
					if (storagies.inventoryId != storagies.storage.inventoryId && idx === $scope.storagesSelected.length) {
						let indexServico = listaStoragesFiltrados.indexOf(servico);
						listaStoragesFiltrados.splice(indexServico, 1);
						removeuElemento = true;
					}
					idx++;
				});
			}

		});

		$scope.listaStorages = [];
		listaStoragesFiltrados.forEach(storage => $scope.listaInventario.push(storage));

		fillLabelsParameters();
	}

	function preparaInformacoes() {
		const itens = {};
		const equipamentos = {};
		const clientes = {};

		let servicosComProblemas = [];

		itensPorEquipamento = {};
		itensPorCliente = {};

	//filtraRegistros();
		console.log("prepara");
		console.log($scope.listaInventory);
		$scope.listaInventory.forEach(inventario => {
			if (inventario.storage !== null) {
				let temProblema = false;
						let total = itens[storage.description];
						if (total == null) total = 0;

						itens[storage.description]= ++total;
					}
		});
		
		listaItens = [];
		for (key of Object.keys(itens)) {
			listaItens.push({ chave: key, valor: itens[key] });
		}
		listaItens.sort((obj1, obj2) => (obj1.valor < obj2.valor) ? 1 : -1);

		console.log(listaItens);
	}

	function configUIComponents() {
		$('input[type=radio][id=radioGraphTypePie]').change(function () {
			$scope.graphType = 'pie';
			selectedGraphType = 'pie';
		});

		$('input[type=radio][id=radioGraphTypeBar]').change(function () {
			$scope.graphType = 'bar';
			selectedGraphType = 'bar';
		});
	}

	function createChartItens() {
		div = document.getElementById('chartItens');
		div.innerHTML = '';

		div.innerHTML += getHTMLCharCanvas('itens', $scope.getLabel('OffendersItens'), 12);

		dataKeys = [];
		dataValues = [];

		listaStorages;
		console.log("lista storage " + listaStorages);
		for (i in listaStorages) {
			const item = listaStorages[i];
			dataKeys.push(item.chave);
			dataValues.push(item.valor);

			if (i == $scope.maximumRecords - 1) break;
		}
			console.log("createchartItensa"+dataKeys);


		colors = [];
		alpha = 1;
		for (let i = 0; i < dataKeys.length; i++) {
			// color = defaultColors[dataKeys[i]];
			colors.push(hexToRgbA(chartItemsColor, alpha));
			alpha -= 0.07;
		}


		datalabelsConf = { align: 'end', anchor: 'start' };
		if (selectedGraphType == 'pie') datalabelsConf = { align: 'start', anchor: 'end' };

		var data = {
			labels: dataKeys,
			datasets: [{
				data: dataValues,
				backgroundColor: colors,
				datalabels: datalabelsConf
			}]
		}

		context = document.getElementById('chart' + 'itens').getContext('2d');
		chart = new Chart(context, {
			type: selectedGraphType,
			options: {
				plugins: {
					datalabels: {
						color: (selectedGraphType == 'pie') ? 'white' : 'black',
						display: function (context) {
							return true;
						},
						font: {
							weight: 'bold'
						},
						formatter: function (value, context) {
							var lb = formatterHourGraphLabel(value, context) + '\n' + percChart(context.dataset, context.datasetIndex, context.dataIndex);
							return lb;

						},
						clip: true,
						tittle: false
					}
				},
				tooltips: {
					custom: function (tooltip) {
						//tooltip.x = 0;
						//tooltip.y = 0;
					},
					mode: 'single',
					callbacks: {
						label: function (tooltipItems, data) {
							return formatterHourGraphLabel(data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index], chart.controller);
						},
						beforeLabel: function (tooltipItems, data) {
							return percChart(data.datasets, tooltipItems.datasetIndex, tooltipItems.index);
						}
					}
				}
			}
		});
		// labelFormatter = 'HOUR';
		// chart['labelFormatter'] = labelFormatter;
		chart.data = data;
		chart.options.legend.display = false;
		if (selectedGraphType == 'bar') {
			chart.options.scales.xAxes[0].display = false;
			chart.options.scales.yAxes[0].ticks.beginAtZero = true;
		}

		chart.update();
		setChartData('chart' + 'itens', { 'data': data, 'tittle': $scope.getLabel('OffendersItens'), 'labelFormatter': null, type: selectedGraphType });
	}

	function createChartEquipments() {
		div = document.getElementById('chartEquipment');
		div.innerHTML = '';

		div.innerHTML += getHTMLCharCanvas('equipment', $scope.getLabel('OffendersItensByEquipment'), 12);

		dataKeys = [];
		dataValues = [];

		for (i in listaEquipamentos) {
			const equipment = listaEquipamentos[i];
			dataKeys.push(equipment.chave);
			dataValues.push(equipment.valor);

			if (i == $scope.maximumRecords - 1) break;
		}

		colors = [];
		for (let i = 0; i < dataKeys.length; i++) {
			color = defaultColors[dataKeys[i]];
			colors.push(color != null ? hexToRgbA(color, 0.7) : hexToRgbA(getRandomColor(), 0.7));
		}


		datalabelsConf = { align: 'end', anchor: 'start' };
		if (selectedGraphType == 'pie') datalabelsConf = { align: 'start', anchor: 'end' };

		var data = {
			labels: dataKeys,
			datasets: [{
				data: dataValues,
				backgroundColor: colors,
				datalabels: datalabelsConf
			}]
		}

		context = document.getElementById('chart' + 'equipment').getContext('2d');
		chart = new Chart(context, {
			type: selectedGraphType,
			options: {
				plugins: {
					datalabels: {
						color: (selectedGraphType == 'pie') ? 'white' : 'black',
						display: function (context) {
							return true;
						},
						font: {
							weight: 'bold'
						},
						formatter: function (value, context) {
							var lb = formatterHourGraphLabel(value, context) + '\n' + percChart(context.dataset, context.datasetIndex, context.dataIndex);
							return lb;

						},
						clip: true,
						tittle: false
					}
				},
				tooltips: {
					custom: function (tooltip) {
						//tooltip.x = 0;
						//tooltip.y = 0;
					},
					mode: 'single',
					callbacks: {
						label: function (tooltipItems, data) {
							return formatterHourGraphLabel(data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index], chart.controller);
						},
						beforeLabel: function (tooltipItems, data) {
							return percChart(data.datasets, tooltipItems.datasetIndex, tooltipItems.index);
						}
					}
				}
			}
		});
		// labelFormatter = 'HOUR';
		// chart['labelFormatter'] = labelFormatter;
		chart.data = data;
		chart.options.legend.display = false;
		if (selectedGraphType == 'bar') {
			chart.options.scales.xAxes[0].display = false;
			chart.options.scales.yAxes[0].ticks.beginAtZero = true;
		}

		chart.update();
		setChartData('chart' + 'equipment', { 'data': data, 'tittle': $scope.getLabel('OffendersItensByEquipment'), 'labelFormatter': null, type: selectedGraphType });
	}

	function createChartInventory2() {
		div = document.getElementById('chartInventory');
		div.innerHTML = '';

		div.innerHTML += getHTMLCharCanvas('inventory', $scope.getLabel('InventorybyStorages'), 12);

		dataKeys = [];
		dataValues = [];

		for (i in listaInventory) {
			const inventory = listaInventory[i];
			dataKeys.push(inventory.chave);
			dataValues.push(inventory.valor);

			if (i == $scope.maximumRecords - 1) break;
		}

		colors = [];
		for (let i = 0; i < dataKeys.length; i++) {
			color = defaultColors[dataKeys[i]];
			colors.push(color != null ? hexToRgbA(color, 0.7) : hexToRgbA(getRandomColor(), 0.7));
		}


		datalabelsConf = { align: 'end', anchor: 'start' };
		if (selectedGraphType == 'pie') datalabelsConf = { align: 'start', anchor: 'end' };

		var data = {
			labels: dataKeys,
			datasets: [{
				data: dataValues,
				backgroundColor: colors,
				datalabels: datalabelsConf
			}]
		}

		context = document.getElementById('chart' + 'inventory').getContext('2d');
		chart = new Chart(context, {
			type: selectedGraphType,
			options: {
				plugins: {
					datalabels: {
						color: (selectedGraphType == 'pie') ? 'white' : 'black',
						display: function (context) {
							return true;
						},
						font: {
							weight: 'bold'
						},
						formatter: function (value, context) {
							var lb = formatterHourGraphLabel(value, context) + '\n' + percChart(context.dataset, context.datasetIndex, context.dataIndex);
							return lb;

						},
						clip: true,
						tittle: false
					}
				},
				tooltips: {
					custom: function (tooltip) {
						//tooltip.x = 0;
						//tooltip.y = 0;
					},
					mode: 'single',
					callbacks: {
						label: function (tooltipItems, data) {
							return formatterHourGraphLabel(data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index], chart.controller);
						},
						beforeLabel: function (tooltipItems, data) {
							return percChart(data.datasets, tooltipItems.datasetIndex, tooltipItems.index);
						}
					}
				}
			}
		});
		// labelFormatter = 'HOUR';
		// chart['labelFormatter'] = labelFormatter;
		chart.data = data;
		chart.options.legend.display = false;
		if (selectedGraphType == 'bar') {
			chart.options.scales.xAxes[0].display = false;
			chart.options.scales.yAxes[0].ticks.beginAtZero = true;
		}

		chart.update();
		setChartData('chart' + 'inventory', { 'data': data, 'tittle': $scope.getLabel('OffendersItensByinventory'), 'labelFormatter': null, type: selectedGraphType });
	}

	function createChartOffendersByinventory() {
		const keys = Object.keys(itensPorinventorye);

		const divElement = 'chartOffendersinventory';
		let chartName = '';
		let chartLabel = '';
		const chartSize = 4;

		const div = document.getElementById(divElement);

		keys.sort();

		let idx = 1;
		div.innerHTML = '';
		for (const key of keys) {
			chartName = 'iteminventory' + idx;
			chartLabel = key;

			div.innerHTML += getHTMLCharCanvas(chartName, chartLabel, chartSize);
			idx++;
		}


		idx = 1;
		for (const key of keys) {
			const itens = itensPorinventorye[key];
			chartLabel = key;
			const lista = [];
			for (k of Object.keys(itens)) {
				lista.push({ chave: k, valor: itens[k] });
			}

			chartName = 'iteminventory' + idx;

			chartColor = defaultColors[key];
			chartColor = chartColor || getRandomColor();

			dataKeys = [];
			dataValues = [];

			lista.sort((obj1, obj2) => obj2.valor - obj1.valor);

			for (i in lista) {
				const item = lista[i];
				dataKeys.push(item.chave);
				dataValues.push(item.valor);

				if (i == $scope.maximumRecords - 1) break;
			}


			colors = [];
			alpha = 1;
			for (let i = 0; i < dataKeys.length; i++) {
				// color = defaultColors[dataKeys[i]];
				colors.push(hexToRgbA(chartColor, alpha));
				alpha -= 0.07;
			}


			datalabelsConf = { align: 'end', anchor: 'start' };
			if (selectedGraphType == 'pie') datalabelsConf = { align: 'start', anchor: 'end' };

			var data = {
				labels: dataKeys,
				datasets: [{
					data: dataValues,
					backgroundColor: colors,
					datalabels: datalabelsConf
				}]
			}

			context = document.getElementById('chart' + chartName).getContext('2d');
			chart = new Chart(context, {
				type: selectedGraphType,
				options: {
					plugins: {
						datalabels: {
							color: (selectedGraphType == 'pie') ? 'white' : 'black',
							display: function (context) {
								return true;
							},
							font: {
								weight: 'bold'
							},
							formatter: function (value, context) {
								var lb = formatterHourGraphLabel(value, context) + '\n' + percChart(context.dataset, context.datasetIndex, context.dataIndex);
								return lb;

							},
							clip: true,
							tittle: false
						}
					},
					tooltips: {
						custom: function (tooltip) {
							//tooltip.x = 0;
							//tooltip.y = 0;
						},
						mode: 'single',
						callbacks: {
							label: function (tooltipItems, data) {
								return formatterHourGraphLabel(data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index], chart.controller);
							},
							beforeLabel: function (tooltipItems, data) {
								return percChart(data.datasets, tooltipItems.datasetIndex, tooltipItems.index);
							}
						}
					}
				}
			});
			// labelFormatter = 'HOUR';
			// chart['labelFormatter'] = labelFormatter;
			chart.data = data;
			chart.options.legend.display = false;
			if (selectedGraphType == 'bar') {
				chart.options.scales.xAxes[0].display = false;
				chart.options.scales.yAxes[0].ticks.beginAtZero = true;
			}

			chart.update();
			setChartData('chart' + chartName, { 'data': data, 'tittle': chartLabel, 'labelFormatter': null, type: selectedGraphType });
			idx++;
		}
	}

	function createChartOffendersByEquipment() {
		const keys = Object.keys(itensPorEquipamento);

		const divElement = 'chartOffendersEquipment';
		let chartName = '';
		let chartLabel = '';
		const chartSize = 4;

		const div = document.getElementById(divElement);
		// div.innerHTML += '';

		keys.sort();

		let idx = 1;
		div.innerHTML = '';
		for (const key of keys) {
			chartName = 'itemEquipment' + idx;
			chartLabel = key;

			div.innerHTML += getHTMLCharCanvas(chartName, chartLabel, chartSize);
			idx++;
		}

		idx = 1;
		for (const key of keys) {
			const itens = itensPorEquipamento[key];
			const lista = [];
			chartLabel = key;
			for (k of Object.keys(itens)) {
				lista.push({ chave: k, valor: itens[k] });
			}

			chartName = 'itemEquipment' + idx;

			chartColor = defaultColors[key];
			chartColor = chartColor || getRandomColor();

			dataKeys = [];
			dataValues = [];

			lista.sort((obj1, obj2) => obj2.valor - obj1.valor);

			for (i in lista) {
				const item = lista[i];
				dataKeys.push(item.chave);
				dataValues.push(item.valor);

				if (i == $scope.maximumRecords - 1) break;
			}

			let alpha = 1;
			colors = [];
			for (let i = 0; i < dataKeys.length; i++) {
				// color = defaultColors[dataKeys[i]];
				colors.push(hexToRgbA(chartColor, alpha));
				alpha -= 0.07;
			}


			datalabelsConf = { align: 'end', anchor: 'start' };
			if (selectedGraphType == 'pie') datalabelsConf = { align: 'start', anchor: 'end' };

			var data = {
				labels: dataKeys,
				datasets: [{
					data: dataValues,
					backgroundColor: colors,
					datalabels: datalabelsConf
				}]
			}

			context = document.getElementById('chart' + chartName).getContext('2d');
			chart = new Chart(context, {
				type: selectedGraphType,
				options: {
					plugins: {
						datalabels: {
							color: (selectedGraphType == 'pie') ? 'white' : 'black',
							display: function (context) {
								return true;
							},
							font: {
								weight: 'bold'
							},
							formatter: function (value, context) {
								var lb = formatterHourGraphLabel(value, context) + '\n' + percChart(context.dataset, context.datasetIndex, context.dataIndex);
								return lb;

							},
							clip: true,
							tittle: false
						}
					},
					tooltips: {
						custom: function (tooltip) {
							//tooltip.x = 0;
							//tooltip.y = 0;
						},
						mode: 'single',
						callbacks: {
							label: function (tooltipItems, data) {
								return formatterHourGraphLabel(data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index], chart.controller);
							},
							beforeLabel: function (tooltipItems, data) {
								return percChart(data.datasets, tooltipItems.datasetIndex, tooltipItems.index);
							}
						}
					}
				}
			});
			// labelFormatter = 'HOUR';
			// chart['labelFormatter'] = labelFormatter;
			chart.data = data;
			chart.options.legend.display = false;
			if (selectedGraphType == 'bar') {
				chart.options.scales.xAxes[0].display = false;
				chart.options.scales.yAxes[0].ticks.beginAtZero = true;
			}

			chart.update();
			setChartData('chart' + chartName, {
				'data': data,
				'tittle': chartLabel,
				'labelFormatter': null,
				type: selectedGraphType
			});
			idx++;
		}
	}

	function fillLabelsParameters() {
		// Usuários
		if ($scope.usersSelected.length > 0) {
			for (i in $scope.usersSelected) {
				user = $scope.usersSelected[i]
				if (!$scope.usersNames.includes(user.name))
					$scope.usersNames += $scope.usersNames.length == 0 ? user.name : ", " + user.name
			}
		}
		else {
			$scope.usersNames = ""
		}

		// inventoryes
		if ($scope.storagesSelected.length > 0) {
			for (i in $scope.storagesSelected) {
				storage = $scope.storagesSelected[i]
				if (!$scope.storagesNames.includes(storage.description))
					$scope.storagesNames += $scope.storage.length == 0 ? storage.description : ", " + inventory.description
			}
		}
		else {
			$scope.inventorysNames = ""
		}

		// Equipamentos
		if ($scope.equipmentsSelected.length > 0) {
			for (i in $scope.equipmentsSelected) {
				equipment = $scope.equipmentsSelected[i]
				if (!$scope.equipmentsNames.includes(equipment.description))
					$scope.equipmentsNames += $scope.equipmentsNames.length == 0 ? equipment.description : ", " + equipment.description
			}
		}
		else {
			$scope.equipmentsNames = ""
		}
	}

	$scope.changeGrapthType = function (event) {
		selectedGraphType = $scope.grapthType;
	}

	function getUsers() {
		$http.get(config.baseUrl + "/admin/user")
			.then(function (response) {
				$scope.users = response.data;
				$scope.users.sort((a, b) => a.name.toLowerCase().localeCompare(b.name.toLowerCase()));

				for (user of $scope.users)
					defaultColors[user.name] = user.colorId == null ? getRandomColor() : user.colorId;

			}, function (error) {
				alert($scope.getMessage('ErrorLoadingUsers'));
			});
	}

	function getClients() {
		$http.get(config.baseUrl + "/admin/client")
			.then(function (response) {
				$scope.clients = response.data;
				$scope.clients.sort((a, b) => a.tradingName.toLowerCase().localeCompare(b.tradingName.toLowerCase()));
				for (client of $scope.clients)
					defaultColors[client.tradingName] = client.colorId == null ? getRandomColor() : client.colorId;
			}, function (error) {
				alert($scope.getMessage('ErrorLoadingUsers'));
			});
	}


	function getStorages() {
		$http.get(config.baseUrl + "/admin/storage")
		.then(function(response){
			$scope.storages = response.data;
				$scope.storages.sort((a, b) => a.description.toLowerCase().localeCompare(b.description.toLowerCase()));
			if(response.data.length > 0){				
				for( storage of $scope.storages){
					defaultColors[storage.description] = storage.colorId == null ? getRandomColor() : storage.colorId;
					listaStorages.push(storage.description);
					console.log(storage.description)
				}
			}
			
		}, function(error){
			//mensagem de erro
		});
	}

	$scope.showModalWindow = function () {
		getUsers();
		getClients();
		getStorages();
		

		$("#parametersModal").modal({ backdrop: 'static', keyboard: false });

	}

	$scope.printChart = function (charId) {
		var img = document.createElement('img');
		img.src = document.getElementById(charId).toDataURL();

		var div = document.getElementById('printChart');
		div.innerHTML = '';
		div.appendChild(img);

		setTimeout(function () {
			var e = document.getElementById('btnPrinterChart');
			e.click();
		}, 500)
	}

	$scope.downloadChart = function (charId) {
		var dwn = document.getElementById('dwnPrinterChart');
		dwn.href = document.getElementById(charId).toDataURL();
		dwn.click();
	}

	$scope.showUsersSelect = function () {
		$scope.showUsers = $scope.showUsers ? false : true

		if ($scope.showUsers)
			$("#userTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up')
		else
			$("#userTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down')
	}
	$scope.showClientSelect = function () {
		$scope.showClient = $scope.showClient ? false : true;

		if ($scope.showClient)
			$("clientTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
		else
			$("clientTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
	}

	$scope.showStoragesSelect = function () {
		$scope.showStorage   = $scope.showStorage ? false : true;

		if ($scope.showStorage)
			$("storageTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
		else
			$("storageTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
	}

	$scope.showEventsSelect = function () {
		$scope.showEvents = $scope.showEvents ? false : true
	}

	$scope.mostrarGraficosInventario = function () {
		$scope.mostrarinventoryes = $scope.mostrarinventoryes ? false : true;

		if ($scope.mostrarinventoryes) {
			$("#arrow-client").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
			$("#chartTemps").css("display", "");
			$("#btnGraphInventory").css("opacity", "1", "border", "1px solid");
			$("#description-client").css("font-weight", "bold");
			
		}
		else {
			$("#arrow-client").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
			$("#chartTemps").css("display", "none");
			$("#btnGraphInventory").css("opacity", "0.6", "border", "0");
			$("#description-client").css("font-weight", "normal");
		}
	}

	$scope.mostrarGraficosEquipamentos = function () {
		$scope.mostrarEquipamentos = $scope.mostrarEquipamentos ? false : true;

		if ($scope.mostrarEquipamentos) {
			$("#arrow-equipment").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
			$("#chartOffendersEquipment").css("display", "");
			$("#btnGraphEquipment").css("opacity", "1", "border", "1px solid");
			$("#description-equipment").css("font-weight", "bold");

			if ($scope.mostrarinventoryes == true) {
				$("#arrow-inventory").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
				$("#chartOffendersinventory").css("display", "none");
				$("#btnGraphinventory").css("opacity", "0.6", "border", "0");
				$("#description-inventory").css("font-weight", "normal");
				$scope.mostrarinventoryes = false;
			}
		}
		else {
			$("#arrow-equipment").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
			$("#chartOffendersEquipment").css("display", "none");
			$("#btnGraphEquipment").css("opacity", "0.6", "border", "0");
			$("#description-equipment").css("font-weight", "normal");
		}
	}
});
