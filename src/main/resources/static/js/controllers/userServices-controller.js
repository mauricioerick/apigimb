appGIMB.controller("userServicesController", function ($scope, $http, $routeParams, $location, config, juiceService, geralAPI) {

	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');
	if (!controllerHeadCheck($scope, $location, juiceService)) return;

	$('#divMenu').show();

	$scope.user = {};
	$scope.dadoSelecionado = {};
	$scope.dados = [];

	function carregarUsuario(userId) {

		$http.get(config.baseUrl + "/admin/user/" + userId)
			.then(function (response) {
				$scope.user = response.data;
				$scope.user.pass = "";
				$scope.user.userWeb = !response.data.userWeb ? "0" : "1";

				carregarDadosServicosPorUsuario($scope.user);
			},
				function (response) {
					alert("Erro ao carregar usuário!");
				});
	}

	function carregarDadosServicosPorUsuario(user) {
		$http.get(config.baseUrl + "/dadosServicosPorUsuario/" + user.userId)
			.then(function (response) {
				$scope.dados = response.data;
			}, function (response) {
				alert("Erro ao carregar serviços. \n" + response.message);
			});
	}

	$scope.selecionaDado = function (obj) {
		$scope.dadoSelecionado = obj;
	}

	function refreshPage() {
		$scope.user = {};
	}

	function convertDate(data) {
		var date = [];
		date = data.split("/");
		return date[2] + "-" + date[1] + "-" + date[0];
	}

	carregarUsuario($routeParams.userId);
});
