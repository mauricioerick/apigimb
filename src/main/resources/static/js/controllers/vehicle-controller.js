appGIMB.controller("vehicleController", function ($http, $scope, $routeParams, clientAPI, config, juiceService, $location) {

	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');
	if (!controllerHeadCheck($scope, $location, juiceService)) return;

	$('#divMenu').show();

	$scope.clientes = {};
	$scope.vehicle = {};
	$scope.veiculos = [];

	if (!$routeParams.vehicleId)
		carregar();
	else {
		carregarVeiculo($routeParams.vehicleId);
	}

	function carregarClientes() {
		clientAPI.getClients()
			.then(function (response) {
				$scope.clientes = response.data;

			}, function (response) {
				$location.path(juiceService.appUrl + 'login');
			});
	};

	function carregar() {
		$http.get(config.baseUrl + "/admin/vehicle")
			.then(function (response) {
				$scope.veiculos = response.data;
				carregarClientes();
			}, function (error) {
				//mensagem de erro
			});
	}

	$scope.loadClients = function () {
		carregarClientes();
	}

	$scope.carregarVeiculos = function () {
		carregar();
	};

	function carregarVeiculo(vehicleId) {
		$http.get(config.baseUrl + "/admin/vehicle/" + vehicleId)
			.then(function (response) {
				carregarClientes();
				$scope.vehicle = response.data;
			}, function (error) {

			});
	}

	$scope.criar = function (obj) {

		$http.put(config.baseUrl + "/admin/createVehicle", obj)
			.then(function (response) {
				refreshPage();
			}, function (error) {

			});
	};

	function refreshPage() {
		$scope.clientes = {};
		$scope.vehicle = {};
		carregarClientes();
	}

});
