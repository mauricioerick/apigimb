appGIMB.controller("storageController", function ($scope, $http, $routeParams, $location, config, juiceService) {

	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');
	$('#divMenu').show();

	if (!controllerHeadCheck($scope, $location, juiceService))
		return;

	var error = $scope.getMessage('ErrorSavingData');
	$scope.storage = { active: true, colorId: getRandomColor() };
	$scope.storages = [];
	$scope.allStorages = [];
	$scope.optActive = "active";

	changeScreenHeightSize('#divTableStorage');
	colorSelectorConf('colorSel', $scope.storage.colorId);

	if ($routeParams.storageId) {
		loadStorage($routeParams.storageId);
	} else {
		loadStorages();
	}

	function loadStorage(storageId) {

		$http.get(config.baseUrl + "/admin/storage/" + storageId).then(
			function (response) {
				$scope.storage = response.data;
				if ($scope.storage.colorId == null)
					$scope.storage.colorId = getRandomColor();

				colorSelectorConf('colorSel', $scope.storage.colorId);

			},
			function (response) {
				alert($scope.getMessage('ErrorLoadData'));
			}
		);
	}

	function loadStorages() {
		$http.get(config.baseUrl + "/admin/storage").then(
			function (response) {
				$scope.allStorages = response.data;
				
				$scope.query = getTextStorage();
				$scope.optActive = getOptionStorage().length > 0 ? getOptionStorage() : 'active'; 
				
				filterStorageByStatus();
			}, function (response) {
				alert($scope.getMessage('ErrorLoadData'));
			}
		);
	}

	$scope.saveStorage = function (obj) {
		if (!validateForm())
			return;

		var url = config.baseUrl + "/admin/createStorage";
		if (obj.observationId > 0)
			url = config.baseUrl + "/admin/updateStorage/" + obj.storageId;

		$http.put(url, obj).then(
			function (response) {
				if (response.status >= 200 && response.status <= 299) {
					alert($scope.getMessage('DataSavedSuccessfully'));
					refreshPage();
					$location.path(juiceService.appUrl + 'storage');
				} else {
					refreshPage();
					alert(error + response.status + "\n" + response.message);
				}

			},
			function (response) {
				refreshPage();
				alert(error + response.status + "\n" + response.message);
			}
		);
	};

	function refreshPage() {
		$scope.storage = {};
	}

	$scope.filterStorageByStatus = function () {
		filterStorageByStatus();
	};

	function filterStorageByStatus() {
		$scope.storages = $scope.allStorages.filter(isTrue);
		
		setOptionStorage($scope.optActive);
		
		if($scope.optActive == 'active')
			document.getElementById('optActive').checked = true;
		else if($scope.optActive == 'inactive')
			document.getElementById('optInactive').checked = true;
		else
			document.getElementById('optAll').checked = true;
	}

	function isTrue(event) {
		if ($scope.optActive == "all")
			return true;
		else if ($scope.optActive == "active" && event.active == true)
			return true;
		else if ($scope.optActive == "inactive" && event.active == false)
			return true;

		return false;
	}

	$scope.trataAtivoInativo = function (objectType) {
		if (objectType.active == true)
			return $scope.getLabel('Active');
		else
			return $scope.getLabel('Inactive');
	};

	$scope.retornaClasse = function (objectType) {
		if (objectType.active == true)
			return 'btnativo';
		else
			return 'btninativo';
	};

	$scope.mudaSituacao = function (obj) {

		obj.active = obj.active == true ? false : true;

		$http.put(config.baseUrl + "/admin/updateStorage/" + obj.storageId, obj).then(
			function (response) {
				alert($scope.getMessage('DataSavedSuccessfully'));
				loadStorages();
			},
			function (response) {
				alert($scope.getMessage('ErrorSavingDataWithoutParam'));
			}
		);
	};
	
	$scope.setTextStorage = () => {
		setTextStorage($scope.query);
	}
});