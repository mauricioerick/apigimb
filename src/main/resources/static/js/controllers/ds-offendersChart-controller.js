appGIMB.controller("dsOffendersChartController", function ($scope, $http, $location, config, juiceService) {

	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');
	if (!controllerHeadCheck($scope, $location, juiceService, false)) return;

	$scope.pathImg = config.pathImg;
	$('.ipt').attr('disabled', 'disabled');
	$('#divMenu').show();
	$("body").css("overflowY", "auto");
	$("#dtInicio").datepicker(datePickerOptions);
	$("#dtFinal").datepicker(datePickerOptions);

	$scope.users = [];
	$scope.usersSelected = [];
	$scope.usersNames = "";

	$scope.clients = [];
	$scope.clientsSelected = [];
	$scope.clientsNames = "";

	$scope.equipments = [];
	$scope.equipmentsSelected = [];
	$scope.equipmentsNames = "";

	$scope.maximumRecords = 10;

	$scope.graphType = 'bar';
	selectedGraphType = 'bar';

	$scope.listaServicos = [];

	listaItens = [];
	listaClientes = [];
	listaEquipamentos = [];

	itensPorEquipamento = {};
	itensPorCliente = {};

	canRunReload = false;

	dtIni = lastUtilDay();
	dtFin = lastUtilDay();

	defaultColors = {};

	$scope.dtInicio = dtIni;
	$scope.dtFinal = dtFin;

	$scope.showUsers = false;
	$scope.showClient = false;
	$scope.showEquipments = false;

	chartItemsColor = '#b71c1c';

	configUIComponents();

	$scope.translate = function (word) {
		return getLabel(word);
	}

	$scope.reload = function () {
		dtaInicio = document.getElementById("dtInicio").value;
		dtaFim = document.getElementById("dtFinal").value;
		if (canRunReload) {
			setDataInicioFiltroOS(document.getElementById("dtInicio").value);
			setDataFinalFiltroOS(document.getElementById("dtFinal").value);

			if ($scope.showUsers)
				$scope.showUsersSelect()

			if ($scope.showEvents)
				$scope.showEventsSelect()

			$("#parametersModal").modal('hide')
			loadTransfer();
		}
	}

	$scope.buscar = function () {
		$('#modal_carregando').modal('show');
		
		var params = {};

		dtaInicio = document.getElementById("dtInicio").value;
		dtaFim = document.getElementById("dtFinal").value;

		params["startDate"] = dtaInicio;
		params["endDate"] = dtaFim;
		params["user"] = JSON.parse(localStorage.getItem("bobby"))

		$http({
			method: 'PUT',
			url: config.baseUrl + "/servicoWeb",
			data: params
		}).then(function (resposta) {
			$scope.listaServicos = resposta.data;
			preparaInformacoes();
			createChartItens();
			createChartEquipments();
			createChartClients();
			createChartOffendersByClient();
			createChartOffendersByEquipment();
			configChartsEvents(document);

			$('#modal_carregando').modal('hide');
		}, function (resposta) {
			alert($scope.getMessage('ErrorLoadingServices') + resposta.message);

			$('#modal_carregando').modal('hide');
		});
	}

	function filtraRegistros() {
		let listaServicosFiltrados = [];

		$scope.listaServicos.forEach(servico => {
			listaServicosFiltrados.push(servico); // Insere o servico em um array auxiliar
			let removeuElemento = false;

			if ($scope.usersSelected.length > 0 && removeuElemento == false) { // Caso usuario inseriu algum filtro de usuario
				let idx = 1; // variavel de index (auxiliar)
				$scope.usersSelected.forEach(usuario => { // Para cada usuario inserido no filtro verifica se existe no servico corrente
					if (usuario.userId == servico.user.userId) return true; // caso tiver, sai da iteracao
					if (usuario.userId != servico.user.userId && idx === $scope.usersSelected.length) { // caso nenhum usuario pertencer ao servico, e for a ultima iteracao
						let indexServico = listaServicosFiltrados.indexOf(servico); // pega o index do servico no array
						listaServicosFiltrados.splice(indexServico, 1); // remove o servico do array
						removeuElemento = true;
					};
					idx++;
				});
			}

			if ($scope.clientsSelected.length > 0 && removeuElemento == false) {
				let idx = 1;
				$scope.clientsSelected.forEach(cliente => {
					if (cliente.clientId == servico.client.clientId) return true;
					if (cliente.clientId != servico.client.clientId && idx === $scope.clientsSelected.length) {
						let indexServico = listaServicosFiltrados.indexOf(servico);
						listaServicosFiltrados.splice(indexServico, 1);
						removeuElemento = true;
					}
					idx++;
				});
			}

			if ($scope.equipmentsSelected.length > 0 && removeuElemento == false) {
				let idx = 1;
				$scope.equipmentsSelected.forEach(equipamento => {
					if (equipamento.equipmentId == servico.vehicle.equipment.equipmentId) return true;
					if (equipamento.equipmentId != servico.vehicle.equipment.equipmentId && idx === $scope.equipmentsSelected.length) {
						let indexServico = listaServicosFiltrados.indexOf(servico);
						listaServicosFiltrados.splice(indexServico, 1);
						removeuElemento = true;
					}
					idx++;
				});
			}
		});

		$scope.listaServicos = [];
		listaServicosFiltrados.forEach(servico => $scope.listaServicos.push(servico));

		fillLabelsParameters();
	}

	function preparaInformacoes() {
		const itens = {};
		const equipamentos = {};
		const clientes = {};

		let servicosComProblemas = [];

		itensPorEquipamento = {};
		itensPorCliente = {};

		filtraRegistros();

		$scope.listaServicos.forEach(servico => {
			if (servico.picsList !== null) {
				let temProblema = false;
				servico.picsList.forEach(picItem => {
					if (picItem.statusChecklist == false) {
						let total = itens[picItem.message];
						if (total == null) total = 0;

						itens[picItem.message] = ++total;
						temProblema = true;
					}
				})

				if (temProblema) {
					servicosComProblemas.push(servico);
				}
			}
		});
		
		if (servicosComProblemas.lenght == 0)
			$scope.listaServicos = [];
			

		servicosComProblemas.forEach(servico => {
			if (servico.vehicle !== null && servico.vehicle.equipment !== null) {
				let descricaoEquipamento = servico.vehicle.equipment.description;

				let itensEquipamento = itensPorEquipamento[descricaoEquipamento];
				if (itensEquipamento == null) itensEquipamento = {}

				servico.picsList.forEach(picItem => {
					if (picItem.statusChecklist == false) {
						let totalItensEquipamento = itensEquipamento[picItem.message];
						if (totalItensEquipamento == null) totalItensEquipamento = 0;
						itensEquipamento[picItem.message] = ++totalItensEquipamento;

						let totalEquip = equipamentos[descricaoEquipamento];
						if (totalEquip == null) totalEquip = 0;
						equipamentos[descricaoEquipamento] = ++totalEquip;
					}
				})
				itensPorEquipamento[descricaoEquipamento] = itensEquipamento;
			}
		}
		);

		servicosComProblemas.forEach(servico => {
			let desc = servico.client.tradingName;

			let itensCliente = itensPorCliente[desc];
			if (itensCliente == null) itensCliente = {}

			servico.picsList.forEach(picItem => {
				if (picItem.statusChecklist == false) {
					let totalItensCliente = itensCliente[picItem.message];
					if (totalItensCliente == null) totalItensCliente = 0;
					itensCliente[picItem.message] = ++totalItensCliente;

					let totalCli = clientes[desc];
					if (totalCli == null) totalCli = 0;
					clientes[desc] = ++totalCli;
				}
			})
			itensPorCliente[desc] = itensCliente;
		})


		listaItens = [];
		for (key of Object.keys(itens)) {
			listaItens.push({ chave: key, valor: itens[key] });
		}
		listaItens.sort((obj1, obj2) => (obj1.valor < obj2.valor) ? 1 : -1);

		listaEquipamentos = [];
		for (key of Object.keys(equipamentos)) {
			listaEquipamentos.push({ chave: key, valor: equipamentos[key] });
		}
		listaEquipamentos.sort((obj1, obj2) => (obj1.valor < obj2.valor) ? 1 : -1);

		listaClientes = [];
		for (key of Object.keys(clientes)) {
			listaClientes.push({ chave: key, valor: clientes[key] });
		}
		listaClientes.sort((obj1, obj2) => (obj1.valor < obj2.valor) ? 1 : -1);
	}

	function configUIComponents() {
		$('input[type=radio][id=radioGraphTypePie]').change(function () {
			$scope.graphType = 'pie';
			selectedGraphType = 'pie';
		});

		$('input[type=radio][id=radioGraphTypeBar]').change(function () {
			$scope.graphType = 'bar';
			selectedGraphType = 'bar';
		});
	}

	function createChartItens() {
		div = document.getElementById('chartItens');
		div.innerHTML = '';

		div.innerHTML += getHTMLCharCanvas('itens', $scope.getLabel('OffendersItens'), 12);

		dataKeys = [];
		dataValues = [];

		listaItens

		for (i in listaItens) {
			const item = listaItens[i];
			dataKeys.push(item.chave);
			dataValues.push(item.valor);

			if (i == $scope.maximumRecords - 1) break;
		}

		colors = [];
		alpha = 1;
		for (let i = 0; i < dataKeys.length; i++) {
			// color = defaultColors[dataKeys[i]];
			colors.push(hexToRgbA(chartItemsColor, alpha));
			alpha -= 0.07;
		}


		datalabelsConf = { align: 'end', anchor: 'start' };
		if (selectedGraphType == 'pie') datalabelsConf = { align: 'start', anchor: 'end' };

		var data = {
			labels: dataKeys,
			datasets: [{
				data: dataValues,
				backgroundColor: colors,
				datalabels: datalabelsConf
			}]
		}

		context = document.getElementById('chart' + 'itens').getContext('2d');
		chart = new Chart(context, {
			type: selectedGraphType,
			options: {
				plugins: {
					datalabels: {
						color: (selectedGraphType == 'pie') ? 'white' : 'black',
						display: function (context) {
							return true;
						},
						font: {
							weight: 'bold'
						},
						formatter: function (value, context) {
							var lb = formatterHourGraphLabel(value, context) + '\n' + percChart(context.dataset, context.datasetIndex, context.dataIndex);
							return lb;

						},
						clip: true,
						tittle: false
					}
				},
				tooltips: {
					custom: function (tooltip) {
						//tooltip.x = 0;
						//tooltip.y = 0;
					},
					mode: 'single',
					callbacks: {
						label: function (tooltipItems, data) {
							return formatterHourGraphLabel(data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index], chart.controller);
						},
						beforeLabel: function (tooltipItems, data) {
							return percChart(data.datasets, tooltipItems.datasetIndex, tooltipItems.index);
						}
					}
				}
			}
		});
		// labelFormatter = 'HOUR';
		// chart['labelFormatter'] = labelFormatter;
		chart.data = data;
		chart.options.legend.display = false;
		if (selectedGraphType == 'bar') {
			chart.options.scales.xAxes[0].display = false;
			chart.options.scales.yAxes[0].ticks.beginAtZero = true;
		}

		chart.update();
		setChartData('chart' + 'itens', { 'data': data, 'tittle': $scope.getLabel('OffendersItens'), 'labelFormatter': null, type: selectedGraphType });
	}

	function createChartEquipments() {
		div = document.getElementById('chartEquipment');
		div.innerHTML = '';

		div.innerHTML += getHTMLCharCanvas('equipment', $scope.getLabel('OffendersItensByEquipment'), 12);

		dataKeys = [];
		dataValues = [];

		for (i in listaEquipamentos) {
			const equipment = listaEquipamentos[i];
			dataKeys.push(equipment.chave);
			dataValues.push(equipment.valor);

			if (i == $scope.maximumRecords - 1) break;
		}

		colors = [];
		for (let i = 0; i < dataKeys.length; i++) {
			color = defaultColors[dataKeys[i]];
			colors.push(color != null ? hexToRgbA(color, 0.7) : hexToRgbA(getRandomColor(), 0.7));
		}


		datalabelsConf = { align: 'end', anchor: 'start' };
		if (selectedGraphType == 'pie') datalabelsConf = { align: 'start', anchor: 'end' };

		var data = {
			labels: dataKeys,
			datasets: [{
				data: dataValues,
				backgroundColor: colors,
				datalabels: datalabelsConf
			}]
		}

		context = document.getElementById('chart' + 'equipment').getContext('2d');
		chart = new Chart(context, {
			type: selectedGraphType,
			options: {
				plugins: {
					datalabels: {
						color: (selectedGraphType == 'pie') ? 'white' : 'black',
						display: function (context) {
							return true;
						},
						font: {
							weight: 'bold'
						},
						formatter: function (value, context) {
							var lb = formatterHourGraphLabel(value, context) + '\n' + percChart(context.dataset, context.datasetIndex, context.dataIndex);
							return lb;

						},
						clip: true,
						tittle: false
					}
				},
				tooltips: {
					custom: function (tooltip) {
						//tooltip.x = 0;
						//tooltip.y = 0;
					},
					mode: 'single',
					callbacks: {
						label: function (tooltipItems, data) {
							return formatterHourGraphLabel(data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index], chart.controller);
						},
						beforeLabel: function (tooltipItems, data) {
							return percChart(data.datasets, tooltipItems.datasetIndex, tooltipItems.index);
						}
					}
				}
			}
		});
		// labelFormatter = 'HOUR';
		// chart['labelFormatter'] = labelFormatter;
		chart.data = data;
		chart.options.legend.display = false;
		if (selectedGraphType == 'bar') {
			chart.options.scales.xAxes[0].display = false;
			chart.options.scales.yAxes[0].ticks.beginAtZero = true;
		}

		chart.update();
		setChartData('chart' + 'equipment', { 'data': data, 'tittle': $scope.getLabel('OffendersItensByEquipment'), 'labelFormatter': null, type: selectedGraphType });
	}

	function createChartClients() {
		div = document.getElementById('chartClients');
		div.innerHTML = '';

		div.innerHTML += getHTMLCharCanvas('client', $scope.getLabel('OffendersItensByClient'), 12);

		dataKeys = [];
		dataValues = [];

		for (i in listaClientes) {
			const client = listaClientes[i];
			dataKeys.push(client.chave);
			dataValues.push(client.valor);

			if (i == $scope.maximumRecords - 1) break;
		}

		colors = [];
		for (let i = 0; i < dataKeys.length; i++) {
			color = defaultColors[dataKeys[i]];
			colors.push(color != null ? hexToRgbA(color, 0.7) : hexToRgbA(getRandomColor(), 0.7));
		}


		datalabelsConf = { align: 'end', anchor: 'start' };
		if (selectedGraphType == 'pie') datalabelsConf = { align: 'start', anchor: 'end' };

		var data = {
			labels: dataKeys,
			datasets: [{
				data: dataValues,
				backgroundColor: colors,
				datalabels: datalabelsConf
			}]
		}

		context = document.getElementById('chart' + 'client').getContext('2d');
		chart = new Chart(context, {
			type: selectedGraphType,
			options: {
				plugins: {
					datalabels: {
						color: (selectedGraphType == 'pie') ? 'white' : 'black',
						display: function (context) {
							return true;
						},
						font: {
							weight: 'bold'
						},
						formatter: function (value, context) {
							var lb = formatterHourGraphLabel(value, context) + '\n' + percChart(context.dataset, context.datasetIndex, context.dataIndex);
							return lb;

						},
						clip: true,
						tittle: false
					}
				},
				tooltips: {
					custom: function (tooltip) {
						//tooltip.x = 0;
						//tooltip.y = 0;
					},
					mode: 'single',
					callbacks: {
						label: function (tooltipItems, data) {
							return formatterHourGraphLabel(data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index], chart.controller);
						},
						beforeLabel: function (tooltipItems, data) {
							return percChart(data.datasets, tooltipItems.datasetIndex, tooltipItems.index);
						}
					}
				}
			}
		});
		// labelFormatter = 'HOUR';
		// chart['labelFormatter'] = labelFormatter;
		chart.data = data;
		chart.options.legend.display = false;
		if (selectedGraphType == 'bar') {
			chart.options.scales.xAxes[0].display = false;
			chart.options.scales.yAxes[0].ticks.beginAtZero = true;
		}

		chart.update();
		setChartData('chart' + 'client', { 'data': data, 'tittle': $scope.getLabel('OffendersItensByClient'), 'labelFormatter': null, type: selectedGraphType });
	}

	function createChartOffendersByClient() {
		const keys = Object.keys(itensPorCliente);

		const divElement = 'chartOffendersClient';
		let chartName = '';
		let chartLabel = '';
		const chartSize = 4;

		const div = document.getElementById(divElement);

		keys.sort();

		let idx = 1;
		div.innerHTML = '';
		for (const key of keys) {
			chartName = 'itemClient' + idx;
			chartLabel = key;

			div.innerHTML += getHTMLCharCanvas(chartName, chartLabel, chartSize);
			idx++;
		}


		idx = 1;
		for (const key of keys) {
			const itens = itensPorCliente[key];
			chartLabel = key;
			const lista = [];
			for (k of Object.keys(itens)) {
				lista.push({ chave: k, valor: itens[k] });
			}

			chartName = 'itemClient' + idx;

			chartColor = defaultColors[key];
			chartColor = chartColor || getRandomColor();

			dataKeys = [];
			dataValues = [];

			lista.sort((obj1, obj2) => obj2.valor - obj1.valor);

			for (i in lista) {
				const item = lista[i];
				dataKeys.push(item.chave);
				dataValues.push(item.valor);

				if (i == $scope.maximumRecords - 1) break;
			}


			colors = [];
			alpha = 1;
			for (let i = 0; i < dataKeys.length; i++) {
				// color = defaultColors[dataKeys[i]];
				colors.push(hexToRgbA(chartColor, alpha));
				alpha -= 0.07;
			}


			datalabelsConf = { align: 'end', anchor: 'start' };
			if (selectedGraphType == 'pie') datalabelsConf = { align: 'start', anchor: 'end' };

			var data = {
				labels: dataKeys,
				datasets: [{
					data: dataValues,
					backgroundColor: colors,
					datalabels: datalabelsConf
				}]
			}

			context = document.getElementById('chart' + chartName).getContext('2d');
			chart = new Chart(context, {
				type: selectedGraphType,
				options: {
					plugins: {
						datalabels: {
							color: (selectedGraphType == 'pie') ? 'white' : 'black',
							display: function (context) {
								return true;
							},
							font: {
								weight: 'bold'
							},
							formatter: function (value, context) {
								var lb = formatterHourGraphLabel(value, context) + '\n' + percChart(context.dataset, context.datasetIndex, context.dataIndex);
								return lb;

							},
							clip: true,
							tittle: false
						}
					},
					tooltips: {
						custom: function (tooltip) {
							//tooltip.x = 0;
							//tooltip.y = 0;
						},
						mode: 'single',
						callbacks: {
							label: function (tooltipItems, data) {
								return formatterHourGraphLabel(data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index], chart.controller);
							},
							beforeLabel: function (tooltipItems, data) {
								return percChart(data.datasets, tooltipItems.datasetIndex, tooltipItems.index);
							}
						}
					}
				}
			});
			// labelFormatter = 'HOUR';
			// chart['labelFormatter'] = labelFormatter;
			chart.data = data;
			chart.options.legend.display = false;
			if (selectedGraphType == 'bar') {
				chart.options.scales.xAxes[0].display = false;
				chart.options.scales.yAxes[0].ticks.beginAtZero = true;
			}

			chart.update();
			setChartData('chart' + chartName, { 'data': data, 'tittle': chartLabel, 'labelFormatter': null, type: selectedGraphType });
			idx++;
		}
	}

	function createChartOffendersByEquipment() {
		const keys = Object.keys(itensPorEquipamento);

		const divElement = 'chartOffendersEquipment';
		let chartName = '';
		let chartLabel = '';
		const chartSize = 4;

		const div = document.getElementById(divElement);
		// div.innerHTML += '';

		keys.sort();

		let idx = 1;
		div.innerHTML = '';
		for (const key of keys) {
			chartName = 'itemEquipment' + idx;
			chartLabel = key;

			div.innerHTML += getHTMLCharCanvas(chartName, chartLabel, chartSize);
			idx++;
		}

		idx = 1;
		for (const key of keys) {
			const itens = itensPorEquipamento[key];
			const lista = [];
			chartLabel = key;
			for (k of Object.keys(itens)) {
				lista.push({ chave: k, valor: itens[k] });
			}

			chartName = 'itemEquipment' + idx;

			chartColor = defaultColors[key];
			chartColor = chartColor || getRandomColor();

			dataKeys = [];
			dataValues = [];

			lista.sort((obj1, obj2) => obj2.valor - obj1.valor);

			for (i in lista) {
				const item = lista[i];
				dataKeys.push(item.chave);
				dataValues.push(item.valor);

				if (i == $scope.maximumRecords - 1) break;
			}

			let alpha = 1;
			colors = [];
			for (let i = 0; i < dataKeys.length; i++) {
				// color = defaultColors[dataKeys[i]];
				colors.push(hexToRgbA(chartColor, alpha));
				alpha -= 0.07;
			}


			datalabelsConf = { align: 'end', anchor: 'start' };
			if (selectedGraphType == 'pie') datalabelsConf = { align: 'start', anchor: 'end' };

			var data = {
				labels: dataKeys,
				datasets: [{
					data: dataValues,
					backgroundColor: colors,
					datalabels: datalabelsConf
				}]
			}

			context = document.getElementById('chart' + chartName).getContext('2d');
			chart = new Chart(context, {
				type: selectedGraphType,
				options: {
					plugins: {
						datalabels: {
							color: (selectedGraphType == 'pie') ? 'white' : 'black',
							display: function (context) {
								return true;
							},
							font: {
								weight: 'bold'
							},
							formatter: function (value, context) {
								var lb = formatterHourGraphLabel(value, context) + '\n' + percChart(context.dataset, context.datasetIndex, context.dataIndex);
								return lb;

							},
							clip: true,
							tittle: false
						}
					},
					tooltips: {
						custom: function (tooltip) {
							//tooltip.x = 0;
							//tooltip.y = 0;
						},
						mode: 'single',
						callbacks: {
							label: function (tooltipItems, data) {
								return formatterHourGraphLabel(data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index], chart.controller);
							},
							beforeLabel: function (tooltipItems, data) {
								return percChart(data.datasets, tooltipItems.datasetIndex, tooltipItems.index);
							}
						}
					}
				}
			});
			// labelFormatter = 'HOUR';
			// chart['labelFormatter'] = labelFormatter;
			chart.data = data;
			chart.options.legend.display = false;
			if (selectedGraphType == 'bar') {
				chart.options.scales.xAxes[0].display = false;
				chart.options.scales.yAxes[0].ticks.beginAtZero = true;
			}

			chart.update();
			setChartData('chart' + chartName, {
				'data': data,
				'tittle': chartLabel,
				'labelFormatter': null,
				type: selectedGraphType
			});
			idx++;
		}
	}

	function fillLabelsParameters() {
		// Usuários
		if ($scope.usersSelected.length > 0) {
			for (i in $scope.usersSelected) {
				user = $scope.usersSelected[i]
				if (!$scope.usersNames.includes(user.name))
					$scope.usersNames += $scope.usersNames.length == 0 ? user.name : ", " + user.name
			}
		}
		else {
			$scope.usersNames = ""
		}

		// Clientes
		if ($scope.clientsSelected.length > 0) {
			for (i in $scope.clientsSelected) {
				client = $scope.clientsSelected[i]
				if (!$scope.clientsNames.includes(client.tradingName))
					$scope.clientsNames += $scope.clientsNames.length == 0 ? client.tradingName : ", " + client.tradingName
			}
		}
		else {
			$scope.clientsNames = ""
		}

		// Equipamentos
		if ($scope.equipmentsSelected.length > 0) {
			for (i in $scope.equipmentsSelected) {
				equipment = $scope.equipmentsSelected[i]
				if (!$scope.equipmentsNames.includes(equipment.description))
					$scope.equipmentsNames += $scope.equipmentsNames.length == 0 ? equipment.description : ", " + equipment.description
			}
		}
		else {
			$scope.equipmentsNames = ""
		}
	}

	$scope.changeGrapthType = function (event) {
		selectedGraphType = $scope.grapthType;
	}

	function getUsers() {
		$http.get(config.baseUrl + "/admin/user")
			.then(function (response) {
				$scope.users = response.data;
				$scope.users.sort((a, b) => a.name.toLowerCase().localeCompare(b.name.toLowerCase()));

				for (user of $scope.users)
					defaultColors[user.name] = user.colorId == null ? getRandomColor() : user.colorId;

			}, function (error) {
				alert($scope.getMessage('ErrorLoadingUsers'));
			});
	}

	function getClients() {
		$http.get(config.baseUrl + "/admin/client")
			.then(function (response) {
				$scope.clients = response.data;
				$scope.clients.sort((a, b) => a.tradingName.toLowerCase().localeCompare(b.tradingName.toLowerCase()));
				for (client of $scope.clients)
					defaultColors[client.tradingName] = client.colorId == null ? getRandomColor() : client.colorId;
			}, function (error) {
				alert($scope.getMessage('ErrorLoadingUsers'));
			});
	}

	function getEquipment() {
		$http.get(config.baseUrl + "/admin/equipment")
			.then(function (response) {
				$scope.equipments = response.data;
				$scope.equipments.sort((a, b) => a.description.toLowerCase().localeCompare(b.description.toLowerCase()));
				for (equipment of $scope.equipments)
					defaultColors[equipment.description] = equipment.description.colorId == null ? getRandomColor() : equipment.description.colorId;
			}, function (error) {
				alert($scope.getMessage('ErrorLoadingUsers'));
			});
	}

	$scope.showModalWindow = function () {
		getUsers();
		getClients();
		getEquipment();

		$("#parametersModal").modal({ backdrop: 'static', keyboard: false });

	}

	$scope.printChart = function (charId) {
		var img = document.createElement('img');
		img.src = document.getElementById(charId).toDataURL();

		var div = document.getElementById('printChart');
		div.innerHTML = '';
		div.appendChild(img);

		setTimeout(function () {
			var e = document.getElementById('btnPrinterChart');
			e.click();
		}, 500)
	}

	$scope.downloadChart = function (charId) {
		var dwn = document.getElementById('dwnPrinterChart');
		dwn.href = document.getElementById(charId).toDataURL();
		dwn.click();
	}

	$scope.showUsersSelect = function () {
		$scope.showUsers = $scope.showUsers ? false : true

		if ($scope.showUsers)
			$("#userTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up')
		else
			$("#userTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down')
	}

	$scope.showClientSelect = function () {
		$scope.showClient = $scope.showClient ? false : true;

		if ($scope.showClient)
			$("clientTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
		else
			$("clientTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
	}

	$scope.showEquipmentsSelect = function () {
		$scope.showEquipments = $scope.showEquipments ? false : true;

		if ($scope.showEquipments)
			$("equipmentTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
		else
			$("equipmentTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
	}

	$scope.showEventsSelect = function () {
		$scope.showEvents = $scope.showEvents ? false : true
	}

	$scope.mostrarGraficosClientes = function () {
		$scope.mostrarClientes = $scope.mostrarClientes ? false : true;

		if ($scope.mostrarClientes) {
			$("#arrow-client").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
			$("#chartOffendersClient").css("display", "");
			$("#btnGraphClient").css("opacity", "1", "border", "1px solid");
			$("#description-client").css("font-weight", "bold");

			if ($scope.mostrarEquipamentos == true) {
				$("#arrow-equipment").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
				$("#chartOffendersEquipment").css("display", "none");
				$("#btnGraphEquipment").css("opacity", "0.6", "border", "0");
				$("#description-equipment").css("font-weight", "normal");
				$scope.mostrarEquipamentos = false
			}
		}
		else {
			$("#arrow-client").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
			$("#chartOffendersClient").css("display", "none");
			$("#btnGraphClient").css("opacity", "0.6", "border", "0");
			$("#description-client").css("font-weight", "normal");
		}
	}

	$scope.mostrarGraficosEquipamentos = function () {
		$scope.mostrarEquipamentos = $scope.mostrarEquipamentos ? false : true;

		if ($scope.mostrarEquipamentos) {
			$("#arrow-equipment").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
			$("#chartOffendersEquipment").css("display", "");
			$("#btnGraphEquipment").css("opacity", "1", "border", "1px solid");
			$("#description-equipment").css("font-weight", "bold");

			if ($scope.mostrarClientes == true) {
				$("#arrow-client").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
				$("#chartOffendersClient").css("display", "none");
				$("#btnGraphClient").css("opacity", "0.6", "border", "0");
				$("#description-client").css("font-weight", "normal");
				$scope.mostrarClientes = false;
			}
		}
		else {
			$("#arrow-equipment").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
			$("#chartOffendersEquipment").css("display", "none");
			$("#btnGraphEquipment").css("opacity", "0.6", "border", "0");
			$("#description-equipment").css("font-weight", "normal");
		}
	}
});
