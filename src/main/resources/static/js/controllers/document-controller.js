appGIMB.controller("documentController", function ($scope, $http, $routeParams, $location, config, juiceService, geralAPI) {
	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');
	if (!controllerHeadCheck($scope, $location, juiceService))
		return;
	
	$('#divMenu').show();
	
	$scope.documents = [];
	$scope.document = {};
	$scope.location = {};
	
	if($routeParams.documentId)
		carregarDocument($routeParams.documentId);
	else
		carregarDocuments();
	
	function carregarDocument(id){
		var url = config.baseUrl + '/admin/document/' + id;
		
		$http.get(url).then(
			function(response){
				$scope.document = response.data;
				$scope.location = JSON.parse($scope.document.location);
				replaceLocation();
				
			}, function(response){
				alert($scope.getMessage('ErrorLoadData'));
			}
		);
	}
	
	function replaceLocation(){
		$scope.location.LATITUDE = $scope.location.LATITUDE.replace(',', '.');
		$scope.location.LONGITUDE = $scope.location.LONGITUDE.replace(',', '.');
	}
	
	function carregarDocuments(){
		var url = config.baseUrl + '/admin/allDocuments';
		
		$http.get(url).then(
			function(response){
				$scope.documents = response.data;
				
				$scope.query = getTextDocument();
				
				setDownloadOptionSelect();
			},
			function(response){
				alert($scope.getMessage('ErrorLoadData'));
			}
		);
	}
	
	$scope.dadosMapa = function () {
		var marcadores = document;
		marcadores.latStartTime = $scope.location.LATITUDE;
		marcadores.lgtStartTime = $scope.location.LONGITUDE;
		
		dadosMapa(null, marcadores);
	};
	
	$scope.setImageDetailDocument = function(path, idx){
		setImageDetailDocument(path, idx);
	}
	
	$scope.rotateMenosDocument= function(){
		rotateImage('-90', $scope, $("#imageDetailDocument"), $("#divModalDD"), $('#modalContent'));
	}
	
	$scope.rotateMaisSettlement = function(){
		rotateImage('90', $scope, $("#imageDetailDocument"), $("#divModalDD"), $('#modalContent'));
	}
	
	$scope.moveImageToPrevDocument = function(){
		moveImageTo('prev', $("#imageDetailDocument"), $scope.document.picsList);
	}
	
	$scope.moveImageToNextDocument = function(){
		moveImageTo('next', $("#imageDetailDocument"), $scope.document.picsList);
	}
	
	$scope.urlDownloadPhotos = function(){
		return config.baseUrl + '/downloadPhotos/' + $routeParams.documentId;
	}
	
	$scope.urlDownloadSelectPhotos = function(){
		var str = config.baseUrl + '/downloadSeveralPhotos/?';
		
		for(doc of $scope.documents)
			if(doc.optionDownloadSelect)
				str += '&id=' + doc.documentId;
		
		return str;
	}
	
	$scope.changeOptionDownload = function(index){
		$scope.documents[index].optionDownloadSelect = !$scope.documents[index].optionDownloadSelect;
	}
	
	function setDownloadOptionSelect(){
		for(var i = 0; i<$scope.documents.length; i++)
			$scope.documents[i].optionDownloadSelect = false;
	}
	
	$scope.selectDocument = function(){
		var show = false;
		
		for (var doc of $scope.documents)
			if(doc.optionDownloadSelect) show = true;
		
		return show; 
	}
	
	$scope.setTextDocument = () => {
		setTextDocument($scope.query);
	}
});


































