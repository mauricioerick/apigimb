appGIMB.controller("loginController", function($scope, $http,$location, config, juiceService) {
	
	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');

	$scope.appUrl = "/";
	if ($location.path().indexOf("apiGIMB") > -1) {
		$scope.appUrl += "apiGIMB/";
	}

	checkCanLoad();

	$('#divMenu').hide();
	
	// set company on global variable
	$http.get(config.baseUrl + "/company/1")
	.then(function(response) {
		localStorage.setItem("company", JSON.stringify(response.data));
		$scope.companyName = JSON.parse(localStorage["company"]).tradingName;
	}, function(response) {
		
	});

	$scope.User = {};
	$scope.token = "";
	arr = navigator.language.split('-') || navigator.userLanguage.split('-');
	if(localStorage["language"] == null) localStorage.setItem("language", arr[0]);
	$scope.language = localStorage["language"];
	
	iUser = document.getElementById("usuario");
	iPass = document.getElementById("senha");
	
	$scope.loginKeyPress = function(event) {
		if (event.keyCode == 13) {
			if (iUser.value.length > 0 && iPass.value.length > 0)
				$scope.autenticar();
		}
	}

	$scope.autenticar = function() {
		var params = {};
		params["language"] = localStorage["language"];
		params["user"] = $scope.User.user;
		params["pass"] = $scope.User.pass;
		$http.post(config.baseUrl + "/autenticarWeb", params)
		.then(function(response) {
			$scope.token = (response.data != null && response.data.token != null) ? response.data.token : "";

			if ($scope.token != "" && $scope.token != null) {
				localStorage.setItem("token", response.data.token);
				localStorage.setItem("permissionAccess", response.data.role != "C" ? "true" : "false");
				localStorage.removeItem('closeTime');
				
				$scope.User["role"] = response.data.role;
				$scope.User["userId"] = response.data.userId;
				$scope.User["pass"] = response.data.pass;
				$scope.User["profile"] = response.data.profile;
				$scope.User["superUser"] = response.data.superUser;
				
				localStorage.setItem("bobby", JSON.stringify($scope.User));
				
				$(".bobbyVaiPraCasa").prop('disabled', true);
				$(".bobbyVaiPraCasa").css('display', 'none');
				if (localStorage.getItem("permissionAccess") != "false") {
					$(".bobbyVaiPraCasa").prop('disabled', false);
					$(".bobbyVaiPraCasa").css('display', '');
				}
				
				$location.path(juiceService.appUrl + 'home');
			}
			else {
				alert(response.data.message);
				$location.path(juiceService.appUrl + 'login');
			}
		}, function(response) {
			alert(response.data.message);
			$location.path(juiceService.appUrl + 'login');
		});
	};
	
	$scope.update = function() {
		localStorage.setItem("language", $scope.language);
	}
});
