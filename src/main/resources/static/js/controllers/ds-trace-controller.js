appGIMB.controller("dsTraceController", function($scope, $http, geralAPI, $location, juiceService,  $routeParams, config){

	$('form').attr('autocomplete', 'off');
	$('input[type=text]').attr('autocomplete', 'off');
	if (!controllerHeadCheck($scope, $location, juiceService)) return;
	
	$scope.pathImg = config.pathImg;
	$('.ipt').attr('disabled', 'disabled');
	$('#divMenu').show();
	$("body").css("overflowY", "auto");
	
	$scope.users = [];
	$scope.userSelected = [];
	$scope.userNames = "";
	$scope.showUsers = false;

	$scope.clients = [];
	$scope.clientSelected = {};

	$scope.traces = [];

	$scope.userColors = [];

	$scope.title = $scope.getLabel('Route');
	
	var canRunReload = false;

	var startDate = getStartDateTR();
	var endDate = getEndDateTR();

	$scope.startDate = startDate.length == 0 ? dateToString(new Date()) : startDate;
	$scope.endDate =  endDate.length == 0 ? dateToString(new Date()) : endDate;

	setStartDateTR($scope.startDate);
	setEndDateTR($scope.endDate);

	load();
	loadUsers();

	function load() {
		$('#modal_carregando').modal('show');
		
		if(!validationDate()){
			alert($scope.getMessage('veryDistantDates'));
			$('#modal_carregando').modal('hide');
		}
		else {
			var params = {};
			params["startDate"] = getStartDateTR();
			params["endDate"] = getEndDateTR();

			if ($scope.userSelected.length > 0)
				params["user"] = $scope.userSelected.join();

			$http({
				method: 'PUT',
				url: config.baseUrl + "/admin/ds/findTracesByDateBetween",
				data: params
			}).then(function(response) {
				$scope.traces = response.data;
				initializeMap();
				loadDatePicker();
				
				canRunReload = true;
				
				$('#modal_carregando').modal('hide');
			}, function(response) {
				alert($scope.getMessage('ErrorLoadData'));
			});
		}
	}
	
	function validationDate() {
		const startMoth = parseInt($scope.startDate.split("/")[1]);
		const endMoth = parseInt($scope.endDate.split("/")[1]);
		const startYear = parseInt($scope.startDate.split("/")[2]);
		const endYear = parseInt($scope.endDate.split("/")[2]);
		
		if( (endYear - startYear) > 1)
			return false;
		
		else if ((endMoth - startMoth) > 6)
			return false;
		
		else if (((endYear - startYear) == 1) && (12 - startMoth + endMoth) > 6 )
			return false;
		
		return true;
	}

	function loadUsers() {
		$http.get(config.baseUrl + "/admin/ds/listUsers")
		.then(function(response){
			if(response.data != null && response.data.length > 0)
				for(user of response.data)
					$scope.users.push(user.user);
			
			
		}, function(error){
			//mensagem de erro
		});
	}

	function loadClients() {
		$http.get(config.baseUrl + "/admin/ds/listClients")
		.then(function(response){
			$scope.clients = response.data;
			$scope.clientSelected = $scope.clients[0];
		}, function(error){
			//mensagem de erro
		});
	}

	function initializeMap() {
		var mapCoord = getTracers($scope.traces)
		var map = mapCoord[0]
		var coordinatesByUser = mapCoord[1]
		displayCoordinatesOnMap(map, coordinatesByUser)
	}

	function displayCoordinatesOnMap(map, coordinatesByUser) {

		var values = Object.values(coordinatesByUser);
		var keys = Object.keys(coordinatesByUser);

		var bounds = new google.maps.LatLngBounds();
		for (var i = 0; i < values.length; i++) {
			coordinates = values[i];
			for (var j = 0; j < coordinates.length; j++)
				bounds.extend(new google.maps.LatLng(coordinates[j].lat, coordinates[j].lng));
		}
		map.fitBounds(bounds);

		var color = '#FF0000';

		$scope.userColors = [];
		for (var i = 0; i < values.length; i++) {
			$scope.userColors.push({user: keys[i], color: color});

			var polyline = new google.maps.Polyline({
			    path: values[i],
			    geodesic: true,
			    strokeColor: color,
			    strokeOpacity: 1.0,
			    strokeWeight: 3
			  });
			polyline.setMap(map);

			color = getRandomColor();
		}
	}

	function getRandomColor() {
		var letters = '0123456789ABCDEF';
		var color = '#';

		for (var i = 0; i < 6; i++)
			color += letters[Math.floor(Math.random() * 16)];

		return color;
	}

	$scope.reload = function() {
		if (canRunReload) {
			setStartDateTR(document.getElementById("startDate").value);
			setEndDateTR(document.getElementById("endDate").value);

			load();
		}
		
		fillLabelsParameters();
	}
	
	$scope.showModalWindow = () => {
		$("#parametersModal").modal({ backdrop: 'static', keyboard: false });
	} 

	$scope.showUserSelect = () => {
		$scope.showUsers = !$scope.showUsers;
		
		if($scope.showUsers)
			$("#userTriangle").removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
		else
			$("#userTriangle").removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
	}
	
	function fillLabelsParameters(){
		$scope.userNames = "";
		
		if($scope.userSelected.length > 0)
			for(user of $scope.userSelected)
				if(!$scope.userNames.includes(user))
					$scope.userNames += $scope.userNames.length == 0 ? user : ", " + user; 
	}
	
	function loadDatePicker(){
		$("#startDate").datepicker(datePickerOptions);
		$("#endDate").datepicker(datePickerOptions);
	}
	
});
