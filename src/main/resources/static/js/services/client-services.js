appGIMB.factory("clientAPI", function($http, config){
	var _getClients = function() {
	    return $http.get("/admin/client");
	  };

	  return {
	    getClients: _getClients
	  };
});
