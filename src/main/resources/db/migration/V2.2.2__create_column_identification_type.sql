alter TABLE `type_payment` add COLUMN identification_type varchar(10);

update `type_payment` set identification_type = "TP" where payment_identifier = true;
