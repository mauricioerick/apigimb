ALTER TABLE type_payment ADD COLUMN
fields text;

ALTER TABLE type_payment ADD COLUMN
images text;

update type_payment 
  set fields = "[]",
      images = "[]";