alter table `user` add column card_id_main bigint(20);
alter table `user` add CONSTRAINT fk_card_id_main FOREIGN KEY (card_id_main) REFERENCES card(card_id);
