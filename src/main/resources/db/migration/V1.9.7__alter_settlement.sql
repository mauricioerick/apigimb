ALTER TABLE settlement ADD COLUMN status TEXT NULL;
UPDATE settlement SET status = 'T' WHERE status IS NULL;