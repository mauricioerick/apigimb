CREATE TABLE text_template (
  text_template_id bigint(20) PRIMARY KEY AUTO_INCREMENT NOT NULL,
  origin char
(3) NOT NULL,
  template text NULL,
  guid char
(36) UNIQUE NOT NULL
)