ALTER TABLE action ADD active bit(1) DEFAULT b'1' NULL;
ALTER TABLE event ADD active bit(1) DEFAULT b'1' NULL;
ALTER TABLE equipment ADD active bit(1) DEFAULT b'1' NULL;
ALTER TABLE profile ADD active bit(1) DEFAULT b'1' NULL;

