create table checklist
(
	checklist_id bigint auto_increment,
	cheklist_item VARCHAR(100) null,
	service_id bigint null,
	status VARCHAR(3),
	guid char(36) NOT NULL UNIQUE,
	constraint checklist_pk
		primary key (checklist_id),
	constraint checklist_services__fk
		foreign key (service_id) references services (service_id)
);