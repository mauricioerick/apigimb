CREATE TABLE `object_type` (
	object_type_id bigint(20) auto_increment NOT NULL,
	description varchar(255) NULL,
	fields TEXT NULL,
	images TEXT NULL,
	active bit(1) NULL,
	color_id varchar(255) NULL,
	CONSTRAINT object_type_PK PRIMARY KEY (object_type_id)
);

CREATE TABLE `storage` (
	storage_id bigint(20) auto_increment NOT NULL,
	description varchar(255) NULL,
	active bit(1) NULL,
	color_id varchar(255) NULL,
	CONSTRAINT storage_PK PRIMARY KEY (storage_id)
);

CREATE TABLE `object` (
	object_id bigint(20) auto_increment NOT NULL,
	object_type_id bigint(20) NULL,
	storage_id bigint(20) NULL,
	identifier varchar(255) NULL,
	serial_number varchar(100) NULL,
	patrimony varchar(100) NULL,
	location varchar(255) NULL,
	user_id_create bigint(20) NULL,
	date_time_create varchar(20) NULL,
	user_id_update bigint(20) NULL,
	date_time_update varchar(20) NULL,
	active bit(1) NULL,
	CONSTRAINT object_PK PRIMARY KEY (object_id),
	CONSTRAINT object_user_create_FK FOREIGN KEY (user_id_create) REFERENCES `user`(user_id),
	CONSTRAINT object_user_update_FK FOREIGN KEY (user_id_update) REFERENCES `user`(user_id),
	CONSTRAINT object_object_type_FK FOREIGN KEY (object_type_id) REFERENCES `object_type`(object_type_id),
	CONSTRAINT object_storage_FK FOREIGN KEY (storage_id) REFERENCES `storage`(storage_id)
);

CREATE TABLE `object_history` (
	object_history_id bigint(20) auto_increment NOT NULL,
	object_id bigint(20) NULL,
	storage_id bigint(20) NULL,
	date_time varchar(20) NULL,
	location varchar(100) NULL,
	user_id bigint(20) NULL,
	note TEXT NULL,
	CONSTRAINT object_history_PK PRIMARY KEY (object_history_id),
	CONSTRAINT object_history_object_FK FOREIGN KEY (object_id) REFERENCES `object`(object_id),
	CONSTRAINT object_history_storage_FK FOREIGN KEY (storage_id) REFERENCES `storage`(storage_id),
	CONSTRAINT object_history_user_FK FOREIGN KEY (user_id) REFERENCES `user`(user_id)
);

CREATE TABLE `observation` (
	observation_id bigint(20) auto_increment NOT NULL,
	description varchar(255) NULL,
	appears_on varchar(255) NULL,
	active bit(1) NULL,
	CONSTRAINT observation_PK PRIMARY KEY (observation_id)
);

