--
-- Table structure for table `action`
--

CREATE TABLE `action` (
  `action_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(80) DEFAULT NULL,
  `messages` varchar(500) DEFAULT NULL,
  `checklist` bit(1) DEFAULT b'0',
  `fields` varchar(5000) DEFAULT NULL,
  `color_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`action_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `client_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(60) DEFAULT NULL,
  `company_name` varchar(60) DEFAULT NULL,
  `document` varchar(20) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `situacao` varchar(7) DEFAULT 'ativo',
  `trading_name` varchar(60) DEFAULT NULL,
  `pass` varchar(45) DEFAULT NULL,
  `color_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `company_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(200) DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `company_name` varchar(80) DEFAULT NULL,
  `document` varchar(20) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `registration` varchar(20) DEFAULT NULL,
  `site` varchar(45) DEFAULT NULL,
  `trading_name` varchar(60) DEFAULT NULL,
  `zip_code` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Table structure for table `custom_field`
--

CREATE TABLE `custom_field` (
  `custom_field_id` int(11) NOT NULL AUTO_INCREMENT,
  `custom_field` varchar(255) DEFAULT NULL,
  `custom_field_value` varchar(255) DEFAULT NULL,
  `reference_id` varchar(255) DEFAULT NULL,
  `reference_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`custom_field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `custom_picture`
--

CREATE TABLE `custom_picture` (
  `custom_picture_id` int(11) NOT NULL AUTO_INCREMENT,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `pic_mime` varchar(3) DEFAULT NULL,
  `pic_name` varchar(80) DEFAULT NULL,
  `pic_path` varchar(255) DEFAULT NULL,
  `reference_id` varchar(255) DEFAULT NULL,
  `reference_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`custom_picture_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `equipment`
--

CREATE TABLE `equipment` (
  `equipment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(80) DEFAULT NULL,
  `estimate_time` int(11) DEFAULT NULL,
  `messages` varchar(2500) DEFAULT NULL,
  `checklist` varchar(5000) DEFAULT NULL,
  `color_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`equipment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `event_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(80) DEFAULT NULL,
  `improdutive_time` bit(1) DEFAULT NULL,
  `appears_on` varchar(255) DEFAULT NULL,
  `fields` varchar(255) DEFAULT NULL,
  `images` varchar(255) DEFAULT NULL,
  `display_formula` bit(1) DEFAULT NULL,
  `color_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `birth_date` date DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  `pass` varchar(60) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `user` varchar(45) NOT NULL,
  `user_web` bit(1) DEFAULT NULL,
  `mail` varchar(80) DEFAULT NULL,
  `phone_2` varchar(20) DEFAULT NULL,
  `active` bit(1) DEFAULT b'1',
  `color_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `settlement`
--

CREATE TABLE `settlement` (
  `settlement_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amount` double DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `payment_id` varchar(255) DEFAULT NULL,
  `payment_type` varchar(255) DEFAULT NULL,
  `client_id` bigint(20) DEFAULT NULL,
  `event_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `start_time` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`settlement_id`),
  KEY `FK_SETTLEMENT_CLIENT` (`client_id`),
  KEY `FK_SETTLEMENT_USER` (`user_id`),
  KEY `FK_SETTLEMENT_EVENT` (`event_id`),
  CONSTRAINT `FK_SETTLEMENT_CLIENT` FOREIGN KEY (`client_id`) REFERENCES `client` (`client_id`),
  CONSTRAINT `FK_SETTLEMENT_EVENT` FOREIGN KEY (`event_id`) REFERENCES `event` (`event_id`),
  CONSTRAINT `FK_SETTLEMENT_USER` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `expense`
--

CREATE TABLE `expense` (
  `expense_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` varchar(20) DEFAULT NULL,
  `end_date` varchar(20) DEFAULT NULL,
  `expense_note` varchar(5000) DEFAULT NULL,
  `payment_expiration_date` varchar(20) DEFAULT NULL,
  `payment_note` varchar(5000) DEFAULT NULL,
  `start_date` varchar(20) DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL,
  `client_id` bigint(20) DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  `payment_client_id` bigint(20) DEFAULT NULL,
  `hash` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`expense_id`),
  KEY `FK_EXPENSE_CLIENT` (`client_id`),
  KEY `FK_EXPENSE_USER` (`create_user`),
  KEY `FK_EXPENSE_PAYMENT_CLIENT` (`payment_client_id`),
  CONSTRAINT `FK_EXPENSE_CLIENT` FOREIGN KEY (`client_id`) REFERENCES `client` (`client_id`),
  CONSTRAINT `FK_EXPENSE_PAYMENT_CLIENT` FOREIGN KEY (`payment_client_id`) REFERENCES `client` (`client_id`),
  CONSTRAINT `FK_EXPENSE_USER` FOREIGN KEY (`create_user`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Table structure for table `expense_item`
--

CREATE TABLE `expense_item` (
  `expense_item_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` varchar(20) DEFAULT NULL,
  `date` varchar(20) DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `quantity` double DEFAULT NULL,
  `total_value` double DEFAULT NULL,
  `unit_value` double DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  `event_id` bigint(20) DEFAULT NULL,
  `expense_id` bigint(20) DEFAULT NULL,
  `settlement_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`expense_item_id`),
  KEY `FK_EXPENSE_ITEM_USER` (`create_user`),
  KEY `FK_EXPENSE_ITEM_EVENT` (`event_id`),
  KEY `FK_EXPENSE_ITEM_EXPENSE` (`expense_id`),
  KEY `FK_EXPENSE_ITEM_SETTLEMENT` (`settlement_id`),
  CONSTRAINT `FK_EXPENSE_ITEM_EVENT` FOREIGN KEY (`event_id`) REFERENCES `event` (`event_id`),
  CONSTRAINT `FK_EXPENSE_ITEM_EXPENSE` FOREIGN KEY (`expense_id`) REFERENCES `expense` (`expense_id`),
  CONSTRAINT `FK_EXPENSE_ITEM_SETTLEMENT` FOREIGN KEY (`settlement_id`) REFERENCES `settlement` (`settlement_id`),
  CONSTRAINT `FK_EXPENSE_ITEM_USER` FOREIGN KEY (`create_user`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Table structure for table `vehicle`
--

CREATE TABLE `vehicle` (
  `vehicle_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `brand` varchar(255) DEFAULT NULL,
  `fabrication_year` int(11) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `model_year` int(11) DEFAULT NULL,
  `plate` varchar(255) DEFAULT NULL,
  `client_client_id` bigint(20) DEFAULT NULL,
  `estimate_time` int(11) DEFAULT NULL,
  `equipment_equipment_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`vehicle_id`),
  KEY `FK_VEHICLE_CLIENT` (`client_client_id`),
  KEY `FK_VEHICLE_EQUIPMENT` (`equipment_equipment_id`),
  CONSTRAINT `FK_VEHICLE_CLIENT` FOREIGN KEY (`client_client_id`) REFERENCES `client` (`client_id`),
  CONSTRAINT `FK_VEHICLE_EQUIPMENT` FOREIGN KEY (`equipment_equipment_id`) REFERENCES `equipment` (`equipment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `service_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `end_time` varchar(20) DEFAULT NULL,
  `lat_end_time` double DEFAULT NULL,
  `lat_start_time` double DEFAULT NULL,
  `lgt_end_time` double DEFAULT NULL,
  `lgt_start_time` double DEFAULT NULL,
  `start_time` varchar(20) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `client_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `vehicle_id` bigint(20) DEFAULT NULL,
  `action_id` bigint(20) DEFAULT NULL,
  `note` varchar(1500) DEFAULT NULL,
  `tags` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`service_id`),
  KEY `FK_SERVICE_CLIENT` (`client_id`),
  KEY `FK_SERVICE_USER` (`user_id`),
  KEY `FK_SERVICE_VEHICLE` (`vehicle_id`),
  KEY `FK_SERVICE_ACTION` (`action_id`),
  CONSTRAINT `FK_SERVICE_ACTION` FOREIGN KEY (`action_id`) REFERENCES `action` (`action_id`),
  CONSTRAINT `FK_SERVICE_CLIENT` FOREIGN KEY (`client_id`) REFERENCES `client` (`client_id`),
  CONSTRAINT `FK_SERVICE_USER` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  CONSTRAINT `FK_SERVICE_VEHICLE` FOREIGN KEY (`vehicle_id`) REFERENCES `vehicle` (`vehicle_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `service_events`
--

CREATE TABLE `service_events` (
  `service_events_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_time` varchar(20) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `event_id` bigint(20) DEFAULT NULL,
  `services_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`service_events_id`),
  KEY `FK_EVET` (`event_id`),
  KEY `FK_SERVICE` (`services_id`),
  CONSTRAINT `FK_EVET` FOREIGN KEY (`event_id`) REFERENCES `event` (`event_id`),
  CONSTRAINT `FK_SERVICE_EVENT_SERVICE` FOREIGN KEY (`services_id`) REFERENCES `services` (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `service_interruption`
--

CREATE TABLE `service_interruption` (
  `service_interruption_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `end_time` varchar(255) DEFAULT NULL,
  `latitude_end_time` double DEFAULT NULL,
  `latitude_start_time` double DEFAULT NULL,
  `longitude_end_time` double DEFAULT NULL,
  `longitude_start_time` double DEFAULT NULL,
  `start_time` varchar(255) DEFAULT NULL,
  `service_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`service_interruption_id`),
  KEY `FK_SERVICE_INTERRUPTION_SERVICE` (`service_id`),
  CONSTRAINT `FK_SERVICE_INTERRUPTION_SERVICE` FOREIGN KEY (`service_id`) REFERENCES `services` (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Table structure for table `service_pics`
--

CREATE TABLE `service_pics` (
  `pic_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pic_latitude` double DEFAULT NULL,
  `pic_longitude` double DEFAULT NULL,
  `pic_mime` varchar(3) DEFAULT NULL,
  `pic_name` varchar(80) DEFAULT NULL,
  `pic_path` varchar(255) DEFAULT NULL,
  `services_id` bigint(20) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `manually` bit(1) DEFAULT b'0',
  `note` varchar(255) DEFAULT NULL,
  `fix_pic_mime` varchar(3) DEFAULT NULL,
  `fix_pic_name` varchar(80) DEFAULT NULL,
  `fix_pic_path` varchar(255) DEFAULT NULL,
  `status_checklist` bit(1) DEFAULT NULL,
  `status_fix` bit(1) DEFAULT NULL,
  PRIMARY KEY (`pic_id`),
  KEY `FK_SERVICE_PICS_SERVICE` (`services_id`),
  CONSTRAINT `FK_SERVICE_PICS_SERVICE` FOREIGN KEY (`services_id`) REFERENCES `services` (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `trace`
--

CREATE TABLE `trace` (
  `trace_id` int(11) NOT NULL AUTO_INCREMENT,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `start_time` varchar(20) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`trace_id`),
  KEY `FK_TRACE_USER` (`user_id`),
  CONSTRAINT `FK_TRACE_USER` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Table structure for table `transfer`
--

CREATE TABLE `transfer` (
  `transfer_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `end_time` varchar(20) DEFAULT NULL,
  `lat_end_time` double DEFAULT NULL,
  `lat_start_time` double DEFAULT NULL,
  `lgt_end_time` double DEFAULT NULL,
  `lgt_start_time` double DEFAULT NULL,
  `start_time` varchar(20) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `client_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `end_time_original` varchar(20) DEFAULT NULL,
  `start_time_original` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`transfer_id`),
  KEY `FK_TRANSFER_CLIENT` (`client_id`),
  KEY `FK_TRANSFER_USER` (`user_id`),
  CONSTRAINT `FK_TRANSFER_CLIENT` FOREIGN KEY (`client_id`) REFERENCES `client` (`client_id`),
  CONSTRAINT `FK_TRANSFER_USER` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `transfer_events`
--

CREATE TABLE `transfer_events` (
  `transfer_events_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_time` varchar(20) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `event_id` bigint(20) DEFAULT NULL,
  `transfers_id` bigint(20) DEFAULT NULL,
  `end_time` varchar(20) DEFAULT NULL,
  `end_time_original` varchar(20) DEFAULT NULL,
  `start_time` varchar(20) DEFAULT NULL,
  `start_time_original` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`transfer_events_id`),
  KEY `FK_EVENT` (`event_id`),
  KEY `FK_TRANSFER` (`transfers_id`),
  CONSTRAINT `FK_EVENT` FOREIGN KEY (`event_id`) REFERENCES `event` (`event_id`),
  CONSTRAINT `FK_TRANSFER` FOREIGN KEY (`transfers_id`) REFERENCES `transfer` (`transfer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




--
-- Default data for table `user`
--

INSERT INTO `user` (birth_date, mail, name, pass, phone, phone_2, `user`, user_web, active, color_id) VALUES('1999-12-30', 'administrador@gimb.com.br', 'GIMB ADMIN', 'f8450a97cc7e38e6d109425c87b41634', '', NULL, 'GIMB.ADMIN', 1, 1, NULL);

