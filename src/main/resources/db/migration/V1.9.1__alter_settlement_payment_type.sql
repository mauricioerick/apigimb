ALTER TABLE settlement ADD type_payment_id bigint(20) NULL;
ALTER TABLE settlement ADD CONSTRAINT FK_SETTLEMENT_TYPE_PAYMENT FOREIGN KEY (type_payment_id) REFERENCES type_payment(type_payment_id);


INSERT INTO type_payment
(type_payment_id, description, consume_balance, payment_identifier, mask, active)
VALUES(1, 'Cartão de Crédito', 0, 1, '####-####-####-####', 1);

INSERT INTO type_payment
(type_payment_id, description, consume_balance, payment_identifier, mask, active)
VALUES(2, 'Espécie', 1, 0, NULL, 1);

UPDATE settlement SET type_payment_id = 1, payment_type = NULL WHERE payment_type = 'CC';
UPDATE settlement SET type_payment_id = 2, payment_type = NULL WHERE payment_type = 'ES';

