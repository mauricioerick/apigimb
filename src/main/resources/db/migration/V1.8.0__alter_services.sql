ALTER TABLE services ADD created_by_user bigint(20) NULL;
ALTER TABLE services ADD CONSTRAINT FK_SERVICE_CREATED_BY_USER FOREIGN KEY (created_by_user) REFERENCES `user`(user_id);
