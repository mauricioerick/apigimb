alter table project add client BIGINT(20);
alter table project add FOREIGN KEY (client) REFERENCES client(client_id);

alter table project add final_date varchar(10);
