CREATE TABLE user_cost_center
(
	user_cost_center_id bigint(20) PRIMARY KEY NOT NULL
	AUTO_INCREMENT,
	user_id bigint
	(20) NOT NULL,
	cost_center_id bigint
	(20) NOT NULL,
	guid char
	(36) NOT NULL UNIQUE
);

	alter table user_cost_center ADD CONSTRAINT `fk_user_id` FOREIGN KEY
	(user_id) REFERENCES user
	(user_id);
	alter table user_cost_center ADD CONSTRAINT `fk_cost_center_id` FOREIGN KEY
	(cost_center_id) REFERENCES cost_center
	(cost_center_id);


DELIMITER #
	CREATE TRIGGER insert_guid 
BEFORE
	INSERT ON
	user_cost_center
	FOR
	EACH
	ROW
	BEGIN
		SET new
		.guid = uuid
		();
	END;
	#
DELIMITER ;
