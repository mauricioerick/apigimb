CREATE TABLE table_parameter (
	table_parameter_id BIGINT auto_increment NOT NULL,
	guid char(36),
	table_reference VARCHAR(100),
	json_version INTEGER NULL,
	json_structure TEXT NULL,
	create_date VARCHAR(20) NULL,
	create_user BIGINT NULL,
	active BIT(1) DEFAULT 1 NULL,
	PRIMARY KEY (table_parameter_id),
	CONSTRAINT FK_TABLE_PARAMETER_USER_CREATE FOREIGN KEY (create_user) REFERENCES `user`(user_id)
);


ALTER TABLE card ADD COLUMN custom_values TEXT DEFAULT NULL;