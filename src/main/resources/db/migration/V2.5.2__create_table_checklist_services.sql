CREATE TABLE checklist_services (
	checklist_services_id bigint(20) NOT NULL AUTO_INCREMENT,
	checklist_name varchar(50),
    active bit(1) default b'1',
    checklist_main_id bigint(20) null,
    service_id bigint null,
    equipment_equipment_id bigint null,
	guid char(36) NOT NULL UNIQUE,
	PRIMARY KEY (checklist_services_id),
    INDEX checklist_idx (checklist_services_id ASC), 
    constraint checklist_services_service__fk
		foreign key (service_id) references services (service_id),
    constraint checklist_services_equipment__fk
		foreign key (equipment_equipment_id) references equipment (equipment_id),
    CONSTRAINT checklist_services_main_id 
        FOREIGN KEY (checklist_main_id) 
        REFERENCES checklist_services (checklist_services_id) 
        ON DELETE NO ACTION 
        ON UPDATE NO ACTION
);

CREATE TABLE checklist_items (
    item_id bigint(20) not null AUTO_INCREMENT,
    item_name varchar(200),
    active bit(1) default b'1',
    checklist_services_id bigint(20),
    note TEXT NULL,
    status VARCHAR(3),
    CHKEXP bit(1) default b'0',
    CHKOBGT bit(1) default b'1',
    CHKGAL bit(1) default b'0',   
    guid char(36) NOT NULL UNIQUE,
    PRIMARY KEY(item_id),
    KEY `FK_EXPENSE_ITEM_CHECKLIST` (`checklist_services_id`),
    CONSTRAINT `FK_EXPENSE_ITEM_CHECKLIST` FOREIGN KEY (`checklist_services_id`) REFERENCES `checklist_services` (`checklist_services_id`)
);