ALTER TABLE expense ADD COLUMN
demonstrative_text text;

update expense 
  set demonstrative_text = "Segue o orçamento descritivo das despesas referente a prestação de servicos de assessoria relacionadas visita, juntamente com a nota de debito para pagamento, de acordo com os responsaveis"
