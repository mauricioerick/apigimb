alter table project add area_id bigint;
alter table project add FOREIGN KEY (area_id) REFERENCES area(area_id);
