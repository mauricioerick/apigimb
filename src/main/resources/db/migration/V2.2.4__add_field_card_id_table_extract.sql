alter table `extract` add column card_id bigint(20);
alter table `extract` add FOREIGN KEY (card_id) REFERENCES card(card_id);