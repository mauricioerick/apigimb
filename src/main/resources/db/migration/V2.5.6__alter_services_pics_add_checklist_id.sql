alter table service_pics
    add checklist_items_item_id bigint null;

alter table service_pics
    add constraint service_pics_checklist_items_item_id_fk
        foreign key (checklist_items_item_id) references checklist_items (item_id);