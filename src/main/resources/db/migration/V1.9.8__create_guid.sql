ALTER TABLE action ADD guid char(36) NOT NULL DEFAULT '';
UPDATE action SET guid = (SELECT uuid());
ALTER TABLE action ADD UNIQUE (guid);

ALTER TABLE balance ADD guid char(36) NOT NULL DEFAULT '';
UPDATE balance SET guid = (SELECT uuid());
ALTER TABLE balance ADD UNIQUE (guid);

ALTER TABLE client ADD guid char(36) NOT NULL DEFAULT '';
UPDATE client SET guid = (SELECT uuid());
ALTER TABLE client ADD UNIQUE (guid);

ALTER TABLE company ADD guid char(36) NOT NULL DEFAULT '';
UPDATE company SET guid = (SELECT uuid());
ALTER TABLE company ADD UNIQUE (guid);

ALTER TABLE contact ADD guid char(36) NOT NULL DEFAULT '';
UPDATE contact SET guid = (SELECT uuid());
ALTER TABLE contact ADD UNIQUE (guid);

ALTER TABLE custom_field ADD guid char(36) NOT NULL DEFAULT '';
UPDATE custom_field SET guid = (SELECT uuid());
ALTER TABLE custom_field ADD UNIQUE (guid);

ALTER TABLE custom_picture ADD guid char(36) NOT NULL DEFAULT '';
UPDATE custom_picture SET guid = (SELECT uuid());
ALTER TABLE custom_picture ADD UNIQUE (guid);

ALTER TABLE email_config ADD guid char(36) NOT NULL DEFAULT '';
UPDATE email_config SET guid = (SELECT uuid());
ALTER TABLE email_config ADD UNIQUE (guid);

ALTER TABLE equipment ADD guid char(36) NOT NULL DEFAULT '';
UPDATE equipment SET guid = (SELECT uuid());
ALTER TABLE equipment ADD UNIQUE (guid);

ALTER TABLE event ADD guid char(36) NOT NULL DEFAULT '';
UPDATE event SET guid = (SELECT uuid());
ALTER TABLE event ADD UNIQUE (guid);

ALTER TABLE expense ADD guid char(36) NOT NULL DEFAULT '';
UPDATE expense SET guid = (SELECT uuid());
ALTER TABLE expense ADD UNIQUE (guid);

ALTER TABLE expense_item ADD guid char(36) NOT NULL DEFAULT '';
UPDATE expense_item SET guid = (SELECT uuid());
ALTER TABLE expense_item ADD UNIQUE (guid);

ALTER TABLE extract ADD guid char(36) NOT NULL DEFAULT '';
UPDATE extract SET guid = (SELECT uuid());
ALTER TABLE extract ADD UNIQUE (guid);

ALTER TABLE object ADD guid char(36) NOT NULL DEFAULT '';
UPDATE object SET guid = (SELECT uuid());
ALTER TABLE object ADD UNIQUE (guid);

ALTER TABLE object_history ADD guid char(36) NOT NULL DEFAULT '';
UPDATE object_history SET guid = (SELECT uuid());
ALTER TABLE object_history ADD UNIQUE (guid);

ALTER TABLE object_type ADD guid char(36) NOT NULL DEFAULT '';
UPDATE object_type SET guid = (SELECT uuid());
ALTER TABLE object_type ADD UNIQUE (guid);

ALTER TABLE observation ADD guid char(36) NOT NULL DEFAULT '';
UPDATE observation SET guid = (SELECT uuid());
ALTER TABLE observation ADD UNIQUE (guid);

ALTER TABLE profile ADD guid char(36) NOT NULL DEFAULT '';
UPDATE profile SET guid = (SELECT uuid());
ALTER TABLE profile ADD UNIQUE (guid);

ALTER TABLE service_events ADD guid char(36) NOT NULL DEFAULT '';
UPDATE service_events SET guid = (SELECT uuid());
ALTER TABLE service_events ADD UNIQUE (guid);

ALTER TABLE service_interruption ADD guid char(36) NOT NULL DEFAULT '';
UPDATE service_interruption SET guid = (SELECT uuid());
ALTER TABLE service_interruption ADD UNIQUE (guid);

ALTER TABLE service_pics ADD guid char(36) NOT NULL DEFAULT '';
UPDATE service_pics SET guid = (SELECT uuid());
ALTER TABLE service_pics ADD UNIQUE (guid);

ALTER TABLE services ADD guid char(36) NOT NULL DEFAULT '';
UPDATE services SET guid = (SELECT uuid());
ALTER TABLE services ADD UNIQUE (guid);

ALTER TABLE settlement ADD guid char(36) NOT NULL DEFAULT '';
UPDATE settlement SET guid = (SELECT uuid());
ALTER TABLE settlement ADD UNIQUE (guid);

ALTER TABLE storage ADD guid char(36) NOT NULL DEFAULT '';
UPDATE storage SET guid = (SELECT uuid());
ALTER TABLE storage ADD UNIQUE (guid);

ALTER TABLE trace ADD guid char(36) NOT NULL DEFAULT '';
UPDATE trace SET guid = (SELECT uuid());
ALTER TABLE trace ADD UNIQUE (guid);

ALTER TABLE transfer ADD guid char(36) NOT NULL DEFAULT '';
UPDATE transfer SET guid = (SELECT uuid());
ALTER TABLE transfer ADD UNIQUE (guid);

ALTER TABLE transfer_events ADD guid char(36) NOT NULL DEFAULT '';
UPDATE transfer_events SET guid = (SELECT uuid());
ALTER TABLE transfer_events ADD UNIQUE (guid);

ALTER TABLE type_payment ADD guid char(36) NOT NULL DEFAULT '';
UPDATE type_payment SET guid = (SELECT uuid());
ALTER TABLE type_payment ADD UNIQUE (guid);

ALTER TABLE user ADD guid char(36) NOT NULL DEFAULT '';
UPDATE user SET guid = (SELECT uuid());
ALTER TABLE user ADD UNIQUE (guid);

ALTER TABLE vehicle ADD guid char(36) NOT NULL DEFAULT '';
UPDATE vehicle SET guid = (SELECT uuid());
ALTER TABLE vehicle ADD UNIQUE (guid);
