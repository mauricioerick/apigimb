ALTER TABLE settlement ADD COLUMN nature_expense_id bigint
(20);
ALTER TABLE settlement ADD FOREIGN KEY (nature_expense_id) REFERENCES nature_expense(nature_expense_id);