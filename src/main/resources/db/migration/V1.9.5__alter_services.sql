ALTER TABLE services ADD COLUMN feedback TEXT NULL;
ALTER TABLE services ADD COLUMN technical_analysis TEXT NULL;
ALTER TABLE services ADD COLUMN conclusion TEXT NULL;