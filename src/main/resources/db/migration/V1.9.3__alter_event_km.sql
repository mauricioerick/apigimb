ALTER TABLE event ADD request_km bit(1) NULL;
ALTER TABLE transfer_events ADD km_start DOUBLE NULL;
ALTER TABLE transfer_events ADD km_end DOUBLE NULL;
ALTER TABLE transfer_events ADD km_traveled DOUBLE NULL;
