CREATE TABLE type_payment (
	type_payment_id bigint(20) NOT NULL AUTO_INCREMENT,
	description varchar(255) NULL,
	consume_balance bit(1) NULL,
	payment_identifier bit(1) NULL,
	mask varchar(255) NULL,
	active bit(1) NULL,
	color_id varchar(100),
	CONSTRAINT type_payment_PK PRIMARY KEY (type_payment_id)
);

CREATE TABLE extract (
	extract_id bigint(20) NOT NULL AUTO_INCREMENT,
	release_date varchar(20) NULL,
	type varchar(1) NULL,
    amount double(11,2) NULL,
	settlement_id bigint(20) NULL,
	payment_type_id bigint(20) NULL,
	status varchar(1) NULL,
	create_date varchar(20) NULL,
	create_user bigint(20) NULL,
	approve_date varchar(20) NULL,
	approve_user bigint(20) NULL,
	user_id bigint(20) NULL,
	info TEXT NULL,
	note TEXT NULL,
	CONSTRAINT EXTRACT_PK PRIMARY KEY (extract_id),
	CONSTRAINT FK_EXTRACT_USER FOREIGN KEY (user_id) REFERENCES `user`(user_id),
	CONSTRAINT FK_EXTRACT_CREATE_USER FOREIGN KEY (create_user) REFERENCES `user`(user_id),
	CONSTRAINT FK_EXTRACT_APPROVE_USER FOREIGN KEY (approve_user) REFERENCES `user`(user_id),
	CONSTRAINT FK_EXTRACT_PAYMENT_TYPE FOREIGN KEY (payment_type_id) REFERENCES type_payment(type_payment_id),
	CONSTRAINT FK_EXTRACT_SETTLEMENT FOREIGN KEY (settlement_id) REFERENCES settlement(settlement_id)
);

CREATE TABLE balance (
	balance_id bigint(20) NOT NULL AUTO_INCREMENT,
    day varchar(20) NULL,
	month varchar(20) NULL,
	year varchar(20) NULL,
	balance double(11,2) NULL,
	user_id bigint(20) NULL,
    payment_type_id bigint(20) NULL,
	CONSTRAINT BALANCE_PK PRIMARY KEY (balance_id),
	CONSTRAINT FK_BALANCE_USER FOREIGN KEY (user_id) REFERENCES `user`(user_id),
	CONSTRAINT FK_BALANCE_PAYMENT_TYPE FOREIGN KEY (payment_type_id) REFERENCES type_payment(type_payment_id)
);