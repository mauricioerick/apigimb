create table nature_expense (
	nature_expense_id bigint(20) not null AUTO_INCREMENT,
	description text,
	active bit(1) DEFAULT b'1',
	color_id varchar(255) NULL,
	guid char(36) NOT NULL DEFAULT '',
	PRIMARY KEY (`nature_expense_id`)
);

create table cost_center (
	cost_center_id bigint(20) not null AUTO_INCREMENT,
	account_code varchar(100),
	description text,
	active bit(1) DEFAULT b'1',
	color_id varchar(255) NULL,
	guid char(36) NOT NULL DEFAULT '',
	PRIMARY KEY (`cost_center_id`)
);

create table area (
	area_id bigint(20) not null AUTO_INCREMENT,
	description text,
	active bit(1) default b'1',
	color_id varchar(255) null,
	guid char(36) NOT NULL DEFAULT '',
	PRIMARY key (`area_id`)
);
