create table debit_note
(
	debit_note_id bigint auto_increment,
	sequence bigint not null,
	series VARCHAR(10) not null,
	guid char(36) NOT NULL DEFAULT '',
	constraint debit_note_pk
		primary key (debit_note_id)
);

create unique index debit_note_series_sequence_uindex
	on debit_note (series, sequence);

alter table expense add column debit_note_id bigint;
alter table expense add FOREIGN KEY (debit_note_id) REFERENCES debit_note(debit_note_id);
