alter table action add column inventory bit(1) default 0;

alter table services
    add justify_not_inventory text null;

alter table object_history
    add service_id bigint null;

alter table object_history
    add constraint FK_OBJECT_HISTORY_SERVICE
        foreign key (service_id) references services (service_id);

create table equipment_object_type
(
    equipment_id bigint not null,
    object_type_id bigint not null,
    constraint equipment_object_type_pk
        primary key (equipment_id, object_type_id),
    constraint equipment_fk
        foreign key (equipment_id) references equipment (equipment_id),
    constraint object_type_fk
        foreign key (object_type_id) references object_type (object_type_id)
);