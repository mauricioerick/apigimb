ALTER TABLE extract ADD COLUMN amount_approved DECIMAL(11,2) NULL;

update `extract`
   set amount_approved = amount
 where status = 'A';