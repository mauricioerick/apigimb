create TABLE `device` (
	device_id bigint(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    push_token varchar(200),
    user_id bigint(20),
    active bit(1) DEFAULT b'1',
    guid char(36) NOT NULL UNIQUE,
    FOREIGN KEY (user_id) REFERENCES user(user_id)
);
