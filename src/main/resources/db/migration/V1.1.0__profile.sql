CREATE TABLE `profile` (
  `profile_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `app_access` text,
  `description` varchar(80) DEFAULT NULL,
  `web_access` text,
  `config` text,
  PRIMARY KEY (`profile_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `user` ADD profile_id bigint(20) NULL;
ALTER TABLE `user` ADD CONSTRAINT user_profile_FK FOREIGN KEY (profile_id) REFERENCES profile(profile_id);
