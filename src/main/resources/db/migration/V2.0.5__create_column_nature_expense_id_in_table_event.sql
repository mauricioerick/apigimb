ALTER TABLE event ADD COLUMN nature_expense_id bigint
(20) null;
ALTER TABLE event ADD CONSTRAINT fk_nature_expense FOREIGN KEY (nature_expense_id) REFERENCES nature_expense(nature_expense_id);
