CREATE TABLE email_config (
  email_config_id INT NOT NULL AUTO_INCREMENT,
  sender TEXT DEFAULT NULL,
  request_credit TEXT DEFAULT NULL,
  PRIMARY KEY (email_config_id)
);

INSERT INTO email_config (email_config_id, sender, request_credit) VALUES (1, '{"smtp_host":"", "smtp_port":"", "user":"", "pass":""}', '{"mail_to":"", "subject":"Solicitação de adiantamento", "body":"<h2>Olá!</h2><br><br>O usuário <strong>{user}</strong> está solicitando o adiantamento de {typePayment} no valor de <strong>{amount}</strong> pelo motivo descrito abaixo:<br><h3>{reason}</h3>"}');

