CREATE TABLE project (
    project_id BIGINT NOT NULL,
    project_name VARCHAR(50),
    project_time INT,
    active bit(1) default b'1',
    guid char(36) NOT NULL UNIQUE,
    PRIMARY KEY (project_id)
);

