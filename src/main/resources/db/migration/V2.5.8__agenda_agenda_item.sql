CREATE TABLE IF NOT EXISTS agenda (
    agenda_id INT NOT NULL,
    user_id INT NULL,
    date datetime NULL,
    PRIMARY KEY (agenda_id)
);

ALTER TABLE `agenda` CHANGE COLUMN `agenda_id` `agenda_id` INT(11) NOT NULL AUTO_INCREMENT ;
ALTER TABLE agenda ADD guid char(36) NOT NULL DEFAULT '';
UPDATE agenda SET guid = (SELECT uuid());
ALTER TABLE agenda ADD UNIQUE (guid);

CREATE TABLE IF NOT EXISTS agenda_item (
    agenda_item_id INT NOT NULL,
    agenda_id INT NULL,
    client_id INT NULL,
    action_id INT NULL,
    vehicle_id INT NULL,
    project_id INT NULL,
    type TEXT NULL,
    status TEXT NULL,
    note TEXT NULL,
    PRIMARY KEY (agenda_item_id)
);

ALTER TABLE `agenda_item` CHANGE COLUMN `agenda_item_id` `agenda_item_id` INT(11) NOT NULL AUTO_INCREMENT ;