alter table card
	add user_id bigint null;

alter table card
	add constraint card_user_user_id_fk
		foreign key (user_id) references user (user_id);

update card set user_id = (select user_id from user_card where user_card.card_id = card.card_id LIMIT 1);
