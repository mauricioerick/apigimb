DELIMITER #
CREATE TABLE client_cost_center
(
	client_cost_center_id bigint(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
	client_id bigint(20) NOT NULL,
	cost_center_id bigint(20) NOT NULL,
	guid char(36) NOT NULL UNIQUE,
	FOREIGN KEY (client_id) REFERENCES client(client_id),
	FOREIGN KEY (cost_center_id) REFERENCES cost_center(cost_center_id)
)
#
DELIMITER ;


DELIMITER #
CREATE TRIGGER insert_guid_client_cost_center
BEFORE INSERT ON client_cost_center
FOR EACH  ROW 
BEGIN  
   SET new.guid = uuid();
END;
#
DELIMITER ;