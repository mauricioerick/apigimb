alter table `settlement` add column card_id bigint(20);
alter table `settlement` add FOREIGN KEY (card_id) REFERENCES card(card_id);