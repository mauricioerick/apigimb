DELIMITER #
CREATE TABLE settlement_cost_center
(
	settlement_cost_center_id bigint(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
	settlement_id bigint(20) NOT NULL,
	cost_center_id bigint(20) NOT NULL,
	perc decimal null,
	guid char(36) NOT NULL UNIQUE,
	FOREIGN KEY (settlement_id) REFERENCES settlement(settlement_id),
	FOREIGN KEY (cost_center_id) REFERENCES cost_center(cost_center_id)
)
#
DELIMITER ;


DELIMITER #
CREATE TRIGGER insert_guid_settlement_cost_center
BEFORE INSERT ON settlement_cost_center
FOR EACH  ROW 
BEGIN  
   SET new.guid = uuid();
END;
#
DELIMITER ;