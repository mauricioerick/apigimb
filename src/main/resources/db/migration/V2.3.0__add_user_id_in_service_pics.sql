alter table service_pics
    add user_id bigint null;

alter table service_pics
    add constraint service_pics_user_id_fk
        foreign key (user_id) references user (user_id);