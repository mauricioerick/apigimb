create table user_card(
	user_id bigint(20) not null,
	card_id bigint(20) not null,
	CONSTRAINT fk_user_id_for_card FOREIGN KEY (user_id) REFERENCES `user`(user_id),
	CONSTRAINT fk_card_id_for_user FOREIGN KEY (card_id) REFERENCES card(card_id)
);
