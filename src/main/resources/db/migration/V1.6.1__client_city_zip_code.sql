ALTER TABLE `client` ADD zip_code varchar(60) NULL;
ALTER TABLE `client` ADD city varchar(255) NULL;
ALTER TABLE `client` ADD registration varchar(20) NULL;

ALTER TABLE `company` ADD logo_name varchar(255) NULL;