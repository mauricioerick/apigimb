alter table checklist modify cheklist_item varchar(500);

alter table `action` add column critically_required bit(1) DEFAULT b'0';
alter table `action` add column tools text;

ALTER table service_pics add column work varchar(255);
ALTER table service_pics add column time varchar(255);

create table tools (
	tool_id bigint auto_increment,
	description varchar(255) null,
	active bit(1) DEFAULT b'1',
	guid char(36) not null unique,
	constraint tools_pk
		primary key (tool_id)
); 

create table tool_services_pics (
	tool_services_pics_id bigint auto_increment,
	tools VARCHAR(100) null,
	service_pics_id bigint null,	
	tool_id bigint null,
	guid char(36) NOT NULL UNIQUE,
	constraint tool_pics_pk
		primary key (tool_services_pics_id),
	constraint tool_service_pics_tools__pk
		foreign key (tool_id) references tools (tool_id),
	FOREIGN KEY (service_pics_id) REFERENCES service_pics(pic_id)
);