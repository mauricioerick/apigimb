ALTER TABLE user ADD COLUMN cost_center_id bigint
(20) null;
ALTER TABLE user ADD CONSTRAINT fk_cost_center FOREIGN KEY (cost_center_id) REFERENCES cost_center(cost_center_id);
