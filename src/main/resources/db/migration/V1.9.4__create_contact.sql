CREATE TABLE `contact` (
	contact_id bigint(20) AUTO_INCREMENT NOT NULL,
	client_id bigint(20) NULL,
	name TEXT NULL,
	phone VARCHAR(15) NULL,
	office TEXT NULL,
	observation TEXT NULL,
	CONSTRAINT CONTACT_PK PRIMARY KEY (contact_id),
	CONSTRAINT CONTACT_CLIENT_FK FOREIGN KEY (client_id) REFERENCES client (client_id)
);
