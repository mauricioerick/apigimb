create table area_user(
	area_user_id  bigint(20) not null AUTO_INCREMENT,
	guid char(36) NOT NULL DEFAULT '',
	user_name varchar(50),
	user_user varchar(50),
	area_id bigint(20),
	user_id bigint(20),
	responsible bit(1) default b'0',
	PRIMARY KEY (`area_user_id`)
);

alter table area_user add CONSTRAINT `area_id` FOREIGN KEY (`area_id`) REFERENCES area(`area_id`);
alter table area_user add CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `user`(`user_id`);

