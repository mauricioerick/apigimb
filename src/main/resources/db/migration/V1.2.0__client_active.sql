ALTER TABLE client ADD active bit DEFAULT b'1' NULL;

UPDATE client SET active = b'0' WHERE situacao = 'inativo';
