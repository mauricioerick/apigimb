ALTER TABLE balance ADD COLUMN card_id bigint null;

ALTER TABLE balance ADD CONSTRAINT FK_EXTRACT_CARD FOREIGN KEY (card_id) REFERENCES card(card_id);
