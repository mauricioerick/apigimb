create table imports (
	imports_id bigint(20) NOT NULL AUTO_INCREMENT,
	name_class varchar(50),
	guid char(36) NOT NULL UNIQUE,
	PRIMARY KEY (imports_id)
);
