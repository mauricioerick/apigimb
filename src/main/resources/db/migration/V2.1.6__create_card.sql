create table card(
	card_id bigint(20) not null AUTO_INCREMENT,
	card_number varchar(19),
	card_banner varchar(20),
	emitting_bank varchar(30),
	invoice_close_date integer,
	invoice_due_date integer,
	blocks_launch bit(1) default b'0',
	automatic_renovation bit(1) default b'1',
	renewal_day integer,
	color_id varchar(255) NULL, 
	active bit(1) DEFAULT b'1',
	guid char(36) NOT NULL UNIQUE,
	PRIMARY KEY (`card_id`)
);
