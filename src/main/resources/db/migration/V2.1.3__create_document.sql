CREATE TABLE IF NOT EXISTS document(
	document_id bigint(20) not null AUTO_INCREMENT,
	user_id bigint(20),
	request varchar(50),
	area_id bigint(20),
	note TEXT,
	start_time VARCHAR(25),
	location TEXT,
	guid char(36) NOT NULL DEFAULT '',
	PRIMARY key (document_id)  
);
