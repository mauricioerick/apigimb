ALTER TABLE client ADD COLUMN cost_center_id bigint
(20) null;
ALTER TABLE client ADD FOREIGN KEY
(cost_center_id) REFERENCES cost_center
(cost_center_id);
